#pragma once
#include <filesystem>
#include <ImportExportProgressCallback.h>

namespace Duco
{
    class RingingDatabase;
}

class TowerSearcher : Duco::ImportExportProgressCallback
{
public:
    TowerSearcher();
    ~TowerSearcher();

    size_t ProcessFile(const std::filesystem::path& filePath, const std::string& towerbaseId);

public: // from ImportExportProgressCallback
    virtual void InitialisingImportExport(bool internalising, unsigned int versionNumber, size_t totalNumberOfObjects);
    virtual void ObjectProcessed(bool internalising, Duco::TObjectType objectType, Duco::ObjectId objectId, int totalPercentage, size_t numberInThisSubDatabase);
    virtual void ImportExportComplete(bool internalising);
    virtual void ImportExportFailed(bool internalising, int errorCode);

};

