// DirectoryChecker.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <filesystem>
#include "DatabaseProcessor.h"
#include "TowerSearcher.h"
#include <chrono>

namespace fs = std::filesystem;
using namespace std::chrono;

bool ProcessDirectory(const std::string path)
{
    bool exportXml = false;
    if (path.find("ducowebpage") != std::string::npos)
    {
        exportXml = true;
    }

    std::cout << std::endl << "Processing directory: " << path << std::endl << std::endl;
    std::cout << "Name\t\t\t\t\tVer\tPeals\tRingers\tTowers\tMethods\tAssoc\tPics\tComps\tBB\tErrors\tSetts\tDefaults" << std::endl << std::endl;
    size_t countFiles = 0;
    auto start = high_resolution_clock::now();
    try
    {
        DatabaseProcessor processor;
        size_t noOfPeals = 0;
        for (const auto& entry : fs::recursive_directory_iterator(path))
        {
            if (entry.path().extension() == ".duc")
            {
                noOfPeals += processor.ProcessFile(entry.path(), exportXml);
                ++countFiles;
            }
        }
        std::wcout << "Total number of peals found in this directory: " << noOfPeals << std::endl;
    }
    catch (std::exception ex)
    {
        std::wcerr << "Unable to process files" << std::endl;
        std::wcerr << ex.what() << std::endl;
        return false;
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);

    std::wcout << std::endl << countFiles << " files processed in " << (duration.count() / 1000000) << "seconds." << std::endl;
    return true;
}

bool SearchForPealsInTower(const std::string path, const std::string& towerbaseId)
{
    std::cout << std::endl << "Processing directory: " << path << std::endl << std::endl;
    size_t countFiles = 0;
    auto start = high_resolution_clock::now();
    try
    {
        TowerSearcher processor;
        size_t noOfPeals = 0;
        for (const auto& entry : fs::recursive_directory_iterator(path))
        {
            if (entry.path().extension() == ".duc")
            {
                noOfPeals += processor.ProcessFile(entry.path(), towerbaseId);
                ++countFiles;
            }
        }
        std::wcout << "Total number of peals found in this directory: " << noOfPeals << std::endl;
    }
    catch (std::exception ex)
    {
        std::wcerr << "Unable to process files" << std::endl;
        std::wcerr << ex.what() << std::endl;
        return false;
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);

    std::wcout << std::endl << countFiles << " files processed in " << (duration.count() / 1000000) << "seconds." << std::endl;
    return true;
}

int main(int argc, char* argv[])
{
    std::string defaultDirectory = "C:\\repositories\\duco\\ducowebpage\\downloads";
    bool exportXml = true;
    std::wcout << "Duco Directory checker\n";
    std::wcout << "This will show the database details for all files in a given directory\n";
    std::wcout << "and export the xml and pdf files for all files found" << std::endl;

    const std::string help = "-h";
    const std::string directory = "-d";
    const std::string tower = "-t";
    const std::string helpLong = help + "elp";
    const std::string directoryLong = directory + "irectory";
    const std::string towerLong = tower + "ower";

    bool success = true;
    if (argc == 1)
    {
        success &= ProcessDirectory(defaultDirectory);
        success &= ProcessDirectory("C:\\repositories\\duco\\ducoengine\\DucoEngineTest\\TestData");
    }
    else if (argc == 2)
    {
        if (help.compare(argv[1]) == 0 || helpLong.compare(argv[1]) == 0)
        {
            std::wcerr << "Command line arguments" << std::endl;
            std::wcerr << "-h[elp]\t Help" << std::endl;
            std::wcerr << "-d[directory]\t directory" << std::endl;
            std::wcerr << "\tCheck all duc files in a directory are readable" << std::endl;
            std::wcerr << "-t[ower]\t towerbaseid" << std::endl;
            std::wcerr << "\tSearch all databases in the default directory" << std::endl;
        }
        else
        {
            success &= ProcessDirectory(argv[1]);
        }
    }
    else if (directory.compare(argv[1]) == 0 || directoryLong.compare(argv[1]) == 0)
    {
        success &= ProcessDirectory(argv[1]);
    }
    else if (tower.compare(argv[1]) == 0 || towerLong.compare(argv[1]) == 0)
    {
        DatabaseProcessor processor;
        success &= SearchForPealsInTower(defaultDirectory, argv[2]);
    }
    else
    {
        std::wcout << argc << " arguments - but didnt know what to do." << std::endl;
        while (*argv != NULL)
        {
            printf("%s.\n", *argv);
            argv++;
        }
    }
    return success ? 0 : -1;
}
