#include "TowerSearcher.h"

#include <iostream>
#include <RingingDatabase.h>
#include <filesystem>
#include <PealLengthInfo.h>
#include <StatisticFilters.h>
#include <Tower.h>
#include <TowerDatabase.h>
#include <windows.h>

TowerSearcher::TowerSearcher()
{

}

TowerSearcher::~TowerSearcher()
{

}

size_t
TowerSearcher::ProcessFile(const std::filesystem::path& filePath, const std::string& towerbaseId)
{
    size_t numberOfPeals = 0;

    size_t maxLength = 32;
    std::wstring fileNameToPrint = filePath.filename().wstring();
    fileNameToPrint = fileNameToPrint.substr(0, fileNameToPrint.length() - 4);
    size_t currentLength = fileNameToPrint.length() - 4;
    std::wcout << fileNameToPrint;
    currentLength += (currentLength % 8);
    std::wcout << "\t";
    while (currentLength < maxLength)
    {
        std::wcout << "\t";
        currentLength += 8;
    }

    try
    {
        Duco::RingingDatabase database;
        unsigned int versionNumber = 0;
        database.Internalise(filePath.string().c_str(), this, versionNumber);

        if (versionNumber > 0)
        {
            int wchars_num = MultiByteToWideChar(CP_UTF8, 0, towerbaseId.c_str(), -1, NULL, 0);
            wchar_t* wstr = new wchar_t[wchars_num];
            MultiByteToWideChar(CP_UTF8, 0, towerbaseId.c_str(), -1, wstr, wchars_num);
            Duco::ObjectId towerId = database.TowersDatabase().FindTowerByTowerbaseId(wstr);
            delete[] wstr;
            if (towerId.ValidId())
            {
                const Duco::Tower* const theTower = database.TowersDatabase().FindTower(towerId, true);
                Duco::StatisticFilters filters(database);
                filters.SetTower(true, towerId);

                std::wcout << towerId << "\t" << theTower->Name() << "\t" << theTower->City() << "\t";
                size_t numberOfPealsInThisTower = database.PerformanceInfo(filters).TotalPeals();
                std::wcout << numberOfPealsInThisTower << " peals found\t";
                numberOfPeals += numberOfPealsInThisTower;
                std::wcout << std::endl;
            }
            else
            {
                std::wcerr << "Tower not found" << std::endl;
            }
        }
        else
        {
            std::wcerr << "Database version incorrect" << std::endl;
        }
    }
    catch (std::exception exception)
    {
        std::wcerr << "Error reading file: " << fileNameToPrint << std::endl;
    }

    return numberOfPeals;
}

void
TowerSearcher::InitialisingImportExport(bool internalising, unsigned int versionNumber, size_t totalNumberOfObjects)
{

}

void
TowerSearcher::ObjectProcessed(bool internalising, Duco::TObjectType objectType, Duco::ObjectId objectId, int totalPercentage, size_t numberInThisSubDatabase)
{

}

void
TowerSearcher::ImportExportComplete(bool internalising)
{

}

void
TowerSearcher::ImportExportFailed(bool internalising, int errorCode)
{
    std::wcerr << "Error reading file: " << errorCode << std::endl;
}
