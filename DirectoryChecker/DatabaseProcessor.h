#pragma once
#include <filesystem>
#include <ImportExportProgressCallback.h>
#include <ProgressCallback.h>

namespace Duco
{
    class RingingDatabase;
}

class DatabaseProcessor: Duco::ImportExportProgressCallback, Duco::ProgressCallback
{
public: 
    DatabaseProcessor();
    ~DatabaseProcessor();

    size_t ProcessFile(const std::filesystem::path& filePath, bool exportXml = true);
    bool ExportFileToXml(const Duco::RingingDatabase& theDatase, const std::filesystem::path& filePath);

public: // from ImportExportProgressCallback
    virtual void InitialisingImportExport(bool internalising, unsigned int versionNumber, size_t totalNumberOfObjects);
    virtual void ObjectProcessed(bool internalising, Duco::TObjectType objectType, Duco::ObjectId objectId, int totalPercentage, size_t numberInThisSubDatabase);
    virtual void ImportExportComplete(bool internalising);
    virtual void ImportExportFailed(bool internalising, int errorCode);

public: // from ProgressCallback
    virtual void Initialised();
    virtual void Step(int progressPercent);
    virtual void Complete(bool error);

protected:
    size_t NoOfErrorsInDatabase(Duco::RingingDatabase& database) const;
    size_t NoOfPerfsWithoutBellBoardID(Duco::RingingDatabase& database);

private:
    bool currentDatabaseState;
};

