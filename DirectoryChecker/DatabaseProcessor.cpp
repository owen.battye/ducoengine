#include "DatabaseProcessor.h"
#include <iostream>
#include <RingingDatabase.h>
#include <DatabaseSettings.h>
#include <filesystem>
#include <PealLengthInfo.h>
#include <StatisticFilters.h>
#include <DatabaseSearch.h>
#include <SearchFieldStringArgument.h>

DatabaseProcessor::DatabaseProcessor()
: currentDatabaseState (true)
{

}

DatabaseProcessor::~DatabaseProcessor()
{

}

size_t
DatabaseProcessor::ProcessFile(const std::filesystem::path& filePath, bool exportXml)
{
    size_t numberOfPeals = 0;

    currentDatabaseState = true;
    size_t maxLength = 32;
    std::wstring fileNameToPrint = filePath.filename().wstring();
    fileNameToPrint = fileNameToPrint.substr(0, fileNameToPrint.length() - 4);
    size_t currentLength = fileNameToPrint.length() - 4;
    std::wcout << fileNameToPrint;
    currentLength += (currentLength % 8);
    std::wcout << "\t";
    while (currentLength < maxLength)
    {
        std::wcout << "\t";
        currentLength += 8;
    }

    try
    {
        Duco::RingingDatabase database;
        unsigned int versionNumber = 0;
        database.Internalise(filePath.string().c_str(), this, versionNumber);

        if (versionNumber > 0)
        {
            std::wcout << versionNumber << "\t";
            Duco::StatisticFilters filters(database);
            size_t numberOfPealsInThisDatabase = database.PerformanceInfo(filters).TotalPeals();
            std::wcout << numberOfPealsInThisDatabase << "\t";
            numberOfPeals += numberOfPealsInThisDatabase;
            std::wcout << database.NumberOfRingers() << "\t";
            std::wcout << database.NumberOfTowers() << "\t";
            std::wcout << database.NumberOfMethods() << "\t";
            std::wcout << database.NumberOfAssociations() << "\t";
            std::wcout << database.NumberOfPictures() << "\t";
            std::wcout << database.NumberOfCompositions() << "\t";
            std::wcout << NoOfPerfsWithoutBellBoardID(database) << "\t";
            std::wcout << NoOfErrorsInDatabase(database) << "\t";

            if (database.Settings().SettingsValid() &&
                database.Settings().PealbaseAssociationURL().compare(L"https://www.pealbase.co.uk/pealbase/guild.php?gid=") == 0)
            {
                std::wcout << "YES\t";
            }
            else
            {
                std::wcout << "NO\t";
            }

            if (database.Settings().SettingsAllDefault())
            {
                std::wcout << "YES\t";
            }
            else
            {
                std::wcout << "NO\t";
            }

            if (exportXml && ExportFileToXml(database, filePath))
            {
                std::wcout << "XML";
            }
            std::wcout  << std::endl;
        }
        else
        {
            currentDatabaseState = false;
        }
    }
    catch (std::exception exception)
    {
        currentDatabaseState = false;
    }

    if (!currentDatabaseState)
    {
        std::wcerr << "Could not read file" << std::endl;
    }
    return numberOfPeals;
}

size_t
DatabaseProcessor::NoOfPerfsWithoutBellBoardID(Duco::RingingDatabase& database)
{
    Duco::DatabaseSearch search(database);
    Duco::TSearchFieldStringArgument* arg = new Duco::TSearchFieldStringArgument(Duco::EBellBoardReference, L"", false, false);
    search.AddArgument(arg);

    std::set<Duco::ObjectId> foundIds;
    search.Search(*this, foundIds, Duco::TObjectType::EPeal);
        
    return foundIds.size();
}

size_t
DatabaseProcessor::NoOfErrorsInDatabase(Duco::RingingDatabase& database) const
{
    size_t numberOfErrors = database.NumberOfObjectsWithErrors(false, Duco::TObjectType::EPeal);
    numberOfErrors += database.NumberOfObjectsWithErrors(false, Duco::TObjectType::EAssociationOrSociety);
    numberOfErrors += database.NumberOfObjectsWithErrors(false, Duco::TObjectType::EComposition);
    numberOfErrors += database.NumberOfObjectsWithErrors(false, Duco::TObjectType::EMethod);
    numberOfErrors += database.NumberOfObjectsWithErrors(false, Duco::TObjectType::EMethodSeries);
    numberOfErrors += database.NumberOfObjectsWithErrors(false, Duco::TObjectType::EPicture);
    numberOfErrors += database.NumberOfObjectsWithErrors(false, Duco::TObjectType::ETower);

    return numberOfErrors;
}

bool
DatabaseProcessor::ExportFileToXml(const Duco::RingingDatabase& theDatase, const std::filesystem::path& filePath)
{
    std::filesystem::path newfileName = filePath;
    newfileName.replace_extension(".xml");
    if (!std::filesystem::exists(newfileName))
        return false;

    return theDatase.ExternaliseToXml(newfileName.string().c_str(), this, filePath.filename().string(), false);
}


void
DatabaseProcessor::InitialisingImportExport(bool internalising, unsigned int versionNumber, size_t totalNumberOfObjects)
{

}

void
DatabaseProcessor::ObjectProcessed(bool internalising, Duco::TObjectType objectType, Duco::ObjectId objectId, int totalPercentage, size_t numberInThisSubDatabase)
{

}

void 
DatabaseProcessor::ImportExportComplete(bool internalising)
{

}

void
DatabaseProcessor::ImportExportFailed(bool internalising, int errorCode)
{
    currentDatabaseState = false;
}


void
DatabaseProcessor::Initialised()
{

}

void
DatabaseProcessor::Step(int progressPercent)
{

}

void
DatabaseProcessor::Complete(bool error)
{

}