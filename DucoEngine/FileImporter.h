#ifndef __FILEIMPORTER_H__
#define __FILEIMPORTER_H__

#include <string>
#include <vector>
#include "DucoEngineCommon.h"

namespace Duco
{
    class RingingDatabase;
    class ProgressCallback;

class FileImporter
{
public:
    DllExport virtual ~FileImporter();
    DllExport bool Import(const std::string& fileName);
    DllExport virtual void Cancel() const;

protected:
    FileImporter(Duco::ProgressCallback& theCallback, Duco::RingingDatabase& theDatabase);

    float GetFileSize(const std::string& fileName) const;
    virtual void TokeniseLine(const std::wstring& fileLine, std::vector<std::wstring>& tokens, bool useComma, bool useTab, bool useAdditionalChar = false, wchar_t additionalChar = ' ') const;
    virtual bool EnforceQuotes() const = 0;
    virtual std::wifstream& GetLine(std::wifstream& inputFile, std::wstring& nextLine);

    virtual void ReadFiles(const std::string& fileName);
    virtual void ReadFile(const std::string& fileName);

    virtual bool ProcessPeal(const std::wstring& nextLine) = 0;
    virtual void GetTotalFileSize(const std::string& fileName) = 0;

protected:
    mutable bool                cancelled;
    Duco::RingingDatabase&      database;
    Duco::ProgressCallback&     callback;
    
    bool                        setLocale;
    float                       totalFileSize;
    float                       processedFileSize;
};

}

#endif //!__FILEIMPORTER_H__
