#include "SearchValidationGroup.h"

#include "Peal.h"
#include "Method.h"
#include <cassert>

using namespace Duco;

TSearchValidationGroup::TSearchValidationGroup(Duco::TSearchFieldId newFieldId)
: TSearchValidationArgument(newFieldId)
{
    assert(newFieldId == EAndGroup || newFieldId == EOrGroup);
}

TSearchValidationGroup::~TSearchValidationGroup()
{
    for (const auto& argIt : searchArguments)
    {
        delete argIt;
    }
    searchArguments.clear();
}

Duco::TFieldType
TSearchValidationGroup::FieldType() const
{
    return TFieldType::EGroupField;
}

size_t
TSearchValidationGroup::AddArgument(Duco::TSearchValidationArgument* newArgument)
{
    searchArguments.push_back(newArgument);

    return searchArguments.size();
}

std::set<Duco::ObjectId>
TSearchValidationGroup::ErrorIds() const
{
    std::set<Duco::ObjectId> errorIds;

    for (const auto& it : searchArguments)
    {
        std::set<Duco::ObjectId> errorIdsFromArgument = it->ErrorIds();
        errorIds.insert(errorIdsFromArgument.begin(), errorIdsFromArgument.end());
    }
    return errorIds;
}

void
TSearchValidationGroup::Reset()
{
    for (const auto& it : searchArguments)
    {
        it->Reset();
    }
}

bool
TSearchValidationGroup::Match(const Duco::Peal& peal, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const
{
    bool evaluatesToTrue (false);

    if (FieldId() == EAndGroup)
    {
        evaluatesToTrue = !searchArguments.empty();
        for (const auto& argIt : searchArguments)
        {
            evaluatesToTrue &= argIt->Match(peal, compareIds, database);
        }
    }
    else if (FieldId() == EOrGroup)
    {
        for (const auto& argIt : searchArguments)
        { // neeed to evaluate all arguments
            evaluatesToTrue |= argIt->Match(peal, compareIds, database);
        }
    }

    return evaluatesToTrue;
}

bool
TSearchValidationGroup::Match(const Duco::Tower& tower, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const
{
    bool evaluatesToTrue (false);

    if (FieldId() == EAndGroup)
    {
        evaluatesToTrue = !searchArguments.empty();
        for (const auto& argIt : searchArguments)
        {
            evaluatesToTrue &= argIt->Match(tower, compareIds, database);
        }
    }
    else if (FieldId() == EOrGroup)
    {
        for (const auto& argIt : searchArguments)
        {
            // neeed to evaluate all arguments
            evaluatesToTrue |= argIt->Match(tower, compareIds, database);
        }
    }

    return evaluatesToTrue;
}

bool
TSearchValidationGroup::Match(const Duco::Ringer& ringer, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const
{
    bool evaluatesToTrue (false);

    if (FieldId() == EAndGroup)
    {
        evaluatesToTrue = !searchArguments.empty();
        for (const auto& argIt : searchArguments)
        {
            evaluatesToTrue &= argIt->Match(ringer, compareIds, database);
        }
    }
    else if (FieldId() == EOrGroup)
    {
        for (const auto& argIt : searchArguments)
        {
            // need to evaluate all arguments
            evaluatesToTrue |= argIt->Match(ringer, compareIds, database);
        }
    }

    return evaluatesToTrue;
}

bool
TSearchValidationGroup::Match(const Duco::Method& method, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const
{
    bool evaluatesToTrue (false);

    if (FieldId() == EAndGroup)
    {
        evaluatesToTrue = !searchArguments.empty();
        for (const auto& argIt : searchArguments)
        {
            evaluatesToTrue &= argIt->Match(method, compareIds, database);
        }
    }
    else if (FieldId() == EOrGroup)
    {
        for (const auto& argIt : searchArguments)
        {
            // neeed to evaluate all arguments
            evaluatesToTrue |= argIt->Match(method, compareIds, database);
        }
    }

    return evaluatesToTrue;
}

bool
TSearchValidationGroup::Match(const Duco::MethodSeries& series, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const
{
    bool evaluatesToTrue (false);

    if (FieldId() == EAndGroup)
    {
        evaluatesToTrue = !searchArguments.empty();
        for (const auto& argIt : searchArguments)
        {
            evaluatesToTrue &= argIt->Match(series, compareIds, database);
        }
    }
    else if (FieldId() == EOrGroup)
    {
        for (const auto& argIt : searchArguments)
        {
            // neeed to evaluate all arguments
            evaluatesToTrue |= argIt->Match(series, compareIds, database);
        }
    }

    return evaluatesToTrue;
}

bool
TSearchValidationGroup::Match(const Duco::Composition& composition, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const
{
    bool evaluatesToTrue(false);

    if (FieldId() == EAndGroup)
    {
        evaluatesToTrue = !searchArguments.empty();
        for (const auto& argIt : searchArguments)
        {
            evaluatesToTrue &= argIt->Match(composition, compareIds, database);
        }
    }
    else if (FieldId() == EOrGroup)
    {
        for (const auto& argIt : searchArguments)
        {
            // need to evaluate all arguments
            evaluatesToTrue |= argIt->Match(composition, compareIds, database);
        }
    }

    return evaluatesToTrue;
}

bool
TSearchValidationGroup::Match(const Duco::Association& association, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const
{
    bool evaluatesToTrue(false);

    if (FieldId() == EAndGroup)
    {
        evaluatesToTrue = !searchArguments.empty();
        for (const auto& argIt : searchArguments)
        {
            evaluatesToTrue &= argIt->Match(association, compareIds, database);
        }
    }
    else if (FieldId() == EOrGroup)
    {
        for (const auto& argIt : searchArguments)
        {
            // need to evaluate all arguments
            evaluatesToTrue |= argIt->Match(association, compareIds, database);
        }
    }

    return evaluatesToTrue;
}

bool
TSearchValidationGroup::Match(const Duco::Picture& picture, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const
{
    bool evaluatesToTrue (false);

    if (FieldId() == EAndGroup)
    {
        evaluatesToTrue = !searchArguments.empty();
        for (const auto& argIt : searchArguments)
        {
            evaluatesToTrue &= argIt->Match(picture, compareIds, database);
        }
    }
    else if (FieldId() == EOrGroup)
    {
        for (const auto& argIt : searchArguments)
        {
            // neeed to evaluate all arguments
            evaluatesToTrue |= argIt->Match(picture, compareIds, database);
        }
    }

    return evaluatesToTrue;
}
