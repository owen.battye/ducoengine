#include "BellboardDownload.h"

#include "DatabaseSettings.h"

#define bellboardAcceptType "application/xml"

using namespace Duco;

BellboardDownload::BellboardDownload()
{

}


BellboardDownload::~BellboardDownload()
{
}


bool
BellboardDownload::DownloadPerformance(const Duco::DatabaseSettings& settings, const std::wstring& bellboardId)
{
    try
    {
        std::wstring url = settings.BellBoardDownloadUrl(bellboardId);
        std::string s(url.begin(), url.end());
        response = cpr::Get(cpr::Url{ s.c_str() },
            cpr::Header{ {"accept", bellboardAcceptType} });
    }
    catch (std::exception const&)
    {
        return false;
    }

    return response.status_code == 200;
}

std::string
BellboardDownload::Performance() const
{
    return response.text;
}