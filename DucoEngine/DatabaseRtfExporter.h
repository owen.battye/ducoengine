#ifndef __DATABASERTFEXPORTER_H__
#define __DATABASERTFEXPORTER_H__

#include "DatabaseWriter.h"
#include "MethodDatabase.h"
#include "Peal.h"

#include <set>

namespace Duco
{
class ImportExportProgressCallback;
class RingingDatabase;

class DatabaseRtfExporter
{
public:
    DllExport DatabaseRtfExporter(const Duco::RingingDatabase& theDatabase, std::set<Duco::ObjectId> pealIds);
    DllExport ~DatabaseRtfExporter();

    DllExport void Export(const char* fileName, Duco::ImportExportProgressCallback* newCallback);

private:
    const RingingDatabase&  database;
    std::set<Duco::ObjectId> pealIds;
};

} // end namespace Duco

#endif //!__DATABASERTFEXPORTER_H__
