#ifndef __DOVESEARCH_H__
#define __DOVESEARCH_H__

#include "DoveTowerValidator.h"
#include "DoveSearchCallback.h"
#include "DucoEngineCommon.h"
#include <set>
#include <string>

namespace Duco
{
    class RingingDatabase;
    class DoveDatabase;
    class ImportCancellationCallback;

class DoveSearch
:   public DoveTowerValidator
{
public:
    DllExport DoveSearch(const Duco::RingingDatabase& database, const std::string& filename, Duco::DoveSearchCallback* callback, const std::string& installationDir);
    DllExport ~DoveSearch();

    DllExport bool Search(const std::wstring& searchStr, unsigned int noOfBells = -1, bool atLeastStage = false, bool ignoreTowersRung = false, bool onlyIgnoreTowersWhereAllBellsRung = true, bool ukOnly = false, bool cathedralsOnly = false, bool ignoreUnringable = true, bool antiClockwise = false);

    DllExport bool FindTower(const std::wstring& doveId, std::set<Duco::DoveObject>& foundTowers) const;
    DllExport void RegisterCancellationCallBack(Duco::ImportCancellationCallback* newCallback);

    DllExport const Duco::DoveDatabase& DoveDatabase() const;

public: // from DoveTowerValidator
    virtual bool IncludeTower(const Duco::DoveObject& nextTower);

protected:
    Duco::DoveDatabase*                 doveDatabase;

    const Duco::RingingDatabase&        database;
    const std::string                   doveFilename;
    const std::string                   ducoInstallationDir;
    Duco::DoveSearchCallback* const     callback;
    Duco::ImportCancellationCallback*   cancellationCheck;

    std::wstring                        searchString;
    unsigned int                        noOfBells;
    bool                                atLeastStage;
    bool                                ignoreTowersRung;
    bool                                onlyIgnoreTowersWhereAllBellsRung;
    bool                                ukOnly;
    bool                                cathedralsOnly;
    bool                                ignoreUnringable;
    bool                                antiClockwiseOnly;
};

}

#endif //!__DOVESEARCH_H__
