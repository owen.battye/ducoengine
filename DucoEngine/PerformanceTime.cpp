#include "PerformanceTime.h"
#include "DucoEngineCommon.h"
#include <sstream>
#include <algorithm>

using namespace Duco;
using namespace std;

PerformanceTime::PerformanceTime()
:   pealTimeInMinutes(0)
{
}

PerformanceTime::PerformanceTime(unsigned int newPealTimeInMinutes)
 :  pealTimeInMinutes(newPealTimeInMinutes)
{

}

PerformanceTime::PerformanceTime(const PerformanceTime& other)
 :  pealTimeInMinutes(other.pealTimeInMinutes)
{

}

PerformanceTime::PerformanceTime(const std::wstring& timeStringStr)
{
    unsigned int hours(0);
    unsigned int minutes(0);

    unsigned int nextUnits(0);
    std::wstring::const_iterator it = timeStringStr.begin();
    while (it != timeStringStr.end())
    {
        switch (*it)
        {
        case L'0':
        case L'1':
        case L'2':
        case L'3':
        case L'4':
        case L'5':
        case L'6':
        case L'7':
        case L'8':
        case L'9':
        {
            nextUnits *= 10;
            std::wstring temp;
            temp += *it;
            nextUnits += _wtoi(temp.c_str());
        }
        break;


        case L'h':
        case L'H':
        case L':':
            hours = nextUnits;
            nextUnits = 0;
            break;

        case L'm':
        case L'M':
            minutes = nextUnits;
            nextUnits = 0;
            break;

        default:
            break;
        }
        ++it;
    }
    if (nextUnits != 0 && minutes == 0 && hours > 0)
    {
        minutes = nextUnits;
    }
    else if (nextUnits > 12 && minutes == 0 && hours == 0)
    {
        minutes = nextUnits;
    }


    pealTimeInMinutes = (hours * 60) + minutes;
}


PerformanceTime::~PerformanceTime()
{

}

void
PerformanceTime::Clear()
{
    pealTimeInMinutes = 0;
}

std::wstring
PerformanceTime::TimeForAssociationReports() const
{
    wchar_t pealTimeBuf[80];
    unsigned int minutes = pealTimeInMinutes % 60;
    unsigned int hours = (pealTimeInMinutes - minutes) / 60;
    if (minutes == 0)
    {
        swprintf(pealTimeBuf, 80, L"%dhrs", hours);
    }
    else
    {
        swprintf(pealTimeBuf, 80, L"%dhrs %dmins", hours, minutes);
    }

    std::wstring fullDateFormat;
    fullDateFormat += pealTimeBuf;
    return fullDateFormat;
}

std::wstring
PerformanceTime::PrintPealTime(bool longFormat) const
{
    unsigned int minutes = pealTimeInMinutes % 60;
    unsigned int hours = (pealTimeInMinutes - minutes) / 60;

    wostringstream printedTime;
    if (hours == 0 && minutes == 0)
    {
        printedTime << "0:00";
    }
    else
    {
        if (hours > 23)
        {
            unsigned int remainingHours = hours % 24;
            unsigned int days = (hours - remainingHours) / 24;
            if (days > 365)
            {
                unsigned int years = days / 365;
                days = days - (years * 365);
                printedTime << years << "y";
                if (longFormat)
                {
                    printedTime << "ear";
                    if (years > 1)
                    {
                        printedTime << "s";
                    }
                }
                printedTime << " ";
            }
            hours = remainingHours;
            if (days != 0)
            {
                printedTime << days << "d";
                if (longFormat)
                {
                    printedTime << "ay";
                    if (days > 1)
                    {
                        printedTime << "s";
                    }
                }
                printedTime << " ";
            }
        }
        if (minutes == 0)
        {
            printedTime << hours;
            if (longFormat)
            {
                printedTime << "hour";
                if (hours != 1)
                    printedTime << "s";
            }
            else
            {
                printedTime << "h 00m";
            }
        }
        else
        {
            if (hours > 0 || !longFormat)
            {
                printedTime << hours;
                if (longFormat)
                {
                    printedTime << "hour";
                    if (hours != 1)
                        printedTime << "s";
                }
                else
                    printedTime << "h";
                printedTime << " ";
            }
            if (minutes < 10)
                printedTime << "0";
            printedTime << minutes;
            if (longFormat)
            {
                printedTime << "minute";
                if (minutes > 1)
                    printedTime << "s";
            }
            else
                printedTime << "m";
        }
    }
    return printedTime.str();
}

std::wstring
PerformanceTime::PrintShortPealTime() const
{
    unsigned int minutes = pealTimeInMinutes % 60;
    unsigned int hours = (pealTimeInMinutes - minutes) / 60;
    bool printedDays = false;

    wostringstream printedTime;
    if (hours == 0 && minutes == 0)
    {
        printedTime << "0:00";
    }
    else
    {
        if (hours > 23)
        {
            unsigned int remainingHours = hours % 24;
            unsigned int days = (hours - remainingHours) / 24;
            if (days > 365)
            {
                unsigned int years = days / 365;
                days = days - (years * 365);
                printedTime << years << ":";
            }
            hours = remainingHours;
            printedTime << days << ":";
            printedDays = true;
        }
        if (minutes == 0)
        {
            printedTime << hours;
            printedTime << ":00";
        }
        else
        {
            if (hours > 0)
            {
                if (hours < 10 && printedDays)
                {
                    printedTime << "0";
                }
                printedTime << hours;
            }
            else
            {
                printedTime << "00";
            }
            printedTime << ":";
            if (minutes < 10)
                printedTime << "0";
            printedTime << minutes;
        }
    }

    return printedTime.str();
}


std::wostream&
PerformanceTime::PrintTime(wostream& stream) const
{
    stream << PrintPealTime(false);

    return stream;
}

bool
PerformanceTime::operator>(const PerformanceTime & lhs) const
{
    if (pealTimeInMinutes > lhs.pealTimeInMinutes)
        return true;
    
    return false;
}

bool
PerformanceTime::operator<(const PerformanceTime & lhs) const
{
    if (pealTimeInMinutes < lhs.pealTimeInMinutes)
         return true;
    
    return false;
}

bool
PerformanceTime::operator==(const PerformanceTime& lhs) const
{
    if (pealTimeInMinutes == lhs.pealTimeInMinutes)
        return true;
    return false;
}

bool
PerformanceTime::operator!=(const PerformanceTime& lhs) const
{
    if (pealTimeInMinutes != lhs.pealTimeInMinutes)
        return true;
    return false;
}

bool
PerformanceTime::PealTimeClose(const PerformanceTime& rhs) const
{
    if (abs((int)rhs.pealTimeInMinutes - (int)pealTimeInMinutes) <= 1)
    {
        return true;
    }

    std::wstring lhsPealTimeString = PrintPealTime(false);
    std::wstring rhsPealTimeString = rhs.PrintPealTime(false);

    size_t shortestStringLength = std::min(rhsPealTimeString.length(), lhsPealTimeString.length());
    size_t charsMatch = 0;
    for (unsigned int i = 0; i < rhsPealTimeString.length() && i < lhsPealTimeString.length(); ++i)
    {
        if (rhsPealTimeString[i] == lhsPealTimeString[i])
        {
            ++charsMatch;
        }
    }

    return charsMatch >= (shortestStringLength - 1);
}

bool
PerformanceTime::operator>=(const PerformanceTime& lhs) const
{
    return this->pealTimeInMinutes == lhs.pealTimeInMinutes || this->pealTimeInMinutes > lhs.pealTimeInMinutes;
}

bool
PerformanceTime::operator<=(const PerformanceTime& lhs) const
{
    return this->pealTimeInMinutes == lhs.pealTimeInMinutes || this->pealTimeInMinutes < lhs.pealTimeInMinutes;
}

unsigned int
PerformanceTime::operator-(const Duco::PerformanceTime& lhs) const
{
    return std::abs((int)pealTimeInMinutes - (int)lhs.pealTimeInMinutes);
}

bool
PerformanceTime::Valid() const
{
    return pealTimeInMinutes > 0;
}

float
PerformanceTime::ChangesPerMinute(unsigned int noOfChanges) const
{
    if (noOfChanges == 0)
        return 0;
    if (pealTimeInMinutes == 0)
        return 0;

    return (float)noOfChanges / (float)pealTimeInMinutes;
}
