#include "SearchFieldNegativeArgument.h"

using namespace Duco;

TSearchFieldNegativeArgument::TSearchFieldNegativeArgument(TSearchArgument* newArgument)
:   TSearchArgument(ENot), theArgument(newArgument)
{

}

TSearchFieldNegativeArgument::~TSearchFieldNegativeArgument()
{
    delete theArgument;
}

bool
TSearchFieldNegativeArgument::Match(const Duco::Method& object, const Duco::RingingDatabase& database) const
{
    return !theArgument->Match(object, database);
}

bool
TSearchFieldNegativeArgument::Match(const Duco::Peal& object, const Duco::RingingDatabase& database) const
{
    return !theArgument->Match(object, database);
}

bool
TSearchFieldNegativeArgument::Match(const Duco::Ringer& object, const Duco::RingingDatabase& database) const
{
    return !theArgument->Match(object, database);
}

bool
TSearchFieldNegativeArgument::Match(const Duco::Tower& object, const Duco::RingingDatabase& database) const
{
    return !theArgument->Match(object, database);
}

bool
TSearchFieldNegativeArgument::Match(const Duco::MethodSeries& object, const Duco::RingingDatabase& database) const
{
    return !theArgument->Match(object, database);
}

bool
TSearchFieldNegativeArgument::Match(const Duco::Composition& object, const Duco::RingingDatabase& database) const
{
    return !theArgument->Match(object, database);
}

bool
TSearchFieldNegativeArgument::Match(const Duco::Association& object, const Duco::RingingDatabase& database) const
{
    return !theArgument->Match(object, database);
}

bool
TSearchFieldNegativeArgument::Match(const Duco::Picture& object, const Duco::RingingDatabase& database) const
{
    return !theArgument->Match(object, database);
}

Duco::TFieldType
TSearchFieldNegativeArgument::FieldType() const
{
    return theArgument->FieldType();
}
