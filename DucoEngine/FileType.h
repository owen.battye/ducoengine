#ifndef __FILETYPE_H__
#define __FILETYPE_H__

namespace Duco
{
    enum TFileType
    {
        EUnknownFileType,
        ENoDatabaseDrivers,
        EWinRkVersion1,
        EWinRkVersion2,
        EWinRkVersion3,
        EPealBasePBR,
        EPealBaseTSV,
        ECsv
    };
}

#endif //!__FILETYPE_H__
