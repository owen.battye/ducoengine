#include "PealDatabase.h"

#include "AlphabetData.h"
#include "Association.h"
#include "AssociationDatabase.h"
#include "BellboardPerformanceParser.h"
#include "CompositionDatabase.h"
#include "DatabaseSettings.h"
#include "DucoEngineUtils.h"
#include "DucoEngineLog.h"
#include "LeadingBellData.h"
#include "ProgressCallback.h"
#include "Method.h"
#include "MethodDatabase.h"
#include "MethodSeries.h"
#include "MethodSeriesDatabase.h"
#include "Peal.h"
#include "ImportExportProgressCallback.h"
#include "RenumberProgressCallback.h"
#include "Ring.h"
#include "Ringer.h"
#include "RingerCirclingData.h"
#include "RingerDatabase.h"
#include "RingingDatabase.h"
#include "Tower.h"
#include "TowerDatabase.h"
#include "SearchArgument.h"
#include "SearchValidationArgument.h"
#include "StatisticFilters.h"
#include "PealLengthInfo.h"
#include "UpdateArgument.h"
#include "YearId.h"
#include "YearIdGroup.h"
#include "DucoXmlUtils.h"

#include <limits>
#include <algorithm>
#include <fstream>
#include <future>

#include <xercesc\dom\DOMElement.hpp>
XERCES_CPP_NAMESPACE_USE

using namespace Duco;
using namespace std;

PealDatabase::PealDatabase(Duco::RingerDatabase& theRingersDatabase, Duco::MethodDatabase& theMethodsDatabase, Duco::TowerDatabase& theTowersDatabase, Duco::MethodSeriesDatabase& theSeriesDatabase, Duco::CompositionDatabase& theCompositionsDatabase, Duco::AssociationDatabase& theAssociationsDatabase, Duco::DatabaseSettings& theSettings)
:   RingingObjectDatabase(TObjectType::EPeal), ringersDatabase(theRingersDatabase), methodsDatabase(theMethodsDatabase), towersDatabase(theTowersDatabase), seriesDatabase(theSeriesDatabase), compositionsDatabase(theCompositionsDatabase), associationsDatabase(theAssociationsDatabase), settings(theSettings)
{
}

PealDatabase::~PealDatabase()
{

}

ObjectId
PealDatabase::AddObject(const Duco::RingingObject& newObject)
{
    ObjectId newObjectId;

    if (newObject.ObjectType() == TObjectType::EPeal)
    {
        const Duco::Peal& newPeal = static_cast<const Duco::Peal&>(newObject);

        Duco::RingingObject* newPealWithId = new Duco::Peal(newPeal);
        if (AddOwnedObject(newPealWithId))
        {
            newObjectId = newPealWithId->Id();
        }
        // object is deleted by RingingObjectDatabase::AddObject if not added.
    }
    return newObjectId;
}

bool
PealDatabase::SaveUpdatedObject(Duco::RingingObject& lhs, const Duco::RingingObject& rhs)
{
    if (lhs.ObjectType() == rhs.ObjectType() && rhs.ObjectType() == TObjectType::EPeal)
    {
        Duco::Peal& lhsObject = static_cast<Duco::Peal&>(lhs);
        const Duco::Peal& rhsObject = static_cast<const Duco::Peal&>(rhs);
        lhsObject = rhsObject;
        return true;
    }
    return false;
}

bool
PealDatabase::Internalise(DatabaseReader& reader, Duco::ImportExportProgressCallback* callback, int databaseVersionNumber, size_t noOfObjectsInThisDatabase, size_t totalObjects, size_t objectsProcessedSoFar)
{
    size_t totalProcessed = objectsProcessedSoFar;
    size_t count = 0;

    while (noOfObjectsInThisDatabase-- > 0)
    {
        ++totalProcessed;
        ++count;
        float percentageComplete = ((float)totalProcessed / (float)totalObjects) * 100;
        Duco::Peal* newPeal = new Duco::Peal(reader, databaseVersionNumber);
        if (newPeal != NULL && newPeal->Id().ValidId())
        {
            if (newPeal->OldAssociation().length() > 0 && !newPeal->AssociationId().ValidId())
            {
                Duco::ObjectId newAssociationId = associationsDatabase.SuggestAssociationOrCreate(newPeal->OldAssociation(), true);
                newPeal->SetAssociationIdAndClearOldAssociation(newAssociationId);
            }
            Duco::RingingObject* newPealBaseClass = newPeal;
            if (AddOwnedObject(newPealBaseClass))
            {
                if (callback != NULL)
                {
                    callback->ObjectProcessed(true, TObjectType::EPeal, newPeal->Id(), (int)percentageComplete, count);
                }
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    return true;
}

bool
PealDatabase::ReplaceRinger(const ObjectId& oldRingerId, const ObjectId& newRingerId, bool deleteOldRinger, const Duco::RingingDatabase& database)
{
    bool ringerReplaced (false);

    for (auto const& [key, value] : listOfObjects)
    {
        Duco::Peal* thePeal = static_cast<Duco::Peal*>(value);
        if (thePeal->ReplaceRingerId(oldRingerId, newRingerId))
        {
            SetDataChanged();
            ringerReplaced = true;
        }
    }

    Duco::StatisticFilters filters(database);
    filters.SetRinger(true, oldRingerId);
    if (ringerReplaced && deleteOldRinger && PerformanceInfo(filters).TotalPeals() == 0)
    {
        return ringersDatabase.DeleteObject(oldRingerId);
    }
    return ringerReplaced;
}

bool
PealDatabase::ReplaceTower(const ObjectId& oldTowerId, const ObjectId& newTowerId)
{
    std::map<Duco::ObjectId, Duco::ObjectId> otherRingIds;
    if (!towersDatabase.RingTranslations(oldTowerId, newTowerId, otherRingIds))
        return false;

    bool towerReplaced(false);
    for (auto const& [key, value] : listOfObjects)
    {
        Duco::Peal* thePeal = static_cast<Duco::Peal*>(value);
        if (thePeal->ReplaceTowerId(oldTowerId, newTowerId, otherRingIds))
        {
            SetDataChanged();
            towerReplaced = true;
        }
    }

    return towerReplaced;
}

void
PealDatabase::RingersAroundDates(const Duco::StatisticFilters& _filters, std::set<Duco::ObjectId>& ringerIds) const
{
    ringerIds.clear();
    Duco::StatisticFilters filters(_filters);

    if (!filters.StartDate().Valid())
    {
        PerformanceDate sinceDate;
        sinceDate.RemoveMonths(5 * 12); // Last 5 years
        filters.SetStartDate(true, sinceDate);
    }

    for (auto const& [key, value] : listOfObjects)
    {
        const Duco::Peal* thePeal = static_cast<const Duco::Peal*>(value);
        if (filters.Match(*thePeal))
        {
            std::set<ObjectId> currentPealRingerIds;
            thePeal->RingerIds(currentPealRingerIds);
            ringerIds.insert(currentPealRingerIds.begin(), currentPealRingerIds.end());
        }
    }
}

void
PealDatabase::GetTopRingers(const Duco::StatisticFilters& filters, size_t noOfRingers, bool combineLinked, std::multimap<Duco::PealLengthInfo, ObjectId>& sortedRingerIds) const
{
    std::map<ObjectId, Duco::PealLengthInfo> allRingerIds;
    GetRingersPealCount(filters, combineLinked, allRingerIds);
    KeepTop(noOfRingers, sortedRingerIds, allRingerIds, true);
}

void
PealDatabase::KeepTop(size_t noOfObjects, std::multimap<Duco::PealLengthInfo, ObjectId>& sortedRingerIds, const std::map<ObjectId, Duco::PealLengthInfo>& allRingerIds, bool ringers) const
{
    sortedRingerIds.clear();
    for (auto const& [key, value] : allRingerIds)
    {
        if (!ringers || key != settings.DefaultRinger())
        {
            std::pair<Duco::PealLengthInfo, ObjectId> newObject(value, key);
            sortedRingerIds.insert(newObject);

            if (sortedRingerIds.size() > noOfObjects)
            {
                sortedRingerIds.erase(sortedRingerIds.begin());
            }
        }
    }
}

void
PealDatabase::GetRingersPealCount(const Duco::StatisticFilters& filters, bool combineLinked, std::map<ObjectId, Duco::PealLengthInfo>& sortedRingerIds) const
{
    sortedRingerIds.clear();

    for (auto const& [key, value] : listOfObjects)
    {
        const Duco::Peal* thePeal = static_cast<const Duco::Peal*>(value);
        if (filters.Match(*thePeal))
        {
            thePeal->AddPealCount(sortedRingerIds);
        }
    }

    ringersDatabase.AddMissingRingers(sortedRingerIds);

    if (!settings.DefaultRingerSet())
    {
        size_t totalNumberOfPeals (listOfObjects.size());
        std::map<ObjectId, Duco::PealLengthInfo>::const_iterator it = sortedRingerIds.begin();
        while (it != sortedRingerIds.end() && !settings.DefaultRingerSet())
        {
            if (it->second.TotalPeals() == totalNumberOfPeals)
            {
                settings.SetDefaultRinger(it->first);
            }
            ++it;
        }
    }
    if (combineLinked)
    {
        ringersDatabase.CombineLinkedRingers(sortedRingerIds);
    }
}

void
PealDatabase::GetAllConductorIds(std::set<ObjectId>& sortedRingerIds) const
{
    sortedRingerIds.clear();
    for (auto const& [key, value] : listOfObjects)
    {
        const Duco::Peal* thePeal = static_cast<const Duco::Peal*>(value);
        if (!thePeal->Withdrawn())
        {
            std::set<ObjectId>::const_iterator condIt = thePeal->Conductors().begin();
            while (condIt != thePeal->Conductors().end())
            {
                sortedRingerIds.insert(*condIt);
                ++condIt;
            }
        }
    }
}

bool
PealDatabase::AnyPealContainingBothRingers(const Duco::ObjectId& ringerId1, const Duco::ObjectId& ringerId2, bool includeLinked) const
{
    if (!ringerId1.ValidId() || !ringerId2.ValidId())
    {
        return false;
    }

    for (const auto& [key, value] : listOfObjects)
    {
        const Duco::Peal* thePeal = static_cast<const Duco::Peal*>(value);
        bool asStrapper (false);
        if (thePeal->ContainsRinger(ringerId1, asStrapper) && thePeal->ContainsRinger(ringerId2, asStrapper))
        {
            return true;
        }
        if (includeLinked)
        {
            std::set<Duco::ObjectId> ringer1links;
            std::set<Duco::ObjectId> ringer2links;
            ringersDatabase.LinkedRingers(ringerId1, ringer1links, true);
            ringersDatabase.LinkedRingers(ringerId2, ringer2links, true);

            if (thePeal->ContainsAnyRinger(ringer1links) && thePeal->ContainsAnyRinger(ringer2links))
            {
                return true;
            }
        }
    }
    return false;
}

void
PealDatabase::GetTopTowers(size_t noOfTowers, std::list<Duco::ObjectId>& topFiveTowerIds, const Duco::RingingDatabase& database) const
{
    topFiveTowerIds.clear();
    std::map<Duco::ObjectId, Duco::PealLengthInfo> allTowerIds;
    StatisticFilters filters(database);
    GetTowersPealCount(filters, allTowerIds);

    std::multimap<Duco::PealLengthInfo, Duco::ObjectId> topTowerIds;
    KeepTop(noOfTowers, topTowerIds, allTowerIds);

    std::multimap<Duco::PealLengthInfo, Duco::ObjectId>::const_iterator it = topTowerIds.begin();
    while (it != topTowerIds.end())
    {
        topFiveTowerIds.push_back(it->second);
        ++it;
    }
}

void
PealDatabase::GetTopTowers(const Duco::StatisticFilters& filters, size_t noOfTowers, std::multimap<Duco::PealLengthInfo, Duco::ObjectId>& topFiveTowerIds) const
{
    topFiveTowerIds.clear();
    std::map<Duco::ObjectId, Duco::PealLengthInfo> allTowerIds;
    GetTowersPealCount(filters, allTowerIds);
    KeepTop(noOfTowers, topFiveTowerIds, allTowerIds);
}

void
PealDatabase::GetTopTower(Duco::ObjectId& topTowerId, Duco::PealLengthInfo& noOfPeals, const Duco::RingingDatabase& database) const
{
    noOfPeals.Clear();
    topTowerId.ClearId();
    std::map<Duco::ObjectId, Duco::PealLengthInfo> allTowerIds;
    StatisticFilters filters (database);
    GetTowersPealCount(filters, allTowerIds);

    std::multimap<Duco::PealLengthInfo, Duco::ObjectId> topTowerIds;
    KeepTop(1, topTowerIds, allTowerIds);
    
    std::multimap<Duco::PealLengthInfo, Duco::ObjectId>::const_iterator it = topTowerIds.begin();
    if (it != topTowerIds.end())
    {
        noOfPeals = it->first;
        topTowerId = it->second;
    }
}

void
PealDatabase::GetTowersPealCount(const Duco::StatisticFilters& filters, std::map<ObjectId, Duco::PealLengthInfo>& sortedTowerIds, bool combineLinkedTowers) const
{
    sortedTowerIds.clear();
    for (auto const& [key, value] : listOfObjects)
    {
        const Duco::Peal* thePeal = static_cast<const Duco::Peal*>(value);
        if (filters.Match(*thePeal))
        {
            Duco::PealLengthInfo newInfo(*thePeal);
            pair<ObjectId, Duco::PealLengthInfo> newObject(thePeal->TowerId(), newInfo);
            pair<std::map<ObjectId, Duco::PealLengthInfo>::iterator, bool> insertedIt = sortedTowerIds.insert(newObject);
            if (!insertedIt.second)
            {
                insertedIt.first->second += newInfo;
            }
        }
    }

    if (combineLinkedTowers && sortedTowerIds.size() > 0)
    {
        towersDatabase.CombineLinkedTowerData(sortedTowerIds);
    }
    if (!combineLinkedTowers)
    {
        towersDatabase.AddMissingTowers(sortedTowerIds, filters);
    }
}

void
PealDatabase::GetTowerCircling(const StatisticFilters& filters,
                               std::map<unsigned int, Duco::CirclingData>& sortedBellPealCounts, Duco::PealLengthInfo& noOfPeals,
                               PerformanceDate& firstCircledDate, size_t& noOfDaysToFirstCircle) const
{
    noOfPeals.Clear();
    firstCircledDate.Clear();
    sortedBellPealCounts.clear();
    noOfDaysToFirstCircle = -1;

    bool error(false);

    Duco::ObjectId towerId;
    Duco::ObjectId ringIdInFilters;

    if (!filters.Tower(towerId))
    {
        DUCOENGINELOG("Error - Tower circling - No tower specified");
        return;
    }
    const Tower* const theTowerFromFilters = towersDatabase.FindTower(towerId, true);

    Duco::PerformanceDate firstPealDate;

    DUCO_OBJECTS_CONTAINER::const_iterator pealIt = listOfObjects.begin();
    while (pealIt != listOfObjects.end() && !error)
    {
        const Duco::Peal* thisPeal = static_cast<const Duco::Peal*>(pealIt->second);
        if (!thisPeal->Handbell() && filters.Match(*thisPeal))
        {
            noOfPeals.AddPeal(*thisPeal);
            std::set<unsigned int> bellsRung;
            bool asStrapper(false);
            if (!error && thisPeal->BellRung(filters.RingerId(), bellsRung, asStrapper))
            {
                std::set<unsigned int>::const_iterator bellsRungIt = bellsRung.begin();
                while (bellsRungIt != bellsRung.end() && !error)
                {
                    unsigned int defaultRingerBellNumber(*bellsRungIt);
                    if (!filters.Ring(ringIdInFilters))
                    {
                        const Ring* theRing = theTowerFromFilters->FindRing(thisPeal->RingId());
                        if (thisPeal->TowerId() != towerId)
                        {
                            const Tower* const theTowerFromPeal = towersDatabase.FindTower(thisPeal->TowerId(), true);
                            if (theTowerFromPeal == NULL)
                            {
                                error = true;
                            }
                            theRing = theTowerFromPeal->FindRing(thisPeal->RingId());
                        }
                        if (theRing == NULL || !theRing->RealBellNumber(defaultRingerBellNumber))
                        {
                            DUCOENGINELOG("Error getting ring for tower to workout tower circling");
                            error = true;
                        }
                    }
                    if (!error)
                    {
                        if (!firstPealDate.Valid() || firstPealDate > thisPeal->Date())
                        {
                            firstPealDate = thisPeal->Date();
                        }
                        Duco::CirclingData newCirclingData(1, thisPeal->Date());
                        pair<unsigned int, Duco::CirclingData> newObject(defaultRingerBellNumber, newCirclingData);
                        pair<std::map<unsigned int, Duco::CirclingData>::iterator, bool> insertedIt = sortedBellPealCounts.insert(newObject);
                        if (!insertedIt.second)
                        {
                            insertedIt.first->second.Increment(thisPeal->Date());
                        }
                        ++bellsRungIt;
                    }
                }
            }
        }
        ++pealIt;
    }
    if (error)
    {
        noOfPeals.Clear();
        firstCircledDate.Clear();
        sortedBellPealCounts.clear();
    }
    else if (CheckForFirstCircleDate(sortedBellPealCounts, firstCircledDate, theTowerFromFilters->NoOfBellsInRing(ringIdInFilters)))
    {
        noOfDaysToFirstCircle = firstPealDate.NumberOfDaysUntil(firstCircledDate);
    }
}

void 
PealDatabase::GetLeadingBells(const Duco::ObjectId& ringerId, std::set<Duco::LeadingBellData>& sortedLeadingBellPealCounts) const
{
    sortedLeadingBellPealCounts.clear();
    std::map<Duco::ObjectId, std::map<unsigned int,unsigned int>> sortedBellPealCounts; // towerid, bell peal counts<real bell no, count>
    size_t highestNumberOfPeals = 0;

    for (auto const& [key, value] : listOfObjects)
    {
        const Duco::Peal* thisPeal = static_cast<const Duco::Peal*>(value);
        bool strapper = false;
        if (!thisPeal->Handbell() && thisPeal->ContainsRinger(ringerId, strapper))
        {
            const Tower* const theTower = towersDatabase.FindTower(thisPeal->TowerId(), false);
            if (theTower != NULL)
            {
                const Ring* const theRing = theTower->FindRing(thisPeal->RingId());
                if (theRing != NULL)
                {
                    std::set<unsigned int> bellsRung;
                    bool asStrapper(false);
                    if (thisPeal->BellRung(ringerId, bellsRung, asStrapper))
                    {
                        std::set<unsigned int>::const_iterator bellsRungIt = bellsRung.begin();
                        while (bellsRungIt != bellsRung.end())
                        {
                            unsigned int defaultRingerBellNumber(*bellsRungIt);
                            if (theRing->RealBellNumber(defaultRingerBellNumber))
                            {
                                std::pair<unsigned int, unsigned int> bellPealCount(defaultRingerBellNumber, 1);
                                std::map<unsigned int, unsigned int> bellPealCounts;
                                bellPealCounts.insert(bellPealCount);
                                Duco::ObjectId towerId(theTower->Id());
                                std::pair<Duco::ObjectId, std::map<unsigned int, unsigned int>> newObject(towerId, bellPealCounts);
                                std::pair<std::map<Duco::ObjectId, std::map<unsigned int, unsigned int>>::iterator, bool> towerInsertedIt = sortedBellPealCounts.insert(newObject);
                                if (towerInsertedIt.second)
                                {
                                    highestNumberOfPeals = std::max<size_t>(highestNumberOfPeals, 1);
                                }
                                else
                                {
                                    // tower already exists
                                    std::pair<std::map<unsigned int, unsigned int>::iterator, bool> bellInsertedIt = towerInsertedIt.first->second.insert(bellPealCount);
                                    if (!bellInsertedIt.second)
                                    {
                                        // bell already exists
                                        bellInsertedIt.first->second += 1;
                                        highestNumberOfPeals = std::max<size_t>(highestNumberOfPeals, bellInsertedIt.first->second);
                                    }
                                }
                            }
                            ++bellsRungIt;
                        }
                    }
                }
            }
        }
    }
    // now we have every tower and every bell counts, convert this to a sorted list of peal counts per bell.
    highestNumberOfPeals /= 10;
    std::map<Duco::ObjectId, std::map<unsigned int, unsigned int>>::const_iterator conversionIt = sortedBellPealCounts.begin();
    while (conversionIt != sortedBellPealCounts.end())
    {
        std::map<unsigned int, unsigned int>::const_iterator bellConversionIt = conversionIt->second.begin();
        while (bellConversionIt != conversionIt->second.end())
        {
            if (bellConversionIt->second > highestNumberOfPeals && (highestNumberOfPeals == 0 || bellConversionIt->second > 1))
            {
                Duco::LeadingBellData nextBell(conversionIt->first, bellConversionIt->first, bellConversionIt->second);
                sortedLeadingBellPealCounts.insert(nextBell);
            }
            ++bellConversionIt;
        }
        ++conversionIt;
    }
}

void
PealDatabase::GetLeadingRingersTowerCircling(const Duco::ObjectId& towerId, std::multimap<Duco::PerformanceDate, RingerCirclingData*>& topCircledRingers, size_t noOfRingers, bool highestStage) const
{
    std::map<Duco::ObjectId, Duco::RingerCirclingData*> towerCircling;
    const Duco::Tower* theTower = towersDatabase.FindTower(towerId);

    for (auto const& [key, value] : listOfObjects)
    {
        const Duco::Peal* thisPeal = static_cast<const Duco::Peal*>(value);
        if (theTower->Id() == thisPeal->TowerId())
        {
            bool stageMatch = !highestStage;
            if (highestStage)
            {
                const Duco::Method* const theMethod = methodsDatabase.FindMethod(thisPeal->MethodId());
                if (theMethod != NULL)
                {
                    stageMatch = theTower->Bells() == theMethod->Order();
                }
            }

            if (stageMatch)
            {
                const Ring* const theRing = theTower->FindRing(thisPeal->RingId());
                if (theRing != NULL)
                {
                    thisPeal->AddRingersToCirclingData(towerCircling, *theRing);
                }
            }
        }
    }

    std::multimap<Duco::PerformanceDate, RingerCirclingData*> circledRingers;

    std::map<Duco::ObjectId, Duco::RingerCirclingData*>::iterator it = towerCircling.begin();
    while (it != towerCircling.end())
    {
        Duco::PerformanceDate circlingDate;
        if (it->second->FirstCircleDate(circlingDate, theTower->Bells()))
        {
            std::pair<Duco::PerformanceDate, RingerCirclingData*> pair(circlingDate, it->second);
            circledRingers.insert(pair);
        }
        else
        {
            delete it->second;
        }
        ++it;
    }

    topCircledRingers.clear();
    std::multimap<Duco::PerformanceDate, Duco::RingerCirclingData*>::iterator it2 = circledRingers.begin();

    while (it2 != circledRingers.end())
    {
        if (noOfRingers > 0)
        {
            topCircledRingers.insert(std::pair<Duco::PerformanceDate, Duco::RingerCirclingData*>(it2->first, it2->second));
            --noOfRingers;
        }
        else
        {
            delete it2->second;
        }
        ++it2;
    }
}

void
PealDatabase::GetRingersTowerCircling(const Duco::StatisticFilters& _filters, const Duco::ObjectId& lastPealId, const std::set<Duco::ObjectId>& ringerIds, std::map<Duco::ObjectId, std::map<unsigned int, Duco::PealLengthInfo>>& pealCountsOnAllBellsByRingerId) const
{
    // Bell to peal count
    //std::map<unsigned int, size_t> pealCountsOnAllBells;
    pealCountsOnAllBellsByRingerId.clear();
    unsigned int highestRealBellNumber = 0;
    StatisticFilters filters(_filters);
    filters.SetIncludeHandBell(false);
    Duco::ObjectId towerId;
    if (!filters.Tower(towerId))
    {
        return;
    }
    const Tower* theTower = towersDatabase.FindTower(towerId, false);
    if (theTower == NULL)
    {
        return;
    }
    for (auto const& [key, value] : listOfObjects)
    {
        const Duco::Peal* thisPeal = static_cast<const Duco::Peal*>(value);
        if (filters.Match(*thisPeal) && (!lastPealId.ValidId() || thisPeal->Id() <= lastPealId))
        {
            const Ring* const theRing = theTower->FindRing(thisPeal->RingId());
            if (theRing != NULL)
            {
                std::set<unsigned int> bellsRung;
                bool asStrapper(false);
                std::set<Duco::ObjectId>::const_iterator ringerId = ringerIds.begin();
                while (ringerId != ringerIds.end())
                {
                    if (thisPeal->BellRung(*ringerId, bellsRung, asStrapper))
                    {
                        std::set<unsigned int>::const_iterator bellsRungIt = bellsRung.begin();
                        while (bellsRungIt != bellsRung.end())
                        {
                            unsigned int realBellNumber(*bellsRungIt);
                            highestRealBellNumber = std::max<unsigned int>(highestRealBellNumber, realBellNumber);
                            if (theRing->RealBellNumber(realBellNumber))
                            {
                                Duco::PealLengthInfo newInfo(*thisPeal);
                                std::map<Duco::ObjectId, std::map<unsigned int, Duco::PealLengthInfo> >::iterator ringerCircling = pealCountsOnAllBellsByRingerId.find(*ringerId);
                                if (ringerCircling == pealCountsOnAllBellsByRingerId.end())
                                {
                                    //Ringer missing create.
                                    std::pair<unsigned int, Duco::PealLengthInfo> newRingerFirstBell(realBellNumber, newInfo);
                                    std::map<unsigned int, Duco::PealLengthInfo> newRingersBells;
                                    newRingersBells.insert(newRingerFirstBell);
                                    std::pair<Duco::ObjectId, std::map<unsigned int, Duco::PealLengthInfo>> newRinger(*ringerId, newRingersBells);
                                    pealCountsOnAllBellsByRingerId.insert(newRinger);
                                }
                                else
                                {
                                    // Ringer exists
                                    if (ringerCircling->second.find(realBellNumber) == ringerCircling->second.end())
                                    {
                                        // Bell doesnt exists
                                        std::pair<unsigned int, Duco::PealLengthInfo> newBellCount(realBellNumber, newInfo);
                                        ringerCircling->second.insert(newBellCount);
                                    }
                                    else
                                    {
                                        // bell exist increment.
                                        ringerCircling->second.find(realBellNumber)->second += newInfo;
                                    }
                                }
                            }
                            ++bellsRungIt;
                        }
                    }
                    ++ringerId;
                }
            }
        }
    }

    //check missing zeros are added.
    std::map<Duco::ObjectId, std::map<unsigned int, Duco::PealLengthInfo>>::iterator it = pealCountsOnAllBellsByRingerId.begin();
    while (it != pealCountsOnAllBellsByRingerId.end())
    {
        for (int count = 1; it->second.size() < highestRealBellNumber; ++count)
        {
            if (it->second.find(count) == it->second.end())
            {
                Duco::PealLengthInfo newInfo;
                std::pair<unsigned int, Duco::PealLengthInfo> newRingerMissingBell(count, newInfo);
                it->second.insert(newRingerMissingBell);
            }
        }
        ++it;
    }
}

Duco::PercentageStatistics
PealDatabase::GetRingersPercentages(const Duco::ObjectId& towerId, const std::set<Duco::ObjectId>& ringerIds, const Duco::ObjectId& lastPealId) const
{
    PercentageStatistics results(ringerIds);

    for (auto const& [key, value] : listOfObjects)
    {
        const Duco::Peal* thisPeal = static_cast<const Duco::Peal*>(value);
        if (!thisPeal->Withdrawn() && (!lastPealId.ValidId() || thisPeal->Id() <= lastPealId))
        {
            if (!towerId.ValidId() || towerId == thisPeal->TowerId())
            {
                results.AddPeal(*thisPeal);
            }
        }
    }

    return results;
}

bool
PealDatabase::CheckForFirstCircleDate(const std::map<unsigned int, Duco::CirclingData>& sortedBellPealCounts, Duco::PerformanceDate& firstCircledDate, size_t noOfBells) const
{
    firstCircledDate.ResetToEarliest();
    if (noOfBells != sortedBellPealCounts.size())
        return false;

    std::map<unsigned int, Duco::CirclingData>::const_iterator it = sortedBellPealCounts.begin();
    while (it != sortedBellPealCounts.end())
    {
        if (it->second.PealCount() <= 0)
        {
            firstCircledDate.Clear();
            return false;
        }
        if (!firstCircledDate.Valid())
        {
            firstCircledDate = it->second.FirstPealDate();
        }
        else if (firstCircledDate < it->second.FirstPealDate())
        {
            firstCircledDate = it->second.FirstPealDate();
        }
        ++it;
    }
    return true;
}

void
PealDatabase::GetRingersInTower(const Duco::ObjectId& towerId, std::set<Duco::ObjectId>& ringers, size_t maxNoOfRingers) const
{
    ringers.clear();

    std::map<ObjectId, size_t> ringerPerformanceCounts;

    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const thisPeal = static_cast<const Peal* const>(value);
        if (!thisPeal->Withdrawn() && thisPeal->TowerId() == towerId)
        {
            std::set<ObjectId> thisPealRingers;
            thisPeal->RingerIds(thisPealRingers);
            std::set<ObjectId>::const_iterator ringerIt = thisPealRingers.begin();
            while (ringerIt != thisPealRingers.end())
            {
                std::map<ObjectId, size_t>::iterator pealCountIt = ringerPerformanceCounts.find(*ringerIt);
                if (pealCountIt == ringerPerformanceCounts.end())
                {
                    std::pair<ObjectId, size_t> newObject(*ringerIt, 1);
                    ringerPerformanceCounts.insert(newObject);
                }
                else
                {
                    pealCountIt->second += 1;
                }
                ++ringerIt;
            }
            
        }
    }
    if (maxNoOfRingers <= 0 || maxNoOfRingers == -1)
    {
        std::map<ObjectId, size_t>::iterator pealCountIt = ringerPerformanceCounts.begin();
        while (pealCountIt != ringerPerformanceCounts.end())
        {
            ringers.insert(pealCountIt->first);
            ++pealCountIt;
        }
    }
    else
    {
        // Sort by count
        std::multimap<size_t, Duco::ObjectId> sortedRingerPerformanceCounts;
        std::map<Duco::ObjectId, size_t>::iterator pealCountIt = ringerPerformanceCounts.begin();
        while (pealCountIt != ringerPerformanceCounts.end())
        {
            std::pair<size_t, ObjectId> newObject(pealCountIt->second, pealCountIt->first);
            sortedRingerPerformanceCounts.insert(newObject);
            ++pealCountIt;
        }

        // Copy to results
        std::multimap<size_t, ObjectId>::const_reverse_iterator countIt = sortedRingerPerformanceCounts.rbegin();
        bool stop = false;
        size_t lastCount = 0;
        while (!stop && countIt != sortedRingerPerformanceCounts.rend())
        {
            bool sameAsLastRinger = lastCount == countIt->first;
            lastCount = countIt->first;
            if (ringers.size() >= maxNoOfRingers && !sameAsLastRinger)
                stop = true;
            else
                ringers.insert(countIt->second);
            ++countIt;
        }
    }
}

void
PealDatabase::GetTownNamesWithPealCount(const::StatisticFilters& filters, std::map<std::wstring, PealLengthInfo>& sortedTowerIds) const
{
    sortedTowerIds.clear();
    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const thisPeal = static_cast<const Peal* const>(value);
        if (filters.Match(*thisPeal))
        {
            const Tower* const theTower = towersDatabase.FindTower(thisPeal->TowerId());
            if (theTower != NULL)
            {
                std::wstring town = theTower->City();
                PealLengthInfo newInfo(*thisPeal);
                pair<std::wstring, PealLengthInfo> newObject(town, newInfo);
                pair<std::map<std::wstring, PealLengthInfo>::iterator,bool> insertedIt = sortedTowerIds.insert(newObject);
                if (!insertedIt.second)
                {   
                    insertedIt.first->second.AddPeal(*thisPeal);
                }
            }
        }
    }
}

void
PealDatabase::GetCountiesWithPealCount(const::StatisticFilters& filters, std::map<std::wstring, PealLengthInfo>& sortedTowerIds) const
{
    sortedTowerIds.clear();
    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const thisPeal = static_cast<const Peal* const>(value);
        if (filters.Match(*thisPeal))
        {
            const Tower* const theTower = towersDatabase.FindTower(thisPeal->TowerId());
            if (theTower != NULL)
            {
                PealLengthInfo newInfo(*thisPeal);
                pair<std::wstring, PealLengthInfo> newObject(theTower->County(), newInfo);
                pair<std::map<std::wstring, PealLengthInfo>::iterator,bool> insertedIt = sortedTowerIds.insert(newObject);
                if (!insertedIt.second)
                {
                    insertedIt.first->second.AddPeal(*thisPeal);
                }
            }
        }
    }
}

void
PealDatabase::GetTenorKeysWithPealCount(const Duco::StatisticFilters& filters, std::map<std::wstring, PealLengthInfo>& sortedTowerIds) const
{
    sortedTowerIds.clear();
    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const thisPeal = static_cast<const Peal* const>(value);
        if (filters.Match(*thisPeal))
        {
            const Tower* const theTower = towersDatabase.FindTower(thisPeal->TowerId());
            if (theTower != NULL)
            {
                const Ring* const theRing = theTower->FindRing(thisPeal->RingId());
                if (theRing != NULL)
                {
                    PealLengthInfo newInfo(*thisPeal);
                    pair<std::wstring, PealLengthInfo> newObject(theRing->TenorKey(), newInfo);
                    pair<std::map<std::wstring, PealLengthInfo>::iterator, bool> insertedIt = sortedTowerIds.insert(newObject);
                    if (!insertedIt.second)
                    {
                        insertedIt.first->second.AddPeal(*thisPeal);
                    }
                }
            }
        }
    }
}

void
PealDatabase::GetTowersAndPealCount(const Duco::StatisticFilters& filters, bool combineLinkedTowers, std::multimap<Duco::PealLengthInfo, Duco::ObjectId>& sortedTowersData) const
{
    sortedTowersData.clear();
    std::map <Duco::ObjectId, Duco::PealLengthInfo> allTowersData;
    GetTowersPealCount(filters, allTowersData, combineLinkedTowers);

    std::map<Duco::ObjectId, Duco::PealLengthInfo>::const_iterator it = allTowersData.begin();
    while (it != allTowersData.end())
    {
        if (!it->second.Empty())
        {
            pair<PealLengthInfo, Duco::ObjectId> newObject(it->second, it->first);
            sortedTowersData.insert(newObject);
        }
        ++it;
    }
}

unsigned int
PealDatabase::NumberOfBellsRungInTower(const Duco::ObjectId& towerId) const
{
    unsigned int minNoOfBells = methodsDatabase.LowestValidOrderNumber();
    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* thisPeal = static_cast<const Peal*>(value);
        if (!thisPeal->Withdrawn() && thisPeal->TowerId() == towerId)
        {
            minNoOfBells = max(minNoOfBells, thisPeal->NoOfBellsRung(towersDatabase, methodsDatabase));
        }
    }
    return minNoOfBells;
}

const Peal* const
PealDatabase::FindPeal(const ObjectId& pealId) const
{
    const RingingObject* const object = FindObject(pealId);
    return static_cast<const Peal* const>(object);
}

Peal*
PealDatabase::FindPeal(const ObjectId& pealId, bool setIndex)
{
    RingingObject* object = FindObject(pealId, setIndex);
    return static_cast<Peal*>(object);
}

void
PealDatabase::GetTopFiveMethods(const Duco::StatisticFilters& filters, std::multimap<Duco::PealLengthInfo, Duco::ObjectId>& topFiveMethodsIds) const
{
    std::map<Duco::ObjectId, Duco::PealLengthInfo> allMethodIds;
    GetMethodsPealCount(filters, allMethodIds);
    KeepTop(5, topFiveMethodsIds, allMethodIds);
}

void
PealDatabase::GetMethodsPealCount(const Duco::StatisticFilters& filters, std::map<Duco::ObjectId, Duco::PealLengthInfo>& sortedMethodIds) const
{
    sortedMethodIds.clear();

    // id / count
    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* thisPeal = static_cast<const Peal*>(value);
        if (filters.Match(*thisPeal))
        {
            PealLengthInfo pealInfo(*thisPeal);
            pair<ObjectId, Duco::PealLengthInfo> newObject(thisPeal->MethodId(), pealInfo);
            pair<map<ObjectId, Duco::PealLengthInfo>::iterator, bool> methodInserted = sortedMethodIds.insert(newObject);
            if (!methodInserted.second)
            {
                methodInserted.first->second += pealInfo;
            }
        }
    }

    if (!filters.Enabled())
    {
        unsigned int stage = -1;
        filters.Stage(stage);
        // Add the methods that no peals have been rung in yet.
        methodsDatabase.AddMissingMethods(sortedMethodIds, stage);
    }
}

void
PealDatabase::GetMethodCircling(const Duco::StatisticFilters& filters, size_t methodOrder, std::map<unsigned int, Duco::CirclingData>& sortedBellPealCounts, PerformanceDate& firstCircledDate) const
{
    sortedBellPealCounts.clear();
    firstCircledDate.ResetToEarliest();

    for (auto const& [key, value] : listOfObjects)
    {
        const Duco::Peal* thisPeal = static_cast<const Duco::Peal*>(value);
        if (!thisPeal->Withdrawn() && !thisPeal->Handbell())
        {
            if (filters.Match(*thisPeal))
            {
                const Tower* const theTower = towersDatabase.FindTower(thisPeal->TowerId());
                if (theTower != NULL)
                {
                    const Ring* const theRing = theTower->FindRing(thisPeal->RingId());
                    if (theRing != NULL)
                    {
                        std::set<unsigned int> bellsRung;
                        bool asStrapper (false);
                        if (thisPeal->BellRung(filters.RingerId(), bellsRung, asStrapper))
                        {
                            std::set<unsigned int>::const_iterator bellsRungIt = bellsRung.begin();
                            while (bellsRungIt != bellsRung.end())
                            {
                                Duco::CirclingData newCirclingData(1, thisPeal->Date());
                                pair<unsigned int, Duco::CirclingData> newObject(*bellsRungIt, newCirclingData);
                                pair<std::map<unsigned int, Duco::CirclingData>::iterator, bool> insertedIt = sortedBellPealCounts.insert(newObject);
                                if (!insertedIt.second)
                                {
                                    insertedIt.first->second.Increment(thisPeal->Date());
                                }
                                ++bellsRungIt;
                            }
                        }
                    }
                }
            }
        }
    }
    CheckForFirstCircleDate(sortedBellPealCounts, firstCircledDate, methodOrder);
}


bool
PealDatabase::PealsInSeries(const Duco::ObjectId& seriesId, Duco::PerformanceDate& pealDate, std::set<Duco::ObjectId>& pealIds) const
{
    pealDate.Clear();
    pealIds.clear();
    if (seriesId.ValidId())
    {
        for (auto const& [key, value] : listOfObjects)
        {
            const Peal* thisPeal = static_cast<const Peal*>(value);
            if (!thisPeal->Withdrawn() && thisPeal->SeriesId() == seriesId)
            {
                if (thisPeal->Date() < pealDate)
                    pealDate = thisPeal->Date();
                pealIds.insert(thisPeal->Id());
            }
        }
    }
    return pealIds.size() > 0;
}

bool
PealDatabase::CompletedSeries(const Duco::MethodSeries& series) const
{
    std::map<ObjectId, unsigned int> methodSeriesPealCounts;

    DUCO_OBJECTS_CONTAINER::const_iterator pealIt = listOfObjects.begin();
    while (pealIt != listOfObjects.end() && methodSeriesPealCounts.size() < series.NoOfMethods())
    {
        const Peal* thisPeal = static_cast<const Peal*>(pealIt->second);
        if (!thisPeal->Withdrawn() && series.ContainsMethod(thisPeal->MethodId()))
        {
            std::pair<ObjectId, unsigned int> newObject(thisPeal->MethodId(), 1);
            std::pair<std::map<ObjectId, unsigned int>::iterator, bool> inserted = methodSeriesPealCounts.insert(newObject);
            if (!inserted.second)
            {
                ++(inserted.first->second);
            }
        }
        ++pealIt;
    }
    return methodSeriesPealCounts.size() == series.NoOfMethods();
}

void
PealDatabase::GetRingersInMethodSeries(const ObjectId& seriesId, std::set<ObjectId>& ringerIds) const
{
    ringerIds.clear();

    const MethodSeries* const series = seriesDatabase.FindMethodSeries(seriesId);
    if (series != NULL)
    {
        for (auto const& [key, value] : listOfObjects)
        {
            const Peal* thisPeal = static_cast<const Peal*>(value);
            if (!thisPeal->Withdrawn() && series->ContainsMethod(thisPeal->MethodId()))
            {
                std::set<ObjectId> allRingerIds;
                thisPeal->RingerIds(allRingerIds);
                if (!allRingerIds.empty())
                {
                    ringerIds.insert(allRingerIds.begin(), allRingerIds.end());
                }
            }
        }
    }
}

void
PealDatabase::GetPealCountInMethodSeriesForRinger(const ObjectId& ringerId, const ObjectId& seriesId, std::map<ObjectId, unsigned int>& methodIdsAndCount, PerformanceDate& completedDate) const
{
    methodIdsAndCount.clear();
    completedDate.ResetToEarliest();

    const MethodSeries* const series = seriesDatabase.FindMethodSeries(seriesId);
    if (series != NULL)
    {
        // First make sure all series Ids are added to the results
        std::set<ObjectId>::const_iterator methodIt = series->Methods().begin();
        while (methodIt != series->Methods().end())
        {
            pair<ObjectId, unsigned int> newObject(*methodIt, 0);
            methodIdsAndCount.insert(newObject);
            ++methodIt;
        }

        size_t totalMethodCount = methodIdsAndCount.size();
        for (auto const& [key, value] : listOfObjects)
        {
            const Peal* thisPeal = static_cast<const Peal*>(value);
            if (!thisPeal->Withdrawn() && series->ContainsMethod(thisPeal->MethodId()))
            {
                bool asStrapper (false);
                if (thisPeal->ContainsRinger(ringerId, asStrapper))
                {
                    pair<Duco::ObjectId, unsigned int> newObject(thisPeal->MethodId(), 1);
                    pair<std::map<Duco::ObjectId, unsigned int>::iterator,bool> it = methodIdsAndCount.insert(newObject);
                    if (!it.second)
                    {
                        it.first->second += 1;
                    }
                    if (it.first->second == 1)
                    {
                        --totalMethodCount;
                        if (completedDate < thisPeal->Date())
                        {
                            completedDate = thisPeal->Date();
                        }
                    }
                }
            }
        }

        if (totalMethodCount != 0)
        {
            completedDate.ResetToEarliest();
        }
    }
}

unsigned int
PealDatabase::GetAlphabeticCompletions(const Duco::StatisticFilters& filters, bool uniqueMethods, bool insideOnly, AlphabetData& alphabets) const
{
    alphabets.Reset();
    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* thisPeal = static_cast<const Peal*>(value);
        if (filters.Match(*thisPeal))
        {
            bool ringerInside (true);
            if (insideOnly && filters.RingerId().ValidId())
            { // Check if this method is a principle, or the ringer wasn't on the treble
                ringerInside = false;
                std::set<unsigned int> bellsRung;
                bool strapper;
                if (thisPeal->BellRung(filters.RingerId(), bellsRung, strapper))
                {
                    if (bellsRung.size() > 1 || (bellsRung.size() == 1 && *bellsRung.begin() != 1))
                    {
                        ringerInside = true;
                    }
                    else
                    {
                        const Duco::Method* const theMethod = methodsDatabase.FindMethod(thisPeal->MethodId());
                        if (theMethod != NULL)
                        {
                            if (theMethod->IsPrinciple())
                            {
                                ringerInside = true;
                            }
                        }
                    }
                }
            }
            if (ringerInside) 
            {
                unsigned int stage;
                filters.Stage(stage);
                std::wstring methodType;
                filters.Type(methodType);
                thisPeal->AlphabetLetter(methodsDatabase, methodType, stage, alphabets, uniqueMethods);
            }
        }
    }

    return alphabets.NumberOfCompletedAlphabets();
}

Duco::ObjectId
PealDatabase::FindPeal(const Duco::PerformanceDate& pealDate) const
{
    if (!pealDate.Valid())
    {
        return KNoId;
    }
    Duco::ObjectId foundId;
    unsigned int dayOrder = -1;

    unsigned int noOfDaysFromDate (numeric_limits<unsigned int>::max());
    
    DUCO_OBJECTS_CONTAINER::const_iterator it = listOfObjects.begin();
    while (it != listOfObjects.end() && noOfDaysFromDate != 0)
    {
        const Peal* thisPeal = static_cast<const Peal*>(it->second);
        unsigned int noOfDaysUntilThisPeal = thisPeal->Date().NumberOfDaysUntil(pealDate);
        if (noOfDaysUntilThisPeal < noOfDaysFromDate)
        {
            noOfDaysFromDate = noOfDaysUntilThisPeal;
            foundId = it->first;
            dayOrder = thisPeal->DayOrder();
        }
        else if (noOfDaysUntilThisPeal == noOfDaysFromDate && dayOrder > thisPeal->DayOrder())
        {
            foundId = it->first;
            dayOrder = thisPeal->DayOrder();
        }
        ++it;
    }
    return foundId;
}

Duco::ObjectId
PealDatabase::FindPealByBellboardId(const std::wstring& bellboardId) const
{
    Duco::ObjectId foundId;

    DUCO_OBJECTS_CONTAINER::const_iterator it = listOfObjects.begin();
    while (it != listOfObjects.end() && !foundId.ValidId())
    {
        const Peal* thisPeal = static_cast<const Peal*>(it->second);
        if (thisPeal->BellBoardId().compare(bellboardId) == 0)
        {
            foundId = it->first;
        }
        ++it;
    }
    return foundId;
}

bool
PealDatabase::MatchingPeal(const Peal& peal,
                           const std::wstring& findRingerName, const std::wstring& findTowerName,
                           const std::wstring& findAssociationName, const std::wstring& findMethodName,
                           const std::wstring& findFootNotes, const std::wstring& findComposer) const
{
    bool ringerFound (true);
    bool towerFound (true);
    bool associationFound (true);
    bool methodFound (true);
    bool footnotesFound (true);
    bool composerFound (true);
    if ( findRingerName.length() != 0)
    {
        ringerFound = false;
        ringerFound = peal.CheckRingerNames(ringersDatabase, settings, false, findRingerName, true);
    }
    if ( findTowerName.length() != 0)
    {
        towerFound = false;
        const Tower* const theTower = towersDatabase.FindTower(peal.TowerId());
        if (theTower != NULL)
        {
            towerFound = DucoEngineUtils::FindSubstring(findTowerName, theTower->FullName());
        }
    }
    if ( findMethodName.length() != 0)
    {
        methodFound = false;
        const Method* const theMethod = methodsDatabase.FindMethod(peal.MethodId());
        if (theMethod != NULL)
        {
            methodFound = DucoEngineUtils::FindSubstring(findMethodName, theMethod->FullName(methodsDatabase));
        }
    }
    if (findAssociationName.length() != 0)
    {
        associationFound = false;
        const Association* const theAssociation = associationsDatabase.FindAssociation(peal.AssociationId());
        if (theAssociation != NULL)
        {
            associationFound = DucoEngineUtils::FindSubstring(findAssociationName, theAssociation->Name());
        }
    }
    if ( findFootNotes.length() != 0)
    {
        footnotesFound = false;
        footnotesFound = DucoEngineUtils::FindSubstring(findFootNotes, peal.Footnotes());
    }
    if ( findComposer.length() != 0)
    {
        composerFound = false;
        composerFound = DucoEngineUtils::FindSubstring(findComposer, peal.Composer());
    }

    return ringerFound && towerFound && associationFound && methodFound && footnotesFound && composerFound;
}

void
PealDatabase::GetAssociationsPealCount(const Duco::StatisticFilters& filters, std::map<Duco::ObjectId, Duco::PealLengthInfo>& sortedPealCounts, bool mergeLinked) const
{
    sortedPealCounts.clear();

    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        if (filters.Match(*nextPeal))
        {
            Duco::ObjectId nextAssociationId = nextPeal->AssociationId();
            Duco::PealLengthInfo newInfo(*nextPeal);
            std::pair<Duco::ObjectId, Duco::PealLengthInfo> newObject (nextAssociationId, newInfo);
            pair< std::map<Duco::ObjectId, Duco::PealLengthInfo>::iterator,bool> insertedIt = sortedPealCounts.insert(newObject);
            if (insertedIt.second == false)
            {
                insertedIt.first->second += newInfo;
            }
        }
    }

    // Add associations with no peals
    std::set<Duco::ObjectId> allAssociationIds;
    associationsDatabase.GetAllObjectIds(allAssociationIds);
    std::set<Duco::ObjectId>::const_iterator missingIdsIt = allAssociationIds.begin();
    while (missingIdsIt != allAssociationIds.end())
    {
        if (sortedPealCounts.find(*missingIdsIt) == sortedPealCounts.end())
        {
            Duco::PealLengthInfo newInfo;
            sortedPealCounts[*missingIdsIt] = newInfo;
        }

        ++missingIdsIt;
    }

    if (mergeLinked)
    {
        associationsDatabase.MergeLinkedAssociationsPealCounts(sortedPealCounts);
    }

    // Merge those missing an association and those with a blank association
    std::map<Duco::ObjectId, Duco::PealLengthInfo>::iterator noAssociationCountIt = sortedPealCounts.find(KNoId);
    if (noAssociationCountIt != sortedPealCounts.end())
    {
        Duco::ObjectId nullAssociationId = associationsDatabase.SuggestAssociation(L"");
        if (nullAssociationId.ValidId())
        {
            std::map<Duco::ObjectId, Duco::PealLengthInfo>::iterator emptyAssociationCountIt = sortedPealCounts.find(nullAssociationId);
            emptyAssociationCountIt->second += noAssociationCountIt->second;
            sortedPealCounts.erase(noAssociationCountIt);
        }
    }
}

void
PealDatabase::MedianPealFeesPerPerson(std::map<unsigned int, unsigned int>& avgPealFeePerYear, const Duco::ObjectId& associationId) const
{
    avgPealFeePerYear.clear(); 

    std::multimap<unsigned int, unsigned int> allPealFeesByYear;

    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        if (!nextPeal->Withdrawn())
        {
            if (nextPeal->AssociationId() == associationId || !associationId.ValidId())
            {
                unsigned int avgPealFeePaid = nextPeal->AvgPealFeePaidPerPerson();
                if (avgPealFeePaid > 0 && avgPealFeePaid != -1)
                {
                    std::pair<unsigned int, unsigned int> newObject (nextPeal->Date().Year(), avgPealFeePaid);
                    allPealFeesByYear.insert(newObject);
                }
            }
        }
    }

    multimap<unsigned int, unsigned int>::const_iterator firstYear = allPealFeesByYear.begin();
    multimap<unsigned int, unsigned int>::const_reverse_iterator lastYear = allPealFeesByYear.rbegin();

    if (lastYear != allPealFeesByYear.rend())
    {
        while (firstYear != allPealFeesByYear.end())
        {
            unsigned int currentYear = firstYear->first;
            ++firstYear;
    
            size_t numberInYear = allPealFeesByYear.count(currentYear);
            size_t thisYearMedian (0);
            if (numberInYear > 0)
            {
                multimap<unsigned int, unsigned int>::const_iterator firstItem = allPealFeesByYear.lower_bound(currentYear);
                for (unsigned int i(0); i < (numberInYear/2); ++i)
                {
                    ++firstItem;
                }
                thisYearMedian = firstItem->second;
            }
            pair<unsigned int, unsigned int> newObject(currentYear, (unsigned int)thisYearMedian);
            avgPealFeePerYear.insert(newObject);
        }
    }
}

void
PealDatabase::GetAllComposersByPealCount(const Duco::StatisticFilters& filters, bool ignoreBracketed, std::map<std::wstring, Duco::PealLengthInfo>& sortedComposerCounts) const
{
    sortedComposerCounts.clear();    

    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        if (filters.Match(*nextPeal))
        {
            std::wstring nextComposer = nextPeal->Composer();
            if (nextComposer.length() > 0)
            {
                if (ignoreBracketed && nextComposer.find(L"(") != std::wstring::npos)
                { // remove brackets
                    nextComposer = DucoEngineUtils::RemoveBrackets(nextComposer);
                }

                Duco::PealLengthInfo newInfo(*nextPeal);
                std::pair<std::wstring, Duco::PealLengthInfo> newObject (nextComposer, newInfo);
                pair< std::map<std::wstring, Duco::PealLengthInfo>::iterator,bool> insertedIt = sortedComposerCounts.insert(newObject);
                if (insertedIt.second == false)
                {
                    insertedIt.first->second += newInfo;
                }
            }
        }
    }
}

void
PealDatabase::GetAllConductorsByPealCount(const Duco::StatisticFilters& filters, bool combineLinked, std::multimap<Duco::PealLengthInfo, ObjectId>& sortedConductorCounts) const
{
    sortedConductorCounts.clear();    

    std::map<ObjectId, Duco::PealLengthInfo> conductorsById;

    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        if (filters.Match(*nextPeal))
        {
            Duco::PealLengthInfo newInfo(*nextPeal);
            if (nextPeal->ConductedType() == ESilentAndNonConducted)
            {
                std::pair<ObjectId, Duco::PealLengthInfo> newObject (KNoId, newInfo);
                pair< std::map<ObjectId, Duco::PealLengthInfo>::iterator,bool> insertedIt = conductorsById.insert(newObject);
                if (insertedIt.second == false)
                {
                    insertedIt.first->second += newInfo;
                }
            }
            else
            {
                const std::set<ObjectId> conductors = nextPeal->Conductors();
                std::set<ObjectId>::const_iterator it2 = conductors.begin();
                while (it2 != conductors.end())
                {
                    std::pair<ObjectId, Duco::PealLengthInfo> newObject (*it2, newInfo);
                    pair< std::map<ObjectId, Duco::PealLengthInfo>::iterator,bool> insertedIt = conductorsById.insert(newObject);
                    if (insertedIt.second == false)
                    {
                        insertedIt.first->second += newInfo;
                    }
                    ++it2;
                }
            }
        }
    }

    if (combineLinked)
    {
        ringersDatabase.CombineLinkedRingers(conductorsById);
    }

    std::map<ObjectId, Duco::PealLengthInfo>::const_iterator it3 = conductorsById.begin();
    while (it3 != conductorsById.end())
    {
        std::pair<Duco::PealLengthInfo, ObjectId> newObject (it3->second, it3->first);
        sortedConductorCounts.insert(newObject);
        ++it3;
    }
}

void
PealDatabase::GetAllFeePayers(const Duco::ObjectId& associationId, std::set<Duco::ObjectId>& feePayerIds) const
{
    feePayerIds.clear();

    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        if (!associationId.ValidId() || nextPeal->AssociationId() == associationId)
        {
            ObjectId feePayerId = nextPeal->GetFeePayerId();
            if (feePayerId.ValidId())
            {
                feePayerIds.insert(feePayerId);
            }
        }
    }
}

bool
PealDatabase::FindPealsWithUnpaidFees(const Duco::ObjectId& feePayerId, const Duco::ObjectId& associationId, unsigned int year, std::set<UnpaidPealFeeData>& pealIds) const
{
    std::set<ObjectId> commonFeePayerIds;
    GetAllFeePayers(KNoId, commonFeePayerIds);

    pealIds.clear();
    std::map<unsigned int, unsigned int> avgPealFeePerYear;
    MedianPealFeesPerPerson(avgPealFeePerYear, associationId);
    
    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        if (year == -1 || nextPeal->Date().Year() == year)
        {
            if (associationId == nextPeal->AssociationId() || !associationId.ValidId())
            {
                ObjectId suggestedFeePayer = nextPeal->SuggestFeePayerId(commonFeePayerIds);
                if (!feePayerId.ValidId() || suggestedFeePayer == feePayerId)
                {
                    std::map<unsigned int, unsigned int>::const_iterator avgPealFee = avgPealFeePerYear.find(nextPeal->Date().Year());
                    if (avgPealFee == avgPealFeePerYear.end() || avgPealFee->second == 0)
                    {
                        UnpaidPealFeeData newData(nextPeal->Id(), suggestedFeePayer, nextPeal->TotalPealFeePaid(), 0);
                        pealIds.insert(newData);
                    }
                    else if (!nextPeal->CheckPaidFeeAgainstMedian(avgPealFee->second))
                    {
                        UnpaidPealFeeData newData(nextPeal->Id(), suggestedFeePayer, nextPeal->TotalPealFeePaid(), nextPeal->MissingPealFee(avgPealFee->second));
                        pealIds.insert(newData);
                    }
                }
            }
        }
    }
    return pealIds.size() > 0;
}

void
PealDatabase::GetAllMethodNamesByPealCount(std::map<std::wstring, Duco::PealLengthInfo>& sortedPealCounts) const
{
    sortedPealCounts.clear();

    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        if (!nextPeal->Withdrawn())
        {
            const Method* const theMethod = methodsDatabase.FindMethod(nextPeal->MethodId());
            if (theMethod != NULL)
            {
                Duco::PealLengthInfo newInfo(*nextPeal);
                std::pair<std::wstring, Duco::PealLengthInfo> newObject (theMethod->Name(), newInfo);
                pair< std::map<std::wstring, Duco::PealLengthInfo>::iterator,bool> insertedIt = sortedPealCounts.insert(newObject);
                if (insertedIt.second == false)
                {
                    insertedIt.first->second += newInfo;
                }
            }
        }
    }
}

void 
PealDatabase::GetAllMethodTypesByPealCount(const Duco::StatisticFilters& filters, std::map<std::wstring, Duco::PealLengthInfo>& sortedPealCounts) const
{
    sortedPealCounts.clear();    

    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        if (filters.Match(*nextPeal))
        {
            const Method* const theMethod = methodsDatabase.FindMethod(nextPeal->MethodId());

            if (theMethod != NULL)
            {
                Duco::PealLengthInfo newInfo(*nextPeal);
                std::pair<std::wstring, Duco::PealLengthInfo> newObject (theMethod->Type(), newInfo);
                pair< std::map<std::wstring, Duco::PealLengthInfo>::iterator,bool> insertedIt = sortedPealCounts.insert(newObject);
                if (insertedIt.second == false)
                {
                    insertedIt.first->second += newInfo;
                }
            }
        }
    }
}

void 
PealDatabase::GetAllYearsByPealCount(const Duco::StatisticFilters& filters, std::map<unsigned int, Duco::PealLengthInfo>& sortedPealCounts) const
{
    sortedPealCounts.clear();

    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        if (filters.Match(*nextPeal))
        {
            const PerformanceDate& pealDate = nextPeal->Date();

            Duco::PealLengthInfo newInfo(*nextPeal);
            std::pair<unsigned int, Duco::PealLengthInfo> newObject (pealDate.Year(), newInfo);
            pair< std::map<unsigned int, Duco::PealLengthInfo>::iterator,bool> insertedIt = sortedPealCounts.insert(newObject);
            if (insertedIt.second == false)
            {
                insertedIt.first->second += newInfo;
            }
        }
    }

    if (sortedPealCounts.size() > 0)
    {
        unsigned int lowestKey = sortedPealCounts.begin()->first;
        unsigned int highestKey = sortedPealCounts.rbegin()->first;
        for (unsigned int count = lowestKey; count <= highestKey; ++count)
        {
            if (sortedPealCounts.find(count) == sortedPealCounts.end())
            {
                sortedPealCounts[count].Clear();
            }
        }
    }
}

void
PealDatabase::GetYearRange(const Duco::StatisticFilters& filters, std::set<unsigned int>& years) const
{
    years.clear();

    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        if (filters.Match(*nextPeal))
        {
            years.insert(nextPeal->Date().Year());
        }
    }
}

void
PealDatabase::GetNoOfChangesRange(const Duco::StatisticFilters& filters, std::map<unsigned int, Duco::PealLengthInfo>& noOfChanges) const
{
    noOfChanges.clear();

    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        if (filters.Match(*nextPeal))
        {
            Duco::PealLengthInfo newInfo(*nextPeal);
            pair<unsigned int, Duco::PealLengthInfo> newObject (nextPeal->NoOfChanges(), newInfo);
            pair< std::map<unsigned int, Duco::PealLengthInfo>::iterator,bool> insertedIt = noOfChanges.insert(newObject);
            if (insertedIt.second == false)
            {
                insertedIt.first->second += newInfo;
            }
        }
    }
}

void
PealDatabase::GetAllMonthsByPealCount(const Duco::StatisticFilters& filters, unsigned int year, std::map<unsigned int, Duco::PealLengthInfo>& monthCounts) const
{
    monthCounts.clear();

    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        if (filters.Match(*nextPeal) && nextPeal->Date().Year() == year)
        {
            const PerformanceDate& pealDate = nextPeal->Date();

            Duco::PealLengthInfo newInfo(*nextPeal);
            std::pair<unsigned int, Duco::PealLengthInfo> newObject(pealDate.Month(), newInfo);
            pair< std::map<unsigned int, Duco::PealLengthInfo>::iterator, bool> insertedIt = monthCounts.insert(newObject);
            if (insertedIt.second == false)
            {
                insertedIt.first->second += newInfo;
            }
        }
    }
}

void
PealDatabase::GetAllStagesByPealCount(const Duco::StatisticFilters& filters, 
                                      std::map<unsigned int, Duco::PealLengthInfo>& sortedPealCounts,
                                      std::map<unsigned int, Duco::PealLengthInfo>& sortedSplicedPealCounts) const
{
    sortedPealCounts.clear();    
    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        if (filters.Match(*nextPeal))
        {
            const Method* const theMethod = methodsDatabase.FindMethod(nextPeal->MethodId());
            if (theMethod != NULL)
            {
                Duco::PealLengthInfo newInfo(*nextPeal);
                std::pair<unsigned int, Duco::PealLengthInfo> newObject (theMethod->Order(), newInfo);
                pair< std::map<unsigned int, Duco::PealLengthInfo>::iterator,bool> insertedIt;
                if (theMethod->DualOrder())
                {
                    insertedIt = sortedSplicedPealCounts.insert(newObject);
                }
                else
                {
                    insertedIt = sortedPealCounts.insert(newObject);
                }
                if (insertedIt.second == false)
                {
                    insertedIt.first->second += newInfo;
                }
            }
        }
    }
}

void
PealDatabase::GetAllTenorKeysByPealCount(const Duco::StatisticFilters& filters, std::map<std::wstring, Duco::PealLengthInfo>& sortedTenorKeys) const
{
    sortedTenorKeys.clear();

    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        if (filters.Match(*nextPeal))
        {
            const Tower* const theTower = towersDatabase.FindTower(nextPeal->TowerId());
            if (theTower != NULL)
            {
                const Ring* const theRing = theTower->FindRing(nextPeal->RingId());
                if (theRing != NULL && theRing->TenorKey().length() > 0)
                {
                    Duco::PealLengthInfo newInfo(*nextPeal);
                    std::pair<std::wstring, Duco::PealLengthInfo> newObject(theRing->TenorKey(), newInfo);
                    pair< std::map<std::wstring, Duco::PealLengthInfo>::iterator, bool> insertedIt = sortedTenorKeys.insert(newObject);
                    if (insertedIt.second == false)
                    {
                        insertedIt.first->second += newInfo;
                    }
                }
            }
        }
    }
}

void 
PealDatabase::GetDatesWithMultiplePeals(std::list<PerformanceDate>& dates) const
{
    dates.clear();

    std::map<PerformanceDate, unsigned int> pealDatesAndCount;

    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const firstPeal = static_cast<const Peal* const>(value);
        PerformanceDate previousDate = firstPeal->Date();
        std::pair<PerformanceDate, unsigned int> newObject (previousDate, 1);
        pair<std::map<PerformanceDate, unsigned int>::iterator, bool> insertedIt = pealDatesAndCount.insert(newObject);
        if (!insertedIt.second)
        {
            insertedIt.first->second += 1;
        }
    }

    std::map<PerformanceDate, unsigned int>::const_iterator it2 = pealDatesAndCount.begin();
    while (it2 != pealDatesAndCount.end())
    {
        if (it2->second > 1)
        {
            dates.push_back(it2->first);
        }
        ++it2;
    }
    dates.unique();
}

void 
PealDatabase::GetPealWithDate(PerformanceDate date, std::vector<ObjectId>& pealIds) const
{
    pealIds.clear();
 
    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        if (date == nextPeal->Date())
        {
            pealIds.push_back(key);
        }
    }
    std::sort(pealIds.begin(), pealIds.end());
}

bool
PealDatabase::GeneratePealSpeedStats(const Duco::StatisticFilters& filters, bool fastest, std::multimap<float, Duco::ObjectId>& pealIds) const
{
    pealIds.clear();

    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        if (filters.Match(*nextPeal) && nextPeal->NoOfChanges() > 0 && nextPeal->ChangesAndTimeValid())
        {
            float changesPerMinute = nextPeal->ChangesPerMinute();
            std::pair<float, Duco::ObjectId> newObject(changesPerMinute, key);

            pealIds.insert(newObject);
            if (fastest)
            {
                while (pealIds.size() > 20)
                {
                    pealIds.erase(pealIds.begin());
                }
            }
            else
            {
                while (pealIds.size() > 20)
                {
                    std::multimap<float, Duco::ObjectId>::const_reverse_iterator it2 = pealIds.rbegin();
                    pealIds.erase(it2->first);
                }
            }
        }
    }

    return pealIds.size() > 0;
}

void
PealDatabase::GetConductedOrderCount(const Duco::StatisticFilters& filters, std::map<unsigned int, Duco::PealLengthInfo>& sortedPealCounts) const
{
    sortedPealCounts.clear();
    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        if (filters.Match(*nextPeal))
        {
            const Method* const theMethod = methodsDatabase.FindMethod(nextPeal->MethodId());
            if (theMethod != NULL)
            {
                Duco::PealLengthInfo newInfo(*nextPeal);
                std::pair<unsigned int, Duco::PealLengthInfo> newObject(theMethod->Order(), newInfo);
                pair< std::map<unsigned int, Duco::PealLengthInfo>::iterator,bool> inserted = sortedPealCounts.insert(newObject);
                if (!inserted.second)
                {
                    inserted.first->second += newInfo;
                }
            }
        }
    }
}

void
PealDatabase::GetConductedMethodCount(const Duco::StatisticFilters& filters, std::map<Duco::ObjectId, Duco::PealLengthInfo>& sortedPealCounts) const
{
    sortedPealCounts.clear();
    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        if (filters.Match(*nextPeal))
        {
            Duco::PealLengthInfo newInfo(*nextPeal);
            std::pair<Duco::ObjectId, Duco::PealLengthInfo> newObject(nextPeal->MethodId(), newInfo);
            pair< std::map<Duco::ObjectId, Duco::PealLengthInfo>::iterator,bool> inserted = sortedPealCounts.insert(newObject);
            if (!inserted.second)
            {
                inserted.first->second += newInfo;
            }
        }
    }
}

void
PealDatabase::FindRingerPerformancesNearDate(const PerformanceDate& performanceDate, const std::set<Duco::ObjectId>& ringerIds, std::set<Duco::RingerPealDates>& nearMatches) const
{
    nearMatches.clear();
    std::map<Duco::ObjectId, Duco::RingerPealDates> pealCounts;
    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        for (auto& ringerId : ringerIds)
        {
            bool asStrapper;
            if (nextPeal->ContainsRinger(ringerId, asStrapper))
            {
                std::map<Duco::ObjectId, Duco::RingerPealDates>::iterator it = pealCounts.find(ringerId);
                if (it != pealCounts.end())
                {
                    it->second.AddPeal(*nextPeal);
                }
                else
                {
                    Duco::RingerPealDates newRingerPealDates(ringerId, performanceDate);
                    newRingerPealDates.AddPeal(*nextPeal);
                    pealCounts.insert(std::pair<Duco::ObjectId, Duco::RingerPealDates>(ringerId,newRingerPealDates));
                }
            }
        }
    }

    std::set <Duco::ObjectId> ringerIdsAdded;
    for (auto const& [key, value] : pealCounts)
    {
        nearMatches.insert(value);
        ringerIdsAdded.insert(key);
    }
    for (auto const& key : ringerIds)
    {
        if (ringerIdsAdded.find(key) == ringerIdsAdded.end())
        {
            Duco::RingerPealDates newRingerPealDates(key, performanceDate);
            nearMatches.insert(newRingerPealDates);
        }
    }
}

void
PealDatabase::MilestonePredictions(const Duco::StatisticFilters& filters, size_t totalPealCountForRinger, std::set<MilestoneData>& milestones) const
{
    size_t majorMilestoneCalc = totalPealCountForRinger / 250;
    ++majorMilestoneCalc;
    size_t minorMilestoneCalc = totalPealCountForRinger / 100;
    ++minorMilestoneCalc;

    size_t majorMilestone (majorMilestoneCalc * 250);
    size_t minorMilestone (minorMilestoneCalc * 100);
    if (minorMilestone == majorMilestone)
    {
        minorMilestone += 100;
    }
    // Really minor milestones - worth adding?
    if (minorMilestone == 100)
    {
        if (totalPealCountForRinger < 5)
        {
            minorMilestone = 5;
            majorMilestone = 10;
        }
        else if (totalPealCountForRinger < 10)
        {
            minorMilestone = 10;
            majorMilestone = 25;
        }
        else if (totalPealCountForRinger < 25)
        {
            minorMilestone = 25;
            majorMilestone = 50;
        }
        else if (totalPealCountForRinger < 50)
        {
            minorMilestone = 50;
            majorMilestone = 100;
        }
    }

    MilestoneData minorMilestoneData (minorMilestone);
    SetDaysToMilestone(filters, totalPealCountForRinger, minorMilestoneData, TMilestoneAverage::E6MonthAverage);
    SetDaysToMilestone(filters, totalPealCountForRinger, minorMilestoneData, TMilestoneAverage::E12MonthAverage);
    SetDaysToMilestone(filters, totalPealCountForRinger, minorMilestoneData, TMilestoneAverage::EAllTimeAverage);
    milestones.insert(minorMilestoneData);

    MilestoneData majorMilestoneData (majorMilestone);
    SetDaysToMilestone(filters, totalPealCountForRinger, majorMilestoneData, TMilestoneAverage::E6MonthAverage);
    SetDaysToMilestone(filters, totalPealCountForRinger, majorMilestoneData, TMilestoneAverage::E12MonthAverage);
    SetDaysToMilestone(filters, totalPealCountForRinger, majorMilestoneData, TMilestoneAverage::EAllTimeAverage);
    milestones.insert(majorMilestoneData);

    if (minorMilestone > 100)
    {
        size_t thousandMilestone = (size_t)((float)totalPealCountForRinger / 1000.0) * 1000 + 1000;
        MilestoneData milestoneData(thousandMilestone);
        SetDaysToMilestone(filters, totalPealCountForRinger, milestoneData, TMilestoneAverage::E6MonthAverage);
        SetDaysToMilestone(filters, totalPealCountForRinger, milestoneData, TMilestoneAverage::E12MonthAverage);
        SetDaysToMilestone(filters, totalPealCountForRinger, milestoneData, TMilestoneAverage::EAllTimeAverage);
        milestones.insert(milestoneData);
    }
}

bool
PealDatabase::Milestones(const Duco::StatisticFilters& filters, std::set<MilestoneData>& milestones) const
{
    milestones.clear();
    PealLengthInfo totalPealCountForRinger(PerformanceInfo(filters));

    MilestonePredictions(filters, totalPealCountForRinger.TotalPeals(), milestones);

    std::set<size_t> milestonesToFind;
    milestonesToFind.insert(1);
    milestonesToFind.insert(10);
    milestonesToFind.insert(25);
    milestonesToFind.insert(50);
    for (size_t i(100); i < totalPealCountForRinger.TotalPeals(); i += 50)
    {
        milestonesToFind.insert(i);
    }

    PealLengthInfo pealCount;
    for (auto const& indexIt : objectsIndex)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(indexIt.Object());
        if (filters.Match(*nextPeal))
        {
            pealCount += *nextPeal;
            if (totalPealCountForRinger == pealCount)
            {
                MilestoneData mileStone(pealCount.TotalPeals());
                mileStone.SetPeal(nextPeal->Id(), nextPeal->Date());
                mileStone.lastPeal = true;
                milestones.insert(mileStone);
            }
            else if (milestonesToFind.find(pealCount.TotalPeals()) != milestonesToFind.end())
            {
                MilestoneData mileStone(pealCount.TotalPeals());
                mileStone.SetPeal(nextPeal->Id(), nextPeal->Date());
                milestones.insert(mileStone);
            }
        }
    }

    return !RebuildRecommended();
}

void
PealDatabase::SetDaysToMilestone(const Duco::StatisticFilters& filters, size_t currentPealCount, Duco::MilestoneData& milestone, TMilestoneAverage averageType) const
{
    float numberOfPeals = 0;
    PerformanceDate startDate = milestone.StartDate(averageType);
    PerformanceDate firstPealDate;

    bool stopCounting = false;
    DUCO_OBJECTS_INDEX::const_reverse_iterator indexIt = objectsIndex.rbegin();
    while (indexIt != objectsIndex.rend() && !stopCounting)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(indexIt->Object());
        if (filters.Match(*nextPeal))
        {
            if (averageType == TMilestoneAverage::EAllTimeAverage)
            {
                if (firstPealDate > nextPeal->Date())
                {
                    firstPealDate = nextPeal->Date();
                }
                ++numberOfPeals;
            }
            else
            {
                if (nextPeal->Date() >= startDate)
                    ++numberOfPeals;
                else
                    stopCounting = true;
            }
        }
        ++indexIt;
    }

    if (numberOfPeals == 0)
        return;

    size_t pealsRequired = milestone.pealCount - currentPealCount;
    unsigned int daysToMileStone = -1;
    switch (averageType)
    {
    case TMilestoneAverage::E6MonthAverage:
        {
        float ratio = numberOfPeals / float(182.625);
        daysToMileStone = int(pealsRequired / ratio);
        milestone.sixMonthAverageDaysToMilestone = daysToMileStone;
        milestone.pealsInSixMonthWindow =(size_t)numberOfPeals;
        }
        break;
    case TMilestoneAverage::E12MonthAverage:
        {
        float ratio = numberOfPeals / float(365.25);
        daysToMileStone = int(pealsRequired / ratio);
        milestone.twelveMonthAverageDaysToMilestone = daysToMileStone;
        milestone.pealsInTwelveMonthWindow = (size_t)numberOfPeals;
        }
        break;
    default:
    case TMilestoneAverage::EAllTimeAverage:
        {
        unsigned int numberOfDaysBetween = firstPealDate.NumberOfDaysUntil(startDate);
        float ratio = numberOfPeals / numberOfDaysBetween;
        daysToMileStone = int(pealsRequired / ratio);
        milestone.allTimeDaysToMilestone = daysToMileStone;
        milestone.firstPealDate = firstPealDate;
        milestone.pealsInAllTimeWindow = (size_t)numberOfPeals;
        }
        break;
    }
}

void
PealDatabase::RemoveUsedPictureIds(std::set<Duco::ObjectId>& unusedPictureIds) const
{
    // Remove used towers / methods and rings from lists.
    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        std::set<Duco::ObjectId>::iterator it = unusedPictureIds.find(nextPeal->PictureId());
        if (it != unusedPictureIds.end())
        {
            unusedPictureIds.erase(it);
        }
        if (unusedPictureIds.size() == 0)
            return;
    }
}

void
PealDatabase::RemoveUsedIds(std::set<Duco::ObjectId>& unusedMethods,
                            std::set<Duco::ObjectId>& unusedRingers,
                            std::set<Duco::ObjectId>& unusedTowers,
                            std::multimap<Duco::ObjectId, Duco::ObjectId>& unusedRings,
                            std::set<Duco::ObjectId>& unusedMethodSeries,
                            std::set<Duco::ObjectId>& unusedCompositions,
                            std::set<Duco::ObjectId>& unusedAssociations) const
{
    // Remove used towers / methods and rings from lists.
    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Duco::Peal* const>(value);

        // Remove this peals method id.
        set<Duco::ObjectId>::iterator it = unusedMethods.find(nextPeal->MethodId());
        if (it != unusedMethods.end())
        {
            unusedMethods.erase(it);
        }

        // Remove this peals method series id.
        it = unusedMethodSeries.find(nextPeal->SeriesId());
        if (it != unusedMethodSeries.end())
        {
            unusedMethodSeries.erase(it);
        }

        // Remove this peals composition id.
        it = unusedCompositions.find(nextPeal->CompositionId());
        if (it != unusedCompositions.end())
        {
            unusedCompositions.erase(it);
        }

        // Remove this peals association id.
        it = unusedAssociations.find(nextPeal->AssociationId());
        if (it != unusedAssociations.end())
        {
            unusedAssociations.erase(it);
        }

        // Remove this peals tower id.
        it = unusedTowers.find(nextPeal->TowerId());
        if (it != unusedTowers.end())
        {
            unusedTowers.erase(it);
        }

        // Remove this peals ring ids for this tower id 
        pair<multimap<Duco::ObjectId, Duco::ObjectId>::iterator,multimap<Duco::ObjectId, Duco::ObjectId>::iterator> ringsIt = unusedRings.equal_range (nextPeal->TowerId());
        bool found = false;
        multimap<Duco::ObjectId, Duco::ObjectId>::iterator ringsIt2 = ringsIt.first;
        while (!found && ringsIt2 != ringsIt.second)
        {
            if (ringsIt2->first == nextPeal->TowerId() && ringsIt2->second == nextPeal->RingId())
            {
                unusedRings.erase(ringsIt2);
                found = true;
            }
            else
            {
                ++ringsIt2;
            }
        }

        // Remove used ringer ids
        std::set<Duco::ObjectId> thisPealRingerIds;
        nextPeal->RingerIds(thisPealRingerIds);
        std::set<Duco::ObjectId>::const_iterator ringerIt = thisPealRingerIds.begin();
        while (ringerIt != thisPealRingerIds.end())
        {
            it = unusedRingers.find(*ringerIt);
            if (it != unusedRingers.end())
            {
                unusedRingers.erase(it);
            }
            ++ringerIt;
        }

        // Remove used conductor ids
        std::set<Duco::ObjectId>::const_iterator condIt = nextPeal->Conductors().begin();
        while (condIt != nextPeal->Conductors().end())
        {
            it = unusedRingers.find(*condIt);
            if (it != unusedRingers.end())
            {
                unusedRingers.erase(it);
            }
            ++condIt;
        }
    }
}

bool
PealDatabase::RemoveDuplicateRings(ProgressCallback*const callback)
{
    return towersDatabase.RemoveDuplicateRings(*this, callback);
    // see UpdateRingIds for call back from towers database
}

bool
PealDatabase::UpdateRingIds(const Duco::ObjectId& theTowerId, const std::map<Duco::ObjectId, Duco::ObjectId>& duplicateRingReplacements)
{
    bool pealUpdated (false);
    std::vector<Duco::ObjectId> ringIdsToDelete;
    for (auto const& [key, value] : listOfObjects)
    {
        Peal* nextPeal = static_cast<Peal*>(value);

        if (nextPeal->TowerId() == theTowerId)
        {
            std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator it2 = duplicateRingReplacements.find(nextPeal->RingId());
            if (it2 != duplicateRingReplacements.end())
            {
                nextPeal->SetRingId(it2->second);
                SetDataChanged();
                pealUpdated = true;
                ringIdsToDelete.push_back(it2->first);
            }
        }
    }
    towersDatabase.DeleteRings(theTowerId, ringIdsToDelete);
        
    return pealUpdated;
}

bool
PealDatabase::ReplaceAssociation(ProgressCallback* callback, const Duco::ObjectId& oldAssociationId, const Duco::ObjectId& newAssociationId)
{
    const float total = float(listOfObjects.size());
    float stepNumber (0);
    bool changesMade (false);
    for (auto const& [key, value] : listOfObjects)
    {
        Peal* nextPeal = static_cast<Peal*>(value);
        ++stepNumber;
        if (nextPeal->AssociationId() == oldAssociationId)
        {
            nextPeal->SetAssociationId(newAssociationId);
            changesMade = true;
            SetDataChanged();
        }

        if (callback != NULL)
        {
            callback->Step(int((stepNumber / total) * float(100)));
        }
    }
    return changesMade;
}


//***********************************************************************
// Sorting / reordering peals
//***********************************************************************

bool
PealDatabase::SortPeals(Duco::RenumberProgressCallback* callback)
{
    bool anyChangesMade (false);
    bool changesMadeThisIt (false);
    unsigned int debugCount (1);
    
    if (callback != NULL)
    {
        callback->RenumberInitialised(RenumberProgressCallback::ESortingPeals);
    }
    DUCO_OBJECTS_INDEX::iterator it1 = objectsIndex.begin();
    while (it1 != objectsIndex.end())
    {
        DUCO_OBJECTS_INDEX::iterator it2 = it1++;
        if (it1 != objectsIndex.end())
        {
            DUCO_OBJECTS_CONTAINER::iterator peal1 = listOfObjects.find(it1->Id());
            DUCO_OBJECTS_CONTAINER::iterator peal2 = listOfObjects.find(it2->Id());
            if (((Duco::Peal*)(peal1->second))->operator<(*(Duco::Peal*)peal2->second))
            {
                if (SwapPeals(it1->Id(), it2->Id(), -1, false))
                {
                    changesMadeThisIt = true;
                    anyChangesMade = true;
                    SetDataChanged();
                    if (it1 != objectsIndex.end())
                    {
                        ++it1;
                    }
                }
            }
        }
        if (it1 == objectsIndex.end() && changesMadeThisIt /*&& debugCount < listOfPeals.size()*/)
        {
            ++debugCount;
            changesMadeThisIt = false;
            it1 = objectsIndex.begin();
            if (callback != NULL)
            {
                callback->RenumberStep(debugCount, NumberOfObjects());
            }
        }
    }
    RebuildIndex();

    return anyChangesMade;
}

bool
PealDatabase::SwapPeals(const Duco::ObjectId& pealId1, const Duco::ObjectId& pealId2, unsigned int selectedIndex, bool rebuildIndex)
{
    DUCO_OBJECTS_CONTAINER::iterator it1 = listOfObjects.find(pealId1);
    DUCO_OBJECTS_CONTAINER::iterator it2 = listOfObjects.find(pealId2);

    return SwapPeals(it1, it2, selectedIndex, rebuildIndex);
}

bool
PealDatabase::SwapPeals(DUCO_OBJECTS_CONTAINER::iterator& it1, DUCO_OBJECTS_CONTAINER::iterator it2, unsigned int selectedIndex, bool rebuildIndex)
{
    if (it1 == listOfObjects.end() || it2 == listOfObjects.end())
    {
        return false;
    }

    Peal* oldFirstPeal = static_cast<Peal*>(it1->second);
    Peal* oldSecondPeal = static_cast<Peal*>(it2->second);
    Duco::ObjectId pealId1 = oldFirstPeal->Id();
    Duco::ObjectId pealId2 = oldSecondPeal->Id();

    oldFirstPeal->SetId(pealId2);
    oldSecondPeal->SetId(pealId1);

    it1->second = oldSecondPeal;
    it2->second = oldFirstPeal;

    if (selectedIndex != -1)
    {
        oldFirstPeal->SetDayOrder(selectedIndex+1);
        oldSecondPeal->SetDayOrder(selectedIndex);
    }
    SetDataChanged();
    if (rebuildIndex)
    {
        RebuildIndex();
    }
    return true;
}

bool
PealDatabase::RenumberRingersInPeals(const std::map<Duco::ObjectId, Duco::ObjectId>& newRingerIds, Duco::RenumberProgressCallback* callback)
{
    bool changesMade = false;
    size_t progress = 0;
    for (auto const& [key, value] : listOfObjects)
    {
        Peal* nextPeal = static_cast<Peal*>(value);
        if (nextPeal->ReplaceRingers(newRingerIds))
        {
            SetDataChanged();
            changesMade = true;
        }
        callback->RenumberStep(progress++, listOfObjects.size());
    }
    return changesMade;
}

bool
PealDatabase::RenumberMethodsInPeals(const std::map<Duco::ObjectId, Duco::ObjectId>& newMethodIds, Duco::RenumberProgressCallback* callback)
{
    bool changesMade (false);
    unsigned int count (0);
    for (auto const& [key, value] : listOfObjects)
	{
		Peal* nextPeal = static_cast<Peal*>(value);

		Duco::ObjectId oldMethodId = nextPeal->MethodId();
		std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator oldMethodIdIt = newMethodIds.find(oldMethodId);
		if (oldMethodIdIt != newMethodIds.end())
		{
			Duco::ObjectId newMethodId = oldMethodIdIt->second;
			if (newMethodId != oldMethodId)
			{
				nextPeal->SetMethodId(newMethodId);
				SetDataChanged();
				changesMade = true;
			}
		}
		if (callback != NULL)
		{
			callback->RenumberStep(++count, NumberOfObjects());
		}
    }
    return changesMade;
}

bool
PealDatabase::RenumberCompositionsInPeals(const std::map<Duco::ObjectId, Duco::ObjectId>& newCompositionsIds, Duco::RenumberProgressCallback* callback)
{
    bool changesMade (false);
    unsigned int count (0);
    for (auto const& [key, value] : listOfObjects)
    {
        Peal* nextPeal = static_cast<Peal*>(value);

        Duco::ObjectId oldCompositionId = nextPeal->CompositionId();
        std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator oldCompositionIdIt = newCompositionsIds.find(oldCompositionId);
        if (oldCompositionIdIt != newCompositionsIds.end())
        {
            Duco::ObjectId newCompositionId = oldCompositionIdIt->second;
            if (newCompositionId != oldCompositionId)
            {
                nextPeal->SetCompositionId(newCompositionId);
                SetDataChanged();
                changesMade = true;
            }
        }
        callback->RenumberStep(++count, NumberOfObjects());
    }
    return changesMade;
}

bool
PealDatabase::RenumberMethodSeriesInPeals(const std::map<Duco::ObjectId, Duco::ObjectId>& newMethodSeriesIds, Duco::RenumberProgressCallback* callback)
{
    bool changesMade (false);
    unsigned int count (0);
    for (auto const& [key, value] : listOfObjects)
    {
        Peal* nextPeal = static_cast<Peal*>(value);

        Duco::ObjectId oldSeriesId = nextPeal->SeriesId();
        std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator oldMethodSeriesIdIt = newMethodSeriesIds.find(oldSeriesId);
        if (oldMethodSeriesIdIt != newMethodSeriesIds.end())
        {
            Duco::ObjectId newSeriesId = oldMethodSeriesIdIt->second;
            if (newSeriesId != oldSeriesId)
            {
                nextPeal->SetSeriesId(newSeriesId);
                SetDataChanged();
                changesMade = true;
            }
        }
        callback->RenumberStep(++count, NumberOfObjects());
    }
    return changesMade;
}

void 
PealDatabase::RenumberTowersInPeals(const std::map<Duco::ObjectId, Duco::ObjectId>& newTowerIds, Duco::RenumberProgressCallback* callback)
{
    unsigned int count (0);
    for (auto const& [key, value] : listOfObjects)
    {
        Peal* nextPeal = static_cast<Peal*>(value);

        Duco::ObjectId oldTowerId = nextPeal->TowerId();
        std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator newTowerIdIt = newTowerIds.find(oldTowerId);
        if (newTowerIdIt != newTowerIds.end() && oldTowerId != newTowerIdIt->second)
        {
            nextPeal->SetTowerId(newTowerIdIt->second);
            SetDataChanged();
        }
        callback->RenumberStep(++count, NumberOfObjects());
    }
}

void
PealDatabase::RenumberRingsInPeals(const Duco::ObjectId& towerId, std::map<Duco::ObjectId, Duco::ObjectId> oldRingIdsToNewRingIds)
{
    for (auto const& [key, value] : listOfObjects)
    {
        Peal* nextPeal = static_cast<Peal*>(value);

        if (nextPeal->TowerId() == towerId)
        {
            std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator ringIt = oldRingIdsToNewRingIds.find(nextPeal->RingId());
            if (ringIt != oldRingIdsToNewRingIds.end())
            {
                nextPeal->SetRingId(ringIt->second);
            }
        }
    }
}

bool
PealDatabase::RebuildObjects(Duco::RenumberProgressCallback* callback, Duco::RingingDatabase& database)
{
    bool changesmade (false);

    if (callback != NULL)
    {
        callback->RenumberInitialised(RenumberProgressCallback::ERenumberPeals);
    }
    unsigned int lastIndex = 0;
    for (auto const& it : objectsIndex)
    {
        Duco::ObjectId currentIndex = it.Id();
        if (currentIndex != ((size_t)lastIndex + 1))
        {
            DUCO_OBJECTS_CONTAINER::iterator currentPealIt = listOfObjects.find(it.Id());
            Peal* currentPeal = static_cast<Peal*>(currentPealIt->second);
            currentPeal->SetId(lastIndex+1);
            listOfObjects.erase(currentPealIt);
            std::pair<ObjectId, RingingObject*> newObject(currentPeal->Id(), currentPeal);
            listOfObjects.insert(newObject);
            SetDataChanged();
            changesmade = true;
        }
        if (callback != NULL)
        {
            callback->RenumberStep(lastIndex, listOfObjects.size());
        }
        ++lastIndex;
    }
    RebuildIndex();

    changesmade |= SortPeals(callback);

    return changesmade;
}

bool
PealDatabase::MatchObject(const Duco::RingingObject& object, const Duco::TSearchArgument& searchArg, const Duco::RingingDatabase& database) const
{
    if (object.ObjectType() != TObjectType::EPeal)
        return false;
    return searchArg.Match(static_cast<const Duco::Peal&>(object), database);
}

bool
PealDatabase::ValidateObject(const Duco::RingingObject& object, const Duco::TSearchValidationArgument& searchArg, const std::set<Duco::ObjectId>& idsToCheckAgainst, const Duco::RingingDatabase& database) const
{
    if (object.ObjectType() != TObjectType::EPeal)
        return false;
    return searchArg.Match(static_cast<const Duco::Peal&>(object), idsToCheckAgainst, database);
}

Duco::PealLengthInfo
PealDatabase::PerformanceInfo(const Duco::StatisticFilters& filters, const Duco::ObjectId& upToAndIncludePealId) const
{
    Duco::PealLengthInfo count;

    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        if (filters.Match(*nextPeal) && (nextPeal->Id() <= upToAndIncludePealId || !upToAndIncludePealId.ValidId()))
        {
            count.AddPeal(*nextPeal);
        }
    }
    return count;
}

Duco::PealLengthInfo
PealDatabase::PerformanceInfo(const Duco::StatisticFilters& filters, const std::set<Duco::ObjectId>& performanceIds) const
{
    Duco::PealLengthInfo count;

    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        if (filters.Match(*nextPeal) && performanceIds.find(nextPeal->Id()) != performanceIds.end())
        {
            count.AddPeal(*nextPeal);
        }
    }
    return count;
}

bool
PealDatabase::AnyMatch(const Duco::StatisticFilters& filters) const
{
    for (auto const& [key, value] : listOfObjects)
    {
        const Duco::Peal* thePeal = static_cast<const Duco::Peal*>(value);
        if (filters.Match(*thePeal))
        {
            return true;
        }
    }
    return false;
}

Duco::PealLengthInfo
PealDatabase::AllMatches(const Duco::StatisticFilters& filters) const
{
    std::set<ObjectId> performanceIds;
    return AllMatches(filters, performanceIds);
}

Duco::PealLengthInfo
PealDatabase::AllMatches(const Duco::StatisticFilters& filters, std::set<ObjectId>& performanceIds) const
{
    performanceIds.clear();
    PealLengthInfo info;
    bool methodFound(false);
    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* thisPeal = static_cast<const Peal*>(value);
        if (filters.Match(*thisPeal))
        {
            info.AddPeal(*thisPeal);
            performanceIds.insert(thisPeal->Id());
        }
    }
    return info;
}

bool
PealDatabase::GetFurthestPeals(const Duco::StatisticFilters& filters, Duco::ObjectId& mostNortherlyId, Duco::ObjectId& mostSoutherlyId, Duco::ObjectId& mostEasterlyId, Duco::ObjectId& mostWesterlyId) const
{
    mostNortherlyId.ClearId();
    mostSoutherlyId.ClearId();
    mostEasterlyId.ClearId();
    mostWesterlyId.ClearId();

    float mostNortherlyPosition(-90);
    float mostSoutherlyPosition(90);
    float mostEasterlyPosition(-180);
    float mostWesterlyPosition(180);

    const Duco::Peal* mostNortherlyPeal = NULL;
    const Duco::Peal* mostSoutherlyPeal = NULL;
    const Duco::Peal* mostEasterlyPeal = NULL;
    const Duco::Peal* mostWesterlyPeal = NULL;

    bool pealFound = false;
    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        if (filters.Match(*nextPeal))
        {
            const Tower* const theTower = towersDatabase.FindTower(nextPeal->TowerId());
            if (theTower != NULL && theTower->PositionValid())
            {
                float thisLong = DucoEngineUtils::ToFloat(theTower->Longitude());
                float thisLat = DucoEngineUtils::ToFloat(theTower->Latitude());
                CheckDetailsForEarlierMostDistantPeal(*nextPeal, thisLong, mostEasterlyPosition, mostEasterlyPeal, true);
                CheckDetailsForEarlierMostDistantPeal(*nextPeal, thisLong, mostWesterlyPosition, mostWesterlyPeal, false);
                CheckDetailsForEarlierMostDistantPeal(*nextPeal, thisLat, mostNortherlyPosition, mostNortherlyPeal, true);
                CheckDetailsForEarlierMostDistantPeal(*nextPeal, thisLat, mostSoutherlyPosition, mostSoutherlyPeal, false);
                pealFound = true;
            }
        }
    }
    if (pealFound)
    {
        mostNortherlyId = mostNortherlyPeal->Id();
        mostSoutherlyId = mostSoutherlyPeal->Id();
        mostEasterlyId = mostEasterlyPeal->Id();
        mostWesterlyId = mostWesterlyPeal->Id();
    }
    return mostNortherlyId.ValidId() && mostEasterlyId.ValidId() && mostSoutherlyId.ValidId() && mostWesterlyId.ValidId();
}

void
PealDatabase::CheckDetailsForEarlierMostDistantPeal(const Peal& nextPeal, const float& thisPealPosition, float& currentFurthestPosition, const Duco::Peal*& currentPeal, bool greaterThan) const
{
    if (currentPeal == NULL)
    {
        currentPeal = &nextPeal;
    }
    else
    {
        bool further(false);
        if (greaterThan)
        {
            further = thisPealPosition > currentFurthestPosition;
        }
        else
        {
            further = thisPealPosition < currentFurthestPosition;
        }
        if (further)
        {
            currentPeal = &nextPeal;
            currentFurthestPosition = thisPealPosition;
        }
        else if (thisPealPosition == currentFurthestPosition && nextPeal < *currentPeal)
        {
            currentPeal = &nextPeal;
        }
    }
}

void // ringerId, <year, count>.
PealDatabase::GetTopRingersPealCountPerYear(const Duco::StatisticFilters& filters, bool combineLinked, size_t noOfRingers, std::map<ObjectId, std::map<unsigned int, Duco::PealLengthInfo> >& sortedPealCounts) const
{
    sortedPealCounts.clear();

    // First work out which ringers are in the top X
    std::set<Duco::ObjectId> topRingerIds;
    {
        std::multimap<Duco::PealLengthInfo, Duco::ObjectId> sortedRingerIds;
        GetTopRingers(filters, noOfRingers, false, sortedRingerIds);
        std::multimap<Duco::PealLengthInfo, Duco::ObjectId>::const_iterator topRingerIt = sortedRingerIds.begin();
        while (topRingerIt != sortedRingerIds.end())
        {
            topRingerIds.insert(topRingerIt->second);
            ++topRingerIt;
        }
    }

    unsigned int firstYear = numeric_limits<unsigned int>::max();
    unsigned int lastYear = 0;
    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        if (filters.Match(*nextPeal))
        {
            const PerformanceDate& pealDate = nextPeal->Date();

            std::set<Duco::ObjectId> thisPealRingerIds;
            nextPeal->RingerIds(thisPealRingerIds);
            std::set<Duco::ObjectId>::const_iterator ringerIt = thisPealRingerIds.begin();
            while (ringerIt != thisPealRingerIds.end())
            {
                std::set<Duco::ObjectId>::const_iterator ringerInTopX = topRingerIds.find(*ringerIt);
                if (ringerInTopX != topRingerIds.end())
                {
                    Duco::PealLengthInfo newInfo(*nextPeal);
                    std::map<ObjectId, std::map<unsigned int, Duco::PealLengthInfo> >::iterator it3 = sortedPealCounts.find(*ringerIt);
                    if (it3 == sortedPealCounts.end())
                    {
                        std::pair<unsigned int, Duco::PealLengthInfo> newObject (pealDate.Year(), newInfo);
                        std::pair<ObjectId, std::map<unsigned int, Duco::PealLengthInfo> > newObject2;
                        newObject2.first = *ringerIt;
                        newObject2.second.insert(newObject);
                        sortedPealCounts.insert(newObject2);
                        if (pealDate.Year() > lastYear)
                            lastYear = pealDate.Year();
                        if (pealDate.Year() < firstYear)
                            firstYear = pealDate.Year();
                    }
                    else
                    {
                        std::map<unsigned int, Duco::PealLengthInfo>::iterator it4 = it3->second.find(pealDate.Year());
                        if (it4 == it3->second.end())
                        {
                            std::pair<unsigned int, Duco::PealLengthInfo> newObject (pealDate.Year(), newInfo);
                            it3->second.insert(newObject);
                            if (pealDate.Year() > lastYear)
                                lastYear = pealDate.Year();
                            if (pealDate.Year() < firstYear)
                                firstYear = pealDate.Year();
                        }
                        else
                        {
                            it4->second += newInfo;
                        }
                    }
                }
                ++ringerIt;
            }
        }
    }

    // Adding missing years with zero.
    std::map<ObjectId, std::map<unsigned int, Duco::PealLengthInfo> >::iterator checkYearsIt = sortedPealCounts.begin();
    while (checkYearsIt != sortedPealCounts.end())
    {
        for (unsigned int count = firstYear; count <= lastYear; ++count)
        {
            Duco::PealLengthInfo newInfo;
            std::pair<unsigned int, Duco::PealLengthInfo> newObject(count, newInfo);
            checkYearsIt->second.insert(newObject);
        }
        ++checkYearsIt;
    }
    if (combineLinked)
    {
        ringersDatabase.CombineLinkedRingers(sortedPealCounts);
    }
}

void // stage <year, count>
PealDatabase::GetStagesCountByYear(const Duco::StatisticFilters& filters, std::map<unsigned int, std::map<unsigned int, Duco::PealLengthInfo> >&  stageYearCounts) const
{
    unsigned int firstYear = numeric_limits<unsigned int>::max();
    unsigned int lastYear = 0;
    unsigned int firstStage = numeric_limits<unsigned int>::max();
    unsigned int lastStage = 0;
    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        if (filters.Match(*nextPeal))
        {
            const PerformanceDate& pealDate = nextPeal->Date();
            unsigned int pealStage (nextPeal->NoOfBellsRung(towersDatabase, methodsDatabase));

            Duco::PealLengthInfo newInfo(*nextPeal);
            std::map<unsigned int, std::map<unsigned int, Duco::PealLengthInfo> >::iterator it3 = stageYearCounts.find(pealStage);
            if (it3 == stageYearCounts.end())
            {
                std::pair<unsigned int, Duco::PealLengthInfo> newObject (pealDate.Year(), newInfo);
                std::pair<unsigned int, std::map<unsigned int, Duco::PealLengthInfo> > newObject2;
                newObject2.first = pealStage;
                newObject2.second.insert(newObject);
                stageYearCounts.insert(newObject2);
                if (pealDate.Year() > lastYear)
                    lastYear = pealDate.Year();
                if (pealDate.Year() < firstYear)
                    firstYear = pealDate.Year();
                if (pealStage > lastStage)
                    lastStage = pealStage;
                if (pealStage < firstStage)
                    firstStage = pealStage;
            }
            else
            {
                std::map<unsigned int, Duco::PealLengthInfo>::iterator it4 = it3->second.find(pealDate.Year());
                if (it4 == it3->second.end())
                {
                    std::pair<unsigned int, Duco::PealLengthInfo> newObject (pealDate.Year(), newInfo);
                    it3->second.insert(newObject);
                    if (pealDate.Year() > lastYear)
                        lastYear = pealDate.Year();
                    if (pealDate.Year() < firstYear)
                        firstYear = pealDate.Year();
                }
                else
                {
                    it4->second += *nextPeal;
                }
            }
        }
    }

    for (unsigned int stageCount = firstStage; stageCount <= lastStage; ++stageCount)
    {
        std::map<unsigned int, Duco::PealLengthInfo> newYear;
        std::pair<unsigned int, std::map<unsigned int, Duco::PealLengthInfo> > newObject(stageCount, newYear);
        stageYearCounts.insert(newObject);
    }

    std::map<unsigned int, std::map<unsigned int, Duco::PealLengthInfo> >::iterator checkStagesIt = stageYearCounts.begin();
    while (checkStagesIt != stageYearCounts.end())
    {
        for (unsigned int yearCount = firstYear; yearCount <= lastYear; ++yearCount)
        {
            Duco::PealLengthInfo newInfo;
            std::pair<unsigned int, Duco::PealLengthInfo> newObject(yearCount, newInfo);
            checkStagesIt->second.insert(newObject);
        }
        ++checkStagesIt;
    }
}

void // tower id <year, count>
PealDatabase::GetTopTowersPealCountPerYear(const Duco::StatisticFilters& filters, size_t noOfTowers, std::map<ObjectId, std::map<unsigned int, Duco::PealLengthInfo> >& sortedPealCounts) const
{
    sortedPealCounts.clear();

    // First work out which towers are in the top X
    std::set<ObjectId> topTowerIds;
    {
        std::multimap<Duco::PealLengthInfo, ObjectId> sortedTowerIds;
        GetTopTowers(filters, noOfTowers, sortedTowerIds);
        std::multimap<Duco::PealLengthInfo, ObjectId>::const_iterator topTowerIt = sortedTowerIds.begin();
        while (topTowerIt != sortedTowerIds.end())
        {
            topTowerIds.insert(topTowerIt->second);
            ++topTowerIt;
        }
        
    }
    unsigned int firstYear = numeric_limits<unsigned int>::max();
    unsigned int lastYear = 0;
    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        if (filters.Match(*nextPeal))
        {
            const PerformanceDate& pealDate = nextPeal->Date();

            std::set<ObjectId>::const_iterator towerInTopX = topTowerIds.find(nextPeal->TowerId());
            if (towerInTopX != topTowerIds.end())
            {
                Duco::PealLengthInfo newInfo(*nextPeal);
                std::map<ObjectId, std::map<unsigned int, Duco::PealLengthInfo> >::iterator it3 = sortedPealCounts.find(nextPeal->TowerId());
                if (it3 == sortedPealCounts.end())
                {
                    std::pair<unsigned int, Duco::PealLengthInfo> newObject (pealDate.Year(), newInfo);
                    std::pair<Duco::ObjectId, std::map<unsigned int, Duco::PealLengthInfo> > newObject2;
                    newObject2.first = nextPeal->TowerId();
                    newObject2.second.insert(newObject);
                    sortedPealCounts.insert(newObject2);
                    if (pealDate.Year() > lastYear)
                        lastYear = pealDate.Year();
                    if (pealDate.Year() < firstYear)
                        firstYear = pealDate.Year();
                }
                else
                {
                    std::map<unsigned int, Duco::PealLengthInfo>::iterator it4 = it3->second.find(pealDate.Year());
                    if (it4 == it3->second.end())
                    {
                        std::pair<unsigned int, Duco::PealLengthInfo> newObject (pealDate.Year(), newInfo);
                        it3->second.insert(newObject);
                        if (pealDate.Year() > lastYear)
                            lastYear = pealDate.Year();
                        if (pealDate.Year() < firstYear)
                            firstYear = pealDate.Year();
                    }
                    else
                    {
                        it4->second += newInfo;
                    }
                }
            }
        }
    }

    std::map<ObjectId, std::map<unsigned int, Duco::PealLengthInfo> >::iterator checkYearsIt = sortedPealCounts.begin();
    while (checkYearsIt != sortedPealCounts.end())
    {
        for (unsigned int count = firstYear; count <= lastYear; ++count)
        {
            std::pair<unsigned int, Duco::PealLengthInfo> newObject(count, Duco::PealLengthInfo());
            checkYearsIt->second.insert(newObject);
        }
        ++checkYearsIt;
    }
}

void
PealDatabase::GetUniqueCounts(const Duco::StatisticFilters& filters, std::map<unsigned int, size_t>& theTowerCounts, std::map<unsigned int, size_t>& theRingerCounts, std::map<unsigned int, size_t>& theMethodCounts) const
{
    theTowerCounts.clear();
    theRingerCounts.clear();
    theMethodCounts.clear();

    //First get all tower and ringer ids used each year thus: year/ringerId or year/Towerid
    YearIdGroup ringerCounts;
    YearIdGroup towerCounts;
    YearIdGroup methodCounts;

    unsigned int firstYear = numeric_limits<unsigned int>::max();
    unsigned int lastYear (0);

    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        if (filters.Match(*nextPeal))
        {
            towerCounts.Add(nextPeal->Date().Year(), nextPeal->TowerId());
            methodCounts.Add(nextPeal->Date().Year(), nextPeal->MethodId());

            std::set<ObjectId> ringerIds;
            nextPeal->RingerIds(ringerIds);
            ringerCounts.Add(nextPeal->Date().Year(), ringerIds);

            firstYear = min(firstYear, nextPeal->Date().Year());
            lastYear = max(lastYear, nextPeal->Date().Year());
        }
    }

    for (unsigned int i (firstYear); i <= lastYear; ++i)
    {
        std::pair<unsigned int, size_t> newTowerCount(i, towerCounts.Count(i));
        theTowerCounts.insert(newTowerCount);
        std::pair<unsigned int, size_t> newRingerCount(i, ringerCounts.Count(i));
        theRingerCounts.insert(newRingerCount);
        std::pair<unsigned int, size_t> newMethodCount(i, methodCounts.Count(i));
        theMethodCounts.insert(newMethodCount);
    }
}

unsigned int
PealDatabase::LowestValidOrderNumber(const ObjectId& towerId, const ObjectId& ringId) const
{
    unsigned int bellsInRing (towersDatabase.NoOfBellsInRing(towerId, ringId));

    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        if (nextPeal->TowerId() == towerId && nextPeal->RingId() == ringId)
        {
            bellsInRing = min(bellsInRing, nextPeal->NoOfBellsRung(towersDatabase, methodsDatabase));
        }
    }
    return bellsInRing;
}

bool
PealDatabase::RebuildRecommended() const
{
    bool rebuildRecommended (RingingObjectDatabase::RebuildRecommended());

    Duco::PerformanceDate lastPealDate;
    lastPealDate.ResetToEarliest();
    DUCO_OBJECTS_INDEX::const_iterator it = objectsIndex.begin();
    while (!rebuildRecommended && it != objectsIndex.end())
    {
        DUCO_OBJECTS_CONTAINER::const_iterator pealIt = listOfObjects.find(it->Id());
        if (pealIt == listOfObjects.end())
        {
            return true;
        }

        const Peal* const nextPeal = static_cast<const Peal* const>(pealIt->second);
        if (nextPeal->Date() < lastPealDate)
        {
            rebuildRecommended = true;
        }
        else
            lastPealDate = nextPeal->Date();
        ++it;
    }

    return rebuildRecommended;
}

void
PealDatabase::RingerGenderStats(const Duco::StatisticFilters& filters, float& percentMale, float& percentFemale, float& percentNotSet, float& percentNonHuman,
                                float& percentMaleConductors, float& percentFemaleConductors, float& percentNotSetConductors, float& percentNonHumanConductors) const
{ 
    percentMale = 0;
    percentFemale = 0;
    percentNotSet = 0;
    percentNonHuman = 0;
    percentMaleConductors = 0;
    percentFemaleConductors = 0;
    percentNotSetConductors = 0;
    percentNonHumanConductors = 0;

    float male (0);
    float female (0);
    float notSet (0);
    float nonHuman(0);
    float total (0);
    float maleConductors (0);
    float femaleConductors (0);
    float notSetConductors (0);
    float nonHumanConductors(0);
    float totalConductors (0);

    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);

        if (filters.Match(*nextPeal))
        {
            std::set<ObjectId> thisPealRingerIds;
            nextPeal->RingerIds(thisPealRingerIds);

            std::set<ObjectId>::const_iterator ringerIt = thisPealRingerIds.begin();
            while (ringerIt != thisPealRingerIds.end())
            {
                const Ringer* const theRinger = ringersDatabase.FindRinger(*ringerIt);
                if (theRinger != NULL)
                {
                    bool conducted(nextPeal->ContainsConductor(*ringerIt));
                    if (theRinger->NonHuman())
                    {
                        ++nonHuman;
                        if (conducted)
                        {
                            ++nonHumanConductors;
                            ++totalConductors;
                        }
                    }
                    else if (theRinger->Male())
                    {
                        ++male;
                        if (conducted)
                        {
                            ++maleConductors;
                            ++totalConductors;
                        }
                    }
                    else if (theRinger->Female())
                    {
                        ++female;
                        if (conducted)
                        {
                            ++femaleConductors;
                            ++totalConductors;
                        }
                    }
                    else
                    {
                        ++notSet;
                        if (conducted)
                        {
                            ++notSetConductors;
                            ++totalConductors;
                        }
                    }

                    ++total;
                }
                ++ringerIt;
            }
        }
    }
    if (total > 0)
    {
        if (male > 0)
            percentMale = (male / total) * float(100);
        if (female > 0)
            percentFemale = (female / total) * float(100);
        if (notSet > 0)
            percentNotSet = (notSet / total) * float(100);
        if (nonHuman > 0)
            percentNonHuman = (nonHuman / total) * float(100);
    }
    if (totalConductors > 0)
    {
        if (maleConductors > 0)
            percentMaleConductors = (maleConductors / totalConductors) * float(100);
        if (femaleConductors > 0)
            percentFemaleConductors = (femaleConductors / totalConductors) * float(100);
        if (notSetConductors > 0)
            percentNotSetConductors = (notSetConductors / totalConductors) * float(100);
        if (nonHumanConductors > 0)
            percentNonHumanConductors = (nonHumanConductors / totalConductors) * float(100);
    }
}

void
PealDatabase::GetMethodSeriesPealCount(const Duco::StatisticFilters& filters, std::map<ObjectId, Duco::PealLengthInfo>& sortedMethodSeriesIds) const
{
    sortedMethodSeriesIds.clear();

    // id / count
    for (auto const& [key, value] : listOfObjects)
    {
        bool asStrapper (false);
        Peal* thisPeal = static_cast<Peal*>(value);
        if (filters.Match(*thisPeal) && thisPeal->SeriesId().ValidId())
        {
            Duco::PealLengthInfo newInfo(*thisPeal);
            pair<ObjectId, Duco::PealLengthInfo> newObject(thisPeal->SeriesId(), newInfo);
            pair<map<ObjectId, Duco::PealLengthInfo>::iterator, bool> methodInserted = sortedMethodSeriesIds.insert(newObject);
            if (!methodInserted.second)
            {
                methodInserted.first->second += newInfo;
            }
        }
    }

    Duco::ObjectId objectId;
    unsigned int stage;
    if (!filters.Stage(stage))
    {
        stage = -1;
    }
    if (!filters.Tower(objectId) && !filters.Method(objectId))
    {
        // Add the methods that no peals have been rung in yet.
        seriesDatabase.AddMissingSeries(sortedMethodSeriesIds, stage);
    }
}

void
PealDatabase::GetCompositionsPealCount(std::map<Duco::ObjectId, Duco::PealLengthInfo>& sortedCompositionIds) const
{
    sortedCompositionIds.clear();

    for (auto const& [key, value] : listOfObjects)
    {
        const Duco::Peal* thePeal = static_cast<const Duco::Peal*>(value);

        if (!thePeal->Withdrawn() && thePeal->CompositionId().ValidId())
        {
            Duco::PealLengthInfo newInfo(*thePeal);
            pair<ObjectId, Duco::PealLengthInfo> newObject(thePeal->CompositionId(), newInfo);
            pair<std::map<ObjectId, Duco::PealLengthInfo>::iterator, bool> compositionInserted = sortedCompositionIds.insert(newObject);
            if (!compositionInserted.second)
            {
                compositionInserted.first->second += newInfo;
            }
        }
    }

    compositionsDatabase.AddMissingCompositions(sortedCompositionIds);
}

void
PealDatabase::GetBellsRung(const Duco::StatisticFilters& filters, std::map<unsigned int, std::map<unsigned int, Duco::PealLengthInfo> >& bellRungCount) const
{
    bellRungCount.clear();
    for (auto const& [key, value] : listOfObjects)
    {
        const Duco::Peal* thePeal = static_cast<const Duco::Peal*>(value);
        if (filters.Match(*thePeal))
        {
            const Tower* const theTower = towersDatabase.FindTower(thePeal->TowerId());
            if (theTower != NULL)
            {
                std::set<unsigned int> bellsRung;
                bool asStrapper (false);
                if (thePeal->BellRung(filters.RingerId(), bellsRung, asStrapper))
                {
                    const Ring* const theRing = theTower->FindRing(thePeal->RingId());
                    if (theRing != NULL)
                    {
                        Duco::PealLengthInfo newInfo(*thePeal);
                        std::set<unsigned int>::const_iterator bellsRungIt = bellsRung.begin();
                        // Bells rung found
                        while (bellsRungIt != bellsRung.end())
                        {
                            // Now add to return values
                            pair<unsigned int, Duco::PealLengthInfo> newPealCount(*bellsRungIt, newInfo);
                            std::pair<unsigned int, std::map<unsigned int, Duco::PealLengthInfo> > newObject;
                            newObject.first = thePeal->NoOfBellsRung(towersDatabase, methodsDatabase);
                            newObject.second.insert(newPealCount);
                            pair<std::map<unsigned int, std::map<unsigned int, Duco::PealLengthInfo> >::iterator, bool> bellsInserted = bellRungCount.insert(newObject);
                            if (!bellsInserted.second)
                            {
                                pair<std::map<unsigned int, Duco::PealLengthInfo>::iterator, bool> bellsInsertedInExistingStage = bellsInserted.first->second.insert(newPealCount);
                                if (!bellsInsertedInExistingStage.second)
                                {
                                    bellsInsertedInExistingStage.first->second += newInfo;
                                }
                            }
                            ++bellsRungIt;
                        }
                    }
                }
            }
        }
    }
}

void
PealDatabase::GetYearCirclingStats(const Duco::StatisticFilters& filters, std::map<PealYearDay, YearCirclingStats, Duco::PealYearDay>& results) const
{
    for (auto const& [key, value] : listOfObjects)
    {
        const Duco::Peal* thePeal = static_cast<const Duco::Peal*>(value);
        if (filters.Match(*thePeal))
        {
            const PerformanceDate& dateTime = thePeal->Date();

            PealYearDay newDay;
            newDay.day = dateTime.Day();
            newDay.month = dateTime.Month();

            YearCirclingStats newCount;
            newCount.count = 1;
            newCount.firstYear = dateTime.Year();

            std::pair<PealYearDay, YearCirclingStats> newObject (newDay, newCount);
            pair< std::map<PealYearDay, YearCirclingStats>::iterator,bool> insertedIt = results.insert(newObject);
            if (!insertedIt.second)
            {
                insertedIt.first->second.count += 1;
                if (insertedIt.first->second.firstYear > dateTime.Year())
                    insertedIt.first->second.firstYear = dateTime.Year();
            }
        }
    }
}

void 
PealDatabase::ExternaliseToCsv(Duco::ImportExportProgressCallback* newCallback, std::wofstream& file) const
{
    size_t count = 0;
    size_t totalObjects = listOfObjects.size();
    ObjectId nextId;
    bool foundObject = FirstObject(nextId);
    while (foundObject)
    {
        ++count;
        float percentageComplete = ((float)count / (float)totalObjects) * 100;
        const Duco::Peal* thePeal = FindPeal(nextId);
        if (thePeal != NULL)
        {
            thePeal->ExportToCsv(file, ringersDatabase, methodsDatabase, towersDatabase, associationsDatabase);
        }

        newCallback->ObjectProcessed(false, TObjectType::EPeal, nextId, (int)percentageComplete, count);
        file << "\n";
        foundObject = NextObject(nextId, true);
    }
}

void
PealDatabase::ExternaliseToXml(Duco::ImportExportProgressCallback* newCallback, XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, bool exportDucoObjects) const
{
    size_t count = 0;
    DOMElement* rootElement = outputFile.getDocumentElement();
    DOMElement* performancesElement = outputFile.createElement(XMLStrL("Performances"));
    rootElement->appendChild(performancesElement);

    ObjectId nextId;
    bool foundObject = FirstObject(nextId);
    while (foundObject)
    {
        ++count;
        const Duco::Peal* thePeal = FindPeal(nextId);
        if (thePeal != NULL)
        {
            thePeal->ExportToXml(outputFile, *performancesElement, exportDucoObjects, ringersDatabase, methodsDatabase, towersDatabase, associationsDatabase);
        }

        newCallback->ObjectProcessed(false, TObjectType::EPeal, nextId, 0, count);
        foundObject = NextObject(nextId, true);
    }
}

void
PealDatabase::ExternaliseToBellboardXml(Duco::ImportExportProgressCallback* newCallback, std::set<Duco::ObjectId> performanceIds, XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile) const
{
    size_t count = 0;
    DOMElement* performancesElement = outputFile.getDocumentElement();
    ObjectId nextId;
    bool foundObject = FirstObject(nextId);
    while (foundObject)
    {
        if (performanceIds.find(nextId) != performanceIds.end())
        {
            const Duco::Peal* thePeal = FindPeal(nextId);
            if (thePeal != NULL)
            {
                thePeal->ExternaliseToBellboardXml(outputFile, *performancesElement, ringersDatabase, methodsDatabase, towersDatabase, associationsDatabase);
            }

            ++count;
            newCallback->ObjectProcessed(false, TObjectType::EPeal, nextId, 0, count);
        }
        foundObject = NextObject(nextId, true);
    }
}

void
PealDatabase::FindPicture(const Duco::ObjectId& pictureId, std::set<Duco::ObjectId>& pealIds) const
{
    pealIds.clear();
    if (pictureId.ValidId())
    {
        for (auto const& [key, value] : listOfObjects)
        {
            const Duco::Peal* thePeal = static_cast<const Duco::Peal*>(value);
            if (thePeal->PictureId() == pictureId)
            {
                pealIds.insert(thePeal->Id());
            }
        }
    }
}

bool
PealDatabase::RemovePictures(const std::set<Duco::ObjectId>& pictureIds)
{
    for (auto const& [key, value] : listOfObjects)
    {
        Duco::Peal* thePeal = static_cast<Duco::Peal*>(value);
        if (pictureIds.find(thePeal->PictureId()) != pictureIds.end())
        {
            thePeal->SetPictureId(KNoId);
            SetDataChanged();
        }
    }

    return true;
}

bool
PealDatabase::FindPictureForTower(const Duco::ObjectId& towerId, Duco::ObjectId& pictureId) const
{
    pictureId.ClearId();

    for (auto const& [key, value] : listOfObjects)
    {
        Duco::Peal* thePeal = static_cast<Duco::Peal*>(value);
        if (thePeal->PictureId().ValidId() && thePeal->TowerId() == towerId)
        {
            if (!pictureId.ValidId())
            {
                pictureId = thePeal->PictureId();
                return true;
            }
            else if (pictureId != thePeal->PictureId())
            {
                return false;
            }
        }
    }
    return false;
}

void
PealDatabase::GetPictureIds(std::map<Duco::ObjectId, Duco::ObjectId>& sortedPictureIds) const
{
    sortedPictureIds.clear();
    for (auto const& [key, value] : listOfObjects)
    {
        const Duco::Peal* thePeal = static_cast<const Duco::Peal*>(value);
        if (thePeal->PictureId().ValidId())
        {
            std::pair<Duco::ObjectId, Duco::ObjectId> newObject (thePeal->Id(), thePeal->PictureId());
            sortedPictureIds.insert(newObject);
        }
    }
}

bool
PealDatabase::RenumberPicturesInPeals(const std::map<Duco::ObjectId, Duco::ObjectId>& sortedPictureIds, Duco::RenumberProgressCallback* callback)
{
    bool anyChangesMade (false);
    
    callback->RenumberInitialised(RenumberProgressCallback::ERebuildPictures);
    for (auto const& [key, value] : listOfObjects)
    {
        Duco::Peal* thePeal = static_cast<Duco::Peal*>(value);
        std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator renumberPictureObj = sortedPictureIds.find(thePeal->PictureId());
        if (renumberPictureObj != sortedPictureIds.end() && renumberPictureObj->first != renumberPictureObj->second)
        {
            anyChangesMade = true;
            thePeal->SetPictureId(renumberPictureObj->second);
        }
    }

    return anyChangesMade;
}

bool
PealDatabase::RenumberAssociationsInPeals(const std::map<Duco::ObjectId, Duco::ObjectId>& sortedAssocaitionIds, Duco::RenumberProgressCallback* callback)
{
    bool anyChangesMade(false);

    callback->RenumberInitialised(RenumberProgressCallback::ERebuildAssociations);
    for (auto const& [key, value] : listOfObjects)
    {
        Duco::Peal* thePeal = static_cast<Duco::Peal*>(value);
        std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator renumberAssociationObj = sortedAssocaitionIds.find(thePeal->AssociationId());
        if (renumberAssociationObj != sortedAssocaitionIds.end() && renumberAssociationObj->first != renumberAssociationObj->second)
        {
            anyChangesMade = true;
            thePeal->SetAssociationId(renumberAssociationObj->second);
        }
    }

    return anyChangesMade;
}

bool
PealDatabase::UpdateObjectField(const Duco::ObjectId& id, const Duco::TUpdateArgument& updateArgument)
{
    DUCO_OBJECTS_CONTAINER::iterator it = listOfObjects.find(id);
    if (it == listOfObjects.end())
        return false;

    Duco::Peal* obj = static_cast<Duco::Peal*>(it->second);

    return updateArgument.Update(*obj);
}

void
PealDatabase::GetMethodIds(const std::set<ObjectId>& pealIds, std::set<ObjectId>& methodIds) const
{
    methodIds.clear();
    for (auto const& [key, value] : listOfObjects)
    {
        if (std::find(pealIds.begin(), pealIds.end(), key) != pealIds.end())
        {
            Duco::Peal* thePeal = static_cast<Duco::Peal*>(value);
            methodIds.insert(thePeal->MethodId());
        }
    }
}

void
PealDatabase::GetTowerIds(const std::set<ObjectId>& pealIds, std::set<ObjectId>& towerIds) const
{
    towerIds.clear();
    for (auto const& [key, value] : listOfObjects)
    {
        if (std::find(pealIds.begin(), pealIds.end(), key) != pealIds.end())
        {
            Duco::Peal* thePeal = static_cast<Duco::Peal*>(value);
            towerIds.insert(thePeal->TowerId());
        }
    }
}

bool
PealDatabase::RemoveDuplicateMethods(ProgressCallback*const callback)
{
    size_t stepNumber = 0;
    size_t totalObjects = NumberOfObjects() + methodsDatabase.NumberOfObjects();
    std::map<Duco::ObjectId, Duco::ObjectId> methodsToRemove = methodsDatabase.RemoveDuplicateMethods(callback, stepNumber, totalObjects);

    if (RenumberMethodsInPeals(methodsToRemove, NULL))
    {
        std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator it = methodsToRemove.begin();
        while (it != methodsToRemove.end())
        {
            methodsDatabase.DeleteObject(it->first);
            ++it;
        }

        return true;
    }
    return false;
}

Duco::ObjectId
PealDatabase::FindFirstPealWithoutBellboardId(Duco::PerformanceDate& lastPealDate) const
{
    lastPealDate.ResetToEarliest();
    Duco::ObjectId lastPeal;

    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        if (nextPeal->BellBoardId().length() == 0 && (nextPeal->Date() < lastPealDate || !lastPeal.ValidId()))
        {
            lastPealDate = nextPeal->Date();
            lastPeal = nextPeal->Id();
        }
    }

    return lastPeal;
}

std::set<std::wstring>
PealDatabase::RemoveExistingBellBoardPealIds(std::list<std::wstring>& pealIds) const
{
    std::set<std::wstring> allExistingPeals;
    std::set<std::wstring> removedPeals;
    std::list<std::wstring> pealsToDownload;

    // Find all bell board ids in the database
    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        if (nextPeal->BellBoardId().length() > 0)
        {
            std::wstring nextPealBellboardId = nextPeal->BellBoardId();
            allExistingPeals.insert(nextPealBellboardId);
        }
    }

    // Find peals which were n the original list and exist in the database
    std::list<std::wstring>::iterator pealDownloadList = pealIds.begin();
    while (pealDownloadList != pealIds.end())
    {
        std::set<std::wstring>::iterator it = allExistingPeals.find(*pealDownloadList);
        if (it == allExistingPeals.end())
        {
            pealsToDownload.push_back(*pealDownloadList);
        }
        else
        {
            removedPeals.insert(*pealDownloadList);
        }

        ++pealDownloadList;
    }
    pealIds = pealsToDownload;

    return removedPeals;
}

Duco::ObjectId
PealDatabase::FindPealWithAverageTime(const PerformanceTime& averageNumberOfMinutes) const
{
    int difference = numeric_limits<int>::max();
    Duco::ObjectId nearestFoundPeal = KNoId;

    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        if (nextPeal->Time() == averageNumberOfMinutes)
            return nextPeal->Id();
        else
        {
            int differenceForThisPeal = nextPeal->Time() - averageNumberOfMinutes;
            differenceForThisPeal = std::abs(differenceForThisPeal);
            if (differenceForThisPeal < difference)
            {
                nearestFoundPeal = nextPeal->Id();
                difference = differenceForThisPeal;
            }
        }
    }

    return nearestFoundPeal;
}

Duco::ObjectId
PealDatabase::FindPealWithAverageChanges(unsigned int averageNumberOfChanges) const
{
    int difference = numeric_limits<int>::max();
    Duco::ObjectId nearestFoundPeal = KNoId;

    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        if (nextPeal->NoOfChanges() == averageNumberOfChanges)
            return nextPeal->Id();
        else 
        {
            int differenceForThisPeal = (int)nextPeal->NoOfChanges() - averageNumberOfChanges;
            differenceForThisPeal = std::abs(differenceForThisPeal);
            if (differenceForThisPeal < difference)
            {
                nearestFoundPeal = nextPeal->Id();
                difference = differenceForThisPeal;
            }
        }
    }

    return nearestFoundPeal;
}

Duco::ObjectId
PealDatabase::FindPealWithAverageChangesPerMinute(double averageNumberOfChanges) const
{
    double difference = numeric_limits<float>::max();
    Duco::ObjectId nearestFoundPeal = KNoId;

    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        if (nextPeal->ChangesPerMinute() == averageNumberOfChanges)
            return nextPeal->Id();
        else
        {
            double differenceForThisPeal = (nextPeal->ChangesPerMinute() - averageNumberOfChanges);
            differenceForThisPeal = std::abs(differenceForThisPeal);
            if (differenceForThisPeal < difference)
            {
                nearestFoundPeal = nextPeal->Id();
                difference = differenceForThisPeal;
            }
        }
    }

    return nearestFoundPeal;;
}

bool
PealDatabase::AnyOnThisDay(const Duco::PerformanceDate& pealDay) const
{
    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        if (nextPeal->Date().DaysMatch(pealDay))
        {
            return true;
        }
    }
    return false;
}

std::set<Duco::ObjectId>
PealDatabase::OnThisDay(const Duco::PerformanceDate& pealDay) const
{
    std::set<Duco::ObjectId> pealsOnThisDay;

    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        if (nextPeal->Date().DaysMatch(pealDay))
        {
            pealsOnThisDay.insert(nextPeal->Id());
        }
    }
    return pealsOnThisDay;
}

void
PealDatabase::GetPealsForYear(const unsigned int pealYear, std::set<ObjectId>& foundPealIds) const
{
    foundPealIds.clear();
    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        if (nextPeal->Date().Year() == pealYear)
        {
            foundPealIds.emplace(nextPeal->Id());
        }
    }
}

void
PealDatabase::ReferenceStats(const Duco::StatisticFilters& filters, std::map<ObjectId, Duco::ReferenceItem>& pealCounts) const
{
    pealCounts.clear();

    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        if (filters.Match(*nextPeal))
        {
            std::map<ObjectId, Duco::ReferenceItem>::iterator it = pealCounts.find(nextPeal->TowerId());
            if (it != pealCounts.end())
            {
                it->second.AddPerformance(*nextPeal);
            }
            else if (nextPeal->TowerId().ValidId())
            {
                std::pair<ObjectId, Duco::ReferenceItem> newObject(nextPeal->TowerId(), *nextPeal);
                pealCounts.insert(newObject);
            }
        }
    }
}

void
PealDatabase::FindTowerIdForHandbellPeals(std::set<Duco::ObjectId>& towerIds) const
{
    towerIds.clear();
    std::set<Duco::ObjectId> nonHandBellTowers;

    for (auto const& [key, value] : listOfObjects)
    {
        const Peal* const nextPeal = static_cast<const Peal* const>(value);
        if (nextPeal->Handbell())
        {
            if (nonHandBellTowers.find(nextPeal->TowerId()) == nonHandBellTowers.end())
            {
                towerIds.insert(nextPeal->TowerId());
            }
        }
        else
        {
            nonHandBellTowers.insert(nextPeal->TowerId());
            if (towerIds.find(nextPeal->TowerId()) == towerIds.end())
            {
                towerIds.erase(nextPeal->TowerId());
            }
        }
    }
}

void
PealDatabase::Cancelled(const std::wstring& bellboardId, const char* message)
{

}
void
PealDatabase::Completed(const std::wstring& bellboardId, bool errors)
{

}

bool
PealDatabase::CheckBellBoardForUpdatedReference(const Duco::Peal& nextPeal, Duco::RingingDatabase& database)
{
    bool updatedPeal = false;
    BellboardPerformanceParser parser(database, *this, NULL);
    parser.DisableUpdates();
    parser.DisableSearches();
    parser.DownloadPerformance(nextPeal.BellBoardId());
    Peal modifiedPeal(nextPeal);

    if (parser.CurrentPerformance().BellBoardId().compare(nextPeal.BellBoardId()) != 0)
    {
        modifiedPeal.SetBellBoardId(parser.CurrentPerformance().BellBoardId());
        updatedPeal = true;
    }
    if (parser.CurrentPerformance().ValidRingingWorldReferenceForUpload() && !modifiedPeal.ValidRingingWorldReferenceForUpload())
    {
        modifiedPeal.SetRingingWorldReference(parser.CurrentPerformance().RingingWorldReference());
        updatedPeal = true;
    }

    if (updatedPeal)
    {
        return database.PealsDatabase().UpdateObject(modifiedPeal);
    }
    return false;
}

size_t
PealDatabase::CheckBellBoardForUpdatedReferences(Duco::ProgressCallback& progressCallback, Duco::RingingDatabase& database, bool onlyThoseWithoutRWPages, bool& cancellationFlag)
{
    XMLPlatformUtils::Initialize();
    cancellationFlag = false;
    size_t updatedCount = 0;
    progressCallback.Initialised();
    float numberOfItems = (float)(listOfObjects.size() * 2);
    float numberOfItemsProcessed = 0.0f;
    vector<std::future<bool>> threads;

    DUCO_OBJECTS_CONTAINER::const_iterator pealIt = listOfObjects.begin();
    while (pealIt != listOfObjects.end() && !cancellationFlag)
    {
        vector<std::future<bool>>::iterator it = threads.begin();
        while (threads.size() > 10 && it != threads.end())
        {
            std::future_status status = it->wait_for(2s);
            if (status == std::future_status::ready)
            {
                numberOfItemsProcessed += 1.0f;
                if (it->get())
                {
                    ++updatedCount;
                }
                threads.erase(it);
                it = threads.begin();
                progressCallback.Step((int)((numberOfItemsProcessed / numberOfItems) * 100));
            }
            ++it;
        }

        const Peal* const nextPeal = static_cast<const Peal* const>(pealIt->second);
        numberOfItemsProcessed += 1.0f;
        if (nextPeal->ValidBellBoardId() && (!onlyThoseWithoutRWPages || !nextPeal->ValidRingingWorldLink()))
        {
            threads.push_back(std::async(std::launch::async, &PealDatabase::CheckBellBoardForUpdatedReference, this, std::ref(*nextPeal), std::ref(database)));
            float workDone = numberOfItemsProcessed / numberOfItems;
            progressCallback.Step((int)(workDone * 100));
        }

        ++pealIt;
    }

    vector<std::future<bool>>::iterator it = threads.begin();
    while (it != threads.end() && !cancellationFlag)
    {
        numberOfItemsProcessed += 1.0f;
        if (it->get())
        {
            ++updatedCount;
        }
        progressCallback.Step((int)((numberOfItemsProcessed / numberOfItems) * 100));
        ++it;
    }

    progressCallback.Complete(false);
    XMLPlatformUtils::Terminate();
    return updatedCount;
}

