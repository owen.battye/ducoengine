#ifndef __RINGINGDATABASE_H__
#define __RINGINGDATABASE_H__

#include <map>
#include <set>
#include <string>

#include "CirclingData.h"
#include "ObjectId.h"
#include "ObjectType.h"
#include "PealLengthInfo.h"
#include "RingerPealDates.h"

namespace Duco
{
class AssociationDatabase;
class CompositionDatabase;
class DatabaseSettings;
class DoveObject;
class PerformanceDate;
class ImportExportProgressCallback;
class Method;
class MethodDatabase;
class MethodSeries;
class MethodSeriesDatabase;
class PealDatabase;
class ProgressCallback;
class PictureDatabase;
class RenumberProgressCallback;
class Ringer;
class RingerDatabase;
class StatisticFilters;
class Tower;
class TowerDatabase;

class RingingDatabase
{
public:
    DllExport RingingDatabase();
    DllExport ~RingingDatabase();

    DllExport std::string BackupFileName(const std::string& fileName) const;

    DllExport void ClearDatabase(bool quarterPealDb, bool createDefaultBellNames);
    DllExport bool DataChanged() const;
    DllExport void ResetDataChanged() const;
    inline unsigned int DatabaseVersion() const;

    inline time_t CreatedDate() const;

    inline Duco::DatabaseSettings& Settings() const;

    DllExport bool Externalise(const char* fileName, Duco::ImportExportProgressCallback* newCallback) const;
    DllExport bool ExternaliseToBellboardXml(const char* fileName, std::set<Duco::ObjectId> performanceIds, Duco::ImportExportProgressCallback* newCallback) const;
    DllExport bool ExternaliseToXml(const char* fileName, Duco::ImportExportProgressCallback* newCallback, const std::string& originalFileName, bool exportDucoIds, bool onlyIncludeDatabaseVersion = false) const;
    DllExport bool ExternaliseToCsv(const char* fileName, Duco::ImportExportProgressCallback* newCallback, const std::string& originalFileName) const;
    DllExport void Internalise(const char* fileName, Duco::ImportExportProgressCallback* newCallback, unsigned int& databaseVersionNumber);

    // functions for post import checks
    DllExport void PostWinRkImportChecks();

    DllExport void GetTowerCircling(const Duco::StatisticFilters& filters, std::map<unsigned int, Duco::CirclingData>& sortedBellPealCounts, Duco::PealLengthInfo& noOfPeals, size_t& numberOfTimesCircled, PerformanceDate& firstCircledDate, size_t& noOfDaysToFirstCircle) const;
    DllExport void GetMethodCircling(const Duco::StatisticFilters& filters, std::map<unsigned int, Duco::CirclingData>& sortedBellPealCounts, size_t& numberOfTimesCircled, PerformanceDate& firstCircledDate) const;
    DllExport bool ReplaceRinger(const ObjectId& oldRingerId, const ObjectId& newRingerId, bool deleteOldRinger);
    DllExport bool ReplaceTower(const ObjectId& oldTowerId, const ObjectId& newTowerId);
    DllExport bool SuggestMethodSeries(const std::wstring& methodsStr, unsigned int order, Duco::ObjectId& methodSeriesId, std::set<Duco::ObjectId>& methodIds) const;
    Duco::ObjectId FindPealInTower(const Duco::DoveObject& doveTower, bool onAllBells) const;
    DllExport bool RemovePictures(const std::set<Duco::ObjectId>& pictureIds);
    DllExport std::wstring MapBoxMap(size_t& noOfTowersPlotted, size_t& noOfTowersNotPlotted, const std::wstring& staticMapKey,
        bool zeroPerformances, bool onePerformance, bool multiplePerformances, bool combineLinkedWithSamePosition, bool useFelsteadPealCount, const Duco::StatisticFilters& filters) const;
    DllExport bool SetTowersWithHandbellPealsAsHandbells();
    DllExport size_t CheckBellBoardForUpdatedReferences(Duco::ProgressCallback& progressCallback, bool onlyThoseWithoutRWPages, bool& cancellationFlag);
    DllExport bool CapitaliseFields(Duco::ProgressCallback& progressCallback, bool methodName, bool MethodType, bool county, bool dedication, bool ringerNames);
    DllExport Duco::ObjectId SuggestRingers(const std::wstring& ringerFullname, const Duco::PerformanceDate& performanceDate, bool partialMatchAllowed, std::set<Duco::RingerPealDates>& nearMatches) const;

    DllExport bool FindUnused(std::set<ObjectId>& unusedMethods, std::set<ObjectId>& unusedRingers, std::set<ObjectId>& unusedTowers, std::multimap<ObjectId, ObjectId>& unusedRings, std::set<ObjectId>& unusedMethodSeries, std::set<ObjectId>& unusedCompositions, std::set<ObjectId>& unusedAssociations) const;
    DllExport bool FindUnusedTowers(std::set<ObjectId>& unusedTowers) const;
    DllExport bool FindUnusedPictures(std::set<ObjectId>& unusedTowers) const;
    DllExport Duco::ObjectId FindPicture(const std::wstring& description, const Duco::ObjectId& currentPictureId = KNoId);

    DllExport bool RebuildRecommended() const;
    DllExport bool RebuildDatabase(Duco::RenumberProgressCallback* callback);
    DllExport bool RebuildInProgress() const;
    DllExport bool RemoveDuplicateAssociations();

    DllExport size_t NumberOfObjectsWithErrors(bool includeWarnings, Duco::TObjectType objectType);

    DllExport PealLengthInfo PerformanceInfo(const Duco::StatisticFilters& filters) const;
    DllExport size_t NumberOfRingers() const;
    DllExport size_t NumberOfTowers() const;
    DllExport size_t NumberOfActiveTowers(size_t& noOfLinkedTowers) const;
    DllExport size_t NumberOfRemovedTowers() const;
    DllExport size_t NumberOfMethods() const;
    DllExport size_t NumberOfMethodSeries() const;
    DllExport size_t NumberOfCompositions() const;
    DllExport size_t NumberOfAssociations() const;
    DllExport size_t NumberOfPictures() const;

    DllExport const Duco::RingerDatabase& RingersDatabase() const;
    DllExport const Duco::MethodDatabase& MethodsDatabase() const;
    DllExport const Duco::TowerDatabase& TowersDatabase() const;
    DllExport const Duco::PealDatabase& PealsDatabase() const;
    DllExport const Duco::PictureDatabase& PicturesDatabase() const;
    DllExport const Duco::MethodSeriesDatabase& MethodSeriesDatabase() const;
    DllExport const Duco::CompositionDatabase& CompositionsDatabase() const;
    DllExport const Duco::AssociationDatabase& AssociationsDatabase() const;

    DllExport Duco::RingerDatabase& RingersDatabase();
    DllExport Duco::MethodDatabase& MethodsDatabase();
    DllExport Duco::TowerDatabase& TowersDatabase();
    DllExport Duco::PealDatabase& PealsDatabase();
    DllExport Duco::PictureDatabase& PicturesDatabase();
    DllExport Duco::MethodSeriesDatabase& MethodSeriesDatabase();
    DllExport Duco::CompositionDatabase& CompositionsDatabase();
    DllExport Duco::AssociationDatabase& AssociationsDatabase();

    DllExport std::wstring MapBoxMarkerString(size_t markerId, size_t noOfperformances, size_t daysSinceLastPeal, const Duco::Tower& theTower) const;
    DllExport bool MapBoxZoomAndMiddle(const std::map<Duco::ObjectId, Duco::PealLengthInfo>& filteredTowerIds, double& latitudeMiddle, double& longitudeMiddle, int& zoomLevel) const;

protected:
    void ResetCurrentPointers(); 
    void DeleteDatabaseContents(bool createDefaultBellNames);
    void CheckMethodSeriesAllContainMethodFromList(const std::set<Duco::ObjectId>& methodIds, std::set<MethodSeries*>& methodSeries) const;
    void CheckMethodSeriesForNumberOfMethods(size_t noOfMethods, std::set<MethodSeries*>& methodSeries) const;
    void CheckPotentialMethodNameAndAdd(const std::wstring& nextPossibleMethodStr, std::set<std::wstring>& allMethodNames) const;

private:
    unsigned int                        databaseVersion;
    time_t                              createdDate;
    bool                                rebuildInProgress;

    Duco::RingerDatabase*               ringersDatabase;
    Duco::TowerDatabase*                towersDatabase;
    Duco::MethodDatabase*               methodsDatabase;
    Duco::PealDatabase*                 pealsDatabase;
    Duco::PictureDatabase*              picturesDatabase;
    Duco::MethodSeriesDatabase*         methodSeriesDatabase;
    Duco::CompositionDatabase*          compositionsDatabase;
    Duco::AssociationDatabase*          associationsDatabase;

    std::wstring                        notes;
    Duco::DatabaseSettings*             settings;
};

Duco::DatabaseSettings& RingingDatabase::Settings() const
{
    return *settings;
}

time_t RingingDatabase::CreatedDate() const
{
    return createdDate;
}

unsigned int RingingDatabase::DatabaseVersion() const
{
    return databaseVersion;
}

} // end namespace Duco

#endif //!__RINGINGDATABASE_H__
