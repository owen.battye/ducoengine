#ifndef __MUSICOBJECT_H__
#define __MUSICOBJECT_H__

#include <string>
#include <vector>
#include "DucoEngineCommon.h"

namespace Duco
{
    class Change;

class MusicObject
{
public:
    DllExport virtual ~MusicObject();
    MusicObject();

    // Accessors
    inline size_t Order() const;
    inline unsigned int Count() const;
    virtual std::wstring Print() const;

    //Settors
    void SetBells(const std::wstring& str);

    //Other
    bool Match(Duco::Change& change);
    virtual std::wstring Name(bool showAll) const;

protected:
    std::vector<unsigned int>   bells;
    unsigned int                count;
};

size_t
MusicObject::Order() const
{
    return bells.size();
}

unsigned int
MusicObject::Count() const
{
    return count;
}

}

#endif //!__MUSICOBJECT_H__
