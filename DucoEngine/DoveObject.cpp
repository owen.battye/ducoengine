#include "DoveObject.h"
#include "DoveFileConfiguration.h"
#include "DucoEngineUtils.h"
#include <cstdlib>
#include <cwctype>
#include <sstream>
#include <algorithm>

using namespace std;
using namespace Duco;

const int KPoundsInACwt = 112;
const int KPoundsInAQtr = KPoundsInACwt/4;

DoveObject::DoveObject()
:   tenorWeightLbls (0), tenorApprox (false), unringable (false), unitedKingdomOrIreland (false), groundfloor (false),
    noOfBells(0), latitude(0), longitude(0), antiClockwise(false)
{
}

DoveObject::DoveObject(const std::wstring& doveLine, const Duco::DoveFileConfiguration& fileConfig)
:   doveId(L""), tenorWeightLbls(0), tenorApprox(false), unringable(false),
    dedication(L""), city(L""), county(L""), country(L""), webpage(L""),
    unitedKingdomOrIreland(false), tenor(L""), altTenor(L""), tenorKey(L""), noOfBells(-1), latitude(0), longitude(0), groundfloor(false), antiClockwise(false)
{
    std::list<std::wstring> tokens;
    DucoEngineUtils::Tokenise<std::wstring>(doveLine, tokens);

    std::list<std::wstring>::const_iterator it = tokens.begin();
    int fieldNumber (0);

    while (it != tokens.end())
    {
        if (fieldNumber == fileConfig.DoveIdFieldNumber())
            doveId = *it;
        else if (fieldNumber == fileConfig.OldDoveIdFieldNumber())
            oldDoveId = *it;
        else if (fieldNumber == fileConfig.TowerbaseIdFieldNumber())
            SetTowerBaseId(*it);
        else if (fieldNumber == fileConfig.RingIdFieldNumber())
            ringId = *it;
        else if (fieldNumber == fileConfig.CountyFieldNumber())
            county = *it;
        else if (fieldNumber == fileConfig.CountryFieldNumber())
        {
            country = *it;
            if (country.compare(L"Great Britain") == 0 ||
                country.compare(L"Wales") == 0 ||
                country.compare(L"England") == 0 ||
                country.compare(L"Scotland") == 0 ||
                country.compare(L"Ireland") == 0)
            {
                unitedKingdomOrIreland = true;
            }
        }
        else if (fieldNumber == fileConfig.CountryIso3166FieldNumber())
        {
            if (country.length() == 0)
            {
                country = *it;
            }
        }
        else if (fieldNumber == fileConfig.TownFieldNumber())
            city = *it;
        else if (fieldNumber == fileConfig.NameFieldNumber())
            name = *it;
        else if (fieldNumber == fileConfig.DedicatationFieldNumber())
        {
            dedication = *it;
            if (dedication.find(L"S ") == 0)
            {
                dedication.insert(1, L"t");
            }
        }
        else if (fieldNumber == fileConfig.BellsFieldNumber())
        {
            noOfBells = _wtoi(it->c_str());
        }
        else if (fieldNumber == fileConfig.TenorFieldNumber())
        {
            tenorWeightLbls = _wtoi(it->c_str());
        }
        else if (fieldNumber == fileConfig.ApproxTenorFieldNumber())
        {
            if (it->length() > 0 && it->compare(L"app") == 0)
            {
                tenorApprox = true;
            }
        }
        else if (fieldNumber == fileConfig.KeyFieldNumber())
        {
            tenorKey = *it;
        }
        else if (fieldNumber == fileConfig.UnringableFieldNumber())
        {
            unringable = DucoEngineUtils::CompareString(L"u/r", *it);
        }
        else if (fieldNumber == fileConfig.AltNameFieldNumber())
        {
            altName = *it;
        }
        else if (fieldNumber == fileConfig.LatitudeFieldNumber())
        {
            latitude = DucoEngineUtils::ToFloat(*it);
        }
        else if (fieldNumber == fileConfig.LongitudeFieldNumber())
        {
            longitude = DucoEngineUtils::ToFloat(*it);
        }
        else if (fieldNumber == fileConfig.SemiTonesFieldNumber())
        {
            GetSemiTones(*it);
        }
        else if (fieldNumber == fileConfig.GroundFloorFieldNumber())
        {
            groundfloor = *it == L"GF";
        }
        else if (fieldNumber == fileConfig.ExtraInfoFieldNumber())
        {
            wstring extraInfo = *it;
            wstring antiClockwiseString = L"anticlockwise";
            auto it = std::search(
                extraInfo.begin(), extraInfo.end(),
                antiClockwiseString.begin(), antiClockwiseString.end(),
                [](wchar_t ch1, wchar_t ch2) { return std::towupper(ch1) == std::towupper(ch2); }
            );
            antiClockwise = (it != extraInfo.end());
        }
        else if (fieldNumber == fileConfig.WebsiteFieldNumber())
        {
            if (it->length() > 0)
            {
                webpage = *it;
            }
        }

        ++fieldNumber;
        ++it;
    }
    SetTenorWeight(tenorApprox);
}

DoveObject::~DoveObject()
{
}

bool
DoveObject::Valid() const
{
    return noOfBells > 0 && noOfBells != -1;
}

std::wstring
DoveObject::FullName() const
{
    std::wstring towerName;
    if (name.length() > 0)
    {
        towerName.append(name);
    }
    if (city.length() > 0)
    {
        if (towerName.length() > 0)
        {
            towerName.append(L", ");
        }
        towerName.append(city);
    }
    if (county.length() > 0)
    {
        towerName.append(L", ");
        towerName.append(county);
    }
    towerName.append(L" (");
    towerName.append(dedication);
    towerName.append(L")");
    return towerName;
}

std::wstring
DoveObject::FullNameAndDescription() const
{
    std::wstring towerName (FullName());
    towerName.append(L", ");
    towerName.append(DucoEngineUtils::ToString(Bells()));
    towerName.append(L" bells. Tenor ");
    towerName.append(Tenor());
    towerName.append(L".");
    return towerName;
}

void
DoveObject::SetTowerBaseId(const std::wstring newTowerbaseId)
{
    towerbaseId = DucoEngineUtils::TrimTowerBaseId(newTowerbaseId);
}

void
DoveObject::SetTenorWeight(bool approx)
{
    tenor.clear();
    altTenor.clear();

    if (approx)
    {
        SetTenorWeight(tenor, true);
        SetTenorWeight(altTenor, false);
    }
    else
    {
        SetTenorWeight(tenor, false);
    }
}

void
DoveObject::SetTenorWeight(std::wstring& tenorStr, bool approx) const
{
    if (tenorWeightLbls == 0)
        return;

    int tempTenorWeight (tenorWeightLbls);

    int cwt = tempTenorWeight / KPoundsInACwt;
    if (cwt > 0)
    {
        tempTenorWeight -= cwt * KPoundsInACwt;
        tenorStr += DucoEngineUtils::ToString(cwt);
        if (!approx)
            tenorStr += L"-";
    }
    int qtrs = tempTenorWeight / KPoundsInAQtr;
    if (approx)
    {
        switch (qtrs)
        {
        case 1:
            tenorStr += L"�";
            break;
        case 2:
            tenorStr += L"�";
            break;
        case 3:
            tenorStr += L"�";
            break;
        default:
            break;
        }
    }
    else
    {
        if (qtrs > 0)
        {
            tempTenorWeight -= qtrs * KPoundsInAQtr;
            tenorStr += DucoEngineUtils::ToString(qtrs);
            tenorStr += L"-";
        }
        else if (tenor.length() > 0)
        {
            tenorStr += L"0-";
        }

        if (tenor.length() > 0)
        {
            tenorStr += DucoEngineUtils::ToString(tempTenorWeight);
        }
        else
        {
            tenorStr += DucoEngineUtils::ToString(tempTenorWeight);
            tenorStr += L"lbs";
        }
    }
}

void
DoveObject::Clear()
{
    doveId.clear();
    ringId.clear();
    oldDoveId.clear();
    towerbaseId.clear();

    tenorWeightLbls = 0;
    tenorApprox = false;
    unringable = false;
    unitedKingdomOrIreland = false;
    groundfloor = false;
    antiClockwise = false;

    dedication.clear();
    name.clear();
    altName.clear();
    city.clear();
    county.clear();
    country.clear();
    tenor.clear();
    altTenor.clear();
    tenorKey.clear();
    webpage.clear();
    semiToneBells.clear();

    noOfBells = 0;
    latitude = 0;
    longitude = 0;
}

bool
DoveObject::operator<(const Duco::DoveObject& rhs) const
{
    if (rhs.doveId != doveId)
    {
        return doveId < rhs.doveId;
    }

    return ringId < rhs.ringId;
}

void
DoveObject::GetSemiTones(const std::wstring& semiTones)
{
    if (semiTones.length() > 0)
    {
        //std::string::difference_type numberOfSemiTones = std::count(semiTones.begin(), semiTones.end(), '+') + 1;
        //noOfBells += numberOfSemiTones;
        std::list<std::wstring> bells;

        DucoEngineUtils::Tokenise(semiTones, bells, L"+");
        std::list<std::wstring>::const_iterator it = bells.begin();
        while (it != bells.end())
        {
            TRenameBellType type = ENormalBell;
            unsigned int bellNo = _wtoi(it->c_str());
            if (it->find(L"extra") != std::wstring::npos)
            {
                type = EExtraTreble;
                bellNo += 1;
            }
            else if (it->find(L"#") != std::wstring::npos)
            {
                type = ESharp;
            }
            else if (it->find(L"b") != std::wstring::npos)
            {
                type = EFlat;
                bellNo += 1;
            }
            if (type != ENormalBell)
            {
                bellNo += (unsigned int)semiToneBells.size();
                std::pair<unsigned int, Duco::TRenameBellType> semiToneBell(bellNo, type);
                semiToneBells.insert(semiToneBell);
            }
            ++it;
        }
    }
}

bool
DoveObject::HasSemiTones() const
{
    return semiToneBells.size() > 0;
}

unsigned int
DoveObject::NumberOfSemiTones() const
{
    return (unsigned int)semiToneBells.size();
}

const std::map<unsigned int, Duco::TRenameBellType>&
DoveObject::SemiToneBells() const
{
    return semiToneBells;
}
