#include "Picture.h"

#include "DucoEngineUtils.h"
#include "DatabaseWriter.h"
#include "DatabaseReader.h"
#include "DucoConfiguration.h"

#include <ostream>
#include <sstream>
#include <codecvt>

using namespace std;
using namespace Duco;

#include "DucoXmlUtils.h"
XERCES_CPP_NAMESPACE_USE

Picture::Picture()
    : RingingObject(), pictureBuffer(NULL), pictureType(PictureType::EUnknown), databaseVersion(DucoConfiguration::LatestDatabaseVersion())
{
}

Picture::Picture(const std::string& fileName)
    : RingingObject(), pictureBuffer(NULL), pictureType(PictureType::EUnknown), databaseVersion(DucoConfiguration::LatestDatabaseVersion())
{
    CreatePictureBuffer(fileName);
    std::wstring wideString = std::wstring_convert<std::codecvt_utf8<wchar_t>>().from_bytes(fileName);
    SetNotes(wideString);
}

Picture::Picture(DatabaseReader& reader, unsigned int currentDatabaseVersion)
: RingingObject(), pictureType(PictureType::EUnknown), databaseVersion(currentDatabaseVersion)
{
    pictureBuffer = new std::string("");
    Internalise(reader, databaseVersion);
}

Picture::Picture(const Picture& other)
    :   RingingObject(other), pictureType(other.pictureType), databaseVersion(other.databaseVersion), notes (other.notes)
{
    pictureBuffer = new std::string(*other.pictureBuffer);
}

Picture::Picture(const Duco::ObjectId& newId, const Picture& other)
:   RingingObject(newId), pictureType(other.pictureType), databaseVersion(DucoConfiguration::LatestDatabaseVersion()), notes(other.notes)
{
    pictureBuffer = new std::string(*other.pictureBuffer);
}

Picture::~Picture()
{
    delete pictureBuffer;
}

Duco::TObjectType
Picture::ObjectType() const
{
    return TObjectType::EPicture;
}

bool
Picture::Duplicate(const Duco::RingingObject& other, const Duco::RingingDatabase& database) const
{
    if (other.ObjectType() != ObjectType())
        return false;

    const Duco::Picture& otherPicture = static_cast<const Duco::Picture&>(other);

    if (pictureType != otherPicture.pictureType)
        return false;

    if (pictureBuffer->length() != otherPicture.pictureBuffer->length())
        return false;

    if (pictureBuffer->compare(*otherPicture.pictureBuffer) != 0)
        return false;

    return true;
}

bool
Picture::Match(const Duco::Picture& other) const
{
    if (id != other.id)
        return false;

    if (pictureType != other.pictureType)
        return false;

    size_t pos = pictureBuffer->compare(*other.pictureBuffer);
    if (pos != 0)
        return false;

    return true;
}

bool
Picture::operator==(const Duco::RingingObject& other) const
{
    if (other.ObjectType() != ObjectType() || id != other.Id())
        return false;

    const Duco::Picture& otherPicture = static_cast<const Duco::Picture&>(other);

    return Match(otherPicture);
}

bool
Picture::operator!=(const Duco::RingingObject& other) const
{
    if (other.ObjectType() != ObjectType())
        return true;

    const Duco::Picture& otherPicture = static_cast<const Duco::Picture&>(other);

    if (pictureType != otherPicture.pictureType)
        return true;

    if (pictureBuffer ->length() != otherPicture.pictureBuffer->length())
        return true;

    if (pictureBuffer->compare(*otherPicture.pictureBuffer) != 0)
        return true;

    return false;

}

Picture&
Picture::operator=(const Duco::Picture& otherPicture)
{
    id = otherPicture.id;
    errorCode = otherPicture.errorCode;
    pictureType = otherPicture.pictureType;
    delete pictureBuffer;
    pictureBuffer = NULL;
    pictureBuffer = new std::string(*otherPicture.pictureBuffer);
    notes = otherPicture.notes;

    return *this;
}

DatabaseWriter&
Picture::Externalise(DatabaseWriter& writer) const
{
    writer.WriteId(id);
    writer.WriteInt((unsigned int)pictureType);
    writer.WritePictureBuffer(*pictureBuffer);
    writer.WriteString(notes);

    return writer;
}

DatabaseReader&
Picture::Internalise(DatabaseReader& reader, unsigned int databaseVersionBeingRead)
{
    id = reader.ReadUInt();
    pictureType = (Duco::PictureType)reader.ReadUInt();
    reader.ReadPictureBuffer(pictureBuffer);
    if (databaseVersionBeingRead >= 46)
    {
        reader.ReadString(notes);
    }

    return reader;
}

std::wostream&
Picture::Print(std::wostream& stream, const RingingDatabase& /*database*/) const
{
    return stream;
}

bool
Picture::Valid(const RingingDatabase& database, bool /*warningFatal*/, bool clearBeforeStart) const
{
    if (clearBeforeStart)
    {
        ClearErrors();
    }
    return ContainsValidPicture();
}

std::wstring
Picture::ErrorString(const Duco::DatabaseSettings& /*settings*/, bool /*showWarning*/) const
{
    return L"";
}

bool
Picture::ContainsValidPicture() const
{
    return pictureBuffer != NULL && pictureBuffer->length() > 0 && (databaseVersion < 41 || pictureType != PictureType::EUnknown);
}

size_t
Picture::PictureSize() const
{
    return pictureBuffer->size();
}

const std::string&
Picture::GetPictureBuffer() const
{
    return *pictureBuffer;
}

void
Picture::ReplacePicture(const std::string& fileName)
{
    CreatePictureBuffer(fileName);
}

void
Picture::CreatePictureBuffer(const std::string& fileName)
{
    if (pictureBuffer)
    {
        delete pictureBuffer;
    }
    std::ifstream fin(fileName, std::ios::in | std::ios::binary);
    if (!fin.is_open())
    {
        throw std::invalid_argument("File doesn't exist");
    }
    std::ostringstream oss;
    oss << fin.rdbuf();
    pictureBuffer = new std::string(oss.str());

    std::string lowercaseFilename;
    DucoEngineUtils::ToLowerCase(fileName, lowercaseFilename);
    if (lowercaseFilename.find(".bmp") != string::npos)
    {
        pictureType = PictureType::EBmp;
    }
    else if (lowercaseFilename.find(".jpeg") != string::npos || fileName.find(".jpg") != wstring::npos)
    {
        pictureType = PictureType::EJpeg;
    }
    else if (lowercaseFilename.find(".gif") != string::npos)
    {
        pictureType = PictureType::EGif;
    }
    else if (lowercaseFilename.find(".png") != string::npos)
    {
        pictureType = PictureType::EPng;
    }
    else
    {
        pictureType = PictureType::EUnknown;
    }
}

void
Picture::SavePicture(std::string& filename) const
{
    filename += GetFileExtension();
    std::ofstream output;
    output.open(filename.c_str(), std::ios::out | std::ios::trunc | std::ios::binary);
    size_t stringLength = GetPictureBuffer().length();
    output.write(GetPictureBuffer().c_str(), stringLength);
    output.close();
}

std::string
Picture::GetFileExtension() const
{
    switch (pictureType)
    {
    case PictureType::EBmp:
        return ".bmp";
    case PictureType::EJpeg:
        return ".jpeg";
    case PictureType::EGif:
        return ".gif";
    case PictureType::EPng:
        return ".png";
    default:
        break;
    }
    return ".tmp";
}

const std::wstring&
Picture::Notes() const
{
    return notes;
}

void
Picture::SetNotes(const std::wstring& newNotes)
{
    notes = newNotes;
}


void
Picture::ExportToXml(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& picturesElement, bool exportDucoObjects) const
{
    DOMElement* newPicture = outputFile.createElement(XMLStrL("Picture"));
    picturesElement.appendChild(newPicture);

    DucoXmlUtils::AddObjectId(*newPicture, id);
}

//EOF
