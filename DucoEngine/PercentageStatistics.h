#pragma once
#include <map>
#include <set>
#include "ObjectId.h"

namespace Duco
{
    class Peal;

    /**
     * Percentages are given as in the range 0-1. (Not 0 to 100)
     */
    class RingerCount
    {
    public:
        inline RingerCount(const Duco::ObjectId& newId);

        void AddPeal(const Duco::Peal& newPeal);
        void SetTotals(size_t totalNumberOfChanges, size_t totalNumberOfPeals);

        inline bool operator<(const Duco::RingerCount& rhs) const;

        DllExport Duco::ObjectId RingerId() const;
        DllExport size_t NumberOfChanges() const;
        DllExport size_t NumberOfPeals() const;
        DllExport double PercentageOfChanges() const;
        DllExport double PercentageOfPeals() const;

    private:
        Duco::ObjectId ringerId;
        size_t numberOfPeals;
        size_t numberOfChanges;
        size_t totalNumberOfPeals;
        size_t totalNumberOfChanges;
    };

    /**
     * This class assumes peals have been sorted in the database into the correct order
     * If not the last peal Id will be wrong.
     */
    class PercentageStatistics
    {
    public:
        DllExport PercentageStatistics(const std::set<Duco::ObjectId>& ringerIds);
        DllExport ~PercentageStatistics();
        void AddPeal(const Duco::Peal& newPeal);
        
        inline bool ContainsResults() const;
        inline size_t NoOfPeals() const;
        inline size_t NoOfChanges() const;
        DllExport std::multiset<Duco::RingerCount> ChangeCounts() const;
        DllExport Duco::ObjectId LastPealId() const;


    private:
        Duco::ObjectId lastPealId;
        std::map<Duco::ObjectId, Duco::RingerCount> changesCountByRinger;
        size_t numberOfPeals;
        size_t numberOfChanges;
    };

    RingerCount::RingerCount(const Duco::ObjectId& newId)
        : ringerId(newId), numberOfPeals(0), numberOfChanges(0), totalNumberOfPeals(0), totalNumberOfChanges(0)
    {

    }

    bool
    RingerCount::operator<(const Duco::RingerCount& rhs) const
    {
        return numberOfPeals > rhs.numberOfPeals;
    }

    bool
    PercentageStatistics::ContainsResults() const
    {
        return numberOfPeals > 0 && numberOfChanges > 0;
    }

    size_t
    PercentageStatistics::NoOfPeals() const
    {
        return numberOfPeals;
    }

    size_t
    PercentageStatistics::NoOfChanges() const
    {
        return numberOfChanges;
    }

}
