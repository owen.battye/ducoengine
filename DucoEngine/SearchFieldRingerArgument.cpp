#include "SearchFieldRingerArgument.h"

#include "DucoEngineUtils.h"
#include "Peal.h"

using namespace Duco;

TSearchFieldRingerArgument::TSearchFieldRingerArgument(const Duco::ObjectId& newRingerId, const Duco::TSearchFieldRingerPosition& posn)
    :   TSearchArgument(ERingerPosition),
        ringerId(newRingerId),
        fieldValue(L""),
        position(posn)
{

}

TSearchFieldRingerArgument::TSearchFieldRingerArgument(const std::wstring& newRingerName, const Duco::TSearchFieldRingerPosition& posn)
    :   TSearchArgument(ERingerPosition),
        ringerId(),
        fieldValue(newRingerName),
        position(posn)
{
}

TSearchFieldRingerArgument::TSearchFieldRingerArgument(const Duco::TSearchFieldRingerArgument& rhs)
    :   TSearchArgument(rhs),
    ringerId(rhs.ringerId),
    fieldValue(rhs.fieldValue),
    position(rhs.position)
{

}

Duco::TFieldType 
TSearchFieldRingerArgument::FieldType() const
{
    if (ringerId.ValidId())
        return TFieldType::ENumberField;

    return TFieldType::EStringField;
}

const std::wstring&
TSearchFieldRingerArgument::RingerName() const
{
    return fieldValue;
}

const Duco::ObjectId&
TSearchFieldRingerArgument::RingerId() const
{
    return ringerId;
}

std::wstring
TSearchFieldRingerArgument::Position() const
{
    return position.Description();
}

bool
TSearchFieldRingerArgument::Match(const Duco::Peal& thePeal, const Duco::RingingDatabase& database) const
{
    if (ringerId.ValidId())
        return MatchById(thePeal, database);

    return MatchByString(thePeal, database);
}

bool
TSearchFieldRingerArgument::MatchById(const Duco::Peal& thePeal, const Duco::RingingDatabase& database) const
{
    std::set<unsigned int> bellsRung;
    bool strapper (false);
    if (thePeal.BellRung(ringerId, bellsRung, strapper))
    {
        std::set<unsigned int>::const_iterator it = bellsRung.begin();
        while (it != bellsRung.end())
        {
            if (position.Match(*it, thePeal.NoOfBellsRung(database)))
                return true;
            ++it;
        }
    }
    return false;
}

bool
TSearchFieldRingerArgument::MatchByString(const Duco::Peal& thePeal, const Duco::RingingDatabase& database) const
{
    unsigned int bellNo;
    if (thePeal.FindRingerBellByName(database, fieldValue, true, bellNo))
    {
        if (position.Match(bellNo, thePeal.NoOfBellsRung(database)))
            return true;
    }
    return false;
}
