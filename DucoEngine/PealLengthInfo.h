#ifndef __PEALLENGTHINFO_H__
#define __PEALLENGTHINFO_H__

#include "DucoEngineCommon.h"
#include "PerformanceDate.h"
#include "ObjectId.h"

namespace Duco
{
    class Peal;
    class FelsteadPealCount;

    class PealLengthInfo
    {
    public:
        DllExport PealLengthInfo();
        DllExport PealLengthInfo(const Duco::Peal& firstPeal);
        DllExport PealLengthInfo(const Duco::FelsteadPealCount& pealInfo);
        DllExport ~PealLengthInfo();

        DllExport Duco::PealLengthInfo operator-=(const Duco::PealLengthInfo& rhs);
        DllExport bool operator==(const Duco::PealLengthInfo& rhs) const;
        DllExport bool operator<(const Duco::PealLengthInfo& rhs) const;
        DllExport bool operator>(const Duco::PealLengthInfo& rhs) const;
        DllExport Duco::PealLengthInfo& operator+=(const Duco::Peal& rhs);
        DllExport Duco::PealLengthInfo& operator+=(const Duco::PealLengthInfo& rhs);
        DllExport void Clear();
        DllExport bool Empty() const;

        DllExport virtual void AddPeal(const Duco::Peal& nextpeal);

        DllExport const Duco::ObjectId& LongestChangesId() const;
        DllExport const Duco::ObjectId& LongestMinutesId() const;
        DllExport const Duco::ObjectId& SlowestCpmId() const;
        DllExport const Duco::ObjectId& ShortestChangesId() const;
        DllExport const Duco::ObjectId& ShortestMinutesId() const;
        DllExport const Duco::ObjectId& FastestCpmId() const;
        DllExport const Duco::ObjectId& FirstPealId() const;
        DllExport const Duco::ObjectId& LastPealId() const;
        DllExport std::wstring DateOfFirstPealString() const;
        DllExport std::wstring DateOfLastPealString() const;
        DllExport const Duco::PerformanceDate& DateOfFirstPeal() const;
        DllExport const Duco::PerformanceDate& DateOfLastPeal() const;

        DllExport size_t DaysSinceLastPeal() const;
        DllExport std::wstring DurationBetweenFirstAndLastPealString() const;
        DllExport unsigned int DaysBetweenFirstAndLastPeal() const;

        DllExport size_t TotalPeals() const;
        DllExport size_t TotalChanges() const;
        DllExport size_t TotalMinutes() const;

        DllExport unsigned int AverageChanges() const;
        DllExport unsigned int AverageMinutes() const;
        DllExport float AveragePealSpeed() const;

        DllExport float FastestChangesPerMinute() const;
        DllExport float SlowestChangesPerMinute() const;

    protected:
        bool SetLongestChanges(const Duco::ObjectId& theId, unsigned int noOfChanges);
        bool SetLongestMinutes(const Duco::ObjectId& theId, unsigned int noOfMinutes);
        bool SetSlowestCpm(const Duco::ObjectId& theId, float speed);
        bool SetShortestChanges(const Duco::ObjectId& theId, unsigned int noOfChanges);
        bool SetShortestMinutes(const Duco::ObjectId& theId, unsigned int noOfMinutes);
        bool SetFastestCpm(const Duco::ObjectId& theId, float speed);
        bool SetPealDates(const Duco::ObjectId& theId, const Duco::PerformanceDate& pealDate);

    protected:
        size_t totalChanges;
        size_t totalMinutes;
        size_t totalPeals;
        size_t totalPealsWithChanges;
        size_t totalPealsWithTime;
        size_t totalChangesWithTimeAndMinutes;
        size_t totalMinutesWithTimeAndMinutes;


        Duco::ObjectId longestChangesId;
        unsigned int longestNumberOfChanges;

        Duco::ObjectId longestMinutesId;
        unsigned int longestNumberOfMinutes;

        Duco::ObjectId slowestCPMId;
        float slowestNumberOfCPM;

        Duco::ObjectId shortestChangesId;
        unsigned int shortestNumberOfChanges;

        Duco::ObjectId shortestMinutesId;
        unsigned int shortestNumberOfMinutes;

        Duco::ObjectId fastestCPMId;
        float fastestNumberOfCPM;

        Duco::PerformanceDate firstPealDate;
        Duco::ObjectId firstPealId;
        Duco::PerformanceDate lastPealDate;
        Duco::ObjectId lastPealId;
    };
}

#endif //!__PEALLENGTHINFO_H__
