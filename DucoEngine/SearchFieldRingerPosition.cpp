#include "SearchFieldRingerPosition.h"
#include "DucoEngineUtils.h"

using namespace Duco;


std::wstring
TSearchFieldRingerPosition::Description() const
{
    if (bellNumber == 0 && !fromBack)
    {
        return L"Any bell";
    }
    if (fromBack)
    {
        switch (bellNumber)
        {
        case 0:
                return L"Tenor";
        default:
            {
                wchar_t buffer[10];
                swprintf(buffer, 10, L"N-%d", bellNumber);
                return std::wstring(buffer);
            }
        }
    }
    else
    {
        switch (bellNumber)
        {
        case 1:
                return L"Treble";
        case 0:
            break;
        default:
            {
                wchar_t buffer[10];
                swprintf(buffer, 10, L"%d", bellNumber);
                return std::wstring(buffer);
            }
        }
    }
    return L"Unknown - Error";
}

TSearchFieldRingerPosition::TSearchFieldRingerPosition(const std::wstring& description)
    : bellNumber(0), fromBack(false)
{
    if (description.length() == 0 || DucoEngineUtils::CompareString(description, L"Any bell"))
    {
        bellNumber = 0;
        fromBack = false;
    }
    else if (DucoEngineUtils::CompareString(description, L"Tenor"))
    {
        bellNumber = 0;
        fromBack = true;
    }
    else if (DucoEngineUtils::CompareString(description, L"Treble"))
    {
        bellNumber = 1;
        fromBack = false;
    }
    else if (DucoEngineUtils::FindSubstring(L"N-", description))
    {
        fromBack = true;
        bellNumber = DucoEngineUtils::ToInteger(description[2]);
    }
    else
    {
        fromBack = false;
        bellNumber = DucoEngineUtils::ToInteger(description[0]);
    }
}

bool
TSearchFieldRingerPosition::Match(const unsigned int position, unsigned int order) const
{
    if (bellNumber == 0 && !fromBack)
        return true;

    if (fromBack)
    {
        if ((order - bellNumber) == position)
            return true;
    }
    else if (position == bellNumber)
    {
        return true;
    }
    return false;
}
