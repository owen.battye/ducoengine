#ifndef __CSVIMPORTFIELDS_H__
#define __CSVIMPORTFIELDS_H__

#include <string>

namespace Duco
{
    const std::wstring KIgnoreFirstLineFieldNo (L"ignoreFirstLine");
    const std::wstring KEnforceQuotesFieldNo(L"enforceQuotes");
    const std::wstring KDecodeMultiByteFieldNo(L"decodeMultibyte");

    const std::wstring KTowerNameFieldNo (L"name");
    const std::wstring KTowerTownFieldNo (L"town");
    const std::wstring KTowerCountyFieldNo (L"county");

    const std::wstring KRemoveBellNumberFromRingersFieldNo (L"removeBellNumberFromRingers");
    const std::wstring KFirstRingerFieldNo (L"firstringer");
    const std::wstring KLastRingerFieldNo (L"lastringer");
    const std::wstring KConductorFieldNo (L"conductor");
    const std::wstring KComposerFieldNo (L"composer");
    const std::wstring KConductorTypeFieldNo (L"conductortype");
    const std::wstring KChangesFieldNo (L"changes");
    const std::wstring KAssociationFieldNo (L"association");

    const std::wstring KTenorWeightFieldNo1 (L"tenorweight1");
    const std::wstring KTenorWeightFieldNo2 (L"tenorweight2");
    const std::wstring KTenorWeightFieldNo3 (L"tenorweight3");
    const std::wstring KTenorKeyFieldNo (L"tenorkey");

    const std::wstring KDateSingleFieldNo (L"singleDateField");
    const std::wstring KDayFieldNo (L"day");
    const std::wstring KMonthFieldNo (L"month");
    const std::wstring KYearFieldNo (L"year");

    const std::wstring KTimeSingleFieldNo (L"singleTimeField");
    const std::wstring KHoursFieldNo (L"hours");
    const std::wstring KMinutesFieldNo (L"minutes");

    const std::wstring KMethodSingleFieldNo (L"singleMethodField");
    const std::wstring KMethodFieldNo (L"method");
    const std::wstring KMethodTypeFieldNo (L"methodtype");
    const std::wstring KSplicedMethodsFieldNo (L"spliceddetails");
    const std::wstring KStageFieldNo (L"stage");

    const std::wstring KHandbellFieldNo(L"handbell");
    const std::wstring KHandbellTypeFieldNo(L"handbelltype");
    const std::wstring KHandbellStringFieldNo(L"handbellstring");
    const std::wstring KHandbellStringDefaultValue(L"HB");
    const std::wstring KHandbellStringDefaultValueForPealBook(L"H");

    const std::wstring KFootnotesFieldNo(L"footnotes");
    const std::wstring KBellBoardFieldNo(L"bellboard");
    const std::wstring KRingingWorldFieldNo(L"ringingworld");
}

#endif //!__CSVIMPORTFIELDS_H__
