#ifndef __ABBREVIATIONOBJECT_H__
#define __ABBREVIATIONOBJECT_H__

#include <string>
#include <set>
#include "DucoEngineCommon.h"

namespace Duco
{
    struct sortStrings
    {
        bool operator() (const std::wstring* i, const std::wstring* j) const
        {
            return (i->compare(*j) < 0);
        }
    };

class AbbreviationObject
{
public:
    DllExport AbbreviationObject(const std::wstring& theNames);
    DllExport ~AbbreviationObject();

    bool Valid() const;
    DllExport const std::wstring& DefaultName() const;
    DllExport bool ContainsName(const std::wstring& newname, bool substring) const;
    bool UpdateName(std::wstring& nameToFindOriginal) const;
    DllExport bool operator<(const Duco::AbbreviationObject& obj);

protected:
    void AddName(const std::wstring& newname);

private:
    std::wstring             defaultName;
    std::set<std::wstring*, sortStrings>* otherNames;
    mutable bool            used;
};

}

#endif //!__ABBREVIATIONOBJECT_H__

