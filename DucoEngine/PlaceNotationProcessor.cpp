#include "PlaceNotationProcessor.h"

#include "DucoEngineUtils.h"
#include "PlaceNotationCommon.h"
#include "PlaceNotation.h"
#include "PlaceNotationPlacesObject.h"
#include "PlaceNotationCrossObject.h"
#include "Method.h"

using namespace std;
using namespace Duco;

PlaceNotationProcessor::PlaceNotationProcessor(Duco::Method& newMethod)
:   currentMethod(newMethod), conversionPosition(0)
{

}

PlaceNotationProcessor::~PlaceNotationProcessor()
{
}

bool
PlaceNotationProcessor::MoreNotation()
{
    bool returnVal = conversionPosition < currentMethod.PlaceNotation().length();
    return returnVal;	
}

std::wstring
PlaceNotationProcessor::NextNotation()
{
    size_t stringLength (currentMethod.PlaceNotation().length());
    
    // First determine length of this notation.
    unsigned int startPosition (conversionPosition);
    unsigned int endLength (1);

    bool endFound (false);
    unsigned int previousCharacter (0);
    while (!endFound && (endLength + startPosition) < stringLength)
    {
        char nextCharacter = toupper(currentMethod.PlaceNotation().at(startPosition + endLength - 1));

        endFound = IsEndCharacter(previousCharacter, nextCharacter);

        if (endLength > 1 && IsSpecialCharacter(nextCharacter))
        {
            --endLength;
        }
        if (!endFound)
        {
            ++endLength;
        }
    }
    
    // then generate the string & convert to capitals
    std::wstring nextNotation (currentMethod.PlaceNotation().substr(startPosition, endLength));
    std::wstring uppercaseNextNotation;
    std::wstring::iterator it;
    for ( it=nextNotation.begin(); it < nextNotation.end(); ++it )
    {
        uppercaseNextNotation += toupper(*it);
    }

    conversionPosition += endLength;

    return uppercaseNextNotation;
}

bool
PlaceNotationProcessor::ProcessNotation(PlaceNotation& notations)
{
    notations.DeleteAllNotations();
    bool symetric (false);
    bool notationUpdated (false);
    conversionPosition = 0;
    while (MoreNotation())
    {
        std::wstring nextNotation = NextNotation();
        
        if (nextNotation.length() != 1 || !IsSpecialCharacter(nextNotation[0]))
        { // Add missing numbers either end.
            if (DucoEngineUtils::IsEven(DucoEngineUtils::ToInteger(nextNotation[0])))
            {
                // first number is missing
                nextNotation.insert(0, L"1");
                notationUpdated = true;
            }
            if (! DucoEngineUtils::IsEven (DucoEngineUtils::ToInteger(nextNotation[nextNotation.length()-1])) && DucoEngineUtils::IsEven(currentMethod.Order()))
            {
                // last number is missing
                nextNotation += DucoEngineUtils::ToChar(currentMethod.Order());
                notationUpdated = true;
            }
        }
        
        if (nextNotation.compare(KSymetricNotationSymbolStr) == 0 )
        {
            symetric = true;
        }
        else if (nextNotation.compare(KLeadEndSymbolStr) == 0 && symetric)
        {
            notations.ReverseAndDuplicate();
        }
        else if (nextNotation.compare(KTerminationSymbolStr) != 0)
        {
            notations.AddNotation(CreateNotationObject(nextNotation));
        }
    }
    notationUpdated = notations.SetLeadHeadAndEnd();
    return notationUpdated;
}

bool
PlaceNotationProcessor::IsSpecialCharacter(const wchar_t& aNextCharacter) const
{
    if (aNextCharacter == KSymetricNotationSymbolChar ||
        aNextCharacter == KCrossSymbolChar ||
        aNextCharacter == KTerminationSymbolChar ||
        aNextCharacter == KLeadEndSymbolChar)
    {
        return true;
    }
    return false;
}

bool
PlaceNotationProcessor::IsEndCharacter(unsigned int aPreviousCharacter, const wchar_t& aNextCharacter) const
{
    if (IsSpecialCharacter(aNextCharacter))
        return true;

    unsigned int nextChar (DucoEngineUtils::ToInteger(aNextCharacter));

    if (nextChar < aPreviousCharacter || nextChar == currentMethod.Order())
        {
        return true;
        }

    return false;
}

PlaceNotationObject*
PlaceNotationProcessor::CreateNotationObject(const std::wstring& notation, TChangeType newType)
{
    if (notation.compare(KCrossSymbolStr) == 0)
    {
        return new PlaceNotationCrossObject(newType);
    }
    return new PlaceNotationPlacesObject(notation, newType);
}
