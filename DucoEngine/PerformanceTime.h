#ifndef __PERFORMANCETIME_H__
#define __PERFORMANCETIME_H__

#include <ostream>
#include <string>
#include "DucoEngineCommon.h"

namespace Duco
{
class PerformanceTime
{
    friend class DatabaseWriter;
    friend class PealDatabase;
    friend class PealLengthInfo;
    friend class PerformanceDate;

public:
    DllExport PerformanceTime();
    DllExport PerformanceTime(unsigned int pealTimeInMinutes);
    DllExport PerformanceTime(const Duco::PerformanceTime& other);
    DllExport PerformanceTime(const std::wstring& timeStringStr);
    DllExport ~PerformanceTime();

    DllExport std::wstring TimeForAssociationReports() const;
    DllExport std::wstring PrintPealTime(bool longFormat) const;
    DllExport std::wstring PrintShortPealTime() const;
    DllExport float ChangesPerMinute(unsigned int noOfChanges) const;
    DllExport void Clear();
    
    DllExport bool operator>=(const Duco::PerformanceTime& lhs) const;
    DllExport bool operator<=(const Duco::PerformanceTime& lhs) const;
    DllExport bool operator>(const Duco::PerformanceTime& lhs) const;
    DllExport bool operator<(const Duco::PerformanceTime& lhs) const;
    DllExport bool operator==(const Duco::PerformanceTime& lhs) const;
    DllExport bool operator!=(const Duco::PerformanceTime& lhs) const;
    DllExport bool PealTimeClose(const Duco::PerformanceTime& lhs) const;
    DllExport unsigned int operator-(const Duco::PerformanceTime& lhs) const;

    DllExport std::wostream& PrintTime(std::wostream& stream) const;

    DllExport bool Valid() const;

protected:
    unsigned int pealTimeInMinutes;
};

}

#endif // __PERFORMANCETIME_H__
