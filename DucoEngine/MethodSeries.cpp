#include "MethodSeries.h"

#include "DatabaseReader.h"
#include "DatabaseWriter.h"
#include "DucoEngineUtils.h"
#include "PerformanceDate.h"
#include "Method.h"
#include "MethodDatabase.h"
#include "PealDatabase.h"
#include "RingingDatabase.h"
#include "StatisticFilters.h"

#include <sstream>
#include <cassert>

using namespace Duco;
using namespace std;

#ifndef __ANDROID__
#include "DucoXmlUtils.h"
XERCES_CPP_NAMESPACE_USE
#endif

MethodSeries::MethodSeries(const Duco::ObjectId& newId, const std::wstring& newName, unsigned int newNoOfMethods)
: RingingObject(newId), name(newName), noOfMethods(newNoOfMethods), noOfBells(-1), singleOrder(true)
{

}

MethodSeries::MethodSeries(const MethodSeries& other)
:   RingingObject(other.id), name(other.name), noOfMethods(other.noOfMethods),
    noOfBells(other.noOfBells), singleOrder(other.singleOrder), methods(other.methods)
{

}

MethodSeries::MethodSeries(const Duco::ObjectId& newId, const MethodSeries& other)
:   RingingObject(newId), name(other.name), noOfMethods(other.noOfMethods), 
    noOfBells(other.noOfBells), singleOrder(other.singleOrder), methods(other.methods)
{

}

MethodSeries::MethodSeries(DatabaseReader& reader, unsigned int databaseVersion)
: RingingObject(), name(L""), noOfMethods(0), noOfBells(-1), singleOrder(true)
{
    Internalise(reader, databaseVersion);
}

MethodSeries::MethodSeries()
: RingingObject(), name(L""), noOfMethods(0), noOfBells(-1), singleOrder(true)
{

}

MethodSeries::~MethodSeries()
{

}

DatabaseWriter& 
MethodSeries::Externalise(DatabaseWriter& writer) const
{
    writer.WriteId(id);
    writer.WriteString(name);
    writer.WriteInt(noOfMethods);
    writer.WriteInt(noOfBells);

    writer.WriteInt(methods.size());

    set<Duco::ObjectId>::const_iterator it = methods.begin();
    while (it != methods.end())
    {
        writer.WriteInt(it->Id());
        ++it;
    }
    writer.WriteBool(singleOrder);

    return writer;
}

DatabaseReader& 
MethodSeries::Internalise(DatabaseReader& reader, unsigned int databaseVersion)
{
    if (databaseVersion >= 32)
    {
        reader.ReadId(id);
    }
    reader.ReadString(name);
    noOfMethods = reader.ReadUInt();
    noOfBells = reader.ReadUInt();

    unsigned int noOfActualMethods = reader.ReadUInt();
    while (noOfActualMethods-- > 0)
    {
        unsigned int nextMethId = reader.ReadUInt();
        methods.insert(nextMethId);
    }

    if (databaseVersion >= 11)
        singleOrder = reader.ReadBool();

    return reader;
}

std::wostream&
MethodSeries::Print(std::wostream& stream, const RingingDatabase& database) const
{
    assert(false); // unused
    return stream;
}

Duco::TObjectType
MethodSeries::ObjectType() const
{
    return TObjectType::EMethodSeries;
}

bool
MethodSeries::AddMethods(const std::set<Duco::ObjectId>& methodIds, const Duco::RingingDatabase& database)
{
    bool allAddedOk(true);
    std::set<Duco::ObjectId>::const_iterator it = methodIds.begin();
    while (it != methodIds.end())
    {
        allAddedOk &= AddMethod(*it, database.MethodsDatabase());
        ++it;
    }
    return allAddedOk;
}

bool
MethodSeries::AddMethod(const Duco::ObjectId& methodId, const Duco::RingingDatabase& database)
{
    return AddMethod(methodId, database.MethodsDatabase());
}

bool
MethodSeries::AddMethod(const Duco::ObjectId& methodId, const Duco::MethodDatabase& database)
{
    bool methodAdded (false);
    if (!singleOrder)
    {
        methods.insert(methodId);
        methodAdded = true;
    }
    else if (methods.find(methodId) == methods.end())
    {
        const Method* const theMethod = database.FindMethod(methodId);

        if (theMethod != NULL)
        {
            if (noOfBells == -1)
            {
                noOfBells = theMethod->Order();
            }
            if (noOfBells == theMethod->Order())
            {
                methods.insert(methodId);
                methodAdded = true;
            }
        }
    }
    if (methodAdded && noOfMethods < methods.size())
    {
        noOfMethods = methods.size();
    }
    return methodAdded;
}

void
MethodSeries::RemoveMethod(const Duco::ObjectId& methodId)
{
    set<Duco::ObjectId>::const_iterator it = methods.find(methodId);
    if (it != methods.end())
    {
        methods.erase(it);
    }
}

bool
MethodSeries::ContainsMethod(const Duco::ObjectId& methodId) const
{
    set<Duco::ObjectId>::const_iterator it = methods.find(methodId);
    if (it == methods.end())
    {
        return false;
    }
    return true;
}

bool
MethodSeries::ContainsOneMethodFromList(const std::set<Duco::ObjectId>& methodIds) const
{
    std::set<Duco::ObjectId>::const_iterator it = methodIds.begin();
    while (it != methodIds.end())
    {
        if (ContainsMethod(*it))
            return true;
        ++it;
    }
    return false;
}

bool
MethodSeries::ReplaceMethods(const std::map<Duco::ObjectId, Duco::ObjectId>& changedMethodIds)
{
    std::set<Duco::ObjectId> newMethodIds;

    bool methodIdsChanged (false);
    std::set<Duco::ObjectId>::const_iterator it = methods.begin();
    while (it != methods.end())
    {
        Duco::ObjectId currentMethodId = *it;
        std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator newIdIt = changedMethodIds.find(currentMethodId);
        if (newIdIt != changedMethodIds.end())
        {
            Duco::ObjectId newMethodId = newIdIt->second;
            if (currentMethodId != newMethodId)
            {
                // Method id changed, remember it.
                newMethodIds.insert(newMethodId);
                methodIdsChanged = true;
            }
            else
            {
                // Method id hasn't changed, keep it.
                newMethodIds.insert(currentMethodId);
            }
        }
        else
        {
            // Method id hasn't changed, keep it.
            newMethodIds.insert(currentMethodId);
        }

        ++it;
    }
    if (methodIdsChanged)
    {
        methods.clear();
        methods = newMethodIds;
    }

    return methodIdsChanged;
}

bool
MethodSeries::operator==(const Duco::RingingObject& other) const
{
    if (other.ObjectType() != ObjectType())
        return false;

    const Duco::MethodSeries& otherMethodSeries = static_cast<const Duco::MethodSeries&>(other);

    if (name.compare(otherMethodSeries.name) != 0)
        return false;

    if (noOfMethods != otherMethodSeries.noOfMethods)
        return false;

    if (methods != otherMethodSeries.methods)
        return false;

    if (noOfBells != otherMethodSeries.noOfBells)
        return false;

    if (singleOrder != otherMethodSeries.singleOrder)
        return false;

    return true;
}

bool
MethodSeries::operator!=(const Duco::RingingObject& other) const
{
    if (other.ObjectType() != ObjectType())
        return true;

    const Duco::MethodSeries& otherMethodSeries = static_cast<const Duco::MethodSeries&>(other);

    if (name.compare(otherMethodSeries.name) != 0)
        return true;

    if (noOfMethods != otherMethodSeries.noOfMethods)
        return true;

    if (methods != otherMethodSeries.methods)
        return true;

    if (noOfBells != otherMethodSeries.noOfBells)
        return true;

    if (singleOrder != otherMethodSeries.singleOrder)
        return true;

    return false;
}

MethodSeries&
MethodSeries::operator=(const Duco::MethodSeries& otherMethodSeries)
{
    id = otherMethodSeries.id;
    name = otherMethodSeries.name;
    noOfMethods = otherMethodSeries.noOfMethods;
    noOfBells = otherMethodSeries.noOfBells;
    methods = otherMethodSeries.methods;
    singleOrder = otherMethodSeries.singleOrder;
    errorCode = otherMethodSeries.errorCode;

    return *this;
}

bool
MethodSeries::SeriesCompletedDate(const RingingDatabase& database, Duco::PerformanceDate& completedDate, size_t& unrungMethods, Duco::PealLengthInfo& allPealsInfo) const
{
    unrungMethods = NoOfMethods() - NoOfExistingMethods();
    allPealsInfo.Clear();
    completedDate.ResetToEarliest();

    std::set<Duco::ObjectId>::const_iterator methodIt = methods.begin();
    bool allMethodsRung = true;
    Duco::PerformanceDate seriesMethodRungDate;
    seriesMethodRungDate.ResetToEarliest();
    while (methodIt != methods.end())
    {
        StatisticFilters filters(database);
        filters.SetMethod(true, *methodIt);
        Duco::PealLengthInfo info = database.PealsDatabase().PerformanceInfo(filters);
        if (info.TotalPeals() > 0)
        {
            if (seriesMethodRungDate < info.DateOfFirstPeal())
            {
                seriesMethodRungDate = info.DateOfFirstPeal();
            }
            allPealsInfo += info;
        }
        else
        {
            allMethodsRung = false;
            ++unrungMethods;
        }
        ++methodIt;
    }

    if (unrungMethods > 0)
    {
        allMethodsRung = false;
    }

    if (allMethodsRung)
        completedDate = seriesMethodRungDate;
    else
        completedDate.ResetToEarliest();

    return allMethodsRung;
}

bool
MethodSeries::SetSingleOrder(bool singleOrder, const Duco::RingingDatabase& database)
{
    return SetSingleOrder(singleOrder, database.MethodsDatabase());
}

bool
MethodSeries::SetSingleOrder(bool newSingleOrder, const Duco::MethodDatabase& database)
{
    if (newSingleOrder)
    {
        std::set<Duco::ObjectId>::const_iterator methodIt = methods.begin();
        bool singleOrderMethods (true);
        unsigned int noOfBells (-1);
        while (singleOrderMethods && methodIt != methods.end())
        {
            const Method* const theMethod = database.FindMethod(*methodIt);
            if (theMethod == NULL)
            {
                singleOrderMethods = false;
            }
            else
            {
                unsigned int thisMethodNoOfBells = theMethod->Order();
                if (noOfBells == -1)
                    noOfBells = thisMethodNoOfBells;
                else if (noOfBells != thisMethodNoOfBells)
                {
                    singleOrderMethods = false;
                }
            }
            ++methodIt;
        }
        if (!singleOrderMethods)
            return false;
    }

    singleOrder = newSingleOrder;
    return true;
}

bool
MethodSeries::Valid(const Duco::RingingDatabase& ringingDb, bool warningFatal, bool clearBeforeStart) const
{
    return Valid(ringingDb.MethodsDatabase(), warningFatal, clearBeforeStart);
}

bool
MethodSeries::Valid(const Duco::MethodDatabase& methodDb, bool warningFatal, bool clearBeforeStart) const
{
    if (clearBeforeStart)
    {
        ClearErrors();
    }
    bool returnVal (true);

    if (name.length() <= 0)
    {
        errorCode.set(ESeries_NameMissing, 1);
        returnVal = false;
    }
    if (methods.size() != noOfMethods)
    {
        errorCode.set(ESeriesWarning_MoreMethodsThanDefined, 1);
        if (warningFatal)
            returnVal = false;
    }

    std::set<Duco::ObjectId>::const_iterator it = methods.begin();
    while (it != methods.end())
    {
        const Method* nextMethod = methodDb.FindMethod(*it);
        if (nextMethod == NULL)
        {
            errorCode.set(ESeries_InvalidMethodInSeries, 1);
            returnVal = false;
        }
        else
        {
            if (singleOrder)
            {
                if (nextMethod->Order() != noOfBells)
                {
                    errorCode.set(ESeries_MethodInSeriesWithMismatchingNoOfBells, 1);
                    returnVal = false;
                }
            }
        }
        ++it;
    }

    return returnVal;
}

std::wstring
MethodSeries::FullNameForSort(bool unusedSetting) const
{
    std::wstring sortableName = DucoEngineUtils::PadStringWithZeros(DucoEngineUtils::ToString(NoOfMethods()));
    sortableName += L" ";
    wostringstream strStream;
    strStream << DucoEngineUtils::PadStringWithZeros(DucoEngineUtils::ToString(noOfBells));
    sortableName += L" ";
    sortableName.append(strStream.str());

    return sortableName + Name();
}

std::wstring
MethodSeries::MethodNames(const Duco::MethodDatabase& methodDb) const
{
    std::wstring returnVal;
    std::multimap<std::wstring, std::wstring> methodTypesToNames;

    std::set<Duco::ObjectId>::const_iterator methodIt = methods.begin();
    while (methodIt != methods.end())
    {
        const Method* const theMethod = methodDb.FindMethod(*methodIt);
        if (theMethod != NULL)
        {
            std::pair<std::wstring, std::wstring> newObject (theMethod->Type(), theMethod->Name());
            methodTypesToNames.insert(newObject);
        }
        ++methodIt;
    }

    std::multimap<std::wstring, std::wstring>::const_iterator it = methodTypesToNames.begin();

    while (it != methodTypesToNames.end())
    {
        std::wstring lastKey = it->first;
        size_t noOfMethodsWithThisKey = methodTypesToNames.count(lastKey);

        std::wstring thisMethodTypeStr;
        for (size_t count(1); count <= noOfMethodsWithThisKey; ++count)
        {
            if (count == noOfMethodsWithThisKey)
            {
                if (thisMethodTypeStr.length() > 0)
                    thisMethodTypeStr += L" and ";
            }
            else if (count > 1)
                thisMethodTypeStr += L", ";
            thisMethodTypeStr += it->second;
            ++it;
        }
        thisMethodTypeStr += L" ";
        thisMethodTypeStr += lastKey;

        if (returnVal.length() > 0)
        {
            if (it == methodTypesToNames.end())
            {
                returnVal += L" and ";
            }
            else
            {
                returnVal += L", ";
            }
        }
        returnVal += thisMethodTypeStr;
    }

    return returnVal;
}

std::wstring
MethodSeries::ErrorString(const Duco::DatabaseSettings& /*settings*/, bool showWarnings) const
{
    std::wstring returnVal;
    if (errorCode.test(ESeries_NameMissing))
    {
        DucoEngineUtils::AddError(returnVal, L"Series missing name");
    }
    if (errorCode.test(ESeries_InvalidMethodInSeries))
    {
        DucoEngineUtils::AddError(returnVal, L"Invalid method");
    }
    if (errorCode.test(ESeries_MethodInSeriesWithMismatchingNoOfBells))
    {
        DucoEngineUtils::AddError(returnVal, L"Number of bells in method doesnt match bells in rest of series");
    }
    if (showWarnings && errorCode.test(ESeriesWarning_MoreMethodsThanDefined))
    {
        DucoEngineUtils::AddError(returnVal, L"Not all methods defined");
    }
    if (errorCode.test(ESeries_DuplicateSeries))
    {
        DucoEngineUtils::AddError(returnVal, L"Identical methods to another series");
    }

    return returnVal;
}

void
MethodSeries::SetError(TMethodSeriesErrorCodes code)
{
    errorCode.set(code,1);
}

void
MethodSeries::ExportToXml(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& methodSeries, bool exportDucoObjects) const
{
    DOMElement* newSeries = outputFile.createElement(XMLStrL("MethodSeries"));
    methodSeries.appendChild(newSeries);

    DucoXmlUtils::AddObjectId(*newSeries, id);

    DucoXmlUtils::AddElement(outputFile, *newSeries, L"NumberOfBells", noOfBells);
    DucoXmlUtils::AddElement(outputFile, *newSeries, L"Name", name);
    DucoXmlUtils::AddElements(outputFile, *newSeries, L"MethodIds", L"Method", methods);

    DucoXmlUtils::AddAttribute(*newSeries, L"NumberOfMethods", noOfMethods);
    DucoXmlUtils::AddAttribute(*newSeries, L"Stage", noOfBells);
}

bool
MethodSeries::Duplicate(const Duco::RingingObject& other, const Duco::RingingDatabase& database) const
{
    if (other.ObjectType() != ObjectType())
        return false;

    const Duco::MethodSeries& otherSeries = static_cast<const Duco::MethodSeries&>(other);

    if (otherSeries.methods != this->methods)
    {
        return false;
    }
    return true;
}
