#include "PealBasePbrImporter.h"

#include "AssociationDatabase.h"
#include "RingingDatabase.h"
#include "TowerDatabase.h"
#include "PealDatabase.h"
#include "MethodDatabase.h"
#include "RingerDatabase.h"
#include "DucoEngineUtils.h"
#include "Peal.h"
#include "Tower.h"

using namespace std;
using namespace Duco;

PealBasePbrImporter::PealBasePbrImporter(Duco::ProgressCallback& theCallback, Duco::RingingDatabase& theDatabase)
:    FileImporter(theCallback, theDatabase)
{
}

PealBasePbrImporter::~PealBasePbrImporter()
{
}

void
PealBasePbrImporter::GetTotalFileSize(const std::string& fileName)
{
    totalFileSize = GetFileSize(fileName);
}

bool
PealBasePbrImporter::ProcessPeal(const std::wstring& fileLine)
{
    std::vector<std::wstring> tokens;
    TokeniseLine(fileLine, tokens, true, false);

    Peal newPeal;

    std::vector<std::wstring>::const_iterator it = tokens.begin();
    unsigned int fieldNo (0);
    unsigned int ringerNo (0);

    std::wstring towerDedication;
    std::wstring towerCity;
    std::wstring towerCounty;
    std::wstring tenorWeight;
    bool newTowerRequired (false);
    unsigned int noOfBells (-1);

    while (it != tokens.end() && !cancelled)
    {
        switch (fieldNo)
        {
        case 0:
            {
            unsigned int pealType = DucoEngineUtils::ToInteger(*it);
            if (pealType >= 2)
                {
                newPeal.SetHandbell(true);
                }
            }
            break;

        case 1: // Conducted type
            {
            TConductorType conductedType = static_cast<TConductorType>(DucoEngineUtils::ToInteger(*it));
            newPeal.SetConductedType(conductedType);
            }
            break;

        case 2:
        {
            Duco::ObjectId associationId = database.AssociationsDatabase().SuggestAssociationOrCreate(*it);
            newPeal.SetAssociationId(associationId);
            }
            break;

        case 3: // Dedication
            towerDedication = *it;
            break;
        case 4: // Tower
            towerCity = *it;
            break;

        case 5: // County
            {
                towerCounty = *it;
            }
            break;

        case 6: // Tenor
            tenorWeight = *it;
            break;

        case 7: // Tenor units
            if ((*it).length() > 0)
            {
                tenorWeight += L" ";
                tenorWeight += *it;
            }
            break;

        case 8: // Date
            {
                Duco::PerformanceDate pealDate(*it);
                newPeal.SetDate(pealDate);
            }
            break;

        case 9: // peal time
            {
                Duco::PerformanceTime pealTime (*it);
                newPeal.SetTime(pealTime);
            }
            break;
        case 10: // Changes
            {
                unsigned int noOfChanges = DucoEngineUtils::ToInteger(*it);
                newPeal.SetChanges(noOfChanges);
            }
            break;

        case 11: //Method
            {
                newPeal.SetMethodId(database.MethodsDatabase().FindOrCreateMethod(*it, noOfBells));
                if (!DucoEngineUtils::IsEven(noOfBells))
                    ++noOfBells;
            }
            break;

        case 12: // Spliced methods
            newPeal.SetMultiMethods(*it);
            break;

        case 13:
            newPeal.SetComposer(*it);
            break;

        case 15: //Ringers
        case 17: //Ringers
        case 19: //Ringers
        case 21: //Ringers
        case 23: //Ringers
        case 25: //Ringers
        case 27: //Ringers
        case 29: //Ringers
        case 31: //Ringers
        case 33: //Ringers
            if (newPeal.Handbell())
                break;

        case 14: //Ringers
        case 16: //Ringers
        case 18: //Ringers
        case 20: //Ringers
        case 22: //Ringers
        case 24: //Ringers
        case 26: //Ringers
        case 28: //Ringers
        case 30: //Ringers
        case 32: //Ringers
            if ((*it).length() > 0)
            {
                unsigned int bellNo(fieldNo - 13);
                ++ringerNo;
                FindOrCreateNewRinger(newPeal, *it, bellNo, false);

            }
            break;

        case 34: //Strapper
            if ((*it).length() > 0)
            {
                FindOrCreateNewRinger(newPeal, *it, ringerNo, true);
            }
            break;

        case 35: //Conductor Bell no.
            {
                unsigned int conductorsBell (DucoEngineUtils::ToInteger(*it));
                if (newPeal.Handbell())
                {
                    ++conductorsBell /= 2;
                }
                newPeal.SetConductorIdFromBell(conductorsBell, true, false);
            }
            break;

        case 36: //Footnotes
            newPeal.SetFootnotes(*it);
            break;

        default:
            break;
        }
        ++fieldNo;
        ++it;
    }
    unsigned int noOfBellsInPeal(ringerNo);
    if (newPeal.Handbell())
        noOfBellsInPeal *= 2;

    ObjectId towerId = database.TowersDatabase().SuggestTower(towerDedication, towerCity, towerCounty, noOfBellsInPeal, tenorWeight, L"", true);
    if (towerId.ValidId())
    {
        newPeal.SetTowerId(towerId);
    }
    else
    {
        newTowerRequired = true;
    }

    if (newTowerRequired)
    {
        Tower newTower(KNoId,towerDedication, towerCity, towerCounty, noOfBellsInPeal);
        ObjectId newRingId = newTower.AddDefaultRing(Tower::DefaultRingName(noOfBellsInPeal), tenorWeight);
        ObjectId newTowerId = database.TowersDatabase().AddObject(newTower);
        newPeal.SetTowerId(newTowerId);
        newPeal.SetRingId(newRingId);
    }
    else
    {
        Tower theTower (*database.TowersDatabase().FindTower(newPeal.TowerId()));
        ObjectId ringId (0);
        if (theTower.SuggestRing(noOfBellsInPeal, tenorWeight, L"", ringId, true))
        {
            newPeal.SetRingId(ringId);
        }
        else
        {
            ObjectId newRingId = theTower.AddRing(noOfBellsInPeal, tenorWeight, L"");
            database.TowersDatabase().UpdateObject(theTower);
            newPeal.SetRingId(newRingId);
        }
    }

    database.PealsDatabase().AddObject(newPeal);
    return newPeal.Valid(database, false, true);
}

bool
PealBasePbrImporter::FindOrCreateNewRinger(Duco::Peal& newPeal, const std::wstring& ringerStr, unsigned int bellNo, bool asStrapper)
{
    if (newPeal.Handbell())
    {
        bellNo = (bellNo + 1) / 2;
    }

    bool ringerAdded (false);
    ObjectId existingRingerId = database.RingersDatabase().FindIdenticalRinger(ringerStr);
    if (existingRingerId.ValidId())
    {
        newPeal.SetRingerId(existingRingerId, bellNo, asStrapper);
        ringerAdded = true;
    }
    else
    {
        ObjectId newRingerId = database.RingersDatabase().AddRinger(ringerStr);
        if (newRingerId.ValidId())
        {
            newPeal.SetRingerId(newRingerId, bellNo, asStrapper);
            ringerAdded = true;
        }
    }

    return ringerAdded;
}

bool
PealBasePbrImporter::EnforceQuotes() const
{
    return true;
}
