#ifndef __RINGINGOBJECT_H__
#define __RINGINGOBJECT_H__

#include "DucoEngineCommon.h"
#include "DucoErrorCodes.h"
#include "ObjectType.h"
#include "ObjectId.h"
#include <bitset>
#include <string>

#include "xercesc\dom\DOMElement.hpp"

namespace Duco
{
    class DatabaseWriter;
    class DatabaseReader;
    class DatabaseSettings;
    class RingingDatabase;

class RingingObject
{
    friend class RingingDatabase;
    friend class RingingObjectDatabase;
    friend class TowerDatabase;
    friend class Tower;

public:
    virtual ~RingingObject();

    virtual Duco::TObjectType ObjectType() const = 0;
    DllExport virtual bool Duplicate(const Duco::RingingObject& other, const Duco::RingingDatabase& database) const = 0;
    DllExport virtual std::wstring FullNameForSort(bool unusedSetting) const;

    // Valid, when saving a warning shouldn't be fatal, but it should the rest of the time.
    virtual bool Valid(const RingingDatabase& database, bool warningFatal, bool clearBeforeStart) const = 0;
    DllExport virtual void ClearErrors() const;
    virtual std::wstring ErrorString(const Duco::DatabaseSettings& settings, bool showWarnings) const = 0;
    DllExport virtual const std::bitset<KMaxErrorBitsetSize> ErrorCode() const;

    //Accessors
    DllExport const Duco::ObjectId& Id() const;
    DllExport virtual bool RebuildRecommended() const;

    // I/O
    virtual std::wostream& Print(std::wostream& stream, const RingingDatabase& database) const = 0;
    virtual DatabaseWriter& Externalise(DatabaseWriter& writer) const = 0;
    virtual void ExportToXml(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& parent, bool exportDucoObjects) const = 0;

protected:
    RingingObject(const RingingObject& other);
    RingingObject(const ObjectId& newId);
    RingingObject();
    virtual DatabaseReader& Internalise(DatabaseReader& reader, unsigned int databaseVersion) = 0;
    void SetId(const Duco::ObjectId& newId);

    Duco::ObjectId id;

    // not stored
    mutable std::bitset<KMaxErrorBitsetSize> errorCode;
};

} // end namespace Duco

#endif //__RINGINGOBJECT_H__
