#include "UnpaidPealFeeData.h"

using namespace Duco;

UnpaidPealFeeData::UnpaidPealFeeData(const Duco::ObjectId& newPealId, const Duco::ObjectId& newFeePayerId, unsigned int newPaidInPence, unsigned int newRemainingInPence)
    : pealId(newPealId), feePayersRingerId(newFeePayerId), paidInPence(newPaidInPence), remainingInPence(newRemainingInPence)
{

}

bool
UnpaidPealFeeData::operator<(const Duco::UnpaidPealFeeData& rhs) const
{
    if (pealId != rhs.pealId)
        return pealId < rhs.pealId;

    if (paidInPence != rhs.paidInPence)
        return paidInPence < rhs.paidInPence;

    if (remainingInPence != rhs.remainingInPence)
        return remainingInPence < rhs.remainingInPence;

    return feePayersRingerId < rhs.feePayersRingerId;
}
