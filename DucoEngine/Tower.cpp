﻿#include "Tower.h"

#include "DatabaseReader.h"
#include "DatabaseWriter.h"
#include "DoveDatabase.h"
#include "DoveObject.h"
#include "DucoEngineUtils.h"
#include "MethodDatabase.h"
#include "PealDatabase.h"
#include "RingingDatabase.h"
#include "Ring.h"
#include "StatisticFilters.h"

#include "DucoXmlUtils.h"
XERCES_CPP_NAMESPACE_USE

using namespace std;
using namespace Duco;

#define PoundsInQuarter 28
#define QuartersInCwt 4
#define KRingNameSuffix L" bell peal"
#define KTenorKGSuffix L"kg"

Tower::Tower()
:   RingingObject(), name(L""), city(L""), county(L""), notes (L""), doveReference(L""),
    latitude(L""), longitude(L""), aka(L""), handbell(false), noOfBells(12), removed(false), towerbaseId(L""), antiClockwise(false)
{
    firstRungDate.ResetToEarliest();
}

Tower::Tower(DatabaseReader& reader, unsigned int databaseVersion)
:   RingingObject(), name(L""), city(L""), county(L""), notes (L""), doveReference(L""),
    latitude(L""), longitude(L""), aka(L""), handbell(false), noOfBells(0), removed(false), towerbaseId(L""), antiClockwise(false)
{
    firstRungDate.ResetToEarliest();
    Internalise(reader, databaseVersion);
}

Tower::Tower(const Duco::ObjectId& newId, const std::wstring& newName, const std::wstring& newCity, const std::wstring& newCounty, unsigned int newBells)
:   RingingObject(newId), name(newName), city(newCity), county(newCounty), notes(L""), doveReference(L""),
    latitude(L""), longitude(L""), aka(L""), handbell(false), noOfBells(newBells), removed(false), towerbaseId(L""), antiClockwise(false)
{
    firstRungDate.ResetToEarliest();
}

Tower::Tower(const Tower& other)
: RingingObject(other), name(other.name), city(other.city), county(other.county), notes(other.notes), doveReference(other.doveReference),
    latitude(other.latitude), longitude(other.longitude), aka(other.aka), handbell(other.handbell), noOfBells(other.noOfBells),
    firstRungDate(other.firstRungDate), removed(other.removed), antiClockwise(other.antiClockwise), renamedBells(other.renamedBells),
    towerbaseId(other.towerbaseId), linkedTowerId(other.linkedTowerId), pictureId(other.pictureId)
{
    CopyAllRings(other.rings);
}

Tower::Tower(const Duco::ObjectId& newId, const Tower& other)
:   RingingObject(newId), name(other.name), city(other.city), county(other.county), notes(other.notes), doveReference(other.doveReference),
    latitude(other.latitude), longitude(other.longitude), aka(other.aka), handbell(other.handbell), noOfBells(other.noOfBells),
    firstRungDate(other.firstRungDate), removed(other.removed), antiClockwise(other.antiClockwise), renamedBells(other.renamedBells),
    towerbaseId(other.towerbaseId), linkedTowerId(other.linkedTowerId), pictureId(other.pictureId)
{
    CopyAllRings(other.rings);
}

Tower::Tower(const DoveObject& other)
:   name(other.Dedication()), city(other.City()), county(other.County()), notes(L""), doveReference(other.DoveId()),
    aka(other.AltName()), handbell(false), noOfBells(other.Bells()), removed(false), antiClockwise(other.AntiClockwise()), towerbaseId(other.TowerbaseId())
{
    latitude = DucoEngineUtils::ToString(other.Latitude());
    longitude = DucoEngineUtils::ToString(other.Longitude());
    firstRungDate.ResetToEarliest();

    if (other.Name().length() > 0)
    {
        name.append(L", ");
        name.append(other.Name());
    }
    if (other.GroundFloor())
    {
        notes = L"Ground floor ringing chamber";
    }
    if (other.WebPage().length() > 0)
    {
        if (notes.length() > 0)
            notes += KNewlineChar;
        notes += other.WebPage();
    }
    if (!other.UKorIreland())
    {
        if (county.length() > 0)
            county += L", ";

        county += other.Country();
    }

    if (other.HasSemiTones())
    {
        noOfBells += other.NumberOfSemiTones();
        renamedBells = other.SemiToneBells();
    }
    SetTowerbaseId(other.TowerbaseId());
    AddRing(noOfBells - other.NumberOfSemiTones(), other.Tenor(), other.TenorKey());
}

Tower::~Tower()
{
    DeleteAllRings();
}

void
Tower::CopyAllRings(const std::map<Duco::ObjectId, Duco::Ring*>& otherRings)
{
    DeleteAllRings();
    std::map<Duco::ObjectId, Duco::Ring*>::const_iterator it = otherRings.begin();
    while (it != otherRings.end())
    {
        Ring* newRing = new Ring(*it->second);
        AddRing(newRing, false);
        ++it;
    }
}


Duco::TObjectType
Tower::ObjectType() const
{
    return TObjectType::ETower;
}

bool
Tower::AddRing(Duco::Ring* ring, bool fromImport)
{
    std::pair<Duco::ObjectId, Duco::Ring*> newObject(ring->Id(), ring);
    if (rings.insert(newObject).second)
    {
        if (fromImport && ring->NoOfBells() > noOfBells)
        {
            noOfBells = ring->NoOfBells();
        }
        return true;
    }
    else
    {
        delete ring;
    }
    return false;
}

Duco::ObjectId
Tower::AddRing(unsigned int noOfBellsInRing, const std::wstring& tenorWeight, const std::wstring& tenorKey)
{
    Duco::ObjectId newRingObjectId;
    if (noOfBells < noOfBellsInRing)
    {
        SetNoOfBells(noOfBellsInRing);
    }
    std::set<unsigned int> newBells;
    
    unsigned int noOfBellsToAdd(noOfBellsInRing);
    while (noOfBellsToAdd > 0)
    {
        newBells.insert(noOfBells-noOfBellsToAdd+1);
        --noOfBellsToAdd;
    }
    Ring* newRing = new Ring(NextFreeRingId(), noOfBellsInRing, DefaultRingName(noOfBellsInRing), newBells, tenorWeight, tenorKey);
    if (AddRing(newRing, true))
        newRingObjectId = newRing->Id();

    return newRingObjectId;
}

std::wstring
Tower::DefaultRingName(unsigned int noOfBellsInRing)
{
    std::wstring newRingName = DucoEngineUtils::ToString(noOfBellsInRing);
    newRingName.append(KRingNameSuffix);
    return newRingName;
}


bool
Tower::AddRing(const Duco::Ring& ring, bool fromImport)
{
    Ring* newRing = new Ring(ring);
    return AddRing(newRing, fromImport);
}

bool
Tower::AddRings(const Duco::Tower& otherTwr)
{
    Duco::Tower towerCopy (otherTwr);
    if (towerCopy.noOfBells > noOfBells)
        SetNoOfBells(towerCopy.Bells());
    else
        towerCopy.SetNoOfBells(noOfBells);

    bool updated (false);
    std::map<Duco::ObjectId, Duco::Ring*>::const_iterator it = towerCopy.rings.begin();
    while (it != towerCopy.rings.end())
    {
        if (!FindSimilarRing(*it->second).ValidId())
        {
            Ring* newRing = new Ring(NextFreeRingId(), *it->second);
            updated |= AddRing(newRing, false);
        }
        ++it;
    }
    return updated;
}

void
Tower::DeleteAllRings()
{
    std::map<Duco::ObjectId, Duco::Ring*>::iterator it = rings.begin();
    while (it != rings.end())
    {
        delete it->second;
        ++it;
    }
    rings.clear();
}

bool
Tower::DeleteRing(const Duco::ObjectId& ringId)
{
    std::map<Duco::ObjectId, Duco::Ring*>::iterator it = rings.find(ringId);
    if (it != rings.end())
    {
        delete it->second;
        rings.erase(it);
        return true;
    }
    return false;
}

bool
Tower::DeleteRings(const std::vector<Duco::ObjectId>& ringIds)
{
    bool changeMade (false);
    for (auto const& it : ringIds)
    {
        std::map<Duco::ObjectId, Duco::Ring*>::iterator it2 = rings.find(it);
        if (it2 != rings.end())
        {
            delete it2->second;
            rings.erase(it2);
            changeMade = true;
        }
    }
    return changeMade;
}

void
Tower::CheckForDefaultRing()
{
    if (rings.empty())
    {
        AddDefaultRing();
    }
}

Duco::ObjectId
Tower::AddDefaultRing(const std::wstring& newRingName, const std::wstring& tenorWeightStr, const std::wstring& tenorKey)
{
    Duco::ObjectId newRingObjectId;

    std::set<unsigned int> newBells;
    for (unsigned int count (1); count <= noOfBells; ++count)
    {
        newBells.insert(count);
    }
    if (newRingName.length() <= 0)
    {
        Duco::Ring theRing(rings.size(), noOfBells, DefaultRingName(noOfBells), newBells, tenorWeightStr, tenorKey);
        if (AddRing(theRing))
            newRingObjectId = theRing.Id();
    }
    else
    {
        Duco::Ring theRing(rings.size(), noOfBells, newRingName, newBells, tenorWeightStr, tenorKey);
        if (AddRing(theRing))
            newRingObjectId = theRing.Id();
    }
    return newRingObjectId;
}

Duco::ObjectId
Tower::NextFreeRingId() const
{
    Duco::ObjectId nextPossibleId = rings.size();
    std::map<Duco::ObjectId, Duco::Ring*>::const_iterator it = rings.find(nextPossibleId);

    while (it != rings.end())
    {
        ++nextPossibleId;
        it = rings.find(nextPossibleId);
    }

    return nextPossibleId;
}

const Duco::Ring* const
Tower::FindRing(const Duco::ObjectId& ringId) const
{
    std::map<Duco::ObjectId, Duco::Ring*>::const_iterator it = rings.find(ringId);
    if (it != rings.end())
    {
        return it->second;
    }

    return NULL;
}

Duco::ObjectId
Tower::FindSimilarRing(const Ring& otherRing) const
{
    Duco::ObjectId foundRingId;

    for (auto const& [key,value] : rings)
    {
        if (value->Duplicate(otherRing))
            return value->Id();
    }
    return foundRingId;
}

size_t
Tower::RingIndex(const Duco::ObjectId& ringId) const
{
    size_t index = 0;
    for (auto const& [key, value] : rings)
    {
        if (value->Id() == ringId)
        {
            return index;
        }
        ++index;
    }
    return -1;
}

const Duco::Ring* const
Tower::FindRingByIndex(size_t index) const
{
    std::map<Duco::ObjectId, Duco::Ring*>::const_iterator it = rings.begin();
    for (size_t i(0); it != rings.end(); ++i)
    {
        if (i == index)
        {
            return it->second;
        }
        ++it;
    }
    return NULL;
}

bool
Tower::SuggestRing(unsigned int noOfBells, const std::wstring& tenorWeight, const std::wstring& tenorKey, Duco::ObjectId& ringId, bool fromImport) const
{
    unsigned int ringMatchProbability = 0;
    if (!SuggestRing(noOfBells, tenorWeight, tenorKey, ringId, ringMatchProbability))
    {
        return false;
    }
    if (fromImport && ringMatchProbability < 4)
    {
        if (NoOfRings(noOfBells) != 1 || ringMatchProbability < 2)
        {
            ringId.ClearId();
            return false;
        }
    }
    return ringId.ValidId();
}

unsigned int
Tower::TenorKeyMatchProbability(const std::wstring& _tenorKeyRequired, const std::wstring& tenorKeyFromARing) const
{
    if (_tenorKeyRequired.length() == 0 || tenorKeyFromARing.length() == 0)
        return 0;

    std::wstring tenorKeyRequired = DucoEngineUtils::ReplaceTenorKeySymbols(_tenorKeyRequired);

    unsigned int ringMatchProbability = 0;
    if (tenorKeyFromARing == tenorKeyRequired)
    {
        ringMatchProbability += 2;
        if (tenorKeyRequired.length() > 1 && tenorKeyFromARing.length() > 1)
            ++ringMatchProbability;
    }
    else if (tenorKeyFromARing[0] == tenorKeyRequired[0])
    {
        ++ringMatchProbability;
    }
    return ringMatchProbability;
}

unsigned int
Tower::TenorWeightMatchProbability(const std::wstring& tenorRequired, const std::wstring& tenorInARing) const
{

    unsigned int tenorMatchProbability = 0;
    if (tenorRequired.length() >= 0)
    {
        if (DucoEngineUtils::CompareString(tenorRequired, tenorInARing))        
        {
            tenorMatchProbability += 2;
        }
        else if (DucoEngineUtils::CompareTenorWeight(tenorRequired, tenorInARing))
        {
            tenorMatchProbability += 1;
        }
    }
    return tenorMatchProbability;
}

bool
Tower::SuggestRing(unsigned int noOfBellsRequired, const std::wstring& tenorWeightRequired, const std::wstring& tenorKeyRequired, Duco::ObjectId& ringIdFound, unsigned int& ringMatchProbability) const
{
    ringMatchProbability = 0;
    ringIdFound.ClearId();
    Duco::ObjectId singleRingWithNoOfBells;

    unsigned int highestProbability = 1;

    for (auto const& [key,value] : rings)
    {
        unsigned int matchProbability = 0;
        if (value->NoOfBells() == noOfBellsRequired)
        {
            if (singleRingWithNoOfBells.ValidId())
            {
                singleRingWithNoOfBells.ClearId();
                matchProbability = 0;
            }
            else if (tenorWeightRequired.length() == 0 && tenorKeyRequired.length() == 0)
            {
                // On import especially from CSV no tenor weight is given - so if we have a ring with the right no of bells and no tenor weight - then we have a match.
                singleRingWithNoOfBells = key;
            }
            matchProbability += 2;
            if (tenorWeightRequired.length() > 0)
                matchProbability += TenorWeightMatchProbability(tenorWeightRequired, value->TenorWeight());
            //else if (noOfBellsRequired == noOfBells)  // bells in tower - not ring
            //    matchProbability += 1;

            if (tenorKeyRequired.length() > 0)
                matchProbability += TenorKeyMatchProbability(tenorKeyRequired, value->TenorKey());
            //else if (noOfBellsRequired == noOfBells) // bells in tower - not ring
            //    matchProbability += 1;

            if (matchProbability > 4)
                ++matchProbability; // right no of bells and key.... gotta be a good chance.
        }
        else if (value->NoOfBells() > noOfBellsRequired)
        {
            matchProbability += 1;
            matchProbability += TenorWeightMatchProbability(tenorWeightRequired, value->TenorWeight());
            matchProbability += TenorKeyMatchProbability(tenorKeyRequired, value->TenorKey());
        }
        if (matchProbability > highestProbability)
        {
            highestProbability = matchProbability;
            ringIdFound = key;
        }
        else if (matchProbability == highestProbability && matchProbability < 7)
        { // Dont clear 7 or more - coz thats a good match and the chances are this is just a duplciated ring - so take the first one.
            ringIdFound.ClearId();
            highestProbability = 0;
        }
    }
    if (singleRingWithNoOfBells.ValidId())
    {
        ringMatchProbability = highestProbability;
        ringIdFound = singleRingWithNoOfBells;
        return true;
    }
    if (highestProbability < 3)
    {
        return false;
    }
    ringMatchProbability = highestProbability;
    return ringIdFound.ValidId();
}

bool
Tower::FindSimilarRing(const Duco::Ring& otherRing, Duco::ObjectId& ringId) const
{
    ringId.ClearId();

    for (auto const& [key, value] : rings)
    {
        if (value->Duplicate(otherRing))
        {
            ringId = key;
            return true;
        }
    }

    return false;
}

bool
Tower::GetRingNameWithNoOfBells(unsigned int noOfBells, std::wstring& ringName) const
{
    ringName.clear();
    for (auto const& [key,value] : rings)
    {
        if (value->NoOfBells() == noOfBells)
        {
            ringName = value->Name();
            return true;
        }
    }
    return false;
}

bool
Tower::FirstInvalidRingId(Duco::ObjectId& ringId) const
{
    ringId.ClearId();
    bool startedInMiddle (true);
    std::map<Duco::ObjectId, Duco::Ring*>::const_iterator it = rings.find(ringId);
    if (it == rings.end())
    {
        it = rings.begin();
        startedInMiddle = false;
    }
    while (it != rings.end())
    {
        if (!it->second->Valid(renamedBells, MaxNoOfBellsInRing(), noOfBells, false, true))
        {
            errorCode |= it->second->ErrorCode();
            ringId = it->first;
            return true;
        }
        ++it;
    }

    if (startedInMiddle)
    {
        it = rings.begin();
        while (it != rings.end() && it->first != ringId)
        {
            if (!it->second->Valid(renamedBells, MaxNoOfBellsInRing(), noOfBells, false, true))
            {
                errorCode |= it->second->ErrorCode();
                ringId = it->first;
                return true;
            }
        ++it;
        }
    }
    return false;
}

std::wofstream&
Tower::ExportToRtf(std::wofstream& outputFile, const RingingDatabase& database) const
{
    outputFile << "{\\b " << city << "}";
    if (name.length() > 0)
        outputFile << ", " << name;
    outputFile << ".";
    if (county.length() > 0)
    {
        outputFile << KRtfLineEnd << std::endl << county << ".";
    }
    return outputFile;
}

void
Tower::ExportToXmlInPeal(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& towerElement, const Duco::ObjectId& ringId, bool includeIds) const
{
    DucoXmlUtils::AddElement(outputFile, towerElement, L"Name", name);
    DucoXmlUtils::AddElement(outputFile, towerElement, L"City", city);
    DucoXmlUtils::AddElement(outputFile, towerElement, L"County", county);

    if (includeIds)
    {
        DucoXmlUtils::AddObjectId(towerElement, id);
        DucoXmlUtils::AddAttribute(towerElement, L"DoveId", doveReference);
    }
    DucoXmlUtils::AddAttribute(towerElement, L"TowerbaseId", towerbaseId);

    const Duco::Ring* const theRing = FindRing(ringId);
    if (theRing != NULL)
    {
        DOMElement* ringElement = DucoXmlUtils::AddElement(outputFile, towerElement, L"Ring", includeIds ? theRing->Name() : L"");
        DucoXmlUtils::AddElement(outputFile, *ringElement, L"Tenor", theRing->TenorWeight());
        if (includeIds)
        {
            DucoXmlUtils::AddObjectId(*ringElement, ringId, false);
        }
    }
}

void
Tower::ExportToCsv(std::wofstream& file) const
{
    file << "\"";
    file << FullName();
    file << "\"";
}

std::wostream&
Tower::Print(std::wostream& stream, const RingingDatabase& /*database*/) const
{
    stream << city << ", " << county << ". " << std::endl << name;

    return stream;
}

std::wostream&
Tower::PrintTenorDescription(std::wostream& stream, const ObjectId& ringId) const
{
    std::map<Duco::ObjectId, Duco::Ring*>::const_iterator it = rings.find(ringId);

    if (it != rings.end())
    {
        stream << L"(" << TenorDescription(ringId) << L")";
    }
    else
    {
        stream << L"(!ERROR!)";
    }

    return stream;
}

DatabaseWriter&
Tower::Externalise(DatabaseWriter& writer) const
{
    writer.WriteId(id);
    writer.WriteString(name);
    writer.WriteString(city);
    writer.WriteString(county);
    writer.WriteString(latitude);
    writer.WriteString(longitude);
    writer.WriteString(aka);
    writer.WriteBool(handbell);
    writer.WriteInt(noOfBells);
    writer.WriteDate(firstRungDate);
    writer.WriteInt(rings.size());

    std::map<Duco::ObjectId, Duco::Ring*>::const_iterator it = rings.begin();
    while (it != rings.end())
    {
        writer.WriteId(it->first);
        it->second->Externalise(writer);
        ++it;
    }
    writer.WriteString(notes);
    writer.WriteBool(removed);
    writer.WriteBool(antiClockwise);

    writer.WriteInt(renamedBells.size());
    std::map<unsigned int, TRenameBellType>::const_iterator it2 = renamedBells.begin();

    while (it2 != renamedBells.end())
    {
        writer.WriteInt(it2->first);
        writer.WriteInt(it2->second);
        ++it2;
    }
    writer.WriteString(doveReference);
    writer.WriteString(towerbaseId);
    writer.WriteId(linkedTowerId);
    writer.WriteId(pictureId);

    return writer;

}

DatabaseReader&
Tower::Internalise(DatabaseReader& reader, unsigned int databaseVersion)
{
    id = reader.ReadUInt();

    reader.ReadString(name);
    reader.ReadString(city);
    reader.ReadString(county);

    if (databaseVersion >= 13)
    {
        reader.ReadString(latitude);
        reader.ReadString(longitude);
    }
    else
    {
        latitude.clear();
        longitude.clear();
    }
    std::wstring tenorKeyDiscard;
    if (databaseVersion >= 19)
    {
        if (databaseVersion < 40)
        {
            reader.ReadString(tenorKeyDiscard);
        }
        reader.ReadString(aka);
        if (databaseVersion >= 47)
        {
            handbell = reader.ReadBool();
        }
    }
    else
    {
        aka.clear();
    }

    noOfBells = reader.ReadUInt();
    if (databaseVersion >= 19)
    {
        if (databaseVersion <= 44)
        {
            unsigned int minutesDiscard = reader.ReadUInt();
        }
        reader.ReadDate(firstRungDate);
    }

    unsigned int noOfRings = reader.ReadUInt();

    while(noOfRings > 0)
    {
        /*unsigned int key =*/ reader.ReadUInt();
        Duco::Ring* ring = new Ring(reader, databaseVersion);
        if (tenorKeyDiscard.length() > 0 && databaseVersion >= 19 && databaseVersion < 40)
        {
            ring->SetTenorKey(tenorKeyDiscard);
        }

        AddRing(ring, false);
        --noOfRings;
    }
    if (databaseVersion >= 3)
    {
        reader.ReadString(notes);
    }
    if (databaseVersion >= 4)
    {
        removed = reader.ReadBool();
    }
    if (databaseVersion >= 48)
    {
        antiClockwise = reader.ReadBool();
    }
    if (databaseVersion >= 7)
    {
        unsigned int numberOfBellTypes = reader.ReadUInt();

        while (numberOfBellTypes > 0)
        {
            unsigned int bellNumber = reader.ReadUInt();
            Duco::TRenameBellType bellType = Duco::TRenameBellType(reader.ReadInt());
            if (bellType != Duco::ENormalBell)
            {
                std::pair<unsigned int, TRenameBellType> newObject(bellNumber, bellType);
                renamedBells.insert(newObject);
            }
        --numberOfBellTypes;
        }
    }

    if (databaseVersion >= 9)
    {
        reader.ReadString(doveReference);
    }
    if (databaseVersion >= 22 && databaseVersion < 40)
    {
        std::wstring pealsCoUkLink;
        reader.ReadString(pealsCoUkLink);
    }
    if (databaseVersion >= 40)
    {
        reader.ReadString(towerbaseId);
        SetTowerbaseId(towerbaseId);
    }
    wstring pealbaseLinkDiscard;
    if (databaseVersion >= 26 && databaseVersion < 46)
    {
        reader.ReadString(pealbaseLinkDiscard);
    }
    wstring felsteadLinkDiscard;
    if (databaseVersion >= 29 && databaseVersion < 46)
    {
        reader.ReadString(felsteadLinkDiscard);
    }
    if (towerbaseId.length() == 0)
    {
        if (pealbaseLinkDiscard.length() > 0)
            towerbaseId = DucoEngineUtils::TrimTowerBaseId(pealbaseLinkDiscard);
        else if (felsteadLinkDiscard.length() > 0)
            towerbaseId = DucoEngineUtils::TrimTowerBaseId(felsteadLinkDiscard);
    }

    if (databaseVersion >= 30)
    {
        reader.ReadId(linkedTowerId);
    }
    if (databaseVersion >= 33)
    {
        reader.ReadId(pictureId);
    }

    return reader;
}

bool
Tower::operator==(const Duco::RingingObject& other) const
{
    if (other.ObjectType() != ObjectType())
        return false;

    const Duco::Tower& otherTower = static_cast<const Duco::Tower&>(other);

    if (name.compare(otherTower.name) != 0)
        return false;

    if (city.compare(otherTower.city) != 0)
        return false;

    if (county.compare(otherTower.county) != 0)
        return false;

    if (notes.compare(otherTower.notes) != 0)
        return false;

    if (doveReference.compare(otherTower.doveReference) != 0)
        return false;

    if (latitude.compare(otherTower.latitude) != 0)
        return false;

    if (longitude.compare(otherTower.longitude) != 0)
        return false;

    if (aka.compare(otherTower.aka) != 0)
        return false;

    if (firstRungDate != otherTower.firstRungDate)
        return false;

    if (noOfBells != otherTower.noOfBells)
        return false;

    if (rings.size() != otherTower.rings.size())
        return false;

    if (rings != otherTower.rings)
        return false;

    if (removed != otherTower.removed)
        return false;

    if (antiClockwise != otherTower.antiClockwise)
        return false;

    if (handbell != otherTower.handbell)
        return false;

    if (towerbaseId.compare(otherTower.towerbaseId) != 0)
        return false;

    if (linkedTowerId != otherTower.linkedTowerId)
        return false;

    if (pictureId != otherTower.pictureId)
        return false;

    return true;
}

bool
Tower::operator!=(const Duco::RingingObject& other) const
{
    if (other.ObjectType() == ObjectType())
        return false;

    const Duco::Tower& otherTower = static_cast<const Duco::Tower&>(other);

    if (name.compare(otherTower.name) == 0)
        return false;

    if (city.compare(otherTower.city) == 0)
        return false;

    if (county.compare(otherTower.county) == 0)
        return false;

    if (notes.compare(otherTower.notes) == 0)
        return false;

    if (doveReference.compare(otherTower.doveReference) == 0)
        return false;

    if (latitude.compare(otherTower.latitude) == 0)
        return false;

    if (longitude.compare(otherTower.longitude) == 0)
        return false;

    if (firstRungDate == otherTower.firstRungDate)
        return false;

    if (aka.compare(otherTower.aka) == 0)
        return false;

    if (noOfBells == otherTower.noOfBells)
        return false;

    if (removed == otherTower.removed)
        return false;

    if (antiClockwise == otherTower.antiClockwise)
        return false;

    if (handbell == otherTower.handbell)
        return false;

    if (towerbaseId.compare(otherTower.towerbaseId) == 0)
        return false;

    if (linkedTowerId == otherTower.linkedTowerId)
        return false;

    if (pictureId == otherTower.pictureId)
        return false;

    return true;
}

Tower&
Tower::operator=(const Duco::Tower& otherTower)
{
    id = otherTower.id;
    name = otherTower.name;
    city = otherTower.city;
    county = otherTower.county;
    notes = otherTower.notes;
    doveReference = otherTower.doveReference;
    latitude = otherTower.latitude;
    longitude = otherTower.longitude;
    aka = otherTower.aka;
    firstRungDate = otherTower.firstRungDate;
    noOfBells = otherTower.noOfBells;
    CopyAllRings(otherTower.rings);
    removed = otherTower.removed;
    antiClockwise = otherTower.antiClockwise;
    handbell = otherTower.handbell;
    renamedBells = otherTower.renamedBells;
    SetTowerbaseId(otherTower.towerbaseId);
    errorCode = otherTower.errorCode;
    linkedTowerId = otherTower.linkedTowerId;
    pictureId = otherTower.pictureId;

    return *this;
}

void
Tower::SetDoveAndLocation(const Duco::DoveObject& other)
{
    doveReference = other.DoveId();
    SetTowerbaseId(other.TowerbaseId());
    latitude = DucoEngineUtils::ToString(other.Latitude());
    longitude = DucoEngineUtils::ToString(other.Longitude());
}

bool
Tower::UpdateDoveLocationAndOtherIds(const Duco::DoveObject& other)
{
    bool updated = false;
    if (DoveRef().length() == 0)
    {
        SetDoveRef(other.DoveId());
        updated = true;
    }
    else if (other.OldDoveId().compare(DoveRef()) == 0)
    {
        SetDoveRef(other.DoveId());
        updated = true;
    }
    std::wstring newLatitude = DucoEngineUtils::ToString(other.Latitude());
    if (latitude.compare(newLatitude) != 0)
    {
        latitude = newLatitude;
        updated = true;
    }
    std::wstring newLongitude = DucoEngineUtils::ToString(other.Longitude());
    if (longitude.compare(newLongitude) != 0)
    {
        longitude = newLongitude;
        updated = true;
    }
    if (towerbaseId.compare(other.TowerbaseId()) != 0)
    {
        SetTowerbaseId(other.TowerbaseId());
        updated = true;
    }

    return updated;
}

bool
Tower::UpdateTenorFromDoveAndOtherIds(const Duco::DoveObject& other)
{
    bool anyUpdated = false;

    for (auto const& [key,value] : rings)
    {
        anyUpdated |= value->UpdateTenorKeyFromDove(other, noOfBells);
    }
    if (anyUpdated && DoveRef().compare(other.OldDoveId()) == 0)
    {
        SetDoveRef(other.DoveId());
        SetTowerbaseId(other.TowerbaseId());
    }

    return anyUpdated;
}

bool
Tower::CapitaliseField(bool updateCounty, bool updateDedication)
{
    bool changed = false;

    if (updateCounty)
    {
        std::wstring newCounty = DucoEngineUtils::ToCapitalised(county, true);
        if (county.compare(newCounty) != 0)
        {
            county = newCounty;
            changed = true;
        }
    }
    if (updateDedication)
    {
        std::wstring newName = DucoEngineUtils::ToCapitalised(name, true);
        if (name.compare(newName) != 0)
        {
            name = newName;
            changed = true;
        }
    }

    return changed;
}


bool
Tower::UpdateRing(const Duco::ObjectId& ringId, const Duco::Ring& ring)
{
    std::map<Duco::ObjectId, Duco::Ring*>::iterator it = rings.find(ringId);

    if (it != rings.end())
    {
        *it->second = ring;
        return true;
    }
    return false;
}

void
Tower::GetRingIds(std::vector<Duco::ObjectId>& ringIds) const
{
    ringIds.clear();
    for (auto const& [key,value] : rings)
    {
        ringIds.push_back(key);
    }
}

const std::wstring&
Tower::HeaviestTenor() const
{
    Duco::Ring* heaviestTenorRing = NULL;
    unsigned int noOfBells = 0;
    
    for (auto const& [key,value] : rings)
    {
        if (value->NoOfBells() > noOfBells)
        {
            noOfBells = value->NoOfBells();
            heaviestTenorRing = value;
        }
    }
    if (heaviestTenorRing != NULL)
    {
        return heaviestTenorRing->TenorWeight();
    }
    return city;
}

std::wstring
Tower::TenorWeight(const Duco::ObjectId& ringId) const
{
    const Duco::Ring* const theRing = FindRing(ringId);
    if (theRing == NULL)
        return L"";

    return theRing->TenorWeight();
}

std::wstring
Tower::TenorDescription(const Duco::ObjectId& ringId) const
{
    const Duco::Ring* const theRing = FindRing(ringId);
    if (theRing == NULL)
        return L"";

    return theRing->TenorDescription();
}

bool
Tower::RingOnBackBells(const Duco::ObjectId& ringId) const
{
    const Duco::Ring* const theRing = FindRing(ringId);
    if (theRing == NULL)
        return false;
    return RingOnBackBells(*theRing);
}

bool
Tower::RingOnBackBells(const Duco::Ring& ring) const
{
    if (ring.ContainsBell(noOfBells))
        return true;

    return false;
}

std::wstring
Tower::FullName() const
{
    std::wstring fullname(city);
    if (county.length() > 0)
    {
        fullname.append(L", ");
        fullname.append(county);
    }
    if (name.length() > 0)
    {
        fullname.append(L". (");
        fullname.append(name);
        fullname.append(L")");
    }
    else
    {
        fullname.append(L".");
    }

    return fullname;
}

std::wstring
Tower::FullNameForSort(bool unusedSetting) const
{
    std::wstring lowercaseFullname;
    DucoEngineUtils::ToLowerCase(FullName(), lowercaseFullname);
    if (removed)
    {
        lowercaseFullname.append(L"_Removed");
    }

    return lowercaseFullname;
}

bool
Tower::RenumberRings(std::map<Duco::ObjectId, Duco::ObjectId>& oldRingIdsToNewRingIds)
{
    oldRingIdsToNewRingIds.clear();
    bool ringsRenumbered (false);
    if (rings.size() >= 1)
    {
        std::multimap<unsigned int, Duco::ObjectId> oldRingsByNoOfBells;
        // Get order the rings should be numbered in.
        std::map<Duco::ObjectId, Duco::Ring*>::const_iterator it = rings.begin();
        while (it != rings.end())
        {
            std::pair<unsigned int, Duco::ObjectId> newObject(it->second->NoOfBells(), it->second->Id());
            oldRingsByNoOfBells.insert(newObject);
            ++it;
        }
        std::multimap<unsigned int, Duco::ObjectId>::const_iterator renumberIt = oldRingsByNoOfBells.begin();
        Duco::ObjectId newId (0);
        // Map old Ids to new ids.
        while (renumberIt != oldRingsByNoOfBells.end())
        {
            if (renumberIt->second != newId)
            {
                std::pair<Duco::ObjectId, Duco::ObjectId> newObject(renumberIt->second, newId);
                oldRingIdsToNewRingIds.insert(newObject);
            }
            ++newId;
            ++renumberIt;
        }
        // Renumber rings
        ringsRenumbered |= DoRenumberRings(oldRingIdsToNewRingIds);

    }
    return ringsRenumbered;
}

bool
Tower::DoRenumberRings(std::map<Duco::ObjectId, Duco::ObjectId> oldRingsByNoOfBells)
{
    bool ringsRenumbered (false);

    std::map<Duco::ObjectId, Duco::ObjectId>::iterator renumberIt = oldRingsByNoOfBells.begin();
    while (renumberIt != oldRingsByNoOfBells.end())
    {
        Duco::ObjectId oldId = renumberIt->first;
        Duco::ObjectId newId = renumberIt->second;
        if (SwapRings(oldId, newId))
        {
            ringsRenumbered = true;
            std::map<Duco::ObjectId, Duco::ObjectId>::iterator otherIt = oldRingsByNoOfBells.find(newId);
            if (otherIt != oldRingsByNoOfBells.end())
            {
                if (renumberIt->first != otherIt->second)
                    renumberIt->second = otherIt->second;
                else
                    oldRingsByNoOfBells.erase(renumberIt);
                oldRingsByNoOfBells.erase(otherIt);
            }
            else
            {
                oldRingsByNoOfBells.erase(renumberIt);
            }
        }
        renumberIt = oldRingsByNoOfBells.begin();
    }
    return ringsRenumbered;
}

bool
Tower::SwapRings(const Duco::ObjectId& leftId, const Duco::ObjectId& rightId)
{
    if (leftId == rightId)
        return false;

    std::map<Duco::ObjectId, Duco::Ring*>::iterator leftIt = rings.find(leftId);
    if (leftIt == rings.end())
    {
        return false;
    }

    Duco::Ring* leftTower = leftIt->second;
    std::map<Duco::ObjectId, Duco::Ring*>::iterator rightIt = rings.find(rightId);
    if (rightIt != rings.end())
    {
        // tower exists at the new location, swap them
        Duco::Ring* rightTower = rightIt->second;
        leftTower->SetId(rightId);
        rightTower->SetId(leftId);

        leftIt->second = rightTower;
        rightIt->second = leftTower;

        return true;
    }

    // Nothing exists at the new index, just move this one.
    leftTower->SetId(rightId);
    std::pair<Duco::ObjectId, Duco::Ring*> newObject (rightId, leftTower);

    if (rings.insert(newObject).second)
    {
        rings.erase(leftId);
        return true;
    }
    return false;
}

bool
Tower::Valid(const Duco::RingingDatabase& ringingDb, bool warningFatal, bool clearBeforeStart) const
{
    if (clearBeforeStart)
    {
        ClearErrors();
    }
    bool errorFound (false);
    
    if (name.length() <= 0)
    {
        errorFound = true;
        errorCode.set(ETower_TowerNameInvalid,1);
    }
    if (city.length() <= 0 && county.length() <= 0)
    {
        errorFound = true;
        errorCode.set(ETower_TowerCountyAndCity,1);
    }
    if (noOfBells < ringingDb.MethodsDatabase().LowestValidOrderNumber())
    {
        errorFound = true;
        errorCode.set(ETower_TooFewBells,1);
    }
    if (noOfBells > DucoEngineUtils::MaxNumberOfBells())
    {
        errorFound = true;
        errorCode.set(ETower_TooManyBells,1);
    }
    if (warningFatal && !removed && doveReference.find_first_not_of(L"1234567890") != std::wstring::npos)
    {
        errorFound = true;
        errorCode.set(ETowerWarning_DoveContainsOldChars, 1);
    }
    if (warningFatal && !removed && !handbell && !ValidTowerbaseId())
    {
        errorFound = true;
        errorCode.set(ETowerWarning_TowerbaseIdMissing, 1);
    }

    if (rings.empty())
    {
        errorFound = true;
        errorCode.set(ETower_NoRings,1);
    }
    else
    {
        std::map<unsigned int, std::wstring> tenorKeys;
        for (auto const& [key,value] : rings)
        {
            if (!value->Valid(renamedBells, MaxNoOfBellsInRing(), noOfBells, warningFatal, clearBeforeStart))
            {
                if (warningFatal)
                    errorFound = true;
                errorCode |= value->ErrorCode();
            }
            if (value->TenorKey().length() > 0)
            {
                unsigned int tenorBellNo = value->TenorBellNumber();
                std::map<unsigned int, std::wstring>::const_iterator tenorKeyIt = tenorKeys.find(tenorBellNo);
                if (tenorKeyIt == tenorKeys.end())
                {
                    std::pair<unsigned int, std::wstring> newTenor(tenorBellNo, value->TenorKey());
                    tenorKeys.insert(newTenor);
                }
                else if (tenorKeyIt->second.compare(value->TenorKey()) != 0)
                {
                    if (warningFatal)
                        errorFound = true;
                    errorCode.set(ERingWarning_PossibleInvalidTenorKey, 1);
                }
            }
        }
        // Check for same key on different bells
        std::set<std::wstring> keys;
        for (auto const& [key, value] : tenorKeys)
        {
            if (!keys.insert(value).second)
            {
                if (warningFatal)
                    errorFound = true;
                errorCode.set(ERingWarning_PossibleInvalidTenorKey, 1);
            }
        }

        if (!AllRingsUniqueBells())
        {
            if (warningFatal)
                errorFound = true;
            errorCode.set(ETowerWarning_DuplicateRingBells, 1);
        }
    }

    std::map<unsigned int, TRenameBellType>::const_iterator it2 = renamedBells.begin();
    while (it2 != renamedBells.end())
    {
        unsigned int otherBell (-1);
        switch (it2->second)
        {
        default:
            ++it2;
            continue;
        case EFlat:
            otherBell = it2->first-1;
            break;
        case ESharp:
            otherBell = it2->first+1;
            break;
        }

        std::map<Duco::ObjectId, Duco::Ring*>::const_iterator it3 = rings.begin();
        while (it3 != rings.end())
        {
            if (it3->second->IncludesBells(it2->first, otherBell))
            {
                if (warningFatal)
                    errorFound = true;
                errorCode |= it3->second->ErrorCode();
            }
            ++it3;
        }
        ++it2;
    }

    return !errorFound;
}

bool
Tower::AllRingsUniqueBells() const
{
    if (rings.size() <= 1)
        return true;

    std::map<Duco::ObjectId, Duco::Ring*>::const_iterator it = rings.begin();
    std::map<Duco::ObjectId, Duco::Ring*>::const_iterator it2 = rings.begin();
    while (it != rings.end() && it2 != rings.end())
    {
        ++it2;
        while (it2 != rings.end())
        {
            if (it->second->BellsMatch(*(it2->second)))
            {
                return false;
            }
            ++it2;
        }

        ++it;
        if (it != rings.end())
        {
            it2 = it;
        }
    }
    return true;
}

void
Tower::SetAllRingsMaxBellCount()
{
    std::map<Duco::ObjectId, Duco::Ring*>::iterator it = rings.begin();
    while (it != rings.end())
    {
        it++->second->SetMaxBellCount(noOfBells);
    }
}

void
Tower::SetFullName(const std::wstring& newNameInput)
{
    std::wstring newName = newNameInput;
    size_t dedicationStart = newName.find_last_of(L"(");
    size_t dedicationEnd = newName.find_last_of(L")");
    if (dedicationStart > 0 && dedicationEnd > 0 && dedicationStart < dedicationEnd)
    {
        std::wstring newDedication = newName.substr(dedicationStart+1, dedicationEnd - dedicationStart - 1);
        SetName(DucoEngineUtils::Trim(newDedication));
        newName = newName.substr(0, dedicationStart);
    }

    size_t countyStart = newName.find_first_of(L",");
    size_t countyEnd = newName.find_last_of(L".");
    if (countyStart > 0 && countyEnd > 0 && countyStart < countyEnd)
    {
        std::wstring newCounty = newName.substr(countyStart+1, countyEnd - countyStart - 1);
        SetCounty(DucoEngineUtils::Trim(newCounty));
        newName = newName.substr(0, countyStart);
    }

    SetCity(DucoEngineUtils::Trim(newName));
}

bool
Tower::Duplicate(const Duco::RingingObject& other, const Duco::RingingDatabase& database) const
{
    if (other.ObjectType() != ObjectType())
        return false;

    const Duco::Tower& otherTower = static_cast<const Duco::Tower&>(other);

    if (otherTower.Removed() != this->Removed())
    {
        return false;
    }
    if (doveReference.length() > 0 && DucoEngineUtils::CompareString(otherTower.doveReference, doveReference))
    { // if dove ref matches, then we can assume the tower is the same
        return true;
    }
    if (towerbaseId.length() > 0 && DucoEngineUtils::CompareString(otherTower.towerbaseId, towerbaseId))
    {
        return true;
    }
    if (otherTower.noOfBells != this->noOfBells)
    {
        return false;
    }
    if (!DucoEngineUtils::CompareString(name, otherTower.name)
        || !DucoEngineUtils::CompareString(city, otherTower.city)
        || !DucoEngineUtils::CompareString(county, otherTower.county))
    {
        return false;
    }
    if (!DucoEngineUtils::CompareString(otherTower.TowerKey(), TowerKey(), true, true) && !DucoEngineUtils::CompareString(otherTower.HeaviestTenor(), HeaviestTenor(), true, true))
    { // this must at least go after things which match more easily, like references
        return false;
    }

    return true;
}

void
Tower::AllBellNames(std::vector<std::wstring>& bellNames, const Duco::ObjectId& theRingId) const
{
    bellNames.clear();

    const Duco::Ring* const theRing = FindRing(theRingId);

    int bellNo (1);
    for (unsigned int i(1); i <= noOfBells ; ++i)
    {
        if (theRingId.ValidId() && theRing == NULL)
        {
            continue;
        }
        else if (theRing != NULL && !theRing->ContainsBell(i))
        {
            continue;
        }
        Duco::TRenameBellType bellType (ENormalBell);
        if (!theRingId.ValidId())
            bellType = RenamedBell(i);
        switch (bellType)
        {
        case EExtraTreble:
            bellNames.push_back(L"0");
            break;
        case ENormalBell:
            bellNames.push_back(DucoEngineUtils::ToString(bellNo));
            ++bellNo;
            break;
        case EFlat:
            {
            std::wstring bellName;
            bellName = DucoEngineUtils::ToString(bellNo-1);
            bellName.append(L"♭");
            bellNames.push_back(bellName);
            }
            break;
        case ESharp:
            {
            std::wstring bellName;
            bellName = DucoEngineUtils::ToString(bellNo);
            bellName.append(L"♯");
            bellNames.push_back(bellName);
            }
            break;
        }
    }
}

std::wstring
Tower::BellName(size_t bellNumberToFind)
{
    int numberOfRenamedBells = 0;
    for (unsigned int i(1); i <= noOfBells; ++i)
    {
        Duco::TRenameBellType bellType = RenamedBell(i);
        switch (bellType)
        {
        case EExtraTreble:
            if (i == bellNumberToFind)
                return L"0";
            ++numberOfRenamedBells;
            break;

        case ENormalBell:
            if (i == bellNumberToFind)
                return DucoEngineUtils::ToString(i - numberOfRenamedBells);
            break;

        case EFlat:
        {
            ++numberOfRenamedBells;
            if (i == bellNumberToFind)
            {
                std::wstring bellName = DucoEngineUtils::ToString(i - numberOfRenamedBells);
                bellName.append(L"♭");
                return bellName;
            }
        }
        break;
        case ESharp:
        {
            if (i == bellNumberToFind)
            {
                std::wstring bellName = DucoEngineUtils::ToString(i - numberOfRenamedBells);
                bellName.append(L"♯");
                return bellName;
            }
            ++numberOfRenamedBells;
        }
        break;
        }
    }
    return L"";
}

Duco::TRenameBellType
Tower::RenamedBell(unsigned int bellNumber) const
{
    std::map<unsigned int, TRenameBellType>::const_iterator it = renamedBells.find(bellNumber);

    if (it == renamedBells.end())
    {
        return Duco::ENormalBell;
    }

    return it->second;
}

std::wstring
Tower::ErrorString(const Duco::DatabaseSettings& /*settings*/, bool showWarnings) const
{
    std::wstring errorString;

    if (errorCode.test(ETower_TowerNameInvalid))
    {
        DucoEngineUtils::AddError(errorString, L"Tower name / dedication missing");
    }
    if (errorCode.test(ETower_TowerCountyAndCity))
    {
        DucoEngineUtils::AddError(errorString, L"County or city missing");
    }
    if (errorCode.test(ETower_TooFewBells))
    {
        DucoEngineUtils::AddError(errorString, L"Not enough bells");
    }
    if (errorCode.test(ETower_TooManyBells))
    {
        DucoEngineUtils::AddError(errorString, L"Too many bells");
    }
    if (errorCode.test(ERing_InvalidRingName))
    {
        DucoEngineUtils::AddError(errorString, L"Invalid ring name");
    }
    if (errorCode.test(ERing_InvalidRingTenorWeight))
    {
        DucoEngineUtils::AddError(errorString, L"Invalid ring tenor weight");
    }
    if (errorCode.test(ERing_MissingRingTenorKey))
    {
        DucoEngineUtils::AddError(errorString, L"Ring tenor key missing");
    }
    if (errorCode.test(ERing_InvalidRingTenorKeyChars))
    {
        DucoEngineUtils::AddError(errorString, L"Invalid characters in ring tenor key");
    }
    if (showWarnings && errorCode.test(ETowerWarning_DoveContainsOldChars))
    {
        DucoEngineUtils::AddError(errorString, L"Dove ref should now only be using the new numerical ids");
    }
    if (showWarnings && errorCode.test(ETowerWarning_TowerbaseIdMissing))
    {
        DucoEngineUtils::AddError(errorString, L"Tower base id missing, run Dove Update tower locations and ids to improve BellBoard tower download reliability");
    }

    if (errorCode.test(ERing_InvalidRingNoOfBells))
    {
        DucoEngineUtils::AddError(errorString, L"Invalid ring no of bells");
    }
    if (showWarnings && errorCode.test(ERingWarning_PossibleInvalidTenorKey))
    {
        DucoEngineUtils::AddError(errorString, L"Check ring tenor keys - one is the same as the tower tenor but a different bell?");
    }
    if (errorCode.test(ERing_PossibleInvalidRingBells))
    {
        DucoEngineUtils::AddError(errorString, L"A ring contains a bell with its flat or sharp");
    }
    if (errorCode.test(ERing_InvalidRingWithExtraBells))
    {
        DucoEngineUtils::AddError(errorString, L"Ring with impossible bell combination");
    }
    if (errorCode.test(ETower_DuplicateTower))
    {
        DucoEngineUtils::AddError(errorString, L"Duplicate tower name or Dove id");
    }

    if (showWarnings && errorCode.test(ETowerWarning_DuplicateRingBells))
    {
        DucoEngineUtils::AddError(errorString, L"Ring with bells identical to another ring");
    }
    if (errorCode.test(ETower_NoRings))
    {
        DucoEngineUtils::AddError(errorString, L"No rings specified");
    }
    
    return errorString;
}

void
Tower::SetError(TTowerErrorCodes code)
{
    errorCode.set(code,1);
}

unsigned int
Tower::MinimumNoOfBells() const
{
    unsigned int minNoOfBells = noOfBells;

    for (auto const& [key,value] : rings)
    {
        minNoOfBells = std::min(minNoOfBells, value->NoOfBells());
    }
    return minNoOfBells;
}

bool
Tower::SetNoOfBells(unsigned int newNoOfBells)
{
    bool changeAllowed (true);
    std::map<Duco::ObjectId, Duco::Ring*>::const_iterator it = rings.begin();
    while (it != rings.end() && changeAllowed)
    {
        if (newNoOfBells <  it++->second->NoOfBells())
        {
            changeAllowed = false;
        }
    }
    if (changeAllowed)
    {
        int delta = newNoOfBells - noOfBells;

        noOfBells = newNoOfBells;

        it = rings.begin();
        while (it != rings.end())
        {
            it->second->ChangeBellsBy(delta, noOfBells);
            ++it;
        }
    }
    return changeAllowed;
}

bool
Tower::ReduceNoOfBellsInNewTower(unsigned int newNoOfBells)
{
    if (rings.size() != 1)
        return false;

    std::map<Duco::ObjectId, Duco::Ring*>::const_iterator it = rings.begin();
    if (it != rings.end())
    {
        delete it->second;
        rings.clear();
    }
    SetNoOfBells(newNoOfBells);
    AddDefaultRing();
    return true;
}

bool
Tower::Match(const Duco::DoveObject& object) const
{
    if (object.DoveId().compare(doveReference) == 0 || object.OldDoveId().compare(doveReference))
    {
        return true;
    }
    if (object.TowerbaseId().compare(towerbaseId) == 0)
    {
        return true;
    }
    return false;
}

unsigned int
Tower::MatchProbability(const Duco::DoveObject& doveTower, const Duco::DoveDatabase& doveDb) const
{
    unsigned int matchProbability (0);
    if (Bells() == doveTower.Bells())
        matchProbability += 2;
    else if (BellsForDove() == doveTower.Bells())
        matchProbability += 2;
    else if (doveTower.Bells() > Bells())
        matchProbability += 1;

    bool nameMatched (false);
    bool cityMatched (false);
    bool altNameMatched (false);

    if (matchProbability > 0)
    {
        std::wstring towerName = DucoEngineUtils::RemoveNoneDoveChars(Name());
        std::wstring cityName = DucoEngineUtils::RemoveNoneDoveChars(City());
        if (DucoEngineUtils::FindSubstring(towerName, doveTower.Dedication(), L"", true) ||
            doveDb.CompareSaint(doveTower.Dedication(), towerName, true))
        {
            nameMatched = true;
            matchProbability += 2;
        }
        else if (DucoEngineUtils::CompareString(doveTower.Name(), towerName))
        {
            nameMatched = true;
            ++matchProbability;
        }
        else if (DucoEngineUtils::CompareString(doveTower.City(), towerName, true))
        {
            cityMatched = true;
            ++matchProbability;
        }
        else if (doveTower.AltName().length() > 3 && DucoEngineUtils::CompareString(doveTower.AltName(), towerName))
        {
            altNameMatched = true;
            ++matchProbability;
        }

        bool countyMatched(false);

        if (!cityMatched && DucoEngineUtils::CompareString(doveTower.City(), cityName))
        {
            cityMatched = true;
            matchProbability += 2;
        }
        else if (DucoEngineUtils::CompareString(doveTower.County(), cityName))
        {
            countyMatched = true;
            ++matchProbability;
        }
        else if (!nameMatched && DucoEngineUtils::CompareString(doveTower.Name(), cityName))
        {
            nameMatched = true;
            ++matchProbability;
        }
        else if (!altNameMatched && doveTower.AltName().length() > 3 && DucoEngineUtils::CompareString(doveTower.AltName(), cityName))
        {
            altNameMatched = true;
            ++matchProbability;
        }

        if (matchProbability >= 3)
        {
            if (!countyMatched && DucoEngineUtils::CompareString(doveTower.County(), County()))
            {
                ++matchProbability;
            }
            else if (!cityMatched && DucoEngineUtils::CompareString(doveTower.City(), County()))
            {
                ++matchProbability;
            }
            else if (!altNameMatched && doveTower.AltName().length() > 3)
            {
                ++matchProbability;
            }

            if (matchProbability >= 4)
            {
                bool tenorMatch(false);
                bool tenorApproxMatch(false);
                std::map<Duco::ObjectId, Duco::Ring*>::const_iterator it = rings.begin();
                while (it != rings.end() && !tenorMatch)
                {
                    if (doveTower.TenorApprox())
                    {
                        if (DucoEngineUtils::StartsWith(doveTower.Tenor(), it->second->TenorWeight()))
                        {
                            tenorMatch = true;
                        }
                        else if (DucoEngineUtils::StartsWith(doveTower.AltTenor(), it->second->TenorWeight()))
                        {
                            tenorMatch = true;
                        }
                    }
                    else if (it->second->TenorWeight().compare(doveTower.Tenor()) == 0)
                    {
                        tenorMatch = true;
                    }
                    else if (it->second->TenorWeight().length() == 2 && DucoEngineUtils::StartsWith(doveTower.Tenor(), it->second->TenorWeight()))
                    {
                        tenorApproxMatch = true;
                    }
                    if (!tenorMatch && DucoEngineUtils::FindSubstring(KTenorKGSuffix, it->second->TenorWeight()))
                    {
                        if (DucoEngineUtils::ToInteger(it->second->TenorWeight()) == doveTower.TenorWeight())
                        {
                            tenorMatch = true;
                        }
                    }

                    ++it;
                }
                if (tenorMatch)
                {
                    ++matchProbability;
                    if (!doveTower.TenorApprox())
                    {
                        ++matchProbability;
                    }
                }
                else if (tenorApproxMatch)
                {
                    ++matchProbability;
                }
            }
        }
    }

    return matchProbability;
}

unsigned int
Tower::BellsForDove() const
{
    return Bells() - (unsigned int)renamedBells.size();
}

unsigned int
Tower::MaxNoOfBellsInRing() const
{
    unsigned int maxNoOfBells = Bells();
    for (auto const& [key,value] : renamedBells)
    {
        if (value != EExtraTreble)
        {
            --maxNoOfBells;
        }
    }
    return maxNoOfBells;
}

bool
Tower::UppercaseCity()
{
    std::wstring newCity;
    DucoEngineUtils::ToUpperCase(city, newCity);
    if (newCity.compare(city) != 0)
    {
        SetCity(newCity);
        return true;
    }
    return false;
}

unsigned int
Tower::NoOfBellsInRing(const Duco::ObjectId& ringId) const
{
    const Duco::Ring* const theRing = FindRing(ringId);
    if (theRing == NULL)
        return noOfBells;

    return theRing->NoOfBells();
}

bool
Tower::RebuildRecommended() const
{
    std::map<Duco::ObjectId, Duco::Ring*>::const_reverse_iterator it = rings.rbegin();
    if (it != rings.rend())
    {
        if ((it->first+1) != rings.size())
        {
            return true;
        }
    }
    return false;
}

unsigned int
Tower::MatchProbability(const std::wstring& towerNameToMatch, const std::wstring& townNameToMatch, const std::wstring& countyNameToMatch, unsigned int requiredNoOfBells, const std::wstring& tenorWeightToMatch, const std::wstring& tenorKeyToMatch, bool fromImport) const
{
    unsigned int matchCount = 0;
    if (!removed && !fromImport)
    {
        ++matchCount;
    }

    const std::wstring charsToIgnore = L" ,.'";
    if (DucoEngineUtils::CompareString(name, towerNameToMatch, true))
    {
        matchCount += 2;
    }
    else if (DucoEngineUtils::FindSubstring(name, towerNameToMatch, charsToIgnore, true))
    {
        ++matchCount;
    }
    else if (DucoEngineUtils::FindSubstring(towerNameToMatch, name, charsToIgnore, true))
    {
        ++matchCount;
    }
    else if (DucoEngineUtils::FindSubstring(towerNameToMatch, aka, charsToIgnore))
    {
        ++matchCount;
    }
    if (DucoEngineUtils::FindSubstring(city, townNameToMatch, charsToIgnore, true))
    {
        matchCount += 2;
    }
    else if (DucoEngineUtils::FindSubstring(city, townNameToMatch, charsToIgnore, true))
    {
        ++matchCount;
    }
    else if (DucoEngineUtils::FindSubstring(townNameToMatch, city, charsToIgnore, true))
    {
        ++matchCount;
    }
    else if (DucoEngineUtils::FindSubstring(townNameToMatch, aka, charsToIgnore, aka.length() > 4))
    {
        ++matchCount;
    }
    if (matchCount >= 3)
    {
        if (DucoEngineUtils::CompareString(countyNameToMatch, county))
        {
            matchCount += 2;
        }

        Duco::ObjectId foundRingId;
        unsigned int ringMatchProbability = 0;
        if (SuggestRing(requiredNoOfBells, tenorWeightToMatch, tenorKeyToMatch, foundRingId, ringMatchProbability))
        {
            matchCount += ringMatchProbability;
            if (requiredNoOfBells == noOfBells)
            {
                matchCount += 1;
            }
        }
    }
    
    return matchCount;
}

bool
Tower::ContainsSpecialBells() const
{
    return !renamedBells.empty();
}

double
Tower::LatitudeDegrees() const
{
    return _wtof(latitude.c_str());
}

double
Tower::LongitudeDegrees() const
{
    return _wtof(longitude.c_str());
}

namespace Duco
{

    const double PI = 3.14159265358979323846;

    double
    TowerDistance(double lat1, double long1, double lat2, double long2)
    {
        double distanceValue(0.0);
        double degreesToRadians(PI / 180.0);
        double earthRadius(6371);  // approximation in kilometers assuming a spherical earth.

        // Convert latitude and longitude values to radians.
        double lat1Radians = lat1 * degreesToRadians;
        double long1Radians = long1 * degreesToRadians;
        double lat2Radians = lat2 * degreesToRadians;
        double long2Radians = long2 * degreesToRadians;

        // Calculate the change in radians between the two locations.
        double latitudeRadianDelta = lat2Radians - lat1Radians;
        double longitudeRadianDelta = long2Radians - long1Radians;

        // Calculate the distance
        double expr1 = (sin(latitudeRadianDelta / 2.0) * sin(latitudeRadianDelta / 2.0)) +
            (cos(lat1Radians) * cos(lat2Radians) * sin(longitudeRadianDelta / 2.0) * sin(longitudeRadianDelta / 2.0));
        double expr2 = 2.0 * atan2(sqrt(expr1), sqrt(1 - expr1));
        distanceValue = earthRadius * expr2;
        return distanceValue;
    }
}

double
Tower::DistanceFrom(const Tower& other) const
{
    return TowerDistance(LatitudeDegrees(), LongitudeDegrees(), other.LatitudeDegrees(), other.LongitudeDegrees());
}

bool
Tower::HasDifferentPosition(const Duco::Tower& other) const
{
    if (!other.PositionValid())
        return false;

    return other.LatitudeDegrees() != LatitudeDegrees() || other.LongitudeDegrees() != LongitudeDegrees();
}

bool
Tower::TowerbaseIdMatchorNull(const Duco::Tower& other) const
{
    return other.towerbaseId.length() == 0 || towerbaseId.length() == 0 || other.towerbaseId.compare(towerbaseId) == 0;
}

bool
Tower::RemoveDuplicateRings(std::map<Duco::ObjectId, Duco::ObjectId>& duplicateRingReplacements) // old id, new id.
{
    duplicateRingReplacements.clear();

    std::map<Duco::ObjectId, Duco::Ring*>::iterator it = rings.begin();
    while (it != rings.end())
    {
        std::map<Duco::ObjectId, Duco::Ring*>::iterator it2 = rings.find(it->first);
        if (it2 != rings.end())
            ++it2;
        while (it2 != rings.end())
        {
            if (it->second->Match(*it2->second))
            {
                std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator it3 = duplicateRingReplacements.find(it->first);
                if (it3 == duplicateRingReplacements.end())
                {
                    std::pair<Duco::ObjectId, Duco::ObjectId> newObject (it2->first, it->first);
                    duplicateRingReplacements.insert(newObject);
                }
                else
                {
                    std::pair<Duco::ObjectId, Duco::ObjectId> newObject (it2->first, it3->second);
                    duplicateRingReplacements.insert(newObject);
                }
                break;
            }
            ++it2;
        }
        ++it;
    }

    std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator ringToRemoveIt = duplicateRingReplacements.begin();
    while (ringToRemoveIt != duplicateRingReplacements.end())
    {
        rings.erase(ringToRemoveIt->first);
        ++ringToRemoveIt;
    }

    return duplicateRingReplacements.size() > 0;
}

#ifndef __ANDROID__
void
Tower::ExportToXml(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& towersElement, bool exportDucoObjects) const
{
    DOMElement* towerElement = outputFile.createElement(XMLStrL("Tower"));
    towersElement.appendChild(towerElement);

    DucoXmlUtils::AddObjectId(*towerElement, id);
    DucoXmlUtils::AddElement(outputFile, *towerElement, L"Name", name);
    DucoXmlUtils::AddElement(outputFile, *towerElement, L"City", city);
    DucoXmlUtils::AddElement(outputFile, *towerElement, L"County", county);
    DucoXmlUtils::AddElement(outputFile, *towerElement, L"Notes", notes, false);
    DucoXmlUtils::AddElement(outputFile, *towerElement, L"AlsoKnownAs", aka, false);

    DucoXmlUtils::AddAttribute(*towerElement, L"DoveReference", doveReference, false);
    DucoXmlUtils::AddAttribute(*towerElement, L"TowerbaseId", towerbaseId, false);
    DucoXmlUtils::AddAttribute(*towerElement, L"Latitude", latitude, false);
    DucoXmlUtils::AddAttribute(*towerElement, L"Longitude", longitude, false);
    DucoXmlUtils::AddAttribute(*towerElement, L"NoOfBells", noOfBells);
    if (handbell)
    {
        DucoXmlUtils::AddBoolAttribute(*towerElement, L"HandBell", handbell);
    }
    if (removed)
    {
        DucoXmlUtils::AddBoolAttribute(*towerElement, L"Removed", removed);
    }
    if (firstRungDate.Valid())
    {
        DucoXmlUtils::AddAttribute(*towerElement, L"FirstRung", firstRungDate.Str());
    }

    DOMElement* ringsElement = DucoXmlUtils::AddElement(outputFile, *towerElement, L"Rings", L"", true);
    std::map<Duco::ObjectId, Duco::Ring*>::const_iterator it = rings.begin();
    while (it != rings.end())
    {
        DOMElement* ringElement = DucoXmlUtils::AddElement(outputFile, *ringsElement, L"Ring", L"", true);
        it->second->ExportToXml(outputFile, *ringElement, exportDucoObjects);

        ++it;
    }

    if (renamedBells.size() > 0)
    {
        DOMElement* renamedBellsElement = DucoXmlUtils::AddElement(outputFile, *towerElement, L"SpecialBells", L"", true);
        std::map<unsigned int, TRenameBellType>::const_iterator it2 = renamedBells.begin();
        while (it2 != renamedBells.end())
        {
            DOMElement* renamedBellElement = DucoXmlUtils::AddElement(outputFile, *renamedBellsElement, L"Bell", L"", true);
            DucoXmlUtils::AddAttribute(*renamedBellElement, L"NumberInTower", it2->first);
            switch (it2->second)
            {
            case EFlat:
                DucoXmlUtils::AddAttribute(*renamedBellElement, L"RenameType", L"Flat");
                break;
            case ESharp:
                DucoXmlUtils::AddAttribute(*renamedBellElement, L"RenameType", L"Sharp");
                break;
            case EExtraTreble:
                DucoXmlUtils::AddAttribute(*renamedBellElement, L"RenameType", L"ExtraTreble");
                break;
            case ENormalBell:
            default:
                break;
            }

            ++it2;
        }
    }
}
#endif

bool
Tower::RingTranslations(const Duco::Tower& otherTower, std::map<Duco::ObjectId, Duco::ObjectId>& otherRingTranslations) const
{
    otherRingTranslations.clear();
    if (noOfBells != otherTower.Bells() || this->NoOfRings() != otherTower.NoOfRings())
    {
        return false;
    }

    std::set<Duco::ObjectId> otherRingIds;

    std::map<Duco::ObjectId, Duco::Ring*>::const_iterator it = rings.begin();
    while (it != rings.end())
    {
        Duco::ObjectId similarRingId = otherTower.FindSimilarRing(*it->second);
        if (!similarRingId.ValidId())
            return false;

        if (!otherRingIds.insert(similarRingId).second)
            return false;

        std::pair<Duco::ObjectId, Duco::ObjectId> oldToNewRingIds(it->first, similarRingId);
        if (!otherRingTranslations.insert(oldToNewRingIds).second)
            return false;

        ++it;
    }

    return true;
}


const std::wstring&
Tower::Name() const
{
    return name;
}

const std::wstring&
Tower::City() const
{
    return city;
}

const std::wstring&
Tower::County() const
{
    return county;
}

unsigned int
Tower::Bells() const
{
    return noOfBells;
}

size_t
Tower::NoOfRings() const
{
    return rings.size();
}

size_t
Tower::NoOfRings(unsigned int noOfBells) const
{
    size_t count = 0;

    for (auto const& [key,value] : rings)
    {
        if (value->NoOfBells() == noOfBells)
        {
            ++count;
        }
    }

    return count;
}

void
Tower::SetName(const std::wstring& newName)
{
    name = newName;
}

void
Tower::SetCity(const std::wstring& newCity)
{
    city = newCity;
}

void
Tower::SetCounty(const std::wstring& newCounty)
{
    county = newCounty;
}

void
Tower::RenamedBells(std::map<unsigned int, TRenameBellType>& renames) const
{
    renames.clear();
    renames = renamedBells;
}

void
Tower::SetRenamedBells(const std::map<unsigned int, TRenameBellType>& renames)
{
    renamedBells.clear();
    renamedBells = renames;
}

const std::wstring&
Tower::Notes() const
{
    return notes;
}

const std::wstring&
Tower::DoveRef() const
{
    return doveReference;
}

bool
Tower::DoveRefSet() const
{
    return doveReference.length() > 0;
}

std::wstring
Tower::DoveUrl(const Duco::DatabaseSettings& settings) const
{
    if (doveReference.find_first_not_of(L"1234567890") != std::wstring::npos)
    {
        return settings.DoveURL() + L"detail.php?DoveID=" + doveReference;
    }
    return settings.DoveURL() + L"tower/" + doveReference;
}

void
Tower::SetNotes(const std::wstring& newNotes)
{
    notes = newNotes;
}

void
Tower::SetDoveRef(const std::wstring& newRef)
{
    doveReference = newRef;
    latitude.clear();
    longitude.clear();
}

void
Tower::SetLatitude(const std::wstring& newPos)
{
    latitude = newPos;
}

void
Tower::SetLongitude(const std::wstring& newPos)
{
    longitude = newPos;
}

const std::wstring&
Tower::Latitude() const
{
    return latitude;
}

const std::wstring&
Tower::Longitude() const
{
    return longitude;
}

bool
Tower::PositionValid() const
{
    if (latitude.length() < 1 || longitude.length() < 1)
        return false;

    bool is_valid = false;
    try
    {
        static_cast<void>(std::stod(latitude.c_str()));
        static_cast<void>(std::stod(longitude.c_str()));
        is_valid = true;
    }
    catch (std::exception&)
    {
        is_valid = false;
    }

    return is_valid;
}

bool
Tower::InEurope() const
{
    if (!PositionValid())
        return false;

    if (LatitudeDegrees() > 60)
        return false;

    if (LatitudeDegrees() < 35)
        return false;

    if (LongitudeDegrees() > 30)
        return false;

    if (LongitudeDegrees() < -10)
        return false;

    return true;
}

const Duco::PerformanceDate&
Tower::FirstRungDate() const
{
    return firstRungDate;
}

bool
Tower::FirstRungDateSet(const Duco::RingingDatabase& database, bool& byPeal) const
{
    bool set = false;
    byPeal = false;
    if (FirstRungDateSet())
    {
        set = true;
    }
    StatisticFilters filters(database);
    filters.SetTower(true, id);

    PealLengthInfo info = database.PerformanceInfo(filters);
    if (info.TotalPeals() > 0)
    {
        if ((firstRungDate.Valid() && info.DateOfFirstPeal() < firstRungDate) || !firstRungDate.Valid())
        {
            byPeal = true;
        }
        set = true;
    }

    return set;
}

void
Tower::SetFirstRungDate(const Duco::PerformanceDate& newFirstRungDate)
{
    firstRungDate = newFirstRungDate;
}

bool
Tower::FirstRungDateSet() const
{
    return firstRungDate.Valid();
}

void
Tower::SetAlsoKnownAs(const std::wstring& newAKA)
{
    aka = newAKA;
}

const std::wstring&
Tower::AlsoKnownAs() const
{
    return aka;
}

std::wstring
Tower::TowerbaseId() const
{
    return towerbaseId;
}

bool
Tower::ValidTowerbaseId() const
{
    return towerbaseId.length() > 0;
}

void
Tower::SetTowerbaseId(const std::wstring& newReference)
{
    towerbaseId = DucoEngineUtils::TrimTowerBaseId(newReference);
}

void
Tower::SetLinkedTowerId(const Duco::ObjectId& newLinkedTowerId)
{
    linkedTowerId = newLinkedTowerId;
}

const Duco::ObjectId&
Tower::LinkedTowerId() const
{
    return linkedTowerId;
}

void
Tower::SetPictureId(const Duco::ObjectId& newPictureId)
{
    pictureId = newPictureId;
}

const Duco::ObjectId&
Tower::PictureId() const
{
    return pictureId;
}

bool
Tower::ClearTenorKeysIfInvalidChars()
{
    bool changed = false;
    
    for (auto const& [key, value] : rings)
    {
        if (value->ClearTenorKeyIfInvalidChars())
        {
            changed = true;
        }
    }
    return changed;
}

std::wstring
Tower::TowerKey() const
{
    std::wstring heaviestTenorKey = L"";
    std::map<Duco::ObjectId, Duco::Ring*>::const_iterator it = rings.begin();
    while (it != rings.end() && heaviestTenorKey.length() == 0)
    {
        if (RingOnBackBells(*it->second))
        {
            heaviestTenorKey = it->second->TenorKey();
        }
        ++it;
    }
    return heaviestTenorKey;
}
