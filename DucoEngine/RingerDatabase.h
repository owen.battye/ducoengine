#ifndef __RINGERDATABASE_H__
#define __RINGERDATABASE_H__

#include "RingingObjectDatabase.h"

#include <string>
#include <map>
#include <set>

namespace Duco
{
    class DatabaseSettings;
    class PerformanceDate;
    class Ringer;
    class RenumberProgressCallback;
    class Peal;

class RingerDatabase: public RingingObjectDatabase
{
    friend class Peal;

public:
    DllExport RingerDatabase();
    ~RingerDatabase();

    // from RingingObjectDatabase
    DllExport virtual void ClearObjects(bool createDefaultBellNames);
    DllExport virtual Duco::ObjectId AddObject(const Duco::RingingObject& newObject);
    virtual bool Internalise(DatabaseReader& reader, Duco::ImportExportProgressCallback* newCallback, int databaseVersionNumber, size_t noOfObjectsInThisDatabase, size_t totalObjects, size_t objectsProcessedSoFar);
    virtual void Externalise(Duco::ImportExportProgressCallback* newCallback, Duco::DatabaseWriter& writer, size_t noOfObjectsInThisDatabase, size_t totalObjects, size_t objectsProcessedSoFar) const;
    virtual bool RebuildObjects(Duco::RenumberProgressCallback* callback, Duco::RingingDatabase& database);
    virtual bool MatchObject(const Duco::RingingObject& object, const Duco::TSearchArgument& searchArg, const Duco::RingingDatabase& database) const;
    virtual bool ValidateObject(const Duco::RingingObject& object, const Duco::TSearchValidationArgument& searchArg, const std::set<Duco::ObjectId>& idsToCheckAgainst, const Duco::RingingDatabase& database) const;
    virtual void ExternaliseToXml(Duco::ImportExportProgressCallback* newCallback, XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, bool exportDucoObjects) const;
    virtual bool UpdateObjectField(const Duco::ObjectId& id, const Duco::TUpdateArgument& updateArgument);
    bool CapitaliseField(Duco::ProgressCallback& progressCallback, size_t& numberOfObjectsUpdated, size_t numberOfObjectsToUpdate);

    // new functions
    // Called from UI, id determined or kept
    DllExport Duco::ObjectId AddRinger(const std::wstring& newRingerName);
    DllExport Duco::ObjectId AddRinger(const std::wstring& forename, const std::wstring& surname);
    DllExport const Ringer* const FindRinger(const Duco::ObjectId& ringerId, bool setIndex = false) const;
    DllExport Duco::ObjectId SuggestRinger(const std::wstring& ringerFullname, bool partialMatchAllowed, std::set<Duco::ObjectId>& nearMatches) const;
    DllExport void SuggestAllRingers(const std::wstring& ringerFullname, std::set<Duco::ObjectId>& nearMatches) const;
    DllExport Duco::ObjectId FindIdenticalRinger(const std::wstring& ringerFullname) const;
    DllExport bool FindRingerIdDuringDownload(const std::wstring& ringerName, Duco::ObjectId& ringerId) const;
    DllExport bool RingerAkaRequired(const Duco::ObjectId& ringerId, const Duco::PerformanceDate& pealDate, size_t& akaNo) const;
    DllExport std::wstring RingerFullName(const Duco::ObjectId& ringerId, const Duco::DatabaseSettings& settings) const;
    DllExport std::wstring RingerFullName(const Duco::ObjectId& ringerId, const Duco::PerformanceDate& pealDate, const Duco::DatabaseSettings& settings) const;

    DllExport bool SetRingerGender(const Duco::ObjectId& ringerId, bool male);
    DllExport void RingerGenderStats(float& percentMale, float& percentFemale, float& percentNotSet, float& percentNonHuman) const;

    void AddMissingRingers(std::map<Duco::ObjectId, Duco::PealLengthInfo>& ringerIds) const;
    void RemoveUsedIds(std::set<Duco::ObjectId>& unusedRingerIds) const;

    DllExport bool LinkedRingers(const Duco::ObjectId& ringerId, std::set<Duco::ObjectId>& links, bool insertSelf) const;
    DllExport bool RingersLinked(const Duco::ObjectId& ringerId, const Duco::ObjectId& ringerId2) const;
    DllExport bool LinkRingers(const Duco::ObjectId& ringerId, const Duco::ObjectId& ringerId2);
    DllExport bool LinkedRinger(const Duco::ObjectId& ringerId) const;
    DllExport bool RemoveLinkedRinger(Duco::ObjectId ringerId);

    DllExport void CombineLinkedRingers(std::map<ObjectId, Duco::PealLengthInfo>& sortedRingerIds) const;
    DllExport void CombineLinkedRingers(std::map<ObjectId, std::map<unsigned int, Duco::PealLengthInfo> >& sortedPealCounts) const;

protected:
    // from RingingObjectDatabase
    bool SaveUpdatedObject(Duco::RingingObject& lhs, const Duco::RingingObject& rhs);

protected:
    // new
    bool RenumberLinkedRingers(const std::map<ObjectId, ObjectId>& newObjectIds);

protected:
    std::vector<std::set<Duco::ObjectId> > linkedRingerIds;
};

}

#endif //!__RINGERDATABASE_H__
