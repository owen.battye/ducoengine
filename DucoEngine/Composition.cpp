#include "Composition.h"

#include "Calling.h"
#include "Change.h"
#include "ChangeCollection.h"
#include "Course.h"
#include "DatabaseReader.h"
#include "DatabaseWriter.h"
#include "DucoEngineUtils.h"
#include "DucoXmlUtils.h"
#include "Method.h"
#include "MethodDatabase.h"
#include "Music.h"
#include "Ringer.h"
#include "RingingDatabase.h"
#include "Lead.h"
#include "LeadEndCollection.h"
#include "PlaceNotation.h"
#include "RingerDatabase.h"
#include <cstdlib>
#include <limits>

using namespace Duco;

Composition::Composition(Duco::Method& method)
:   RingingObject(), methodId(method.Id()), repeats(0), proven(false), callingId(0),
    noOfChanges(0), notation(NULL), proveInProgress(false), currentEndChange(NULL),
    snapStartChangesMissing(0), snapFinishChangesMissing(0), musicChecker(NULL), currentPosition(-1)
{
    composerId.ClearId();
    composerName = new std::wstring(L"");
    name = new std::wstring(L"");
    notes = new std::wstring(L"");
    notation = new PlaceNotation(method, callingId);
    currentEndChange = new Change(method.Order());
    currentEndChange->SetChangeType(ELeadEnd);
    leadEnds = new Duco::LeadEndCollection();
    changes = new Duco::ChangeCollection(*currentEndChange);
}

Composition::Composition(const Duco::ObjectId& newMethodId, const MethodDatabase& methodDb)
:   RingingObject(), methodId(newMethodId), repeats(0), proven(false), callingId(0),
    noOfChanges(0), notation(NULL), currentEndChange(NULL), proveInProgress(false), musicChecker(NULL),
    snapStartChangesMissing(0), snapFinishChangesMissing(0), currentPosition(-1)
{
    composerId.ClearId();
    composerName = new std::wstring(L"");
    name = new std::wstring(L"");
    notes = new std::wstring(L"");
    const Method* const theMethod = methodDb.FindMethod(methodId);
    if (theMethod != NULL)
    {
        notation = new PlaceNotation(*const_cast<Duco::Method*>(theMethod), callingId);
        currentEndChange = new Change(theMethod->Order());
        currentEndChange->SetChangeType(ELeadEnd);
        leadEnds = new Duco::LeadEndCollection();
        changes = new Duco::ChangeCollection(*currentEndChange);
    }
    else
    {
        leadEnds = NULL;
        notation = NULL;
        currentEndChange = NULL;
        changes = NULL;
    }
}

Composition::Composition(const Duco::ObjectId& newId, const Composition& other)
    : RingingObject(newId), proven(other.proven), composerId(other.composerId), repeats(other.repeats), methodId(other.methodId), callingId(other.callingId),
    notation(NULL), currentEndChange(NULL), noOfChanges(other.noOfChanges), musicChecker(NULL), proveInProgress(false), leadEnds(NULL),
    snapStartChangesMissing(other.snapStartChangesMissing), snapFinishChangesMissing(other.snapFinishChangesMissing), changes(NULL)
{
    composerName = new std::wstring(L"");
    (*composerName) = (*other.composerName);
    name = new std::wstring();
    (*name) = (*other.name);
    notes = new std::wstring();
    (*notes) = (*other.notes);
    CopyAllLeads(other);
}

Composition::Composition(const Composition& other)
:   RingingObject(other), proven(other.proven), composerId(other.composerId), repeats(other.repeats), methodId(other.methodId), callingId(other.callingId),
    notation(NULL), currentEndChange(NULL), noOfChanges(other.noOfChanges), musicChecker(NULL), proveInProgress(false), leadEnds(NULL),
    snapStartChangesMissing(other.snapStartChangesMissing), snapFinishChangesMissing(other.snapFinishChangesMissing), changes(NULL)
{
    composerName = new std::wstring(L"");
    (*composerName) = (*other.composerName);
    name = new std::wstring();
    (*name) = (*other.name);
    notes = new std::wstring();
    (*notes) = (*other.notes);
    CopyAllLeads(other);
}

Composition&
Composition::operator=(const Duco::Composition& other)
{
    DeleteAllLeads();
    id = other.id;
    noOfChanges = other.noOfChanges;
    proven = other.proven;
    composerId = other.composerId;
    (*composerName) = (*other.composerName);
    repeats = other.repeats;
    methodId = other.methodId;
    callingId = other.callingId;
    (*name) = (*other.name);
    (*notes) = (*other.notes);
    delete currentEndChange;
    currentEndChange = NULL;
    delete notation;
    notation = NULL;
    CopyAllLeads(other);
    delete leadEnds;
    leadEnds = NULL;
    delete changes;
    changes = NULL;
    snapStartChangesMissing = other.snapStartChangesMissing;
    snapFinishChangesMissing = other.snapFinishChangesMissing;
    ClearErrors();
    return *this;
}

Composition::Composition(DatabaseReader& reader, unsigned int databaseVersion)
:   RingingObject(), repeats(0), proven(false), noOfChanges(0),
    notation(NULL), proveInProgress(false), 
    callingId(0), currentEndChange(NULL), leadEnds(NULL), musicChecker(NULL),
    snapStartChangesMissing(0), snapFinishChangesMissing(0), changes(NULL)
{
    composerId.ClearId();
    composerName = new std::wstring(L"");
    methodId.ClearId();
    name = new std::wstring(L"");
    notes = new std::wstring(L"");
    Composition::Internalise(reader, databaseVersion);
}

Composition::~Composition()
{
    delete currentEndChange;
    delete notation;
    delete composerName;
    delete name;
    delete notes;
    delete musicChecker;
    delete leadEnds;
    delete changes;

    DeleteAllLeads();
}

void
Composition::MoveToHome()
{
    if (currentEndChange->IsHome())
        return;
    Duco::Calling* newCall = new Duco::Calling(Order(), EPlainLead, KNoId);
    size_t noOfExtraChanges (0);
    if (newCall->AdvanceToCall(*currentEndChange, *notation, NULL, 0, noOfExtraChanges, true))
    {
        if (!proven)
            noOfChanges += noOfExtraChanges;
    }
    delete newCall;
}

bool
Composition::AddCallByPositionNumber(unsigned int position, Duco::TLeadType type, const Duco::ObjectId& changeMethodId)
{
    if (position > notation->Order() || position < 1)
    {
        return false;
    }

    if (!notation->HasNotationForCallingType(type))
    {
        return false;
    }

    proven = false;

    bool callAdded (false);
    Duco::Calling* newCall = new Duco::Calling(position, type, changeMethodId);
    try
    {
        size_t noOfNewChanges(0);
        if (newCall->AdvanceToCall(*currentEndChange, *notation, NULL, 0, noOfNewChanges, false))
        {
            if (!proven)
                noOfChanges += noOfNewChanges;
            callAdded = true;
        }
    }
    catch (std::exception*)
    {
        callAdded = false;
    }
    if (callAdded)
    {
        callings.push_back(newCall);
    }
    else
    {
        delete newCall;
    }
    return callAdded;
}

bool
Composition::AddCall(wchar_t position, Duco::TLeadType type, const Duco::ObjectId& changeMethodId)
{
    unsigned int thePosition(0);
    switch (position)
    {
    case 'M':
    case 'm':
        thePosition = notation->LeadOrder().MiddlePosition();
        break;
    case 'W':
    case 'w':
        thePosition = notation->LeadOrder().WrongPosition();
        break;
    case 'H':
    case 'h':
        thePosition = notation->LeadOrder().HomePosition();
        break;
    case 'I':
    case 'i':
        thePosition = 2;
        break;
    case 'O':
    case 'o':
        thePosition = 3;
        break;
    case 'V':
    case 'v':
        thePosition = 5;
        break;
    case 'F':
    case 'f':
        thePosition = 4;
        break;
    case 'B':
    case 'b':
        thePosition = 3;
        break;
    default:
        thePosition = DucoEngineUtils::ToInteger(position);
        break;
    }

    return AddCallByPositionNumber(thePosition, type, changeMethodId);
}

bool
Composition::AddCalls(wchar_t position, const std::wstring& callingStr)
{
    bool success(true);
    std::wstring::const_iterator it = callingStr.begin();
    while (it != callingStr.end() && success)
    {
        wchar_t nextChar = *it;
        ++it;

        if (nextChar == '-')
        {
            success &= AddCall(position, EBobLead);
        }
        else if (nextChar == 's')
        {
            success &= AddCall(position, ESingleLead);
        }
        else if (isdigit(nextChar))
        {
            TLeadType leadType (EBobLead);
            if (it != callingStr.end() && (*it) == 's')
            {
                leadType = ESingleLead;
                ++it;
            }
            int noOfCalls = _wtoi(&nextChar);
            while (noOfCalls-- > 0)
                success &= AddCall(position, leadType);
        }
    }
    return success;
}

void
Composition::Reset()
{
    currentPosition = notation->Order();
    currentEndChange->Reset();
    currentEndChange->SetChangeType(ELeadEnd);
    ClearErrors();
}

void
Composition::Clear()
{
    proven = false;
    noOfChanges = 0;
    Reset();
    ClearErrors();
    DeleteAllLeads();
}

std::wstring
Composition::StartingLeadEnd() const
{
    if (notation)
        return notation->LeadOrder().StartingLeadEnd(Order());
    else
        return std::wstring(L"123456");
}

std::wstring
Composition::EndChange(bool all, bool showTreble) const
{
    if (proven)
        return changes->EndChange().Str(all, showTreble);

    return currentEndChange->Str(all, showTreble);
}

void
Composition::DeleteAllLeads()
{
    std::vector<Duco::Calling*>::iterator it = callings.begin();
    while (it != callings.end())
    {
        delete *it;
        ++it;
    }
    callings.clear();
}

void
Composition::CopyAllLeads(const Composition& other)
{
    std::vector<Duco::Calling*>::const_iterator it = other.callings.begin();
    while (it != other.callings.end())
    {
        Duco::Calling* newCalling = new Duco::Calling(**it);
        callings.push_back(newCalling);
        ++it;
    }
}

Duco::TObjectType
Composition::ObjectType() const
{
    return TObjectType::EComposition;
}

bool
Composition::Duplicate(const Duco::RingingObject& other, const Duco::RingingDatabase& database) const
{
    if (other.ObjectType() != ObjectType())
        return false;

    const Duco::Composition& otherComposition = static_cast<const Duco::Composition&>(other);

    return false;
}

bool
Composition::operator==(const Duco::RingingObject& other) const
{
    if (other.ObjectType() != ObjectType())
        return false;

    const Duco::Composition& otherPtr = static_cast<const Duco::Composition&>(other);
    if (proven && otherPtr.proven)
    {
        if (snapStartChangesMissing != otherPtr.snapStartChangesMissing ||
            snapFinishChangesMissing != otherPtr.snapFinishChangesMissing)
                return false;
    }
    if (composerId != otherPtr.composerId)
        return false;
    if (!DucoEngineUtils::CompareString(*composerName, *otherPtr.composerName, false, true))
        return false;
    if (callings.size() != otherPtr.callings.size())
        return false;
    if (methodId != otherPtr.methodId)
        return false;
    if (repeats != otherPtr.repeats)
        return false;
    if (!DucoEngineUtils::CompareString(*name,*otherPtr.name, false, true))
        return false;
    if (!DucoEngineUtils::CompareString(*notes,*otherPtr.notes, false, true))
        return false;

    std::vector<Duco::Calling*>::const_iterator it = callings.begin();
    std::vector<Duco::Calling*>::const_iterator it2 = otherPtr.callings.begin();
    while (it != callings.end() && it2 != otherPtr.callings.end())
    {
        if ((*it2)->operator!=(**it))
            return false;

        ++it2;
        ++it;
    }

    return true;
}

bool
Composition::operator!=(const Duco::RingingObject& other) const
{
    if (other.ObjectType() != ObjectType())
        return true;

    const Duco::Composition& otherPtr = static_cast<const Duco::Composition&>(other);
    if (proven && otherPtr.proven)
    {
        if (snapStartChangesMissing != otherPtr.snapStartChangesMissing ||
            snapFinishChangesMissing != otherPtr.snapFinishChangesMissing)
                return true;
    }

    if (composerId != otherPtr.composerId)
        return true;
    if (!DucoEngineUtils::CompareString(*composerName, *otherPtr.composerName, false, true))
        return true;
    if (callings.size() != otherPtr.callings.size())
        return true;
    if (methodId != otherPtr.methodId)
        return true;
    if (repeats != otherPtr.repeats)
        return true;
    if (!DucoEngineUtils::CompareString(*name,*otherPtr.name, false, true))
        return true;
    if (!DucoEngineUtils::CompareString(*notes,*otherPtr.notes, false, true))
        return true;

    std::vector<Duco::Calling*>::const_iterator it = callings.begin();
    std::vector<Duco::Calling*>::const_iterator it2 = otherPtr.callings.begin();
    while (it != callings.end() && it2 != otherPtr.callings.end())
    {
        if ((*it2) != (*it))
            return true;

        ++it2;
        ++it;
    }

    return false;
}

std::wostream&
Composition::Print(std::wostream& stream, const RingingDatabase& /*database*/) const
{
    return stream;
}

DatabaseWriter&
Composition::Externalise(DatabaseWriter& writer) const
{
    writer.WriteId(id);
    writer.WriteBool(proven);
    writer.WriteInt(noOfChanges);
    writer.WriteInt(snapStartChangesMissing);
    writer.WriteInt(snapFinishChangesMissing);
    writer.WriteInt(composerId.Id());
    writer.WriteString(*composerName);
    writer.WriteInt(repeats);
    writer.WriteString(*name);
    writer.WriteString(*notes);
    writer.WriteId(methodId);
    writer.WriteInt(callings.size());
    std::vector<Duco::Calling*>::const_iterator it = callings.begin();
    while (it != callings.end())
    {
        (*it)->Externalise(writer);
        ++it;
    }

    return writer;
}

DatabaseReader&
Composition::Internalise(DatabaseReader& reader, unsigned int databaseVersion)
{
    if (databaseVersion > 44)
    {
        id = reader.ReadUInt();
    }
    proven = reader.ReadBool();
    noOfChanges = reader.ReadUInt();
    snapStartChangesMissing = reader.ReadUInt();
    snapFinishChangesMissing = reader.ReadUInt();
    composerId = reader.ReadUInt();
    if (databaseVersion >= 37)
    {
        reader.ReadString(*composerName);
    }
    repeats = reader.ReadUInt();
    reader.ReadString(*name);
    reader.ReadString(*notes);
    methodId = reader.ReadUInt();
    unsigned int noOfLeads (reader.ReadUInt());
    while (noOfLeads-- > 0)
    {
        Duco::Calling* newCalling = new Duco::Calling(reader, databaseVersion);
        callings.push_back(newCalling);
    }

    return reader;
}

void
Composition::SetMethodId(const Duco::ObjectId& newMethodId)
{
    methodId = newMethodId;
    delete notation;
    notation = NULL;
    delete currentEndChange;
    currentEndChange = NULL;
    proven = false;
}

const Duco::ObjectId&
Composition::MethodId() const
{
    return methodId;
}

bool
Composition::Valid(const Duco::RingingDatabase& ringingDb, bool warningFatal, bool clearBeforeStart) const
{
    return Valid(ringingDb.MethodsDatabase(), ringingDb.RingersDatabase(), warningFatal, clearBeforeStart);
}

bool
Composition::Valid(const Duco::MethodDatabase& methodDb, const Duco::RingerDatabase& ringerDb, bool warningFatal, bool clearBeforeStart) const
{
    if (clearBeforeStart)
    {
        ClearErrors();
    }
    if (name->length() <= 0)
    {
        errorCode.set(EComposition_CompositionName);
    }
    if (callings.empty())
    {
        errorCode.set(EComposition_TooFewCallings);
    }
    if (!methodId.ValidId() || methodDb.FindMethod(methodId) == NULL)
    {
        errorCode.set(EComposition_InvalidMethod);
    }
    if ((!composerId.ValidId() || ringerDb.FindRinger(composerId) == NULL) && composerName->length() == 0)
    {
        errorCode.set(EComposition_InvalidComposer);
    }

    return errorCode.none();
}

std::wstring
Composition::ErrorString(const Duco::DatabaseSettings& /*settings*/, bool /*showWarnings*/) const
{
    std::wstring errorStr;

    if (errorCode.test(EComposition_CompositionName))
    {
        DucoEngineUtils::AddError(errorStr, L"No composition name");
    }
    if (errorCode.test(EComposition_TooFewCallings))
    {
        DucoEngineUtils::AddError(errorStr, L"No callings");
    }
    if (errorCode.test(EComposition_InvalidMethod))
    {
        DucoEngineUtils::AddError(errorStr, L"Invalid method");
    }
    if (errorCode.test(EComposition_InvalidComposer))
    {
        DucoEngineUtils::AddError(errorStr, L"Invalid composer");
    }

    return errorStr;
}

bool
Composition::PrepareToProve(const MethodDatabase& methodDb, const Duco::DatabaseSettings& settings)
{
    Method* const theMethod = const_cast<Duco::Method*>(methodDb.FindMethod(methodId));
    if (theMethod != NULL)
    {
        return PrepareToProve(*theMethod, settings);
    }
    return false;
}

bool
Composition::PrepareToProve(Duco::Method& method, const Duco::DatabaseSettings& settings)
{
    if (method.Id() != methodId)
        return false;

    delete notation;
    notation = new PlaceNotation(method, callingId);
    delete currentEndChange;
    currentEndChange = new Change(method.Order());
    return method.ValidNotation(true, true, settings, true);
}

bool
Composition::Ready() const
{
    return methodId.ValidId() && notation != NULL && currentEndChange != NULL && currentEndChange->IsRounds();
}

bool
Composition::Prove(ProgressCallback& callback, const std::string& installationDir, bool stopAtFalse, bool stopAtRounds)
{
    if (notation == NULL)
        return false;

    proveInProgress = true;
    Reset();
    if (installationDir.length() > 0)
    {
        musicChecker = new Music(installationDir, Order());
    }
    if (leadEnds == NULL)
    {
        leadEnds = new LeadEndCollection();
    }
    else
    {
        leadEnds->Clear();
    }
    if (changes == NULL)
    {
        changes = new ChangeCollection(*currentEndChange);
    }
    else
    {
        changes->Clear(*currentEndChange);
    }
    unsigned int remainingRepeats(repeats+1);

    std::vector<Duco::Calling*>::const_iterator callingsIt = callings.begin();
    size_t snapStartPos (snapStartChangesMissing);
    while (callingsIt != callings.end() && remainingRepeats > 0)
    {
        (*callingsIt)->AdvanceToCall(*changes, *leadEnds, *notation, snapStartPos, stopAtFalse, stopAtRounds);
        snapStartPos = 0;
        ++callingsIt;

        if (callingsIt == callings.end() && remainingRepeats > 0)
        {
            --remainingRepeats;
            callingsIt = callings.begin();
            leadEnds->SetPartEnd();
        }
    }
    leadEnds->SetPartEnd();
    if (!changes->EndsWithRounds() && (!stopAtFalse || changes->True()) )
    {
        notation->AdvanceToRounds(*changes, *leadEnds);
    }
    if (musicChecker != NULL)
    {
        musicChecker->GenerateMusic(*changes, callback);
    }
    proven = true;
    snapFinishChangesMissing = changes->SnapFinish();
    noOfChanges = changes->NoOfChanges();

    proveInProgress = false;
    return changes->True();
}

std::wstring
Composition::MusicDetail() const
{
    if (musicChecker == NULL)
        return L"";

    return musicChecker->Detail();
}

std::wstring
Composition::FalseDetails() const
{
    return changes->FalseDetails();
}

const Duco::Change&
Composition::CurrentEndChange() const
{
    if (proven)
        return changes->EndChange();

    return *currentEndChange;
}

std::wstring
Composition::Title(const MethodDatabase& methodDb) const
{
    std::wstring returnVal;
    if (proven)
    {
        returnVal = DucoEngineUtils::ToString(noOfChanges);
        returnVal += L" changes";
    }
    else
    {
        returnVal = *name;
    }

    const Method* const theMethod = methodDb.FindMethod(methodId);
    if (theMethod != NULL)
    {
        returnVal += L" of ";
        returnVal += theMethod->FullName(methodDb);
    }
    returnVal += L".";
    return returnVal;
}

std::wstring
Composition::Composer(const RingerDatabase& ringerDb) const
{
    std::wstring returnVal (L"Composed by ");
    if (composerId.ValidId())
    {
        const Ringer* const theRinger = ringerDb.FindRinger(composerId);
        if (theRinger != NULL)
        {
            returnVal += theRinger->FullName(false);
        }
    }
    else
    {
        returnVal += (*composerName);
    }
    if (name != NULL && name->length() > 0)
    {
        returnVal += L" (";
        returnVal += *name;
        returnVal += L")";
    }
    return returnVal;
}

void
Composition::FullName(const RingingDatabase& ringingDb, std::wstring& title, std::wstring& composer) const
{
    title = Title(ringingDb.MethodsDatabase());
    composer = Composer(ringingDb.RingersDatabase());
}

std::wstring
Composition::FullNameForSort(bool unusedSetting) const
{
    if (noOfChanges > 0 && noOfChanges < 100000000)
    {
        std::wstring fullNameString = DucoEngineUtils::ToString(noOfChanges);
        fullNameString = DucoEngineUtils::PadStringWithZeros(fullNameString);
        fullNameString += L" ";
        if (name->length() > 0)
        {
            fullNameString += *name;
        }
        return fullNameString;
    }
    return *name;
}

const LeadEndCollection&
Composition::LeadEnds() const
{
    return *leadEnds;
}

bool
Composition::Proven() const
{
    return proven;
}

bool
Composition::ProvenTrue() const
{
    if (!Proven())
    {
        return false;
    }

    if (changes == NULL)
    {
        return false;
    }

    return changes->True();
}

size_t
Composition::NoOfChanges() const
{
    size_t totalNoOfChanges (noOfChanges);
    if (!proven)
    {
        totalNoOfChanges *= (1+(size_t)repeats);
        totalNoOfChanges -= snapStartChangesMissing;
        totalNoOfChanges -= snapFinishChangesMissing;
    }

    return totalNoOfChanges;
}

void
Composition::SetSnapStart(int newSnapStart)
{
    if (newSnapStart < 0)
    {
        snapStartChangesMissing = notation->ChangesPerLead() + newSnapStart;
    }
    else
        snapStartChangesMissing = newSnapStart;
}

unsigned int
Composition::Order() const
{
    if (notation != NULL)
        return notation->Order();

    return -1;
}

unsigned int
Composition::Order(const MethodDatabase& methodDb) const
{
    if (notation == NULL)
    {
        const Method* const theMethod = methodDb.FindMethod(methodId);
        if (theMethod != NULL)
        {
            return theMethod->Order();
        }
        return -1;
    }
    else
        return notation->Order();
}

size_t
Composition::ChangesPerLead() const
{
    if (notation == NULL)
        return std::numeric_limits<size_t>::max();

    return notation->ChangesPerLead();
}

const ChangeCollection&
Composition::Changes() const
{
    return *changes;
}

#ifndef __ANDROID__
void
Composition::ExportToXml(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& parent, bool exportDucoObjects) const
{
}
#endif

const std::wstring&
Composition::Notes() const
{
    return *notes;
}

void
Composition::SetNotes(const std::wstring& newNotes) const
{
    *notes = newNotes;
}

const Duco::ObjectId&
Composition::ComposerId() const
{
    return composerId;
}

const std::wstring&
Composition::ComposerName() const
{
    return *composerName;
}

void
Composition::SetComposerId(const Duco::ObjectId& newComposerId)
{
    composerId = newComposerId;
    composerName->clear();
}

void
Composition::SetComposerName(const std::wstring& newComposerName)
{
    composerId.ClearId();
    (*composerName) = newComposerName;
}

const std::wstring&
Composition::Name() const
{
    return *name;
}

void
Composition::SetName(const std::wstring& newName) const
{
    *name = newName;
}

unsigned int
Composition::Repeats() const
{
    return repeats;
}

void
Composition::SetRepeats(unsigned int newRepeats)
{
    repeats = newRepeats;
    proven = false;
}

Duco::PlaceNotation&
Composition::Notation() const
{
    return *notation;
}

Duco::CompositionTable
Composition::CreateCompositionTable() const
{
    CompositionTable newTable(notation->LeadOrder());

    std::vector<Duco::Calling*>::const_iterator it = callings.begin();
    while (it != callings.end())
    {
        newTable.AddCall(**it, *notation);
        ++it;
    }

    newTable.CompletedGeneration();

    return newTable;
}
