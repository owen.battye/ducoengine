#ifndef __EXTERNALMETHOD_H__
#define __EXTERNALMETHOD_H__

#include <string>
#include "ObjectId.h"

namespace Duco
{

class ExternalMethod
{
    public:
        ExternalMethod(const Duco::ObjectId& newId, const std::wstring& newName, const std::wstring& newType, const std::wstring& newTitle,
                       const std::wstring& newNotation, unsigned int newStage);
        ~ExternalMethod();

        DllExport const std::wstring& Name() const;
        DllExport const std::wstring& Type() const;
        DllExport const std::wstring& Title() const;
        DllExport const std::wstring& Notation() const;
        inline unsigned int Stage() const;
        DllExport const Duco::ObjectId& Id() const;

    protected:
        Duco::ObjectId  id;
        unsigned int    stage;
        std::wstring*    name;
        std::wstring*    type;
        std::wstring*    title;
        std::wstring*    notation;
};

unsigned int
ExternalMethod::Stage() const
{
    return stage;
}

}


#endif //!__EXTERNALMETHOD_H__
