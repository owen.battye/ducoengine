#include "MethodNotationDatabaseSingleImporter.h"

#include "Method.h"
#include "MethodDatabase.h"
#include "DucoEngineUtils.h"
#include "ExternalMethod.h"

using namespace std;
using namespace Duco;

MethodNotationDatabaseSingleImporter::MethodNotationDatabaseSingleImporter(Duco::MethodDatabase& newDatabase, const std::string& newDatabaseFilename, ProgressCallback* const newCallback)
: MethodNotationDatabaseBase(newDatabase, newDatabaseFilename, newCallback)
{
}

MethodNotationDatabaseSingleImporter::~MethodNotationDatabaseSingleImporter()
{
    std::multimap<unsigned int, ExternalMethod*>::iterator it = methods.begin();
    while (it != methods.end())
    {
        delete it->second;
        ++it;
    }
}

void
MethodNotationDatabaseSingleImporter::AddMethod(const Duco::ObjectId& id, unsigned int stage, const std::wstring& name, const std::wstring& type, const std::wstring& title, const std::wstring& notation)
{
    ExternalMethod* newMethod = new ExternalMethod(id, name, type, title, notation, stage);
    std::pair<unsigned int, ExternalMethod*> newObject (stage, newMethod);
    methods.insert(newObject);
}

std::pair<std::multimap<unsigned int,Duco::ExternalMethod*>::const_iterator,std::multimap<unsigned int,Duco::ExternalMethod*>::const_iterator>
MethodNotationDatabaseSingleImporter::FindMethods(unsigned int stage) const
{
    return methods.equal_range(stage);
}

std::multimap<unsigned int,Duco::ExternalMethod*>::const_iterator
MethodNotationDatabaseSingleImporter::Begin() const
{
    return methods.begin();
}

std::multimap<unsigned int,Duco::ExternalMethod*>::const_iterator
MethodNotationDatabaseSingleImporter::End() const
{
    return methods.end();
}

const ExternalMethod* const
MethodNotationDatabaseSingleImporter::FindMethod(const Duco::ObjectId& methodId) const
{
    std::multimap<unsigned int, ExternalMethod*>::const_iterator it = methods.begin();
    while (it != methods.end())
    {
        if (it->second->Id() == methodId)
        {
            return it->second;
        }
        ++it;
    }
    return NULL;
}

Duco::ObjectId
MethodNotationDatabaseSingleImporter::FindMethod(const std::wstring& fullMethodName) const
{
    unsigned int methodOrder = 0;
    bool dualOrder = false;
    std::wstring methodName = fullMethodName;
    std::wstring methodType = L"";
    if (database.FindAndRemoveOrderName(methodOrder, methodName, dualOrder))
    {
        database.FindAndRemoveMethodType(methodType, methodName);

        std::multimap<unsigned int, ExternalMethod*>::const_iterator it = Begin();
        while (it != End())
        {
            if (it->second->Stage() == methodOrder)
            {
                if (it->second->Name() == methodName)
                {
                    bool methodFound = false;
                    if (it->second->Type() == methodType)
                    {
                        methodFound = true;
                    }
                    else if (methodType.length() == 1 && it->second->Type().length() >= 1)
                    {
                        wchar_t findChar = methodType[0];
                        findChar = toupper(findChar);
                        wchar_t findType = it->second->Type()[0];
                        findType = toupper(findType);
                        if (findChar == findType)
                        {
                            methodFound = true;
                        }
                    }

                    if (methodFound)
                    {
                        Duco::Method newMethod(*it->second);
                        return database.AddObject(newMethod);
                    }
                }
            }
            ++it;
        }
    }
    return KNoId;
}
