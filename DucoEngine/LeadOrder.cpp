#include "LeadOrder.h"

#include "Calling.h"
#include "Change.h"
#include "DucoEngineUtils.h"
#include <algorithm>

using namespace Duco;
using namespace std;

LeadOrder::LeadOrder(const Duco::LeadOrder& other)
:   leadOrder(other.leadOrder), includeTreble(other.includeTreble),
    wrongPosition(other.wrongPosition), homePosition(other.homePosition), middlePosition(other.middlePosition)
{
    leadOrderPosition = leadOrder.end();
}

LeadOrder::LeadOrder()
:   includeTreble(false),
    wrongPosition(-1), homePosition(-1), middlePosition(-1)
{
    leadOrderPosition = leadOrder.end();
}

LeadOrder::~LeadOrder()
{

}

Duco::LeadOrder&
LeadOrder::operator=(const Duco::LeadOrder& other)
{
    leadOrder = other.leadOrder;
    includeTreble = other.includeTreble;
    wrongPosition = other.wrongPosition;
    homePosition = other.homePosition;
    middlePosition = other.middlePosition;
    leadOrderPosition = leadOrder.end();

    return *this;
}

Duco::LeadOrder&
LeadOrder::Reset()
{
    leadOrder.clear();
    includeTreble = false;
    wrongPosition = -1;
    homePosition = -1;
    middlePosition = -1;
    leadOrderPosition = leadOrder.end();

    return *this;
}

bool
LeadOrder::ValidLeadOrder(unsigned int order) const
{
    return leadOrder.size() != (order - 1);
}

void
LeadOrder::SetPositions()
{
    //Set wrong home and middle positions
    std::vector<unsigned int>::iterator findHomeIt = leadOrder.begin();
    while (findHomeIt != leadOrder.end())
    {
        if (homePosition == -1)
        {
            homePosition = *findHomeIt;
        }
        else if (*findHomeIt == 1)
            includeTreble = true;
        else
            homePosition = max(homePosition, *findHomeIt);
        ++findHomeIt;
    }
    if (homePosition != -1)
    {
        wrongPosition = homePosition - 1;
        middlePosition = homePosition - 2;

        std::vector<unsigned int>::const_reverse_iterator reorderIt = leadOrder.rbegin();
        while (reorderIt != leadOrder.rend() && *reorderIt != homePosition)
        {
            std::vector<unsigned int>::iterator removeIt = leadOrder.begin();
            unsigned int remove = *removeIt;
            leadOrder.erase(removeIt);
            leadOrder.push_back(remove);
            reorderIt = leadOrder.rbegin();   
        }
    }
    leadOrderPosition = leadOrder.end();
}

std::wstring
LeadOrder::CheckPosition(unsigned int position) const
{
    if (position == homePosition)
        return L"H";
    else if (position == wrongPosition)
        return L"W";
    else if (position == middlePosition)
        return L"M";
    else if (position == 3)
        return L"O";
    else if (position == 2)
        return L"I";

    wchar_t temp = DucoEngineUtils::ToChar(position);
    std::wstring tempStr;
    tempStr += temp;

    return tempStr;
}

unsigned int
LeadOrder::CheckPosition(wchar_t position) const
{
    switch (position)
    {
        case L'H':
        case L'h':
            return homePosition;
        case L'W':
        case L'w':
            return wrongPosition;
        case L'M':
        case L'm':
            return middlePosition;
        case L'O':
        case L'o':
        case L'3':
            return 3;
        case L'I':
        case L'i':
        case L'2':
            return 2;

        default:
            break;
    }

    return DucoEngineUtils::ToInteger(position);
}

std::wstring
LeadOrder::StartingLeadEnd(unsigned int order) const
{
    Duco::Change rounds(order);
    return rounds.Str(false, includeTreble);
}

const std::vector<unsigned int>&
LeadOrder::Leads() const
{
    return leadOrder;
}

bool
LeadOrder::AddCall(unsigned int callingPosition)
{
    std::vector<unsigned int>::const_iterator originalLeadOrderPosition = leadOrderPosition;
    if (leadOrderPosition == leadOrder.end())
    {
        leadOrderPosition = leadOrder.begin();
    }
    else if ((*leadOrderPosition) == callingPosition)
    {
        return true;
    }

    while (leadOrderPosition != leadOrder.end())
    {
        if ((*leadOrderPosition) == callingPosition)
        {
            return true;
        }
        ++leadOrderPosition;
    }

    leadOrderPosition = originalLeadOrderPosition;
    return false;
}

bool
LeadOrder::IsPrinciple() const
{
    if (leadOrder.size() == 0 || find(leadOrder.begin(), leadOrder.end(), 1) != leadOrder.end())
    {
        return true;
    }

    return false;
}

bool
LeadOrder::IsHome(const Calling& calling, bool affectedPosition) const
{
    return calling.Position() == HomePosition();
}

unsigned int
LeadOrder::Index(unsigned int bell) const
{
    std::vector<unsigned int>::const_iterator it = leadOrder.begin();
    unsigned int count = 0;
    while (it != leadOrder.end())
    {
        if (*it == bell)
        {
            return count;
        }
        ++count;
        ++it;
    }

    return -1;
}