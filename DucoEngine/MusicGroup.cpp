#include "MusicGroup.h"

#include "Change.h"
#include "DucoEngineUtils.h"

using namespace Duco;
using namespace std;

MusicGroup::MusicGroup(const std::wstring& theNames)
:   MusicObject(), name(NULL)
{
    size_t nextSeperatorChar = theNames.find('.', 0);
    if (nextSeperatorChar == std::wstring::npos)
    {
        SetBells(theNames);
    }
    else
    {
        name = new std::wstring();
        SetBells(theNames.substr(0, nextSeperatorChar));
        size_t previousSeperatorChar = nextSeperatorChar;
        nextSeperatorChar = theNames.find('.', previousSeperatorChar+1);
        if (nextSeperatorChar == string::npos)
        {
            name->append(theNames.substr(previousSeperatorChar+1));
        }
        else
        {
            name->append(theNames.substr(previousSeperatorChar+1, nextSeperatorChar - previousSeperatorChar - 1));
            previousSeperatorChar = nextSeperatorChar;
            nextSeperatorChar = theNames.find('.', previousSeperatorChar+1);

            while (nextSeperatorChar != string::npos)
            {
                AddMusicObject(theNames.substr(previousSeperatorChar+1, nextSeperatorChar - previousSeperatorChar - 1));
                previousSeperatorChar = nextSeperatorChar;
                nextSeperatorChar = theNames.find('.', previousSeperatorChar+1);
            }
            AddMusicObject(theNames.substr(previousSeperatorChar+1, nextSeperatorChar - previousSeperatorChar - 1));
        }
    }
}

MusicGroup::~MusicGroup()
{
    delete name;
    std::vector<MusicObject*>::iterator it = objects.begin();
    while (it != objects.end())
    {
        delete *it;
        ++it;
    }
}

bool
MusicGroup::Match(Duco::Change& change)
{
    if (MusicObject::Match(change))
    {
        bool matched (false);
        std::vector<MusicObject*>::iterator it = objects.begin();
        while (it != objects.end() && !matched)
        {
            matched = (*it)->Match(change);
            ++it;
        }

        return true;
    }
    return false;
}

std::wstring
MusicGroup::Name(bool showAll) const
{
    if (name != NULL && name->length() > 0)
        return *name;

    return MusicObject::Name(showAll);
}

std::wstring
MusicGroup::Print() const
{
    switch (count)
    {
    case 1:
        if (name != NULL && name->length() > 0)
        {
            return *name;
        }
        else
        {
            std::wstring returnVal (L"");
            returnVal += MusicObject::Name(true);
            return returnVal;
        }

    default:
        {
            std::wstring returnVal (Name(true));
            returnVal += L"s";

            if (ContainsSubCount())
            {
                returnVal += L"; of which: ";
                returnVal += KNewlineChar;
                returnVal += L"\t\t";
                std::vector<MusicObject*>::const_iterator it = objects.begin();
                bool first (true);
                while (it != objects.end())
                {
                    if ((*it)->Count() > 0)
                    {
                        if (!first)
                        {
                            returnVal += L", ";
                        }
                        first = false;
                        returnVal += (*it)->Print();
                    }
                    ++it;
                }
            }
            return returnVal;
        }
    case 0:
        break;
    }

    return L"";
}

bool
MusicGroup::AddMusicObject(const std::wstring& newObjectStr)
{
    if (newObjectStr.length() == Order())
    {
        MusicObject* newObj = new MusicObject();
        newObj->SetBells(newObjectStr);
        objects.push_back(newObj);
        return true;
    }
    return false;
}

bool
MusicGroup::ContainsSubCount() const
{
    unsigned int count (0);
    std::vector<MusicObject*>::const_iterator it = objects.begin();
    while (count == 0 && it != objects.end())
    {
        count += (*it)->Count();
        ++it;
    }
    return count > 0;
}