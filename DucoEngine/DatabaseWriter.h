#ifndef __DATABASEWRITER_H__
#define __DATABASEWRITER_H__

#include <fstream>
#include "DucoEngineCommon.h"

namespace Duco
{
    class PerformanceDate;
    class PerformanceTime;
    class ObjectId;

class DatabaseWriter
{
public:
    DllExport DatabaseWriter(const char* filename);
    DllExport ~DatabaseWriter();

    DllExport void WriteInt(int number);
    DllExport void WriteInt(unsigned int number);
    DllExport void WriteInt(unsigned long long  number);
    DllExport void WriteBool(bool number);
    DllExport void WriteTime(const time_t& date);
    DllExport void WriteString(const std::wstring& str);
    DllExport void WritePictureBuffer(const std::string& pictureBuffer);
    DllExport void WriteDate(const Duco::PerformanceDate& date);
    DllExport void WriteTime(const Duco::PerformanceTime& time);
    DllExport void WriteId(const Duco::ObjectId& id);

private:
    std::wofstream output;
};

} // end namespace Duco

#endif //!__DATABASEWRITER_H__
