#ifndef __DUCOINDEX_H__
#define __DUCOINDEX_H__

#include "ObjectId.h"

namespace Duco
{
    class RingingObject;

class DucoIndex
{
public:
    DucoIndex(const Duco::ObjectId& objectId, Duco::RingingObject* object);
    ~DucoIndex();
    const Duco::ObjectId& Id() const;
    Duco::RingingObject* Object() const;

private:
    Duco::ObjectId objId;
    Duco::RingingObject* obj;
};

}

#endif //!__DUCOINDEX_H__
