#ifndef __RING_H__
#define __RING_H__

#include <set>
#include <map>
#include <string>
#include "DucoEngineCommon.h"
#include "RingingObject.h"
#include "DucoTypes.h"

namespace Duco
{
class DatabaseWriter;
class DatabaseReader;
class DoveObject;

class Ring : public RingingObject
{
    friend class Tower;

public:
    DllExport Ring();
    DllExport Ring(DatabaseReader& reader, unsigned int databaseVersion);
    DllExport Ring(const Duco::ObjectId& newId, unsigned int newNoOfBells, const std::wstring& name, const std::set<unsigned int>& newBells, const std::wstring& newTenorWeight, const std::wstring& newTenorKey);
    DllExport Ring(const Ring& other);
    DllExport Ring(const Duco::ObjectId& newId, const Ring& other);
    DllExport ~Ring();

    DllExport virtual TObjectType ObjectType() const;
    DllExport virtual bool Duplicate(const Duco::RingingObject& other, const Duco::RingingDatabase& database) const;
    DllExport bool Duplicate(const Duco::Ring& otherRing) const;

    DllExport bool BellsMatch(const Duco::Ring& otherObject) const;
    DllExport bool Match(const Duco::Ring& otherObject) const;
    bool IncludesBells(unsigned int firstBell, unsigned int secondBell) const;

    DatabaseReader& Internalise(DatabaseReader& reader, unsigned int databaseVersion);
    DatabaseWriter& Externalise(DatabaseWriter& writer) const;
    DllExport std::wostream& Print(std::wostream& stream, const RingingDatabase& database) const;
#ifndef __ANDROID__
    virtual void ExportToXml(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& parent, bool exportDucoObjects) const;
#endif

    DllExport virtual bool operator==(const Duco::RingingObject& other) const;
    DllExport virtual bool operator!=(const Duco::RingingObject& other) const;
    DllExport virtual Ring& operator=(const Duco::Ring& other);

    // Accessor
    inline unsigned int NoOfBells() const;
    inline const std::wstring& TenorWeight() const;
    inline const std::wstring& TenorKey() const;
    DllExport unsigned int TenorBellNumber() const;
    DllExport std::wstring TenorDescription() const;
    inline const std::wstring& Name() const;
    DllExport bool RealBellNumber(unsigned int& bellNumber) const;
    DllExport bool BellNumberInRing(unsigned int bellNumber) const;
	DllExport bool ContainsBell(unsigned int realBellNumber) const;

    // Settors
    inline void SetNoOfBells(unsigned int newNoOfBells);
    DllExport void SetTenorWeight(const std::wstring& newTenorWeight);
    DllExport void SetTenorKey(const std::wstring& newTenorKey);
    inline void SetName(const std::wstring& newName);
    DllExport void ChangeBell(unsigned int bellId, bool add);
    DllExport void UpdateTenorWeightToDove(const std::wstring& newTenor, int noOfbells);
    DllExport bool UpdateTenorKeyFromDove(const Duco::DoveObject& other, unsigned int noOfBells);
    bool ClearTenorKeyIfInvalidChars();

    DllExport void SetMaxBellCount(unsigned int maxBellCount);
    DllExport bool Valid(const std::map<unsigned int, Duco::TRenameBellType>& renamedBells, unsigned int maxNoOfBellsInRing, unsigned int noOfBellsInTower, bool includeWarning, bool clearBeforeStart) const;
    DllExport bool Valid(const RingingDatabase& ringingDb, bool warningFatal, bool clearBeforeStart) const;
    DllExport std::wstring ErrorString(const Duco::DatabaseSettings& /*settings*/, bool showWarnings) const;

protected:
    DllExport bool ChangeBellsBy(int delta, unsigned int totalNoOfBells);

private:
    unsigned int            noOfBells;
    std::set<unsigned int>  bells;
    std::wstring            tenorKey;
    std::wstring            tenorWeight;
    std::wstring             name;
};

const std::wstring&
Ring::Name() const
{
    return name;
}

unsigned int
Ring::NoOfBells() const
{
    return noOfBells;
}

const std::wstring&
Ring::TenorWeight() const
{
    return tenorWeight;
}

const std::wstring&
Ring::TenorKey() const
{
    return tenorKey;
}

void
Ring::SetNoOfBells(unsigned int newNoOfBells)
{
    noOfBells = newNoOfBells;
}

void
Ring::SetName(const std::wstring& newName)
{
    name = newName;
}

} // end namespace Duco

#endif //!__RING_H__
