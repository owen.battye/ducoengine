#ifndef __DATABASEEXPORTER_H__
#define __DATABASEEXPORTER_H__

#include "DatabaseWriter.h"
#include "MethodDatabase.h"
#include "Peal.h"

#include <set>

namespace Duco
{
class ImportExportProgressCallback;
class RingingDatabase;

class DatabaseExporter
{
public:
    DllExport DatabaseExporter(const Duco::RingingDatabase& theDatabase);
    DllExport ~DatabaseExporter();

    DllExport bool AddAllObjectsForPeals(const std::set<ObjectId>& pealIds);
    DllExport bool Export(const char* fileName, Duco::ImportExportProgressCallback* newCallback);
    DllExport size_t NumberOfPealsToImport() const;
    DllExport size_t TotalNumberOfObjectsToImport() const;

private:
    const Duco::RingingDatabase&  database;

    std::set<ObjectId> pealIds;
    std::set<ObjectId> requiredAssociations;
    std::set<ObjectId> requiredCompositions;
    std::set<ObjectId> requiredMethods;
    std::set<ObjectId> requiredMethodSeries;
    std::set<ObjectId> requiredPictures;
    std::set<ObjectId> requiredRingers;
    std::set<ObjectId> requiredTowers;
};

} // end namespace Duco

#endif //!__DATABASEEXPORTER_H__
