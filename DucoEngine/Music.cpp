#include "Music.h"

#include "ChangeCollection.h"
#include "DucoEngineUtils.h"
#include "MusicGroup.h"
#include "Rollup.h"
#include "ProgressCallback.h"
#include <fstream>

using namespace Duco;
using namespace std;

Music::Music(const std::string& installationDir, unsigned int noOfBells, const std::string& fileName)
{
    std::string musicFile(installationDir);
    musicFile.append(fileName);
    wifstream* inputFile = new wifstream(musicFile.c_str());
    if (inputFile != NULL && inputFile->is_open())
    {
        while (!inputFile->eof())
        {
            std::wstring nextLine;
            getline(*inputFile, nextLine);
            if (nextLine.length() > 0)
            {
                if (nextLine[0] == '!')
                {
                    Rollup* newRollup = new Rollup(nextLine);
                    if (newRollup->Order() == noOfBells)
                        rollups.push_back(newRollup);
                    else
                        delete newRollup;
                }
                else if (nextLine[0] != '#')
                {
                    MusicGroup* newObject = new MusicGroup(nextLine);
                    if (newObject->Order() == noOfBells)
                        objects.push_back(newObject);
                    else
                        delete newObject;
                }
            }
        }
        inputFile->close();
    }
    delete inputFile;
    inputFile = NULL;
}

Music::~Music()
{
    std::list<Duco::MusicGroup*>::iterator it = objects.begin();
    while (it != objects.end())
    {
        delete *it;
        ++it;
    }
    std::list<Duco::Rollup*>::iterator it2 = rollups.begin();
    while (it2 != rollups.end())
    {
        delete *it2;
        ++it2;
    }
}

/*bool sort_music (Duco::MusicGroup* first, Duco::MusicGroup* second)
{
    return first->Count() < second->Count();
}*/

void
Music::GenerateMusic(const ChangeCollection& changes, ProgressCallback& callback)
{
    callback.Initialised();

    size_t noOfChanges = changes.NoOfChanges();
    std::set<Duco::Change*, ChangeComparer>::const_iterator it = changes.changes->begin();

    unsigned int changeNo (0);
    while (it != changes.changes->end())
    {
        ++changeNo;
        callback.Step(int(float(changeNo) / float(noOfChanges) * 100));
        CheckChange(**it);
        CheckChangeForRollup(**it);
        ++it;
    }
    callback.Complete(false);
//    objects.sort(sort_music);
}

bool
Music::CheckChange(Duco::Change& change) const
{
    bool found (false);
    std::list<Duco::MusicGroup*>::const_iterator it = objects.begin();
    while (it != objects.end())
    {
        found |= (*it)->Match(change);
        ++it;
    }
    return found;
}

bool
Music::CheckChangeForRollup(Duco::Change& change) const
{
    bool found (false);
    std::list<Duco::Rollup*>::const_iterator it = rollups.begin();
    while (it != rollups.end())
    {
        found |= (*it)->Match(change);
        ++it;
    }
    return found;
}

std::wstring
Music::Detail() const
{
    std::wstring musicStr(L"");

    std::list<Duco::MusicGroup*>::const_iterator it = objects.begin();
    while (it != objects.end())
    {
        switch ((*it)->Count())
        {
        case 0:
            break;

        case 1:
        {
            if (musicStr.length() > 0)
            {
                musicStr += KNewlineChar;
            }
            musicStr += (*it)->Print();
        }
        break;

        default:
        {
            musicStr += KNewlineChar;
            musicStr += (*it)->Print();
        }
        break;
        }
        ++it;
    }

    std::wstring rollUpsString = L"Rollups:";
    std::list<Duco::Rollup*>::const_iterator it2 = rollups.begin();
    while (it2 != rollups.end())
    {
        if ((*it2)->Count() > 0)
        {
            rollUpsString += KNewlineChar;
            rollUpsString += (*it2)->Print();
        }
        ++it2;
    }
    if (rollUpsString.length() > 9)
    {
        if (musicStr.length() > 0)
        {
            musicStr += KNewlineChar;
            musicStr += KNewlineChar;
        }
        musicStr.append(rollUpsString);
    }

    return musicStr;
}

size_t
Music::NumberOfMusicGroups() const
{
    return objects.size();
}

size_t
Music::NumberOfRollups() const
{
    return rollups.size();
}
