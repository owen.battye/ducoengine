#ifndef __LEAD_H__
#define __LEAD_H__

#include "DucoEngineCommon.h"
#include "LeadType.h"
#include <vector>
#include <cstddef>

namespace Duco
{
    class Calling;
    class Change;
    class ChangeCollection;
    class Method;
    class PlaceNotation;

class Lead
{
    friend class PlaceNotation;

public:
    DllExport virtual ~Lead();

    DllExport bool operator==(const Duco::Lead& other) const;
    DllExport bool operator!=(const Duco::Lead& other) const;

    DllExport bool LeadEnd(Duco::TLeadType newType, Duco::Change& change) const;
    DllExport bool LeadEnd(Duco::Change& change) const;
    DllExport bool Position(size_t changeNo, unsigned int bell, unsigned int& position) const;
    DllExport bool LeadEndPosition(unsigned int bell, unsigned int& position) const;
    DllExport bool TreblePosition(unsigned int changeNo, unsigned int& position) const;
    DllExport bool Bell(unsigned int changeNo, unsigned int position, unsigned int& bell) const;
    DllExport const Duco::Change& Change(size_t changeNo) const;
    DllExport Duco::Change& Change(size_t changeNo);
    DllExport void Changes(Duco::ChangeCollection& changes, bool stopAtFalse, bool stopAtRounds) const;
    inline size_t NoOfChanges() const;
    unsigned int Order() const;

    DllExport size_t BlowsInLead() const;
    DllExport bool LeadEndDifferencesFromRounds(std::vector<unsigned int>& bells) const;
    DllExport bool AffectedBells(Duco::TLeadType newType, std::vector<unsigned int>& bells) const;
    DllExport bool EndsWithRounds(Duco::TLeadType newType) const;
    DllExport bool EndsWithRounds() const;

    DllExport bool CallingPosition(Duco::TLeadType newType, unsigned int& currentPosition) const;
    DllExport bool IsCallingPosition(const Duco::Calling& calling) const;
    DllExport bool IsCallingPosition(Duco::TLeadType newType, const Duco::Calling& calling) const;
    DllExport bool IsCallingPosition(Duco::TLeadType newType, unsigned int position) const;
    DllExport bool IsHome(Duco::TLeadType newType) const;
    DllExport bool SetLeadEndType(Duco::TLeadType newType);
    inline Duco::TLeadType LeadEndType() const;
    DllExport bool IsCourseEnd() const;

    DllExport void Clear();

protected:
    DllExport Lead(const Duco::Method& newMethod);
    DllExport void SetLeadEndChange(Duco::TLeadType newType, const Duco::Change& leadEndChange);

protected:
    const Duco::Method& currentMethod;

    TLeadType           leadType;
    std::vector<Duco::Change*> changes;
    Duco::Change*       bobLeadEnd;
    Duco::Change*       singleLeadEnd;
};

Duco::TLeadType
Lead::LeadEndType() const
{
    return leadType;
}

size_t
Lead::NoOfChanges() const
{
    return changes.size() - 1;
}

}

#endif //!__LEAD_H__
