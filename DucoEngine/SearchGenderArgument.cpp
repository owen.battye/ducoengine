#include "SearchGenderArgument.h"

#include "RingingDatabase.h"
#include "MethodDatabase.h"
#include "Peal.h"
#include "Tower.h"
#include "Ringer.h"
#include "Method.h"

using namespace Duco;

TSearchGenderArgument::TSearchGenderArgument(bool newMale, bool newFemale)
: TSearchFieldBoolArgument(EGenderField, newMale), female(newFemale)
{

}

TSearchGenderArgument::~TSearchGenderArgument()
{

}

bool
TSearchGenderArgument::Match(const Duco::Ringer& ringer, const Duco::RingingDatabase& database) const
{
    switch (fieldId)
    {
    case EGenderField:
        return (fieldValue && ringer.Male()) || (female && ringer.Female() || (!fieldValue && !ringer.Male() && !female && !ringer.Female()));

    default:
        break;
    }

    return false;
}

bool
TSearchGenderArgument::Match(const Duco::Tower& tower, const Duco::RingingDatabase& database) const
{
    return false;
}

bool 
TSearchGenderArgument::Match(const Duco::Method& method, const Duco::RingingDatabase& database) const
{
    return false;
}


bool 
TSearchGenderArgument::Match(const Duco::Peal& peal, const Duco::RingingDatabase& database) const
{
    return false;
}

bool
TSearchGenderArgument::Match(const Duco::Composition& object, const Duco::RingingDatabase& database) const
{
    return false;
}