#include "SearchFieldTimeArgument.h"

#include "Peal.h"
#include "Tower.h"

using namespace Duco;

TSearchFieldTimeArgument::TSearchFieldTimeArgument(Duco::TSearchFieldId newFieldId, const Duco::PerformanceTime& newFieldValue, Duco::TSearchType newSearchType)
:   TSearchArgument(newFieldId), fieldValue(newFieldValue), searchType(newSearchType)
{

}

bool
TSearchFieldTimeArgument::Match(const Duco::Peal& peal, const Duco::RingingDatabase& database) const
{
    switch (fieldId)
    {
        case Duco::EPealTime:
            return CompareTime(peal.Time());

        default:
            return false;
    }
}

Duco::TFieldType 
TSearchFieldTimeArgument::FieldType() const
{
    return TFieldType::ETimeField;
}

bool
TSearchFieldTimeArgument::CompareTime(const Duco::PerformanceTime& pealTime) const
{
    bool returnVal (false);
    switch (searchType)
    {
    case ELessThan:
        returnVal = pealTime < fieldValue;
        break;
    case ELessThanOrEqualTo:
        returnVal = pealTime <= fieldValue;
        break;
    case EEqualTo:
        returnVal = pealTime == fieldValue;
        break;
    case EMoreThanOrEqualTo:
        returnVal = pealTime >= fieldValue;
        break;
    case EMoreThan:
        returnVal = pealTime > fieldValue;
        break;
    }

    return returnVal;
}
