#include "RingerDatabase.h"

#include "CompositionDatabase.h"
#include "DatabaseSettings.h"
#include "DucoEngineUtils.h"
#include <fstream>
#include "PealDatabase.h"
#include "ImportExportProgressCallback.h"
#include "Ringer.h"
#include "RingerNameChange.h"
#include "RingingDatabase.h"
#include "RenumberProgressCallback.h"
#include "SearchArgument.h"
#include "SearchValidationArgument.h"
#include "StatisticFilters.h"
#include "DucoEngineLog.h"
#include <chrono>
#include "ProgressCallback.h"
#include "DatabaseReader.h"
#include "DatabaseWriter.h"

using namespace std::chrono;
using namespace Duco;
using namespace std;

#include <xercesc\dom\DOMElement.hpp>
XERCES_CPP_NAMESPACE_USE

RingerDatabase::RingerDatabase()
:   RingingObjectDatabase(TObjectType::ERinger)
{
}

RingerDatabase::~RingerDatabase()
{

}

void
RingerDatabase::ClearObjects(bool createDefaultBellNames)
{
    RingingObjectDatabase::ClearObjects(createDefaultBellNames);
    linkedRingerIds.clear();
}

Duco::ObjectId
RingerDatabase::AddObject(const Duco::RingingObject& newObject)
{
    Duco::ObjectId addedId;

    if (newObject.ObjectType() == TObjectType::ERinger)
    {
        const Duco::Ringer& newRinger = static_cast<const Duco::Ringer&>(newObject);

        Duco::RingingObject* newRingerWithId = new Duco::Ringer(newRinger);
        if (AddOwnedObject(newRingerWithId))
        {
            addedId = newRingerWithId->Id(); 
        }
        // object is deleted by RingingObjectDatabase::AddObject if not added.
    }
    return addedId;
}

bool
RingerDatabase::SaveUpdatedObject(Duco::RingingObject& lhs, const Duco::RingingObject& rhs)
{
    if (lhs.ObjectType() == rhs.ObjectType() && rhs.ObjectType() == TObjectType::ERinger)
    {
        Duco::Ringer& lhsObject = static_cast<Duco::Ringer&>(lhs);
        const Duco::Ringer& rhsObject = static_cast<const Duco::Ringer&>(rhs);
        lhsObject = rhsObject;
        return true;
    }
    return false;
}

void
RingerDatabase::ExternaliseToXml(Duco::ImportExportProgressCallback* newCallback, XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, bool exportDucoObjects) const
{
    size_t count = 0;
    DOMElement* rootElem = outputFile.getDocumentElement();
    DOMElement* newRingers = outputFile.createElement(XMLStrL("Ringers"));
    rootElem->appendChild(newRingers);

    for (auto const& [key, value] : listOfObjects)
    {
        ++count;
        const Duco::Ringer* theRinger = static_cast<const Duco::Ringer*>(value);
        theRinger->ExportToXml(outputFile, *newRingers, exportDucoObjects);

        newCallback->ObjectProcessed(false, TObjectType::ERinger, value->Id(), 0, count);
    }
}

Duco::ObjectId
RingerDatabase::AddRinger(const std::wstring& newRingerName)
{
    std::wstring lastName = L"";
    std::wstring firstNames = L"";
    bool isConductor(false);
    DucoEngineUtils::SplitRingerName(newRingerName, firstNames, lastName, isConductor);
    return AddRinger(firstNames, lastName);
}

Duco::ObjectId
RingerDatabase::AddRinger(const std::wstring& forename, const std::wstring& surname)
{
    Duco::RingingObject* newRinger = new Duco::Ringer(KNoId, forename, surname);

    Duco::ObjectId addedId;
    if (AddOwnedObject(newRinger))
    {
        addedId = newRinger->Id();
    }
    return addedId;
}

bool
RingerDatabase::Internalise(DatabaseReader& reader, Duco::ImportExportProgressCallback* callback, int databaseVersionNumber, size_t noOfObjectsInThisDatabase, size_t totalObjects, size_t objectsProcessedSoFar)
{
    size_t totalProcessed = objectsProcessedSoFar;
    size_t count = 0;

    while (noOfObjectsInThisDatabase-- > 0)
    {
        ++totalProcessed;
        ++count;
        float percentageComplete = ((float)totalProcessed / (float)totalObjects) * 100;
        Duco::RingingObject* newRinger = new Duco::Ringer(reader, databaseVersionNumber);
        if (newRinger == NULL)
            return false;

        AddOwnedObject(newRinger);
        if (callback != NULL)
        {
            callback->ObjectProcessed(true, TObjectType::ERinger, newRinger->Id(), (int)percentageComplete, count);
        }
    }
    if (databaseVersionNumber >= 52)
    {
        int numberOfLinkedRingerGroups = reader.ReadInt();
        for (int i = 0; i < numberOfLinkedRingerGroups; ++i)
        {
            int numberOfRingers = reader.ReadInt();
            std::set<Duco::ObjectId> linkedRingers;
            for (int j = 0; j < numberOfRingers; ++j)
            {
                Duco::ObjectId nextRingerId;
                reader.ReadId(nextRingerId);
                if (nextRingerId.ValidId())
                {
                    linkedRingers.insert(nextRingerId);
                }
            }
            linkedRingerIds.push_back(linkedRingers);
        }
    }
    return true;
}

void
RingerDatabase::Externalise(Duco::ImportExportProgressCallback* newCallback, Duco::DatabaseWriter& writer, size_t noOfObjectsInThisDatabase, size_t totalObjects, size_t objectsProcessedSoFar) const
{
    RingingObjectDatabase::Externalise(newCallback, writer, noOfObjectsInThisDatabase, totalObjects, objectsProcessedSoFar);
    writer.WriteInt(linkedRingerIds.size());
    for (auto it : linkedRingerIds)
    {
        writer.WriteInt(it.size());
        for (auto it2 : it)
        {
            writer.WriteId(it2);
        }
    }
}


const Ringer* const
RingerDatabase::FindRinger(const Duco::ObjectId& ringerId, bool setIndex) const
{
    const RingingObject* const object = FindObject(ringerId, setIndex);

    return static_cast<const Ringer* const>(object);
}

bool
RingerDatabase::RebuildObjects(Duco::RenumberProgressCallback* callback, Duco::RingingDatabase& database)
{
    if (listOfObjects.size() <= 0)
        return false;

    bool changesMade = false;

    if (callback != NULL)
    {
        callback->RenumberInitialised(RenumberProgressCallback::EReindexRingers);
    }
    std::map<Duco::ObjectId, Duco::ObjectId> newRingerIds;
    {
        if (database.Settings().AlphabeticReordering())
        {
            GetNewObjectIdsByAlphabetic(newRingerIds, database.Settings().LastNameFirst());
        }
        else
        {
            StatisticFilters filters (database);
            std::map<Duco::ObjectId, Duco::PealLengthInfo> ringerPealCounts;
            database.PealsDatabase().GetRingersPealCount(filters, false, ringerPealCounts);
            GetNewIdsByPealCountAndAlphabetic(ringerPealCounts, newRingerIds, database.Settings().LastNameFirst());
        }
    }

    if (callback != NULL)
    {
        callback->RenumberInitialised(RenumberProgressCallback::ERenumberRingers);
    }
    // Renumber all towers first.
    changesMade |= RenumberObjects(newRingerIds, callback);
    changesMade |= RenumberLinkedRingers(newRingerIds);

    if (callback != NULL)
    {
        callback->RenumberInitialised(RenumberProgressCallback::ERebuildRingers);
    }
    // Renumber ringerids in Peals
    changesMade |= database.PealsDatabase().RenumberRingersInPeals(newRingerIds, callback);
    changesMade |= database.CompositionsDatabase().RenumberRingersInCompositions(newRingerIds, callback);

    if (database.Settings().DefaultRingerSet())
    {
        std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator newDefaultIt = newRingerIds.find(database.Settings().DefaultRinger());
        if (newDefaultIt != newRingerIds.end())
        {
            database.Settings().SetDefaultRinger(newDefaultIt->second);
            changesMade = true;
        }
    }

    newRingerIds.clear();
    return changesMade;
}

bool
RingerDatabase::RenumberLinkedRingers(const std::map<ObjectId, ObjectId>& newObjectIds)
{
    bool changed = false;
    for (auto& linkedSet : linkedRingerIds)
    {
        std::set<ObjectId> newLinkedSet;
        for (const auto& ringerId : linkedSet)
        {
            std::map<ObjectId, ObjectId>::const_iterator newIdIt = newObjectIds.find(ringerId);
            if (newIdIt != newObjectIds.end())
            {
                newLinkedSet.insert(newIdIt->second);
            }
        }
        if (linkedSet != newLinkedSet)
        {
            linkedSet = newLinkedSet;
            changed = true;
        }
    }
    return changed;
}

void
RingerDatabase::AddMissingRingers(std::map<Duco::ObjectId, Duco::PealLengthInfo>& ringerIds) const
{
    for (auto const& [key, value] : listOfObjects)
    {
        std::map<Duco::ObjectId, Duco::PealLengthInfo>::const_iterator alreadyAdded = ringerIds.find(key);
        if (alreadyAdded == ringerIds.end())
        {
            Duco::PealLengthInfo newInfo;
            pair<Duco::ObjectId, Duco::PealLengthInfo> newObject(key, newInfo);
            ringerIds.insert(newObject);
        }
    }
}

void
RingerDatabase::RemoveUsedIds(std::set<Duco::ObjectId>& unusedRingerIds) const
{
    std::vector<std::set<Duco::ObjectId> >::const_iterator it = linkedRingerIds.begin();
    while (it != linkedRingerIds.end())
    {
        std::set<Duco::ObjectId>::const_iterator it2 = it->begin();
        while (it2 != it->end())
        {
            if (unusedRingerIds.contains(*it2))
            {
                unusedRingerIds.erase(*it2);
            }
            ++it2;
        }
        ++it;
    }
}

bool
RingerDatabase::MatchObject(const Duco::RingingObject& object, const Duco::TSearchArgument& searchArg, const Duco::RingingDatabase& database) const
{
    if (object.ObjectType() != TObjectType::ERinger)
        return false;
    return searchArg.Match(static_cast<const Duco::Ringer&>(object), database);
}

bool 
RingerDatabase::ValidateObject(const Duco::RingingObject& object, const Duco::TSearchValidationArgument& searchArg, const std::set<Duco::ObjectId>& idsToCheckAgainst, const Duco::RingingDatabase& database) const
{
    if (object.ObjectType() != TObjectType::ERinger)
        return false;
    return searchArg.Match(static_cast<const Duco::Ringer&>(object), idsToCheckAgainst, database);
}

bool
RingerDatabase::SetRingerGender(const Duco::ObjectId& ringerId, bool male)
{
    DUCO_OBJECTS_CONTAINER::const_iterator it = listOfObjects.find(ringerId);
    if (it != listOfObjects.end())
    {
        Duco::Ringer* theRinger = static_cast<Duco::Ringer*>(it->second);
        if (theRinger != NULL)
        {
            if (male)
                theRinger->SetMale();
            else
                theRinger->SetFemale();
            SetDataChanged();
            return true;
        }
    }
    return false;
}

void
RingerDatabase::RingerGenderStats(float& percentMale, float& percentFemale, float& percentNotSet, float& percentNonHuman) const
{
    percentMale = 0;
    percentFemale = 0;
    percentNotSet = 0;
    percentNonHuman = 0;

    float male (0);
    float female (0);
    float notSet (0);
    float nonHuman(0);
    float total (0);

    for (auto const& [key, value] : listOfObjects)
    {
        Duco::Ringer* theRinger = static_cast<Duco::Ringer*>(value);
        if (theRinger != NULL)
        {
            ++total;
            if (theRinger->NonHuman())
                ++nonHuman;
            else if (theRinger->Male())
                ++male;
            else if (theRinger->Female())
                ++female;
            else
                ++notSet;
        }
    }

    if (male > 0)
        percentMale = (male / total) * float(100);
    if (female > 0)
        percentFemale = (female / total) * float(100);
    if (notSet > 0)
        percentNotSet = (notSet / total) * float(100);
    if (nonHuman > 0)
        percentNonHuman = (nonHuman / total) * float(100);
}

bool
RingerDatabase::FindRingerIdDuringDownload(const std::wstring& ringerName, Duco::ObjectId& ringerId) const
{
    ringerId.ClearId();
    std::wstring forenames;
    std::wstring surname;
    bool isConductor;
    DucoEngineUtils::SplitRingerName(ringerName, forenames, surname, isConductor);

    DUCO_OBJECTS_CONTAINER::const_iterator it = listOfObjects.begin();
    while (it != listOfObjects.end() && !ringerId.ValidId())
    {
        const Ringer* const theRinger = static_cast<const Ringer* const>(it->second);
        if (theRinger->MatchProbability(forenames, surname, false))
        {
            ringerId = it->first;
        }   

        ++it;
    }
    return ringerId.ValidId();
}

Duco::ObjectId
RingerDatabase::SuggestRinger(const std::wstring& fullNameToMatch, bool partialMatchAllowed, std::set<Duco::ObjectId>& nearMatches) const
{
    nearMatches.clear();
    if (fullNameToMatch.length() == 0)
    {
        return KNoId;
    }
    time_point<high_resolution_clock> start = high_resolution_clock::now();

    unsigned int highestProbability = 1;
    Duco::ObjectId foundRingerId;

    for (auto const& [key, value] : listOfObjects)
    {
        const Ringer* const theRinger = static_cast<const Ringer* const>(value);
        unsigned int lastProbability = theRinger->MatchProbability(fullNameToMatch, partialMatchAllowed);
        if (highestProbability < lastProbability)
        {
            nearMatches.clear();
            foundRingerId = theRinger->Id();
            highestProbability = lastProbability;
        }
        else if (highestProbability == lastProbability && highestProbability > 0)
        {
            nearMatches.insert(theRinger->Id());
            if (foundRingerId.ValidId())
            {
                nearMatches.insert(foundRingerId);
                foundRingerId.ClearId();
            }
        }
    }
    DUCOENGINEDEBUGLOG4(start, "SuggestRinger '%ls'; from %u objects", fullNameToMatch.c_str(), NumberOfObjects());
    if (foundRingerId.ValidId() && highestProbability < 4 && partialMatchAllowed)
    {
        nearMatches.insert(foundRingerId);
        foundRingerId = KNoId;
    }

    return foundRingerId;
}

void
RingerDatabase::SuggestAllRingers(const std::wstring& ringerFullname, std::set<Duco::ObjectId>& nearMatches) const
{
    nearMatches.clear();
    if (ringerFullname.length() < 2)
    {
        return;
    }
    time_point<high_resolution_clock> start = high_resolution_clock::now();
    std::wstring lowercaseName;

    DucoEngineUtils::ToLowerCase(DucoEngineUtils::Trim(ringerFullname), lowercaseName);

    for (const auto& [key, value] : listOfObjects)
    {
        const Ringer* const theRinger = static_cast<const Ringer* const>(value);
        if (theRinger->AnyMatch(lowercaseName))
        {
            nearMatches.insert(theRinger->Id());
        }
    }
    DUCOENGINEDEBUGLOG4(start, "SuggestRingers '%ls'; from %u objects", ringerFullname.c_str(), NumberOfObjects());
}

bool
RingerDatabase::CapitaliseField(Duco::ProgressCallback& progressCallback, size_t& numberOfObjectsUpdated, size_t numberOfObjectsToUpdate)
{
    bool changes = false;
    for (const auto& [key, value] : listOfObjects)
    {
        Ringer* const theRinger = static_cast<Ringer* const>(value);
        if (theRinger->CapitaliseField())
        {
            SetDataChanged();
            changes = true;
        }
        ++numberOfObjectsUpdated;
        progressCallback.Step(int(((float)numberOfObjectsUpdated / (float)numberOfObjectsToUpdate) * 100));
    }
    return changes;
}

Duco::ObjectId 
RingerDatabase::FindIdenticalRinger(const std::wstring& ringerFullname) const
{ // Doesnt even check case as this is used duing import.
    if (ringerFullname.length() == 0)
    {
        return KNoId;
    }
    time_point<high_resolution_clock> start = high_resolution_clock::now();

    unsigned int highestProbability = 1;
    std::wstring firstNames;
    std::wstring surnameName;
    bool ignore;
    DucoEngineUtils::SplitRingerName(ringerFullname, firstNames, surnameName, ignore);
    ObjectId foundRingerId;

    for (auto const& [key, value] : listOfObjects)
    {
        const Ringer* const theRinger = static_cast<const Ringer* const>(value);
        if (theRinger->IdenticalRinger(firstNames, surnameName))
        {
            foundRingerId = theRinger->Id();
            break;
        }
    }
    DUCOENGINEDEBUGLOG4(start, "FindIdenticalRinger '%ls'; from %u objects", ringerFullname.c_str(), NumberOfObjects());

    return foundRingerId;
}

bool
RingerDatabase::RingerAkaRequired(const Duco::ObjectId& ringerId, const Duco::PerformanceDate& pealDate, size_t& akaNo) const
{
    const Ringer* theRinger = FindRinger(ringerId, false);
    if (theRinger != NULL)
    {
        return theRinger->AkaRequired(pealDate, akaNo);
    }
    return false;
}

std::wstring
RingerDatabase::RingerFullName(const Duco::ObjectId& ringerId, const Duco::DatabaseSettings& settings) const
{
    std::wstring fullringerName;
    const Ringer* const theRinger = FindRinger(ringerId);
    if (theRinger != NULL)
    {
        fullringerName = theRinger->FullName(settings.LastNameFirst());
    }
    return fullringerName;
}

std::wstring
RingerDatabase::RingerFullName(const Duco::ObjectId& ringerId, const Duco::PerformanceDate& pealDate, const Duco::DatabaseSettings& settings) const
{
    const Ringer* theRinger = FindRinger(ringerId);
    if (theRinger != NULL)
    {
        return theRinger->FullName(pealDate, settings.LastNameFirst());
    }
    return L"";
}

bool
RingerDatabase::UpdateObjectField(const Duco::ObjectId&, const Duco::TUpdateArgument&)
{
    return false;
}

bool
RingerDatabase::LinkedRingers(const Duco::ObjectId& ringerId, std::set<Duco::ObjectId>& links, bool insertSelfIfNotFound) const
{
    links.clear();
    for (auto it : linkedRingerIds)
    {
        if (it.find(ringerId) != it.end())
        {
            links.insert(it.begin(), it.end());
            return true;
        }
    }
    if (insertSelfIfNotFound)
    {
        links.insert(ringerId);
    }
    return false;
}

bool
RingerDatabase::RingersLinked(const Duco::ObjectId& ringerId, const Duco::ObjectId& ringerId2) const
{
    for (auto it : linkedRingerIds)
    {
        if (it.find(ringerId) != it.end() && it.find(ringerId2) != it.end())
        {
            return true;
        }
    }
    return false;
}

bool
RingerDatabase::LinkRingers(const Duco::ObjectId& ringerId, const Duco::ObjectId& ringerId2)
{
    if (ringerId == ringerId2)
    {
        return false;
    }
    if (!listOfObjects.contains(ringerId) || !listOfObjects.contains(ringerId2))
    {
        return false;
    }
    std::vector<std::set<Duco::ObjectId> >::iterator it = linkedRingerIds.begin();
    std::vector<std::set<Duco::ObjectId> >::iterator addedTo = linkedRingerIds.end();

    while (it != linkedRingerIds.end())
    {
        if (addedTo == linkedRingerIds.end())
        {
            if (it->contains(ringerId))
            {
                it->insert(ringerId2);
                dataChanged = true;
                addedTo = it;
            }
            else if (it->contains(ringerId2))
            {
                it->insert(ringerId);
                dataChanged = true;
                addedTo = it;
            }
        }
        else
        {
            if (it->contains(ringerId) || it->contains(ringerId2))
            {
                addedTo->insert(it->begin(), it->end());
                linkedRingerIds.erase(it);
                dataChanged = true;
                return true;
            }
        }
        ++it;
    }
    dataChanged = true;
    linkedRingerIds.push_back(std::set<Duco::ObjectId>{ringerId, ringerId2});
    return true;
}

bool
RingerDatabase::LinkedRinger(const Duco::ObjectId& ringerIdToFind) const
{
    for (const auto& ringerGroup : linkedRingerIds)
    {
        for (const auto& ringerId : ringerGroup)
        {
            if (ringerId == ringerIdToFind)
                return true;
        }
    }
    return false;
}

bool
RingerDatabase::RemoveLinkedRinger(Duco::ObjectId ringerId)
{
    bool modified = false;
    for (auto& it : linkedRingerIds)
    {
        std::set<Duco::ObjectId>::iterator foundRinger = it.find(ringerId);
        if (foundRinger != it.end())
        {
            it.erase(ringerId);
            modified = true;
        }
    }
    if (modified)
    {
        dataChanged = true;
        std::vector<std::set<Duco::ObjectId> >::iterator it2 = linkedRingerIds.begin();
        while (it2 != linkedRingerIds.end())
        {
            if (it2->size() <= 1)
            {
                linkedRingerIds.erase(it2);
                it2 = linkedRingerIds.begin();
            }
            else
            {
                ++it2;
            }
        }
    }
    return modified;
}

void
RingerDatabase::CombineLinkedRingers(std::map<ObjectId, Duco::PealLengthInfo>& sortedRingerIds) const
{
    for (const auto& linkedRingerSet : linkedRingerIds)
    {
        Duco::ObjectId largestId;
        size_t mostPeals = 0;

        PealLengthInfo newInfo;
        for (const auto& ringerId : linkedRingerSet)
        {
            if (mostPeals == 0 || sortedRingerIds[ringerId].TotalPeals() > mostPeals)
            {
                mostPeals = sortedRingerIds[ringerId].TotalPeals();
                largestId = ringerId;
            }
            newInfo += sortedRingerIds[ringerId];
            sortedRingerIds.erase(ringerId);
        }
        if (largestId.ValidId())
        {
            sortedRingerIds[largestId] = newInfo;
        }
    }
}

void
RingerDatabase::CombineLinkedRingers(std::map<ObjectId, std::map<unsigned int, Duco::PealLengthInfo> >& sortedPealCounts) const
{
    std::set<Duco::ObjectId> leadingRingers;
    // First need to find the ringer in each group who hans rung most peals - not per year - totals.
    for (const auto& linkedRingerSet : linkedRingerIds)
    {
        Duco::ObjectId largestIdInGroup;
        size_t mostPeals = 0;

        for (const auto& ringerId : linkedRingerSet)
        {
            std::map<ObjectId, std::map<unsigned int, Duco::PealLengthInfo> >::const_iterator firstRinger = sortedPealCounts.find(ringerId);
            if (firstRinger != sortedPealCounts.end())
            {
                size_t totalPealsForThisRinger = 0;
                for (const auto& [key, value] : firstRinger->second)
                {
                    totalPealsForThisRinger += value.TotalPeals();
                }
                if (mostPeals == 0 || totalPealsForThisRinger > mostPeals)
                {
                    mostPeals = totalPealsForThisRinger;
                    largestIdInGroup = ringerId;
                }
            }
        }
        if (largestIdInGroup.ValidId())
        {
            leadingRingers.insert(largestIdInGroup);
        }
    }

    // Now combine the peal counts for each group.
    for (const auto& linkedRingerSet : linkedRingerIds)
    {
        std::set <Duco::ObjectId>::const_iterator firstRinger = linkedRingerSet.begin();
        bool removeIfNotAdded = true;
        if (sortedPealCounts.contains(*firstRinger))
        {
            removeIfNotAdded = false;
        }
        std::map<unsigned int, Duco::PealLengthInfo> newYearlyInfo = sortedPealCounts[*firstRinger];

        Duco::ObjectId largestId;

        std::set <Duco::ObjectId>::const_iterator nextRingers = ++linkedRingerSet.begin();
        while (nextRingers != linkedRingerSet.end())
        {
            for (const auto& [key, value] : sortedPealCounts[*nextRingers])
            {
                newYearlyInfo[key] += value;
            }
            sortedPealCounts.erase(*nextRingers);
            if (leadingRingers.contains(*nextRingers))
            {
                largestId = *nextRingers;
            }
            ++nextRingers;
        }
        if (largestId.ValidId())
        {
            sortedPealCounts[largestId] = newYearlyInfo;
            sortedPealCounts.erase(*firstRinger);
        }
        else if (newYearlyInfo.size() > 0)
        {
            sortedPealCounts[*firstRinger] = newYearlyInfo;
        }
        else if (removeIfNotAdded)
        {
            sortedPealCounts.erase(*firstRinger);
        }
    }
}
