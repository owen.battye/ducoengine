#include "MethodDatabase.h"

#include "CompositionDatabase.h"
#include "DatabaseWriter.h"
#include "DatabaseSettings.h"
#include "DucoEngineUtils.h"
#include "MethodSeriesDatabase.h"
#include "Method.h"
#include "ImportExportProgressCallback.h"
#include "PealDatabase.h"
#include "RenumberProgressCallback.h"
#include "RingingDatabase.h"
#include "SearchArgument.h"
#include "SearchValidationArgument.h"
#include "SettingsFile.h"
#include "StatisticFilters.h"
#include "UpdateArgument.h"
#include <algorithm>
#include "ProgressCallback.h"
#include <xercesc\dom\DOMElement.hpp>
#include <chrono>
#include "DucoEngineLog.h"

using namespace std::chrono;
using namespace Duco;
using namespace std;
XERCES_CPP_NAMESPACE_USE

#define KMethodTypeAbbreviationsFileName "methodabbreviations.config"

MethodDatabase::MethodDatabase()
:   RingingObjectDatabase(TObjectType::EMethod)
{
}

MethodDatabase::~MethodDatabase()
{
    bellNames.clear();
}

void
MethodDatabase::ClearObjects(bool createDefaultBellNames)
{
    RingingObjectDatabase::ClearObjects(createDefaultBellNames);
    bellNames.clear();
    if (createDefaultBellNames)
        CreateDefaultBellNames();
}


Duco::ObjectId
MethodDatabase::AddObject(const Duco::RingingObject& newObject)
{
    Duco::ObjectId foundId;
    if (newObject.ObjectType() == TObjectType::EMethod)
    {
        const Duco::Method& newMethod = static_cast<const Duco::Method&>(newObject);

        Duco::RingingObject* newMethodWithId = new Duco::Method(newMethod);
        if (AddOwnedObject(newMethodWithId))
        {
            foundId = newMethodWithId->Id();
        }
        // object is deleted by RingingObjectDatabase::AddObject if not added.
    }
    return foundId;
}

bool
MethodDatabase::SaveUpdatedObject(Duco::RingingObject& lhs, const Duco::RingingObject& rhs)
{
    if (lhs.ObjectType() == rhs.ObjectType() && rhs.ObjectType() == TObjectType::EMethod)
    {
        Duco::Method& lhsMethod = static_cast<Duco::Method&>(lhs);
        const Duco::Method& rhsMethod = static_cast<const Duco::Method&>(rhs);
        lhsMethod = rhsMethod;
        return true;
    }
    return false;
}

void
MethodDatabase::ExternaliseToXml(Duco::ImportExportProgressCallback* newCallback, XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, bool exportDucoObjects) const
{
    size_t count = 0;
    DOMElement* rootElem = outputFile.getDocumentElement();
    DOMElement* newMethod = outputFile.createElement(XMLStrL("Methods"));
    rootElem->appendChild(newMethod);

    for (auto const& [key, value] : listOfObjects)
    {
        const Duco::Method* theMethod = static_cast<const Duco::Method*>(value);
        theMethod->ExportToXml(outputFile, *newMethod, exportDucoObjects);

        newCallback->ObjectProcessed(false, TObjectType::EMethod, value->Id(), 0, ++count);
    }
}

bool
MethodDatabase::Internalise(DatabaseReader& reader, Duco::ImportExportProgressCallback* callback, int databaseVersionNumber, size_t noOfObjectsInThisDatabase, size_t totalObjects, size_t objectsProcessedSoFar)
{
    size_t totalProcessed = objectsProcessedSoFar;
    size_t count = 0;

    while (noOfObjectsInThisDatabase-- > 0)
    {
        ++totalProcessed;
        ++count;
        float percentageComplete = ((float)totalProcessed / (float)totalObjects) * 100;
        Duco::RingingObject* newMethod = new Duco::Method(reader, databaseVersionNumber);
        if (newMethod == NULL)
            return false;
        AddOwnedObject(newMethod);
        if (callback != NULL)
        {
            callback->ObjectProcessed(true, TObjectType::EMethod, newMethod->Id(), (int)percentageComplete, count);
        }
    }
    return true;
}

const Method* const
MethodDatabase::FindMethod(const Duco::ObjectId& methodId, bool setIndex) const
{
    const RingingObject* const object = FindObject(methodId, setIndex);

    return static_cast<const Method* const>(object);
}

const Method* const
MethodDatabase::FindMethod(unsigned int noOfBells, const std::wstring& placeNotation) const
{
    for (auto const& [key, value] : listOfObjects)
    {
        Method* theMethod = static_cast<Method*>(value);
        if (theMethod->Order() == noOfBells && DucoEngineUtils::CompareString(theMethod->PlaceNotation(), placeNotation))
        {
            return theMethod;
        }
    }
    return NULL;
}

Duco::ObjectId
MethodDatabase::FindMethod(const std::wstring& name, const std::wstring& type, unsigned int order) const
{
    for (auto const& [key, value] : listOfObjects)
    {
        Method* theMethod = static_cast<Method*>(value);
        if (theMethod->Match(name, type, order))
        {
            return theMethod->Id();
        }
    }
    return ObjectId();
}

Duco::ObjectId
MethodDatabase::AddMethod(const std::wstring& methodName, const std::wstring& methodType, unsigned int noOfBells, bool dualOrderMethod)
{
    Duco::RingingObject* newObject = new Method(KNoId, noOfBells, methodName, methodType, L"", dualOrderMethod);
    if (AddOwnedObject(newObject))
    {
        return newObject->Id();
    }
    return KNoId;
}

Duco::ObjectId
MethodDatabase::AddMethod(const std::wstring& fullMethodName)
{
    Duco::ObjectId foundId;
    unsigned int noOfBells = 0;
    bool dualOrderMethod = false;
    wstring processedMethodName;
    if (RemoveOrderNameFromMethodName(fullMethodName, processedMethodName, noOfBells, dualOrderMethod))
    {
        size_t seperatorPos = processedMethodName.rfind(L" ");

        wstring methodName = processedMethodName.substr(0, seperatorPos);
        wstring methodType = L"";
        if ( seperatorPos != wstring::npos && (seperatorPos+1) < processedMethodName.length())
            methodType = processedMethodName.substr(seperatorPos+1);

        foundId = AddMethod(methodName, methodType, noOfBells, dualOrderMethod);
    }
    return foundId;
}

bool
MethodDatabase::MethodNameMatchesOrderInName(const std::wstring& fullMethodName, unsigned int noOfBellsExpected) const
{
    wstring processedMethodName;
    bool dualOrderMethod (false);
    unsigned int foundNoOfBells = 0;
    if (RemoveOrderNameFromMethodName(fullMethodName, processedMethodName, foundNoOfBells, dualOrderMethod))
    {
        if (foundNoOfBells == noOfBellsExpected)
        {
            return true;
        }
        if (foundNoOfBells % 2 == 1 && (noOfBellsExpected-1) == foundNoOfBells)
        {
            return true;
        }
    }
    return false;
}

Duco::ObjectId
MethodDatabase::SuggestMethod(const std::wstring& originalName, const std::wstring& type, unsigned int order, unsigned int requiredProbability) const
{
    time_point<high_resolution_clock> start = high_resolution_clock::now();
    std::wstring name = originalName;
    std::wstring methodCount = L"";
    DucoEngineUtils::RemoveMethodCount(name, methodCount);
    unsigned int highestProbability = 0;
    Duco::ObjectId methodId;
    for (auto const &[key, value] : listOfObjects)
    {
        Method* theMethod = static_cast<Method*>(value);
        unsigned int probability = theMethod->MatchProbability(name, methodCount, type, order, false);
        if (probability > highestProbability)
        {
            methodId = theMethod->Id();
            highestProbability = probability;
        }
        else if (highestProbability == probability)
        {
            methodId.ClearId();
        }
    }
    if (highestProbability < requiredProbability)
    {
        methodId.ClearId();
    }
    DUCOENGINEDEBUGLOG4(start, "SuggestMethod '%ls'; from %u objects", originalName, NumberOfObjects());
    return methodId;
}


Duco::ObjectId
MethodDatabase::SuggestMethod(const std::wstring& methodName, unsigned int requiredProbability) const
{
    std::wstring processedMethodName;
    unsigned int orderNumber = 0;
    bool dualOrderMethod(false);

    Duco::ObjectId suggestedMethod;
    // change a peal to owen surprise major, in the manc database.... this goes wrong.
    if (RemoveOrderNameFromMethodName(methodName, processedMethodName, orderNumber, dualOrderMethod))
    {
        std::wstring methodType;
        std::wstring methodNameOnly;
        std::wstring::size_type seperatorPos = std::wstring::npos;


        bool tryAgain = true;
        while (tryAgain)
        {
            seperatorPos = processedMethodName.rfind(' ', seperatorPos - 1);
            if (seperatorPos == std::wstring::npos)
            {
                methodNameOnly = processedMethodName;
                methodType.clear();
            }
            else
            {
                methodType = processedMethodName.substr(seperatorPos);
                methodNameOnly = processedMethodName.substr(0, processedMethodName.length() - methodType.length());
            }
            methodType = DucoEngineUtils::Trim(methodType);
            methodNameOnly = DucoEngineUtils::Trim(methodNameOnly);

            suggestedMethod = SuggestMethod(methodNameOnly, methodType, orderNumber, requiredProbability);
            if (suggestedMethod.ValidId() || processedMethodName.rfind(' ') == std::wstring::npos || seperatorPos == std::wstring::npos)
            {
                tryAgain = false;
            }
        }
    }

    return suggestedMethod;
}

Duco::ObjectId
MethodDatabase::FindOrCreateMethod(const std::wstring& newMethodName, const std::wstring& newMethodType, const std::wstring& newMethodOrder, unsigned int& order)
{
    order = -1;
    bool dualOrder(false);
    if (FindOrderNumber(newMethodOrder, order, dualOrder))
    {
        Duco::ObjectId existingMethodId = FindMethod(newMethodName, newMethodType, order);
        if (existingMethodId.ValidId())
        {
            return existingMethodId;
        }
        return AddMethod(newMethodName, newMethodType, order, dualOrder);
    }
    return ObjectId();
}

Duco::ObjectId
MethodDatabase::FindOrCreateMethod(const std::wstring& newMethodStr, unsigned int& order)
{
    std::wstring bracketedText;
    std::wstring fullMethodTitle = newMethodStr;
    bool removedBracketed = DucoEngineUtils::RemoveMethodCount(fullMethodTitle, bracketedText);
    std::wstring methodName;
    std::wstring methodType;
    std::wstring orderName;
    order = -1;
    bool dualOrder(false);
    if (FindOrderNumber(fullMethodTitle, order, dualOrder) && FindOrderName(order, orderName))
    {
        size_t orderNameIndex = fullMethodTitle.find(orderName);
        if (orderNameIndex != string::npos)
        {
            std::wstring tempMethodStr = fullMethodTitle.substr(0, orderNameIndex);
            if (orderNameIndex + orderName.length() < fullMethodTitle.length())
            {
                tempMethodStr += fullMethodTitle.substr(orderNameIndex + orderName.length() + 1);
            }
            fullMethodTitle = DucoEngineUtils::Trim(tempMethodStr);
        }
        if (removedBracketed)
        {
            fullMethodTitle = L"(" + bracketedText + L") " + fullMethodTitle;
        }
        size_t lastSpaceIndex = fullMethodTitle.find_last_of(L" ");
        if (lastSpaceIndex > 0 && lastSpaceIndex != string::npos)
        {
            methodName = fullMethodTitle.substr(0, lastSpaceIndex);
            methodName = DucoEngineUtils::Trim(methodName);
            methodType = fullMethodTitle.substr(lastSpaceIndex);
            methodType = DucoEngineUtils::Trim(methodType);
        }
        else
        {
            methodName = DucoEngineUtils::Trim(fullMethodTitle);
        }
    }

    Duco::ObjectId existingMethodId = FindMethod(methodName, methodType, order);
    if (existingMethodId.ValidId())
    {
        return existingMethodId;
    }
    return AddMethod(methodName, methodType, order, dualOrder);
}

bool
MethodDatabase::RemoveOrderNameFromMethodName(const std::wstring& fullMethodNameBeforeMethodCountCheck, std::wstring& processedMethodName, unsigned int& possibleOrderNumber, bool& dualOrderMethod) const
{
    bool orderNameFound(false);
    std::wstring orderName;
    possibleOrderNumber = 0;
    dualOrderMethod = false;

    size_t index = fullMethodNameBeforeMethodCountCheck.find_last_of(L" ");
    std::wstring potentialOrderName = fullMethodNameBeforeMethodCountCheck.substr(index+1);
    std::wstring fullMethodName = fullMethodNameBeforeMethodCountCheck;

    if (potentialOrderName.length() > 3 && potentialOrderName[0] == '(' && *potentialOrderName.rbegin() == ')')
    {
        fullMethodName = potentialOrderName.append(L" " + fullMethodNameBeforeMethodCountCheck.substr(0, index));
        size_t secondIndex = fullMethodNameBeforeMethodCountCheck.find_last_of(L" ", index-1);
        potentialOrderName = fullMethodNameBeforeMethodCountCheck.substr(secondIndex+1, index - secondIndex - 1);
    }

    if (FindOrderName(potentialOrderName, possibleOrderNumber))
    {
        orderNameFound = true;
        if (index != string::npos)
            processedMethodName = fullMethodName.substr(0, index);
        else
            processedMethodName.clear();
    }

    std::wstring lowerOrderName;
    if (FindOrderName(possibleOrderNumber - 1, lowerOrderName))
    {
        index = processedMethodName.find(lowerOrderName);
        if (index != string::npos)
        {
            dualOrderMethod = true;
            processedMethodName = fullMethodName.substr(0, index);
        }
    }

    if (orderNameFound)
    {
        processedMethodName = DucoEngineUtils::Trim(processedMethodName);
    }
    return orderNameFound;
}

bool
MethodDatabase::SetNumberOfLeads(const Duco::ObjectId& methodId, unsigned int leadNumber)
{
    DUCO_OBJECTS_CONTAINER::iterator it = listOfObjects.find(methodId);
    if (it != listOfObjects.end())
    {
        Method* theMethod = static_cast<Method*>(it->second);
        if (theMethod->NoOfLeads() != leadNumber)
        {
            theMethod->SetNoOfLeads(leadNumber);
            SetDataChanged();
        }
        return true;
    }
    return false;
}

bool
MethodDatabase::SetStartingLead(const Duco::ObjectId& methodId, unsigned int startingLead)
{
    DUCO_OBJECTS_CONTAINER::iterator it = listOfObjects.find(methodId);
    if (it != listOfObjects.end())
    {
        Method* theMethod = static_cast<Method*>(it->second);
        if (theMethod->PrintFromBell() != startingLead)
        {
            theMethod->SetPrintFromBell(startingLead);
            theMethod->SetNoOfLeads(-1);
            SetDataChanged();
        }
        return true;
    }
    return false;
}

void
MethodDatabase::GetMethodsWithoutPlaceNotation(std::map<Duco::ObjectId, std::wstring>& methods) const
{
    methods.clear();
    for (auto const& [key, value] : listOfObjects)
    {
        Method* theMethod = static_cast<Method*>(value);
        if (theMethod->PlaceNotation().length() == 0)
        {
            std::pair<Duco::ObjectId, std::wstring> newObject (key, theMethod->FullName(*this));
            methods.insert(newObject);
        }
    }
}

void
MethodDatabase::GetAllMethodsByNumberOfBells(std::set<Duco::ObjectId>& methodIds, unsigned int noOfBells, bool anyStage, bool excludeSpliced) const
{
    methodIds.clear();
    if (noOfBells <= 0 || noOfBells == -1)
    {
        anyStage = true;
    }

    for (auto const& [key, value] : listOfObjects)
    {
        Method* theMethod = static_cast<Method*>(value);
        if ((!excludeSpliced || !theMethod->Spliced()) && 
            (anyStage || DucoEngineUtils::NoOfBellsMatch(theMethod->Order(), noOfBells) ) )
        {
            methodIds.insert(key);
        }
    }
}

bool
MethodDatabase::UpdateNotation(const Duco::ObjectId& methodId, const std::wstring& newFullNotation)
{
    bool notationUpdated = false;
    DUCO_OBJECTS_CONTAINER::iterator it = listOfObjects.find(methodId);
    if (it != listOfObjects.end())
    {
        Method* theMethod = static_cast<Method*>(it->second);
        if (!DucoEngineUtils::CompareString(theMethod->PlaceNotation(),newFullNotation))
        {
            theMethod->SetPlaceNotation(newFullNotation);
            SetDataChanged();
            notationUpdated = true;
        }
        ++it;
    }
    return notationUpdated;
}

void
MethodDatabase::GetAllMethodNames(std::set<std::wstring>& names) const
{
    names.clear();
    for (auto const& [key, value] : listOfObjects)
    {
        Method* nextMethod = static_cast<Method*>(value);
        DucoEngineUtils::InsertString(names, nextMethod->Name());
    }
}

void
MethodDatabase::GetAllMethodTypes(std::set<std::wstring>& types) const
{
    types.clear();
    for (auto const& [key, value] : listOfObjects)
    {
        Method* nextMethod = static_cast<Method*>(value);
        DucoEngineUtils::InsertString(types, nextMethod->Type());
    }
}

void
MethodDatabase::GetAllMethodOrders(std::set<unsigned int>& methodOrders) const
{
    methodOrders.clear();
    for (auto const& [key, value] : listOfObjects)
    {
        Method* theMethod = static_cast<Method*>(value);
        unsigned int methodOrder = theMethod->Order();
        methodOrders.insert(methodOrder);
    }
}

void
MethodDatabase::GetAllPlaceNotations(std::set<std::wstring>& placeNotations) const
{
    placeNotations.clear();
    for (auto const& [key, value] : listOfObjects)
    {
        Method* nextMethod = static_cast<Method*>(value);
        DucoEngineUtils::InsertString(placeNotations, nextMethod->PlaceNotation());
    }
}

void
MethodDatabase::AddMissingMethods(std::map<Duco::ObjectId, Duco::PealLengthInfo>& sortedMethodIds, unsigned int noOfBells) const
{
    for (auto const& [key, value] : listOfObjects)
    {
        Method* nextMethod = static_cast<Method*>(value);

        if (noOfBells == -1 || nextMethod->Order() == noOfBells)
        {
            std::map<Duco::ObjectId, Duco::PealLengthInfo>::const_iterator alreadyAdded = sortedMethodIds.find(key);
            if (alreadyAdded == sortedMethodIds.end())
            {
                Duco::PealLengthInfo newEmptyData;
                pair<Duco::ObjectId, Duco::PealLengthInfo> newObject(key, newEmptyData);
                sortedMethodIds.insert(newObject);
            }
        }
    }
}

bool
MethodDatabase::MatchObject(const Duco::RingingObject& object, const Duco::TSearchArgument& searchArg, const Duco::RingingDatabase& database) const
{
    if (object.ObjectType() != TObjectType::EMethod)
        return false;
    return searchArg.Match(static_cast<const Duco::Method&>(object), database);
}

bool
MethodDatabase::ValidateObject(const Duco::RingingObject& object, const Duco::TSearchValidationArgument& searchArg, const std::set<Duco::ObjectId>& idsToCheckAgainst, const Duco::RingingDatabase& database) const
{
    if (object.ObjectType() != TObjectType::EMethod)
        return false;
    return searchArg.Match(static_cast<const Duco::Method&>(object), idsToCheckAgainst, database);
}

bool
MethodDatabase::SuggestMethodBySeriesString(const std::wstring& methodName, unsigned int noOfBells, std::set<Duco::ObjectId>& objectIds, Duco::ObjectId& mostLikelyMethodId) const
{
    std::wstring processedMethodName (methodName);
    objectIds.clear();
    mostLikelyMethodId.ClearId();
    ObjectId secondLikelyMethodId;

    Duco::ObjectId suggestedMethod;
    for (auto const& [key, value] : listOfObjects)
    {
        bool methodMatched (false);
        while (!methodMatched && processedMethodName.length() > 0)
        {
            Method* theMethod = static_cast<Method*>(value);
            if (theMethod->Order() == noOfBells)
            {
                size_t index = theMethod->FullName(*this).find(processedMethodName);
                if (index != string::npos)
                {
                    if (!mostLikelyMethodId.ValidId() && index == 0)
                        mostLikelyMethodId = key;
                    else if (!secondLikelyMethodId.ValidId())
                        secondLikelyMethodId = key;
                    objectIds.insert(key);
                    methodMatched = true;
                }
            }
            if (!methodMatched)
            {
                size_t index = processedMethodName.find_last_of(L" ");
                if (index != string::npos)
                    processedMethodName = processedMethodName.substr(0, index-1);
                if (index == string::npos || processedMethodName.length() <= 3)
                    processedMethodName.clear();
            }
        }
        processedMethodName = methodName;
    }
    if (!mostLikelyMethodId.ValidId() && objectIds.size() > 0)
    {
        mostLikelyMethodId = secondLikelyMethodId;
    }

    return objectIds.size() > 0;
}
bool
MethodDatabase::ReplaceMethodTypes(Duco::ProgressCallback*const callback, const std::string& installDir)
{
    SettingsFile* settings = new SettingsFile(installDir, KMethodTypeAbbreviationsFileName);
    if (settings->NoOfSettings() <= 0)
    {
        CreateDefaultMethodAbbreviationReplacements(*settings);
    }

    size_t totalObjects = NumberOfObjects();
    size_t stepNumber = 0;
    bool updated (false);
    if (settings->NoOfSettings() > 0)
    {
        for (auto const& [key, value] : listOfObjects)
        {
            Method* theMethod = static_cast<Method*>(value);
            std::wstring replaceValue;
            if (settings->Setting(theMethod->Type(), replaceValue))
            {
                updated = true;
                theMethod->SetType(replaceValue);
                SetDataChanged();
            }
            ++stepNumber;
            if (callback != NULL)
            {
                callback->Step(int(stepNumber / totalObjects));
            }
        }
    }
    delete settings;

    return updated;
}

bool
MethodDatabase::CapitaliseField(Duco::ProgressCallback& progressCallback, bool methodName, bool methodType, size_t& numberOfObjectsUpdated, size_t numberOfObjectsToUpdate)
{
    bool changes = false;
    for (auto const& [key, value] : listOfObjects)
    {
        Method* const theMethod = static_cast<Method* const>(value);
        if (theMethod->CapitaliseField(methodName, methodType))
        {
            SetDataChanged();
            changes = true;
        }
        ++numberOfObjectsUpdated;
        progressCallback.Step(int(((float)numberOfObjectsUpdated / (float)numberOfObjectsToUpdate) * 100));
    }
    return changes;
}


void
MethodDatabase::CreateDefaultMethodAbbreviationReplacements(Duco::SettingsFile& settings)
{
    settings.Set(L"D",  L"Delight");
    settings.Set(L"S",  L"Surprise");
    settings.Set(L"TB", L"Treble Bob");
    settings.Set(L"A",  L"Alliance");
    settings.Set(L"CB", L"Court Bob");
    settings.Set(L"P",  L"Plain");
    settings.Set(L"TP", L"Treble Place");
    settings.Set(L"TD", L"Treble Dodging");
    settings.Set(L"Little A",  L"Little Alliance");
    settings.Set(L"Little B",  L"Little Bob");
    settings.Set(L"Little D",  L"Little Delight");
    settings.Set(L"Little TB", L"Little Treble Bob");
    settings.Set(L"Little S",  L"Little Surprise");
    settings.Set(L"Little TP", L"Little Treble Place");
}

//***********************************************************************
// Sorting / reordering Methods
//***********************************************************************
bool
MethodDatabase::RebuildObjects(Duco::RenumberProgressCallback* callback, Duco::RingingDatabase& database)
{
    if (listOfObjects.size() <= 0)
        return false;

    Duco::StatisticFilters filters (database);
    callback->RenumberInitialised(RenumberProgressCallback::EReindexMethods);
    std::map<Duco::ObjectId, Duco::ObjectId> newMethodIds;
    {
        if (database.Settings().AlphabeticReordering())
            GetNewObjectIdsByAlphabetic(newMethodIds, database.Settings().LastNameFirst());
        else
        {
            std::map<Duco::ObjectId, Duco::PealLengthInfo> methodPealCounts;
            database.PealsDatabase().GetMethodsPealCount(filters, methodPealCounts);
            GetNewIdsByPealCountAndAlphabetic(methodPealCounts, newMethodIds, database.Settings().LastNameFirst());
        }
    }

    // Renumber all methods first.
    callback->RenumberInitialised(RenumberProgressCallback::ERenumberMethods);
    bool changesMade = RenumberObjects(newMethodIds, callback);

    // Renumber methodids in Peals
    callback->RenumberInitialised(RenumberProgressCallback::ERebuildMethods);
    changesMade |= database.PealsDatabase().RenumberMethodsInPeals(newMethodIds, callback);
    changesMade |= database.CompositionsDatabase().RenumberMethodsInCompositions(newMethodIds, callback);
    // Renumber methodids in MethodSeries
    callback->RenumberInitialised(RenumberProgressCallback::ERebuildMethodSeries);
    changesMade |= database.MethodSeriesDatabase().RenumberMethodsInMethodSeries(newMethodIds, callback);
    return changesMade;
}

//****************************************************************************************************
// Bell names
//****************************************************************************************************

bool
MethodDatabase::SplitOrderName(const std::wstring& orderName, unsigned int& orderNumber) const
{
    unsigned int orderNumber1 (-1);
    unsigned int orderNumber2 (-1);
    std::map<unsigned int, std::wstring>::const_iterator it = bellNames.begin();
    while ((orderNumber1 == -1 || orderNumber2 == -1) && it != bellNames.end())
    {
        const std::wstring& stringToFind = it->second;
        if (DucoEngineUtils::FindSubstring(stringToFind, orderName))
        {
            if (orderNumber1 == -1)
            {
                orderNumber1 = it->first;
            }
            else
            {
                orderNumber2 = it->first;
            }
        }
        ++it;
    }
    if (orderNumber1 != -1 && orderNumber2 != -1)
    {
        orderNumber = max(orderNumber1, orderNumber2);
        return true;
    }
    return false;
}

void
MethodDatabase::CreateDefaultBellNames()
{
    AddOrderName(3, L"Singles");
    AddOrderName(4, L"Minimus");
    AddOrderName(5, L"Doubles");
    AddOrderName(6, L"Minor");
    AddOrderName(7, L"Triples");
    AddOrderName(8, L"Major");
    AddOrderName(9, L"Caters");
    AddOrderName(10, L"Royal");
    AddOrderName(11, L"Cinques");
    AddOrderName(12, L"Maximus");
    AddOrderName(13, L"Sextuples");
    AddOrderName(14, L"Fourteen");
    AddOrderName(15, L"Septuples");
    AddOrderName(16, L"Sixteen");
    AddOrderName(17, L"Octuples");
    AddOrderName(18, L"Eighteen");
    AddOrderName(19, L"Nontuples");
    AddOrderName(20, L"Twenty");
}

size_t
MethodDatabase::NoOfOrderNames() const
{
    return bellNames.size();
}

bool
MethodDatabase::AddOrderName(unsigned int order, const std::wstring& newName)
{
    unsigned int existingOrder (-1);
    bool dualOrder (false);
    if (!FindOrderNumber(newName, existingOrder, dualOrder))
    {
        pair<unsigned int, std::wstring> newObject(order, newName);
        if (bellNames.insert(newObject).second)
        {
            SetDataChanged();
            return true;
        }
    }
    return false;
}

bool
MethodDatabase::RemoveOrderName(unsigned int order)
{
    std::map<unsigned int, std::wstring>::iterator it = bellNames.find(order);
    if (it != bellNames.end())
    {
        bellNames.erase(it);
        SetDataChanged();
        return true;
    }
    return false;
}

bool
MethodDatabase::UpdateOrderName(unsigned int order, const std::wstring& newName)
{
    std::map<unsigned int, std::wstring>::iterator it = bellNames.find(order);
    if (it != bellNames.end())
    {
        it->second = newName;
        SetDataChanged();
        return true;
    }
    return false;
}

bool
MethodDatabase::ContainsOrderName(unsigned int order) const
{
    std::wstring newName;
    return FindOrderName(order, newName);
}

bool
MethodDatabase::FindOrderName(unsigned int order, std::wstring& foundOrderName) const
{
    std::map<unsigned int, std::wstring>::const_iterator it = bellNames.find(order);
    if (it == bellNames.end())
    {
        return false;
    }
    foundOrderName = it->second;

    return true;
}

bool
MethodDatabase::FindOrderName(const std::wstring& orderName, unsigned int& foundOrder) const
{
    foundOrder = 0;
    std::map<unsigned int, std::wstring>::const_iterator it = bellNames.begin();
    while (it != bellNames.end())
    {
        if (DucoEngineUtils::CompareString(DucoEngineUtils::Trim(orderName), it->second))
        {
            foundOrder = it->first;
            return true;
        }
        ++it;
    }

    return false;
}

unsigned int
MethodDatabase::HighestValidOrderNumber() const
{
    unsigned int highestOrderName (5);
    std::map<unsigned int, std::wstring>::const_iterator it = bellNames.begin();
    while (it != bellNames.end())
    {
        highestOrderName = max(highestOrderName, it->first);
        ++it;
    }

    return highestOrderName;
}

unsigned int
MethodDatabase::LowestValidOrderNumber() const
{
    unsigned int orderNumber (-1);
    std::map<unsigned int, std::wstring>::const_iterator it = bellNames.begin();
    if (it == bellNames.end())
    {
        orderNumber = 4;
    }
    while (it != bellNames.end())
    {
        if (it->second.length() > 0)
        {
            orderNumber = min (orderNumber, it->first);
        }
        ++it;
    }
    return orderNumber;
}

bool
MethodDatabase::FindOrderNumber(const std::wstring& name, unsigned int& order, bool& dualOrder) const
{
    order = -1;
    dualOrder = false;
    std::map<unsigned int, std::wstring>::const_iterator it = bellNames.begin();
    bool found = false;
    while (it != bellNames.end())
    {
        if (DucoEngineUtils::FindSubstring(it->second, name))
        {
            found = true;
            if (order != -1)
            {
                dualOrder = true;
                order = max(order, it->first);
            }
            else
                order = it->first;
        }
        ++it;
    }

    return found;
}

void
MethodDatabase::GetAllOrderNames(std::vector<std::wstring>& orderNames) const
{
    orderNames.clear();
    std::map<unsigned int, std::wstring>::const_iterator it = bellNames.begin();
    while (it != bellNames.end())
    {
        orderNames.push_back(it->second);
        ++it;
    }
}

void
MethodDatabase::GetAllOrders(std::vector<unsigned int>& orders) const
{
    orders.clear();
    std::map<unsigned int, std::wstring>::const_iterator it = bellNames.begin();
    while (it != bellNames.end())
    {
        orders.push_back(it->first);
        ++it;
    }
}

void
MethodDatabase::ExternaliseOrderNames(Duco::ImportExportProgressCallback* callback, Duco::DatabaseWriter& writer) const
{
    size_t count = 0;
    std::map<unsigned int, std::wstring>::const_iterator it = bellNames.begin();
    while (it != bellNames.end())
    {
        ++count;
        int noOfBells = it->first;
        writer.WriteInt(noOfBells);
        writer.WriteString(it++->second);
        if (callback != NULL)
        {
            callback->ObjectProcessed(false, TObjectType::EOrderName, noOfBells, 0, count);
        }
    }
}

bool
MethodDatabase::DualOrder(const std::wstring& orderNameToFind) const
{
    if (orderNameToFind.find(L"and") != std::wstring::npos || orderNameToFind.find(L"&") != std::wstring::npos)
        return true;

    unsigned int order (0);
    bool dualOrder (false);
    if (FindOrderNumber(orderNameToFind, order, dualOrder))
        return dualOrder;

    std::wstring orderNameToFindTemp (orderNameToFind);

    int noOfOrderNamesFound (0);
    std::map<unsigned int, std::wstring>::const_iterator it = bellNames.begin();
    while (it != bellNames.end())
    {
        size_t index = orderNameToFindTemp.find(it->second);
        if (index != string::npos)
        {
            ++noOfOrderNamesFound;
        }
        ++it;
    }

    return noOfOrderNamesFound == 2;
}

bool
MethodDatabase::FindAndRemoveOrderName(unsigned int& order, std::wstring& completeMethodName, bool& dualOrder) const
{
    dualOrder = false;
    std::map<unsigned int, std::wstring>::const_reverse_iterator it = bellNames.rbegin();
    while (it != bellNames.rend())
    {
        size_t methodStagePos = completeMethodName.find(it->second);
        if (methodStagePos != string::npos)
        {
            order = it->first;
            std::wstring startTemp (L"");
            if (methodStagePos > 0)
            {
                startTemp = completeMethodName.substr(0, methodStagePos -1);
            }
            std::wstring endTemp = completeMethodName.substr(methodStagePos + it->second.length());

            // Check for spliced stage
            ++it;
            if (it != bellNames.rend())
            {
                size_t secondMethodStagePos = completeMethodName.find(it->second);
                if (secondMethodStagePos != std::wstring::npos)
                {
                    dualOrder = true;
                    if (secondMethodStagePos > 0)
                    {
                        startTemp = completeMethodName.substr(0, secondMethodStagePos-1);
                    }
                    else
                    {
                        startTemp.clear();
                    }
                }
            }
            completeMethodName = DucoEngineUtils::Trim(startTemp + endTemp);

            return true;
        }
        ++it;
    }

    return false;
}

bool
MethodDatabase::FindAndRemoveMethodType(std::wstring& type, std::wstring& fullMethodName) const
{
    type.clear();
    std::set<std::wstring> methodTypes;
    GetAllMethodTypes(methodTypes);

    std::set<std::wstring>::const_iterator it = methodTypes.begin();
    while (it != methodTypes.end() && type.length() == 0)
    {
        if (it->length() > 2)
        {
            size_t pos (fullMethodName.find(*it));
            if (pos != string::npos)
            {
                type = *it;
                std::wstring startTemp = fullMethodName.substr(0, pos - 1);
                std::wstring endTemp = fullMethodName.substr(pos + it->length());
                fullMethodName = startTemp + endTemp;
            }
        }
        ++it;
    }

    if (type.length() == 0)
    {
        std::wstring::size_type closeBracket = fullMethodName.find_last_of(')');
        std::wstring::size_type openBracket = fullMethodName.find_last_of('(', closeBracket);
        std::wstring noOfMethodsStr (L"");
        if (closeBracket != string::npos && openBracket != string::npos)
        {
            noOfMethodsStr = fullMethodName.substr(openBracket, closeBracket - openBracket + 1);
            std::wstring startTemp;
            if (openBracket > 0)
                startTemp = fullMethodName.substr(0, openBracket - 1);
            std::wstring endTemp = fullMethodName.substr(closeBracket + 1);
            fullMethodName = startTemp + endTemp;
        }
        else if (openBracket != string::npos)
        {
            fullMethodName = fullMethodName.substr(0, openBracket - 1);
        }
        size_t typeSeperator = fullMethodName.find_last_of(' ');
        if (typeSeperator != string::npos)
        {
            type = DucoEngineUtils::Trim(fullMethodName.substr(typeSeperator));
            fullMethodName = DucoEngineUtils::Trim(fullMethodName.substr(0, typeSeperator));
        }
        if (noOfMethodsStr.length() > 0)
        {
            if (fullMethodName.length() > 0)
                fullMethodName.append(L" ");
            fullMethodName.append(noOfMethodsStr);
        }
    }
    return type.length() > 0;
}

bool
MethodDatabase::MethodIsSpliced(const Duco::ObjectId& methodId) const
{
    DUCO_OBJECTS_CONTAINER::const_iterator it = listOfObjects.find(methodId);
    while (it != listOfObjects.end())
    {
        Method* theMethod = static_cast<Method*>(it->second);
        return theMethod->Spliced();
    }
    return false;
}

bool
MethodDatabase::UpdateObjectField(const Duco::ObjectId& id, const Duco::TUpdateArgument& updateArgument)
{
    DUCO_OBJECTS_CONTAINER::iterator it = listOfObjects.find(id);
    if (it == listOfObjects.end())
        return false;

    Duco::Method* obj = static_cast<Duco::Method*>(it->second);

    return updateArgument.Update(*obj);
}

std::map<Duco::ObjectId, Duco::ObjectId>
MethodDatabase::RemoveDuplicateMethods(ProgressCallback*const callback, size_t& stepNumber, const size_t totalObjects)
{
    std::multimap<std::wstring, Duco::ObjectId> candidateMethodIds;

    DUCO_OBJECTS_CONTAINER::const_iterator it = listOfObjects.begin();
    while (it != listOfObjects.end())
    {
        const Duco::Method* const theMethod = static_cast<const Duco::Method* const>(it->second);
        std::wstring methodDescription(theMethod->FullName(*this));
        if (theMethod->PlaceNotation().length() > 0)
        {
            methodDescription.append(theMethod->PlaceNotation());
        }
        std::pair<std::wstring, Duco::ObjectId> newObject2(methodDescription, it->first);
        candidateMethodIds.insert(newObject2);
        ++it;
    }

    // Find duplicated with same name and placenotation.
    std::map<Duco::ObjectId, Duco::ObjectId> methodsIdsToReplace;
    std::wstring lastString;
    Duco::ObjectId lastId;
    std::multimap<std::wstring, Duco::ObjectId>::const_iterator it2 = candidateMethodIds.begin();
    while (it2 != candidateMethodIds.end())
    {
        if (lastString.compare(it2->first) != 0)
        {
            lastString = it2->first;
            lastId = it2->second;
        }
        else if (lastString.length() > 0 && lastId.ValidId())
        {
            std::pair<Duco::ObjectId, Duco::ObjectId> newObject(it2->second, lastId);
            methodsIdsToReplace.insert(newObject);
        }
        ++it2;
    }

    return methodsIdsToReplace;
}
