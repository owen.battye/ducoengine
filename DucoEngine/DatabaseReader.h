#ifndef __DATABASEREADER_H__
#define __DATABASEREADER_H__

#include <ctime>
#include <fstream>
#include "DucoEngineCommon.h"
#include <string>

namespace Duco
{
    class PerformanceDate;
    class PerformanceTime;
    class ObjectId;

class DatabaseReader
{
public:
    DllExport DatabaseReader(const char* filename);
    DllExport ~DatabaseReader();

    DllExport int ReadInt();
    DllExport unsigned int ReadUInt();
    DllExport bool ReadBool();
    DllExport time_t ReadTime();
    DllExport void ReadString(std::wstring& str);
    DllExport void ReadPictureBuffer(std::string*& str);
    DllExport void ReadTime(Duco::PerformanceTime& dateTime);
    DllExport void ReadDate(Duco::PerformanceDate& dateTime);
    DllExport void ReadId(Duco::ObjectId& id);

private:
    std::wifstream* input;
};

} // namespace Duco;

#endif // __DATABASEREADER_H__
