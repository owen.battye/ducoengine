#ifndef __REPLACESTRINGFIELD_H__
#define __REPLACESTRINGFIELD_H__

#include "UpdateArgument.h"
#include <string>

namespace Duco
{
class TReplaceStringField : public TUpdateArgument
{
public:
    DllExport TReplaceStringField(Duco::TUpdateFieldId newFieldId, bool exactMatch, const std::wstring& stringToFind, const std::wstring& replacementString);
    DllExport virtual ~TReplaceStringField();

    virtual bool Update(Duco::Method& object) const;
    virtual bool Update(Duco::Peal& object) const;
    virtual bool Update(Duco::Tower& object) const;

    virtual Duco::TFieldType FieldType() const;
 
protected:
    bool StringMatch(const std::wstring& stringToSearch, std::wstring& replacementString) const;

    bool exactMatch;
    const std::wstring stringToFind;
    const std::wstring replacementString;
};
 
}
#endif //__REPLACESTRINGFIELD_H__

