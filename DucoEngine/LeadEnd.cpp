#include "LeadEnd.h"

using namespace std;
using namespace Duco;

#include "Change.h"

LeadEnd::LeadEnd(const Duco::Change& other, TLeadType newLeadType, bool isCourseEnd)
:   leadType (newLeadType), partEnd(false), courseEnd(isCourseEnd)

{
    leadEndString = new wstring(other.Str(true, true));
}

LeadEnd::LeadEnd(const Duco::LeadEnd& rhs)
    :   leadType(rhs.leadType), partEnd(rhs.partEnd), courseEnd(rhs.courseEnd)
{
    leadEndString = new wstring(*rhs.leadEndString);
}

LeadEnd::~LeadEnd()
{
    delete leadEndString;
}

LeadEnd*
LeadEnd::Realloc()
{
    return new LeadEnd(*this);
}

bool
LeadEnd::Check(const Duco::Change& leadEnd) const
{
    return leadEnd.Str() == Str();
}
