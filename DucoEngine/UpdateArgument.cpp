#include "UpdateArgument.h"

using namespace Duco;

TUpdateArgument::TUpdateArgument(Duco::TUpdateFieldId newFieldId)
: fieldId(newFieldId)
{

}

TUpdateArgument::~TUpdateArgument()
{

}

const Duco::TUpdateFieldId
TUpdateArgument::FieldId() const
{
    return fieldId;
}
