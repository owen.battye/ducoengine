#ifndef __TOWERDATABASE_H__
#define __TOWERDATABASE_H__

#include "RingingObjectDatabase.h"
#include <list>
#include <string>
#include <set>
#include <vector>

namespace Duco
{
    class DatabaseSettings;
    class DoveDatabase;
    class DoveObject;
    class PealDatabase;
    class ProgressCallback;
    class RenumberProgressCallback;
    class StatisticFilters;
    class Tower;

class TowerDatabase: public RingingObjectDatabase
{
public:
    DllExport TowerDatabase();
    DllExport ~TowerDatabase();

    // from RingingObjectDatabase
    DllExport virtual Duco::ObjectId AddObject(const Duco::RingingObject& newObject);
    virtual bool Internalise(DatabaseReader& reader, Duco::ImportExportProgressCallback* newCallback, int databaseVersionNumber, size_t noOfObjectsInThisDatabase, size_t totalObjects, size_t objectsProcessedSoFar);
    virtual bool RebuildObjects(Duco::RenumberProgressCallback* callback, Duco::RingingDatabase& database);
    bool RenumberPicturesInTowers(const std::map<Duco::ObjectId, Duco::ObjectId>& sortedPictureIds, Duco::RenumberProgressCallback* callback);
    virtual bool MatchObject(const Duco::RingingObject& object, const Duco::TSearchArgument& searchArg, const Duco::RingingDatabase& database) const;
    virtual bool ValidateObject(const Duco::RingingObject& object, const Duco::TSearchValidationArgument& searchArg, const std::set<Duco::ObjectId>& idsToCheckAgainst, const Duco::RingingDatabase& database) const;
    virtual void ExternaliseToXml(Duco::ImportExportProgressCallback* newCallback, XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, bool exportDucoObjects) const;
    virtual bool UpdateObjectField(const Duco::ObjectId& id, const Duco::TUpdateArgument& updateArgument);
    bool CapitaliseField(Duco::ProgressCallback& progressCallback, bool county, bool dedication, size_t& numberOfObjectsUpdated, size_t numberOfObjectsToUpdate);

    // new functions
    DllExport bool RingsIdentical(const Duco::ObjectId& lhs, const Duco::ObjectId& rhs) const;
    DllExport bool RingTranslations(const Duco::ObjectId& lhs, const Duco::ObjectId& rhs, std::map<Duco::ObjectId, Duco::ObjectId>& otherRingIds) const;
    DllExport const Tower* const FindTower(const Duco::ObjectId& towerId, bool setIndex = false) const;
    DllExport Duco::ObjectId FindTowerByDoveId(const std::wstring& doveId, unsigned int noOfBells = -1, const std::wstring& tenorWeight = L"", const std::wstring& tenorKey = L"") const;
    DllExport Duco::ObjectId FindTowerByTowerbaseId(const std::wstring& towerbaseId, unsigned int noOfBells = -1, const std::wstring& tenorWeight = L"", const std::wstring& tenorKey = L"") const;
    DllExport Duco::ObjectId SuggestTower(const std::wstring& title, unsigned int noOfBells, bool duringImport = false) const;
    DllExport Duco::ObjectId SuggestTower(const std::wstring& originalFullName, const std::wstring& town, const std::wstring& county, unsigned int noOfBells, const std::wstring& tenorWeight, const std::wstring& tenorKey, bool duringImport = false) const;
    DllExport void RemoveUsedIds(std::set<Duco::ObjectId>& unusedTowers, const Duco::DatabaseSettings& settings) const;
    void RemoveUsedPictureIds(std::set<Duco::ObjectId>& unusedPictureIds) const;
    DllExport Duco::ObjectId AddTower(const Duco::DoveObject& newTower);
    DllExport bool CheckLinkedTowerId(const Duco::ObjectId& towerId, const Duco::ObjectId& towerIdToLink, std::wstring& errorString) const;
    DllExport bool Position(const Duco::ObjectId& towerId, std::wstring& latitudeLongitude) const;
    DllExport bool SetTowersAsHandbell(const std::set<Duco::ObjectId>& towerIds);

    DllExport bool MergeTowerRings(const Duco::ObjectId& towerId, const Duco::Tower& otherTower);
    DllExport size_t NumberOfActiveTowers(size_t& noOfLinkedTowers) const;
    DllExport size_t NumberOfActiveTowersWithoutDoveId() const;
    DllExport bool TowersWithoutDoveId(std::set<Duco::ObjectId>& towerIds) const;
    DllExport bool TowersWithDoveId(std::set<Duco::ObjectId>& towerIds, bool andBlankLocationOrTowerbaseId) const;
    DllExport bool ImportMissingTenorKeys(const Duco::DoveDatabase& doveDatabase);
    DllExport size_t NumberOfRemovedTowers() const;
    DllExport size_t NumberOfUniqueTowers() const;
    DllExport unsigned int NoOfBellsInRing(const Duco::ObjectId& towerId, const Duco::ObjectId& ringId);
    DllExport virtual bool RebuildRecommended() const;

    DllExport bool DeleteRing(const Duco::ObjectId& towerId, const Duco::ObjectId& ringId);
    bool DeleteRings(const Duco::ObjectId& towerId, const std::vector<Duco::ObjectId>& ringIdsToDelete);
    DllExport void GetFelsteadPealCount(std::map<Duco::ObjectId, Duco::PealLengthInfo>& sortedTowerIds, const Duco::DatabaseSettings& settings, const Duco::StatisticFilters& filters, bool combineLinkedTowers = false) const;
    void CombineLinkedTowerData(std::map<ObjectId, Duco::PealLengthInfo>& towersPealData) const;

    //*************************************************************
    // Pictures
    //*************************************************************
    DllExport void FindPicture(const Duco::ObjectId& pictureId, std::set<Duco::ObjectId>& pealIds) const;
    void GetPictureIds(std::map<Duco::ObjectId, Duco::ObjectId>& sortedPictureIds) const;
    bool GetPictureIdForTower(const Duco::ObjectId& towerId, Duco::ObjectId& pictureId) const;
    bool RemovePictures(const std::set<Duco::ObjectId>& pictureIds);

    void AddMissingTowers(std::map<Duco::ObjectId, Duco::PealLengthInfo>& towerIds, const Duco::StatisticFilters& filters) const;
    DllExport void GetAllPlaceOptions(std::set<std::wstring>& counties, std::set<std::wstring>& cities, std::set<std::wstring>& names) const;
    DllExport void GetAllObjectIds(std::set<Duco::ObjectId>& allTowerIds, std::multimap<Duco::ObjectId, Duco::ObjectId>& allRingIds) const;
    DllExport void GetAllObjectIds(std::set<Duco::ObjectId>& parentTowerIds, std::map<Duco::ObjectId, Duco::ObjectId>& linksToParent) const;
    DllExport void GetAllLinkedIds(std::list<std::set<Duco::ObjectId> >& linkedTowerIds) const;
    DllExport void MergeTowersWithSameLocation(std::map<Duco::ObjectId, Duco::PealLengthInfo>& towerPerformanceInfo) const;
    DllExport void GetLinkedIds(const Duco::ObjectId& parentTowerIds, std::set<Duco::ObjectId>& linkedIds) const;
    DllExport Duco::ObjectId GetMasterTowerId(const std::set<Duco::ObjectId>& linkedIds) const;
    DllExport void GetTowersWithoutRings(std::set<Duco::ObjectId>& towersWithOutRings) const;

    DllExport bool UppercaseCities(Duco::ProgressCallback*const callback);
    DllExport bool ClearInvalidTenorKeys(Duco::ProgressCallback* const callback);
    bool RemoveDuplicateRings(PealDatabase& pealsDb, ProgressCallback*const callback);

protected:
    // from RingingObjectDatabase
    bool SaveUpdatedObject(Duco::RingingObject& lhs, const Duco::RingingObject& rhs);

    // new functions
    bool CheckTowerDetails(const Duco::Tower& theTower, unsigned int noOfBells, const std::wstring& tenorWeight, const std::wstring& tenorKey) const;
};

}

#endif //!__TOWERDATABASE_H__
