#ifndef __COUNTSTRINGSORTOBJECT_H__
#define __COUNTSTRINGSORTOBJECT_H__

#include <string>
#include "PealLengthInfo.h"

namespace Duco
{

class CountStringSortObject
{
public:
    CountStringSortObject(Duco::PealLengthInfo newPealCount, const std::wstring& newObjectName);
    CountStringSortObject(const CountStringSortObject& rhs);
    ~CountStringSortObject();

    bool operator<(const CountStringSortObject& rhs) const;
    bool operator>(const CountStringSortObject& rhs) const;
    bool operator==(const CountStringSortObject& rhs) const;
    bool operator!=(const CountStringSortObject& rhs) const;
    CountStringSortObject& operator=(const CountStringSortObject& rhs);

protected:
    Duco::PealLengthInfo    pealCount;
    std::wstring             objectName;

};

} // Namespace Duco

#endif//! __COUNTSTRINGSORTOBJECT_H__
