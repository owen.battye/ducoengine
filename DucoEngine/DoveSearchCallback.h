#ifndef __DOVESEARCHCALLBACK_H__
#define __DOVESEARCHCALLBACK_H__

#include "ProgressCallback.h"

namespace Duco
{
    class DoveObject;

class DoveSearchCallback
:   public ProgressCallback
{
public:
    virtual void TowerFound(const Duco::DoveObject& newTower) = 0;
};

} // end namespace Duco

#endif //!__DOVESEARCHCALLBACK_H__
