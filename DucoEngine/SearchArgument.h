#ifndef __SEARCHARGUMENT_H__
#define __SEARCHARGUMENT_H__

#include "SearchCommon.h"
#include "DucoEngineCommon.h"
#include "ObjectId.h"

namespace Duco
{
    class Association;
    class Composition;
    class Method;
    class MethodSeries;
    class Peal;
    class Ringer;
    class RingingDatabase;
    class Tower;
    class Picture;

class TSearchArgument
{
public:
    virtual bool Match(const Duco::Peal& object, const Duco::RingingDatabase& database) const = 0;
    virtual bool Match(const Duco::MethodSeries& object, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Method& object, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Ringer& object, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Tower& object, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Composition& object, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Association& object, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Picture& object, const Duco::RingingDatabase& database) const;

    virtual Duco::TFieldType FieldType() const = 0;
    DllExport virtual const TSearchFieldId FieldId() const;

    DllExport virtual ~TSearchArgument();

protected:
    TSearchArgument(Duco::TSearchFieldId newFieldId);

    const TSearchFieldId fieldId;
};
 
}
#endif //__SEARCHARGUMENT_H__

