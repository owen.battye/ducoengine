#ifndef __YEARIDGROUP_H__
#define __YEARIDGROUP_H__

#include <set>
#include "ObjectId.h"

namespace Duco
{
    class YearId;

struct YearIdCompare
{
  bool operator() (const YearId* lhs, const YearId* rhs) const;
};

class YearIdGroup
{
public:
    YearIdGroup();
    ~YearIdGroup();

    size_t Count(unsigned int year) const;
    void Add(unsigned int newYear, const Duco::ObjectId& newId);
    void Add(unsigned int newYear, const std::set<ObjectId>& newIds);

protected:
    std::set<YearId*, Duco::YearIdCompare>* ids;
};

}

#endif //!__YEARIDGROUP_H__
