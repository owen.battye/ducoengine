#ifndef __GENDER_H__
#define __GENDER_H__

#include <bitset>
#include <string>
#include "DucoEngineCommon.h"

const int KGenderBitsetLength = 2;
const int KFemaleBit = 0;
const int KMaleBit = 1;

namespace Duco
{
    class DatabaseWriter;
    class DatabaseReader;

class Gender
{
public:
    DllExport Gender();
    DllExport Gender(const Gender& other);
    DllExport ~Gender();

    DatabaseWriter& Externalise(DatabaseWriter& writer) const;
    DatabaseReader& Internalise(DatabaseReader& reader, unsigned int databaseVersion);

    DllExport Duco::Gender& operator=(const Gender& rhs);
    DllExport bool operator==(const Duco::Gender& rhs) const;
    DllExport bool operator!=(const Duco::Gender& rhs) const;

    // Accessors
    inline bool Defined() const;
    inline bool Male() const;
    inline bool Female() const;

    // Settors
    inline void SetMale();
    inline void SetFemale();
    DllExport std::wstring Str() const;

private:
    std::bitset<KGenderBitsetLength> genderFlags;
};

bool
Gender::Defined() const
{
    return genderFlags.any();
}

bool
Gender::Male() const
{
    return genderFlags.test(KMaleBit);
}

void
Gender::SetMale()
{
    genderFlags.set(KMaleBit);
    genderFlags.reset(KFemaleBit);
}

bool
Gender::Female() const
{
    return genderFlags.test(KFemaleBit);
}

void
Gender::SetFemale()
{
    genderFlags.set(KFemaleBit);
    genderFlags.reset(KMaleBit);
}

} // end namespace Duco

#endif //!__GENDER_H__
