#ifndef __LEADORDER_H__
#define __LEADORDER_H__

#include "DucoEngineCommon.h"
#include <vector>
#include <string>

namespace Duco
{
class Calling;

class LeadOrder
{
    friend class PlaceNotation;

public:
    DllExport LeadOrder(const LeadOrder& other);
    DllExport virtual ~LeadOrder();

    // Accessors
    inline bool                 IncludeTreble() const;
    inline unsigned int         WrongPosition() const;
    inline unsigned int         HomePosition() const;
    inline unsigned int         MiddlePosition() const;
    DllExport const std::vector<unsigned int>& Leads() const;
    DllExport bool              IsPrinciple() const;
    bool                        IsHome(const Calling& calling, bool affectedPosition = true) const;

    // Settors
    inline void                 Add(unsigned int startPosition);
    void                        SetPositions();
    Duco::LeadOrder&            Reset();

    // Queries
    DllExport bool              ValidLeadOrder(unsigned int order) const;
    inline size_t               Size() const;
    DllExport std::wstring       CheckPosition(unsigned int position) const;
    DllExport unsigned int      CheckPosition(wchar_t position) const;
    std::wstring                 StartingLeadEnd(unsigned int order) const;
    DllExport unsigned int      Index(unsigned int bell) const;

    // Course generation
    bool                        AddCall(unsigned int newCallingPosition);
    inline void                 ResetCallingPostion();

    //operators
    DllExport Duco::LeadOrder& operator=(const Duco::LeadOrder& other);

protected:
    DllExport LeadOrder();

    std::vector<unsigned int>   leadOrder;
    std::vector<unsigned int>::const_iterator leadOrderPosition;
    bool                        includeTreble;
    unsigned int                wrongPosition;
    unsigned int                homePosition;
    unsigned int                middlePosition;
};

bool
LeadOrder::IncludeTreble() const
{
    return includeTreble;
}

unsigned int
LeadOrder::WrongPosition() const
{
    return wrongPosition;
}

unsigned int
LeadOrder::HomePosition() const
{
    return homePosition;
}

unsigned int
LeadOrder::MiddlePosition() const
{
    return middlePosition;
}

size_t
LeadOrder::Size() const
{
    return leadOrder.size();
}

void
LeadOrder::Add(unsigned int startPosition)
{
    leadOrder.push_back(startPosition);
}

void
LeadOrder::ResetCallingPostion()
{
    leadOrderPosition = leadOrder.end();
}

}

#endif //!__LEADORDER_H__
