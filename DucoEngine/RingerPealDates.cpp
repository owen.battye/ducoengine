#include "RingerPealDates.h"
#include "Peal.h"

using namespace Duco;
using namespace std;

RingerPealDates::RingerPealDates(const Duco::ObjectId& _ringerId, const Duco::PerformanceDate& _dateOfNewPeal) : PealLengthInfo(), ringerId(_ringerId), dateOfNewPeal(_dateOfNewPeal)
{
    nearestDateBefore.ResetToEarliest();
    nearestDateAfter.ResetToLatest();
}

bool
RingerPealDates::operator<(const Duco::RingerPealDates& rhs) const
{
    unsigned int lhsDaysBefore = nearestDateBefore.NumberOfDaysUntil(dateOfNewPeal);
    unsigned int lhsDaysAfter = dateOfNewPeal.NumberOfDaysUntil(nearestDateAfter);
    unsigned int lhsMin = min(lhsDaysBefore, lhsDaysAfter);

    unsigned int rhsDaysBefore = rhs.nearestDateBefore.NumberOfDaysUntil(rhs.dateOfNewPeal);
    unsigned int rhsDaysAfter = rhs.dateOfNewPeal.NumberOfDaysUntil(rhs.nearestDateAfter);
    unsigned int rhsMin = min(rhsDaysBefore, rhsDaysAfter);

    if (lhsMin == rhsMin)
    {
        return ringerId < rhs.ringerId;
    }

    return lhsMin < rhsMin;
}

bool
RingerPealDates::operator>(const Duco::RingerPealDates& rhs) const
{
    unsigned int lhsDaysBefore = nearestDateBefore.NumberOfDaysUntil(dateOfNewPeal);
    unsigned int lhsDaysAfter = dateOfNewPeal.NumberOfDaysUntil(nearestDateAfter);
    unsigned int lhsMin = min(lhsDaysBefore, lhsDaysAfter);

    unsigned int rhsDaysBefore = rhs.nearestDateBefore.NumberOfDaysUntil(rhs.dateOfNewPeal);
    unsigned int rhsDaysAfter = rhs.dateOfNewPeal.NumberOfDaysUntil(rhs.nearestDateAfter);
    unsigned int rhsMin = min(rhsDaysBefore, rhsDaysAfter);

    if (lhsMin == rhsMin)
    {
        return ringerId > rhs.ringerId;
    }
    return lhsMin > rhsMin;
}

void
RingerPealDates::AddPeal(const Duco::Peal& nextPeal)
{
    PealLengthInfo::AddPeal(nextPeal);
    if (nextPeal.Date() <= dateOfNewPeal && (!nearestIdBefore.ValidId() || nextPeal.Date() > nearestDateBefore))
    {
        nearestDateBefore = nextPeal.Date();
        nearestIdBefore = nextPeal.Id();
    }
    if (nextPeal.Date() >= dateOfNewPeal && (!nearestIdAfter.ValidId() || nextPeal.Date() > nearestDateAfter))
    {
        nearestDateAfter = nextPeal.Date();
        nearestIdAfter = nextPeal.Id();
    }
}

std::wstring
RingerPealDates::Description() const
{
    std::wstring description;
    if (nearestIdBefore.ValidId() && nearestIdAfter.ValidId())
    {
        description = L"Ringer has rung in performances before and after this one.";
    }
    else if (nearestIdBefore.ValidId())
    {
        unsigned int daysSince = dateOfNewPeal.NumberOfDaysUntil(nearestDateBefore);
        unsigned int yearsSince = daysSince / 365;
        unsigned int monthsSince = daysSince / 30;
        if (yearsSince > 0)
            description = L"Ringer has rung in performances " + std::to_wstring(yearsSince) + L" years before this one.";
        else if (monthsSince > 0)
            description = L"Ringer has rung in performances " + std::to_wstring(monthsSince) + L" months before this one.";
        else
            description = L"Ringer has rung in performances " + std::to_wstring(daysSince) + L" days before this one.";
    }
    else if (nearestIdAfter.ValidId())
    {
        unsigned int daysAfter = nearestDateAfter.NumberOfDaysUntil(dateOfNewPeal);
        unsigned int yearsAfter = daysAfter / 365;
        unsigned int monthsAfter = daysAfter / 30;
        if (yearsAfter > 0)
            description = L"Ringer has rung in performances " + std::to_wstring(yearsAfter) + L" years after this one.";
        else if (monthsAfter > 0)
            description = L"Ringer has rung in performances " + std::to_wstring(monthsAfter) + L" months after this one.";
        else
            description = L"Ringer has rung in performances " + std::to_wstring(daysAfter) + L" days after this one.";
    }
    else
    {
        description = L"No performances found.";
    }
    return description;
}

bool
RingerPealDates::BeforeAndAfter() const
{
    return nearestIdBefore.ValidId() && nearestIdAfter.ValidId();
}
