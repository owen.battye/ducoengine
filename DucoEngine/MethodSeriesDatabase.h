#ifndef __METHODSERIESDATABASE_H__
#define __METHODSERIESDATABASE_H__

#include "RingingObjectDatabase.h"

#include <map>
#include <set>

namespace Duco
{
    class RenumberProgressCallback;
    class MethodSeries;
    class MethodDatabase;

class MethodSeriesDatabase: public RingingObjectDatabase
{
public:
    DllExport MethodSeriesDatabase();
    ~MethodSeriesDatabase();

    // from RingingObjectDatabase
    virtual Duco::ObjectId AddObject(const Duco::RingingObject& newObject);
    virtual bool Internalise(Duco::DatabaseReader& reader, Duco::ImportExportProgressCallback* newCallback, int databaseVersionNumber, size_t noOfObjectsInThisDatabase, size_t totalObjects, size_t objectsProcessedSoFar);
    DllExport virtual bool RebuildObjects(Duco::RenumberProgressCallback* callback, Duco::RingingDatabase& database);
    virtual bool MatchObject(const Duco::RingingObject& object, const Duco::TSearchArgument& searchArg, const Duco::RingingDatabase& database) const;
    virtual bool ValidateObject(const Duco::RingingObject& object, const Duco::TSearchValidationArgument& searchArg, const std::set<Duco::ObjectId>& idsToCheckAgainst, const Duco::RingingDatabase& database) const;
    virtual void ExternaliseToXml(Duco::ImportExportProgressCallback* newCallback, XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, bool exportDucoObjects) const;
    virtual bool UpdateObjectField(const Duco::ObjectId& id, const Duco::TUpdateArgument& updateArgument);

    // new functions
    DllExport const MethodSeries* const FindMethodSeries(const Duco::ObjectId& seriesId, bool setIndex = false) const;
    bool FindMethodSeries(std::set<Duco::MethodSeries*>& series, unsigned int order) const;

    DllExport bool FindMethodInMethodSeries(const Duco::ObjectId& methodId, Duco::ObjectId& seriesId) const;
    DllExport void GetMethodSeriesByNoOfBells(std::vector<Duco::ObjectId>& methodIds, unsigned int noOfBells) const;

    DllExport void RemoveUsedIds(std::set<Duco::ObjectId>& unusedMethodsIds) const;
    DllExport bool RenumberMethodsInMethodSeries(const std::map<Duco::ObjectId, Duco::ObjectId>& newMethodIds, Duco::RenumberProgressCallback* callback);
    void AddMissingSeries(std::map<Duco::ObjectId, Duco::PealLengthInfo>& sortedMethodSeriesIds, unsigned int noOfBells) const;

protected:
    // from RingingObjectDatabase
    bool SaveUpdatedObject(Duco::RingingObject& lhs, const Duco::RingingObject& rhs);
};

}

#endif //!__METHODSERIESDATABASE_H__
