#include "YearIdGroup.h"
#include "YearId.h"

using namespace Duco;

bool
YearIdCompare::operator() (const YearId* lhs, const YearId* rhs) const
{
    return (*lhs).operator<(*rhs);
}

YearIdGroup::YearIdGroup()
{
    ids = new std::set<YearId*, YearIdCompare>();
}

YearIdGroup::~YearIdGroup()
{
    std::set<YearId*, YearIdCompare>::iterator it = ids->begin();
    while (it != ids->end())
    {
        delete *it;
        ++it;
    }
    ids->clear();
    delete ids;
}

size_t
YearIdGroup::Count(unsigned int year) const
{
    Duco::YearId* newTowerObject = new Duco::YearId(year, 0);
    std::set<Duco::YearId*, YearIdCompare>::iterator idsIt = ids->find(newTowerObject);
    if (idsIt == ids->end())
        return 0;

    return (*idsIt)->Count();
}

void
YearIdGroup::Add(unsigned int newYear, const ObjectId& newId)
{
    Duco::YearId* newTowerObject = new Duco::YearId(newYear, newId);
    std::set<Duco::YearId*, YearIdCompare>::iterator idsIt = ids->find(newTowerObject);
    if (idsIt == ids->end())
    {
        ids->insert(newTowerObject);
    }
    else
    {
        (*idsIt)->Add(newId);
    }
}

void
YearIdGroup::Add(unsigned int newYear, const std::set<ObjectId>& newIds)
{
    std::set<ObjectId>::const_iterator it = newIds.begin();
    while (it != newIds.end())
    {
        Add(newYear, *it);
        ++it;
    }
}
