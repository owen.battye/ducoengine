#ifndef __PICTURE_H__
#define __PICTURE_H__

#include "DucoEngineCommon.h"
#include "RingingObject.h"
#include "PictureType.h"

namespace Duco
{
class DatabaseWriter;
class DatabaseReader;

class Picture : public RingingObject
{
public:
    DllExport Picture();
    DllExport Picture(const std::string& fileName);
    DllExport Picture(DatabaseReader& reader, unsigned int databaseVersion);
    DllExport Picture(const Picture& other);
    DllExport Picture(const Duco::ObjectId& newId, const Duco::Picture& other);
    DllExport ~Picture();

    DllExport virtual TObjectType ObjectType() const;
    DllExport virtual bool Duplicate(const Duco::RingingObject& other, const Duco::RingingDatabase& database) const;
    DllExport bool Match(const Duco::Picture& otherObject) const;

    DatabaseReader& Internalise(DatabaseReader& reader, unsigned int databaseVersion);
    DatabaseWriter& Externalise(DatabaseWriter& writer) const;
    DllExport void SavePicture(std::string& filename) const;
    DllExport std::wostream& Print(std::wostream& stream, const RingingDatabase& database) const;
    virtual void ExportToXml(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& parent, bool exportDucoObjects) const;

    DllExport virtual bool operator==(const Duco::RingingObject& other) const;
    DllExport virtual bool operator!=(const Duco::RingingObject& other) const;
    DllExport virtual Picture& operator=(const Duco::Picture& other);

    virtual bool Valid(const RingingDatabase& database, bool warningFatal, bool clearBeforeStart) const;
    virtual std::wstring ErrorString(const Duco::DatabaseSettings& settings, bool showWarnings) const;

    // Accessor
    DllExport size_t PictureSize() const;
    DllExport const std::string& GetPictureBuffer() const;
    DllExport std::string GetFileExtension() const;
    DllExport bool ContainsValidPicture() const;
    DllExport const std::wstring& Notes() const;

    // Settors
    DllExport void ReplacePicture(const std::string& fileName);
    DllExport void SetNotes(const std::wstring& notes);

protected:
    DllExport void CreatePictureBuffer(const std::string& fileName);

private:
    std::string*            pictureBuffer;
    Duco::PictureType       pictureType;
    unsigned int            databaseVersion;
    std::wstring            notes;
};

} // end namespace Duco

#endif //!__PICTURE_H__
