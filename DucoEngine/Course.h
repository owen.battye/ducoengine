#ifndef __COURSE_H__
#define __COURSE_H__

#include "DucoEngineCommon.h"
#include <vector>
#include <string>

namespace Duco
{
    class Calling;
    class Change;
    class Lead;
    class PlaceNotation;

class Course
{
    friend class CompositionTable;

public:
    DllExport Course(const Duco::Course& rhs);
    DllExport ~Course();

    // Generating the course.
    void Clear(bool includeCourseEnd = false);
    bool AddCalling(const Duco::Calling& newCalling, size_t& noOfChanges, size_t snapStartPos);

    // Accessors
    DllExport const Duco::Change& CourseEnd() const;
    inline size_t NoOfChanges() const;

    DllExport std::wstring CallingStr(wchar_t position) const;
    DllExport std::wstring CallingStr(unsigned int position) const;

protected:
    DllExport Course(const Duco::PlaceNotation& theNotation);
    DllExport Course(const Duco::PlaceNotation& theNotation, const Duco::Change& lastCourseEnd);
    void MoveToHome();

private:
    const Duco::PlaceNotation&  notation;
    Duco::Change*               courseEnd;
    std::vector<Duco::Calling>  callings;
    size_t                      noOfChangesInCourse;
};

size_t
Course::NoOfChanges() const
{
    return noOfChangesInCourse;
}
}

#endif //!__COURSE_H__
