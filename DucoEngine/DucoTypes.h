#ifndef __DUCOTYPES_H__
#define __DUCOTYPES_H__

namespace Duco
{
    enum TConductorType
    {
        ESingleConductor = 0,
        ESilentAndNonConducted,
        EJointlyConducted
    };


    enum TRenameBellType
    {
        ENormalBell = 0,
        EFlat,
        ESharp,
        EExtraTreble
    };
}

#endif //__DUCOTYPES_H__

