#ifndef __MUSIC_H__
#define __MUSIC_H__

#include <string>
#include <list>
#include "DucoEngineCommon.h"

namespace Duco
{
    class Change;
    class ChangeCollection;
    class MusicGroup;
    class ProgressCallback;
    class Rollup;

class Music
{
public:
    DllExport Music(const std::string& installationDir, unsigned int noOfBells, const std::string& fileName = "\\Music.txt");
    DllExport virtual ~Music();

    // Accessors
    DllExport void GenerateMusic(const ChangeCollection& changes, ProgressCallback& callback);
    DllExport std::wstring Detail() const;
    DllExport size_t NumberOfMusicGroups() const;
    DllExport size_t NumberOfRollups() const;

    // Settors

    // other
    DllExport bool CheckChange(Duco::Change& change) const;
    DllExport bool CheckChangeForRollup(Duco::Change& change) const;

protected:
    std::list<Duco::MusicGroup*> objects;
    std::list<Duco::Rollup*>     rollups;
};

}

#endif //!__MUSIC_H__

