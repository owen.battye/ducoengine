#include "Association.h"

#include "DatabaseReader.h"
#include "DatabaseWriter.h"
#include "DucoEngineUtils.h"
#include "DucoXmlUtils.h"
#include "RingingDatabase.h"

#include <sstream>

using namespace std;
using namespace Duco;
XERCES_CPP_NAMESPACE_USE

Association::Association(const Duco::ObjectId& newId, const std::wstring& newName, const std::wstring& newNotes)
    : RingingObject(newId), name(newName), notes(newNotes),
    pealbaseLink(L"")
{
}

Association::Association(const Association& other)
    : RingingObject(other), name(other.name), notes(other.notes),
    pealbaseLink(other.pealbaseLink), linkedAssociations(other.linkedAssociations)
{
}

Association::Association(const Duco::ObjectId& newId, const Association& other)
    : RingingObject(newId), name(other.name), notes(other.notes),
    pealbaseLink(other.pealbaseLink), linkedAssociations(other.linkedAssociations)
{
}

Association::Association(DatabaseReader& reader, unsigned int databaseVersion)
    : RingingObject(), name(L""), notes(L""), pealbaseLink(L"")
{
    Association::Internalise(reader, databaseVersion);
}

Association::Association()
    : RingingObject(), name(L""), notes(L""), pealbaseLink(L"")
{
}

Association::~Association()
{
}

Duco::TObjectType
Association::ObjectType() const
{
    return TObjectType::EAssociationOrSociety;
}

bool
Association::operator==(const RingingObject& other) const
{
    if (other.ObjectType() != ObjectType())
        return false;

    const Duco::Association& otherAssociation = static_cast<const Duco::Association&>(other);

    if (id != otherAssociation.id ||
        name.compare(otherAssociation.name) != 0 ||
        notes.compare(otherAssociation.notes) != 0 ||
        pealbaseLink.compare(otherAssociation.pealbaseLink) != 0)
    {
        return false;
    }

    return true;
}

bool
Association::operator!=(const RingingObject& other) const
{
    if (other.ObjectType() != ObjectType())
        return true;

    const Duco::Association& otherAssociation = static_cast<const Duco::Association&>(other);

    if (id != otherAssociation.id)
        return true;

    return false;
}

Association&
Association::operator=(const Duco::Association& other)
{
    id = other.id;
    name = other.name;
    notes = other.notes;
    errorCode = other.errorCode;
    pealbaseLink = other.pealbaseLink;
    linkedAssociations = other.linkedAssociations;

    return *this;
}

std::wostream&
Association::Print(std::wostream& stream, const RingingDatabase& database) const
{
    stream << name;
    return stream;
}

DatabaseWriter&
Association::Externalise(DatabaseWriter& writer) const
{
    writer.WriteInt(id.Id());
    writer.WriteString(name);
    writer.WriteString(notes);
    writer.WriteString(pealbaseLink);

    writer.WriteInt(linkedAssociations.size());
    for (const auto& it : linkedAssociations)
    {
        writer.WriteId(it);
    }

    return writer;
}

DatabaseReader&
Association::Internalise(DatabaseReader& reader, unsigned int databaseVersion)
{
    id = reader.ReadUInt();
    reader.ReadString(name);
    reader.ReadString(notes);
    reader.ReadString(pealbaseLink);
    if (databaseVersion <= 42)
    {
        std::wstring oldPealsCoUkLink_discard;
        reader.ReadString(oldPealsCoUkLink_discard);
    }
    if (databaseVersion >= 52)
    {
        int numberOfLinks = reader.ReadInt();
        for (int i = 0; i < numberOfLinks; i++)
        {
            linkedAssociations.insert(reader.ReadUInt());
        }
    }

    return reader;
}

std::wofstream&
Association::ExportToRtf(std::wofstream& outputFile, const RingingDatabase& database) const
{
    outputFile << name;
    return outputFile;
}

bool
Association::Valid(const Duco::RingingDatabase& /*database*/, bool warningFatal, bool clearBeforeStart) const
{
    if (clearBeforeStart)
    {
        ClearErrors();
    }
    bool returnVal(true);
    if (name.length() <= 0)
    {
        if (warningFatal)
            returnVal = false;
        errorCode.set(EAssociationWarning_NameMissing, 1);
    }
    if (pealbaseLink.length() <= 0)
    {
        if (warningFatal)
            returnVal = false;
        errorCode.set(EAssociationWarning_PealbaseLinkMissing, 1);
    }
    return returnVal;
}

std::wstring
Association::ErrorString(const Duco::DatabaseSettings& /*settings*/, bool showWarnings) const
{
    std::wstring returnVal(L"");
    if (showWarnings && errorCode.test(EAssociationWarning_NameMissing))
    {
        DucoEngineUtils::AddError(returnVal, L"Association name missing");
    }
    if (errorCode.test(EAssociation_DuplicateAssociation))
    {
        DucoEngineUtils::AddError(returnVal, L"Duplicate Association name");
    }
    if (showWarnings && errorCode.test(EAssociationWarning_PealbaseLinkMissing))
    {
        DucoEngineUtils::AddError(returnVal, L"Pealbase id missing");
    }

    return returnVal;
}

bool
Association::Similar(const Duco::RingingObject& otherMethod) const
{
    bool similar(false);
    if (otherMethod.ObjectType() != ObjectType())
        return false;

    const Duco::Association& otherAssociationPtr = static_cast<const Duco::Association&>(otherMethod);

    if (DucoEngineUtils::CompareString(name, otherAssociationPtr.name) ||
        DucoEngineUtils::CompareString(pealbaseLink, otherAssociationPtr.pealbaseLink, true, false))
    {
        similar = true;
    }

    return similar;
}

bool
Association::operator!=(const Duco::Association& rhs) const
{
    if (name.compare(rhs.name) != 0)
        return true;
    if (pealbaseLink.compare(rhs.pealbaseLink) != 0)
        return true;

    return false;
}

bool
Association::operator==(const Duco::Association& rhs) const
{
    if (name.compare(rhs.name) != 0)
        return false;
    if (pealbaseLink.compare(rhs.pealbaseLink) != 0)
        return true;

    return true;
}

void
Association::SetError(TAssociationErrorCodes code)
{
    errorCode.set(code, 1);
}

void
Association::ExportToCsv(std::wofstream& file) const
{
    file << "\"";
    file << name;
    file << "\"";
}

bool
Association::UppercaseAssociation()
{
    std::wstring newName;
    DucoEngineUtils::ToUpperCase(name, newName);
    if (name != newName)
    {
        name = newName;
        return true;
    }
    return false;
}

std::wstring
Association::FullNameForSort(bool unusedSetting) const
{
    return name;
}

void
Association::ExportToXml(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& associationsElement, bool exportDucoObjects) const
{
    DOMElement* newAssociation = outputFile.createElement(XMLStrL("Association"));
    associationsElement.appendChild(newAssociation);

    DucoXmlUtils::AddObjectId(*newAssociation, id);
    DucoXmlUtils::AddElement(outputFile, *newAssociation, L"Name", name);

    DucoXmlUtils::AddAttribute(*newAssociation, L"PealBaseLink", pealbaseLink);
}

bool
Association::Duplicate(const Duco::RingingObject& other, const Duco::RingingDatabase& database) const
{
    if (other.ObjectType() != ObjectType())
        return false;

    const Duco::Association& otherAssociation = static_cast<const Duco::Association&>(other);

    if (DucoEngineUtils::CompareString(otherAssociation.Name(), this->Name(), true, false))
    {
        return true;
    }
    return false;
}


bool
Association::AddAssociationLink(const Duco::ObjectId& newAssocoationId)
{
    if (newAssocoationId.ValidId())
    {
        linkedAssociations.insert(newAssocoationId);
        return true;
    }
    return false;
}

bool
Association::RemoveAssociationLink(const Duco::ObjectId& oldAssociationId)
{
    if (linkedAssociations.contains(oldAssociationId))
    {
        linkedAssociations.erase(oldAssociationId);
        return true;
    }
    return false;
}

bool
Association::ReplaceAssociationLink(const Duco::ObjectId& oldAssociationId, const Duco::ObjectId& newAssociationId)
{
    if (linkedAssociations.contains(oldAssociationId))
    {
        linkedAssociations.erase(oldAssociationId);
        linkedAssociations.insert(newAssociationId);
        return true;
    }
    return false;
}

bool
Association::LinkedToAssociation(const Duco::ObjectId& associationId) const
{
    return linkedAssociations.contains(associationId);
}

bool
Association::RenumberAssociationLinks(const std::map<Duco::ObjectId, Duco::ObjectId>& newAssociationIds)
{
    std::set<Duco::ObjectId> newLinkedAssociations;
    bool changed = false;
    for (auto& oldId : linkedAssociations)
    {
        const std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator it = newAssociationIds.find(oldId);
        if (it != newAssociationIds.end())
        {
            newLinkedAssociations.insert(it->second);
            changed = true;
        }
        else
        {
            newLinkedAssociations.insert(oldId);
        }
    }
    if (changed)
    {
        linkedAssociations = newLinkedAssociations;
    }

    return changed;
}


const std::wstring&
Association::Name() const
{
    return name;
}

const std::wstring&
Association::Notes() const
{
    return notes;
}

const std::wstring&
Association::PealbaseLink() const
{
    return pealbaseLink;
}

void
Association::SetName(const std::wstring& newName)
{
    name = newName;
}

void
Association::SetNotes(const std::wstring& newNotes)
{
    notes = newNotes;
}

void
Association::SetPealbaseLink(const std::wstring& newLink)
{
    ClearErrors();
    pealbaseLink = newLink;
}

const std::set<Duco::ObjectId>&
Association::Links() const
{
    return linkedAssociations;
}
