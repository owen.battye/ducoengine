﻿#include "DucoEngineUtils.h"

#include "Ring.h"
#include <cwctype>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <ranges>
#include <algorithm>

using namespace Duco;
using namespace std;

namespace Duco
{
    //const std::wstring timeSeperatorCharacters = L":hH";
    const unsigned int KMaxNoOfBellsInTower = 30;
    const std::wstring numericCharacters = L"1234567890";
}

bool
DucoEngineUtils::IsWhiteSpace(const wchar_t& inputChar)
{
    switch (inputChar)
    {
        case 9:     // Tab
        case 10:    // Line feed
        case 12:    // Form feed
        case 32:    // Space
            return true;

        default:
            break;
    }
    return false;
}

std::wstring
DucoEngineUtils::Trim(const std::wstring& inputStr)
{
    if (inputStr.length() == 0)
        return inputStr;

    size_t endOfString = inputStr.length() - 1;
    while (endOfString != std::wstring::npos && IsWhiteSpace(inputStr[endOfString]))
    {
        --endOfString;
    }

    size_t startOfString = 0;
    while (startOfString < endOfString && IsWhiteSpace(inputStr[startOfString]))
    {
        ++startOfString;
    }

    return inputStr.substr(startOfString, endOfString - startOfString + 1);
}

std::wstring
DucoEngineUtils::TrimTowerBaseId(const std::wstring& inputStr)
{
    std::wstring trimmedOfWhitespace = Trim(inputStr);
    std::wstring trimmedOfLeadingZeros = trimmedOfWhitespace.erase(0, trimmedOfWhitespace.find_first_not_of('0'));
    return trimmedOfLeadingZeros;
}

std::wstring
DucoEngineUtils::RemoveNoneDoveChars(const std::wstring& inputStrOrig)
{
    std::wstring outputString;

    std::wstring inputStr = inputStrOrig;
    size_t index = inputStr.find(L"'s");
    if (index != std::wstring::npos && index == inputStr.length() - 2)
    {
        inputStr = inputStr.substr(0, inputStr.length() - 2);
    }

    for (auto const& it : inputStr)
    {
        switch (it)
        {
        default:
            outputString += it;
            break;
        case '-':
            outputString += ' ';
            break;
        }
    }
    return outputString;
}

std::wstring
DucoEngineUtils::MethodComparisionCharacters(const std::wstring& inputStr)
{
    return RemoveInvalidChars(inputStr, L"()[].;: '");
}

std::wstring
DucoEngineUtils::RemoveAllButValidChars(const std::wstring& inputStr, const std::wstring& validCharacters)
{
    wstring newString;
    for (auto const& it : inputStr)
    {
        if (validCharacters.find_first_of(it) != std::wstring::npos)
        {
            newString += it;
        }
    }

    return newString;
}

std::wstring
DucoEngineUtils::RemoveInvalidChars(const std::wstring& inputStr, const std::wstring& invalidCharacters)
{
    wstring newString;
    for (auto const& it : inputStr)
    {
        if (invalidCharacters.find_first_of(it) == std::wstring::npos)
        {
            newString += it;
        }
    }

    return newString;
}

std::wstring
DucoEngineUtils::RemoveInvalidChars(DatabaseSettings::TPreferredViewWebsite website, const std::wstring& inputStr)
{
    if (inputStr.length() == 0)
        return inputStr;
    else if (inputStr.compare(L"0") == 0)
    {
        return L"";
    }

    std::wstring digitCharacters(numericCharacters);
    std::wstring newString;

    for (unsigned int i = 0; i < inputStr.length(); ++i)
    {
        wchar_t nextChar = inputStr[i];
        if (digitCharacters.find(nextChar) != std::wstring::npos)
        {
            newString.append(1, nextChar);
        }
    }

    return newString;
}
std::wstring
DucoEngineUtils::RemoveInvalidCharsFromTenorWeight(const std::wstring& inputStr)
{
    std::wstring returnValue;
    std::wstring::const_iterator it = inputStr.cbegin();
    while (it != inputStr.cend())
    {
        wchar_t singleChar = (*it);
        if (numericCharacters.find(singleChar) != std::wstring::npos)
        {
            returnValue += singleChar;
        }
        else
        {
            switch (singleChar)
            {
            case L'¼':
            case L'½':
            case L'¾':
            case L' ':
                returnValue += singleChar;
                break;
            case L'-':
            case L'–':
                returnValue += L'-';
                break;

            default:
            {
                if (std::iswalpha(singleChar) != 0)
                {
                    returnValue += singleChar;
                }
                break;
            }
            }
        }
        ++it;
    }
    return DucoEngineUtils::Trim(returnValue);
}

void
DucoEngineUtils::ToLowerCase(const std::string& stringToProcess, std::string& newString)
{
    newString.clear();

    for (unsigned int count(0); count < stringToProcess.length(); ++count)
    {
        char originalChar = stringToProcess.at(count);
        char lowerChar = tolower(originalChar);
        newString.append(&lowerChar, 1);
    }
}

void
DucoEngineUtils::ToLowerCase(const std::wstring& stringToProcess, std::wstring& newString)
{
    newString.clear();

    for (unsigned int count(0); count < stringToProcess.length(); ++count)
    {
        wchar_t originalChar = stringToProcess.at(count);
        wchar_t lowerChar = towlower(originalChar);
        newString.append(&lowerChar, 1);
    }
}

void
DucoEngineUtils::ToUpperCase(const std::wstring& stringToProcess, std::wstring& newString)
{
    newString.clear();

    for (unsigned int count(0); count < stringToProcess.length(); ++count)
    {
        wchar_t lowerChar = towupper(stringToProcess.at(count));
        newString.append(&lowerChar, 1);
    }
}

std::wstring
DucoEngineUtils::ToCapitalised(const std::wstring& string, bool everyWord)
{
    std::wstring newString;
    std::list<std::wstring> tokens;
    DucoEngineUtils::Tokenise<std::wstring>(string, tokens, L" ");

    for (auto const& it : tokens)
    {
        std::wstring token;
        DucoEngineUtils::ToLowerCase(it, token);
        if (token.length() > 0)
        {
            if (newString.length() > 0)
            {
                newString += L" ";
            }
            if (everyWord || newString.length() < 1 || token.length() == 1)
            {
                token[0] = towupper(token[0]);
            }
            newString += token;
        }
    }

    return newString;
}

bool
DucoEngineUtils::MatchString(const std::wstring& str, const std::wstring& substr, bool findSubStr)
{
    if (findSubStr)
    {
        return DucoEngineUtils::FindSubstring(substr, str);
    }
    else
    {
        std::wstring trimmedStr = DucoEngineUtils::Trim(str);
        return DucoEngineUtils::CompareString(trimmedStr, substr);
    }
}

bool
DucoEngineUtils::FindSubstring(const std::wstring& stringToFind, const std::wstring& inString, const std::wstring& charsToIgnore, bool checkBoth, size_t minStringLengthRequired)
{
    size_t position = std::wstring::npos;
    if (charsToIgnore.length() > 0)
    {
        return FindSubstring(RemoveInvalidChars(stringToFind, charsToIgnore), RemoveInvalidChars(inString, charsToIgnore), position, checkBoth, minStringLengthRequired);
    }
    return FindSubstring(stringToFind, inString, position, checkBoth, minStringLengthRequired);
}

bool
DucoEngineUtils::FindSubstring(const std::wstring& stringToFind, const std::wstring& inString, size_t& position, bool checkBoth, size_t minStringLengthRequired, bool convertToLower)
{
    if (minStringLengthRequired > 0 && minStringLengthRequired != std::wstring::npos)
    {
        if (inString.length() < minStringLengthRequired || stringToFind.length() < minStringLengthRequired)
            return false;
    }

    position = std::wstring::npos;

    std::wstring stringToFindLowerCase;
    if (convertToLower)
    {
        ToLowerCase(stringToFind, stringToFindLowerCase);
    }
    else
    {
        stringToFindLowerCase = stringToFind;
    }
    std::wstring inStringLowerCase;
    if (convertToLower)
    {
        ToLowerCase(inString, inStringLowerCase);
    }
    else
    {
        inStringLowerCase = inString;
    }

    position = inStringLowerCase.find(stringToFindLowerCase);

    if (position >= 0 && position < inString.length() && stringToFind.length() > 0)
        return true;

    if (checkBoth)
    {
        return FindSubstring(inStringLowerCase, stringToFindLowerCase, position, false, minStringLengthRequired, false);
    }

    return false;
}

bool
DucoEngineUtils::StartsWith(const std::wstring& stringToFind, const std::wstring& inString)
{
    if (stringToFind.length() == 0)
        return false;
    if (stringToFind.length() > inString.length())
        return false;

    for (size_t i = 0; i < stringToFind.length(); ++i)
    {
        if (stringToFind[i] != inString[i])
        {
            return false;
        }
    }
    return true;
}

void
DucoEngineUtils::InsertString(std::set<std::wstring>& list, const std::wstring& string)
{
    list.insert(DucoEngineUtils::Trim(string));
}

std::wstring
DucoEngineUtils::RemoveBrackets(const std::wstring& str)
{
    size_t startBracket = str.find(L"(");
    std::wstring newString = str;
    while (startBracket != std::wstring::npos)
    {
        size_t endBracket = str.find(L")", startBracket);
        if (endBracket == std::wstring::npos)
        {
            break;
        }

        std::wstring firstHalf = newString.substr(0, startBracket);
        if (endBracket < newString.length())
        {
            std::wstring secondHalf = newString.substr(endBracket + 1);
            newString = firstHalf + secondHalf;
        }
        else
        {
            newString = firstHalf;
        }
        startBracket = newString.find(L"(");
    }
    return DucoEngineUtils::Trim(newString);
}


unsigned int
DucoEngineUtils::ToInteger(const std::wstring& aChar)
{
    return _wtoi(aChar.c_str());
}

unsigned int
DucoEngineUtils::ToInteger(const wchar_t& aChar)
{
    switch (aChar)
    {
        case '1':
            return 1;
        case '2':
            return 2;
        case '3':
            return 3;
        case '4':
            return 4;
        case '5':
            return 5;
        case '6':
            return 6;
        case '7':
            return 7;
        case '8':
            return 8;
        case '9':
            return 9;
        case '0':
            return 10;
        case 'E':
        case 'e':
            return 11;
        case 'T':
        case 't':
            return 12;
        case 'A':
        case 'a':
            return 13;
        case 'B':
        case 'b':
            return 14;
        case 'C':
        case 'c':
            return 15;
        case 'D':
        case 'd':
            return 16;
        case 'F':
        case 'f':
            return 17;
        case 'G':
        case 'g':
            return 18;
        case 'H':
        case 'h':
            return 19;
        case 'J':
        case 'j':
            return 20;
        case 'K':
        case 'k':
            return 21;
        case 'L':
        case 'l':
            return 22;

        default:
            return 0;
    }
}

wchar_t
DucoEngineUtils::ToChar(unsigned int noOfBells)
{
    switch (noOfBells)
    {
        case 1:
            return '1';
        case 2:
            return '2';
        case 3:
            return '3';
        case 4:
            return '4';
        case 5:
            return '5';
        case 6:
            return '6';
        case 7:
            return '7';
        case 8:
            return '8';
        case 9:
            return '9';
        case 10:
            return '0';
        case 11:
            return 'E';
        case 12:
            return 'T';
        case 13:
            return 'A';
        case 14:
            return 'B';
        case 15:
            return 'C';
        case 16:
            return 'D';
        case 17:
            return 'F';
        case 18:
            return 'G';
        case 19:
            return 'H';
        case 20:
            return 'J';
        case 21:
            return 'K';
        case 22:
            return 'L';
        default:
            return 'Z';
    }
}

bool 
DucoEngineUtils::IsEven(unsigned int aCurrentPosition)
{
    if (aCurrentPosition % 2 == 0)
        return true;

    return false;
}

void
DucoEngineUtils::AddError(std::wstring& existingErrorString, const wchar_t* newErrorString)
{
    if (existingErrorString.length() > 0)
    {
        existingErrorString += L", ";
    }
    existingErrorString.append(newErrorString);
}

void
DucoEngineUtils::SwapNumbers(const std::map<Duco::ObjectId, Duco::PealLengthInfo>& oldsIdsFirst, std::multimap<Duco::PealLengthInfo, Duco::ObjectId>& countsFirst)
{
    countsFirst.clear();
    for (auto const& [key, value] : oldsIdsFirst)
    {
        std::pair<Duco::PealLengthInfo, Duco::ObjectId> newObject(value, key);
        countsFirst.insert(newObject);
    }
}

std::wstring
DucoEngineUtils::ToName(unsigned int number)
{
    switch (number)
    {
    case 4:
        return L"Four";
    case 5:
        return L"Five";
    case 6:
        return L"Six";
    case 7:
        return L"Seven";
    case 8:
        return L"Eight";
    case 9:
        return L"Nine";
    case 10:
        return L"Ten";
    case 11:
        return L"Eleven";
    case 12:
        return L"Twelve";
    case 13:
        return L"Thirteen";
    case 14:
        return L"Fourteen";
    case 15:
        return L"Fifteen";
    case 16:
        return L"Sixteen";
    default:
        return ToString(number);
    }
}

std::wstring
DucoEngineUtils::ToString(const double number, bool withSeperators)
{
    std::wostringstream ss;
    if (withSeperators)
    {
        ss.imbue(std::locale(""));
    }
    ss << std::fixed << std::setprecision(5) << number;
    return ss.str();
}

std::wstring
DucoEngineUtils::ToString(int number, bool withSeperators)
{
    std::wostringstream ss;
    if (withSeperators)
    {
        ss.imbue(std::locale(""));
    }
    ss << std::fixed << number;
    return ss.str();
}

std::wstring
DucoEngineUtils::ToString(unsigned int number, bool withSeperators)
{
    std::wostringstream ss;
    if (withSeperators)
    {
        ss.imbue(std::locale(""));
    }
    ss << std::fixed << number;
    return ss.str();
}

std::wstring
DucoEngineUtils::ToString(long number, bool withSeperators)
{
    std::wostringstream ss;
    if (withSeperators)
    {
        ss.imbue(std::locale(""));
    }
    ss << std::fixed << number;
    return ss.str();
}

std::wstring
DucoEngineUtils::ToString(long long number, bool withSeperators)
{
    std::wostringstream ss;
    if (withSeperators)
    {
        ss.imbue(std::locale(""));
    }
    ss << std::fixed << number;
    return ss.str();
}

std::wstring
DucoEngineUtils::ToString(unsigned long long number, bool withSeperators)
{
    std::wostringstream ss;
    if (withSeperators)
    {
        ss.imbue(std::locale(""));
    }
    ss << std::fixed << number;
    return ss.str();
}


float
DucoEngineUtils::ToFloat(const std::wstring& fieldValue)
{
    size_t decimalPointPos = fieldValue.find_first_of(L".");

    std::wstring left = fieldValue.substr(0, decimalPointPos);
    std::wstring right = DucoEngineUtils::Trim(fieldValue.substr(decimalPointPos+1));

    float returnVal = float(_wtoi(right.c_str()));

    for (unsigned int i(0); i < right.length(); ++i)
    {
        returnVal /= 10;
    }

    bool negative(fieldValue.find_first_of(L"-") == 0);
    if (negative)
    {
        left = left.substr(1);
    }

    returnVal += float(_wtoi(left.c_str()));

    if (negative)
    {
        returnVal *= -1;
    }

    return returnVal;
}

std::wstring
DucoEngineUtils::ToString(TConductorType conductorType)
{
    switch (conductorType)
    {
    case ESingleConductor:
        return L"Single conductor";
    case ESilentAndNonConducted:
        return L"Silent and non conducted";
    case EJointlyConducted:
        return L"Jointly conducted";
    default:
        break;
    }
    return L"";
}

void 
DucoEngineUtils::AppendToString(std::wstring& str, unsigned int number)
{
    const int bufferSize (5);
    wchar_t* buffer = new wchar_t[bufferSize];
    swprintf(buffer, 5, L"%d", number);
    std::wstring temp (buffer);
    delete[] buffer;
    str += temp;
}

bool
DucoEngineUtils::IsNumber(const std::wstring& inputStr)
{
    for (auto const& it : inputStr)
    {
        if (!iswdigit(it))
            return false;
    }
    return true;
}

bool
DucoEngineUtils::IsAllUpperCase(const std::wstring& inputStr)
{
    const std::wstring lowerCaseChars(L"abcdefghijlkmnopqrstuvwxyz");
    bool isAllUpperCase(true);
    wstring::const_iterator it = inputStr.begin();
    while (it != inputStr.end() && isAllUpperCase)
    {
        if (lowerCaseChars.find(*it) != std::wstring::npos)
        {
            isAllUpperCase = false;
        }
        ++it;
    }
    return isAllUpperCase;
}

std::wstring
DucoEngineUtils::PrintToRtf(const std::wstring& origString)
{
    std::wstring* copyStr = new std::wstring(L"");
    for (auto const& it : origString)
    {
        switch (it)
        {
            case '\n':
                (*copyStr) += KRtfLineEnd;
                break;

            default:
                (*copyStr) += it;
                break;
        }
    }

    return *copyStr;
}

bool
DucoEngineUtils::NoOfBellsMatch(unsigned int noOfBellsInMethodToMatch, unsigned int noOfBells)
{
    if (noOfBells == -1 || noOfBellsInMethodToMatch == -1)
        return false;

    if (noOfBellsInMethodToMatch == noOfBells)
        return true;

    if (noOfBells % 2 == 0 && (noOfBellsInMethodToMatch == (noOfBells - 1)))
        return true;

    if (noOfBells <= 5 && (noOfBellsInMethodToMatch == (noOfBells - 1)))
        return true;

    return false;
}

unsigned int
DucoEngineUtils::MaxNumberOfBells()
{
    return KMaxNoOfBellsInTower;
}

std::wstring
DucoEngineUtils::PadStringWithZeros(const std::wstring& inputString) 
{
    try
    {
        if (inputString.length() > 0)
        {
            std::wstring newString;
            unsigned int numberOfMethods = _wtoi (inputString.c_str());
            if (numberOfMethods == 0)
            {
                return L"00000";
            }
            if (numberOfMethods < 10000)
            {
                newString.append(L"0");
            }
            if (numberOfMethods < 1000)
            {
                newString.append(L"0");
            }
            if (numberOfMethods < 100)
            {
                newString.append(L"0");
            }
            if (numberOfMethods < 10)
            {
                newString.append(L"0");
            }
            newString += inputString;
            return newString;
        }
    }
    catch (exception*)
    {
    }
    return inputString;
}

bool
DucoEngineUtils::MethodNameContainsSpliced(const std::wstring& name)
{
    bool spliced = DucoEngineUtils::FindSubstring(L"spliced", name);
    if (!spliced)
    {
        spliced = DucoEngineUtils::FindSubstring(L"method", name);
    }
    if (!spliced)
    {
        spliced = DucoEngineUtils::FindSubstring(L"multi", name);
    }
    if (!spliced)
    {
        spliced = DucoEngineUtils::Trim(name).length() == 0;
    }
    return spliced;
}

bool
DucoEngineUtils::RemoveMethodCount(std::wstring& name, std::wstring& bracketedText)
{
    bracketedText.clear();
    size_t openBrackets = name.find_first_of('(');
    size_t endBrackets = name.find_first_of(')', openBrackets);
    if (openBrackets == std::wstring::npos && endBrackets == std::wstring::npos)
        return false;

    std::wstring methods = name.substr(openBrackets + 1, endBrackets - openBrackets - 1);
    bracketedText = methods.c_str();

    std::wstring firstPart;
    if (openBrackets != std::wstring::npos && openBrackets > 0)
    {
        firstPart = DucoEngineUtils::Trim(name.substr(0, openBrackets - 1));
    }

    std::wstring lastPart;
    if (endBrackets != std::wstring::npos && endBrackets < name.length())
    {
        lastPart = DucoEngineUtils::Trim(name.substr(endBrackets + 1));
    }

    if (firstPart.length() > 0 && lastPart.length() > 0)
    {
        name = firstPart + L" " + lastPart;
    }
    else 
    {
        name = firstPart + lastPart;
    }

    return true;
}

unsigned int
DucoEngineUtils::Count(const std::wstring& stringToSearch, wchar_t charToCount)
{
    unsigned int count(0);
    for (auto const& it : stringToSearch)
    {
        if (it == charToCount)
        {
            ++count;
        }
    }

    return count;
}

void
DucoEngineUtils::ConvertFromMethodMaster(const std::wstring& notationToConvert, std::wstring& convertedNotation)
{
    convertedNotation.clear();
    std::wstring::const_iterator it = notationToConvert.begin();
    while (it != notationToConvert.end())
    {
        switch ((*it))
        {
            case 'L':
            case 'l':
            {
                ++it;
                if (it != notationToConvert.end() && ((*it) == 'H' || (*it) == 'h'))
                {
                    convertedNotation.push_back('-');
                    convertedNotation.insert(0, L"&");
                }
            }
                break;

            case '-':
                convertedNotation.push_back('X');
                break;

            default:
                convertedNotation.push_back(toupper(*it));
                break;
        }
        ++it;
    }
}

bool
DucoEngineUtils::ConductorIndicator(const std::wstring& conductorStr)
{
    std::wstring condStr;
    DucoEngineUtils::ToLowerCase(DucoEngineUtils::Trim(conductorStr), condStr);

    if (condStr.compare(L"c") == 0 ||
        condStr.compare(L"cond") == 0 ||
        condStr.compare(L"conductor") == 0 ||
        condStr.compare(L"cond.") == 0)
    {
        return true;
    }
    return false;
}

std::wstring
DucoEngineUtils::CheckRingerNameForConductor(const std::wstring& originalName, bool& conductor)
{
    conductor = false;
    std::wstring returnVal;

    int bracketCount (0);
    std::vector<std::wstring> bracketContent;

    for (auto const& it : originalName | std::views::reverse)
    {
        switch (it)
        {
            case ']':
            case ')':
            case '}':
                {
                    std::wstring newBracketContent;
                    newBracketContent += it;
                    bracketContent.insert(bracketContent.end(), newBracketContent);
                    ++bracketCount;
                }
                break;

            case '[':
            case '(':
            case '{':
                if (bracketCount > 0)
                {
                    bool conductorThisSetOfbrackets = false;
                    if (ConductorIndicator(bracketContent[bracketCount]))
                    {
                        conductor = true;
                        conductorThisSetOfbrackets = true;
                        bracketContent.pop_back();
                    }
                    else
                    {
                        bracketContent[bracketCount] = it + bracketContent[bracketCount] + bracketContent[bracketCount - 1];
                    }
                    --bracketCount;
                    if (conductorThisSetOfbrackets)
                    {
                        bracketContent.pop_back();
                    }
                    else
                    {
                        std::wstring last = *bracketContent.rbegin();
                        bracketContent.pop_back();
                        bracketContent.pop_back();
                        bracketContent.insert(bracketContent.end(), last);
                    }
                }
                else
                {
                    returnVal.insert(returnVal.begin(), it);
                }
                break;
            
            default:
                if (bracketCount > 0)
                {
                    if ((bracketCount + 1) > bracketContent.size())
                    {
                        bracketContent.insert(bracketContent.end(), L"");
                    }
                    bracketContent[bracketCount].insert(bracketContent[bracketCount].begin(), it);
                }
                else
                {
                    returnVal.insert(returnVal.begin(), it);
                }
                break;
        }
    }
    if (conductor)
    {
        for (auto const& it : bracketContent)
        {
            if (bracketCount == 0)
            {
                returnVal += it;
            }
            else
            {
                --bracketCount;
            }
        }
    }
    else
    {
        returnVal = originalName;
    }
    return DucoEngineUtils::Trim(returnVal);
}

void
DucoEngineUtils::SplitRingerName(const std::wstring& originalNameStr, std::wstring& forename, std::wstring& surname, bool& isConductor)
{
    isConductor = false;
    forename = L"";
    surname = L"";

    std::wstring originalName = DucoEngineUtils::Trim(DucoEngineUtils::CheckRingerNameForConductor(originalNameStr, isConductor));

    std::wstring textInBrackets;
    std::wstring::size_type startOfBrackets = originalName.find_last_of('(');
    if (startOfBrackets != std::wstring::npos)
    {
        std::wstring::size_type endOfBrackets = originalName.find_last_of(')');
        if (endOfBrackets != std::wstring::npos)
        {
            textInBrackets = originalName.substr(startOfBrackets, endOfBrackets- startOfBrackets+1);
            originalName = DucoEngineUtils::Trim(originalName.substr(0, startOfBrackets - 1) + originalName.substr(endOfBrackets + 1));
        }
    }

    size_t seperatorChar = originalName.find(L",");
    if (seperatorChar != std::wstring::npos)
    {
        surname = DucoEngineUtils::Trim(originalName.substr(0, seperatorChar));
        forename = DucoEngineUtils::Trim(originalName.substr(seperatorChar+1));
    }
    else
    {
        seperatorChar = originalName.rfind(L" ");
        surname = DucoEngineUtils::Trim(originalName.substr(seperatorChar+1));
        if (seperatorChar != std::wstring::npos)
        {
            forename = DucoEngineUtils::Trim(originalName.substr(0,seperatorChar));
        }
    }
    if (textInBrackets.length() > 0)
    {
        forename.append(L" ");
        forename.append(textInBrackets);
    }
}

bool
DucoEngineUtils::SplitTowerName(const std::wstring& originalFullName, std::wstring& name, std::wstring& townCity, std::wstring& county)
{
    name.clear();
    townCity.clear();
    county.clear();

    if (originalFullName.length() == 0)
        return false;

    std::wstring fullNameWithoutDedication;
    size_t startOfName = originalFullName.find_first_of('(');
    if (startOfName != std::wstring::npos)
    {
        size_t endOfName = originalFullName.find_first_of(')', startOfName);
        if (endOfName != std::wstring::npos)
        {
            name = originalFullName.substr(startOfName + 1, endOfName - startOfName - 1);
            fullNameWithoutDedication = DucoEngineUtils::Trim(originalFullName.substr(0, startOfName));
        }
    }
    if (fullNameWithoutDedication.length() == 0)
    {
        fullNameWithoutDedication = originalFullName;
    }

    size_t endOfTown = fullNameWithoutDedication.find_first_of(L", .");
    if (endOfTown == std::wstring::npos)
    {
        townCity = fullNameWithoutDedication;
    }
    else
    {
        townCity = fullNameWithoutDedication.substr(0, endOfTown);
        fullNameWithoutDedication = fullNameWithoutDedication.substr(endOfTown+1);
        size_t endOfCounty = fullNameWithoutDedication.find_last_of(L".");
        if (endOfCounty == std::wstring::npos)
        {
            county = fullNameWithoutDedication;
        }
        else
        {
            county = fullNameWithoutDedication.substr(0, endOfCounty);
        }
    }

    name = DucoEngineUtils::Trim(name);
    townCity = DucoEngineUtils::Trim(townCity);
    county = DucoEngineUtils::Trim(county);

    return true;
}

std::wstring
DucoEngineUtils::RemovePunctuationFromFirstNames(const std::wstring& firstNames)
{
    std::wstring firstNamesWithoutPunctuation;

    std::wstring charactersToRemove (L".,'`");
    
    for (auto const& it : firstNames)
    {
        if (charactersToRemove.find(it) == std::wstring::npos)
        {
            firstNamesWithoutPunctuation += it;
        }
    }
    return firstNamesWithoutPunctuation;
}

std::wstring
DucoEngineUtils::GetInitialsFromName(const std::wstring& firstNames, bool excludeLast)
{
    std::wstring response = L"";
    std::wstring trimmed = DucoEngineUtils::Trim(firstNames);
    while (trimmed.length() > 0)
    {
        size_t space = trimmed.find_first_of(L" ");
        if (space != std::wstring::npos)
        {
            response += trimmed[0];
            trimmed = trimmed.substr(space + 1);
        }
        else
        {
            if (!excludeLast)
            {
                response += trimmed[0];
            }
            trimmed = L"";
        }
    }
    return response;
}

void
DucoEngineUtils::ToSet(const std::map<unsigned int, PealRingerData>& ringerIds, std::set<Duco::ObjectId>& ids)
{
    ids.clear();
    for (auto const& [key, value] : ringerIds)
    {
        ids.insert(value.RingerId());
    }
}

std::wstring
DucoEngineUtils::GetInitials(const std::wstring& str)
{
    std::wstring temp = Trim(str);
    std::wstring returnVal;

    size_t index = 0;
    while (index != std::wstring::npos && index < temp.length())
    {
        returnVal += (temp[index]);
        index = temp.find_first_of(L" ", index + 1);
        if (index != std::wstring::npos)
            ++index;
        if (index > temp.length())
            index = std::wstring::npos;
    }

    return returnVal;
}

std::wstring
DucoEngineUtils::FormatTowerName(const std::wstring& name, const std::wstring& county, const std::wstring& dedication)
{
    std::wstring fullTowerName(name);
    fullTowerName += L", ";
    fullTowerName += county;
    fullTowerName += L". (";
    fullTowerName += dedication;
    fullTowerName += L")";
    return fullTowerName;
}

std::wstring
DucoEngineUtils::FormatRingName(const std::wstring& ringName, const std::wstring& tenorWeight, unsigned int noOfBells)
{
    std::wstring fullName;
    if (ringName.length() > 0)
    {
        fullName = ringName + L", ";
    }
    DucoEngineUtils::AppendToString(fullName, noOfBells);
    fullName += L" bells.";
    fullName += L" (" + tenorWeight + L") ";

    return fullName;
}

std::wstring
DucoEngineUtils::FormatRingName(const Duco::Ring& theRing)
{
    return DucoEngineUtils::FormatRingName(theRing.Name(), theRing.TenorWeight(), theRing.NoOfBells());
}

std::list<std::set<Duco::ObjectId> >::iterator
DucoEngineUtils::FindId(std::list<std::set<Duco::ObjectId> >& linkedTowerIds, const Duco::ObjectId& idToFind)
{
    if (idToFind.ValidId())
    {
        std::list<std::set<Duco::ObjectId> >::iterator it = linkedTowerIds.begin();
        while (it != linkedTowerIds.end())
        {
            if ((*it).find(idToFind) != (*it).end())
            {
                return it;
            }

            ++it;
        }
    }
    return linkedTowerIds.end();
}

std::wstring
DucoEngineUtils::EncodeString(const std::wstring& original)
{
    std::wstring newString;
    for (auto const& it : original)
    {
        wchar_t nextChar = it;
        switch (nextChar)
        {
        case '\'':
        case '\\':
        case '\"':
            newString += L"\\";

        default:
            newString += it;
            break;

        case '\r':
            newString += L"\\r";
            break;
        case '\n':
            newString += L"\\n";
            break;
        }
    }
    return newString;
}


bool
DucoEngineUtils::CompareTenorWeight(std::wstring firstTenor, std::wstring secondTenor)
{
    if (firstTenor.length() == secondTenor.length())
    {
        std::replace(firstTenor.begin(), firstTenor.end(), L'–', L'-');
        std::replace(secondTenor.begin(), secondTenor.end(), L'–', L'-');

        return CompareString(firstTenor, secondTenor, true, false);
    }
    else if (firstTenor.length() > 0 && secondTenor.length() > 0) // && firstTenor.length() != secondTenor.length())
    {
        //  just try rounding down the tenor weight, e.g. 25-2-6, but the person entered 26.
        // tenors have to be different lengths, else they should be identical
        int firstPartOfTenorWeightOne = DucoEngineUtils::ToInteger(firstTenor);
        int firstPartOfTenorWeightTwo = DucoEngineUtils::ToInteger(secondTenor);
        if (abs(firstPartOfTenorWeightOne - firstPartOfTenorWeightTwo) < 2)
        {
            return true;
        }
    }
    return false;
}

std::wstring
DucoEngineUtils::ReplaceTenorKeySymbols(const std::wstring& tenorKey)
{
    std::wstring tenorKeyToSave = tenorKey;

    if (tenorKeyToSave.length() > 2)
    {
        tenorKeyToSave = tenorKeyToSave.substr(0, 2);
    }
    if (tenorKeyToSave.length() >= 2)
    {
        wchar_t key = tenorKeyToSave[1];
        switch (key)
        {
        case 'b':
        case L'♭':
            tenorKeyToSave[1] = L'♭';
            break;
        case '#':
        case L'♯':
            tenorKeyToSave[1] = L'♯';
            break;
        default:
            tenorKeyToSave = tenorKeyToSave.substr(0, 1);
        }
    }
    if (tenorKeyToSave.length() >= 1)
    {
        wchar_t note = tenorKeyToSave[0];
        note = toupper(note);
        switch (note)
        {
        case 'A':
        case 'B':
        case 'C':
        case 'D':
        case 'E':
        case 'F':
        case 'G':
            tenorKeyToSave[0] = note;
            break;
        default:
            tenorKeyToSave.clear();
        }
    }
    return tenorKeyToSave;
}


bool 
DucoEngineUtils::CompareString(const std::wstring& string1, const std::wstring& string2, bool ignoreCase, bool allowEmptyString)
{
    if (!allowEmptyString && (string1.length() <= 0 || string1.length() <= 0))
    {
        return false;
    }
    if (string1.length() != string2.length())
    {
        return false;
    }

    std::wstring::const_iterator strOneIt = string1.begin();
    std::wstring::const_iterator strTwoIt = string2.begin();

    while (strOneIt != string1.end() && strTwoIt != string2.end())
    {
        wchar_t nextCharStr1 = *strOneIt;
        wchar_t nextCharStr2 = *strTwoIt;

        if (ignoreCase)
        {
            nextCharStr1 = tolower(nextCharStr1);
            nextCharStr2 = tolower(nextCharStr2);
        }
        if (nextCharStr1 != nextCharStr2)
        {
            return false;
        }
        strOneIt++;
        strTwoIt++;
    }

    return true;
}

bool
DucoEngineUtils::MatchingRingingWorldReference(const std::wstring& lhs, const std::wstring& rhs)
{
    std::wstring lhsIssue = DucoEngineUtils::RingingWorldIssue(lhs);
    std::wstring lhsPage = DucoEngineUtils::RingingWorldPage(lhs);
    if (lhsIssue.length() == 0 && lhsPage.length() == 0)
    {
        return false;
    }
    std::wstring rhsPage = DucoEngineUtils::RingingWorldPage(rhs);
    std::wstring rhsIssue = DucoEngineUtils::RingingWorldIssue(rhs);
    if (rhsIssue.length() == 0 && rhsPage.length() == 0)
    {
        return false;
    }
    bool issueMatch = lhsIssue.length() > 0 && rhsIssue.length() > 0 && lhsIssue.compare(rhsIssue) == 0;
    bool pageMatch = lhsPage.length() > 0 && rhsPage.length() > 0 && lhsPage.compare(rhsPage) == 0;
    if (issueMatch && pageMatch)
    {
        return true;
    }
    else if (issueMatch && (lhsPage.length() == 0 || rhsPage.length() == 0))
    {
        return true;
    }

    return false;
}

std::wstring
DucoEngineUtils::RingingWorldIssue(const std::wstring& fullReference)
{
    std::wstring fullIssue = L"";

    size_t index = fullReference.find_first_of('.');
    if (index == std::wstring::npos)
    {
        fullIssue = fullReference;
    }
    fullIssue = fullReference.substr(0, index);

    while (fullIssue.length() > 0 && fullIssue.at(0) == '0')
    {
        fullIssue = fullIssue.substr(1);
    }

    return fullIssue;
}

std::wstring 
DucoEngineUtils::RingingWorldPage(const std::wstring& fullReference)
{
    size_t index = fullReference.find_first_of('.');
    if (index == std::wstring::npos)
    {
        return L"";
    }
    std::wstring pageNumberStr = fullReference.substr(index + 1);
    while (pageNumberStr.length() > 0 && pageNumberStr.at(0) == '0')
    {
        pageNumberStr = pageNumberStr.substr(1);
    }
    return pageNumberStr;
}

