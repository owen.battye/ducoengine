#include "SearchDuplicateObject.h"

#include "Association.h"
#include "Method.h"
#include "MethodSeries.h"
#include "Peal.h"
#include "Ringer.h"
#include "Tower.h"
#include "RingingDatabase.h"
#include "AssociationDatabase.h"
#include "MethodDatabase.h"
#include "MethodSeriesDatabase.h"
#include "PealDatabase.h"
#include "RingerDatabase.h"
#include "TowerDatabase.h"

using namespace Duco;

TSearchDuplicateObject::TSearchDuplicateObject(bool _includeRingers)
    : TSearchValidationArgument(EDuplicate), includeRingers(_includeRingers)
{
}

TSearchDuplicateObject::~TSearchDuplicateObject()
{

}

Duco::TFieldType 
TSearchDuplicateObject::FieldType() const
{
    return TFieldType::EDuplicateField;
}

bool
TSearchDuplicateObject::Match(const Duco::Ringer& ringer, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const
{
    if (!includeRingers)
    {
        return false;
    }
    bool duplicateFound = false;
    Duco::Ringer& theRingerToTest = const_cast<Duco::Ringer&>(ringer);

    for (auto const& ringerIt : compareIds)
    {
        if (ringer.Id() != ringerIt && !BothAlreadyInList(ringer.Id(), ringerIt))
        {
            const Duco::Ringer* ringerInDatabase = database.RingersDatabase().FindRinger(ringerIt);
            if (ringer.Duplicate(*ringerInDatabase, database))
            {
                theRingerToTest.SetError(ERinger_DuplicateName);
                possibleDuplicateIds.insert(theRingerToTest.Id());
                possibleDuplicateIds.insert(ringerInDatabase->Id());
                duplicateFound = true;

                Duco::Ringer& theRingerInDatabase = const_cast<Duco::Ringer&>(*ringerInDatabase);
                theRingerInDatabase.SetError(ERinger_DuplicateName);
            }
        }
    }

    return duplicateFound;
}

bool
TSearchDuplicateObject::Match(const Duco::Tower& tower, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const
{
    bool duplicateFound = false;
    Duco::Tower& theTowerToTest = const_cast<Duco::Tower&>(tower);

    for (auto const& towerIt : compareIds)
    {
        if (tower.Id() != towerIt && !BothAlreadyInList(tower.Id(), towerIt))
        {
            const Duco::Tower* towerInDatabase = database.TowersDatabase().FindTower(towerIt);
            if (tower.Duplicate(*towerInDatabase, database))
            {
                theTowerToTest.SetError(ETower_DuplicateTower);
                possibleDuplicateIds.insert(theTowerToTest.Id());
                possibleDuplicateIds.insert(towerInDatabase->Id());
                duplicateFound = true;

                Duco::Tower& theTowerInDatabase = const_cast<Duco::Tower&>(*towerInDatabase);
                theTowerInDatabase.SetError(ETower_DuplicateTower);
            }
        }
    }

    return duplicateFound;
}

bool 
TSearchDuplicateObject::Match(const Duco::Method& method, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const
{
    bool duplicateFound = false;
    Duco::Method& theMethodToTest = const_cast<Duco::Method&>(method);

    for (auto const& methodIt : compareIds)
    {
        if (method.Id() != methodIt && !BothAlreadyInList(method.Id(), methodIt))
        {
            const Duco::Method* methodInDatabase = database.MethodsDatabase().FindMethod(methodIt);
            if (method.Duplicate(*methodInDatabase, database))
            {
                theMethodToTest.SetError(EMethod_DuplicateMethod);
                possibleDuplicateIds.insert(theMethodToTest.Id());
                possibleDuplicateIds.insert(methodInDatabase->Id());
                duplicateFound = true;

                Duco::Method& theMethodInDatabase = const_cast<Duco::Method&>(*methodInDatabase);
                theMethodInDatabase.SetError(EMethod_DuplicateMethod);
            }
        }
    }

    return duplicateFound;
}

bool 
TSearchDuplicateObject::Match(const Duco::Peal& peal, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const
{
    bool duplicateFound = false;
    Duco::Peal& thePealToTest = const_cast<Duco::Peal&>(peal);

    for (auto const& pealIt : compareIds)
    {
        if (peal.Id() != pealIt && !BothAlreadyInList(peal.Id(), pealIt))
        {
            const Duco::Peal* pealInDatabase = database.PealsDatabase().FindPeal(pealIt);
            if (peal.Duplicate(*pealInDatabase, database))
            {
                thePealToTest.SetError(EPeal_DuplicatePeal);
                possibleDuplicateIds.insert(thePealToTest.Id());
                possibleDuplicateIds.insert(pealInDatabase->Id());
                duplicateFound = true;

                Duco::Peal& thePealInDatabase = const_cast<Duco::Peal&>(*pealInDatabase);
                thePealInDatabase.SetError(EPeal_DuplicatePeal);
            }
        }
    }

    return duplicateFound;
}

bool
TSearchDuplicateObject::Match(const Duco::MethodSeries& series, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const
{
    bool duplicateFound = false;
    Duco::MethodSeries& theSeriesToTest = const_cast<Duco::MethodSeries&>(series);

    for (auto const& seriesIt : compareIds)
    {
        if (series.Id() != seriesIt && !BothAlreadyInList(series.Id(), seriesIt))
        {
            const Duco::MethodSeries* seriesInDatabase = database.MethodSeriesDatabase().FindMethodSeries(seriesIt);
            if (series.Duplicate(*seriesInDatabase, database))
            {
                theSeriesToTest.SetError(ESeries_DuplicateSeries);
                possibleDuplicateIds.insert(theSeriesToTest.Id());
                possibleDuplicateIds.insert(seriesInDatabase->Id());
                duplicateFound = true;

                Duco::MethodSeries& theSeriesInDatabase = const_cast<Duco::MethodSeries&>(*seriesInDatabase);
                theSeriesInDatabase.SetError(ESeries_DuplicateSeries);
            }
        }
    }

    return duplicateFound;
}

bool
TSearchDuplicateObject::Match(const Duco::Association& association, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const
{
    bool duplicateFound = false;
    Duco::Association& theAssociationToTest = const_cast<Duco::Association&>(association);

    for (auto const& associationIt : compareIds)
    {
        if (association.Id() != associationIt && !BothAlreadyInList(association.Id(), associationIt))
        {
            if (association.Duplicate(*database.AssociationsDatabase().FindAssociation(associationIt), database))
            {
                theAssociationToTest.SetError(EAssociation_DuplicateAssociation);
                possibleDuplicateIds.insert(theAssociationToTest.Id());
                duplicateFound = true;
            }
        }
    }

    return duplicateFound;
}

std::set<Duco::ObjectId>
TSearchDuplicateObject::ErrorIds() const
{
    return possibleDuplicateIds;
}

void
TSearchDuplicateObject::Reset()
{
    possibleDuplicateIds.clear();
}

bool
TSearchDuplicateObject::BothAlreadyInList(const Duco::ObjectId& lhs, const Duco::ObjectId& rhs) const
{
    return possibleDuplicateIds.contains(lhs) && possibleDuplicateIds.contains(rhs);
}
