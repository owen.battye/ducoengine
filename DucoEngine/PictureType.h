#ifndef __PICTURETYPE_H__
#define __PICTURETYPE_H__

#include "PictureType.h"

namespace Duco
{
    enum PictureType
    {
        EUnknown,
        EJpeg,
        EPng,
        EGif,
        EBmp
    };
}

#endif //!__PICTURETYPE_H__