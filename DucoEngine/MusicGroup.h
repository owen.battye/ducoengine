#ifndef __MUSICGROUP_H__
#define __MUSICGROUP_H__

#include <string>
#include <vector>
#include "DucoEngineCommon.h"
#include "MusicObject.h"

namespace Duco
{
    class Change;

class MusicGroup : public MusicObject
{
public:
    DllExport MusicGroup(const std::wstring& theNames);
    DllExport ~MusicGroup();

    // Accessors
    DllExport std::wstring Name(bool showAll) const;
    std::wstring Print() const;

    //Settors

    //Other
    DllExport bool Match(Duco::Change& change);

protected:
    bool AddMusicObject(const std::wstring& newObjectStr);
    bool ContainsSubCount() const;

private:
    std::vector<MusicObject*>   objects;
    std::wstring*                name;
};

}

#endif //!__MUSICGROUP_H__
