#ifndef __SEARCHFIELDIDARGUMENT_H__
#define __SEARCHFIELDIDARGUMENT_H__
#include "SearchArgument.h"
#include "DucoEngineCommon.h"
#include "ObjectId.h"

namespace Duco
{
    class Peal;
    class Ringer;

class TSearchFieldIdArgument : public TSearchArgument
{
public:
    DllExport explicit TSearchFieldIdArgument(Duco::TSearchFieldId fieldId, const Duco::ObjectId& newFieldValue, Duco::TSearchType newSearchType = EEqualTo);
    virtual bool Match(const Duco::Peal& object, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Ringer& object, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Tower& object, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::MethodSeries& object, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Composition& object, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Association& object, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Picture& object, const Duco::RingingDatabase& database) const;
    Duco::TFieldType FieldType() const;
 
protected:
    virtual bool CheckRingerIds(const Duco::Peal& peal) const;
    bool CheckStrapperId(const Duco::Peal& peal) const;
    bool CheckSeriesContainsMethod(const Duco::Peal& peal, const Duco::RingingDatabase& database) const;

protected:
    const Duco::ObjectId       fieldValue;
    const Duco::TSearchType    searchType;
};

}
#endif //__SEARCHFIELDIFARGUMENT_H__
