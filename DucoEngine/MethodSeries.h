#ifndef __METHODSERIES_H__
#define __METHODSERIES_H__

#include <string>
#include <set>
#include <map>
#include "RingingObject.h"
#include "DucoTypes.h"

namespace Duco
{
    class DatabaseWriter;
    class DatabaseReader;
    class MethodDatabase;
    class PerformanceDate;
    class PealDatabase;
    class PealLengthInfo;

class MethodSeries : public RingingObject
{
    friend class RingingDatabase;

public:
    DllExport MethodSeries(const Duco::ObjectId& newId, const std::wstring& newName, unsigned int newNoOfMethods);
    DllExport MethodSeries(const MethodSeries& other);
    DllExport MethodSeries(const Duco::ObjectId& newId, const MethodSeries& other);
    DllExport MethodSeries(DatabaseReader& reader, unsigned int databaseVersion);
    DllExport MethodSeries();
    DllExport ~MethodSeries();

    inline const std::wstring& Name() const;
    virtual std::wstring FullNameForSort(bool unusedSetting) const;

    inline size_t NoOfMethods() const;
    inline size_t NoOfExistingMethods() const;
    inline size_t NoOfUndefinedMethods() const;
    inline unsigned int NoOfBells() const;
    inline bool NoOfBellsSet() const;
    inline const std::set<Duco::ObjectId>& Methods() const;
    inline bool SingleOrder() const;

    inline void SetName(const std::wstring& newName);
    inline void SetNoOfMethods(unsigned int newNoOfMethods);
    DllExport bool SetSingleOrder(bool singleOrder, const Duco::RingingDatabase& database);
    DllExport bool SetSingleOrder(bool singleOrder, const Duco::MethodDatabase& database);

    DllExport bool AddMethod(const Duco::ObjectId& methodId, const Duco::RingingDatabase& database);
    DllExport bool AddMethod(const Duco::ObjectId& methodId, const Duco::MethodDatabase& database);
    DllExport bool AddMethods(const std::set<Duco::ObjectId>& methodIds, const Duco::RingingDatabase& database);
    DllExport void RemoveMethod(const Duco::ObjectId& methodId);

    DllExport virtual Duco::TObjectType ObjectType() const;
    DllExport virtual bool Duplicate(const Duco::RingingObject& other, const Duco::RingingDatabase& database) const;

    DllExport bool ContainsMethod(const Duco::ObjectId& methodId) const;
    bool ContainsOneMethodFromList(const std::set<Duco::ObjectId>& methodIds) const;
    bool ReplaceMethods(const std::map<Duco::ObjectId, Duco::ObjectId>& changedMethodIds);
    DllExport bool SeriesCompletedDate(const RingingDatabase& database, Duco::PerformanceDate& completedDate, size_t& unrungMethods, Duco::PealLengthInfo& allPealsInfo) const;
    DllExport bool Valid(const Duco::RingingDatabase& ringingDb, bool warningFatal, bool clearBeforeStart) const;
    DllExport bool Valid(const Duco::MethodDatabase& methodDb, bool warningFatal, bool clearBeforeStart) const;
    DllExport std::wstring MethodNames(const Duco::MethodDatabase& methodDb) const;
    DllExport std::wstring ErrorString(const Duco::DatabaseSettings& settings, bool showWarnings) const;
    void SetError(TMethodSeriesErrorCodes code);

    // Operators
    DllExport bool operator==(const Duco::RingingObject& other) const;
    DllExport bool operator!=(const Duco::RingingObject& other) const;
    DllExport Duco::MethodSeries& operator=(const Duco::MethodSeries& other);

    // Internal/Externalisers
    DllExport DatabaseWriter& Externalise(Duco::DatabaseWriter& writer) const;
    std::wostream& Print(std::wostream& stream, const Duco::RingingDatabase& database) const;
#ifndef __ANDROID__
    virtual void ExportToXml(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& parent, bool exportDucoObjects) const;
#endif

protected:
    DatabaseReader& Internalise(Duco::DatabaseReader& reader, unsigned int databaseVersion);

private:
    std::wstring     name;
    size_t          noOfMethods;
    unsigned int    noOfBells;
    bool            singleOrder;

    std::set<Duco::ObjectId> methods;
};

const std::set<Duco::ObjectId>&
MethodSeries::Methods() const
{
    return methods;
}

const std::wstring&
MethodSeries::Name() const
{
    return name;
}

unsigned int
MethodSeries::NoOfBells() const
{
    return noOfBells;
}

bool
MethodSeries::NoOfBellsSet() const
{
    return noOfBells > 0 && noOfBells != -1;
}

size_t
MethodSeries::NoOfExistingMethods() const
{
    return methods.size();
}

size_t
MethodSeries::NoOfMethods() const
{
    return noOfMethods;
}

size_t
MethodSeries::NoOfUndefinedMethods() const
{
    return noOfMethods - methods.size();
}

void
MethodSeries::SetName(const std::wstring& newName)
{
    name = newName;
}

void
MethodSeries::SetNoOfMethods(unsigned int newNoOfMethods)
{
    noOfMethods = newNoOfMethods;
}

bool
MethodSeries::SingleOrder() const
{
    return singleOrder;
}

}

#endif //!__METHODSERIES_H__
