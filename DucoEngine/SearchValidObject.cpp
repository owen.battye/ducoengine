#include "SearchValidObject.h"

#include "Peal.h"
#include "Tower.h"
#include "Ringer.h"
#include "Method.h"
#include "MethodSeries.h"

using namespace Duco;

TSearchValidObject::TSearchValidObject(bool newIncludeWarnings)
: TSearchValidationArgument(ENotValid), includeWarnings(newIncludeWarnings)
{

}

TSearchValidObject::~TSearchValidObject()
{

}

Duco::TFieldType 
TSearchValidObject::FieldType() const
{
    return TFieldType::EInvalidField;
}

std::set<Duco::ObjectId>
TSearchValidObject::ErrorIds() const
{
    return std::set<Duco::ObjectId>();
}

bool
TSearchValidObject::Match(const Duco::Ringer& ringer, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const
{
    if (compareIds.find(ringer.Id()) != compareIds.end() || compareIds.size() == 0)
    {
        switch (fieldId)
        {
        case ENotValid:
            return !ringer.Valid(database, includeWarnings, false);

        default:
            break;
        }
    }

    return false;
}

bool
TSearchValidObject::Match(const Duco::Tower& tower, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const
{
    if (compareIds.find(tower.Id()) != compareIds.end() || compareIds.size() == 0)
    {
        switch (fieldId)
        {
        case ENotValid:
            return !tower.Valid(database, includeWarnings, false);

        default:
            break;
        }
    }
    return false;
}

bool 
TSearchValidObject::Match(const Duco::Method& method, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const
{
    if (compareIds.find(method.Id()) != compareIds.end() || compareIds.size() == 0)
    {
        switch (fieldId)
        {
        case ENotValid:
            return !method.Valid(database, includeWarnings, false);

        default:
            break;
        }
    }
    return false;
}


bool 
TSearchValidObject::Match(const Duco::Peal& peal, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const
{
    if (compareIds.find(peal.Id()) != compareIds.end() || compareIds.size() == 0)
    {
        switch (fieldId)
        {
        case ENotValid:
            return !peal.Valid(database, includeWarnings, false);

        default:
            break;
        }
    }
    return false;
}

bool
TSearchValidObject::Match(const Duco::MethodSeries& series, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const
{
    if (compareIds.find(series.Id()) != compareIds.end() || compareIds.size() == 0)
    {
        switch (fieldId)
        {
        case ENotValid:
            return !series.Valid(database, includeWarnings, false);

        default:
            break;
        }
    }
    return false;
}
