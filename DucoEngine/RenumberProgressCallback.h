#ifndef __RENUMBERPROGRESSCALLBACK_H__
#define __RENUMBERPROGRESSCALLBACK_H__

#include <cstddef>

namespace Duco
{
    class ObjectId;

class RenumberProgressCallback
{
public:
	enum TRenumberStage
    {
        ENotStarted = 0,
        ERenumberPeals,
        ESortingPeals,
        EReindexTowers,
        ERenumberTowers,
        ERebuildTowers,
        ERebuildRings,
        EReindexMethods,
        ERenumberMethods,
        ERebuildMethods,
        ERebuildMethodSeries,
        EReindexRingers,
        ERenumberRingers,
        ERebuildRingers,
        EReindexMethodSeries,
        ERenumberMethodsSeries,
        ERebuildMethodsSeries,
        EReindexCompositions,
        ERenumberCompositions,
        ERebuildCompositions,
        EReindexAssociations,
        ERenumberAssociations,
        ERebuildAssociations,
        EReindexPictures,
        ERenumberPictures,
        ERebuildPictures,

        ENumberOfRebuildStages
    };
    virtual void RenumberInitialised(RenumberProgressCallback::TRenumberStage newStage) = 0;
    virtual void RenumberStep(size_t objectId, size_t total) = 0;
    virtual void RenumberComplete() = 0;
};

} // end namespace Duco

#endif //!__RENUMBERPROGRESSCALLBACK_H__
