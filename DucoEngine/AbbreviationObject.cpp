#include "AbbreviationObject.h"

using namespace std;
using namespace Duco;

#include "DucoEngineUtils.h"
#include <algorithm>
#include <regex>

AbbreviationObject::AbbreviationObject(const std::wstring& theNames)
    : defaultName(L""), used(false)
{
    otherNames = new std::set<std::wstring*, sortStrings>();

    std::list<std::wstring> tokens;
    DucoEngineUtils::Tokenise<std::wstring>(theNames, tokens);
    
    std::list<std::wstring>::const_iterator it = tokens.begin();
    while (it != tokens.end())
    {
        AddName(*it);
        ++it;
    }
}

AbbreviationObject::~AbbreviationObject()
{
    std::set<std::wstring*>::iterator it = otherNames->begin();
    while (it != otherNames->end())
    {
        delete *it;
        ++it;
    }
    delete otherNames;
}

void
AbbreviationObject::AddName(const std::wstring& newName)
{
    if (newName.length() != 0 && defaultName.length() == 0)
    {
        defaultName = newName;
    }
    else
    {
        std::wstring* name = new std::wstring(newName);
        otherNames->insert(name);
    }
}

bool
AbbreviationObject::UpdateName(std::wstring& nameToFindOriginal) const
{
    bool updateMade = false;
    std::set<std::wstring*, sortStrings>::const_iterator it = otherNames->begin();
    while (it != otherNames->end())
    {
        std::wstring stringToReplace = **it;
        size_t index = nameToFindOriginal.find(stringToReplace, 0);
        if (index != string::npos)
        {
            updateMade = true;
            nameToFindOriginal = std::regex_replace(nameToFindOriginal, std::wregex(stringToReplace), defaultName);
        }
        ++it;
    }
    return updateMade;
}


bool
AbbreviationObject::ContainsName(const std::wstring& nameToFindOriginal, bool substring) const
{
    if (DucoEngineUtils::CompareString(defaultName, nameToFindOriginal))
    {
        return true;
    }

    std::wstring nameToFind;
    DucoEngineUtils::ToLowerCase(nameToFindOriginal, nameToFind);
    bool found (false);
    std::set<std::wstring*>::const_iterator it = otherNames->begin();
    while (it != otherNames->end() && !found)
    {
        if (!substring)
        {
            int comparision = nameToFind.compare(**it);
            if (comparision == 0)
            {
                used = true;
                found = true;
            }
            else if (comparision < 0)
            {
                return false;
            }
        }
        else if (substring && DucoEngineUtils::FindSubstring(nameToFind, **it))
        {
            used = true;
            found = true;
        }
        ++it;
    }
    return found;
}

bool
AbbreviationObject::Valid() const
{
    return defaultName.length() > 0 && !otherNames->empty();
}

const std::wstring&
AbbreviationObject::DefaultName() const
{
    return defaultName;
}

bool
AbbreviationObject::operator<(const Duco::AbbreviationObject& obj)
{
    if (used != obj.used)
        return used;
    return defaultName.compare(obj.defaultName) < 0;
}
