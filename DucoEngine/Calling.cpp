#include <cassert>
#include <stdlib.h>
#include "Calling.h"
#include "Change.h"
#include "ChangeCollection.h"
#include "LeadEndCollection.h"
#include "DatabaseReader.h"
#include "DatabaseWriter.h"
#include "PlaceNotation.h"
#include "Lead.h"

using namespace Duco;
using namespace std;

Calling::Calling(unsigned int newPosition, Duco::TLeadType newType, const Duco::ObjectId& changeMethodId)
    : leadType(newType), position(newPosition), count(1), methodId(changeMethodId), callingId(0)
{

}

Calling::Calling(const Calling& other)
: leadType(other.leadType), position(other.position), count(other.count), methodId(other.methodId), callingId(other.callingId)
{

}

Calling::Calling(DatabaseReader& reader, unsigned int databaseVersion)
:   leadType(EPlainLead), position(-1), count(1), callingId(0)
{
    methodId.ClearId();
    Internalise(reader, databaseVersion);
}

Calling::~Calling()
{

}

DatabaseReader&
Calling::Internalise(DatabaseReader& reader, unsigned int databaseVersion)
{
    leadType = (TLeadType)reader.ReadUInt();
    methodId = reader.ReadUInt();
    if (databaseVersion >= 27)
    {
        callingId = reader.ReadUInt();
    }
    position = reader.ReadUInt();
    count = reader.ReadUInt();
    if (databaseVersion >= 25)
    {
        bool isBefore = reader.ReadBool();
        if (isBefore)
        {
            position = 3;
        }
    }
    return reader;
}

DatabaseWriter&
Calling::Externalise(DatabaseWriter& writer)
{
    writer.WriteInt(leadType);
    writer.WriteId(methodId);
    writer.WriteId(callingId);
    writer.WriteInt(position);
    writer.WriteInt(count);
    bool before = false;
    writer.WriteBool(before);
    return writer;
}

std::wstring
Calling::Str(bool alreadyContainsString) const
{
    if (count == 1)
    {
        if (EBobLead == leadType)
            return L"-";
        else if (ESingleLead == leadType)
            return L"s";
    }

    std::wstring str (L"");
    if (!alreadyContainsString)
    {
        wchar_t buffer[5] = L" ";
        swprintf(buffer, 5, L"%u", count);
        str += buffer;
        if (ESingleLead == leadType)
        {
            if (count == 2)
            {
                str = L"ss";
            }
            else
                str += L"s";
        }
    }
    else
    {
        for (unsigned int i (0); i < count; ++i)
        {
            if (EBobLead == leadType)
                str += L"-";
            else if (ESingleLead == leadType)
                str += L"s";
        }
    }

    return str;
}

bool
Calling::AdvanceToCall(Duco::Change& previousEndChange, const Duco::PlaceNotation& notation, const Duco::Calling* const previousCalling, size_t snapStartPos, size_t& noOfChanges, bool generateCourseOnly) const
{
    noOfChanges = 0;

    Change endChange(previousEndChange);
    unsigned int noOfCallings (count);

    bool callingFound (false);
    while (noOfCallings > 0)
    {
        Duco::Lead* nextLead = notation.CreateLead(endChange, snapStartPos);
        snapStartPos = 0;

        unsigned int noOfLeads (0);
        while (!nextLead->IsCallingPosition(leadType, *this))
        {
            ++noOfLeads;
            if (noOfLeads > notation.LeadOrder().Size() + 1)
                return false;

            noOfChanges += nextLead->NoOfChanges();
            nextLead = notation.CreateNextLead(nextLead);
        }
        if (nextLead->IsCallingPosition(leadType, *this))
        {
            callingFound = true;
            noOfChanges += nextLead->NoOfChanges();
            nextLead->SetLeadEndType(leadType);
            nextLead->LeadEnd(endChange);
            delete nextLead;
            --noOfCallings;
        }
    }
    if (callingFound)
    {
        previousEndChange = endChange;
    }
    else
    {
        noOfChanges = 0;
    }

    return callingFound;
}

void
Calling::AdvanceToCall(Duco::ChangeCollection& changes, Duco::LeadEndCollection& leadEnds, const PlaceNotation& notation, size_t snapStartPos, bool stopAtFalse, bool stopAtRounds) const
{
    unsigned int noOfCallings (count);
    while (noOfCallings > 0 && (!changes.EndsWithRounds() || !stopAtRounds))
    {
        Duco::Lead* nextLead = notation.CreateLead(changes.EndChange(), snapStartPos);
        //while ((!changes.EndsWithRounds() || !stopAtRounds) && !nextLead->IsCallingPosition(leadType, position))
        while (!nextLead->IsCallingPosition(leadType, position))
        {
            nextLead->Changes(changes, stopAtFalse, stopAtRounds);
            leadEnds.Add(*nextLead, false);
            if (!changes.EndsWithRounds())
                nextLead = notation.CreateNextLead(nextLead);
        }
        if (!changes.EndsWithRounds() || !stopAtRounds)
        {
            nextLead->SetLeadEndType(leadType);
            leadEnds.Add(*nextLead, false);
            nextLead->Changes(changes, false, false);
        }
        delete nextLead;
        --noOfCallings;
    }
}

bool
Calling::MatchPosition(wchar_t positionToMatch, const PlaceNotation& notation) const
{
    return (notation.LeadOrder().CheckPosition(positionToMatch) == position);
}

bool
Calling::MatchPosition(const Calling& other) const
{
    return position == other.position;
}

wchar_t
Calling::Position(const Duco::PlaceNotation& notation) const
{
    return notation.LeadOrder().CheckPosition(position)[0];
}

bool
Calling::IsHome(const Duco::LeadOrder& leadOrder) const
{
    return leadOrder.HomePosition() == position;
}

bool
Calling::operator==(const Duco::Calling& rhs) const
{
    if (leadType != rhs.leadType)
        return false;
    if (methodId != rhs.methodId)
        return false;
    if (callingId != rhs.callingId)
        return false;
    if (position != rhs.position)
        return false;
    if (count != rhs.count)
        return false;

    return true;
}

bool
Calling::operator!=(const Duco::Calling& rhs) const
{
    if (leadType != rhs.leadType)
        return true;
    if (methodId != rhs.methodId)
        return true;
    if (callingId != rhs.callingId)
        return true;
    if (position != rhs.position)
        return true;
    if (count != rhs.count)
        return true;

    return false;
}
