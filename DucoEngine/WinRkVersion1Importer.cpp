#include "WinRkVersion1Importer.h"

#include <algorithm>
#include <fstream>
#include <cstdlib>
#include "PerformanceDate.h"
#include "PerformanceTime.h"
#include "DucoEngineUtils.h"
#include "Method.h"
#include "MethodDatabase.h"
#include "Peal.h"
#include "PealDatabase.h"
#include "RingerDatabase.h"
#include "RingingDatabase.h"
#include "Tower.h"
#include "TowerDatabase.h"
#include "AssociationDatabase.h"

using namespace std;
using namespace Duco;

WinRkVersion1Importer::WinRkVersion1Importer(Duco::ProgressCallback& theCallback,
            Duco::RingingDatabase& theDatabase)
:   FileImporter(theCallback, theDatabase)
{
    setLocale = false;
}

WinRkVersion1Importer::~WinRkVersion1Importer()
{
}

void
WinRkVersion1Importer::GetTotalFileSize(const std::string& fileName)
{
    totalFileSize = GetFileSize(fileName);
}

std::wifstream&
WinRkVersion1Importer::GetLine(std::wifstream& inputFile, std::wstring& nextLine)
{
    nextLine.clear();
    wchar_t charsRead[1024];
    inputFile.getline(charsRead, 1024, NULL); // TODO, NULL to a non pointer argument
    if (!inputFile.bad() && !inputFile.fail() && !inputFile.eof())
    {
        nextLine.append(charsRead);
    }
    if (nextLine.find(L'�') != std::wstring::npos)
    {
        nextLine.clear();
    }
    return inputFile;
}

bool
WinRkVersion1Importer::ProcessPeal(const std::wstring& nextLine)
{
    std::vector<std::wstring> tokens;
    TokeniseLine(nextLine, tokens, false, false, true, L'�');
    if (tokens.size() <= 1)
        return false;

    Peal newPeal;
    std::wstring association;
    if (GetFieldValue(tokens, 0, association))
    {
        Duco::ObjectId associationId = database.AssociationsDatabase().SuggestAssociationOrCreate(association);
        newPeal.SetAssociationId(associationId);
    }

    unsigned int noOfRingersInMethod (0);
    FindMethod(tokens, newPeal, noOfRingersInMethod);
    FindDate(tokens, newPeal);
    FindTime(tokens, newPeal);

    size_t noOfChanges (0);
    if (GetFieldValue(tokens, 8, noOfChanges))
    {
        newPeal.SetChanges(noOfChanges);
        if (noOfChanges < 5000)
            database.Settings().SetPealDatabase(false);
    }

    unsigned int noOfRingersInPeal = CreateRingers(tokens, newPeal);
    if (noOfRingersInMethod == -1)
        noOfRingersInMethod = 0;

    if (noOfRingersInPeal != noOfRingersInMethod)
    {
        unsigned int noOfRingers = max(noOfRingersInMethod, noOfRingersInPeal);
/*        if ((!DucoEngineUtils::IsEven(noOfRingers)) && noOfRingersInPeal == (noOfRingersInMethod+1))
        {
            CreateTower(tokens, newPeal, noOfRingers);
        }
        else*/
        {
            CreateTower(tokens, newPeal, noOfRingers);
        }
    }
    else
        CreateTower(tokens, newPeal, noOfRingersInPeal);

    size_t conductorBellNo (0);
    if (GetFieldValue(tokens, 24, conductorBellNo))
    {
        newPeal.SetConductorIdFromBell(conductorBellNo, true, false);
    }
    std::wstring composer;
    if (GetFieldValue(tokens, 25, composer))
    {
        newPeal.SetComposer(RecodeRingerName(composer));
    }
    std::wstring footnotes;
    if (GetFieldValue(tokens, 26, footnotes) && footnotes.length() > 0)
    {
        std::wstring extendedFootnotes;
        if (GetFieldValue(tokens, 27, extendedFootnotes) && extendedFootnotes.length() > 0)
        {
            footnotes += L" ";
            footnotes += extendedFootnotes;
        }
        if (GetFieldValue(tokens, 28, extendedFootnotes) && extendedFootnotes.length() > 0)
        {
            footnotes += L" ";
            footnotes += extendedFootnotes;
        }
        newPeal.SetFootnotes(footnotes);
    }

    return database.PealsDatabase().AddObject(newPeal).ValidId();
}

void
WinRkVersion1Importer::CreateTower(const std::vector<std::wstring>& tokens, Duco::Peal& thePeal, unsigned int noOfRingers) const
{
    std::wstring towerName;
    std::wstring towerTown;
    std::wstring towerCounty;
    if (GetFieldValue(tokens, 3, towerName) &&
        GetFieldValue(tokens, 1, towerTown) &&
        GetFieldValue(tokens, 2, towerCounty))
    {
        std::wstring tenorWeight = CreateTenorField(tokens);
        ObjectId towerId = database.TowersDatabase().SuggestTower(towerName, towerTown, towerCounty, noOfRingers, tenorWeight, L"", true);
        if (towerId.ValidId())
        {
            thePeal.SetTowerId(towerId);
            const Tower* const theTower = database.TowersDatabase().FindTower(towerId);
            ObjectId ringId;
            if (theTower->SuggestRing(noOfRingers, tenorWeight, L"", ringId, true))
            {
                thePeal.SetRingId(ringId);
            }
            else
            {
                // need to create new ring
                Tower updatedTower (*theTower);
                ringId = updatedTower.AddRing(noOfRingers, tenorWeight, L"");
                if (ringId.ValidId() && database.TowersDatabase().UpdateObject(updatedTower))
                {
                    thePeal.SetRingId(ringId);
                }
            }
        }
        else
        {
            Tower newTower(KNoId, towerName, towerTown, towerCounty, noOfRingers);
            newTower.AddDefaultRing(Tower::DefaultRingName(noOfRingers), tenorWeight);
            towerId = database.TowersDatabase().AddObject(newTower);
            thePeal.SetTowerId(towerId);
            thePeal.SetRingId(0);
        }
    }
}

std::wstring
WinRkVersion1Importer::RecodeRingerName(std::wstring& ringerStr) const
{
    std::wstring tempRingerStr;
    std::wstring::const_iterator it = ringerStr.begin();
    while (it != ringerStr.end())
    {
        if (isupper(*it) != 0 && tempRingerStr.length() > 0)
        {
            tempRingerStr += ' ';
        }

        tempRingerStr += *it;
        ++it;
    }
    return tempRingerStr;
}


unsigned int
WinRkVersion1Importer::CreateRingers(std::vector<std::wstring>& tokens, Duco::Peal& thePeal) const
{
    const size_t firstRingerFieldId (12);
    const size_t lastRingerFieldId (23);

    unsigned int numberOfRingers (0);
    for (int i(firstRingerFieldId); i <= lastRingerFieldId; ++i)
    {
        std::wstring ringerStr = RecodeRingerName(tokens[i]);
        if (ringerStr.length() > 0)
        {
            ObjectId ringerId = database.RingersDatabase().FindIdenticalRinger(ringerStr);
            if (!ringerId.ValidId())
            {
                ringerId = database.RingersDatabase().AddRinger(ringerStr);
            }
            if (ringerId.ValidId())
            {
                ++numberOfRingers;
                thePeal.SetRingerId(ringerId, numberOfRingers, false);
            }
        }
    }

    if (thePeal.PossibleHandbellPeal())
    {
        thePeal.ConvertToHandbellPeal();
    }

    return numberOfRingers;
}

bool
WinRkVersion1Importer::GetFieldValue(const std::vector<std::wstring>& tokens, size_t fieldNo, std::wstring& fieldValue) const
{
    fieldValue = L"";
    if (fieldNo != -1 && tokens.size() > fieldNo)
    {
        fieldValue = tokens[fieldNo];
        return true;
    }
    return false;
}

bool
WinRkVersion1Importer::GetFieldValue(const std::vector<std::wstring>& tokens, size_t fieldNo, size_t& fieldValue) const
{
    fieldValue = 0;
    std::wstring tempValue;
    if (!GetFieldValue(tokens, fieldNo, tempValue) || tempValue.length() <= 0 || isdigit (tempValue[0]) == 0)
        return false;

    fieldValue = _wtoi(tempValue.c_str());
    return true;
}

std::wstring
WinRkVersion1Importer::CreateTenorField(const std::vector<std::wstring>& tokens) const
{
    std::wstring tenorWeight;
    GetFieldValue(tokens, 5, tenorWeight);
    return tenorWeight;
}

void
WinRkVersion1Importer::FindDate(const std::vector<std::wstring>& tokens, Duco::Peal& thePeal) const
{
    size_t dayNumber;
    PerformanceDate theDate(KFirstJanuary1900);
    if (GetFieldValue(tokens, 4, dayNumber))
    {
        if (dayNumber < 10000)
        {
            // Move to post 2000.
            PerformanceDate newDate(KSecondJanuary2000);
            theDate = newDate;
        }
        theDate = theDate.AddDays(dayNumber);
    }
    else
    {
        theDate.ResetToEarliest();
    }
    thePeal.SetDate(theDate);
}

void
WinRkVersion1Importer::FindTime(const std::vector<std::wstring>& tokens, Duco::Peal& thePeal) const
{
    size_t hours(0);
    size_t minutes(0);
    GetFieldValue(tokens, 6, hours);
    GetFieldValue(tokens, 7, minutes);

    minutes += hours * 60;
    PerformanceTime theDate(minutes);
    thePeal.SetTime(theDate);
}

void
WinRkVersion1Importer::FindMethod(const std::vector<std::wstring>& tokens, Duco::Peal& thePeal, unsigned int& noOfRingers) const
{
    std::wstring method;
    std::wstring methodtype;
    std::wstring stage;

    GetFieldValue(tokens, 9, method);
    GetFieldValue(tokens, 10, methodtype);
    GetFieldValue(tokens, 11, stage);
    noOfRingers = -1;

    thePeal.SetMethodId(database.MethodsDatabase().FindOrCreateMethod(method, methodtype, stage, noOfRingers));
}

bool
WinRkVersion1Importer::EnforceQuotes() const
{
    return false;
}
