#ifndef __RINGINGOBJECTDATABASE_H__
#define __RINGINGOBJECTDATABASE_H__

#include <deque>
#include <map>
#include <set>
#include <unordered_map>
#include <vector>

#include "DucoEngineCommon.h"
#include "DucoIndex.h"
#include "PealLengthInfo.h"
#include "ObjectType.h"
#include "ObjectId.h"
#include <xercesc\dom\DomDocument.hpp>

namespace Duco
{
    class ImportExportProgressCallback;
    class RenumberProgressCallback;
    class DatabaseReader;
    class DatabaseWriter;
    class RingingDatabase;
    class RingingObject;
    class TSearchArgument;
    class TSearchValidationArgument;
    class TUpdateArgument;
    class ProgressCallback;


struct ObjectIdHash
{
    std::size_t operator()(ObjectId const& s) const
    {
        return std::hash<size_t>()(s.Id());
    }
};

#define DUCO_OBJECTS_CONTAINER std::unordered_map<Duco::ObjectId, Duco::RingingObject*, ObjectIdHash>
#define DUCO_OBJECTS_INDEX std::vector<Duco::DucoIndex>

class RingingObjectDatabase
{
public:
    virtual ~RingingObjectDatabase();

    DllExport size_t NumberOfObjects() const;
    DllExport virtual void ClearObjects(bool createDefaultBellNames);

    DllExport bool DeleteObject(const Duco::ObjectId& id);
    DllExport bool DeleteObjects(const std::set<Duco::ObjectId>& pictureIds);
    DllExport bool UpdateObject(const Duco::RingingObject& newObject);
    DllExport bool FirstObject(Duco::ObjectId& objectId, bool setIndex = false) const;
    DllExport bool LastObject(Duco::ObjectId& objectId, bool setIndex = false) const;
    DllExport bool NextObject(Duco::ObjectId& objectId, bool forwards, bool setIndex = true) const;
    DllExport bool ForwardMultipleObjects(Duco::ObjectId& objectId, bool forwards, size_t numberToMoveBy = 20) const;
    DllExport const Duco::RingingObject* const FindObject(const Duco::ObjectId& objectId, bool setIndex = true) const;
    DllExport bool ObjectExists(const Duco::ObjectId& objectId) const;
    virtual bool RebuildObjects(Duco::RenumberProgressCallback* callback, Duco::RingingDatabase& database) = 0;
    virtual void GetNewObjectIdsByAlphabetic(std::map<Duco::ObjectId, Duco::ObjectId>& newIdsOldFirst, bool lastNameFirst) const;
    bool SwapObjects(const Duco::ObjectId& existingId, const Duco::ObjectId& newId);

    DllExport ObjectId FindDuplicateObject(const Duco::RingingObject& object, const Duco::RingingDatabase& database) const;

    DllExport void GetAllObjectIds(std::set<Duco::ObjectId>& allIds) const;
    void SearchObjects(ProgressCallback& callback, std::set<Duco::ObjectId>& foundIds, const std::deque<Duco::TSearchArgument*>& searchArguments, const RingingDatabase& database, bool doubleProgressCallbacks) const;
    void ValidateObjects(Duco::ProgressCallback& callback, std::set<ObjectId>& checkIds, const std::deque<Duco::TSearchValidationArgument*>& searchArguments, const RingingDatabase& database, bool doubleProgressCallbacks) const;
    DllExport void ClearValidationFlags();
    DllExport bool UpdateObjects(ProgressCallback& callback, const std::set<Duco::ObjectId>& ids, const Duco::TUpdateArgument& updateArgument);
    void GetNewIdsByPealCountAndAlphabetic(const std::map<Duco::ObjectId, Duco::PealLengthInfo>& objectPealCounts, std::map<Duco::ObjectId, Duco::ObjectId>& newObjectIds, bool lastNameFirst) const;

    inline bool DataChanged() const;
    inline void SetDataChanged() const;
    inline void ResetDataChanged() const;
    void ResetCurrentPointer();

    virtual void Externalise(Duco::ImportExportProgressCallback* newCallback, Duco::DatabaseWriter& writer, size_t noOfObjectsInThisDatabase, size_t totalObjects, size_t objectsProcessedSoFar) const;
    virtual bool Internalise(DatabaseReader& reader, Duco::ImportExportProgressCallback* newCallback, int databaseVersionNumber, size_t noOfObjectsInThisDatabase, size_t totalObjects, size_t objectsProcessedSoFar) = 0;
    virtual void ExternaliseToXml(Duco::ImportExportProgressCallback* newCallback, XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, bool exportDucoObjects) const = 0;

    DllExport virtual bool RebuildRecommended() const;

protected:
    virtual bool UpdateObjectField(const Duco::ObjectId& id, const Duco::TUpdateArgument& updateArgument) = 0;
    virtual bool MatchObject(const Duco::RingingObject& object, const Duco::TSearchArgument& searchArg, const Duco::RingingDatabase& database) const = 0;
    virtual bool ValidateObject(const Duco::RingingObject& object, const Duco::TSearchValidationArgument& searchArg, const std::set<Duco::ObjectId>& idsToCheckAgainst, const Duco::RingingDatabase& database) const = 0;

    Duco::RingingObject* FindObject(const Duco::ObjectId& objectId, bool setIndex = true);
    virtual Duco::ObjectId AddObject(const Duco::RingingObject& newObject) = 0;
    RingingObjectDatabase(Duco::TObjectType newObjectType);
    // Called by internaliser only, id is kept
    bool AddOwnedObject(Duco::RingingObject*& newObject);
    virtual bool SaveUpdatedObject(Duco::RingingObject& lhs, const Duco::RingingObject& rhs) = 0;
    bool RenumberObjects(std::map<Duco::ObjectId, Duco::ObjectId> newObjectIds, Duco::RenumberProgressCallback* callback);
    void RebuildIndex();
    bool SetIndex(const Duco::ObjectId& objectId) const;

protected:
    Duco::TObjectType                               objectType;
    DUCO_OBJECTS_CONTAINER                          listOfObjects;
    DUCO_OBJECTS_INDEX                              objectsIndex;

    mutable DUCO_OBJECTS_INDEX::iterator            objectsIndexIterator;
    mutable bool                                    dataChanged;

#if _DEBUG
    mutable Duco::ObjectId                          lastSearchedForId;
    size_t                                          lastSearchedForCount;       
#endif
};

bool
RingingObjectDatabase::DataChanged() const
{
    return dataChanged;
}

void
RingingObjectDatabase::SetDataChanged() const
{
    dataChanged = true;
}

void
RingingObjectDatabase::ResetDataChanged() const
{
    dataChanged = false;
}

}

#endif //!__RINGINGOBJECTDATABASE_H__
