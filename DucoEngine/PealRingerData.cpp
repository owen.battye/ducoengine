#include "PealRingerData.h"

using namespace Duco;

PealRingerData::PealRingerData(Duco::ObjectId newRingerId, unsigned int newPealFeeInPence, bool newFullyPaid)
:   ringerId(newRingerId), pealFeeInPence(newPealFeeInPence), fullyPaid(newFullyPaid)
{

}

PealRingerData::PealRingerData()
:   ringerId(KNoId), pealFeeInPence(0), fullyPaid(false)
{

}

PealRingerData::PealRingerData(const PealRingerData& other)
:   ringerId(other.ringerId), pealFeeInPence(other.pealFeeInPence), fullyPaid(other.fullyPaid)
{

}

PealRingerData::~PealRingerData()
{

}

bool
PealRingerData::SetPealFee(unsigned int newPealFee)
{
    if (pealFeeInPence == newPealFee)
    {
        return false;
    }

    pealFeeInPence = newPealFee;
    return true;
}

PealRingerData&
PealRingerData::operator=(const PealRingerData& rhs)
{
    ringerId = rhs.ringerId;
    pealFeeInPence = rhs.pealFeeInPence;
    fullyPaid = rhs.fullyPaid;
    return *this;
}

bool
PealRingerData::operator==(const PealRingerData& rhs) const
{
    if (ringerId == rhs.ringerId &&
        pealFeeInPence == rhs.pealFeeInPence &&
        fullyPaid == rhs.fullyPaid)
    {
        return true;
    }
    return false;
}

bool
PealRingerData::operator!=(const PealRingerData& rhs) const
{
    if (ringerId == rhs.ringerId &&
        pealFeeInPence == rhs.pealFeeInPence &&
        fullyPaid == rhs.fullyPaid)
    {
        return false;
    }
    return true;
}

unsigned int
PealRingerData::PealFee() const
{
    return pealFeeInPence;
}

const Duco::ObjectId&
PealRingerData::RingerId() const
{
    return ringerId;
}

void
PealRingerData::SetRingerId(const Duco::ObjectId& newId)
{
    ringerId = newId;
}

bool
PealRingerData::FullyPaid() const
{
    return fullyPaid;
}

void
PealRingerData::SetFullyPaid(bool newFullyPaid)
{
    fullyPaid = newFullyPaid;
}

