#include "SettingsFile.h"

#include <fstream>
#include <algorithm>
#include "DucoEngineUtils.h"

using namespace std;
using namespace Duco;

SettingsFile::SettingsFile(const std::string& installDir, const std::string& fileNameOnly)
    : settingsChanged(false), expectedNoOfFields(0)
{
    SetFilename(installDir, fileNameOnly);
    wifstream* inputFile = new wifstream(fileName.c_str());
    if (inputFile != NULL && inputFile->is_open())
    {
        while (!inputFile->eof())
        {
            std::wstring nextLine;
            getline(*inputFile, nextLine);
            if (nextLine.length() > 0)
            {
                ProcessLine(nextLine);
            }
        }
        inputFile->close();
        delete inputFile;
    }
    settingsChanged = false;
}

SettingsFile::SettingsFile(const Duco::SettingsFile& other)
:   fileName(other.fileName), expectedNoOfFields(other.expectedNoOfFields), settingsChanged(false)
{
    std::map<std::wstring,std::wstring>::const_iterator it = other.strSettings.begin();
    while (it != other.strSettings.end())
    {
        std::pair<std::wstring,std::wstring> newObject(it->first, it->second);
        strSettings.insert(newObject);
        ++it;
    }
}

SettingsFile::~SettingsFile()
{
    if (settingsChanged)
        Save();
    strSettings.clear();
}

void
SettingsFile::SetFilename(const std::string& installDir, const std::string& fileNameOnly)
{
    fileName = installDir;
    if (installDir.length() > 0 && installDir[installDir.length() - 1] != '\\' && installDir[installDir.length() - 1] != '/')
    {
        fileName += '\\';
    }
    fileName += fileNameOnly;
}

void
SettingsFile::ClearAllSettings()
{
    strSettings.clear();
    settingsChanged = false;
}

bool
SettingsFile::Setting(const std::wstring& settingId, std::wstring& settingValue) const
{
    std::map<std::wstring,std::wstring>::const_iterator it = strSettings.find(settingId);
    if (it == strSettings.end())
    {
        settingValue = L"";
        return false;
    }
    settingValue = it->second;
    return true;
}

bool
SettingsFile::Setting(const std::wstring& settingId, int& settingValue) const
{
    std::wstring tempStrSetting;
    if (!Setting(settingId, tempStrSetting))
        return false;

    settingValue = _wtoi(tempStrSetting.c_str());
    if (settingValue != -1)
    {
        expectedNoOfFields = std::max(expectedNoOfFields, (unsigned int)settingValue);
    }
    return true;
}

bool
SettingsFile::Setting(const std::wstring& settingId, bool& settingValue) const
{
    std::wstring tempStrSetting;
    if (!Setting(settingId, tempStrSetting))
        return false;

    std::transform(tempStrSetting.begin(), tempStrSetting.end(), tempStrSetting.begin(), ::tolower);

    if (tempStrSetting.compare(L"true") == 0)
    {
        settingValue = true;
        return true;
    }
    else if (tempStrSetting.compare(L"false") == 0)
    {
        settingValue = false;
        return true;
    }
    // If the setting is a number, then we can convert it to an int and use that as the setting value
    // (0 = false, 1 = true)
    settingValue = false;
    int settingValueInt = _wtoi(tempStrSetting.c_str());
    if (settingValueInt == 1)
    {
        settingValue = true;
    }

    return true;
}

void
SettingsFile::ProcessLine(const std::wstring& nextLine)
{
    if (nextLine.length() > 0)
    {
        if (nextLine[0] != '#')
        {
            size_t seperatorPos = nextLine.find_first_of(L":");
            std::wstring index = nextLine.substr(0, seperatorPos);
            std::wstring setting = nextLine.substr(seperatorPos+1);

            std::pair<wstring, wstring> newStrObject(index, setting);
            strSettings.insert(newStrObject);
        }
    }
}

bool
SettingsFile::Save() const
{
    wofstream* outputFile = new wofstream(fileName.c_str(), ios_base::out | ios_base::trunc);
    if (outputFile == NULL)
    {
        return false;
    }

    bool returnVal (outputFile->is_open());
    if (returnVal)
    {
        std::multimap<std::wstring,std::wstring> settingsBySetting;
        {
            std::map<std::wstring,std::wstring>::const_iterator it = strSettings.begin();
            while (it != strSettings.end())
            {
                std::pair<std::wstring,std::wstring> newSettings (it->second, it->first);
                settingsBySetting.insert(newSettings);
                ++it;
            }
        }

        std::multimap<std::wstring,std::wstring>::const_iterator it2 = settingsBySetting.begin();
        while (it2 != settingsBySetting.end())
        {
            (*outputFile) << it2->second << ":" << it2->first << endl;
            ++it2;
        }
        outputFile->close();
    }
    delete outputFile;
    return returnVal;
}

bool
SettingsFile::Set(const std::wstring& settingId, int settingValue)
{
    std::wstring settingVal = DucoEngineUtils::ToString(settingValue);
    return Set(settingId, settingVal);
}

bool
SettingsFile::Set(const std::wstring& settingId, const std::wstring& settingValue)
{
    std::pair<wstring, wstring> newStrObject(settingId, settingValue);
    pair<std::map<wstring, wstring>::iterator, bool> it = strSettings.insert(newStrObject);
    if (it.second)
    {
        settingsChanged = true;
    }
    else
    {
        settingsChanged = true;
        it.first->second = settingValue;
        return true;
    }
    return false;
}

bool
SettingsFile::operator==(const SettingsFile& rhs) const
{
    if (fileName.compare(rhs.fileName) != 0)
        return false;
    if (expectedNoOfFields != rhs.expectedNoOfFields)
        return false;
    if (strSettings.size() != rhs.strSettings.size())
        return false;

    std::map<std::wstring,std::wstring>::const_iterator lhsIt = strSettings.begin();
    std::map<std::wstring,std::wstring>::const_iterator rhsIt = rhs.strSettings.begin();
    while (lhsIt != strSettings.end() && rhsIt != rhs.strSettings.end())
    {
        if (lhsIt->first.compare(rhsIt->first) != 0 || lhsIt->second.compare(rhsIt->second) != 0)
            return false;

        ++lhsIt;
        ++rhsIt;
    }
    return true;
}

bool
SettingsFile::operator!=(const SettingsFile& rhs) const
{
    if (fileName.compare(rhs.fileName) != 0)
        return true;
    if (expectedNoOfFields != rhs.expectedNoOfFields)
        return true;
    if (strSettings.size() != rhs.strSettings.size())
        return true;

    std::map<std::wstring,std::wstring>::const_iterator lhsIt = strSettings.begin();
    std::map<std::wstring,std::wstring>::const_iterator rhsIt = rhs.strSettings.begin();
    while (lhsIt != strSettings.end() && rhsIt != rhs.strSettings.end())
    {
        if (lhsIt->first.compare(rhsIt->first) != 0 || lhsIt->second.compare(rhsIt->second) != 0)
            return true;

        ++lhsIt;
        ++rhsIt;
    }
    return false;
}
