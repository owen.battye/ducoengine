#include "LeadEndCollection.h"

#include "Lead.h"
#include "LeadEnd.h"
#include "Change.h"

using namespace Duco;

LeadEndCollection::LeadEndCollection()
{
    leadEnds = new std::list<Duco::LeadEnd*>();
}

LeadEndCollection::LeadEndCollection(const LeadEndCollection& rhs)
{
    leadEnds = new std::list<Duco::LeadEnd*>();
    this->operator=(rhs);
    leadEndsIt = leadEnds->end();
}

LeadEndCollection::~LeadEndCollection()
{
    DeleteAllLeadEnds();
    delete leadEnds;
}

bool
LeadEndCollection::Add(const Change& newChange, TLeadType leadType, bool isCourseEnd)
{
    Duco::LeadEnd* newLeadEnd = new Duco::LeadEnd(newChange, leadType, isCourseEnd);
    leadEnds->push_back(newLeadEnd);
    return true;
}

bool
LeadEndCollection::Add(const Lead& nextLead, bool isCourseEnd)
{
    Change change(nextLead.Order());
    if (nextLead.LeadEnd(change))
    {
        return Add(change, nextLead.LeadEndType(), isCourseEnd);
    }
    return false;
}

void
LeadEndCollection::Clear()
{
    DeleteAllLeadEnds();
    leadEndsIt = leadEnds->end();
}

void
LeadEndCollection::DeleteAllLeadEnds()
{
    std::list<Duco::LeadEnd*>::iterator it = leadEnds->begin();
    while (it != leadEnds->end())
    {
        delete *it;
        ++it;
    }
    leadEnds->clear();
}

void
LeadEndCollection::SetPartEnd()
{
    std::list<Duco::LeadEnd*>::reverse_iterator it = leadEnds->rbegin();
    if (it != leadEnds->rend())
    {
        (*it)->SetPartEnd();
    }
}

void
LeadEndCollection::Start()
{
    leadEndsIt = leadEnds->begin();
}

bool
LeadEndCollection::MoreLeadEnds()
{
    return leadEndsIt != leadEnds->end();
}

const Duco::LeadEnd&
LeadEndCollection::LeadEnd()
{
    return **(leadEndsIt++);
}

LeadEndCollection&
LeadEndCollection::operator=(const LeadEndCollection& rhs)
{
    Clear();

    std::list<Duco::LeadEnd*>::const_iterator it = rhs.leadEnds->begin();
    while (it != rhs.leadEnds->end())
    {
        leadEnds->push_back((*it)->Realloc());
        ++it;
    }
    return *this;
}

bool
LeadEndCollection::Check(size_t leadEndNumber, const Duco::Change& leadEnd) const
{
    size_t i = 0;
    std::list<Duco::LeadEnd*>::const_iterator it = leadEnds->begin();
    while (i < leadEndNumber && it != leadEnds->end())
    {
        if (i == leadEndNumber)
        {
            return (*it)->Check(leadEnd);
        }
        ++it;
        ++i;
    }
    return false;
}
