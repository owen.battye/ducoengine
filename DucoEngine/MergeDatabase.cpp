#include "MergeDatabase.h"

#include "Association.h"
#include "AssociationDatabase.h"
#include "Composition.h"
#include "CompositionDatabase.h"
#include "DucoEngineUtils.h"
#include "Method.h"
#include "MethodDatabase.h"
#include "MethodSeries.h"
#include "MethodSeriesDatabase.h"
#include "Peal.h"
#include "PealDatabase.h"
#include "Picture.h"
#include "PictureDatabase.h"
#include "ProgressCallback.h"
#include "Ring.h"
#include "Ringer.h"
#include "RingingDatabase.h"
#include "RingerDatabase.h"
#include "StatisticFilters.h"
#include "Tower.h"
#include "TowerDatabase.h"
#include <chrono>
#include "DucoEngineLog.h"

using namespace std::chrono;
using namespace Duco;
using namespace std;

MergeDatabase::MergeDatabase(Duco::ProgressCallback& theCallback, Duco::RingingDatabase& theCurrentDatabase)
    : callback(theCallback), currentDatabase(theCurrentDatabase), otherDatabase(NULL)
{
    externalRingerId.ClearId();
    externalTowerId.ClearId();
}

MergeDatabase::~MergeDatabase()
{
    delete otherDatabase;
}

void
MergeDatabase::ReadDatabase(const std::string& databaseFileName, const Duco::ObjectId& onlyWithRingerId, const std::wstring& newTowerbaseId)
{
    ringerId = onlyWithRingerId;
    towerbaseId = newTowerbaseId;
    externalRingerId.ClearId();
    externalTowerId.ClearId();
    delete otherDatabase;
    externalRingerIdsToInternal.clear();
    externalMethodIdsToInternal.clear();
    externalTowerIdsToInternal.clear();
    externalAssociationIdsToInternal.clear();
    externalPictureIdsToInternal.clear();
    externalCompositionIdsToInternal.clear();
    externalMethodSeriesIdsToInternal.clear();
    requiredTowers.clear();
    requiredMethods.clear();
    requiredRingers.clear();
    externalPealIds.clear();
    externalOrderNames.clear();
    requiredMethodSeries.clear();
    requiredCompositions.clear();
    requiredPictures.clear();


    otherDatabase = new RingingDatabase();

    unsigned int databaseVersionNo (-1);
    otherDatabase->Internalise(databaseFileName.c_str(), this, databaseVersionNo);
}

std::wstring
MergeDatabase::DefaultRingerExternalName() const
{
    if (externalRingerId.ValidId())
    {
        const Ringer* const externalRinger = otherDatabase->RingersDatabase().FindRinger(externalRingerId);
        if (externalRinger != NULL)
        {
            return externalRinger->FullName(currentDatabase.Settings().LastNameFirst());
        }
    }
    else if (ringerId.ValidId())
    {
        const Ringer* const internalRinger = currentDatabase.RingersDatabase().FindRinger(ringerId);
        if (internalRinger != NULL)
        {
            return L"Could not find " + internalRinger->FullName(currentDatabase.Settings().LastNameFirst()) + L" in other database";
        }
    }
    return L"";
}

std::wstring
MergeDatabase::TowerExternalName() const
{
    if (externalTowerId.ValidId())
    {
        const Tower* const externalTower = otherDatabase->TowersDatabase().FindTower(externalTowerId);
        if (externalTower != NULL)
        {
            return L"Found " + externalTower->FullName() + L" in other database";
        }
    }
    else if (towerbaseId.length() > 0)
    {
        Duco::ObjectId internalTowerId = currentDatabase.TowersDatabase().FindTowerByTowerbaseId(towerbaseId);
        if (internalTowerId.ValidId())
        {
            const Tower* const internalTower = currentDatabase.TowersDatabase().FindTower(internalTowerId);
            if (internalTower != NULL)
            {
                return L"Could not find " + internalTower->FullName() + L" in other database";
            }
        }
    }
    return L"";
}

void
MergeDatabase::InitialisingImportExport(bool /*internalising*/, unsigned int /*versionNumber*/, size_t /*totalNumberOfObjects*/)
{
    callback.Initialised();
}

void
MergeDatabase::ObjectProcessed(bool /*internalising*/, Duco::TObjectType objectType, Duco::ObjectId objectId, int totalPercentage, size_t numberInThisSubDatabase)
{
    callback.Step(totalPercentage);

    switch (objectType)
    {
    case TObjectType::EAssociationOrSociety:
    {
        const Association* const theAssociation = otherDatabase->AssociationsDatabase().FindAssociation(objectId);
        if (theAssociation != NULL)
        {
            std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator it = externalAssociationIdsToInternal.find(objectId);
            if (it == externalAssociationIdsToInternal.end())
            {
                time_point<high_resolution_clock> start = high_resolution_clock::now();
                Duco::ObjectId internalAssociationId = currentDatabase.AssociationsDatabase().FindDuplicateObject(*theAssociation, currentDatabase);
                if (internalAssociationId.ValidId())
                {
                    std::pair<Duco::ObjectId, Duco::ObjectId> newObject(objectId, internalAssociationId);
                    externalAssociationIdsToInternal.insert(newObject);
                }
                DUCOENGINEDEBUGLOG3(start, "Find equavalent society: %u", objectId.Id());
            }
        }
    }
    break;
    case TObjectType::ERinger:
    {
        const Ringer* const theRinger = otherDatabase->RingersDatabase().FindRinger(objectId);
        if (theRinger != NULL)
        {
            std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator it = externalRingerIdsToInternal.find(objectId);
            if (it == externalRingerIdsToInternal.end())
            {
                time_point<high_resolution_clock> start = high_resolution_clock::now();
                Duco::ObjectId internalRingerId = currentDatabase.RingersDatabase().FindDuplicateObject(*theRinger, currentDatabase);
                if (internalRingerId.ValidId())
                {
                    std::pair<Duco::ObjectId, Duco::ObjectId> newObject(objectId, internalRingerId);
                    externalRingerIdsToInternal.insert(newObject);
                }
                DUCOENGINEDEBUGLOG3(start, "Find equavalent ringer: %u", objectId.Id());
            }
        }
    }
    break;

    case TObjectType::ETower:
    {
        const Tower* const theTower = otherDatabase->TowersDatabase().FindTower(objectId);
        if (theTower != NULL)
        {
            std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator it = externalTowerIdsToInternal.find(objectId);
            if (it == externalTowerIdsToInternal.end())
            {
                time_point<high_resolution_clock> start = high_resolution_clock::now();
                Duco::ObjectId internalTowerId = currentDatabase.TowersDatabase().FindDuplicateObject(*theTower, currentDatabase);
                if (internalTowerId.ValidId())
                {
                    std::pair<Duco::ObjectId, Duco::ObjectId> newObject(objectId, internalTowerId);
                    externalTowerIdsToInternal.insert(newObject);
                }
                DUCOENGINEDEBUGLOG3(start, "Find equavalent tower: %u", objectId.Id());
            }
        }
    }
    break;

    case TObjectType::EMethod:
    {
        const Method* const theMethod = otherDatabase->MethodsDatabase().FindMethod(objectId);
        if (theMethod != NULL)
        {
            std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator it = externalMethodIdsToInternal.find(objectId);
            if (it == externalMethodIdsToInternal.end())
            {
                time_point<high_resolution_clock> start = high_resolution_clock::now();
                Duco::ObjectId internalMethodId = currentDatabase.MethodsDatabase().FindDuplicateObject(*theMethod, currentDatabase);
                if (internalMethodId.ValidId())
                {
                    std::pair<Duco::ObjectId, Duco::ObjectId> newObject(objectId, internalMethodId);
                    externalMethodIdsToInternal.insert(newObject);
                }
                DUCOENGINEDEBUGLOG3(start, "Find equavalent method: %u", objectId.Id());
            }
        }
    }
    break;

    case TObjectType::EOrderName:
    {
        std::wstring name;
        if (!currentDatabase.MethodsDatabase().FindOrderName((unsigned int)objectId.Id(), name))
        {
            externalOrderNames.push_back((unsigned int)objectId.Id());
        }
    }
    break;

    case TObjectType::EPeal:
    {
        const Peal* const thePeal = otherDatabase->PealsDatabase().FindPeal(objectId);
        if (thePeal != NULL)
        {
            bool defaultRingerFoundInOtherDatabase(!ringerId.ValidId() || externalRingerId.ValidId());
            bool asStrapper(false);
            bool defaultRingFoundInThisPeal(!ringerId.ValidId() || thePeal->ContainsRinger(externalRingerId, asStrapper));

            time_point<high_resolution_clock> start = high_resolution_clock::now();
            if ((defaultRingerFoundInOtherDatabase && defaultRingFoundInThisPeal) && !currentDatabase.PealsDatabase().FindDuplicateObject(*thePeal, currentDatabase).ValidId())
            {
                externalPealIds.insert(objectId);
            }
            DUCOENGINEDEBUGLOG3(start, "Determine if peal needs import: %u", objectId.Id());
        }
    }
    break;

    case TObjectType::EMethodSeries:
    {
        const MethodSeries* const theSeries = otherDatabase->MethodSeriesDatabase().FindMethodSeries(objectId);
        if (theSeries != NULL)
        {
            std::set<Duco::ObjectId> methodIds = theSeries->Methods();
            for (auto const& methodIdIt : methodIds)
            {
                std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator it = externalMethodIdsToInternal.find(methodIdIt);
                if (it == externalMethodIdsToInternal.end())
                {
                    const Method* const theMethod = otherDatabase->MethodsDatabase().FindMethod(methodIdIt);
                    if (theMethod != NULL)
                    {
                        time_point<high_resolution_clock> start = high_resolution_clock::now();
                        Duco::ObjectId internalMethodId = currentDatabase.MethodsDatabase().FindDuplicateObject(*theMethod, currentDatabase);
                        if (internalMethodId.ValidId())
                        {
                            std::pair<Duco::ObjectId, Duco::ObjectId> newObject(objectId, internalMethodId);
                            externalMethodIdsToInternal.insert(newObject);
                        }
                        DUCOENGINEDEBUGLOG3(start, "Find equavalent method: %u", objectId.Id());
                    }
                }
            }
        }
    }
    break;
    case TObjectType::EPicture:
    {
        // If importing all pictures
        //requiredPictures.insert(objectId);
    }
    break;

    case TObjectType::EComposition:
    {
        const Composition* const theComposition = otherDatabase->CompositionsDatabase().FindComposition(objectId);
        if (theComposition != NULL)
        {
            const Ringer* const theRinger = otherDatabase->RingersDatabase().FindRinger(theComposition->ComposerId());
            if (theRinger != NULL)
            {
                std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator it = externalRingerIdsToInternal.find(objectId);
                if (it == externalRingerIdsToInternal.end())
                {
                    time_point<high_resolution_clock> start = high_resolution_clock::now();
                    Duco::ObjectId internalRingerId = currentDatabase.RingersDatabase().FindDuplicateObject(*theRinger, currentDatabase);
                    if (internalRingerId.ValidId())
                    {
                        std::pair<Duco::ObjectId, Duco::ObjectId> newObject(objectId, internalRingerId);
                        externalRingerIdsToInternal.insert(newObject);
                    }
                    DUCOENGINEDEBUGLOG3(start, "Find equavalent composition: %u", objectId.Id());
                }
            }
        }
    }
    break;

    default:
        break;
    }
}

void
MergeDatabase::ImportExportComplete(bool /*internalising*/)
{
    const Duco::Ringer* const internalRequiredRinger = currentDatabase.RingersDatabase().FindRinger(ringerId);
    if (internalRequiredRinger != NULL)
    {
        externalRingerId = otherDatabase->RingersDatabase().FindDuplicateObject(*internalRequiredRinger, currentDatabase);
    }
    if (towerbaseId.length() > 0)
    {
        externalTowerId = otherDatabase->TowersDatabase().FindTowerByTowerbaseId(towerbaseId);
    }
    if (externalRingerId.ValidId() || externalTowerId.ValidId())
    {
        Duco::StatisticFilters filters(*otherDatabase);
        if (externalRingerId.ValidId())
        {
            filters.SetRinger(true, externalRingerId);
        }
        if (externalTowerId.ValidId())
        {
            filters.SetTower(true, externalTowerId);
        }
        otherDatabase->PealsDatabase().AllMatches(filters, externalPealIds);
    }
    callback.Complete(false);
}

void MergeDatabase::ImportExportFailed(bool /*internalising*/, int errorCode)
{
    callback.Complete(errorCode != 0);
}

void
MergeDatabase::Merge(const std::set<Duco::ObjectId>& pealIdsToImport)
{
    callback.Initialised();

    // check that all methods, towers and ringers are used in the peals to be imported
    std::set<Duco::ObjectId>::const_iterator pealIt = externalPealIds.begin();
    while (pealIt != externalPealIds.end())
    {
        if (pealIdsToImport.size() == 0 || pealIdsToImport.find(*pealIt) != pealIdsToImport.end())
        {
            const Peal* const thePeal = otherDatabase->PealsDatabase().FindPeal(*pealIt);
            if (thePeal != NULL)
            {
                requiredMethods.insert(thePeal->MethodId());
                requiredTowers.insert(thePeal->TowerId());
                if (thePeal->AssociationId().ValidId())
                    requiredAssociations.insert(thePeal->AssociationId());
                std::set<Duco::ObjectId> thisPealRingerIds;
                thePeal->RingerIds(thisPealRingerIds);
                requiredRingers.insert(thisPealRingerIds.begin(), thisPealRingerIds.end());
                if (thePeal->CompositionId().ValidId())
                    requiredCompositions.insert(thePeal->CompositionId());
                if (thePeal->PictureId().ValidId())
                    requiredPictures.insert(thePeal->PictureId());
                if (thePeal->SeriesId().ValidId())
                    requiredMethodSeries.insert(thePeal->SeriesId());
            }
        }
        ++pealIt;
    }
    for (auto const& towerIt : requiredTowers)
    {
        const Tower* const theTower = otherDatabase->TowersDatabase().FindTower(towerIt);
        if (theTower != NULL)
        {
            if (theTower->PictureId().ValidId())
                requiredPictures.insert(theTower->PictureId());

            if (theTower->LinkedTowerId().ValidId())
                requiredTowers.insert(theTower->LinkedTowerId());
        }
    }

    float objectsProcessed (0);
    float objectsToProcess = float(requiredRingers.size() + requiredMethods.size() + requiredTowers.size() + externalPealIds.size() + requiredAssociations.size() +
        requiredCompositions.size() + requiredMethodSeries.size() + requiredPictures.size());

  {
    // Add missing order names
    std::list<unsigned int>::const_iterator it = externalOrderNames.begin();
    while (it != externalOrderNames.end())
    {
        std::wstring name;
        if (otherDatabase->MethodsDatabase().FindOrderName(*it, name))
        {
            currentDatabase.MethodsDatabase().AddOrderName(*it, name);
        }
        ++it;
    }
  }
  {
      // Merge missing Associations
      std::set<Duco::ObjectId>::const_iterator associationIt = requiredAssociations.begin();
      while (associationIt != requiredAssociations.end())
      {
          std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator it = externalAssociationIdsToInternal.find(*associationIt);
          if (it == externalAssociationIdsToInternal.end())
          {
              const Association* const theAssociation = otherDatabase->AssociationsDatabase().FindAssociation(*associationIt);
              if (theAssociation != NULL)
              {
                  Association newAssociation(KNoId, *theAssociation);
                  Duco::ObjectId newAssociationId = currentDatabase.AssociationsDatabase().AddObject(newAssociation);
                  if (newAssociationId.ValidId())
                  {
                      std::pair<Duco::ObjectId, Duco::ObjectId> newObject(*associationIt, newAssociationId);
                      externalAssociationIdsToInternal.insert(newObject);
                  }
              }
          }
          ++associationIt;
          ++objectsProcessed;
          callback.Step(int(objectsProcessed / objectsToProcess * 100));
      }
  }
  {
    // Merge missing ringers
    std::set<Duco::ObjectId>::const_iterator ringerIt = requiredRingers.begin();
    while (ringerIt != requiredRingers.end())
    {
        std::map<Duco::ObjectId,Duco::ObjectId>::const_iterator it = externalRingerIdsToInternal.find(*ringerIt);
        if (it == externalRingerIdsToInternal.end())
        {
            const Ringer* const theRinger = otherDatabase->RingersDatabase().FindRinger(*ringerIt);
            if (theRinger != NULL)
            {
                Ringer newRinger (KNoId, *theRinger);
                Duco::ObjectId newRingerId = currentDatabase.RingersDatabase().AddObject(newRinger);
                if (newRingerId.ValidId())
                {
                    std::pair<Duco::ObjectId,Duco::ObjectId> newObject (*ringerIt, newRingerId);
                    externalRingerIdsToInternal.insert(newObject);
                }
            }
        }
        ++ringerIt;
        ++objectsProcessed;
        callback.Step(int(objectsProcessed /objectsToProcess * 100));
    }
  }
  {
      // Check merging method series to additional methods
      std::set<Duco::ObjectId>::const_iterator methodSeriesIt = requiredMethodSeries.begin();
      while (methodSeriesIt != requiredMethodSeries.end())
      {
            const MethodSeries* const theMethodSeries = otherDatabase->MethodSeriesDatabase().FindMethodSeries(*methodSeriesIt);
            if (theMethodSeries != NULL)
            {
                const std::set<Duco::ObjectId> methodIds = theMethodSeries->Methods();
                std::set<Duco::ObjectId>::const_iterator methodIdIt = methodIds.begin();
                while (methodIdIt != methodIds.end())
                {
                    const Method* const nextMethod = otherDatabase->MethodsDatabase().FindMethod(*methodIdIt);
                    if (nextMethod != NULL)
                    {
                        Duco::ObjectId internalMethodId = currentDatabase.MethodsDatabase().FindDuplicateObject(*nextMethod, currentDatabase);
                        if (internalMethodId.ValidId())
                        {
                            std::pair<Duco::ObjectId, Duco::ObjectId> newObject(*methodIdIt, internalMethodId);
                            externalMethodIdsToInternal.insert(newObject);
                        }
                        else
                        {
                            requiredMethods.insert(*methodIdIt);
                        }
                    }
                    ++methodIdIt;
                }
            }
            ++methodSeriesIt;
      }
  }
  {
    // Merge missing methods
    std::set<Duco::ObjectId>::const_iterator methodIt = requiredMethods.begin();
    while (methodIt != requiredMethods.end())
    {
        std::map<Duco::ObjectId,Duco::ObjectId>::const_iterator it = externalMethodIdsToInternal.find(*methodIt);
        if (it == externalMethodIdsToInternal.end())
        {
            const Method* const theMethod = otherDatabase->MethodsDatabase().FindMethod(*methodIt);
            if (theMethod != NULL)
            {
                Method newMethod (KNoId, *theMethod);
                Duco::ObjectId newMethodId = currentDatabase.MethodsDatabase().AddObject(newMethod);
                if (newMethodId.ValidId())
                {
                    std::pair<Duco::ObjectId,Duco::ObjectId> newObject (theMethod->Id(), newMethodId);
                    externalMethodIdsToInternal.insert(newObject);
                }
            }
        }
        ++methodIt;
        ++objectsProcessed;
        callback.Step(int(objectsProcessed / objectsToProcess * 100));
    }
  }
  {
      // Merge missing methods series
      std::set<Duco::ObjectId>::const_iterator methodSeriesIt = requiredMethodSeries.begin();
      while (methodSeriesIt != requiredMethodSeries.end())
      {
          std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator it = externalMethodSeriesIdsToInternal.find(*methodSeriesIt);
          if (it == externalMethodSeriesIdsToInternal.end())
          {
              const MethodSeries* const theMethodSeries = otherDatabase->MethodSeriesDatabase().FindMethodSeries(*methodSeriesIt);
              if (theMethodSeries != NULL)
              {
                  MethodSeries newMethodSeries(KNoId, *theMethodSeries);
                  // Convert method IDs
                  newMethodSeries.ReplaceMethods(externalMethodIdsToInternal);
                  Duco::ObjectId newMethodSeriesId = currentDatabase.MethodSeriesDatabase().AddObject(newMethodSeries);
                  if (newMethodSeriesId.ValidId())
                  {
                      std::pair<Duco::ObjectId, Duco::ObjectId> newObject(theMethodSeries->Id(), newMethodSeriesId);
                      externalMethodSeriesIdsToInternal.insert(newObject);
                  }
              }
          }
          ++methodSeriesIt;
          ++objectsProcessed;
          callback.Step(int(objectsProcessed / objectsToProcess * 100));
      }
  }
  {
      // Merge missing compositions
      std::set<Duco::ObjectId>::const_iterator compositionIt = requiredCompositions.begin();
      while (compositionIt != requiredCompositions.end())
      {
          std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator it = externalCompositionIdsToInternal.find(*compositionIt);
          if (it == externalCompositionIdsToInternal.end())
          {
              const Composition* const theComposition = otherDatabase->CompositionsDatabase().FindComposition(*compositionIt);
              if (theComposition != NULL)
              {
                  Composition newComposition(KNoId, *theComposition);
                  // map method to internal method id
                  std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator newMethodId = externalMethodIdsToInternal.find(theComposition->MethodId());
                  if (newMethodId != externalMethodIdsToInternal.end())
                  {
                      newComposition.SetMethodId(newMethodId->second);
                  }
                  Duco::ObjectId newCompositionId = currentDatabase.CompositionsDatabase().AddObject(newComposition);
                  if (newCompositionId.ValidId())
                  {
                      std::pair<Duco::ObjectId, Duco::ObjectId> newObject(theComposition->Id(), newCompositionId);
                      externalCompositionIdsToInternal.insert(newObject);
                  }
              }
          }
          ++compositionIt;
          ++objectsProcessed;
          callback.Step(int(objectsProcessed / objectsToProcess * 100));
      }
  }
  {
      // Merge missing pictures
      std::set<Duco::ObjectId>::const_iterator pictureIt = requiredPictures.begin();
      while (pictureIt != requiredPictures.end())
      {
          std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator it = externalPictureIdsToInternal.find(*pictureIt);
          if (it == externalPictureIdsToInternal.end())
          {
              const Picture* const thePicture = otherDatabase->PicturesDatabase().FindPicture(*pictureIt);
              if (thePicture != NULL)
              {
                  Picture newPicture(KNoId, *thePicture);
                  Duco::ObjectId newPictureId = currentDatabase.PicturesDatabase().AddObject(newPicture);
                  if (newPictureId.ValidId())
                  {
                      std::pair<Duco::ObjectId, Duco::ObjectId> newObject(thePicture->Id(), newPictureId);
                      externalPictureIdsToInternal.insert(newObject);
                  }
              }
          }
          ++pictureIt;
          ++objectsProcessed;
          callback.Step(int(objectsProcessed / objectsToProcess * 100));
      }
  }
  {
    // Merge missing towers
    std::set<Duco::ObjectId>::const_iterator towerIt = requiredTowers.begin();
    while (towerIt != requiredTowers.end())
    {
        std::map<Duco::ObjectId,Duco::ObjectId>::const_iterator it = externalTowerIdsToInternal.find(*towerIt);
        if (it == externalTowerIdsToInternal.end())
        {
            const Tower* const theTower = otherDatabase->TowersDatabase().FindTower(*towerIt);
            if (theTower != NULL)
            {
                Tower newTower (KNoId, *theTower);
                std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator pictureIdMapping = externalPictureIdsToInternal.find(theTower->PictureId());
                if (pictureIdMapping != externalPictureIdsToInternal.end())
                {
                    newTower.SetPictureId(pictureIdMapping->second);
                }
                newTower.SetLinkedTowerId(KNoId);
                Duco::ObjectId newTowerId = currentDatabase.TowersDatabase().AddObject(newTower);
                if (newTowerId.ValidId())
                {
                    std::pair<Duco::ObjectId,Duco::ObjectId> newObject (*towerIt, newTowerId);
                    externalTowerIdsToInternal.insert(newObject);
                }
                requiredPictures.insert(newTower.PictureId());
            }
        }
        else
        {
            const Tower* const otherTower = otherDatabase->TowersDatabase().FindTower(it->first);
            currentDatabase.TowersDatabase().MergeTowerRings(it->second, *otherTower);
        }

        ++towerIt;
        ++objectsProcessed;
        callback.Step(int(objectsProcessed / objectsToProcess * 100));
    }
  }
  {
    // Merge missing peals
    std::set<Duco::ObjectId>::const_iterator it = externalPealIds.begin();
    while (it != externalPealIds.end())
    {
        if (pealIdsToImport.size() == 0 || pealIdsToImport.find(*it) != pealIdsToImport.end())
        {
            const Peal* const thePeal = otherDatabase->PealsDatabase().FindPeal(*it);
            if (thePeal != NULL)
            {
                Peal newPeal(KNoId, *thePeal);
                std::set<Duco::ObjectId> originalConductors = newPeal.Conductors();
                std::set<Duco::ObjectId> newConductors;
                newPeal.ClearConductors();

                // map associations to internal associations
                std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator newAssociationIdIt = externalAssociationIdsToInternal.find(newPeal.AssociationId());
                if (newAssociationIdIt != externalAssociationIdsToInternal.end())
                {
                    newPeal.SetAssociationId(newAssociationIdIt->second);
                }

                // map ringer to internal ringers
                std::set<Duco::ObjectId> ringerIds;
                thePeal->RingerIds(ringerIds);
                std::set<Duco::ObjectId>::const_iterator ringerIt = ringerIds.begin();
                while (ringerIt != ringerIds.end())
                {
                    std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator newRingerIdIt = externalRingerIdsToInternal.find(*ringerIt);
                    if (newRingerIdIt != externalRingerIdsToInternal.end())
                    {
                        std::set<unsigned int> bellsRung;
                        bool strapper;
                        if (thePeal->BellRung(*ringerIt, bellsRung, strapper))
                        {
                            newPeal.SetRingerId(newRingerIdIt->second, *bellsRung.begin(), false);
                            if (originalConductors.find(*ringerIt) != originalConductors.end())
                            {
                                // Add as conductor if the original peal contains this ringer.
                                newConductors.insert(newRingerIdIt->second);
                            }
                        }
                    }
                    ++ringerIt;
                }
                // Save conductors
                std::set<Duco::ObjectId>::const_iterator newConductorIt = newConductors.begin();
                while (newConductorIt != newConductors.end())
                {
                    newPeal.SetConductorId(*newConductors.begin());
                    ++newConductorIt;
                }

                // map method to internal method id
                std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator newMethodId = externalMethodIdsToInternal.find(thePeal->MethodId());
                if (newMethodId != externalMethodIdsToInternal.end())
                {
                    newPeal.SetMethodId(newMethodId->second);
                }
                // map tower to internal tower id
                std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator newTowerId = externalTowerIdsToInternal.find(thePeal->TowerId());
                if (newTowerId != externalTowerIdsToInternal.end())
                {
                    newPeal.SetTowerId(newTowerId->second);
                    const Tower* oldTower = otherDatabase->TowersDatabase().FindTower(newTowerId->first);
                    if (oldTower != NULL)
                    {
                        // Check Ring Id;
                        CheckRingId(newPeal, *thePeal, newTowerId->second, *oldTower);
                    }
                }
                // Map picture Ids
                std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator pictureIdMapping = externalPictureIdsToInternal.find(thePeal->PictureId());
                if (pictureIdMapping != externalPictureIdsToInternal.end())
                {
                    newPeal.SetPictureId(pictureIdMapping->second);
                }
                // Map compsoition Ids
                std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator compositionIdMapping = externalCompositionIdsToInternal.find(thePeal->CompositionId());
                if (compositionIdMapping != externalCompositionIdsToInternal.end())
                {
                    newPeal.SetCompositionId(compositionIdMapping->second);
                }
                // Map Series id
                std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator seriesIdMapping = externalMethodSeriesIdsToInternal.find(thePeal->SeriesId());
                if (seriesIdMapping != externalMethodSeriesIdsToInternal.end())
                {
                    newPeal.SetSeriesId(seriesIdMapping->second);
                }

                currentDatabase.PealsDatabase().AddObject(newPeal);
            }
        }
        ++it;
    }
    ++objectsProcessed;
    callback.Step(int(objectsProcessed / objectsToProcess * 100));
  }

    callback.Complete(false);
}

void
MergeDatabase::CheckRingId(Peal& newPeal, const Peal& oldExternalPeal, const Duco::ObjectId& internalTowerId, const Tower& externalTower)
{
    const Tower* const internalTower = currentDatabase.TowersDatabase().FindTower(internalTowerId);
    if (internalTower != NULL)
    {
        Duco::ObjectId newRingId;
        bool existingRingFound (false);
        const Ring* const externalRing = externalTower.FindRing(oldExternalPeal.RingId());
        if (externalRing != NULL)
        {
            existingRingFound = internalTower->FindSimilarRing(*externalRing, newRingId);
        }
        else
        {
            existingRingFound = internalTower->SuggestRing(newPeal.NoOfBellsRung(currentDatabase), externalRing->TenorWeight(), externalRing->TenorKey(), newRingId, true);
        }
        if (existingRingFound && newRingId.ValidId())
        {
            newPeal.SetRingId(newRingId);
        }
        else
        {
            Tower updatedTower (*internalTower);
            Ring newRing (internalTower->NextFreeRingId(), *externalRing);
            updatedTower.AddRing(newRing);
            newPeal.SetRingId(newRing.Id());
            currentDatabase.TowersDatabase().UpdateObject(updatedTower);
        }
    }
}

const Duco::Peal* const
MergeDatabase::FindPeal(const Duco::ObjectId& id) const
{
    if (otherDatabase != NULL)
    {
        const Peal* const thePeal = otherDatabase->PealsDatabase().FindPeal(id);
        return thePeal;
    }
    return NULL;
}

std::wstring
MergeDatabase::MethodName(const Duco::ObjectId& id) const
{
    std::wstring returnVal;

    if (otherDatabase != NULL)
    {
        const Method* const theMethod = otherDatabase->MethodsDatabase().FindMethod(id);
        if (theMethod != NULL)
        {
            returnVal = theMethod->FullName(*otherDatabase);
        }
    }

    return returnVal;
}

std::wstring
MergeDatabase::AssociationName(const Duco::ObjectId& id) const
{
    std::wstring returnVal;

    if (otherDatabase != NULL)
    {
        const Association* const theAssociation = otherDatabase->AssociationsDatabase().FindAssociation(id);
        if (theAssociation != NULL)
        {
            returnVal = theAssociation->Name();
        }
    }

    return returnVal;
}

std::wstring
MergeDatabase::TowerName(const Duco::ObjectId& id) const
{
    std::wstring returnVal;

    if (otherDatabase != NULL)
    {
        const Tower* const theTower = otherDatabase->TowersDatabase().FindTower(id);
        if (theTower != NULL)
        {
            returnVal = theTower->FullName();
        }
    }

    return returnVal;
}

