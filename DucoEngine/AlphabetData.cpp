#include "AlphabetData.h"
#include "AlphabetLetter.h"
#include "PerformanceDate.h"
#include "ObjectId.h"
#include <cctype>
#include <limits>
#include <algorithm>

using namespace Duco;

AlphabetData::AlphabetData()
{
    for (char i ='a'; i <= 'z'; ++i)
    {
        std::pair<char, Duco::AlphabetLetter*> pair (i, new AlphabetLetter(i));
        letters.insert(pair);
    }
}

AlphabetData::~AlphabetData()
{
    std::map<wchar_t, Duco::AlphabetLetter*>::iterator it = letters.begin();
    while (it != letters.end())
    {
        delete it->second;
        ++it;
    }
}

void
AlphabetData::Reset()
{
    std::map<wchar_t, Duco::AlphabetLetter*>::iterator it = letters.begin();
    while (it != letters.end())
    {
        it->second->Reset();
        ++it;
    }
}

bool
AlphabetData::AddPeal(const std::wstring& methodName, const Duco::ObjectId& pealId, const Duco::ObjectId& methodId, const Duco::PerformanceDate& pealDate, bool uniqueMethods)
{
    bool addedPeal = false;
    if (methodName.length() > 0)
    {
        wchar_t methodLetter = tolower(methodName[0]);
        std::map<wchar_t, Duco::AlphabetLetter*>::iterator it = letters.find(methodLetter);
        if (it != letters.end())
        {
            addedPeal = it->second->AddPeal(methodLetter, pealId, methodId, pealDate, uniqueMethods);
        }
    }
    return addedPeal;
}

unsigned int
AlphabetData::NumberOfCompletedAlphabets() const
{
    unsigned int noOfCompletedAlphabets(std::numeric_limits<unsigned int>::max());
    std::map<wchar_t, Duco::AlphabetLetter*>::const_iterator it = letters.begin();
    while (it != letters.end() && noOfCompletedAlphabets > 0)
    {
        noOfCompletedAlphabets = std::min<unsigned int>(noOfCompletedAlphabets, (unsigned int)it->second->Count());
        ++it;
    }
    return noOfCompletedAlphabets;
}

const AlphabetLetter&
AlphabetData::operator[](wchar_t letter) const
{
    return *letters.at(letter);
}

bool
AlphabetData::Completed(unsigned int circleNumber, Duco::PerformanceDate& completionDate, std::set<Duco::ObjectId>& pealIds) const
{
    completionDate.ResetToEarliest();
    bool completed = true;

    pealIds.clear();

    std::map<wchar_t, Duco::AlphabetLetter*>::const_iterator it = letters.begin();
    while (it != letters.end())
    {
        Duco::ObjectId pealId;
        completed = completed && it->second->RungDate(circleNumber, completionDate, pealId);
        if (pealId.ValidId())
        {
            pealIds.insert(pealId);
        }
        ++it;
    }

    return completed;
}
