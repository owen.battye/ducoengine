#ifndef __PLACENOTATIONOBJECT_H__
#define __PLACENOTATIONOBJECT_H__

#include <string>
#include "ChangeType.h"

namespace Duco
{
    class Change;

class PlaceNotationObject
{
public:
    PlaceNotationObject(const std::wstring& newStringRepresentation, TChangeType newType);
    PlaceNotationObject(const PlaceNotationObject& other);
    virtual ~PlaceNotationObject();

    virtual PlaceNotationObject* Realloc() const = 0;
    virtual bool RequiresSeperator() const = 0;
    virtual inline const std::wstring& Str() const;
    virtual unsigned int BestStartingLead() const = 0;

    virtual Change ProcessRow(const Change& change) const = 0;

    inline bool IsLeadEnd() const;
    inline bool IsLeadHead() const;
    inline void SetLeadEnd();
    inline void SetLeadHead();

protected:
    std::wstring     stringRepresentation;
    TChangeType     changeType;
};

const std::wstring&
PlaceNotationObject::Str() const
{
    return stringRepresentation;
}

bool
PlaceNotationObject::IsLeadEnd() const
{
    return changeType == ELeadEnd;
}

bool
PlaceNotationObject::IsLeadHead() const
{
    return changeType == ELeadHead;
}

void
PlaceNotationObject::SetLeadEnd()
{
    changeType = ELeadEnd;
}

void
PlaceNotationObject::SetLeadHead()
{
    changeType = ELeadHead;
}

}

#endif //!__PLACENOTATIONOBJECT_H__
