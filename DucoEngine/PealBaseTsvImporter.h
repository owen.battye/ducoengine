#ifndef __PEALBASETSVIMPORTER_H__
#define __PEALBASETSVIMPORTER_H__

#include <string>
#include <map>
#include "DucoEngineCommon.h"
#include "PerformanceDate.h"
#include "ObjectId.h"
#include "FileImporter.h"

namespace Duco
{
    class RingingDatabase;
    class Peal;
    class ProgressCallback;

class PealBaseTsvImporter : public FileImporter
{
public:
    DllExport PealBaseTsvImporter(Duco::ProgressCallback& theCallback, Duco::RingingDatabase& theDatabase);
    DllExport ~PealBaseTsvImporter();

protected: // from FileImporter
    void GetTotalFileSize(const std::string& fileName);
    bool ProcessPeal(const std::wstring& nextLine);
    void ReadFiles(const std::string& fileName);
    bool EnforceQuotes() const;

protected:
    bool FindFileNames(const std::string& fileName);
    bool ProcessTower(const std::wstring& nextLine);
    bool ProcessRinger(const std::wstring& nextLine);

private:
    Duco::PerformanceDate   lastPealDate;
    unsigned int            pealsOnDate;
    
    std::string             bandsFileName;
    std::string             pealsFileName;
    std::string             towersFileName;
    float                   bandsFilesize;
    float                   towersFilesize;
    float                   pealsFilesize;

    struct TowerInfo
    {
        std::wstring     name;
        std::wstring     town;
        std::wstring     county;
        bool            handbell;
        Duco::ObjectId  towerId;
    };
    std::map<unsigned int, TowerInfo*> towers;
    struct RingerInfo
    {
        std::wstring     name;
        unsigned int    bell;
        bool            conductor;
        bool            handbell;
    };
    std::multimap<unsigned int, RingerInfo*> ringers;

    enum ProcessStage
    {
        ENotStarted,
        EBands,
        ETowers,
        EPeals
    }   processingStage;
};

}

#endif //!__PEALBASETSVIMPORTER_H__
