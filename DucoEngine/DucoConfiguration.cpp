#include "DucoConfiguration.h"

#include <algorithm>
#include <stdlib.h>
#include <config.h>
#include "DucoEngineUtils.h"
#include <sstream>
#define KDucoEngineVersionNumber L"2025.3.1"
const unsigned int KDucoEngineLatestDatabaseVersionNumber = 52;

using namespace Duco;
using namespace rude;

namespace Duco
{
#define KSETTINGSECTIONNAME_GENERALSETTINGS                 "General Settings"
#define KSETTINGNAME_INCLUDEWARNINGSINVALIDATION            "IncludeWarningsInObjectValidation"
#define KSETTINGNAME_DISABLEFEATURESTOIMPROVEPERFORMANCE    "DisableFeaturesToImprovePerformance"
#define KSETTINGNAME_IMPORTNEWMETHODDURINGPEALCREATE        "ImportNewMethodDuringPealCreate"
#define KSETTINGNAME_CHECKFORNEXTDUCOVERSION                "CheckForNextDucoVersion"
#define KSETTINGNAME_INCLUDEDUCOOBJECTSDURINGXMLSAVE        "IncludeDucoObjectsDuringXMLSave"

#define KSETTINGSECTIONNAME_EXTERNALFILES                   "External files"
#define KSETTINGNAME_LASTXMLCOLLECTIONSFILENAME             "LastXmlCollectionsFileName"
#define KSETTINGNAME_LASTDOVEDATAFILENAME                   "LastDoveDataFileName"

#define KSETTINGSECTIONNAME_IMAGESETTINGS                   "Image settings"
#define KSETTINGNAME_DATABASEMAXIMUMSIZE                    "DatabaseMaximumSize"
#define KSETTINGNAME_MAXIMUMIMAGESIZE                       "ImageScaleAboveSize"
#define KSETTINGNAME_IMADEMAXDIMENSION                      "MaximumImageDimensions"
#define KSETTINGNAME_IMAGEMAXRESOLUTION                     "MaximumImageResolution"
#define KSETTINGNAME_IMAGECOLOURDEPTH                       "MaximumImageColourDepth"

#define KSETTINGSECTIONNAME_MAPPINGMSETTINGS                "Mapping settings"
//#define KSETTINGGOOGLEAPIMAPKEY                             "GoogleApi"
#define KSETTINGMAPBOXAPIMAPKEY                             "MapBoxApi"
#define KSETTINGCHROMEZOOM                                  "ChromeZoom"

    const unsigned long long DefaultMaximumDatabaseSize = 37058560; //35MB
    const unsigned long long DefaultMaximumImageSize = 1048576; // 1MB
    const double DefaultImageMaxDimension = 450;
    const double DefaultImageMaxResolution = 72;
    const int DefaultImageColourDepth = 24;
    unsigned int DefaultZoomLevel = 1;
    std::string DefaultMapBoxApiKey = "pk.eyJ1IjoiYmVsbHJpbmdlciIsImEiOiJjbDY0MXFreTMwdXM0M2txazR6emZ1NWFkIn0.zMVsJCK0GZGPcjb-kH0YvA";
    //std::string DefaultGoogleApiKey = "AIzaSyBFtl2FQE83khFw959m_aiJIKMlgfeKRKw";
}

DucoConfiguration::DucoConfiguration()
    :   includeWarningsInValidation(true),
        disablePerformance(false),
        importNewMethod(false),
        checkForNextVersion(true),
        exportDucoIdsWithXml(false),
        xmlMethodCollectionFile("allmeths.xml"),
        doveDataFile("dove.txt"),
        databaseMaximumSize(DefaultMaximumDatabaseSize),
        maximumImageSize(DefaultMaximumImageSize),
        imageMaxDimension(DefaultImageMaxDimension),
        imageMaxResolution(DefaultImageMaxResolution),
        imageColourDepth(DefaultImageColourDepth),
        mapBoxApiKey(DefaultMapBoxApiKey),
//        googleApiKey(DefaultGoogleApiKey),
        chromeZoomLevel(DefaultZoomLevel)
{
    configFile = new Config();
}

DucoConfiguration::~DucoConfiguration()
{
    delete configFile;
}

bool
DucoConfiguration::Load(const std::string& filePath, const std::string& fileName)
{
    configFileName = filePath;
    configFileName.append(fileName);
    if (!configFile->load(configFileName.c_str()))
    {
        return false;
    }

    configFile->setSection(KSETTINGSECTIONNAME_GENERALSETTINGS, true);
    if (configFile->exists(KSETTINGNAME_INCLUDEWARNINGSINVALIDATION))
    {
        includeWarningsInValidation = configFile->getBoolValue(KSETTINGNAME_INCLUDEWARNINGSINVALIDATION);
    }
    if (configFile->exists(KSETTINGNAME_DISABLEFEATURESTOIMPROVEPERFORMANCE))
    {
        disablePerformance = configFile->getBoolValue(KSETTINGNAME_DISABLEFEATURESTOIMPROVEPERFORMANCE);
    }
    if (configFile->exists(KSETTINGNAME_IMPORTNEWMETHODDURINGPEALCREATE))
    {
        importNewMethod = configFile->getBoolValue(KSETTINGNAME_IMPORTNEWMETHODDURINGPEALCREATE);
    }
    if (configFile->exists(KSETTINGNAME_CHECKFORNEXTDUCOVERSION))
    {
        checkForNextVersion = configFile->getBoolValue(KSETTINGNAME_CHECKFORNEXTDUCOVERSION);
    }
    if (configFile->exists(KSETTINGNAME_INCLUDEDUCOOBJECTSDURINGXMLSAVE))
    {
        exportDucoIdsWithXml = configFile->getBoolValue(KSETTINGNAME_INCLUDEDUCOOBJECTSDURINGXMLSAVE);
    }

    configFile->setSection(KSETTINGSECTIONNAME_IMAGESETTINGS, true);
    if (configFile->exists(KSETTINGNAME_DATABASEMAXIMUMSIZE))
    {
        databaseMaximumSize = configFile->getLongLongIntValue(KSETTINGNAME_DATABASEMAXIMUMSIZE);
    }
    if (configFile->exists(KSETTINGNAME_MAXIMUMIMAGESIZE))
    {
        maximumImageSize = configFile->getLongLongIntValue(KSETTINGNAME_MAXIMUMIMAGESIZE);
    }
    if (configFile->exists(KSETTINGNAME_IMADEMAXDIMENSION))
    {
        imageMaxDimension = configFile->getDoubleValue(KSETTINGNAME_IMADEMAXDIMENSION);
    }
    if (configFile->exists(KSETTINGNAME_IMAGEMAXRESOLUTION))
    {
        imageMaxResolution = configFile->getDoubleValue(KSETTINGNAME_IMAGEMAXRESOLUTION);
    }
    if (configFile->exists(KSETTINGNAME_IMAGECOLOURDEPTH))
    {
        imageColourDepth = configFile->getIntValue(KSETTINGNAME_IMAGECOLOURDEPTH);
    }

    configFile->setSection(KSETTINGSECTIONNAME_EXTERNALFILES, true);
    if (configFile->exists(KSETTINGNAME_LASTXMLCOLLECTIONSFILENAME))
    {
        xmlMethodCollectionFile = configFile->getStringValue(KSETTINGNAME_LASTXMLCOLLECTIONSFILENAME);
    }
    if (configFile->exists(KSETTINGNAME_LASTDOVEDATAFILENAME))
    {
        doveDataFile = configFile->getStringValue(KSETTINGNAME_LASTDOVEDATAFILENAME);
    }

    return true;
}

bool
DucoConfiguration::Save(const std::string& newFilename)
{
    configFileName = newFilename;
    return Save();
}

bool
DucoConfiguration::Save()
{
    configFile->setSection(KSETTINGSECTIONNAME_GENERALSETTINGS, true);
    configFile->setBoolValue(KSETTINGNAME_INCLUDEWARNINGSINVALIDATION, includeWarningsInValidation);
    configFile->setBoolValue(KSETTINGNAME_DISABLEFEATURESTOIMPROVEPERFORMANCE, disablePerformance);
    configFile->setBoolValue(KSETTINGNAME_IMPORTNEWMETHODDURINGPEALCREATE, importNewMethod);
    configFile->setBoolValue(KSETTINGNAME_CHECKFORNEXTDUCOVERSION, checkForNextVersion);
    configFile->setBoolValue(KSETTINGNAME_INCLUDEDUCOOBJECTSDURINGXMLSAVE, exportDucoIdsWithXml);

    CheckNonDefaultAndWrite(*configFile, KSETTINGSECTIONNAME_IMAGESETTINGS, KSETTINGNAME_DATABASEMAXIMUMSIZE, databaseMaximumSize, DefaultMaximumDatabaseSize);
    CheckNonDefaultAndWrite(*configFile, KSETTINGSECTIONNAME_IMAGESETTINGS, KSETTINGNAME_MAXIMUMIMAGESIZE, maximumImageSize, DefaultMaximumImageSize);
    CheckNonDefaultAndWrite(*configFile, KSETTINGSECTIONNAME_IMAGESETTINGS, KSETTINGNAME_IMADEMAXDIMENSION, imageMaxDimension, DefaultImageMaxDimension);
    CheckNonDefaultAndWrite(*configFile, KSETTINGSECTIONNAME_IMAGESETTINGS, KSETTINGNAME_IMAGEMAXRESOLUTION, imageMaxResolution, DefaultImageMaxResolution);
    CheckNonDefaultAndWrite(*configFile, KSETTINGSECTIONNAME_IMAGESETTINGS, KSETTINGNAME_IMAGECOLOURDEPTH, imageColourDepth, DefaultImageColourDepth);

    configFile->setSection(KSETTINGSECTIONNAME_EXTERNALFILES, true);
    configFile->setStringValue(KSETTINGNAME_LASTXMLCOLLECTIONSFILENAME, xmlMethodCollectionFile.c_str());
    configFile->setStringValue(KSETTINGNAME_LASTDOVEDATAFILENAME, doveDataFile.c_str());

    if (configFile->setSection(KSETTINGSECTIONNAME_IMAGESETTINGS, false))
    {
        if (configFile->getNumDataMembers() == 0)
        {
            configFile->deleteSection(KSETTINGSECTIONNAME_IMAGESETTINGS);
        }
    }

    configFile->setSection(KSETTINGSECTIONNAME_MAPPINGMSETTINGS, true);
    CheckNonDefaultAndWrite(*configFile, KSETTINGSECTIONNAME_IMAGESETTINGS, KSETTINGCHROMEZOOM, chromeZoomLevel, DefaultZoomLevel);

    return configFile->save(configFileName.c_str());
}

bool
DucoConfiguration::CheckNonDefaultAndWrite(rude::Config& configFile, const char*sectionName, const char* settingName, const double value, const double defaultValue)
{
    if (!configFile.setSection(sectionName, value != defaultValue))
    {
        if (value != defaultValue)
            return false;

        return true;
    }
    if (value != defaultValue)
    {
        configFile.setDoubleValue(settingName, value);
        return true;
    }
    else if (configFile.exists(settingName))
    {
        configFile.deleteData(settingName);
        return true;
    }
    return false;
}

bool
DucoConfiguration::CheckNonDefaultAndWrite(rude::Config& configFile, const char*sectionName, const char*settingName, const int value, const int defaultValue)
{
    if (!configFile.setSection(sectionName, value != defaultValue))
    {
        if (value != defaultValue)
            return false;

        return true;
    }
    if (value != defaultValue)
    {
        configFile.setIntValue(settingName, value);
        return true;
    }
    else
    {
        configFile.deleteData(settingName);
        return true;
    }
    return false;
}

bool
DucoConfiguration::CheckNonDefaultAndWrite(rude::Config& configFile, const char*sectionName, const char*settingName, const unsigned long long value, const unsigned long long defaultValue)
{
    if (!configFile.setSection(sectionName, value != defaultValue))
    {
        if (value != defaultValue)
            return false;

        return true;
    }
    if (value != defaultValue)
    {
        configFile.setLongLongIntValue(settingName, value);
        return true;
    }
    else
    {
        configFile.deleteData(settingName);
        return true;
    }
    return false;
}


void
DucoConfiguration::SetImportNewMethod(bool newImportNewMethod)
{
    importNewMethod = newImportNewMethod;
}

bool
DucoConfiguration::ImportNewMethod() const
{
    return importNewMethod;
}

void
DucoConfiguration::SetXmlMethodCollectionFile(const std::string& newXmlMethodsFile)
{
    xmlMethodCollectionFile = newXmlMethodsFile;
}

const std::string&
DucoConfiguration::XmlMethodCollectionFile() const
{
    return xmlMethodCollectionFile;
}


void
DucoConfiguration::SetDoveDataFile(const std::string& newDoveDataFile)
{
    doveDataFile = newDoveDataFile;
}

const std::string&
DucoConfiguration::DoveDataFile() const
{
    return doveDataFile;
}

void
DucoConfiguration::EnableNextVersion()
{
    checkForNextVersion = true;
}

void
DucoConfiguration::DisableNextVersion()
{
    checkForNextVersion = false;
}

bool
DucoConfiguration::CheckForNextVersion() const
{
    return checkForNextVersion;
}


void
DucoConfiguration::SetIncludeWarningsInValidation(bool newValidationSetting)
{
    includeWarningsInValidation = newValidationSetting;
}

bool
DucoConfiguration::IncludeWarningsInValidation() const
{
    return includeWarningsInValidation;
}


void
DucoConfiguration::SetExportDucoIdsWithXml(bool newValue)
{
    exportDucoIdsWithXml = newValue;
}

bool
DucoConfiguration::ExportDucoIdsWithXml() const
{
    return exportDucoIdsWithXml;
}

bool
DucoConfiguration::DisableFeaturesForPerformance() const
{
    return disablePerformance;
}

bool
DucoConfiguration::DisableFeaturesForPerformance(size_t noOfObjects) const
{
    if (noOfObjects < 200)
        return false;

    return disablePerformance;
}

void
DucoConfiguration::SetDisableFeaturesForPerformance(bool newValue)
{
    disablePerformance = newValue;
}

void
DucoConfiguration::ClearUISettings()
{
    includeWarningsInValidation = true;
    importNewMethod = false;
    checkForNextVersion = true;
    disablePerformance = false;
    chromeZoomLevel = DefaultZoomLevel;
    mapBoxApiKey = DefaultMapBoxApiKey;
}

Duco::DucoVersionNumber
DucoConfiguration::VersionNumber()
{
    return DucoVersionNumber(KDucoEngineVersionNumber);
}

unsigned int
DucoConfiguration::LatestDatabaseVersion()
{
    return KDucoEngineLatestDatabaseVersionNumber;
}

unsigned long long
DucoConfiguration::DatabaseMaximumSize() const
{
    return databaseMaximumSize;
}

unsigned long long
DucoConfiguration::MaximumImageSize() const
{
    return maximumImageSize;
}

double
DucoConfiguration::ImageMaxDimension() const
{
    return imageMaxDimension;
}

double
DucoConfiguration::ImageMaxResolution() const
{
    return imageMaxResolution;
}

int
DucoConfiguration::ImageColourDepth() const
{
    std::set<int> acceptableColourDepths = { 1, 4, 16, 24, 32, 48 };
    if (acceptableColourDepths.find(imageColourDepth) == acceptableColourDepths.end())
    {
        std::set<int>::const_iterator it = std::lower_bound(acceptableColourDepths.begin(), acceptableColourDepths.end(), imageColourDepth);
        if (it != acceptableColourDepths.end())
        {
            --it;
        }
        if (it == acceptableColourDepths.end())
        {
            imageColourDepth = DefaultImageColourDepth;
        }
        else
        {
            imageColourDepth = *it;
        }
    }
    return imageColourDepth;
}

std::wstring
DucoConfiguration::MapBoxApi() const
{
    std::wstring wsTmp(mapBoxApiKey.begin(), mapBoxApiKey.end());
    return wsTmp;
}

void
DucoConfiguration::SetMapBoxApi(const std::string& newSetting)
{
    mapBoxApiKey = newSetting;
}

double
DucoConfiguration::ChromeZoomLevel() const
{
    return chromeZoomLevel;
}

void
DucoConfiguration::SetChromeZoomLevel(double zoomLevel)
{
    chromeZoomLevel = zoomLevel;
}
