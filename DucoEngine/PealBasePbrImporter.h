#ifndef __PEALBASEPBRIMPORTER_H__
#define __PEALBASEPBRIMPORTER_H__

#include <string>
#include "DucoEngineCommon.h"
#include "FileImporter.h"

namespace Duco
{
    class RingingDatabase;
    class Peal;
    class ProgressCallback;

class PealBasePbrImporter : public FileImporter
{
public:
    DllExport PealBasePbrImporter(Duco::ProgressCallback& theCallback, Duco::RingingDatabase& theDatabase);
    DllExport ~PealBasePbrImporter();

protected: // from FileImporter
    virtual void GetTotalFileSize(const std::string& fileName);
    virtual bool ProcessPeal(const std::wstring& nextLine);
    virtual bool EnforceQuotes() const;

protected: // new functions
    bool FindOrCreateNewRinger(Duco::Peal& newPeal, const std::wstring& ringerStr, unsigned int bellNo, bool asStrapper = false);

};

}

#endif //!__PEALBASEPBRIMPORTER_H__
