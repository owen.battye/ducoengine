#include "DatabaseSettings.h"

#include "DatabaseWriter.h"
#include "DatabaseReader.h"
#include "DucoConfiguration.h"
#include "DucoEngineLog.h"

using namespace Duco;

#define defaultBellBoardUrl L"https://bb.ringingworld.co.uk"
#define defaultBellBoardUrlViewSuffix L"/view.php?id="
#define defaultTowerPealbaseUrl L"https://www.pealbase.co.uk/pealbase/tower.php?tid="
#define defaultMethodPealbaseUrl L""
#define defaultFelsteadUrl L"https://felstead.cccbr.org.uk/"
#define defaultFelsteadTowerSuffixUrl L"tbid.php?tid="
#define defaultFelsteadCountySuffixUrl L"county.php?yid="
#define defaultAssociationPealBaseUrl L"https://www.pealbase.co.uk/pealbase/guild.php?gid="

#define defaultDoveUrl L"https://dove.cccbr.org.uk/"
//#define defaultGoogleUrl L"http://maps.google.co.uk/maps?f=q&source=s_q&hl=en&geocode=&q="
#define defaultCompLibUrl L"https://complib.org/composition/search"

DatabaseSettings::DatabaseSettings() 
:   bellBoardURL(defaultBellBoardUrl), bellBoardViewSuffixURL(defaultBellBoardUrlViewSuffix), bellBoardAutoSetRingingWorldPage(true),
    doveURL(defaultDoveUrl), viewWebsite(EViewOnBellBoard), googleURL(L""), useMapBox(true),
    lastNameFirst(true), alphabeticReordering (true), showTrebleInDrawing(true), showGridLinesInDrawing(true),
    showFullGrid(true), showBobsAndSingles(true), lineType (ENormalLine), webSite(EPaulGraupnerBlueLine),
    printFontSize(6), usePrintPreview(true), pealPrintFormat (ERingingWorldStyle), printSurnamesFirst(true),
    quarterpealDatabase(true), allowErrors(false), settingsChanged(false),
    pealbaseTowerURL(defaultTowerPealbaseUrl), felsteadURL(defaultFelsteadUrl),
    pealbaseMethodURL(defaultMethodPealbaseUrl), pealbaseAssociation(defaultAssociationPealBaseUrl), 
    keepTowersWithRungDate(false), compLibURL(defaultCompLibUrl),
    showAssociationColumn(true), showComposerColumn(true), showConductorColumn(true), showRingColumn(true), showBellRungColumn(true), showReferencesColumn(false)
{
    ClearSettings(quarterpealDatabase);
}

DatabaseSettings::~DatabaseSettings()
{
}

void
DatabaseSettings::ResetWebsiteLinks()
{
    bellBoardURL = defaultBellBoardUrl;
    bellBoardViewSuffixURL = defaultBellBoardUrlViewSuffix;
    felsteadURL = defaultFelsteadUrl;
    pealbaseAssociation = defaultAssociationPealBaseUrl;
    pealbaseTowerURL = defaultTowerPealbaseUrl;
    pealbaseMethodURL = defaultMethodPealbaseUrl;
    doveURL = defaultDoveUrl;
    googleURL = L"";
    compLibURL = defaultCompLibUrl;
}

void
DatabaseSettings::ClearSettings(bool quarterPealDb)
{
    ResetWebsiteLinks();
    viewWebsite = EViewOnBellBoard;
    lastNameFirst = true;
    defaultRinger.ClearId();
    settingsChanged = false;
    alphabeticReordering = true;
    showTrebleInDrawing = true;
    showGridLinesInDrawing = true;
    showFullGrid = true;
    showBobsAndSingles = true;
    lineType = ENormalLine;
    usePrintPreview = true;
    printFontSize = 6;
    webSite = EPaulGraupnerBlueLine;
    allowErrors = false;
    pealPrintFormat = ERingingWorldStyle;
    printSurnamesFirst = true;
    quarterpealDatabase = quarterPealDb;
    keepTowersWithRungDate = false;
    showAssociationColumn = true;
    showComposerColumn = true;
    showConductorColumn = true;
    showRingColumn = true;
    showBellRungColumn = false;
    showReferencesColumn = false;
    disabledPealChecks.reset();
    disabledTowerChecks.reset();
    disabledMethodChecks.reset();
    disabledMethodSeriesChecks.reset();
    disabledCompositionChecks.reset();
    disabledRingerChecks.reset();
    disabledAssociationChecks.reset();
}

void 
DatabaseSettings::Internalise(DatabaseReader& reader, unsigned int databaseVersion)
{
    std::wstring discard;
    if (databaseVersion <= 33)
    {
        reader.ReadString(discard);
    }
    if (databaseVersion >= 17)
    {
        if (databaseVersion <= 33)
        {
            reader.ReadString(discard);
        }
        if (databaseVersion <= 42)
        {
            std::wstring discardOldPealsCoUkUrl;
            reader.ReadString(discardOldPealsCoUkUrl);
        }
        if (databaseVersion >= 23)
        {
            reader.ReadString(bellBoardURL);
            reader.ReadString(bellBoardViewSuffixURL);
            if (databaseVersion >= 49)
            {
                bellBoardAutoSetRingingWorldPage = reader.ReadBool();
            }
        }
        viewWebsite = (TPreferredViewWebsite)reader.ReadInt();
        if (viewWebsite == 0)
        {
            // 0 was campanophile.
            viewWebsite = EViewOnBellBoard;
        }
        else if (viewWebsite == 2)
        {
            // 2 was Peals.co.uk.
            viewWebsite = EViewOnBellBoard;
        }
        if (databaseVersion >= 23 && databaseVersion <= 34)
            reader.ReadInt();
    }
    if (databaseVersion >= 9)
    {
        reader.ReadString(doveURL);
    }
    lastNameFirst = reader.ReadBool();
    defaultRinger = reader.ReadUInt();
    alphabeticReordering = reader.ReadBool();
    showTrebleInDrawing = reader.ReadBool();
    showGridLinesInDrawing = reader.ReadBool();
    webSite = TBlueLineWebsite(reader.ReadInt());
    if (databaseVersion >= 4)
    {
        usePrintPreview = reader.ReadBool();
        printFontSize = reader.ReadUInt();
    }
    else
    {
        usePrintPreview = true;
        printFontSize = 6;
    }
    if (databaseVersion >= 5)
    {
        showFullGrid = reader.ReadBool();
        if (databaseVersion >= 14)
        {
            lineType = TLineType(reader.ReadInt());
            showBobsAndSingles = reader.ReadBool();
        }
        else if (reader.ReadBool())
        {
            lineType = ELongLine;
            showFullGrid = false;
        }
        else
        {
            lineType = ENormalLine;
        }
    }
    else
    {
        showFullGrid = true;
        lineType = ENormalLine;
    }
    if (databaseVersion >= 6)
    {
        allowErrors = reader.ReadBool();
        pealPrintFormat = TPealPrintoutFormat(reader.ReadInt());
        printSurnamesFirst = reader.ReadBool();
        quarterpealDatabase = reader.ReadBool();
    }
    else
    {
        allowErrors = false;
    }
    if (databaseVersion >= 13)
    {
        reader.ReadString(googleURL);
        googleURL.clear(); //deliberately remove google maps
    }
    if (databaseVersion >= 38)
    {
        useMapBox = reader.ReadBool();
        useMapBox = true; // deliberatly removed google maps
    }
    else
    {
        useMapBox = true;
    }
    if (databaseVersion >= 19)
    {
        keepTowersWithRungDate = reader.ReadBool();
    }
    if (databaseVersion >= 22 && databaseVersion <= 42)
    {
        std::wstring discardOldPealsCoUkUrl;
        reader.ReadString(discardOldPealsCoUkUrl);
    }
    if (databaseVersion >= 26)
    {
        reader.ReadString(pealbaseTowerURL);
        if (databaseVersion <= 42)
        {
            std::wstring discardOldPealsCoUkMethodURL;
            reader.ReadString(discardOldPealsCoUkMethodURL);
        }
        reader.ReadString(pealbaseMethodURL);
    }
    if (databaseVersion >= 29)
    {
        reader.ReadString(felsteadURL);
    }
    if (databaseVersion >= 36)
    {
        if (databaseVersion <= 42)
        {
            std::wstring discardOldPealsCoUkAssociationURL;
            reader.ReadString(discardOldPealsCoUkAssociationURL);
        }
        reader.ReadString(pealbaseAssociation);
    }
    if (databaseVersion >= 46)
    {
        reader.ReadString(compLibURL);
    }
    if (databaseVersion >= 50)
    {
        showAssociationColumn = reader.ReadBool();
        showComposerColumn = reader.ReadBool();
        showConductorColumn = reader.ReadBool();
        showRingColumn = reader.ReadBool();
        showBellRungColumn = reader.ReadBool();
        if (databaseVersion >= 51)
        {
            showReferencesColumn = reader.ReadBool();
        }
    }
    else
    {
        showAssociationColumn = true;
        showComposerColumn = true;
        showConductorColumn = true;
        showRingColumn = true;
        showBellRungColumn = DefaultRingerSet();
        showReferencesColumn = false;
    }
    if (databaseVersion >= 52)
    {
        disabledPealChecks = reader.ReadInt();
        disabledTowerChecks = reader.ReadInt();
        disabledMethodChecks = reader.ReadInt();
        disabledMethodSeriesChecks = reader.ReadInt();
        disabledCompositionChecks = reader.ReadInt();
        disabledRingerChecks = reader.ReadInt();
        disabledAssociationChecks = reader.ReadInt();
    }
    else
    {
        disabledPealChecks.reset();
        disabledTowerChecks.reset();
        disabledMethodChecks.reset();
        disabledMethodSeriesChecks.reset();
        disabledCompositionChecks.reset();
        disabledRingerChecks.reset();
        disabledAssociationChecks.reset();
    }

    settingsChanged = false;
    if (databaseVersion < Duco::DucoConfiguration::LatestDatabaseVersion())
    {
        ResetWebsiteLinks();
    }
}

void 
DatabaseSettings::Externalise(DatabaseWriter& writer) const
{
    writer.WriteString(bellBoardURL);
    writer.WriteString(bellBoardViewSuffixURL);
    writer.WriteBool(bellBoardAutoSetRingingWorldPage);
    writer.WriteInt(int(viewWebsite));
    writer.WriteString(doveURL);
    writer.WriteBool(lastNameFirst);
    writer.WriteId(defaultRinger);
    writer.WriteBool(alphabeticReordering);
    writer.WriteBool(showTrebleInDrawing);
    writer.WriteBool(showGridLinesInDrawing);
    writer.WriteInt(int(webSite));
    writer.WriteBool(usePrintPreview);
    writer.WriteInt(printFontSize);
    writer.WriteBool(showFullGrid);
    writer.WriteInt(lineType);
    writer.WriteBool(showBobsAndSingles);
    writer.WriteBool(allowErrors);
    writer.WriteInt(pealPrintFormat);
    writer.WriteBool(printSurnamesFirst);
    writer.WriteBool(quarterpealDatabase);
    writer.WriteString(googleURL);
    writer.WriteBool(useMapBox);
    writer.WriteBool(keepTowersWithRungDate);
    writer.WriteString(pealbaseTowerURL);
    writer.WriteString(pealbaseMethodURL);
    writer.WriteString(felsteadURL);
    writer.WriteString(pealbaseAssociation);
    writer.WriteString(compLibURL);
    writer.WriteBool(showAssociationColumn);
    writer.WriteBool(showComposerColumn);
    writer.WriteBool(showConductorColumn);
    writer.WriteBool(showRingColumn);
    writer.WriteBool(showBellRungColumn);
    writer.WriteBool(showReferencesColumn);
    writer.WriteInt(disabledPealChecks.to_ullong());
    writer.WriteInt(disabledTowerChecks.to_ullong());
    writer.WriteInt(disabledMethodChecks.to_ullong());
    writer.WriteInt(disabledMethodSeriesChecks.to_ullong());
    writer.WriteInt(disabledCompositionChecks.to_ullong());
    writer.WriteInt(disabledRingerChecks.to_ullong());
    writer.WriteInt(disabledAssociationChecks.to_ullong());

    settingsChanged = false;
}

void
DatabaseSettings::SetShowFullGrid(bool newShowFullGrid)
{
    showFullGrid = newShowFullGrid;
    if (showFullGrid && lineType == ELongLine)
    {
        lineType = ENormalLine;
    }
    SetSettingsChanged();
}

void 
DatabaseSettings::SetLineType(const TLineType& newLineType)
{
    lineType = newLineType;
    switch (lineType)
    {
    case ELongLine:
        showFullGrid = false;
    case EAnnotated:
        showGridLinesInDrawing = false;
        break;
    default:
        break;
    }
    SetSettingsChanged();
}

void
DatabaseSettings::SetShowGridLines(bool newShowTreble)
{
    showGridLinesInDrawing = newShowTreble;
    if (showGridLinesInDrawing && lineType != ENormalLine)
    {
        lineType = ENormalLine;
    }
    SetSettingsChanged();
}

void 
DatabaseSettings::SetShowBobsAndSingles(bool newShowBobsAndSingles)
{
    showBobsAndSingles = newShowBobsAndSingles;
    SetSettingsChanged();
}

void
DatabaseSettings::SetDefaultRinger(const Duco::ObjectId& newRinger)
{
    if (defaultRinger != newRinger)
    {
        defaultRinger = newRinger;
        SetSettingsChanged();
    }
}


const std::wstring&
DatabaseSettings::PreferedDownloadWebsiteUrl() const
{
    return BellBoardURL();
}

void
DatabaseSettings::SetBellBoardURL(const std::wstring& newUrl)
{
    bellBoardURL = newUrl;
    SetSettingsChanged();
}

void
DatabaseSettings::SetBellBoardViewSuffixURL(const std::wstring& newSuffix)
{
    bellBoardViewSuffixURL = newSuffix;
    SetSettingsChanged();
}

void
DatabaseSettings::SetPealbaseTowerURL(const std::wstring& newUrl)
{
    pealbaseTowerURL = newUrl;
    SetSettingsChanged();
}

void
DatabaseSettings::SetFelsteadURL(const std::wstring& newUrl)
{
    felsteadURL = newUrl;
    SetSettingsChanged();
}

void
DatabaseSettings::SetCompLibURL(const std::wstring& newUrl)
{
    compLibURL = newUrl;
    SetSettingsChanged();
}

void
DatabaseSettings::SetPealbaseMethodURL(const std::wstring& newUrl)
{
    pealbaseMethodURL = newUrl;
    SetSettingsChanged();
}

void
DatabaseSettings::SetPreferredOnlineWebsite(DatabaseSettings::TPreferredViewWebsite newWebsite)
{
    if (viewWebsite != newWebsite)
    {
        viewWebsite = newWebsite;
        SetSettingsChanged();
    }
}

void
DatabaseSettings::SetLastNameFirst(bool newSetting)
{
    lastNameFirst = newSetting;
    SetSettingsChanged();
}

void
DatabaseSettings::SetAlphabeticReordering(bool newOrdering)
{
    alphabeticReordering = newOrdering;
    SetSettingsChanged();
}

void
DatabaseSettings::SetShowTreble(bool newShowTreble)
{
    showTrebleInDrawing = newShowTreble;
    SetSettingsChanged();
}

void
DatabaseSettings::SetBlueLineWebsite(DatabaseSettings::TBlueLineWebsite newWebSite)
{
    webSite = newWebSite;
    SetSettingsChanged();
}

void
DatabaseSettings::SetPrintPreview(bool newPrintPreview)
{
    usePrintPreview = newPrintPreview;
    SetSettingsChanged();
}

void
DatabaseSettings::SetSettingsChanged() const
{
    settingsChanged = true;
}

void
DatabaseSettings::ResetSettingsChanged() const
{
    settingsChanged = false;
}

void
DatabaseSettings::SetAllowErrors(bool newAllowErrors)
{
    allowErrors = newAllowErrors;
    SetSettingsChanged();
}

void
DatabaseSettings::SetPealPrintFormat(TPealPrintoutFormat newFormat)
{
    pealPrintFormat = newFormat;
    SetSettingsChanged();
}

void
DatabaseSettings::SetPealDatabase(bool newPealDatabase)
{
    quarterpealDatabase = !newPealDatabase;
    SetSettingsChanged();
}

void
DatabaseSettings::SetDoveURL(const std::wstring& newDoveURL)
{
    doveURL = newDoveURL;
    SetSettingsChanged();
}

void
DatabaseSettings::SetGoogleURL(const std::wstring& newGoogleURL)
{
    // Google disabled
    //googleURL = newGoogleURL;
    //SetSettingsChanged();
}

void
DatabaseSettings::SetUseMapBox(bool newUseMapBox)
{
    useMapBox = newUseMapBox;
    SetSettingsChanged();
}

void
DatabaseSettings::SetKeepTowersWithRungDate(bool newKeepTowersWithRungDate)
{
    keepTowersWithRungDate = newKeepTowersWithRungDate;
    SetSettingsChanged();
}

void
DatabaseSettings::SetPealbaseAssociationURL(const std::wstring& newUrl)
{
    pealbaseAssociation = newUrl;
    SetSettingsChanged();
}

void
DatabaseSettings::SetPrintSurnameFirst(bool newPrintSurnameFirst)
{
    SetSettingsChanged();
    printSurnamesFirst = newPrintSurnameFirst;
}

bool
DatabaseSettings::SettingsValid() const
{
    if (bellBoardURL.length() == 0 ||
        bellBoardViewSuffixURL.length() == 0 ||
        doveURL.length() == 0 ||
        pealbaseTowerURL.length() == 0 ||
        felsteadURL.length() == 0 ||
        pealbaseAssociation.length() == 0)
    {
        return false;
    }

    return true;
}

bool
DatabaseSettings::SettingsAllDefault() const
{
    DatabaseSettings defaultSettings;
    defaultSettings.ClearSettings(this->quarterpealDatabase);

    return (*this) == defaultSettings;
}

bool
DatabaseSettings::operator==(const Duco::DatabaseSettings& rhs) const
{
    if (showAssociationColumn == rhs.showAssociationColumn ||
        showComposerColumn == rhs.showComposerColumn ||
        showConductorColumn == rhs.showConductorColumn ||
        showRingColumn == rhs.showRingColumn ||
        showBellRungColumn == rhs.showBellRungColumn ||
        showReferencesColumn == rhs.showReferencesColumn)
    {
        if (bellBoardURL.compare(rhs.bellBoardURL) == 0 &&
            bellBoardViewSuffixURL.compare(rhs.bellBoardViewSuffixURL) == 0 &&
            bellBoardAutoSetRingingWorldPage == rhs.bellBoardAutoSetRingingWorldPage &&
            pealbaseTowerURL.compare(rhs.pealbaseTowerURL) == 0 &&
            felsteadURL.compare(rhs.felsteadURL) == 0 &&
            pealbaseMethodURL.compare(rhs.pealbaseMethodURL) == 0 &&
            pealbaseAssociation.compare(rhs.pealbaseAssociation) == 0)
        {
            if (doveURL.compare(rhs.doveURL) == 0 &&
                useMapBox == rhs.useMapBox &&
                viewWebsite == rhs.viewWebsite &&
                lastNameFirst == rhs.lastNameFirst &&
                defaultRinger == rhs.defaultRinger)
            {
                if (alphabeticReordering == rhs.alphabeticReordering &&
                    showTrebleInDrawing == rhs.showTrebleInDrawing &&
                    showGridLinesInDrawing == rhs.showGridLinesInDrawing &&
                    showFullGrid == rhs.showFullGrid &&
                    showBobsAndSingles == rhs.showBobsAndSingles &&
                    lineType == rhs.lineType &&
                    webSite == rhs.webSite &&
                    usePrintPreview == rhs.usePrintPreview &&
                    allowErrors == rhs.allowErrors &&
                    pealPrintFormat == rhs.pealPrintFormat &&
                    printSurnamesFirst == rhs.printSurnamesFirst &&
                    quarterpealDatabase == rhs.quarterpealDatabase &&
                    keepTowersWithRungDate == rhs.keepTowersWithRungDate)
                {
                    return true;
                }
            }
        }
    }

    return false;
}

std::wstring
DatabaseSettings::BellBoardDownloadUrl(const std::wstring& bellboardId) const
{
    return bellBoardURL + bellBoardViewSuffixURL + bellboardId;
}

const std::wstring&
DatabaseSettings::DoveURL() const
{
    return doveURL;
}

std::wstring
DatabaseSettings::FelsteadTowerURL() const
{
    return felsteadURL + defaultFelsteadTowerSuffixUrl;
}

std::wstring
DatabaseSettings::FelsteadCountyURL() const
{
    return felsteadURL + defaultFelsteadCountySuffixUrl;
}

const std::wstring&
DatabaseSettings::FelsteadURL() const
{
    return felsteadURL;
}

void
DatabaseSettings::SetShowAssociationColumn(bool newValue)
{
    if (newValue != showAssociationColumn)
    {
        showAssociationColumn = newValue;
        SetSettingsChanged();
    }
}

void
DatabaseSettings::SetShowComposerColumn(bool newValue)
{
    if (newValue != showComposerColumn)
    {
        showComposerColumn = newValue;
        SetSettingsChanged();
    }
}

void
DatabaseSettings::SetShowConductorColumn(bool newValue)
{
    if (newValue != showConductorColumn)
    {
        showConductorColumn = newValue;
        SetSettingsChanged();
    }
}

void
DatabaseSettings::SetShowRingColumn(bool newValue)
{
    if (newValue != showRingColumn)
    {
        showRingColumn = newValue;
        SetSettingsChanged();
    }
}

void
DatabaseSettings::SetShowBellRungColumn(bool newValue)
{
    if (newValue != showBellRungColumn)
    {
        showBellRungColumn = newValue;
        SetSettingsChanged();
    }
}

void
DatabaseSettings::SetShowReferencesColumn(bool newValue)
{
    if (newValue != showReferencesColumn)
    {
        showReferencesColumn = newValue;
        SetSettingsChanged();
    }
}

void
DatabaseSettings::SetValidationCheck(std::size_t pos, const Duco::TObjectType& objectType, bool disabled)
{
    switch (objectType)
    {
        case TObjectType::EPeal:
            disabledPealChecks.set(pos, disabled);
            break;
        case TObjectType::ETower:
            disabledTowerChecks.set(pos, disabled);
            break;
        case TObjectType::EMethod:
            disabledMethodChecks.set(pos, disabled);
            break;
        case TObjectType::EMethodSeries:
            disabledMethodSeriesChecks.set(pos, disabled);
            break;
        case TObjectType::EComposition:
            disabledCompositionChecks.set(pos, disabled);
            break;
        case TObjectType::ERinger:
            disabledRingerChecks.set(pos, disabled);
            break;
        case TObjectType::EAssociationOrSociety:
            disabledAssociationChecks.set(pos, disabled);
            break;
        default:
            DUCOENGINELOG(L"Invalid object type in DatabaseSettings::DisableValidationCheck");
    }
    SetSettingsChanged();
}

bool
DatabaseSettings::ValidationCheckDisabled(std::size_t pos, const Duco::TObjectType& objectType) const
{
    switch (objectType)
    {
    case TObjectType::EPeal:
        return disabledPealChecks.test(pos);
    case TObjectType::ETower:
        return disabledTowerChecks.test(pos);
    case TObjectType::EMethod:
        return disabledMethodChecks.test(pos);
    case TObjectType::EMethodSeries:
        return disabledMethodSeriesChecks.test(pos);
    case TObjectType::EComposition:
        return disabledCompositionChecks.test(pos);
    case TObjectType::ERinger:
        return disabledRingerChecks.test(pos);
    case TObjectType::EAssociationOrSociety:
        return disabledAssociationChecks.test(pos);
    default:
        DUCOENGINELOG(L"Invalid object type in DatabaseSettings::CheckValidationCheck");
    }
    return true;
}