#include "FelsteadCounty.h"
#include "RingingDatabase.h"
#include "DatabaseSettings.h"

#include <cpr/cpr.h>
#include <xercesc/framework/MemBufInputSource.hpp>
#include <xercesc/framework/LocalFileInputSource.hpp>
#include <xercesc/sax/AttributeList.hpp>
#include "DucoEngineLog.h"
#include <fstream>
#include "DoveDatabase.h"
#include "DoveObject.h"
#include "ObjectId.h"
#include "Tower.h"
#include "TowerDatabase.h"
#include "DucoEngineUtils.h"

using namespace Duco;
XERCES_CPP_NAMESPACE_USE


FelsteadCounty::FelsteadCounty(Duco::RingingDatabase& theDatabase, const std::wstring& theCountyId, const std::wstring& theCountyName)
    : database(theDatabase), downloadThread(NULL), countyTowerbaseId(theCountyId), countyName(theCountyName)
{
    XMLPlatformUtils::Initialize();
}

FelsteadCounty::~FelsteadCounty()
{
    if (downloadThread != NULL)
    {
        downloadThread->request_stop();
        if (downloadThread->joinable())
        {
            downloadThread->join();
        }
        delete downloadThread;
    }
}

bool
FelsteadCounty::StartDownload()
{
    try
    {
        downloadThread = new std::jthread(&FelsteadCounty::Download, this);
    }
    catch (std::exception const&)
    {
        return false;
    }

    return true;
}

void
FelsteadCounty::Download()
{
    try
    {
        std::wstring url = database.Settings().FelsteadCountyURL();
        url += countyTowerbaseId;
        std::string s(url.begin(), url.end());

        cpr::Response r = cpr::Get(cpr::Url{ s.c_str() });
        SAXParser* parser = new SAXParser();
        parser->setExitOnFirstFatalError(false);
        parser->setDocumentHandler(this);
        parser->setErrorHandler(this);

        MemBufInputSource src((const XMLByte*)r.text.c_str(), r.text.length(), "dummy", false);
        parser->parse(src);
        //Process(*parser);
        delete parser;
    }
    catch (const XMLException& toCatch) {
        char* message = XMLString::transcode(toCatch.getMessage());
        DUCOENGINELOG(message);
        XMLString::release(&message);
    }
    catch (const SAXParseException& toCatch) {
        char* message = XMLString::transcode(toCatch.getMessage());
        DUCOENGINELOG(message);
        XMLString::release(&message);
    }
    catch (std::exception const& toCatch)
    {
        DUCOENGINELOG(toCatch.what());
    }
}

void
FelsteadCounty::Process(const std::wstring& filename)
{
    try
    {
        SAXParser* parser = new SAXParser();
        parser->setExitOnFirstFatalError(false);
        parser->setDocumentHandler(this);
        parser->setErrorHandler(this);
        LocalFileInputSource src(filename.c_str());
        parser->parse(src);
        //Process(*parser);
        delete parser;
    }
    catch (const XMLException& toCatch) {
        char* message = XMLString::transcode(toCatch.getMessage());
        DUCOENGINELOG(message);
        XMLString::release(&message);
    }
    catch (const SAXParseException& toCatch) {
        char* message = XMLString::transcode(toCatch.getMessage());
        DUCOENGINELOG(message);
        XMLString::release(&message);
    }
    catch (std::exception const& ex)
    {
        DUCOENGINELOG(ex.what());
    }
}

void
FelsteadCounty::Wait()
{
    if (downloadThread != NULL && downloadThread->joinable())
    {
        downloadThread->join();
    }
}

size_t
FelsteadCounty::NumberOfTowers() const
{
    return towers.size();
}

void
FelsteadCounty::endDocument()
{

}

void
FelsteadCounty::ignorableWhitespace(const XMLCh* const chars, const XMLSize_t length)
{

}

void
FelsteadCounty::processingInstruction(const XMLCh* const target, const XMLCh* const data)
{

}

void
FelsteadCounty::resetDocument()
{
    towers.clear();
    startedDiv = false;
    startedTable = false;
    startedCountyRow = false;
    startedCell = false;
    currentTowerbaseId.clear();
    currentPlace.clear();
    currentPealCount.clear();
    currentInvalidPealCount.clear();

}

void
FelsteadCounty::setDocumentLocator(const XERCES_CPP_NAMESPACE_QUALIFIER Locator* const locator)
{

}

void
FelsteadCounty::startDocument()
{

}

void
FelsteadCounty::startElement(const XMLCh* const name, XERCES_CPP_NAMESPACE_QUALIFIER AttributeList& attrs)
{
    if (wcscmp(name, L"div") == 0 && attrs.getLength() == 1 && wcscmp(attrs.getValue(L"class"), L"mensa") == 0)
    {
        startedDiv = true;
    }
    else if (startedDiv && wcscmp(name, L"table") == 0)
    {
        startedTable = true;
    }
    else if (startedTable && wcscmp(name, L"tr") == 0)
    {
        startedCountyRow = true;
        cellCount = 0;
    }
    else if (startedTable && wcscmp(name, L"td") == 0)
    {
        startedCell = true;
        cellCount += 1;
    }
}

void
FelsteadCounty::endElement(const XMLCh* const name)
{
    if (wcscmp(name, L"div") == 0)
    {
        startedDiv = false;
    }
    else if (startedDiv && wcscmp(name, L"table") == 0)
    {
        startedTable = false;
    }
    else if (startedCountyRow && wcscmp(name, L"td") == 0)
    {
        startedCell = false;
    }
    else if (startedTable && wcscmp(name, L"tr") == 0)
    {
        startedCountyRow = false;
        
        FelsteadCountyItem item;
        item.towerbaseId = DucoEngineUtils::Trim(currentTowerbaseId);;
        item.place = DucoEngineUtils::Trim(currentPlace);

        if (item.towerbaseId.length() > 0 && item.place.length() > 0)
        {
            try
            {
                if (currentPealCount.length() > 0)
                {
                    item.pealCount = std::stoi(currentPealCount);
                }
                else
                {
                    item.pealCount = 0;
                }
                if (currentInvalidPealCount.length() > 0)
                {
                    item.invalidPealCount = std::stoi(currentInvalidPealCount);
                }
                else
                {
                    item.invalidPealCount = 0;
                }

                if (item.pealCount > 0)
                {
                    towers.push_back(item);
                }
            }
            catch (std::exception const& ex)
            {
                DUCOENGINELOG(ex.what());
            }
        }

        currentTowerbaseId.clear();
        currentPlace.clear();
        currentPealCount.clear();
        currentInvalidPealCount.clear();
    }
}

void
FelsteadCounty::characters(const XMLCh* const chars, const XMLSize_t length)
{
    if (startedCell)
    {
        if (cellCount == 1)
        {
            currentTowerbaseId += chars;
        }
        else if (cellCount == 2)
        {
            currentPlace += chars;
        }
        else if (cellCount == 3)
        {
            currentPealCount += chars;
        }
        else if (cellCount == 4)
        {
            currentInvalidPealCount += chars;
        }
    }
}


void
FelsteadCounty::warning(const XERCES_CPP_NAMESPACE_QUALIFIER SAXParseException& exc)
{
    DUCOENGINELOG(exc.getMessage());
}

void
FelsteadCounty::error(const XERCES_CPP_NAMESPACE_QUALIFIER SAXParseException& exc)
{
    DUCOENGINELOG(exc.getMessage());
}

void
FelsteadCounty::fatalError(const XERCES_CPP_NAMESPACE_QUALIFIER SAXParseException& exc)
{
    DUCOENGINELOG(exc.getMessage());
}

void
FelsteadCounty::resetErrors()
{

}

bool
FelsteadCounty::ResolveTowers(const Duco::DoveDatabase& doveDb)
{
    bool foundAtLeastOne = false;
    for (auto const& it : towers)
    {
        std::set<Duco::DoveObject> doveTowers;
        if (doveDb.FindTowerByTowerbaseId(it.towerbaseId, doveTowers))
        {
            std::set<Duco::DoveObject>::const_iterator doveIt = doveTowers.begin();
            while (doveIt != doveTowers.end())
            {
                if (database.TowersDatabase().AddTower(*doveIt).ValidId())
                {
                    foundAtLeastOne = true;
                }
                ++doveIt;
            }
        }
        else
        {
            // Tower not found in Dove, must have been removed

            size_t index = it.place.find_last_of(L",.");
            std::wstring towerName = it.place.substr(0, index);
            std::wstring towerPlace = DucoEngineUtils::Trim(it.place.substr(index+1));

            Tower removedTower(KNoId, towerName, towerPlace, countyName, 8);
            removedTower.SetRemoved(true);
            removedTower.SetTowerbaseId(it.towerbaseId);
            if (database.TowersDatabase().AddObject(removedTower).ValidId())
            {
                foundAtLeastOne = true;
            }
        }
    }

    return foundAtLeastOne;
}
