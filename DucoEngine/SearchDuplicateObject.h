#ifndef __SEARCHDUPLICATEOBJECT_H__
#define __SEARCHDUPLICATEOBJECT_H__

#include "SearchValidationArgument.h"
#include <set>

namespace Duco
{
    class RingingObject;

    class TSearchDuplicateObject : public TSearchValidationArgument
    {
    public:
        DllExport explicit TSearchDuplicateObject(bool includeRingers);
        bool Match(const Duco::Peal& peal, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const;
        bool Match(const Duco::Tower& tower, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const;
        bool Match(const Duco::Ringer& ringer, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const;
        bool Match(const Duco::Method& method, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const;
        bool Match(const Duco::MethodSeries& series, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const;
        bool Match(const Duco::Association& series, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const;
        virtual Duco::TFieldType FieldType() const;
        ~TSearchDuplicateObject();

        virtual std::set<Duco::ObjectId> ErrorIds() const;
        void Reset();

    protected:
        bool BothAlreadyInList(const Duco::ObjectId& lhs, const Duco::ObjectId& rhs) const;

    private:
        mutable std::set<Duco::ObjectId> possibleDuplicateIds;
        bool includeRingers;
    };

}

#endif //!__SEARCHDUPLICATEOBJECT_H__
