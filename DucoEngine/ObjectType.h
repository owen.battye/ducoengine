#ifndef __OBJECTTYPE_H__
#define __OBJECTTYPE_H__

namespace Duco
{
    enum class TObjectType
    {
        ENon = -1,
        EPeal = 0,
        EMethod,
        EMethodSeries,
        ETower,
        ERinger,
        ERing,
        EOrderName,
        EComposition,
        ESettings,
        EPicture,
        EAssociationOrSociety
    };
}

#endif// !__OBJECTTYPE_H__
