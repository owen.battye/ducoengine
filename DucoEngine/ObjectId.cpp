#include "ObjectId.h"
#include <sstream>
#include "DucoEngineUtils.h"

using namespace Duco;

ObjectId::ObjectId(size_t newId)
    : id(newId)
{
    ConvertFrom32Bit();
}

ObjectId::ObjectId(const std::wstring& rhs)
{
    id = _wtoi(rhs.c_str());
}

ObjectId&
ObjectId::operator=(const ObjectId& rhs)
{
    id = rhs.id;
    ConvertFrom32Bit();
    return *this;
}

bool
ObjectId::operator==(const ObjectId& rhs) const
{
    return id == rhs.id;
}

bool
ObjectId::operator!=(const ObjectId& rhs) const
{
    return id != rhs.id;
}

bool
ObjectId::ValidId() const
{
    return id != KNoId;
}

bool
ObjectId::operator>(const Duco::ObjectId& rhs) const
{
    return id > rhs.Id();
}

bool
ObjectId::operator<(const ObjectId& rhs) const
{
    return id < rhs.id;
}

ObjectId&
ObjectId::operator++()
{
    ++id;
    return *this;
}

ObjectId&
ObjectId::operator--()
{
    --id;
    return *this;
}

ObjectId
ObjectId::operator+(size_t num) const
{
    ObjectId newId (*this);
    newId.id += num;
    return newId;
}

ObjectId
ObjectId::operator-(size_t num) const
{
    ObjectId newId (*this);
    newId.id -= num;
    return newId;
}

bool
ObjectId::operator<=(const ObjectId& rhs) const
{
    return id <= rhs.id;
}

std::wostream&
ObjectId::operator<<(std::wostream& strm) const
{
    strm << id;
    return strm;
}

std::wistream&
ObjectId::operator>>(std::wistream& strm)
{
    size_t tempInput = 0;
    strm >> tempInput;
    strm.ignore();

    id = tempInput;
    return strm;
}

std::wstring
ObjectId::Str() const
{
    std::wstringstream strm;
    strm << *this;
    return strm.str();
}

void 
ObjectId::ConvertFrom32Bit()
{
    if (std::numeric_limits<size_t>::max() != std::numeric_limits<unsigned int>::max())
    {
        if (id == std::numeric_limits<size_t>::max())
        {
            id = std::numeric_limits<unsigned int>::max();
        }
    }
}

void
ObjectId::ClearId()
{
    id = KNoId;
    ConvertFrom32Bit();
}
