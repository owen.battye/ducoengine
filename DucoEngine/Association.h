#pragma once
#ifndef __ASSOCIATION_H__
#define __ASSOCIATION_H__

#include <string>
#include <set>
#include <map>
#include "RingingObject.h"
#include <xercesc/dom/DOMDocument.hpp>

namespace Duco
{
    class DatabaseSettings;
    class DatabaseWriter;
    class DatabaseReader;
    class MethodDatabase;

    class Association : public RingingObject
    {
    public:
        friend class AssociationDatabase;

        DllExport Association(const Duco::ObjectId& newId, const std::wstring& newName, const std::wstring& newNotes);
        DllExport Association(const Association& other);
        DllExport Association(const Duco::ObjectId& newId, const Duco::Association& other);
        DllExport Association(Duco::DatabaseReader& reader, unsigned int databaseVersion);
        DllExport Association();
        DllExport ~Association();
        DllExport virtual bool Duplicate(const Duco::RingingObject& other, const Duco::RingingDatabase& database) const;

        // from RingingObject
        DllExport virtual TObjectType ObjectType() const;
        bool Similar(const Duco::RingingObject& otherMethod) const;
        virtual std::wstring FullNameForSort(bool unusedSetting) const;

        //Accessors
        DllExport const std::wstring& Name() const;
        DllExport const std::wstring& Notes() const;
        DllExport const std::wstring& PealbaseLink() const;
        DllExport const std::set<Duco::ObjectId>& Links() const;

        //Settors
        DllExport void SetNotes(const std::wstring& newNotes);
        DllExport void SetName(const std::wstring& newName);
        DllExport bool AddAssociationLink(const Duco::ObjectId& newAssociationId);
        DllExport bool RemoveAssociationLink(const Duco::ObjectId& oldAssociationId);
        DllExport bool ReplaceAssociationLink(const Duco::ObjectId& oldAssociationId, const Duco::ObjectId& newAssociationId);
        DllExport bool LinkedToAssociation(const Duco::ObjectId& associationId) const;
        DllExport bool UppercaseAssociation();
        DllExport void SetPealbaseLink(const std::wstring& newLink);
        void SetError(TAssociationErrorCodes code);

        // Operators
        DllExport bool operator==(const Duco::RingingObject& other) const;
        DllExport bool operator!=(const Duco::RingingObject& other) const;
        DllExport bool operator!=(const Duco::Association& rhs) const;
        DllExport bool operator==(const Duco::Association& rhs) const;
        DllExport Association& operator=(const Duco::Association& other);

        // Validity checking
        DllExport bool Valid(const Duco::RingingDatabase& database, bool warningFatal, bool clearBeforeStart) const;
        DllExport std::wstring ErrorString(const Duco::DatabaseSettings& /*settings*/, bool showWarnings) const;

        // I/O
        DllExport std::wostream& Print(std::wostream& stream, const RingingDatabase& database) const;
        DllExport DatabaseWriter& Externalise(DatabaseWriter& writer) const;
        std::wofstream& ExportToRtf(std::wofstream& outputFile, const RingingDatabase& database) const;
        virtual void ExportToXml(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& parent, bool exportDucoObjects) const;
        void ExportToCsv(std::wofstream& file) const;

        bool RenumberAssociationLinks(const std::map<Duco::ObjectId, Duco::ObjectId>& newAssocationIds);

    protected:
        DatabaseReader& Internalise(DatabaseReader& reader, unsigned int databaseVersion);

    protected:
        std::wstring  name;
        std::wstring  notes;
        std::wstring  pealbaseLink;
        std::set<Duco::ObjectId> linkedAssociations;
    };

} // end namespace Duco

#endif // __ASSOCIATION_H__
