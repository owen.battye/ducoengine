#ifndef __DUMMYPROGRESSCALLBACK_H__
#define __DUMMYPROGRESSCALLBACK_H__

#include "ProgressCallback.h"
#include "DucoEngineCommon.h"

namespace Duco
{

class DummyProgressCallback : public Duco::ProgressCallback
{
public:
    DllExport void Initialised();
    DllExport void Step(int progressPercent);
    DllExport void Complete(bool error);
};

}

#endif // __DUMMYPROGRESSCALLBACK_H__
