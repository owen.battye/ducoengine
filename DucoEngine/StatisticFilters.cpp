#include "StatisticFilters.h"
#include "Peal.h"
#include "Association.h"
#include "AssociationDatabase.h"
#include "MethodDatabase.h"
#include "RingerDatabase.h"
#include "RingingDatabase.h"
#include "Tower.h"
#include "TowerDatabase.h"
#include "Method.h"
#include "Ring.h"
#include "DucoEngineUtils.h"

using namespace Duco;

StatisticFilters::StatisticFilters(const Duco::RingingDatabase& theDatabase)
    : ringers(theDatabase.RingersDatabase()),
    methods(theDatabase.MethodsDatabase()),
    towers(theDatabase.TowersDatabase()),
    associations(theDatabase.AssociationsDatabase()),
    settings(theDatabase.Settings()),
    allLinkedTowerIds (nullptr)
{
    Clear();
}

StatisticFilters::StatisticFilters(const Duco::StatisticFilters& other)
    : ringers(other.ringers),
    methods(other.methods),
    towers(other.towers),
    associations(other.associations),
    settings(other.settings),
    allLinkedTowerIds(nullptr)
{
    this->operator=(other);
}

StatisticFilters::~StatisticFilters()
{
    if (allLinkedTowerIds != nullptr)
    {
        delete allLinkedTowerIds;
    }
}

const Duco::StatisticFilters&
StatisticFilters::operator=(const Duco::StatisticFilters& other)
{
    excludeValid = other.excludeValid;
    excludeInvalidTimeOrChanges = other.excludeInvalidTimeOrChanges;
    includeWithdrawn = other.includeWithdrawn;
    includeTowerBell = other.includeTowerBell;
    includeHandBell = other.includeHandBell;

    associationEnabled = other.associationEnabled;
    associationId = other.associationId;

    ringerEnabled = other.ringerEnabled;
    ringerId = other.ringerId;
    includeLinkedRingers = other.includeLinkedRingers;

    conductorEnabled = other.conductorEnabled;
    conductorId = other.conductorId;

    methodEnabled = other.methodEnabled;
    methodId = other.methodId;

    typeEnabled = other.typeEnabled;
    type = other.type;

    stageEnabled = other.stageEnabled;
    stage = other.stage;

    towerEnabled = other.towerEnabled;
    towerId = other.towerId;
    includeLinkedTowers = other.includeLinkedTowers;
    ringEnabled = other.ringEnabled;
    ringId = other.ringId;

    countyEnabled = other.countyEnabled;
    county = other.county;
    startDateEnabled = other.startDateEnabled;
    startDate = other.startDate;
    endDateEnabled = other.endDateEnabled;
    endDate = other.endDate;

    europeOnly = other.europeOnly;

    if (allLinkedTowerIds != nullptr)
    {
        delete allLinkedTowerIds;
        allLinkedTowerIds = nullptr;
    }
    if (other.allLinkedTowerIds != nullptr)
    {
        allLinkedTowerIds = new std::list <std::set<Duco::ObjectId> >(*other.allLinkedTowerIds);
    }

    return *this;
}

void
StatisticFilters::Clear()
{
    excludeValid = false;
    excludeInvalidTimeOrChanges = false;
    includeWithdrawn = false;
    includeTowerBell = true;
    includeHandBell = true;
    associationId.ClearId();
    associationEnabled = false;
    ringerEnabled = false;
    ringerId.ClearId();
    includeLinkedRingers = true;
    conductorEnabled = false;
    conductorId.ClearId();
    methodEnabled = false;
    methodId.ClearId();
    typeEnabled = false;
    type.clear();
    includeLinkedTowers = true;
    stageEnabled = false;
    stage = -1;
    towerEnabled = false;
    towerId.ClearId();
    europeOnly = false;
    ringEnabled = false;
    ringId.ClearId();

    countyEnabled = false;
    county.clear();
    startDateEnabled = false;
    startDate.ResetToEarliest();
    endDateEnabled = false;
    endDate.ResetToEarliest();
    if (allLinkedTowerIds != nullptr)
    {
        delete allLinkedTowerIds;
        allLinkedTowerIds = nullptr;
    }
}

void
StatisticFilters::SetDefaultRinger()
{
    if (settings.DefaultRingerSet())
    {
        ringerEnabled = true;
        ringerId = settings.DefaultRinger();
    }
}

void
StatisticFilters::SetDefaultConductor()
{
    if (settings.DefaultRingerSet())
    {
        conductorEnabled = true;
        conductorId = settings.DefaultRinger();
    }
}

bool
StatisticFilters::Enabled() const
{ // ringEnabled missing on purpose, because tower must be enabled anyway
    return associationEnabled || !includeWithdrawn || excludeValid || excludeInvalidTimeOrChanges || ringerEnabled || conductorEnabled || methodEnabled || typeEnabled || stageEnabled || towerEnabled || includeLinkedTowers || !includeTowerBell || !includeHandBell || countyEnabled || startDateEnabled || endDateEnabled || europeOnly;
}

std::wstring
StatisticFilters::Description() const
{
    if (!Enabled())
        return L"Filters disabled";

    std::wstring filterDesc = L"Filtered by:\n";
    if (excludeValid)
    {
        filterDesc += L"\tExclude valid peals.";
    }
    if (includeWithdrawn)
    {
        filterDesc += L"\tInclude withdrawn peals.";
    }
    else
    {
        filterDesc += L"\tExclude withdrawn peals.";
    }
    if (excludeInvalidTimeOrChanges)
    {
        filterDesc += L"\tExclude peals with invalid time or changes.";
    }
    if (!includeTowerBell && !includeHandBell)
    {
        filterDesc += L"\tExclude tower and handbell peals!";
    }
    else if (includeTowerBell && !includeHandBell)
    {
        filterDesc += L"\tTower peals only";
    }
    else if (!includeTowerBell && includeHandBell)
    {
        filterDesc += L"\tHandbell peals only";
    }

    if (associationEnabled)
    {
        const Duco::Association* theAssociation = associations.FindAssociation(methodId);
        if (theAssociation != NULL)
        {
            filterDesc += L"\tAssociation: " + theAssociation->Name() + L".";
        }
        else
        {
            filterDesc += L"\tAssociation: Empty.";
        }
    }

    if (ringerEnabled)
    {
        filterDesc += L"\tRinger: " + ringers.RingerFullName(ringerId, settings);
        if (includeLinkedRingers)
        {
            filterDesc += L" and include linked.";
        }
        else
        {
            filterDesc += L".";
        }
    }
    if (conductorEnabled)
    {
        filterDesc += L"\tConductor: " + ringers.RingerFullName(conductorId, settings);
    }
    if (methodEnabled)
    {
        const Duco::Method* theMethod = methods.FindMethod(methodId);
        if (theMethod != NULL)
        {
            filterDesc += L"\tMethod: " + theMethod->Name() + L".";
        }
        else
        {
            filterDesc += L"\tMethod: Unknown.";
        }
    }
    if (typeEnabled)
    {
        filterDesc += L"\tType: " + type + L".";
    }
    if (stageEnabled)
    {
        filterDesc += L"\tStage: " + DucoEngineUtils::ToString(stage) + L".";
    }

    if (towerEnabled)
    {
        const Duco::Tower* const theTower = towers.FindTower(towerId, true);
        if (theTower != NULL)
        {
            filterDesc += L"\tTower: " + theTower->FullName();
            if (ringEnabled)
            {
                const Duco::Ring* const theRing = theTower->FindRing(ringId);
                if (theRing != NULL)
                {
                    filterDesc += L",\tRing: " + theRing->Name() + L".";
                }
            }
            else
            {
                filterDesc += L".";
            }
        }
        else
        {
            filterDesc += L"\tTower: Missing tower.";
        }

        if (includeLinkedTowers)
        {
            filterDesc += L"\tIncluding linked towers.";
        }
    }
    else
    {
        if (countyEnabled)
        {
            filterDesc += L"\tCounty: " + county + L".";
        }

        if (startDateEnabled && startDate.Valid())
        {
            filterDesc += L"\tAfter: " + startDate.DateWithLongerMonth() + L".";
        }

        if (endDateEnabled && endDate.Valid())
        {
            filterDesc += L"\tBefore: " + endDate.DateWithLongerMonth() + L".";
        }
    }

    if (europeOnly)
    {
        filterDesc += L"\tOnly towers in europe.";
    }
    return filterDesc;
}

bool
StatisticFilters::Association(Duco::ObjectId& associationValue) const
{
    associationValue = associationId;
    return associationEnabled;
}

void
StatisticFilters::SetAssociation(bool state, const Duco::ObjectId& newAssociationId)
{
    associationEnabled = state;
    associationId = newAssociationId;
}

bool
StatisticFilters::Stage(unsigned int& theStage) const
{
    theStage = stage;
    return stageEnabled;
}

void
StatisticFilters::SetStage(bool state, unsigned int newStage)
{
    stage = newStage;
    stageEnabled = state;
}

bool
StatisticFilters::Ringer(Duco::ObjectId& theRingerId) const
{
    theRingerId = ringerId;
    return ringerEnabled;
}

void
StatisticFilters::SetRinger(bool state, const Duco::ObjectId& theRingerId)
{
    ringerEnabled = state;
    ringerId = theRingerId;
}

bool
StatisticFilters::Conductor(Duco::ObjectId& theConductorId) const
{
    theConductorId = conductorId;
    return conductorEnabled;
}

void
StatisticFilters::SetConductor(bool state, const Duco::ObjectId& theConductorId)
{
    conductorId = theConductorId;
    conductorEnabled = state;
}

bool
StatisticFilters::Tower(Duco::ObjectId& theTowerId) const
{
    theTowerId = towerId;
    return towerEnabled;
}

void
StatisticFilters::SetTower(bool state, const Duco::ObjectId& theTowerId)
{
    towerId = theTowerId;
    towerEnabled = state;
}

bool
StatisticFilters::Ring(Duco::ObjectId& theRingId) const
{
    theRingId = ringId;
    return ringEnabled;
}

void
StatisticFilters::SetRing(bool state, const Duco::ObjectId& theRingId)
{
    ringId = theRingId;
    ringEnabled = state;
}

bool
StatisticFilters::Method(Duco::ObjectId& theMethodId) const
{
    theMethodId = methodId;
    return methodEnabled;
}

void
StatisticFilters::SetMethod(bool state, const Duco::ObjectId& theMethodId)
{
    methodId = theMethodId;
    methodEnabled = state;
}

bool
StatisticFilters::Type(std::wstring& theType) const
{
    theType = type;
    return typeEnabled;
}

void
StatisticFilters::SetType(bool state, const std::wstring& theType)
{
    type = theType;
    typeEnabled = state;
}

bool
StatisticFilters::Match(const Duco::Peal& peal) const
{
    bool match = true;
    if (excludeValid && !peal.Withdrawn())
    {
        match = false;
    }
    if (!includeWithdrawn && peal.Withdrawn())
    {
        match = false;
    }
    if (excludeInvalidTimeOrChanges && !peal.ChangesAndTimeValid())
    {
        match = false;
    }
    if (match && !includeTowerBell && !peal.Handbell())
    {
        match &= false;
    }
    if (match && !includeHandBell && peal.Handbell())
    {
        match &= false;
    }
    if (match && associationEnabled)
    {
        match &= peal.AssociationId() == associationId;
    }
    if (match && ringerEnabled)
    {
        bool strapper = false;
        bool containsRinger = peal.ContainsRinger(ringerId, strapper);
        if (includeLinkedRingers && !containsRinger)
        {
            std::set<Duco::ObjectId> linkedRingers;
            ringers.LinkedRingers(ringerId, linkedRingers, false);
            for (std::set<Duco::ObjectId>::const_iterator it = linkedRingers.begin(); it != linkedRingers.end() && !containsRinger; ++it)
            {
                containsRinger |= peal.ContainsRinger(*it, strapper);
            }
        }
        match &= containsRinger;
    }
    if (match && conductorEnabled)
    {
        match &= peal.ContainsConductor(conductorId);
    }
    if (match && methodEnabled)
    {
        match &= peal.MethodId() == methodId;
    }
    if (match && startDateEnabled && startDate.Valid())
    {
        match &= peal.Date() >= startDate;
    }
    if (match && endDateEnabled && endDate.Valid())
    {
        match &= peal.Date() <= endDate;
    }
    if (match && (stageEnabled || typeEnabled))
    {
        const Duco::Method* const theMethod = methods.FindMethod(peal.MethodId());
        if (theMethod != NULL)
        {
            if (stageEnabled)
                match &= stage == theMethod->Order();
            if (typeEnabled)
                match &= type.compare(theMethod->Type()) == 0;
        }
    }
    if (match && (towerEnabled || countyEnabled || europeOnly))
    {
        const Duco::Tower* const theTower = towers.FindTower(peal.TowerId(), true);
        if (theTower != NULL)
        {
            match &= Match(*theTower);
            if (match && ringEnabled && ringId.ValidId())
            {
                match &= peal.RingId() == ringId;
            }
        }
        else
        {
            match = false;
        }
    }

    return match;
}

bool
StatisticFilters::Match(const Duco::Tower& tower) const
{
    bool match = true;
    if (match && countyEnabled)
    {
        match &= county.compare(tower.County()) == 0;
    }
    if (match && europeOnly)
    {
        match &= tower.InEurope();
    }
    if (match && towerEnabled)
    {
        bool towerMatch = tower.Id() == towerId;
        if (!towerMatch && includeLinkedTowers)
        {
            if (allLinkedTowerIds == nullptr)
            {
                allLinkedTowerIds = new std::list <std::set<Duco::ObjectId> >();
                towers.GetAllLinkedIds(*allLinkedTowerIds);
            }
            std::list<std::set<Duco::ObjectId> >::iterator linkedTowerIds = DucoEngineUtils::FindId(*allLinkedTowerIds, tower.Id());
            if (linkedTowerIds == allLinkedTowerIds->end())
            {
                match &= towerMatch;
            }
            else
            {
                bool pealInLinkedTower = linkedTowerIds->find(towerId) != linkedTowerIds->end();
                match &= pealInLinkedTower;
            }
        }
        else
        {
            match &= towerMatch;
        }
    }

    return match;
}


const Duco::ObjectId&
StatisticFilters::RingerId() const
{
    if (ringerId.ValidId())
        return ringerId;
    if (conductorId.ValidId())
        return conductorId;

    return settings.DefaultRinger();
}


bool
StatisticFilters::County(std::wstring& returnCounty) const
{
    returnCounty = county;
    return countyEnabled;
}

void
StatisticFilters::SetCounty(bool state, const std::wstring& newCounty)
{
    county = newCounty;
    countyEnabled = state;
}

void
StatisticFilters::SetStartDate(bool state, const Duco::PerformanceDate& newStartDate)
{
    startDateEnabled = state;
    startDate = newStartDate;
}

const Duco::PerformanceDate&
StatisticFilters::StartDate() const
{
    return startDate;
}

void
StatisticFilters::SetEndDate(bool state, const Duco::PerformanceDate& newEndDate)
{
    endDateEnabled = state;
    endDate = newEndDate;
}

const Duco::PerformanceDate&
StatisticFilters::EndDate() const
{
    return endDate;
}

bool
StatisticFilters::StartDateEnabled(Duco::PerformanceDate& startDate) const
{
    startDate = this->startDate;
    return startDateEnabled;
}
bool
StatisticFilters::EndDateEnabled(Duco::PerformanceDate& endDate) const
{
    endDate = this->endDate;
    return endDateEnabled;
}


void
StatisticFilters::SetEuropeOnly(bool state)
{
    europeOnly = state;
}

bool
StatisticFilters::EuropeOnly() const
{
    return europeOnly;
}
