#ifndef __LEADEND_H__
#define __LEADEND_H__

#include <string>
#include "DucoEngineCommon.h"
#include "LeadType.h"

namespace Duco
{
    class Change;

class LeadEnd
{
public:
    DllExport LeadEnd(const Duco::Change& other, TLeadType newLeadType, bool isCourseEnd);
    DllExport LeadEnd(const Duco::LeadEnd& rhs);
    DllExport virtual ~LeadEnd();
    LeadEnd* Realloc();
    bool Check(const Duco::Change& leadEnd) const;


    // Accessors
    inline std::wstring Str() const;
    inline TLeadType LeadType() const;
    inline bool PartEnd() const;
    inline bool CourseEnd() const;

    //Settors
    inline void SetPartEnd();

protected:

protected:
    std::wstring*    leadEndString;
    TLeadType       leadType;
    bool            partEnd;
    bool            courseEnd;
};

TLeadType
LeadEnd::LeadType() const
{
    return leadType;
}

bool
LeadEnd::PartEnd() const
{
    return partEnd;
}

bool
LeadEnd::CourseEnd() const
{
    return courseEnd;
}

std::wstring
LeadEnd::Str() const
{
    return *leadEndString;
}

void
LeadEnd::SetPartEnd()
{
    partEnd = true;
}

}

#endif //!__LEADEND_H__
