#ifndef __SEARCHFIELDNEGATIVEARGUMENT_H__
#define __SEARCHFIELDNEGATIVEARGUMENT_H__

#include "SearchArgument.h"
#include "DucoEngineCommon.h"

namespace Duco
{
class TSearchFieldNegativeArgument : public TSearchArgument
{
public:
    DllExport TSearchFieldNegativeArgument(Duco::TSearchArgument* newArgument);
    ~TSearchFieldNegativeArgument();
    virtual bool Match(const Duco::Method& object, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Peal& object, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Ringer& object, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Tower& object, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::MethodSeries& object, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Composition& object, const Duco::RingingDatabase& database) const;
	virtual bool Match(const Duco::Association& object, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Picture& object, const Duco::RingingDatabase& database) const;
    Duco::TFieldType FieldType() const;
 
protected:
    TSearchArgument*    theArgument;
};

}
#endif // __SEARCHFIELDNEGATIVEARGUMENT_H__
