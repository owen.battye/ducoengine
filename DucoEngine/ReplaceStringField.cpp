#include "ReplaceStringField.h"

#include "Peal.h"
#include "Tower.h"
#include "Method.h"
#include "DucoEngineUtils.h"

using namespace Duco;

TReplaceStringField::TReplaceStringField(Duco::TUpdateFieldId newFieldId, bool exctMatch, const std::wstring& strToFind, const std::wstring& replacementStr)
: TUpdateArgument(newFieldId), exactMatch(exctMatch), stringToFind(strToFind), replacementString(replacementStr)
{

}

TReplaceStringField::~TReplaceStringField()
{

}

Duco::TFieldType
TReplaceStringField::FieldType() const
{
    return TFieldType::EStringField;
}

bool
TReplaceStringField::Update(Duco::Method& method) const
{
    switch (fieldId)
    {
    case EMethod_Name:
        {
            std::wstring replacementStr;
            if (StringMatch(method.Name(), replacementStr))
            {
                method.SetName(replacementStr);
                return true;
            }
        }
        break;
    case EMethod_Type:
        {
            std::wstring replacementStr;
            if (StringMatch(method.Type(), replacementStr))
            {
                method.SetType(replacementStr);
                return true;
            }
        }
        break;
    default:
        break;        
    }
    return false;
}

bool
TReplaceStringField::Update(Duco::Peal& object) const
{
    switch (fieldId)
    {
    case EPeal_Composer:
        {
            std::wstring replacementStr;
            if (StringMatch(object.Composer(), replacementStr))
            {
                object.SetComposer(replacementStr);
                return true;
            }
        }
        break;
    case EPeal_Footnotes:
        {
            std::wstring replacementStr;
            if (StringMatch(object.Footnotes(), replacementStr))
            {
                object.SetFootnotes(replacementStr);
                return true;
            }
        }
        break;
    default:
        break;
    }
    return false;
}

bool
TReplaceStringField::Update(Duco::Tower& object) const
{
    switch (fieldId)
    {
    case ETower_Name:
        {
            std::wstring replacementStr;
            if (StringMatch(object.Name(), replacementStr))
            {
                object.SetName(replacementStr);
                return true;
            }
        }
        break;

    default:
        break;
    }
    return false;
}

bool
TReplaceStringField::StringMatch(const std::wstring& strToSearch, std::wstring& updatedString) const
{
    updatedString.clear();
    if (exactMatch)
    {
        if (strToSearch.compare(stringToFind) == 0)
        {
            updatedString = replacementString;
            return true;
        }
    }
    else
    {
        std::wstring strToSearchInUpper;
        std::wstring stringToFindInUpper;
        DucoEngineUtils::ToUpperCase(strToSearch, strToSearchInUpper);
        DucoEngineUtils::ToUpperCase(stringToFind, stringToFindInUpper);
        size_t index = strToSearchInUpper.find(stringToFindInUpper);
        if (index != std::wstring::npos)
        {
            updatedString = strToSearch.substr(0, index);
            updatedString.append(replacementString);
            updatedString.append(strToSearch.substr(index + stringToFindInUpper.length()));
            return true;
        }
    }
    return false;
}