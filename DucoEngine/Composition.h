#ifndef __COMPOSITION_H__
#define __COMPOSITION_H__

#include <vector>
#include "DucoEngineCommon.h"
#include "DucoTypes.h"
#include "LeadType.h"
#include "RingingObject.h"
#include "CompositionTable.h"

namespace Duco
{
    class Calling;
    class Change;
    class ChangeCollection;
    class Method;
    class MethodDatabase;
    class Music;
    class LeadEndCollection;
    class PlaceNotation;
    class ProgressCallback;
    class RingerDatabase;

class Composition : public RingingObject
{
public:
    DllExport Composition(const Composition& other);
    Composition(const Duco::ObjectId& newId, const Composition& other);
    DllExport Composition(const Duco::ObjectId& newMethodId, const Duco::MethodDatabase& methodDb);
    DllExport Composition(Duco::Method& method);
    DllExport Composition(DatabaseReader& reader, unsigned int databaseVersionNumber);
    DllExport virtual ~Composition();

    // Accessors
    DllExport const std::wstring& Name() const;
    virtual std::wstring FullNameForSort(bool unusedSetting) const;
    DllExport std::wstring Title(const MethodDatabase& methodDb) const;
    DllExport std::wstring Composer(const RingerDatabase& ringerDb) const;
    DllExport void FullName(const RingingDatabase& ringingDb, std::wstring& title, std::wstring& composer) const;
    DllExport unsigned int Repeats() const;
    DllExport const std::wstring& Notes() const;
    DllExport const Duco::ObjectId& ComposerId() const;
    DllExport const std::wstring& ComposerName() const;
    DllExport const Duco::ObjectId& MethodId() const;
    DllExport unsigned int Order() const;
    DllExport unsigned int Order(const MethodDatabase& methodDb) const;
    DllExport Duco::PlaceNotation& Notation() const;
    DllExport const Duco::Change& CurrentEndChange() const;
    DllExport size_t NoOfChanges() const;
    DllExport size_t ChangesPerLead() const;
    DllExport const LeadEndCollection& LeadEnds() const;
    DllExport const ChangeCollection& Changes() const;
    DllExport std::wstring MusicDetail() const;
    DllExport std::wstring FalseDetails() const;
    DllExport Duco::CompositionTable CreateCompositionTable() const;
    inline bool IsSnapStart() const;
    inline size_t SnapStart() const;
    inline bool IsSnapFinish() const;
    inline size_t SnapFinish() const;

    // Settors
    DllExport void SetName(const std::wstring& newName) const;
    DllExport void SetRepeats(unsigned int newRepears);
    DllExport void SetNotes(const std::wstring& newNotes) const;
    DllExport void SetComposerId(const Duco::ObjectId& newComposerId);
    DllExport void SetComposerName(const std::wstring& newComposerName);
    DllExport void SetMethodId(const Duco::ObjectId& newMethodId);
    DllExport void SetSnapStart(int newSnapStart);

    DllExport bool Valid(const Duco::RingingDatabase& ringingDb, bool warningFatal, bool clearBeforeStart) const;
    DllExport bool Valid(const Duco::MethodDatabase& methodDb, const Duco::RingerDatabase& ringerDb, bool warningFatal, bool clearBeforeStart) const;
    DllExport void Clear();
    DllExport virtual std::wstring ErrorString(const Duco::DatabaseSettings& /*settings*/, bool showWarnings) const;

    // To create a composition
    DllExport bool AddCallByPositionNumber(unsigned int position, Duco::TLeadType type, const Duco::ObjectId& changeMethodId = KNoId);
    DllExport bool AddCall(wchar_t position, Duco::TLeadType type, const Duco::ObjectId& changeMethodId = KNoId);
    DllExport bool AddCalls(wchar_t position, const std::wstring& callingStr);
    DllExport void MoveToHome();
    DllExport std::wstring StartingLeadEnd() const;
    DllExport std::wstring EndChange(bool all, bool showTreble) const;

    // Used to prove the composition, generate all lead ends, music etc
    DllExport bool Ready() const;
    DllExport bool PrepareToProve(const MethodDatabase& methodDb, const Duco::DatabaseSettings& settings);
    DllExport bool PrepareToProve(Duco::Method& method, const Duco::DatabaseSettings& settings);
    DllExport bool Prove(Duco::ProgressCallback& callback, const std::string& installationDir, bool stopAtFalse = true, bool stopAtRounds = true);
    inline bool ProveInProgress() const;
    DllExport bool Proven() const;
    DllExport bool ProvenTrue() const;

    DllExport Composition& operator=(const Duco::Composition& other);

public: // from RingingObject
    virtual Duco::TObjectType ObjectType() const;
    DllExport virtual bool Duplicate(const Duco::RingingObject& other, const Duco::RingingDatabase& database) const;

    // Operators
    virtual bool operator==(const Duco::RingingObject& other) const;
    virtual bool operator!=(const Duco::RingingObject& other) const;

    // I/O
    virtual std::wostream& Print(std::wostream& stream, const RingingDatabase& database) const;
    virtual DatabaseWriter& Externalise(DatabaseWriter& writer) const;
#ifndef __ANDROID__
    virtual void ExportToXml(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& parent, bool exportDucoObjects) const;
#endif

protected: // from RingingObject
    DatabaseReader& Internalise(DatabaseReader& reader, unsigned int databaseVersion);

protected:
    void DeleteAllLeads();
    void CopyAllLeads(const Composition& other);
    void Reset();

protected:
    std::vector<Duco::Calling*>   callings;
    Duco::ObjectId          composerId;
    std::wstring*           composerName;
    Duco::ObjectId          methodId;
    size_t                  callingId;
    std::wstring*           name;
    unsigned int            repeats;
    std::wstring*           notes;
    size_t                  snapStartChangesMissing;
    size_t                  snapFinishChangesMissing;
    size_t                  noOfChanges;
    bool                    proven;

    // Not stored.
    bool                     proveInProgress;
    Duco::LeadEndCollection* leadEnds;
    Duco::ChangeCollection*  changes;

    Duco::Music*             musicChecker;
    PlaceNotation*           notation;
    Change*                  currentEndChange;
    unsigned int             currentPosition;
};

bool
Composition::ProveInProgress() const
{
    return proveInProgress;
}

bool
Composition::IsSnapFinish() const
{
    return snapFinishChangesMissing > 0;
}

size_t
Composition::SnapFinish() const
{
    return snapFinishChangesMissing;
}

bool
Composition::IsSnapStart() const
{
    return snapStartChangesMissing > 0;
}

size_t
Composition::SnapStart() const
{
    return snapStartChangesMissing;
}

} // end namespace Duco

#endif //!__COMPOSITION_H__

