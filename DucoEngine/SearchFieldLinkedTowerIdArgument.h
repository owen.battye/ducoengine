#ifndef __SEARCHFIELDLINKEDTOWERIDARGUMENT_H__
#define __SEARCHFIELDLINKEDTOWERIDARGUMENT_H__
#include "SearchFieldIdArgument.h"
#include <list>
#include <set>

namespace Duco
{

class TSearchFieldLinkedTowerIdArgument : public TSearchFieldIdArgument
{
public:
    DllExport explicit TSearchFieldLinkedTowerIdArgument(const Duco::ObjectId& newFieldValue, Duco::TSearchType newSearchType = EEqualTo);
    virtual ~TSearchFieldLinkedTowerIdArgument();

    virtual bool Match(const Duco::Peal& object, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Tower& object, const Duco::RingingDatabase& database) const;
 
protected:
    bool MatchTowerId(const Duco::ObjectId& objectId, const Duco::RingingDatabase& database) const;

    mutable std::list<std::set<Duco::ObjectId> >* parentTowerIds;
};

}
#endif //__SEARCHFIELDLINKEDTOWERIDARGUMENT_H__
