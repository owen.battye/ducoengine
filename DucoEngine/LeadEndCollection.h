#ifndef __LEADENDCOLLECTION_H__
#define __LEADENDCOLLECTION_H__

#include "DucoEngineCommon.h"
#include "LeadType.h"
#include <list>
#include <cstddef>

namespace Duco
{
    class Change;
    class LeadEnd;
    class Lead;

class LeadEndCollection
{
public:
    DllExport LeadEndCollection();
    DllExport LeadEndCollection(const LeadEndCollection& rhs);
    DllExport virtual ~LeadEndCollection();

    // Accessors
    inline size_t NoOfLeads() const;
    DllExport void Start();
    DllExport bool MoreLeadEnds();
    DllExport const Duco::LeadEnd& LeadEnd();

    DllExport void Clear();

    // Settors
    bool Add(const Change& newChange, TLeadType leadType, bool isCourseEnd);
    bool Add(const Lead& nextLead, bool isCourseEnd);

    void SetPartEnd();

    DllExport LeadEndCollection& operator=(const LeadEndCollection& rhs);
    DllExport bool Check(size_t leadEndNumber, const Duco::Change& leadEnd) const;

protected:
    void DeleteAllLeadEnds();

protected:
    std::list<Duco::LeadEnd*>* leadEnds;
    std::list<Duco::LeadEnd*>::const_iterator leadEndsIt;
};

size_t
LeadEndCollection::NoOfLeads() const
{
    return leadEnds->size();
}

}

#endif //!__LEADENDCOLLECTION_H__

