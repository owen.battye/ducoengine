#pragma once
#ifndef __ASSOCIATIONDATABASE_H__
#define __ASSOCIATIONDATABASE_H__

#include "RingingObjectDatabase.h"
#include <xercesc/dom/DOMDocument.hpp>

namespace Duco
{
    class Association;
    class RenumberProgressCallback;

    class AssociationDatabase : public RingingObjectDatabase
    {
    public:
        DllExport AssociationDatabase();
        DllExport ~AssociationDatabase();

        // from RingingObjectDatabase
        DllExport virtual Duco::ObjectId AddObject(const Duco::RingingObject& newObject);
        virtual bool Internalise(DatabaseReader& reader, Duco::ImportExportProgressCallback* newCallback, int databaseVersionNumber, size_t noOfObjectsInThisDatabase, size_t totalObjects, size_t objectsProcessedSoFar);
        virtual bool RebuildObjects(Duco::RenumberProgressCallback* callback, Duco::RingingDatabase& database);
        virtual bool MatchObject(const Duco::RingingObject& object, const Duco::TSearchArgument& searchArg, const Duco::RingingDatabase& database) const;
        virtual bool ValidateObject(const Duco::RingingObject& object, const Duco::TSearchValidationArgument& searchArg, const std::set<Duco::ObjectId>& idsToCheckAgainst, const Duco::RingingDatabase& database) const;
        virtual void ExternaliseToXml(Duco::ImportExportProgressCallback* newCallback, XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, bool exportDucoObjects) const;
        virtual bool UpdateObjectField(const Duco::ObjectId& id, const Duco::TUpdateArgument& updateArgument);

        // new functions
        DllExport const Duco::Association* const FindAssociation(const Duco::ObjectId& associationId, bool setIndex = false) const;
        DllExport std::wstring FindAssociationName(const Duco::ObjectId& associationId) const;
        DllExport Duco::ObjectId SuggestAssociation(const std::wstring& name, bool fromImport = false) const;
        DllExport Duco::ObjectId SuggestAssociationOrCreate(const std::wstring& name, bool fromImport = false);
        DllExport Duco::ObjectId CreateAssociation(const std::wstring& name);
        DllExport void FindLinksToAssociation(const Duco::ObjectId& associationId, std::set<Duco::ObjectId>& links);

        DllExport bool UppercaseAssociations(ProgressCallback*const callback);
        DllExport void FindDuplicateIds(std::map<Duco::ObjectId, Duco::ObjectId>& objectIds) const;
        void MergeLinkedAssociationsPealCounts(std::map<Duco::ObjectId, Duco::PealLengthInfo>& sortedPealCounts) const;
        void RemoveUsedIds(std::set<Duco::ObjectId>& unusedAssociationsIds) const;
        bool ReplaceAssociationLinks(ProgressCallback* callback, const Duco::ObjectId& oldAssociationId, const Duco::ObjectId& newAssociationId);

    protected:
        // from RingingObjectDatabase
        bool SaveUpdatedObject(Duco::RingingObject& lhs, const Duco::RingingObject& rhs);
    };

}
#endif //!__ASSOCIATIONDATABASE_H__