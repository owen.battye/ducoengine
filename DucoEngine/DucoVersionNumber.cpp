#include "DucoVersionNumber.h"

#include "DucoEngineUtils.h"
#include <cstdlib>
#include "DucoConfiguration.h"

using namespace Duco;
using namespace std;

#define VersionDelimiter '.'

DucoVersionNumber::DucoVersionNumber(const std::wstring& versionNo)
:   majorVerNo(0), minorVerNo(0), buildNo(0)
{
    if (versionNo.length() > 0)
    {
        wstring::size_type lastPos = versionNo.find_first_not_of(VersionDelimiter, 0);
        wstring::size_type pos = versionNo.find_first_of(VersionDelimiter, lastPos);
        wstring majorStr = versionNo.substr(lastPos, pos - lastPos);
        majorVerNo = _wtoi(majorStr.c_str());

        if (pos == string::npos)
        {
            minorVerNo = 1;
            buildNo = 1;
        }
        else
        {
            std::wstring versionWithoutMajor = versionNo.substr(pos + 1);

            lastPos = versionWithoutMajor.find_first_not_of(VersionDelimiter);
            pos = versionWithoutMajor.find_first_of(VersionDelimiter, lastPos + 1);
            wstring minorStr = versionWithoutMajor.substr(lastPos, pos - lastPos);
            minorVerNo = _wtoi(minorStr.c_str());

            if (pos == string::npos)
            {
                buildNo = 1;
            }
            else
            {
                versionWithoutMajor = versionWithoutMajor.substr(pos + 1);

                lastPos = versionWithoutMajor.find_first_not_of(VersionDelimiter);
                pos = versionWithoutMajor.find_first_of(VersionDelimiter, pos + 1);
                wstring buildNoStr = versionWithoutMajor.substr(lastPos, pos - lastPos);
                buildNo = _wtoi(buildNoStr.c_str());
            }
        }
    }
    else
    {
        majorVerNo = 0;
        minorVerNo = 0;
        buildNo = 0;
    }
}

DucoVersionNumber::~DucoVersionNumber()
{

}

std::wstring
DucoVersionNumber::Str() const
{
    const unsigned int bufferSize = 12;
    const unsigned int bufferSizeWithNull = bufferSize + 1;
    wchar_t buffer[bufferSizeWithNull];
    if (majorVerNo > 2000)
    {
        _snwprintf(buffer, bufferSize, L"%04u.%02u.%02u", majorVerNo, minorVerNo, buildNo);
    }
    else
    {
        _snwprintf(buffer, bufferSize, L"%u.%u.%u", majorVerNo, minorVerNo, buildNo);
    }
    return buffer;
}

bool
DucoVersionNumber::operator<(const Duco::DucoVersionNumber& rhs) const
{
    if (majorVerNo < rhs.majorVerNo)
        return true;
    if (majorVerNo == rhs.majorVerNo && minorVerNo < rhs.minorVerNo)
        return true;
    if (majorVerNo == rhs.majorVerNo && minorVerNo == rhs.minorVerNo && buildNo < rhs.buildNo)
        return true;

    return false;
}

bool
DucoVersionNumber::operator==(const Duco::DucoVersionNumber& rhs) const
{
    if (majorVerNo != rhs.majorVerNo)
        return false;
    if (minorVerNo != rhs.minorVerNo)
        return false;
    if (buildNo != rhs.buildNo)
        return false;

    return true;
}
