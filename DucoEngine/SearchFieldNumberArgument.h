#ifndef __SEARCHFIELDNUMBERARGUMENT_H__
#define __SEARCHFIELDNUMBERARGUMENT_H__

#include "SearchArgument.h"
#include "DucoEngineCommon.h"
#include <cstddef>

namespace Duco
{
    class Peal;
    class Tower;
    class Ringer;
    class Method;

class TSearchFieldNumberArgument : public TSearchArgument
{
public:
    DllExport explicit TSearchFieldNumberArgument(Duco::TSearchFieldId fieldId, size_t newFieldValue, Duco::TSearchType newSearchType = EEqualTo);
    Duco::TFieldType FieldType() const;
    ~TSearchFieldNumberArgument();
 
    virtual bool Match(const Duco::Peal& peal, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Tower& tower, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Ringer& ringer, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Method& method, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::MethodSeries& object, const Duco::RingingDatabase& database) const;

protected:
    bool CompareDualOrder(const Duco::Peal& peal, const Duco::RingingDatabase& database) const;
    bool CompareNumber(size_t numberToCompare) const;
    bool CompareSeriesCount(const Duco::Peal& peal, const Duco::RingingDatabase& database) const;

protected:
    const size_t				fieldValue;
    const Duco::TSearchType     searchType;
};

}

#endif // __SEARCHFIELDNUMBERARGUMENT_H__

