#ifndef __MERGEDATABASE_H__
#define __MERGEDATABASE_H__

#include <string>
#include <list>
#include <map>
#include <set>
#include "DucoEngineCommon.h"
#include "ImportExportProgressCallback.h"

namespace Duco
{
    class ObjectId;
    class RingingDatabase;
    class ProgressCallback;
    class Peal;
    class Method;
    class Tower;

class MergeDatabase : public Duco::ImportExportProgressCallback
{
public:
    DllExport MergeDatabase(Duco::ProgressCallback& theCallback, Duco::RingingDatabase& currentDatabase);
    DllExport ~MergeDatabase();

    DllExport void ReadDatabase(const std::string& databaseFileName, const Duco::ObjectId& onlyWithRingerId = KNoId, const std::wstring& towerbaseId = L"");
    DllExport void Merge(const std::set<Duco::ObjectId>& pealIdsToImport);

    DllExport std::wstring DefaultRingerExternalName() const;
    inline const Duco::ObjectId& DefaultRingerExternalId() const;
    DllExport std::wstring TowerExternalName() const;
    inline const Duco::ObjectId& TowerExternalId() const;
    DllExport const Duco::Peal* const FindPeal(const Duco::ObjectId& id) const;
    DllExport std::wstring MethodName(const Duco::ObjectId& id) const;
    DllExport std::wstring AssociationName(const Duco::ObjectId& id) const;
    DllExport std::wstring TowerName(const Duco::ObjectId& id) const;

    // Found peals - if Ringer id was specified on read - only those containing that ringer.
    inline const std::set<Duco::ObjectId>& PealIds() const;

    // from ImportExportProgressCallback
    virtual void InitialisingImportExport(bool internalising, unsigned int versionNumber, size_t totalNumberOfObjects);
    virtual void ObjectProcessed(bool internalising, Duco::TObjectType objectType, Duco::ObjectId objectId, int totalPercentage, size_t numberInThisSubDatabase);
    virtual void ImportExportComplete(bool internalising);
    virtual void ImportExportFailed(bool internalising, int errorCode);

protected:
    void CheckRingId(Peal& newPeal, const Peal& oldExternalPeal, const Duco::ObjectId& internalTowerId, const Tower& externalTower);

protected:
    Duco::ProgressCallback&                 callback;
    Duco::RingingDatabase&                  currentDatabase;
    Duco::ObjectId                          ringerId;
    std::wstring                            towerbaseId;

    Duco::RingingDatabase*                  otherDatabase;
    Duco::ObjectId                          externalRingerId;
    Duco::ObjectId                          externalTowerId;

    std::map<Duco::ObjectId,Duco::ObjectId> externalRingerIdsToInternal;
    std::map<Duco::ObjectId,Duco::ObjectId> externalMethodIdsToInternal;
    std::map<Duco::ObjectId,Duco::ObjectId> externalTowerIdsToInternal;
    std::map<Duco::ObjectId,Duco::ObjectId> externalAssociationIdsToInternal;
    std::map<Duco::ObjectId,Duco::ObjectId> externalPictureIdsToInternal;
    std::map<Duco::ObjectId,Duco::ObjectId> externalCompositionIdsToInternal;
    std::map<Duco::ObjectId,Duco::ObjectId> externalMethodSeriesIdsToInternal;

    std::list<unsigned int>                 externalOrderNames;
    std::set<Duco::ObjectId>                externalPealIds;
    std::set<Duco::ObjectId>                requiredTowers;
    std::set<Duco::ObjectId>                requiredMethods;
    std::set<Duco::ObjectId>                requiredRingers;
    std::set<Duco::ObjectId>                requiredAssociations;
    std::set<Duco::ObjectId>                requiredMethodSeries;
    std::set<Duco::ObjectId>                requiredCompositions;
    std::set<Duco::ObjectId>                requiredPictures;

    Duco::ObjectId                          defaultRingerId;
};

const std::set<Duco::ObjectId>&
MergeDatabase::PealIds() const
{
    return externalPealIds;
}

const Duco::ObjectId&
MergeDatabase::DefaultRingerExternalId() const
{
    return externalRingerId;
}

const Duco::ObjectId&
MergeDatabase::TowerExternalId() const
{
    return externalTowerId;
}

}

#endif //__MERGEDATABASE_H__

