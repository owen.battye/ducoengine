#include "FelsteadPealCount.h"

#include "DatabaseSettings.h"
#include <cpr/cpr.h>
#include "DucoEngineUtils.h"
#include <thread>
#include "Tower.h"

using namespace Duco;
using namespace std;

namespace Duco
{
    std::string stringToFind("valid peals for");
}

FelsteadPealCount::FelsteadPealCount(const Duco::DatabaseSettings& newSettings, const Duco::Tower& tower)
    : numberOfPeals(0), settings(newSettings), towerId(tower.Id()), felsteadId(tower.TowerbaseId()), downloadThread(NULL), success(false)
{

}

FelsteadPealCount::~FelsteadPealCount()
{
    if (downloadThread != NULL)
    {
        delete downloadThread;
    }
}

bool
FelsteadPealCount::StartDownload()
{
    try
    {
        downloadThread = new std::jthread(&FelsteadPealCount::Download, this);
    }
    catch (std::exception const&)
    {
        return false;
    }

    return true;
}

void
FelsteadPealCount::Download()
{
    success = false;
    try
    {
        std::wstring url = settings.FelsteadTowerURL();
        url += felsteadId;
        std::string s(url.begin(), url.end());

        cpr::Response r = cpr::Get(cpr::Url{ s.c_str() });

        success = SearchString(r.text);

    }
    catch (std::exception const&)
    {
        numberOfPeals = 0;
        success = false;
    }
}

void
FelsteadPealCount::Wait()
{
    if (downloadThread != NULL)
    {
        downloadThread->join();
    }
}


void
FelsteadPealCount::ImportFile(const char* towerFilename)
{
    std::ifstream input (towerFilename, ios::in | ios::binary);
    bool found = false;
    success = false;

    while (!found && input.good())
    {
        char line[1024];
        input.getline(line, 1024);

        std::string searchStr = line;
        if (SearchString(line))
        {
            success = true;
        }
    }
}


bool
FelsteadPealCount::SearchString(const std::string& data)
{
    std::wstring::size_type index = data.find(stringToFind);
    if (index != std::wstring::npos)
    {
        std::wstring::size_type startIndex = data.rfind(">", index);

        std::string pealCount = data.substr(startIndex + 1, index - startIndex - 1);
        numberOfPeals = atol(pealCount.c_str());
        return true;
    }
    return false;
}


size_t
FelsteadPealCount::NumberOfPeals() const
{
    if (!success)
        return -1;

    return numberOfPeals;
}

const Duco::ObjectId&
FelsteadPealCount::TowerId() const
{
    return towerId;
}
