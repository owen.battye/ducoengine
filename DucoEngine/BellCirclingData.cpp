#include "BellCirclingData.h"
#include <algorithm>

using namespace Duco;
using namespace std;

BellCirclingData::BellCirclingData(const Duco::PerformanceDate& pealDate)
{
    AddPeal(pealDate);
}

BellCirclingData::~BellCirclingData()
{

}

void
BellCirclingData::AddPeal(const Duco::PerformanceDate& pealDate)
{
    pealCounts.push_back (pealDate);
    std::sort(pealCounts.begin(), pealCounts.end());
}

Duco::PerformanceDate
BellCirclingData::FirstPealDate() const
{
    std::vector<PerformanceDate>::const_iterator it = pealCounts.begin();
    return PerformanceDate(*it);
}

Duco::PerformanceDate
BellCirclingData::LastPealDate() const
{
    std::vector<PerformanceDate>::const_reverse_iterator it = pealCounts.rbegin();
    return PerformanceDate(*it);
}

Duco::PerformanceDate
BellCirclingData::PealDate(size_t pealNumber) const
{
    if (pealNumber > pealCounts.size())
    {
        PerformanceDate earliestDate;
        earliestDate.ResetToEarliest();
        return earliestDate;
    }
    return pealCounts[pealNumber-1];
}

size_t
BellCirclingData::PealCount() const
{
    return pealCounts.size();
}