#pragma once
#ifndef __DOWNLOADEDPEAL_H__
#define __DOWNLOADEDPEAL_H__

#include "Peal.h"
#include "DucoEngineCommon.h"
#include "MissingRinger.h"
#include <set>

namespace Duco
{
    class DownloadedPeal : public Duco::Peal
    {
    public:
        // New functions
        bool SaveMissingRinger(const Duco::MissingRinger& missingRingerInfo);
        DllExport bool IsMissingRinger(unsigned int bellNo, bool strapper) const;
        DllExport bool IsMissingRingerConductor(unsigned int bellNo, bool strapper) const;
        DllExport std::wstring MissingRinger(unsigned int bellNo, bool strapper, const Duco::DatabaseSettings& settings) const;
        DllExport std::wstring MissingRingers(const Duco::DatabaseSettings& settings) const;
        DllExport bool HasMissingRingers() const;

        void SaveMissingMethod(const std::wstring& methodName);
        DllExport const std::wstring& MissingMethod() const;

        void SaveMissingAssociation(const std::wstring& newAssociationName);
        DllExport const std::wstring& MissingAssociation() const;
        void SaveMissingTower(const std::wstring& towerDedication, const std::wstring& towerName, const std::wstring& towerCounty, const std::wstring& towerbaseId, const std::wstring& doveId);
        DllExport const std::wstring& MissingTower() const;
        DllExport const std::wstring& MissingTowerbaseId() const;
        DllExport const std::wstring& MissingDoveId() const;
        void SaveMissingRing(const std::wstring& tenorWeight, const std::wstring& tenorKey);
        DllExport std::wstring MissingRing(const Duco::RingingDatabase& ringingDb) const;
        DllExport bool MissingRingOnly() const;
        DllExport const std::wstring& MissingTenorWeight() const;
        DllExport const std::wstring& MissingTenorKey() const;

        //from Peal
        DllExport void SetAssociationId(const Duco::ObjectId& newAssociationId);
        DllExport void SetTowerId(const ObjectId& newTowerId);
        DllExport void SetRingId(const ObjectId& newRingId);
        DllExport virtual unsigned int NoOfRingers(bool includeStrappers = true) const;
        DllExport virtual bool ConductorOnBell(unsigned int bellNo, bool strapper) const;
        DllExport virtual std::wstring ConductorName(const RingerDatabase& database, const Duco::DatabaseSettings& settings) const;
        using Peal::ConductorName;
        DllExport void SetRingerId(const ObjectId& newRingerId, unsigned int newBellNumber, bool asStrapper);
        DllExport void Clear();

    private:
        std::wstring missingAssociation;
        std::wstring missingTower;
        std::wstring missingMethod;
        std::wstring missingTenorWeight;
        std::wstring missingTenorKey;
        std::wstring missingTowerbaseId;
        std::wstring missingDoveId;
        std::set<Duco::MissingRinger> missingRingers;
    };

} // end namespace Duco

#endif // __DOWNLOADEDPEAL_H__
