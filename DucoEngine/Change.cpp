#include "Change.h"

using namespace std;
using namespace Duco;

#include "DucoEngineUtils.h"
#include <algorithm>

Change::Change(unsigned int noOfBells)
    : numberOfBells(noOfBells), changeType(EOther), startingPosOfMusic(-1), endingPosOfMusic(-1), rollup(false)

{
    GenerateRounds();
}

Change::Change(const Change& other)
    : numberOfBells(other.numberOfBells), row(other.row), changeType(other.changeType),
    startingPosOfMusic(other.startingPosOfMusic), endingPosOfMusic(other.endingPosOfMusic),
    rollup(other.rollup)
{

}

Change::Change(const std::wstring& change)
    : numberOfBells(change.length()), changeType(EOther), startingPosOfMusic(-1), endingPosOfMusic(-1), rollup(false)
{
    std::set<unsigned int> uniqueBells;
    if (change.find('1') == string::npos)
    {
        uniqueBells.insert(1);
        row.push_back(1);
        numberOfBells += 1;
    }

    std::wstring::const_iterator it = change.begin();
    while (it != change.end())
    {
        unsigned int bellNumber = DucoEngineUtils::ToInteger(*it);
        row.push_back(bellNumber);
        if (!uniqueBells.insert(bellNumber).second)
        {
            std::string errorMsg = "Invalid bells specified, bell ";
            errorMsg += *it;
            errorMsg += " repeated";
            throw std::invalid_argument(errorMsg);
        }
        ++it;
    }
}

Change::Change(const std::wstring& partialChange, unsigned int noOfBells)
    : numberOfBells(noOfBells), changeType(EOther), startingPosOfMusic(-1), endingPosOfMusic(-1), rollup(false)
{
    std::wstring change = partialChange;
    if (change.find('1') == string::npos)
    {
        change.insert(change.begin(), '1');
    }
    while (change.length() < noOfBells)
    {
        change.push_back(DucoEngineUtils::ToChar(change.length() + 1));
    }
    std::set<unsigned int> uniqueBells;
    std::wstring::const_iterator it = change.begin();
    while (it != change.end())
    {
        unsigned int bellNumber = DucoEngineUtils::ToInteger(*it);
        row.push_back(bellNumber);
        if (!uniqueBells.insert(bellNumber).second)
        {
            std::string errorMsg = "Invalid bells specified, bell ";
            errorMsg += *it;
            errorMsg += " repeated";
            throw std::invalid_argument(errorMsg);
        }
        ++it;
    }
}

Change*
Change::Realloc() const
{
    return new Change(*this);
}

Change::~Change()
{
}

void
Change::GenerateRounds()
{
    row.clear();

    for (unsigned int i (1); i <= numberOfBells; ++i)
    {
        row.push_back(i);
    }
    changeType = EOther;
    rollup = false;
}

void
Change::Reset()
{
    GenerateRounds();
    startingPosOfMusic = -1;
    endingPosOfMusic = -1;
    rollup = false;
}

std::wstring
Change::Str(bool all, bool showTreble) const
{
    unsigned int count (numberOfBells);
    if (!all)
    {
        std::vector<unsigned int>::const_reverse_iterator it2 = row.rbegin();
        bool stop (false);
        while (it2 != row.rend() && !stop)
        {
            if ((*it2) != count)
            {
                stop= true;
            }
            else
                --count;
            ++it2;
        }
        count = max<unsigned int>(count, 6);
    }

    std::wstring changeAsString;

    std::vector<unsigned int>::const_iterator it = row.begin();
    if (it != row.end())
    {
        if (showTreble || (*it) != 1)
            changeAsString += DucoEngineUtils::ToChar(*it);
        ++it;
    }
    --count;
    while (it != row.end() && count-- > 0)
    {
        changeAsString += DucoEngineUtils::ToChar(*it);
        ++it;
    }

    return changeAsString;
}

bool
Change::SwapPair(unsigned int position)
{
    if (position <= 0 || position >= numberOfBells)
        return false;

    unsigned int lowerPos = row[position-1];

    row[position-1] = row[position];
    row[position] = lowerPos;
    changeType = EOther;
    rollup = false;
    startingPosOfMusic = -1;
    endingPosOfMusic = -1;

    return true;
}

bool
Change::Position(unsigned int bellNumber, unsigned int& position) const
{
    position = 1;
    
    std::vector<unsigned int>::const_iterator it = row.begin();
    while (it != row.end())
    {
        if (*it == bellNumber)
        {
            return true;
        }
        ++position;
        ++it;
    }
    position = -1;
    return false;
}

bool
Change::IsBefore() const
{
    return ( (row[1] == numberOfBells && row[2] == (numberOfBells-1)) || 
             (row[2] == numberOfBells && row[1] == (numberOfBells-1)) );
}

bool
Change::Bell(unsigned int position, unsigned int& bellNumber) const
{
    bellNumber = -1;
    unsigned int count (1);
    std::vector<unsigned int>::const_iterator it = row.begin();
    while (it != row.end())
    {
        if (count == position)
        {
            bellNumber = *it;
            return true;
        }
        ++count;
        ++it;
    }
    bellNumber = -1;
    return false;
}

unsigned int
Change::CallingPosition() const
{
    unsigned int currentTenorPos (-1);
    Position(numberOfBells, currentTenorPos);
    return currentTenorPos;
}

bool
Change::TreblePosition(unsigned int& position) const
{
    return Position(1, position);
}

Change&
Change::operator=(const Change& other)
{
    numberOfBells = other.numberOfBells;
    row.clear();
    row = other.row;
    changeType = other.changeType;
    startingPosOfMusic = other.startingPosOfMusic;
    endingPosOfMusic = other.endingPosOfMusic;
    rollup = other.rollup;

    return *this;
}

bool
Change::operator<(const Change& other) const
{
    if (numberOfBells != other.numberOfBells)
        return numberOfBells < other.numberOfBells;

    for (unsigned int i(0); i < row.size(); ++i)
    {
        if (row[i] != other.row[i])
            return row[i] < other.row[i];
    }
    return false;
}

bool
Change::operator==(const Change& other) const
{
    if (numberOfBells != other.numberOfBells)
        return false;
    if (row.size() != other.row.size())
        return false;

    for (unsigned int i(0); i < row.size(); ++i)
    {
        if (row[i] != other.row[i])
            return false;
    }

    return true;
}

bool
Change::operator!=(const Change& other) const
{
    if (numberOfBells != other.numberOfBells)
        return true;
    if (row.size() != other.row.size())
        return true;
    if (changeType != other.changeType)
        return true;

    for (unsigned int i(0); i < row.size(); ++i)
    {
        if (row[i] != other.row[i])
            return true;
    }

    return false;
}

unsigned int
Change::operator[](unsigned int position) const
{
    return row[position];
}

bool
Change::Differences(const Change& other, std::vector<unsigned int>& affectedBells) const
{
    affectedBells.clear();
    for (unsigned int i(0); i < other.numberOfBells && i < numberOfBells; ++i)
    {
        if (other.row[i] != row[i])
            affectedBells.push_back(row[i]);
    }
    return affectedBells.size() > 0;
}

bool
Change::DifferencesFromRounds(const Change& other, std::vector<unsigned int>& affectedBells)
{
    Change rhs (other.numberOfBells);
    return rhs.Differences(other, affectedBells);
}

bool
Change::IsRounds() const
{
    Change rounds(numberOfBells);
    return (*this) == rounds;
}

void
Change::Bells(std::set<unsigned int>& bells) const
{
    bells.clear();
    std::vector<unsigned int>::const_iterator it = row.begin();
    while (it != row.end())
    {
        bells.insert(*it);
        ++it;
    }
}

bool
Change::IsHome() const
{
    std::vector<unsigned int>::const_reverse_iterator it = row.rbegin();
    if (it != row.rend() && (*it) == Order())
    {
        return true;
    }
    return false;
}

void
Change::SetContainsRollup(unsigned int betweenStart, unsigned int betweenEnd)
{
    unsigned int diffBetweenCurrent (endingPosOfMusic - startingPosOfMusic);
    unsigned int diffBetweenNew (betweenEnd - betweenStart);
    if (diffBetweenNew > diffBetweenCurrent)
    {
        startingPosOfMusic = betweenStart;
        endingPosOfMusic = betweenEnd;
        rollup = true;
    }
}

void
Change::SetContainsMusic(unsigned int betweenStart, unsigned int betweenEnd)
{
    unsigned int diffBetweenCurrent (endingPosOfMusic - startingPosOfMusic);
    unsigned int diffBetweenNew (betweenEnd - betweenStart);
    if (diffBetweenNew > diffBetweenCurrent)
    {
        startingPosOfMusic = betweenStart;
        endingPosOfMusic = betweenEnd;
        rollup = false;
    }
}
