#include "Lead.h"

using namespace std;
using namespace Duco;

#include "Change.h"
#include "Calling.h"
#include "ChangeCollection.h"
#include "Method.h"

Lead::Lead(const Duco::Method& newMethod)
:   currentMethod(newMethod), leadType(EPlainLead), bobLeadEnd(NULL), singleLeadEnd(NULL)
{
}

Lead::~Lead()
{
    Clear();
}

void
Lead::Clear()
{
    std::vector<Duco::Change*>::iterator it = changes.begin();
    while (it != changes.end())
    {
        delete *it;
        ++it;
    }
    changes.clear();

    delete bobLeadEnd;
    bobLeadEnd = NULL;
    delete singleLeadEnd;
    singleLeadEnd = NULL;
}

bool
Lead::operator==(const Lead& other) const
{
    if (currentMethod != other.currentMethod)
        return false;
    if (other.leadType != leadType)
        return false;
    if (other.bobLeadEnd != NULL && bobLeadEnd != NULL && *other.bobLeadEnd != *bobLeadEnd)
        return false;
    if (other.singleLeadEnd != NULL && singleLeadEnd != NULL && *other.singleLeadEnd != *singleLeadEnd)
        return false;
    if (other.changes.size() != changes.size())
        return false;

    for (unsigned int i(0); i < changes.size(); ++i)
    {
        Duco::Change* lhs = changes[i];
        Duco::Change* rhs = other.changes[i];
        if ((*rhs) != (*lhs))
            return false;
    }

    return true;
}

bool
Lead::operator!=(const Lead& other) const
{
    if (currentMethod != other.currentMethod)
        return true;
    if (other.leadType != leadType)
        return true;
    if (other.bobLeadEnd != NULL && bobLeadEnd != NULL && *other.bobLeadEnd != *bobLeadEnd)
        return true;
    if (other.singleLeadEnd != NULL && singleLeadEnd != NULL && *other.singleLeadEnd != *singleLeadEnd)
        return true;
    if (other.changes.size() != changes.size())
        return true;

    for (unsigned int i(0); i < changes.size(); ++i)
    {
        const Duco::Change* const lhs = changes[i];
        const Duco::Change* const rhs = other.changes[i];
        if ((*rhs) != (*lhs))
            return true;
    }

    return false;
}

void
Lead::SetLeadEndChange(Duco::TLeadType newType, const Duco::Change& leadEndChange)
{
    if (newType == EBobLead)
    {
        if (bobLeadEnd != NULL)
        {
            delete bobLeadEnd;
        }
        bobLeadEnd = leadEndChange.Realloc();
    }
    else if (newType == ESingleLead)
    {
        if (singleLeadEnd != NULL)
        {
            delete singleLeadEnd;
        }
        singleLeadEnd = leadEndChange.Realloc();
    }
}

bool
Lead::SetLeadEndType(Duco::TLeadType newType)
{
    switch (newType)
    {
    case EBobLead:
        if (bobLeadEnd == NULL)
        {
            return false;
        }
        leadType = EBobLead;
        break;
    case ESingleLead:
        if (singleLeadEnd == NULL)
        {
            return false;
        }
        leadType = ESingleLead;
        break;
    
    default:
        break;
    }
    return true;
}


size_t
Lead::BlowsInLead() const
{
    size_t noOfBlows = changes.size();
    return noOfBlows-1;
}

bool
Lead::LeadEnd(Duco::Change& change) const
{
    return LeadEnd(leadType, change);
}

bool
Lead::LeadEnd(Duco::TLeadType newType, Duco::Change& change) const
{
    switch (newType)
    {
    case EPlainLead:
    default:
        if (!changes.empty())
        {
            change = **changes.rbegin();
            return true;
        }
    case EBobLead:
        if (bobLeadEnd != NULL)
        {
            change = *bobLeadEnd;
            return true;
        }
        break;
    case ESingleLead:
        if (singleLeadEnd != NULL)
        {
            change = *singleLeadEnd;
            return true;
        }
        break;
    }
    change.Reset();
    return false;
}

bool
Lead::LeadEndDifferencesFromRounds(std::vector<unsigned int>& bells) const
{
    bells.clear();
    if (changes.empty())
        return false;

    Duco::Change rounds (currentMethod.Order());
    rounds.Differences(**changes.rbegin(), bells);

    return bells.size() > 0;
}

bool
Lead::AffectedBells(Duco::TLeadType newType, std::vector<unsigned int>& bells) const
{
    bells.clear();
    if (changes.empty())
        return false;

    switch (newType)
    {
    case EBobLead:
        if (bobLeadEnd != NULL)
        {
            bobLeadEnd->Differences(**changes.rbegin(), bells);
        }
        break;

    case ESingleLead:
        if (singleLeadEnd != NULL)
        {
            singleLeadEnd->Differences(**changes.rbegin(), bells);
        }
        break;

    default:
        return false;
    }

    return bells.size() > 0;
}

bool
Lead::IsHome(Duco::TLeadType newType) const
{
    switch (newType)
    {
    case EBobLead:
        {
        if (bobLeadEnd == NULL)
            return false;
        return bobLeadEnd->IsHome();
        }
    case ESingleLead:
        {
        if (singleLeadEnd == NULL)
            return false;
        return singleLeadEnd->IsHome();
        }
    default:
        break;
    }
    if (changes.empty())
        return false;

    return (*changes.rbegin())->IsHome();
}

bool
Lead::IsCallingPosition(const Duco::Calling& calling) const
{
    return IsCallingPosition(calling.LeadType(), calling.Position());
}

bool
Lead::IsCallingPosition(Duco::TLeadType newType, const Duco::Calling& calling) const
{
    return IsCallingPosition(newType, calling.Position());
}

bool
Lead::IsCallingPosition(Duco::TLeadType newType, unsigned int position) const
{
    unsigned int currentPosition (-1);
    if (!CallingPosition(newType, currentPosition))
    {
        return false;
    }

    return currentPosition == position;
}

bool
Lead::CallingPosition(Duco::TLeadType newType, unsigned int& currentPosition) const
{
    if (currentPosition <= 0 || currentPosition > currentMethod.Order())
        currentPosition = currentMethod.Order();
    
    switch (newType)
    {
    case EBobLead:
        {
        if (bobLeadEnd == NULL)
            return false;
        return bobLeadEnd->Position(currentPosition, currentPosition);
        }
    case ESingleLead:
        {
        if (singleLeadEnd == NULL)
            return false;
        return singleLeadEnd->Position(currentPosition, currentPosition);
        }
    default:
        break;
    }
    if (changes.empty())
        return false;

    return (*changes.rbegin())->Position(currentPosition, currentPosition);
}

bool
Lead::EndsWithRounds() const
{
    return EndsWithRounds(leadType);
}

bool
Lead::EndsWithRounds(Duco::TLeadType newType) const
{
    Duco::Change lastChange(currentMethod.Order());
    if (!Lead::LeadEnd(newType, lastChange))
        return false;

    return lastChange.IsRounds();
}

bool
Lead::Position(size_t changeNo, unsigned int bell, unsigned int& position) const
{
    position = -1;
    if (changeNo >= 0 && changeNo < changes.size())
    {
        return Change(changeNo).Position(bell, position);
    }
    return false;
}

bool
Lead::LeadEndPosition(unsigned int bell, unsigned int& position) const
{
    return Position(BlowsInLead(), bell, position);
}

bool
Lead::Bell(unsigned int changeNo, unsigned int position, unsigned int& bell) const
{
    bell = -1;
    if (changeNo >= 0 && changeNo < changes.size())
    {
        bell = Change(changeNo)[position];
        return true;
    }
    return false;
}

bool
Lead::TreblePosition(unsigned int changeNo, unsigned int& position) const
{
    position = -1;
    if (changeNo >= 0 && changeNo < changes.size())
    {
        const Duco::Change* it = (changes)[changeNo];
        return it->Position(1, position);
    }
    return false;
}

const Change&
Lead::Change(size_t changeNo) const
{
    if ((changeNo+1) >= changes.size() && leadType != EPlainLead)
    {
        if (leadType == EBobLead && bobLeadEnd != NULL)
            return *bobLeadEnd;
        else if (leadType == ESingleLead && singleLeadEnd != NULL)
            return *singleLeadEnd;
    }
    return *(changes)[changeNo];
}

Change&
Lead::Change(size_t changeNo)
{
    if ((changeNo+1) >= changes.size() && leadType != EPlainLead)
    {
        if (leadType == EBobLead && bobLeadEnd != NULL)
            return *bobLeadEnd;
        else if (leadType == ESingleLead && singleLeadEnd != NULL)
            return *singleLeadEnd;
    }
    return *(changes)[changeNo];
}

void
Lead::Changes(Duco::ChangeCollection& allChanges, bool stopAtFalse, bool stopAtRounds) const
{
    std::vector<Duco::Change*>::const_iterator it = changes.begin();
    if (it != changes.end())
    {   // to avoid the previous lead end.
        ++it;
    }

    bool rounds (false);
    unsigned int noOfChangesAdded (0);
    while (it != changes.end() && !rounds)
    { // Skip both lead ends
        if (!(*it)->IsLeadEnd())
        {
            if (allChanges.Add(**it) || !stopAtFalse)
            {
                ++noOfChangesAdded;
                if (stopAtRounds)
                    rounds = (*it)->IsRounds();
            }
        }
        ++it;
    }

    if (rounds)
    { // If rounds occured before the lead end remember where.
        size_t changesFromLeadMissing (changes.size() - 1);
        changesFromLeadMissing -= noOfChangesAdded;
        allChanges.SetSnapFinish(changesFromLeadMissing);
    }
    else
    {
        // Add the final lead end.
        Duco::Change tempChange((*changes.begin())->Order());
        if (LeadEnd(leadType, tempChange))
        {
            allChanges.Add(tempChange);
        }
    }
}

bool
Lead::IsCourseEnd() const
{
    Duco::Change lastChange(currentMethod.Order());
    if (!Lead::LeadEnd(lastChange))
        return false;

    unsigned int tenorPosition (-1);
    lastChange.Position(currentMethod.Order(), tenorPosition);
    return tenorPosition == currentMethod.Order();
}

unsigned int
Lead::Order() const
{
    return currentMethod.Order();
}