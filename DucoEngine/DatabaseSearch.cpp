#include "DatabaseSearch.h"

#include "AssociationDatabase.h"
#include "CompositionDatabase.h"
#include "DatabaseSettings.h"
#include "MethodDatabase.h"
#include "MethodSeriesDatabase.h"
#include "PealDatabase.h"
#include "PictureDatabase.h"
#include "ProgressCallback.h"
#include "RingerDatabase.h"
#include "RingingDatabase.h"
#include "SearchDuplicateObject.h"
#include "SearchFieldPealFeesArgument.h"
#include "SearchFieldNumberArgument.h"
#include "SearchGroup.h"
#include "SearchValidObject.h"
#include "SearchArgument.h"
#include "SearchValidationGroup.h"
#include "UpdateArgument.h"
#include "Tower.h"
#include "TowerDatabase.h"
#include <algorithm>
#include "SearchValidationArgument.h"
#include <mutex>

using namespace Duco;

DatabaseSearch::DatabaseSearch(Duco::RingingDatabase& theDatabase)
    : database(theDatabase),
    containsInconsistantFeesCheck(NULL),
    includeWarnings(false), updateArgument(NULL)
{
}

DatabaseSearch::~DatabaseSearch()
{
    Clear();
}

void
DatabaseSearch::Clear()
{
    if (containsInconsistantFeesCheck != NULL)
    {
        // delete containsInconsistantFeesCheck; Doesnt need deleting - already in the list of searchArguments to be deleted
        containsInconsistantFeesCheck = NULL;
    }
    std::deque<TSearchArgument*>::iterator it = searchArguments.begin();
    while (it != searchArguments.end())
    {
        TSearchArgument* nextArgument = *it;
        delete nextArgument;
        ++it;
    }
    searchArguments.clear();
    std::deque<TSearchValidationArgument*>::iterator it2 = validationArguments.begin();
    while (it2 != validationArguments.end())
    {
        TSearchValidationArgument* nextArgument = *it2;
        delete nextArgument;
        ++it2;
    }
    validationArguments.clear();
    if (updateArgument != NULL)
    {
        delete updateArgument;
        updateArgument = NULL;
    }
}

size_t
DatabaseSearch::AddArgument(Duco::TSearchArgument* newArgument)
{
    searchArguments.push_back(newArgument);
    if (newArgument->FieldId() == EInconsistantFees)
    {
        TSearchFieldPealFeesArgument* inconsistantFeesArg = static_cast<TSearchFieldPealFeesArgument*>(newArgument);
        containsInconsistantFeesCheck = inconsistantFeesArg;
    }

    return searchArguments.size();
}

size_t
DatabaseSearch::AddValidationArgument(Duco::TSearchValidationArgument* newArgument)
{
    if (newArgument->FieldId() == ENotValid)
    {
        TSearchValidObject* newArg = static_cast<TSearchValidObject*>(newArgument);
        TSearchValidationGroup* group = new TSearchValidationGroup(EOrGroup);
        group->AddArgument(newArgument);
        TSearchDuplicateObject* duplicate = new TSearchDuplicateObject(newArg->IncludeWarnings());
        group->AddArgument(duplicate);

        validationArguments.push_back(group);
    }
    else
    {
        validationArguments.push_back(newArgument);
    }

    return searchArguments.size();
}

void
DatabaseSearch::AddUpdateArgument(Duco::TUpdateArgument* newArgument)
{
    if (updateArgument != NULL)
    {
        delete updateArgument;
    }
    updateArgument = newArgument;
}

void
DatabaseSearch::Search(Duco::ProgressCallback& callback, std::set<Duco::ObjectId>& foundIds, Duco::TObjectType objectType) const
{
    if (NoOfArguments() <= 0)
        return;

    static std::mutex g_search_mutex;
    if (!g_search_mutex.try_lock())
        return;

    foundIds.clear();
    for (auto& it : validationArguments)
    {
        it->Reset();
    }
    callback.Initialised();

    bool doubleProgressCallbacks = validationArguments.size() > 0 && searchArguments.size() > 0;
    if (validationArguments.size() > 0)
    {
        switch (objectType)
        {
        case TObjectType::ETower:
            database.TowersDatabase().ClearValidationFlags();
            break;
        case TObjectType::EPeal:
            database.PealsDatabase().ClearValidationFlags();
            break;
        case TObjectType::ERinger:
            database.RingersDatabase().ClearValidationFlags();
            break;
        case TObjectType::EMethod:
            database.MethodsDatabase().ClearValidationFlags();
            break;
        case TObjectType::EMethodSeries:
            database.MethodSeriesDatabase().ClearValidationFlags();
            break;
        case TObjectType::EComposition:
            database.CompositionsDatabase().ClearValidationFlags();
            break;
        case TObjectType::EAssociationOrSociety:
            database.AssociationsDatabase().ClearValidationFlags();
            break;
        case TObjectType::EPicture:
            database.PicturesDatabase().ClearValidationFlags();
            break;
        default:
            break;
        }
    }

    switch (objectType)
    {
    case TObjectType::ETower:
        database.TowersDatabase().SearchObjects(callback, foundIds, searchArguments, database, doubleProgressCallbacks);
        break;
    case TObjectType::EPeal:
        if (containsInconsistantFeesCheck != NULL)
        {
            database.PealsDatabase().MedianPealFeesPerPerson(containsInconsistantFeesCheck->AvgPealFees(), containsInconsistantFeesCheck->AssociationId());
        }
        database.PealsDatabase().SearchObjects(callback, foundIds, searchArguments, database, doubleProgressCallbacks);
        break;
    case TObjectType::ERinger:
        database.RingersDatabase().SearchObjects(callback, foundIds, searchArguments, database, doubleProgressCallbacks);
        break;
    case TObjectType::EMethod:
        database.MethodsDatabase().SearchObjects(callback, foundIds, searchArguments, database, doubleProgressCallbacks);
        break;
    case TObjectType::EMethodSeries:
        database.MethodSeriesDatabase().SearchObjects(callback, foundIds, searchArguments, database, doubleProgressCallbacks);
        break;
    case TObjectType::EComposition:
        database.CompositionsDatabase().SearchObjects(callback, foundIds, searchArguments, database, doubleProgressCallbacks);
        break;
    case TObjectType::EAssociationOrSociety:
        database.AssociationsDatabase().SearchObjects(callback, foundIds, searchArguments, database, doubleProgressCallbacks);
        break;
    case TObjectType::EPicture:
        database.PicturesDatabase().SearchObjects(callback, foundIds, searchArguments, database, doubleProgressCallbacks);
        break;
    default:
        break;
    }

    if (foundIds.size() == 0 && searchArguments.size() == 0)
    { // NO search terms so need all objects
        switch (objectType)
        {
        case TObjectType::ETower:
            {
                std::multimap<Duco::ObjectId, Duco::ObjectId> allRingIds;
                database.TowersDatabase().GetAllObjectIds(foundIds, allRingIds);
            }
            break;
        case TObjectType::EPeal:
            database.PealsDatabase().GetAllObjectIds(foundIds);
            break;
        case TObjectType::ERinger:
            database.RingersDatabase().GetAllObjectIds(foundIds);
            break;
        case TObjectType::EMethod:
            database.MethodsDatabase().GetAllObjectIds(foundIds);
            break;
        case TObjectType::EMethodSeries:
            database.MethodSeriesDatabase().GetAllObjectIds(foundIds);
            break;
        case TObjectType::EComposition:
            database.CompositionsDatabase().GetAllObjectIds(foundIds);
            break;
        case TObjectType::EAssociationOrSociety:
            database.AssociationsDatabase().GetAllObjectIds(foundIds);
            break;
        case TObjectType::EPicture:
            database.PicturesDatabase().GetAllObjectIds(foundIds);
            break;
        default:
            break;
        }
    }

    switch (objectType)
    {
    case TObjectType::ETower:
        database.TowersDatabase().ValidateObjects(callback, foundIds, validationArguments, database, doubleProgressCallbacks);
        break;
    case TObjectType::EPeal:
        database.PealsDatabase().ValidateObjects(callback, foundIds, validationArguments, database, doubleProgressCallbacks);
        break;
    case TObjectType::ERinger:
        database.RingersDatabase().ValidateObjects(callback, foundIds, validationArguments, database, doubleProgressCallbacks);
        break;
    case TObjectType::EMethod:
        database.MethodsDatabase().ValidateObjects(callback, foundIds, validationArguments, database, doubleProgressCallbacks);
        break;
    case TObjectType::EMethodSeries:
        database.MethodSeriesDatabase().ValidateObjects(callback, foundIds, validationArguments, database, doubleProgressCallbacks);
        break;
    case TObjectType::EComposition:
        database.CompositionsDatabase().ValidateObjects(callback, foundIds, validationArguments, database, doubleProgressCallbacks);
        break;
    case TObjectType::EAssociationOrSociety:
        database.AssociationsDatabase().ValidateObjects(callback, foundIds, validationArguments, database, doubleProgressCallbacks);
        break;
    case TObjectType::EPicture:
        database.PicturesDatabase().ValidateObjects(callback, foundIds, validationArguments, database, doubleProgressCallbacks);
        break;
    default:
        break;
    }

    if (UpdateArgument())
    {
        switch (objectType)
        {
            case TObjectType::EMethod:
                database.MethodsDatabase().UpdateObjects(callback, foundIds, *updateArgument);
                break;
            case TObjectType::EPeal:
                database.PealsDatabase().UpdateObjects(callback, foundIds, *updateArgument);
                break;
            case TObjectType::ETower:
                database.TowersDatabase().UpdateObjects(callback, foundIds, *updateArgument);
                break;
            default:
                break;
        }
    }


    callback.Complete(false);
    g_search_mutex.unlock();
}

bool
DatabaseSearch::Update(ProgressCallback& callback, const std::set<Duco::ObjectId>& objectIds, Duco::TObjectType objectTypeRequired, TObjectType objectIdsType) const
{
    callback.Initialised();
    bool objectUpdated (false);

    std::set<ObjectId> convertedObjectIds;

    if (objectTypeRequired != objectIdsType)
    {
        if (objectIdsType != TObjectType::EPeal)
            return false;

        if (objectTypeRequired == TObjectType::EMethod)
        {
            database.PealsDatabase().GetMethodIds(objectIds, convertedObjectIds);
        }
        else if (objectTypeRequired == TObjectType::ETower)
        {
            database.PealsDatabase().GetTowerIds(objectIds, convertedObjectIds);
        }
    }
    else
    {
        convertedObjectIds = objectIds;
    }

    if (UpdateArgument())
    {
        switch (objectTypeRequired)
        {
        case TObjectType::EMethod:
            objectUpdated = database.MethodsDatabase().UpdateObjects(callback, convertedObjectIds, *updateArgument);
            break;
        case TObjectType::ETower:
            objectUpdated = database.TowersDatabase().UpdateObjects(callback, convertedObjectIds, *updateArgument);
            break;
        case TObjectType::EPeal:
            objectUpdated = database.PealsDatabase().UpdateObjects(callback, convertedObjectIds, *updateArgument);
            break;
        default:
            break;
        }
    }
    callback.Complete(false);
    return objectUpdated;
}

std::list<Duco::ObjectId>
DatabaseSearch::Sort(const std::set<Duco::ObjectId>& objectIds, Duco::TObjectType objectTypeToSearch, const Duco::ObjectId& distanceFrom) const
{
    // Only support sort at the moment, is towers
    if (objectTypeToSearch != TObjectType::ETower)
    {
        std::list<Duco::ObjectId> objectIdsList(objectIds.begin(), objectIds.end());
        return objectIdsList;
    }

    std::list<Duco::ObjectId> sortedId;
    const Tower* const distanceFromTower = database.TowersDatabase().FindTower(distanceFrom);
    if (distanceFromTower != NULL)
    {
        std::map<double, Duco::ObjectId> sortedObjects;
        for (const auto& objectId : objectIds)
        {
            const Tower* tower = database.TowersDatabase().FindTower(objectId);
            if (tower != NULL)
            {
                sortedObjects.insert(std::make_pair(tower->DistanceFrom(*distanceFromTower), tower->Id()));
            }
        }

        for (const auto& sortedObject : sortedObjects)
        {
            sortedId.push_back(sortedObject.second);
        }
    }
    return sortedId;
}
