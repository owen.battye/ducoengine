﻿#include "DoveFileConfiguration.h"
#include <sstream>
#include <string>
#include <vector>

using namespace std;
using namespace Duco;

DoveFileConfiguration::DoveFileConfiguration()
{
    oldDoveIdFieldNumber = -1;
    doveIdFieldNumber = -1;
    ringIdFieldNumber = -1;
    towerbaseIdFieldNumber = -1;
    countyFieldNumber = -1;
    countryFieldNumber = -1;
    countryIso3166FieldNumber = -1; // ISO3166
    townFieldNumber = -1;
    nameFieldNumber = -1;
    dedicatationFieldNumber = -1;
    bellsFieldNumber = -1;
    tenorFieldNumber = -1;
    approxTenorFieldNumber = -1;
    keyFieldNumber = -1;
    unringableFieldNumber = -1;
    altNameFieldNumber = -1;
    latitudeFieldNumber = -1;
    longitudeFieldNumber = -1;
    semiTonesFieldNumber = -1;
    groundFloorFieldNumber = -1;
    websiteFieldNumber = -1;
    extraInfoFieldNumber = -1;
}

DoveFileConfiguration::~DoveFileConfiguration()
{

}

void
DoveFileConfiguration::Configure(const std::wstring& fileLine)
{
    std::vector<std::wstring> tokens;
    wstringstream tokeniseLine(fileLine);
    std::wstring intermediate;

    while (getline(tokeniseLine, intermediate, L','))
    {
        tokens.push_back(intermediate);
    }

    int fieldNumber(0);
    int ignoreFieldCount(0);
    std::vector<std::wstring>::const_iterator it = tokens.begin();
    while (it != tokens.end())
    {
        wstring fieldValue = *it;
        if (fieldValue == L"﻿TowerID")
        {
            doveIdFieldNumber = fieldNumber;
        }
        else if (fieldValue == L"RingID")
        {
            ringIdFieldNumber = fieldNumber;
        }
        else if (fieldValue == L"DoveID")
        {
            oldDoveIdFieldNumber = fieldNumber;
        }
        else if (fieldValue == L"TowerBase")
        {
            towerbaseIdFieldNumber = fieldNumber;
        }
        else if (fieldValue == L"County")
        {
            countyFieldNumber = fieldNumber;
        }
        else if (fieldValue == L"Country")
        {
            countryFieldNumber = fieldNumber;
        }
        else if (fieldValue == L"ISO3166code")
        {
            countryIso3166FieldNumber = fieldNumber;
        }
        else if (fieldValue == L"Place")
        {
            townFieldNumber = fieldNumber;
        }
        else if (fieldValue == L"Place2")
        {
            nameFieldNumber = fieldNumber;
        }
        else if (fieldValue == L"Dedicn")
        {
            dedicatationFieldNumber = fieldNumber;
        }
        else if (fieldValue == L"Bells")
        {
            bellsFieldNumber = fieldNumber;
        }
        else if (fieldValue == L"Wt")
        {
            tenorFieldNumber = fieldNumber;
        }
        else if (fieldValue == L"App")
        {
            approxTenorFieldNumber = fieldNumber;
        }
        else if (fieldValue == L"Note")
        {
            keyFieldNumber = fieldNumber;
        }
        else if (fieldValue == L"UR")
        {
            unringableFieldNumber = fieldNumber;
        }
        else if (fieldValue == L"AltName")
        {
            altNameFieldNumber = fieldNumber;
        }
        else if (fieldValue == L"Lat")
        {
            latitudeFieldNumber = fieldNumber;
        }
        else if (fieldValue == L"Long")
        {
            longitudeFieldNumber = fieldNumber;
        }
        else if (fieldValue == L"Semitones")
        {
            semiTonesFieldNumber = fieldNumber;
        }
        else if (fieldValue == L"GF")
        {
            groundFloorFieldNumber = fieldNumber;
        }
        else if (fieldValue == L"WebPage")
        {
            websiteFieldNumber = fieldNumber;
        }
        else if (fieldValue == L"ExtraInfo")
        {
            extraInfoFieldNumber = fieldNumber;
        }
        else
        {
            ++ignoreFieldCount;
        }
        ++it;
        ++fieldNumber;
    }
}

bool
DoveFileConfiguration::ConfigValid() const
{
    if (doveIdFieldNumber == -1 ||
        oldDoveIdFieldNumber == -1 ||
        ringIdFieldNumber == -1 ||
        towerbaseIdFieldNumber == -1 ||
        countyFieldNumber == -1 ||
        countryFieldNumber == -1 ||
        countryIso3166FieldNumber == -1 ||
        townFieldNumber == -1 ||
        nameFieldNumber == -1 ||
        dedicatationFieldNumber == -1 ||
        bellsFieldNumber == -1 ||
        tenorFieldNumber == -1 ||
        approxTenorFieldNumber == -1 ||
        keyFieldNumber == -1 ||
        unringableFieldNumber == -1 ||
        altNameFieldNumber == -1 ||
        latitudeFieldNumber == -1 ||
        longitudeFieldNumber == -1 ||
        groundFloorFieldNumber == -1 ||
        websiteFieldNumber == -1 ||
        extraInfoFieldNumber == -1)
        return false;

        return true;
}

unsigned int
DoveFileConfiguration::OldDoveIdFieldNumber() const
{
    return oldDoveIdFieldNumber;
}

unsigned int
DoveFileConfiguration::DoveIdFieldNumber() const
{
    return doveIdFieldNumber;
}

unsigned int
DoveFileConfiguration::TowerbaseIdFieldNumber() const
{
    return towerbaseIdFieldNumber;
}

unsigned int
DoveFileConfiguration::RingIdFieldNumber() const
{
    return ringIdFieldNumber;
}


unsigned int
DoveFileConfiguration::CountyFieldNumber() const
{
    return countyFieldNumber;
}

unsigned int
DoveFileConfiguration::CountryFieldNumber() const
{
    return countryFieldNumber;
}

unsigned int
DoveFileConfiguration::CountryIso3166FieldNumber() const // ISO3166
{
    return countryIso3166FieldNumber;
}

unsigned int
DoveFileConfiguration::TownFieldNumber() const
{
    return townFieldNumber;
}

unsigned int
DoveFileConfiguration::NameFieldNumber() const
{
    return nameFieldNumber;
}

unsigned int
DoveFileConfiguration::DedicatationFieldNumber() const
{
    return dedicatationFieldNumber;
}

unsigned int
DoveFileConfiguration::BellsFieldNumber() const
{
    return bellsFieldNumber;
}

unsigned int
DoveFileConfiguration::TenorFieldNumber() const
{
    return tenorFieldNumber;
}

unsigned int
DoveFileConfiguration::ApproxTenorFieldNumber() const
{
    return approxTenorFieldNumber;
}

unsigned int
DoveFileConfiguration::KeyFieldNumber() const
{
    return keyFieldNumber;
}

unsigned int
DoveFileConfiguration::UnringableFieldNumber() const
{
    return unringableFieldNumber;
}

unsigned int
DoveFileConfiguration::AltNameFieldNumber() const //(T)
{
    return altNameFieldNumber;
}

unsigned int
DoveFileConfiguration::LatitudeFieldNumber() const
{
    return latitudeFieldNumber;
}

unsigned int
DoveFileConfiguration::LongitudeFieldNumber() const
{
    return longitudeFieldNumber;
}

unsigned int
DoveFileConfiguration::SemiTonesFieldNumber() const
{
    return semiTonesFieldNumber;
}

unsigned int
DoveFileConfiguration::GroundFloorFieldNumber() const
{
    return groundFloorFieldNumber;
}

unsigned int 
DoveFileConfiguration::WebsiteFieldNumber() const
{
    return websiteFieldNumber;
}

unsigned int
DoveFileConfiguration::ExtraInfoFieldNumber() const
{
    return extraInfoFieldNumber;
}

