#ifndef __ROLLUP_H__
#define __ROLLUP_H__

#include "DucoEngineCommon.h"
#include <string>
#include <list>

namespace Duco
{
    class Change;
    class ChangeCollection;

class Rollup
{
public:
    DllExport Rollup(const std::wstring& description);
    DllExport virtual ~Rollup();

    // Accessors
    inline unsigned int Order() const;
    DllExport unsigned int Count() const;

    // Settors

    // Other
    DllExport bool Match(Change& changes) const;
    DllExport std::wstring Print() const;

protected:
    void SetBells(const std::wstring& str);

protected:
    unsigned int                noOfBells;
    std::list<unsigned int>     bells;
    std::wstring*                name;
    mutable unsigned int        noOfMatches;
};

unsigned int
Rollup::Order() const
{
    return noOfBells;
}

}

#endif //!__ROLLUP_H__

