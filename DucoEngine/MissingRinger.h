#ifndef __MISSINGRINGER_H__
#define __MISSINGRINGER_H__

#include <string>
#include "DucoEngineCommon.h"

namespace Duco
{
    struct MissingRinger
    {
        DllExport MissingRinger();

        DllExport bool operator<(const Duco::MissingRinger& lhs) const;
        bool operator==(const Duco::MissingRinger& lhs) const;
        std::wstring Name(bool lastNameFirst) const;

        unsigned int    bellNo;
        bool            strapper;
        std::wstring     surname;
        std::wstring     forename;
        bool            conductor;
    };

} // end namespace Duco

#endif //!__MISSINGRINGER_H__
