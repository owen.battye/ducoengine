#ifndef __PEALDATABASE_H__
#define __PEALDATABASE_H__

#include "RingingObjectDatabase.h"

#include "BellboardPerformanceParserCallback.h"
#include "CirclingData.h"
#include "DucoEngineCommon.h"
#include "MilestoneData.h"
#include "PercentageStatistics.h"
#include "UnpaidPealFeeData.h"
#include "ReferenceItem.h"

#include <set>
#include <list>
#include <xercesc/dom/DOMDocument.hpp>
#include "PealLengthInfo.h"
#include "RingerPealDates.h"

namespace Duco
{
    class AlphabetData;
    class AssociationDatabase;
    class CompositionDatabase;
    class DatabaseSettings;
    class PerformanceDate;
    class PerformanceTime;
    class RingerDatabase;
    class RingerCirclingData;
    class Method;
    class MethodSeries;
    class MethodDatabase;
    class MethodSeriesDatabase;
    class StatisticFilters;
    class TowerDatabase;
    class Tower;
    class Peal;
    class LeadingBellData;

    class ProgressCallback;
    class RenumberProgressCallback;

    struct PealYearDay
    {
        unsigned int month;
        unsigned int day;
        bool operator() (const PealYearDay& lhs, const PealYearDay& rhs) const
        {
            return lhs.month == rhs.month ? lhs.day < rhs.day : lhs.month < rhs.month;
        }
    };

    struct YearCirclingStats
    {
        unsigned int count;
        unsigned int firstYear;
    };

class PealDatabase: public RingingObjectDatabase, Duco::BellboardPerformanceParserCallback
{
public:
    PealDatabase(Duco::RingerDatabase& ringersDatabase, Duco::MethodDatabase& methodsDatabase, Duco::TowerDatabase& theTowersDatabase, Duco::MethodSeriesDatabase& theSeriesDatabase, Duco::CompositionDatabase& theCompositionsDatabase, Duco::AssociationDatabase& theAssociationDatabase, Duco::DatabaseSettings& theSettings);
    ~PealDatabase();

    // from RingingObjectDatabase
    DllExport virtual ObjectId AddObject(const Duco::RingingObject& newObject);
    virtual bool Internalise(DatabaseReader& reader, Duco::ImportExportProgressCallback* newCallback, int databaseVersionNumber, size_t noOfObjectsInThisDatabase, size_t totalObjects, size_t objectsProcessedSoFar);
    virtual bool RebuildObjects(Duco::RenumberProgressCallback* callback, Duco::RingingDatabase& database);
    virtual bool MatchObject(const Duco::RingingObject& object, const Duco::TSearchArgument& searchArg, const Duco::RingingDatabase& database) const;
    virtual bool ValidateObject(const Duco::RingingObject& object, const Duco::TSearchValidationArgument& searchArg, const std::set<Duco::ObjectId>& idsToCheckAgainst, const Duco::RingingDatabase& database) const;
    DllExport virtual bool RebuildRecommended() const;
    virtual void ExternaliseToXml(Duco::ImportExportProgressCallback* newCallback, XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, bool exportDucoObjects) const;
    virtual void ExternaliseToBellboardXml(Duco::ImportExportProgressCallback* newCallback, std::set<Duco::ObjectId> performanceIds, XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile) const;
    virtual void ExternaliseToCsv(Duco::ImportExportProgressCallback* newCallback, std::wofstream& file) const;
    virtual bool UpdateObjectField(const Duco::ObjectId& id, const Duco::TUpdateArgument& updateArgument);

    // BellboardPerformanceParserCallback
    void Cancelled(const std::wstring& bellboardId, const char* message);
    void Completed(const std::wstring& bellboardId, bool errors);

    // New Functions
    DllExport const Peal* const FindPeal(const ObjectId& pealId) const;
    DllExport Peal* FindPeal(const ObjectId& pealId, bool setIndex = false);
    DllExport Duco::ObjectId FindPeal(const Duco::PerformanceDate& pealDate) const;
    DllExport Duco::ObjectId FindPealByBellboardId(const std::wstring& bellboardId) const;
    size_t CheckBellBoardForUpdatedReferences(Duco::ProgressCallback& progressCallback, Duco::RingingDatabase& database, bool onlyThoseWithoutRWPages, bool& cancellationFlag);
    void GetMethodIds(const std::set<ObjectId>& pealIds, std::set<ObjectId>& methodIds) const;
    void GetTowerIds(const std::set<ObjectId>& pealIds, std::set<ObjectId>& towerIds) const;

    //*************************************************************
    // General statistics
    //*************************************************************
    DllExport bool GeneratePealSpeedStats(const Duco::StatisticFilters& filters, bool fastest, std::multimap<float, Duco::ObjectId>& pealIds) const;
    //GetUniqueCounts - Year and number of objects
    DllExport void GetUniqueCounts(const Duco::StatisticFilters& filters, std::map<unsigned int, size_t>& towerCounts, std::map<unsigned int, size_t>& ringerCounts, std::map<unsigned int, size_t>& theMethodCounts) const;
    DllExport bool Milestones(const Duco::StatisticFilters& filters, std::set<MilestoneData>& mileStones) const;
    DllExport void RemoveUsedIds(std::set<ObjectId>& unusedMethods, std::set<ObjectId>& unusedRingers, std::set<ObjectId>& unusedTowers, std::multimap<ObjectId, ObjectId>& unusedRings, std::set<ObjectId>& unusedMethodSeries, std::set<ObjectId>& unusedCompositions, std::set<ObjectId>& unusedAssociations) const;
    void RemoveUsedPictureIds(std::set<ObjectId>& unusedPictureIds) const;
    void KeepTop(size_t noOfObjects, std::multimap<Duco::PealLengthInfo, Duco::ObjectId>& sortedRingerIds, const std::map<Duco::ObjectId, Duco::PealLengthInfo>& allRingerIds, bool ringers = false) const;
    DllExport void GetYearCirclingStats(const Duco::StatisticFilters& filters, std::map<Duco::PealYearDay, Duco::YearCirclingStats, Duco::PealYearDay>& results) const;
    DllExport Duco::ObjectId FindPealWithAverageTime (const Duco::PerformanceTime& averageTime) const;
    DllExport Duco::ObjectId FindPealWithAverageChanges(unsigned int averageNumberOfChanges) const;
    DllExport Duco::ObjectId FindPealWithAverageChangesPerMinute(double averageNumberOfChanges) const;
    DllExport bool AnyOnThisDay(const Duco::PerformanceDate& pealDay) const;
    DllExport std::set<Duco::ObjectId> OnThisDay(const Duco::PerformanceDate& pealDay) const;
    DllExport void ReferenceStats(const Duco::StatisticFilters& filters, std::map<ObjectId, Duco::ReferenceItem>& pealCounts) const;
    DllExport void GetPealsForYear(const unsigned int pealYear, std::set<ObjectId>& pealIds) const;
    DllExport Duco::PealLengthInfo PerformanceInfo(const Duco::StatisticFilters& filters, const Duco::ObjectId& upToAndIncludePealId = KNoId) const;
    DllExport Duco::PealLengthInfo PerformanceInfo(const Duco::StatisticFilters& filters, const std::set<Duco::ObjectId>& performanceIds) const;
    DllExport bool AnyMatch(const Duco::StatisticFilters& filters) const;
    DllExport Duco::PealLengthInfo AllMatches(const Duco::StatisticFilters& filters, std::set<ObjectId>& performanceIds) const;
    DllExport Duco::PealLengthInfo AllMatches(const Duco::StatisticFilters& filters) const;

    //*************************************************************
    // Ringers
    //*************************************************************
    // Replace ringer deliberatly not exported, so clients have to call the exported function in RingingDatabase
    bool ReplaceRinger(const ObjectId& oldRingerId, const ObjectId& newRingerId, bool deleteOldRinger, const Duco::RingingDatabase& database);
    DllExport void GetRingersPealCount(const Duco::StatisticFilters& filters, bool combineLinked, std::map<Duco::ObjectId, Duco::PealLengthInfo>& sortedRingerIds) const;
    DllExport void GetTopRingers(const Duco::StatisticFilters& filters, size_t noOfRingers, bool combineLinked, std::multimap<Duco::PealLengthInfo, ObjectId>& sortedRingerIds) const;
    DllExport void RingersAroundDates(const Duco::StatisticFilters& filters, std::set<Duco::ObjectId>& ringerIds) const;
    // GetTopRingersPealCountPerYear ringerId, <year, PealLengthInfo>
    DllExport void GetTopRingersPealCountPerYear(const Duco::StatisticFilters& filters, bool combineLinked, size_t noOfRingersToKeep, std::map<ObjectId, std::map<unsigned int, Duco::PealLengthInfo> >& sortedPealCounts) const;
    DllExport void GetAllConductorIds(std::set<ObjectId>& sortedRingerIds) const;
    DllExport void RingerGenderStats(const Duco::StatisticFilters& filters, float& percentMale, float& percentFemale, float& percentNotSet, float& percentNonHuman, float& percentMaleConductors, float& percentFemaleConductors, float& percentNotSetConductors, float& percentNonHumanConductors) const;
    DllExport void GetAllComposersByPealCount(const Duco::StatisticFilters& filters, bool ignoreBracketed, std::map<std::wstring, Duco::PealLengthInfo>& sortedComposers) const;
    DllExport bool AnyPealContainingBothRingers(const Duco::ObjectId& ringerId1, const Duco::ObjectId& ringerId2, bool includeLinked) const;
    DllExport void GetAllConductorsByPealCount(const Duco::StatisticFilters& filters, bool combineLinked, std::multimap<Duco::PealLengthInfo, ObjectId>& sortedConductors) const;
    DllExport void GetAllFeePayers(const Duco::ObjectId& associationId, std::set<Duco::ObjectId>& feePayerIds) const;
    DllExport bool FindPealsWithUnpaidFees(const Duco::ObjectId& feePayerId, const Duco::ObjectId& assocationId, unsigned int year, std::set<Duco::UnpaidPealFeeData>& unpaidPeals) const;
    DllExport void GetConductedOrderCount(const Duco::StatisticFilters& filters, std::map<unsigned int, Duco::PealLengthInfo>& sortedPealCounts) const;
    DllExport void GetConductedMethodCount(const Duco::StatisticFilters& filters, std::map<Duco::ObjectId, Duco::PealLengthInfo>& sortedMethodCounts) const;
    void FindRingerPerformancesNearDate(const Duco::PerformanceDate& performanceDate, const std::set<Duco::ObjectId>& nearMatches, std::set<Duco::RingerPealDates>& nearMatchesWithPealCounts) const;


    //*************************************************************
    // Towers
    //*************************************************************
    DllExport void GetTowersPealCount(const Duco::StatisticFilters& filters, std::map<Duco::ObjectId, Duco::PealLengthInfo>& sortedTowerIds, bool combineLinkedTowers=false) const;
    DllExport void GetTopTowers(size_t noOfTowers, std::list<Duco::ObjectId>& topTowerIds, const Duco::RingingDatabase& database) const;
    DllExport void GetTopTowers(const Duco::StatisticFilters& filters, size_t noOfTowers, std::multimap<Duco::PealLengthInfo, Duco::ObjectId>& topTowerIds) const;
    DllExport void GetTopTower(Duco::ObjectId& topTowerId, Duco::PealLengthInfo& noOfPeals, const Duco::RingingDatabase& database) const;
    DllExport void GetTowerCircling(const Duco::StatisticFilters& filters, std::map<unsigned int, Duco::CirclingData>& sortedBellPealCounts, Duco::PealLengthInfo& noOfPeals, Duco::PerformanceDate& firstCircledDate, size_t& noOfDaysToFirstCircle) const;
    DllExport void GetLeadingBells(const Duco::ObjectId& ringerId, std::set<Duco::LeadingBellData>& sortedBellPealCounts) const;
    DllExport void GetLeadingRingersTowerCircling(const Duco::ObjectId& towerId, std::multimap<Duco::PerformanceDate, RingerCirclingData*>& topCircledRingers, size_t noOfRingers, bool highestStage) const;
    DllExport void GetRingersTowerCircling(const Duco::StatisticFilters& filters, const Duco::ObjectId& lastPealId, const std::set<Duco::ObjectId>& ringerIds, std::map<Duco::ObjectId, std::map<unsigned int, Duco::PealLengthInfo>>& pealCountsOnAllBellsByRingerId) const;
    DllExport Duco::PercentageStatistics GetRingersPercentages(const Duco::ObjectId& towerId, const std::set<Duco::ObjectId>& ringerIds, const Duco::ObjectId& lastPealId) const;
    DllExport void GetRingersInTower(const Duco::ObjectId& towerId, std::set<Duco::ObjectId>& ringers, size_t maxNoOfRingers = -1) const;
    bool ReplaceTower(const Duco::ObjectId& oldTowerId, const Duco::ObjectId& newTowerId);
    DllExport void GetTowersAndPealCount(const Duco::StatisticFilters& filters, bool combineLinkedTowers, std::multimap<Duco::PealLengthInfo, Duco::ObjectId>& towerIds) const;
    DllExport void GetTownNamesWithPealCount(const Duco::StatisticFilters& filters, std::map<std::wstring, Duco::PealLengthInfo>& sortedTowerIds) const;
    DllExport void GetCountiesWithPealCount(const Duco::StatisticFilters& filters, std::map<std::wstring, Duco::PealLengthInfo>& sortedTowerIds) const;
    DllExport void GetTenorKeysWithPealCount(const Duco::StatisticFilters& filters, std::map<std::wstring, Duco::PealLengthInfo>& sortedTowerIds) const;
    DllExport unsigned int NumberOfBellsRungInTower(const Duco::ObjectId& towerId) const;
    DllExport void GetTopTowersPealCountPerYear(const Duco::StatisticFilters& filters, size_t noOfTowers, std::map<Duco::ObjectId, std::map<unsigned int, Duco::PealLengthInfo> >& sortedPealCounts) const;
    DllExport unsigned int LowestValidOrderNumber(const Duco::ObjectId& towerId, const Duco::ObjectId& ringId) const;
    DllExport bool GetFurthestPeals(const Duco::StatisticFilters& filters, Duco::ObjectId& mostNortherly, Duco::ObjectId& mostSoutherly, Duco::ObjectId& mostEasterly, Duco::ObjectId& mostWesterly) const;
    DllExport bool RemoveDuplicateRings(ProgressCallback*const callback);
    DllExport void GetAllStagesByPealCount(const Duco::StatisticFilters& filters, std::map<unsigned int, Duco::PealLengthInfo>& sortedPealCounts, std::map<unsigned int, Duco::PealLengthInfo>& sortedSplicedPealCounts) const;
    DllExport void GetAllTenorKeysByPealCount(const Duco::StatisticFilters& filters, std::map<std::wstring, Duco::PealLengthInfo>& sortedTenorKeys) const;
    // Bells Rung: stage, <bell, count>
    DllExport void GetBellsRung(const Duco::StatisticFilters& filters, std::map<unsigned int, std::map<unsigned int, Duco::PealLengthInfo> >& bellRungCount) const;
    DllExport void FindTowerIdForHandbellPeals(std::set<Duco::ObjectId>& towerIds) const;

    //*************************************************************
    // Methods
    //*************************************************************
    DllExport void GetMethodsPealCount(const Duco::StatisticFilters& filters, std::map<Duco::ObjectId, Duco::PealLengthInfo>& sortedMethodIds) const;
    DllExport void GetTopFiveMethods(const Duco::StatisticFilters& filters, std::multimap<Duco::PealLengthInfo, Duco::ObjectId>& topFiveMethodsIds) const;
    DllExport void GetMethodCircling(const Duco::StatisticFilters& filters, size_t methodOrder, std::map<unsigned int, Duco::CirclingData>& sortedBellPealCounts, PerformanceDate& firstCircledDate) const;
    DllExport void GetAllMethodNamesByPealCount(std::map<std::wstring, Duco::PealLengthInfo>& sortedPealCounts) const;
    DllExport void GetAllMethodTypesByPealCount(const Duco::StatisticFilters& filters, std::map<std::wstring, Duco::PealLengthInfo>& sortedPealCounts) const;
	DllExport bool RemoveDuplicateMethods(ProgressCallback*const callback);
    /**
     * GetAlphabeticCompletions
     * @param alphabets, char is the first character of each method (a-z), unsigned int is the id of the first peal found.
     */
    DllExport unsigned int GetAlphabeticCompletions(const Duco::StatisticFilters& filters, bool uniqueMethods, bool insideOnly, Duco::AlphabetData& alphabets) const;

    //*************************************************************
    // Method Series
    //*************************************************************
    DllExport void GetMethodSeriesPealCount(const Duco::StatisticFilters& filters, std::map<Duco::ObjectId, Duco::PealLengthInfo>& sortedMethodSeriesIds) const;
    DllExport bool PealsInSeries(const Duco::ObjectId& seriesId, Duco::PerformanceDate& pealDate, std::set<Duco::ObjectId>& pealIds) const;
    DllExport void GetRingersInMethodSeries(const Duco::ObjectId& seriesId, std::set<Duco::ObjectId>& ringerIds) const;
    DllExport void GetPealCountInMethodSeriesForRinger(const Duco::ObjectId& ringerId, const Duco::ObjectId& seriesId, std::map<Duco::ObjectId, unsigned int>& methodIdsAndCount, PerformanceDate& completedDate) const;
    DllExport bool CompletedSeries(const Duco::MethodSeries& series) const;

    //*************************************************************
    // Compositions
    //*************************************************************
    DllExport void GetCompositionsPealCount(std::map<Duco::ObjectId, Duco::PealLengthInfo>& sortedCompositionIds) const;
    //*************************************************************
    // Pictures
    //*************************************************************
    DllExport void FindPicture(const Duco::ObjectId& pictureId, std::set<Duco::ObjectId>& pealIds) const;
    void GetPictureIds(std::map<Duco::ObjectId, Duco::ObjectId>& sortedPictureIds) const;
    bool RemovePictures(const std::set<Duco::ObjectId>& pictureIds);
    DllExport bool FindPictureForTower(const Duco::ObjectId& towerId, Duco::ObjectId& pictureId) const;
    //*************************************************************
    // Associations
    //*************************************************************
    DllExport void GetAssociationsPealCount(const Duco::StatisticFilters& filters, std::map<Duco::ObjectId, Duco::PealLengthInfo>& sortedPealCounts, bool mergeLinked) const;
    DllExport bool ReplaceAssociation(ProgressCallback* callback, const Duco::ObjectId& oldAssociationId, const Duco::ObjectId& newAssociationId);
    DllExport void MedianPealFeesPerPerson(std::map<unsigned int, unsigned int>& avgPealFeePerYear, const Duco::ObjectId& associationId) const;
    //*************************************************************
    // Renumbering / reindexing
    //*************************************************************
    bool RenumberRingersInPeals(const std::map<Duco::ObjectId, Duco::ObjectId>& newRingerIds, Duco::RenumberProgressCallback* callback);
    bool RenumberMethodsInPeals(const std::map<Duco::ObjectId, Duco::ObjectId>& newMethodIds, Duco::RenumberProgressCallback* callback);
    bool RenumberMethodSeriesInPeals(const std::map<Duco::ObjectId, Duco::ObjectId>& newMethodSeriesIds, Duco::RenumberProgressCallback* callback);
    bool RenumberCompositionsInPeals(const std::map<Duco::ObjectId, Duco::ObjectId>& newCompositionsIds, Duco::RenumberProgressCallback* callback);
    void RenumberTowersInPeals(const std::map<Duco::ObjectId, Duco::ObjectId>& newTowerIds, Duco::RenumberProgressCallback* callback);
    void RenumberRingsInPeals(const Duco::ObjectId& towerId, std::map<Duco::ObjectId, Duco::ObjectId> oldRingIdsToNewRingIds);
    bool RenumberPicturesInPeals(const std::map<Duco::ObjectId, Duco::ObjectId>& sortedPictureIds, Duco::RenumberProgressCallback* callback);
    bool RenumberAssociationsInPeals(const std::map<Duco::ObjectId, Duco::ObjectId>& sortedAssocaitionIds, Duco::RenumberProgressCallback* callback);
    bool UpdateRingIds(const Duco::ObjectId& theTowerId, const std::map<Duco::ObjectId, Duco::ObjectId>& duplicateRingReplacements);
    DllExport bool SwapPeals(const Duco::ObjectId& pealId1, const Duco::ObjectId& pealId2, unsigned int selectedIndex, bool rebuildIndex = true);
    //*************************************************************
    // Dates and times
    //*************************************************************
    DllExport void GetAllYearsByPealCount(const Duco::StatisticFilters& filters, std::map<unsigned int, Duco::PealLengthInfo>& sortedPealCounts) const;
    DllExport void GetYearRange(const Duco::StatisticFilters& filters, std::set<unsigned int>& years) const;
    DllExport Duco::ObjectId FindFirstPealWithoutBellboardId(Duco::PerformanceDate& lastPealDate) const;
    DllExport void GetNoOfChangesRange(const Duco::StatisticFilters& filters, std::map<unsigned int, Duco::PealLengthInfo>& noOfChanges) const;
    DllExport void GetAllMonthsByPealCount(const Duco::StatisticFilters& filters, unsigned int year, std::map<unsigned int, Duco::PealLengthInfo>& monthCounts) const;
    DllExport void GetDatesWithMultiplePeals(std::list<Duco::PerformanceDate>& dates) const;
    DllExport void GetPealWithDate(Duco::PerformanceDate date, std::vector<Duco::ObjectId>& pealIds) const;
    DllExport std::set<std::wstring> RemoveExistingBellBoardPealIds(std::list<std::wstring>& pealIds) const;
    // stage <year, count>
    DllExport void GetStagesCountByYear(const Duco::StatisticFilters& filters, std::map<unsigned int, std::map<unsigned int, Duco::PealLengthInfo> >& stageYearCounts) const;

protected:
    bool CheckForFirstCircleDate(const std::map<unsigned int, Duco::CirclingData>& sortedBellPealCounts, Duco::PerformanceDate& firstCircledDate, size_t noOfBellsInTower) const;
    bool MatchingPeal(const Peal& peal,
                      const std::wstring& findRingerName, const std::wstring& findTowerName,
                      const std::wstring& findAssocationName, const std::wstring& findMethodName,
                      const std::wstring& findFootNotes, const std::wstring& findComposer) const;

    bool SwapPeals(DUCO_OBJECTS_CONTAINER::iterator& it1, DUCO_OBJECTS_CONTAINER::iterator it2, unsigned int selectedIndex = -1, bool rebuildIndex = true);
    bool SortPeals(Duco::RenumberProgressCallback* callback);
    void SetDaysToMilestone(const Duco::StatisticFilters& filters, size_t currentPealCount, Duco::MilestoneData& milestone, Duco::TMilestoneAverage averageType) const;
    void MilestonePredictions(const Duco::StatisticFilters& filters, size_t pealCount, std::set<MilestoneData>& milestones) const;
    void CheckDetailsForEarlierMostDistantPeal(const Peal& nextPeal, const float& thisPealPosition, float& currentFurthestPosition, const Duco::Peal*& currentPeal, bool greaterThan) const;
    bool CheckBellBoardForUpdatedReference(const Duco::Peal& nextPeal, Duco::RingingDatabase& database);

protected:
    // from RingingObjectDatabase
    bool SaveUpdatedObject(Duco::RingingObject& lhs, const Duco::RingingObject& rhs);

private: // member data
    Duco::RingerDatabase&       ringersDatabase;
    Duco::MethodDatabase&       methodsDatabase;
    Duco::TowerDatabase&        towersDatabase;
    Duco::MethodSeriesDatabase& seriesDatabase;
    Duco::CompositionDatabase&  compositionsDatabase;
    Duco::AssociationDatabase&  associationsDatabase;

    Duco::DatabaseSettings&     settings;
};

}

#endif // __PEALDATABASE_H__
