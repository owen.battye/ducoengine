#include "MusicObject.h"

#include "Change.h"
#include "DucoEngineUtils.h"

#include <algorithm>

using namespace Duco;
using namespace std;

MusicObject::MusicObject()
:   count(0)
{
}

MusicObject::~MusicObject()
{
}

void
MusicObject::SetBells(const std::wstring& str)
{
    bells.clear();
    std::wstring::const_iterator it = str.begin();
    while (it != str.end())
    {
        unsigned int bellNo = DucoEngineUtils::ToInteger(*it);
        bells.push_back(bellNo);
        ++it;
    }
}

bool
MusicObject::Match(Duco::Change& change)
{
    if (change.Order() != bells.size())
        return false;

    std::vector<unsigned int>::const_iterator thisIt = bells.begin();
    std::vector<unsigned int>::const_iterator changeIt = change.row.begin();

    unsigned int firstMatchingPosition ((unsigned int)change.Order());
    unsigned int lastMatchingPosition (0);
    bool match(true);
    unsigned int position(0);
    while (match && thisIt != bells.end() && changeIt != change.row.end())
    {
        if (*thisIt > 0)
        {
            if (*thisIt != *changeIt)
                match = false;
            else
            {
                firstMatchingPosition  = min(firstMatchingPosition, position);
                lastMatchingPosition  = max(lastMatchingPosition, position);
            }
        }


        ++thisIt;
        ++changeIt;
        ++position;
    }

    if (match)
    {
        change.SetContainsMusic(firstMatchingPosition, lastMatchingPosition);
        ++count;
    }
    return match;
}

std::wstring
MusicObject::Name(bool showAll) const
{
    std::wstring returnVal(DucoEngineUtils::ToString(count) + L": ");
    std::vector<unsigned int>::const_iterator it = bells.begin();
    while (it != bells.end())
    {
        if ((*it) != 0)
        {
            returnVal += DucoEngineUtils::ToChar(*it);
        }
        else if (showAll)
        {
            returnVal += L"x";
        }
        ++it;
    }
    return returnVal;
}

std::wstring
MusicObject::Print() const
{
    if (count == 0)
        return L"";

    std::wstring returnVal (DucoEngineUtils::ToString(count));
    if (count == 1)
        returnVal += L" is a ";
    else
        returnVal += L" are ";
    returnVal += Name(true);
    if (count > 1)
        returnVal += L"s";

    return returnVal;
}
