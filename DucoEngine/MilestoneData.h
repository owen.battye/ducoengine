#ifndef __MILESTONEDATA_H__
#define __MILESTONEDATA_H__

#include "ObjectId.h"
#include "ObjectType.h"
#include "PerformanceDate.h"
#include "DucoEngineCommon.h"

namespace Duco
{
    enum class TMilestoneAverage
    {
        E6MonthAverage,
        E12MonthAverage,
        EAllTimeAverage
    };

    class MilestoneData
    {
        friend class PealDatabase;

    public:
        MilestoneData(size_t newPealCount);
        DllExport virtual ~MilestoneData();

        bool operator<(const Duco::MilestoneData& rhs) const;
        void SetPeal(const Duco::ObjectId& pealId, const Duco::PerformanceDate& date);
        DllExport const Duco::ObjectId& PealId() const;
        DllExport const Duco::PerformanceDate& DateAchieved() const;
        
        DllExport bool Prediction() const;
        DllExport bool LastPeal() const;
        DllExport size_t Milestone() const;
        DllExport size_t DaysToMilestoneUsing6MonthAverage() const;
        DllExport size_t DaysToMilestoneUsing12MonthAverage() const;
        DllExport size_t DaysToMilestoneUsingAllTimeAverage() const;

        DllExport size_t PealsIn6MonthWindow() const;
        DllExport size_t PealsIn12MonthWindow() const;
        DllExport size_t PealsInAllTimeWindow() const;

        DllExport Duco::PerformanceDate DateOfMilestoneUsing6MonthAverage() const;
        DllExport Duco::PerformanceDate DateOfMilestoneUsing12MonthAverage() const;
        DllExport Duco::PerformanceDate DateOfMilestoneUsingAllTimeAverage() const;

        DllExport Duco::PerformanceDate StartDate(Duco::TMilestoneAverage type) const;
        DllExport Duco::PerformanceDate StartDateOf6MonthAverageCalculation() const;
        DllExport Duco::PerformanceDate StartDateOf12MonthAverageCalculation() const;
        DllExport Duco::PerformanceDate StartDateOfAllTimeCalculation() const;

    protected:
        const size_t        pealCount;
        bool                lastPeal;

        Duco::ObjectId      pealId;
        Duco::PerformanceDate  dateAchieved;
        size_t              sixMonthAverageDaysToMilestone;
        size_t              twelveMonthAverageDaysToMilestone;
        size_t              allTimeDaysToMilestone;
        size_t              pealsInSixMonthWindow;
        size_t              pealsInTwelveMonthWindow;
        size_t              pealsInAllTimeWindow;
        Duco::PerformanceDate  firstPealDate;
    };
}

#endif //__MILESTONEDATA_H__
