#include "InvalidDatabaseVersionException.h"

#include <cstdio>

using namespace Duco;

InvalidDatabaseVersionException::InvalidDatabaseVersionException(int dbVersion, int expectedDBVersion)
: exception(), actualDatabaseVersion(dbVersion), expectedMaximumDatabaseVersion(expectedDBVersion)
{

}

const char*
InvalidDatabaseVersionException::what() const throw()
{
    return "Database version not supported"; 
}
