#ifndef __DUCOENGINEUTILS_H__
#define __DUCOENGINEUTILS_H__

#include <string>
#include <set>
#include <map>
#include <list>

#include "DatabaseSettings.h"
#include "DucoTypes.h"
#include "PealRingerData.h"
#include "PealLengthInfo.h"

#define KNewlineChar L"\r\n"

namespace Duco
{
    class Ring;
    class Calling;

    class DucoEngineUtils
    {
    public:
        static bool IsWhiteSpace(const wchar_t& inputChar);
        DllExport static std::wstring Trim(const std::wstring& inputStr);
        DllExport static std::wstring TrimTowerBaseId(const std::wstring& inputStr);
        DllExport static std::wstring PadStringWithZeros(const std::wstring& inputString);
        DllExport static bool MethodNameContainsSpliced(const std::wstring& name);
        DllExport static bool RemoveMethodCount(std::wstring& name, std::wstring& bracketedText);

        DllExport static void ToLowerCase(const std::string& string, std::string& newString);
        DllExport static void ToLowerCase(const std::wstring& string, std::wstring& newString);
        DllExport static void ToUpperCase(const std::wstring& string, std::wstring& newString);
        DllExport static std::wstring ToCapitalised(const std::wstring& string, bool everyword);
        DllExport static bool IsEven(unsigned int aCurrentPosition);

        DllExport static bool IsAllUpperCase(const std::wstring& string);
        DllExport static bool IsNumber(const std::wstring& string);

        static std::wstring RemoveNoneDoveChars(const std::wstring& inputStr);
        DllExport static std::wstring MethodComparisionCharacters(const std::wstring& inputStr);
        DllExport static std::wstring RemoveAllButValidChars(const std::wstring& inputStr, const std::wstring& validCharacters);
        static std::wstring RemoveInvalidChars(const std::wstring& inputStr, const std::wstring& invalidCharacters);
        static std::wstring RemoveInvalidChars(DatabaseSettings::TPreferredViewWebsite website, const std::wstring& inputStr);
        DllExport static std::wstring RemoveInvalidCharsFromTenorWeight(const std::wstring& inputStr);

        DllExport static bool ConductorIndicator(const std::wstring& condStr);
        DllExport static std::wstring CheckRingerNameForConductor(const std::wstring& originalName, bool& conducted);
        DllExport static std::wstring RemoveBrackets(const std::wstring& str);
        DllExport static void SplitRingerName(const std::wstring& originalName, std::wstring& forename, std::wstring& surname, bool& isConductor);
        DllExport static bool SplitTowerName(const std::wstring& originalFullName, std::wstring& name, std::wstring& townCity, std::wstring& county);
        DllExport static std::wstring RemovePunctuationFromFirstNames(const std::wstring& firstNames);
        DllExport static std::wstring GetInitialsFromName(const std::wstring& firstNames, bool excludeLast = false);
        DllExport static std::wstring FormatTowerName(const std::wstring& name, const std::wstring& county, const std::wstring& dedication);
        DllExport static std::wstring FormatRingName(const std::wstring& ringName, const std::wstring& tenorWeight, unsigned int noOfBells);
        DllExport static std::wstring FormatRingName(const Duco::Ring& theRing);

        DllExport static bool FindSubstring(const std::wstring& stringToFind, const std::wstring& inString, const std::wstring& charsToIgnore = L"", bool checkBoth = false, size_t minStringLengthRequired = 0);
        DllExport static bool FindSubstring(const std::wstring& stringToFind, const std::wstring& inString, size_t& position, bool checkBoth = false, size_t minStringLengthRequired = 0, bool convertToLower = true);
        DllExport static bool StartsWith(const std::wstring& stringToFind, const std::wstring& inString);
        static bool MatchString(const std::wstring& str, const std::wstring& substr, bool findSubStr);
        static void InsertString(std::set<std::wstring>& list, const std::wstring& string);
        DllExport static unsigned int Count(const std::wstring& stringToSearch, wchar_t charToCount);
        static void ConvertFromMethodMaster(const std::wstring& notationToConvert, std::wstring& convertedNotation);
        static std::wstring PrintToRtf(const std::wstring& origString);
        DllExport static std::wstring EncodeString(const std::wstring& original);

        DllExport static bool CompareString(const std::wstring& string1, const std::wstring& string2, bool ignoreCase = true, bool allowEmptyString = false);
        DllExport static bool CompareTenorWeight(std::wstring string1, std::wstring string2);
        DllExport static std::wstring ReplaceTenorKeySymbols(const std::wstring& tenorKey);

        DllExport static bool MatchingRingingWorldReference(const std::wstring& lhs, const std::wstring& rhs);
        DllExport static std::wstring RingingWorldIssue(const std::wstring& fullReference);
        DllExport static std::wstring RingingWorldPage(const std::wstring& fullReference);


        template<class T>
        DllExport static void Tokenise(const std::wstring& str, std::list<T>& listOfTokens, const std::wstring& delimiters = L",")
        {
            listOfTokens.clear();
            std::wstring lastToken;
            bool insideQuotes = false;

            std::wstring::const_iterator it = str.begin();
            while (it != str.end())
            {
                std::wstring::value_type nextChar = *it;
                if (nextChar == '\"')
                {
                    insideQuotes = !insideQuotes;
                }
                else if (delimiters.find(nextChar) != std::wstring::npos && !insideQuotes)
                {
                    listOfTokens.push_back(T(lastToken));
                    lastToken.clear();
                }
                else
                {
                    lastToken.append(1, nextChar);
                }
                ++it;
            }
            if (lastToken.size() > 0)
            {
                listOfTokens.push_back(T(lastToken));
            }
        }


        DllExport static wchar_t ToChar(unsigned int noOfBells);

        DllExport static std::wstring ToString(const double posn, bool withSeperators = false);
        DllExport static std::wstring ToString(unsigned int number, bool withSeperators = false);
        DllExport static std::wstring ToString(int number, bool withSeperators = false);
        DllExport static std::wstring ToString(long number, bool withSeperators = false);
        DllExport static std::wstring ToString(long long number, bool withSeperators = false);
        DllExport static std::wstring ToString(unsigned long long number, bool withSeperators = false);

        DllExport static std::wstring ToString(Duco::TConductorType conductorType);

        DllExport static unsigned int ToInteger(const wchar_t& aChar);
        DllExport static unsigned int ToInteger(const std::wstring& aChar);
        DllExport static float ToFloat(const std::wstring& fieldValue);
        DllExport static std::wstring ToName(unsigned int number);

        DllExport static void AppendToString(std::wstring& str, unsigned int number);
        DllExport static std::wstring GetInitials(const std::wstring& str);
        static void AddError(std::wstring& existingErrorString, const wchar_t* newErrorString);

        DllExport static void SwapNumbers(const std::map<Duco::ObjectId, Duco::PealLengthInfo>& oldsIdsFirst, std::multimap<Duco::PealLengthInfo, Duco::ObjectId>& countsFirst);
        DllExport static std::list<std::set<Duco::ObjectId> >::iterator FindId(std::list<std::set<Duco::ObjectId> >& linkedTowerIds, const Duco::ObjectId& idToFind);

        DllExport static bool NoOfBellsMatch(unsigned int noOfBellsInMethodToMatch, unsigned int noOfBells);
        DllExport static unsigned int MaxNumberOfBells();

        static void ToSet(const std::map<unsigned int, Duco::PealRingerData>& ringerIds, std::set<Duco::ObjectId>& ids);

        template<class T, class K>
        static size_t NumberWithSameCount(const T& sortedIds, K count)
        {
            std::pair<typename T::const_iterator, typename T::const_iterator> rangeIts = sortedIds.equal_range(count);

            size_t number = 0;
            while (rangeIts.first != sortedIds.end() && rangeIts.second != sortedIds.end() && rangeIts.first != rangeIts.second)
            {
                ++number;
                ++rangeIts.first;
            }

            return number;
        }
    };
}

#endif //!__DUCOENGINEUTILS_H__
