#include "ChangeCollection.h"
#include "Change.h"
#include "DucoEngineUtils.h"
#include <cassert>

using namespace Duco;

ChangeCollection::ChangeCollection(const Duco::Change& firstChange)
:   snapStartChangesMissing(0), snapFinishChangesMissing(0), isTrue(true), endsWithRounds(false),
   firstFalseChangeNumber(-1), noOfFalseChanges(0), falseChange(NULL), lastChange(NULL)
{
    changes = new std::set<Duco::Change*,ChangeComparer>();
    startingChange = firstChange.Realloc();
}

ChangeCollection::ChangeCollection(const ChangeCollection& rhs)
:   snapStartChangesMissing(rhs.snapStartChangesMissing),
    snapFinishChangesMissing(rhs.snapFinishChangesMissing),
    isTrue(rhs.isTrue),
    endsWithRounds(rhs.endsWithRounds),
    firstFalseChangeNumber(rhs.firstFalseChangeNumber),
    noOfFalseChanges(rhs.noOfFalseChanges),
    falseChange(NULL), lastChange(NULL)
{
    changes = new std::set<Duco::Change*, ChangeComparer>();
    startingChange = new Duco::Change(*rhs.startingChange);
    this->operator=(rhs);
}

ChangeCollection::~ChangeCollection()
{
    delete falseChange;
    DeleteAllChanges();
    delete changes;
    delete lastChange;
    delete startingChange;
}

void
ChangeCollection::DeleteAllChanges()
{
    std::set<Duco::Change*, ChangeComparer>::iterator it = changes->begin();
    while (it != changes->end())
    {
        delete *it;
        ++it;
    }
    changes->clear();
}

bool
ChangeCollection::Add(const Duco::Change& newChange)
{
    Duco::Change* ownedChange = newChange.Realloc();

    if (lastChange == NULL)
    {
        lastChange = newChange.Realloc();
    }
    else
    {
        *lastChange = newChange;
    }

    if (changes->insert(ownedChange).second)
    {
        endsWithRounds = newChange.IsRounds();
        return true;
    }
    else
    {
        endsWithRounds = newChange.IsRounds();
        ++noOfFalseChanges;
        if (firstFalseChangeNumber == -1)
        {
            firstFalseChangeNumber = changes->size() + 1;
            delete falseChange;
            falseChange = ownedChange;
        }
        else
        {
            delete ownedChange;
        }
        isTrue = false;
    }
    return false;
}

void
ChangeCollection::Clear(const Duco::Change& firstChange)
{
    DeleteAllChanges();
    isTrue = true;
    firstFalseChangeNumber = -1;
    delete falseChange;
    falseChange = NULL;
    noOfFalseChanges = 0;
    if (lastChange != NULL)
        (*lastChange) = firstChange;
    (*startingChange) = firstChange;
    snapStartChangesMissing = 0;
    snapFinishChangesMissing = 0;
}

bool
ChangeCollection::operator==(const ChangeCollection& other) const
{
    if (changes->size() != other.changes->size())
        return false;

    std::set<Duco::Change*, ChangeComparer>::const_iterator it1 = changes->begin();
    std::set<Duco::Change*, ChangeComparer>::const_iterator it2 = other.changes->begin();
    while (it1 != changes->end() && it2 != other.changes->end())
    {
        if ((**it1) != (**it2))
            return false;

        ++it1;
        ++it2;
    }
    return snapStartChangesMissing == other.snapStartChangesMissing && snapFinishChangesMissing == other.snapFinishChangesMissing;
}

ChangeCollection&
ChangeCollection::operator=(const ChangeCollection& other)
{
    snapStartChangesMissing = other.snapStartChangesMissing;
    snapFinishChangesMissing = other.snapFinishChangesMissing;
    isTrue = other.isTrue;
    endsWithRounds = other.endsWithRounds;
    firstFalseChangeNumber = other.firstFalseChangeNumber;
    noOfFalseChanges = other.noOfFalseChanges;

    delete falseChange;
    falseChange = NULL;
    if (other.falseChange != NULL)
    {
        falseChange = new Duco::Change(*other.falseChange);
    }
    delete lastChange;
    if (other.lastChange != NULL)
    {
        lastChange = new Duco::Change(*other.lastChange);
    }
    else
    {
        lastChange = NULL;
    }
    delete startingChange;
    startingChange = new Duco::Change(*other.startingChange);

    DeleteAllChanges();
    std::set<Duco::Change*, ChangeComparer>::const_iterator it = other.changes->begin();
    while (it != other.changes->end())
    {
        Duco::Change* ownedChange = (*it)->Realloc();
        bool changeAdded = changes->insert(ownedChange).second;
        assert(changeAdded);
        ++it;
    }
    return *this;
}

bool
ChangeCollection::FalseChange(Duco::Change& falseChng) const
{
    falseChng.Reset();
    if (firstFalseChangeNumber == -1 || falseChange == NULL)
        return false;

    falseChng = (*falseChange);
    
    return true;
}

std::wstring
ChangeCollection::FalseDetails() const
{
    if (isTrue)
        return L"";

    std::wstring returnVal (L"False at change number ");
    returnVal += DucoEngineUtils::ToString(firstFalseChangeNumber);
    returnVal += L": ";
    returnVal += falseChange->Str(true, true);
    if (noOfFalseChanges > 1)
    {
        returnVal += KNewlineChar;
        returnVal += L"and ";
        returnVal += DucoEngineUtils::ToString(noOfFalseChanges);
        returnVal += L" other false changes";
    }
    return returnVal;
}

const Change&
ChangeCollection::EndChange() const
{
    if (lastChange == NULL)
        return *startingChange;

    return *lastChange;
}

size_t
ChangeCollection::NoOfChanges() const
{
    return changes->size() + noOfFalseChanges;
}

bool
ChangeCollection::EndsWithRounds() const
{
    return endsWithRounds;
}

const Change&
ChangeCollection::StartingChange() const
{
    return *startingChange;
}


bool
ChangeCollection::True() const
{
    return isTrue;
}

bool
ChangeCollection::FalseChangeNumber(size_t& falseChangeNo) const
{
    falseChangeNo = firstFalseChangeNumber;
    return !True();
}
