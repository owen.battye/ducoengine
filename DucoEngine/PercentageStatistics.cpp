#include "PercentageStatistics.h"
#include "Peal.h"

using namespace Duco;

void
RingerCount::AddPeal(const Duco::Peal& newPeal)
{
    numberOfChanges += newPeal.NoOfChanges();
    ++numberOfPeals;
}

void
RingerCount::SetTotals(size_t newTotalNumberOfChanges, size_t newTotalNumberOfPeals)
{
    totalNumberOfChanges = newTotalNumberOfChanges;
    totalNumberOfPeals = newTotalNumberOfPeals;

}

Duco::ObjectId
RingerCount::RingerId() const
{
    return ringerId;
}

size_t
RingerCount::NumberOfChanges() const
{
    return numberOfChanges;
}

size_t
RingerCount::NumberOfPeals() const
{
    return numberOfPeals;
}

double
RingerCount::PercentageOfChanges() const
{
    return (double)numberOfChanges / (double)totalNumberOfChanges;
}

double
RingerCount::PercentageOfPeals() const
{
    return (double)numberOfPeals / (double)totalNumberOfPeals;
}

PercentageStatistics::PercentageStatistics(const std::set<Duco::ObjectId>& ringerIds)
    : numberOfPeals(0), numberOfChanges(0)
{
    std::set<Duco::ObjectId>::const_iterator it = ringerIds.begin();
    while (it != ringerIds.end())
    {
        RingerCount nextRinger(*it);
        std::pair<Duco::ObjectId, Duco::RingerCount> newPair(*it, nextRinger);
        changesCountByRinger.insert(newPair);
        ++it;
    }
}

PercentageStatistics::~PercentageStatistics()
{

}

Duco::ObjectId
PercentageStatistics::LastPealId() const
{
    return lastPealId;
}

void
PercentageStatistics::AddPeal(const Duco::Peal& newPeal)
{
    numberOfPeals += 1;
    numberOfChanges += newPeal.NoOfChanges();
    if (!lastPealId.ValidId() || lastPealId < newPeal.Id())
    {
        lastPealId = newPeal.Id();
    }

    bool asStrapper(false);
    std::set<Duco::ObjectId> ringerIds;
    newPeal.RingerIds(ringerIds);
    std::set<Duco::ObjectId>::const_iterator ringerId = ringerIds.begin();
    while (ringerId != ringerIds.end())
    {
        std::map<Duco::ObjectId, Duco::RingerCount>::iterator ringerOfInterest = changesCountByRinger.find(*ringerId);
        if (ringerOfInterest != changesCountByRinger.end())
        {
            ringerOfInterest->second.AddPeal(newPeal);
        }
        ++ringerId;
    }
}

std::multiset<Duco::RingerCount>
PercentageStatistics::ChangeCounts() const
{
    std::multiset<Duco::RingerCount> counts;
    std::map<Duco::ObjectId, Duco::RingerCount>::const_iterator it = changesCountByRinger.begin();
    while (it != changesCountByRinger.end())
    {
        Duco::RingerCount nextRinger = it->second;
        nextRinger.SetTotals(numberOfChanges, numberOfPeals);
        counts.insert(nextRinger);
        ++it;
    }
    return counts;
}