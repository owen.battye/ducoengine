#include "AlphabetLetter.h"
#include "AlphabetObject.h"
#include <algorithm>

using namespace Duco;

AlphabetLetter::AlphabetLetter(wchar_t newLetter)
:   letter(newLetter)
{

}

AlphabetLetter::~AlphabetLetter()
{
    Reset();
}

namespace Duco
{
    bool
    cmp(const Duco::AlphabetObject* lhs, const Duco::AlphabetObject* rhs)
    {
        return lhs->operator<(*rhs);
    }
}

bool
AlphabetLetter::AddPeal(wchar_t newLetter, const ObjectId& pealId, const ObjectId& methodId, const Duco::PerformanceDate& pealDate, bool uniqueMethods)
{
    bool methodAlreadyExists (false);
    if (uniqueMethods)
    {
        std::vector<AlphabetObject*>::iterator it = pealInfo.begin();
        while (it != pealInfo.end() && !methodAlreadyExists)
        {
            if ((*it)->MethodId() == methodId)
            {
                if (pealDate < (*it)->Date())
                {
                    AlphabetObject object (newLetter, pealId, methodId, pealDate);
                    (**it) = object;
                }
                methodAlreadyExists = true;
            }
            ++it;
        }
    }
    if (letter == newLetter && !methodAlreadyExists)
    {
        AlphabetObject* object = new AlphabetObject(newLetter, pealId, methodId, pealDate);
        pealInfo.push_back(object);
        std::sort(pealInfo.begin(), pealInfo.end(), cmp);
        return true;
    }
    return false;
}

void
AlphabetLetter::Reset()
{
    std::vector<AlphabetObject*>::iterator it = pealInfo.begin();
    while (it != pealInfo.end())
    {
        delete *it;
        ++it;
    }
    pealInfo.clear();
}

const AlphabetObject&
AlphabetLetter::operator[](unsigned int position) const
{
    return *pealInfo[position];
}

bool
AlphabetLetter::RungDate(unsigned int circleNumber, Duco::PerformanceDate& completionDate, Duco::ObjectId& pealId) const
{
    if (pealInfo.size() < ((size_t)circleNumber+1))
    {
        completionDate.ResetToEarliest();
        pealId.ClearId();
        return false;
    }
    if (completionDate < pealInfo[circleNumber]->Date())
    {
        completionDate = pealInfo[circleNumber]->Date();
    }
    pealId = pealInfo[circleNumber]->PealId();

    return true;
}
