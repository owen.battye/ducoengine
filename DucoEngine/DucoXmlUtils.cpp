#include "DucoXmlUtils.h"
#include "DucoEngineUtils.h"
#include "ObjectId.h"
#include <xercesc\util\XMLString.hpp>
#include <xercesc\dom\DOMElement.hpp>

using namespace Duco;
XERCES_CPP_NAMESPACE_USE

XERCES_CPP_NAMESPACE_QUALIFIER DOMElement*
DucoXmlUtils::AddElement(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& parentElement,
            const std::wstring& elementName, const std::wstring& elementValue, bool addIfValueBlank)
{
    XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* newElement = NULL;
    if (addIfValueBlank || elementValue.length() > 0)
    {
        newElement = outputFile.createElement(elementName.c_str());
        parentElement.appendChild(newElement);
        if (elementValue.length() > 0)
        {
            XMLCh* newString = new XMLCh[elementValue.size() + 1];
            XMLString::copyString(newString, elementValue.c_str());
            XMLString::replaceWS(newString);
            DOMText* newText = outputFile.createTextNode(newString);
            newElement->appendChild((DOMNode*)newText);
            delete[] newString;
        }
    }
    return newElement;
}

XERCES_CPP_NAMESPACE_QUALIFIER DOMElement*
DucoXmlUtils::AddBoolElement(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile,
                                                          XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& parentElement,
                                                          const std::wstring& elementName, bool elementValue)
{
    if (elementValue)
    {
        return AddElement(outputFile, parentElement, elementName, L"true", true);
    }
    return AddElement(outputFile, parentElement, elementName, L"false", true);
}

XERCES_CPP_NAMESPACE_QUALIFIER DOMElement*
DucoXmlUtils::AddElement(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile,
                         XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& parentElement,
                         const std::wstring& elementName, size_t elementValue)
{
    std::wstring elementVal = DucoEngineUtils::ToString(elementValue);
    return AddElement(outputFile, parentElement, elementName, elementVal);
}

XERCES_CPP_NAMESPACE_QUALIFIER DOMElement*
DucoXmlUtils::AddElements(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile,
                          XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& parentElement,
                          const std::wstring& elementsName, const std::wstring& elementName,
                          const std::set<Duco::ObjectId>& ids)
{
    DOMElement* elements = AddElement(outputFile, parentElement, elementsName, L"", true);

    for (auto const& it : ids)
    {
        DOMElement* element = AddElement(outputFile, *elements, elementName, L"", true);
        AddObjectId(*element, it, true);
    }

    return elements;
}



void
DucoXmlUtils::AddAttribute(XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& parentElement,
                                                          const std::wstring& attributeName, const std::wstring& attributeValue,
                                                          bool addIfValueBlank)
{
    if (addIfValueBlank || attributeValue.length() > 0)
    {
        parentElement.setAttribute(attributeName.c_str(), attributeValue.c_str());
    }
}

void
DucoXmlUtils::AddBoolAttribute(XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& parentElement,
                           const std::wstring& elementName, bool attributeValue)
{
    if (attributeValue)
    {
        XMLCh* trueTag = XMLString::transcode("true");
        parentElement.setAttribute(elementName.c_str(), trueTag);
        XMLString::release(&trueTag);
    }
    else
    {
        XMLCh* falseTag = XMLString::transcode("false");
        parentElement.setAttribute(elementName.c_str(), falseTag);
        XMLString::release(&falseTag);
    }
}

void
DucoXmlUtils::AddAttribute(XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& parentElement,
                           const std::wstring& elementName, size_t attributeValue)
{
    std::wstring attrVal = DucoEngineUtils::ToString(attributeValue);
    AddAttribute(parentElement, elementName, attrVal);
}

void
DucoXmlUtils::AddObjectId(XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& parentElement,
                          const Duco::ObjectId& objectId,
                          bool addIfValueBlank)
{
    if (addIfValueBlank || objectId.ValidId())
    {
        AddAttribute(parentElement, L"DucoId", objectId.Str());
    }
}
