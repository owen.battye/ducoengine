#include "CountStringSortObject.h"

using namespace Duco;

CountStringSortObject::CountStringSortObject(Duco::PealLengthInfo newPealCount, const std::wstring& newObjectName)
: pealCount(newPealCount), objectName(newObjectName)
{
}

CountStringSortObject::CountStringSortObject(const CountStringSortObject& rhs)
: pealCount(rhs.pealCount), objectName(rhs.objectName)
{
}


CountStringSortObject::~CountStringSortObject()
{

}

CountStringSortObject&
CountStringSortObject::operator=(const CountStringSortObject& rhs)
{
    pealCount = rhs.pealCount;
    objectName = rhs.objectName;

    return *this;
}

bool 
CountStringSortObject::operator<(const CountStringSortObject& rhs) const
{
    if (pealCount > rhs.pealCount)
        return true;
    else if (pealCount < rhs.pealCount)
        return false;

    return objectName.compare(rhs.objectName) < 0;
}

bool 
CountStringSortObject::operator>(const CountStringSortObject& rhs) const
{
    if (pealCount < rhs.pealCount)
        return true;
    else if (pealCount > rhs.pealCount)
        return false;

    return objectName.compare(rhs.objectName) > 0;
}

bool
CountStringSortObject::operator==(const CountStringSortObject& rhs) const
{
    return pealCount == rhs.pealCount && objectName.compare(rhs.objectName) == 0;
}

bool
CountStringSortObject::operator!=(const CountStringSortObject& rhs) const
{
    return pealCount != rhs.pealCount || objectName.compare(rhs.objectName) != 0;
}
