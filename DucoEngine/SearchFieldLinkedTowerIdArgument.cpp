#include "SearchFieldLinkedTowerIdArgument.h"
#include "RingingDatabase.h"
#include "Tower.h"
#include "Peal.h"
#include "TowerDatabase.h"

using namespace Duco;

TSearchFieldLinkedTowerIdArgument::TSearchFieldLinkedTowerIdArgument(const Duco::ObjectId& newFieldValue, Duco::TSearchType newSearchType)
: TSearchFieldIdArgument(ELinkedTowerId, newFieldValue, newSearchType), parentTowerIds(NULL)
{
}

TSearchFieldLinkedTowerIdArgument::~TSearchFieldLinkedTowerIdArgument()
{
    delete parentTowerIds;
}

bool
TSearchFieldLinkedTowerIdArgument::Match(const Duco::Tower& object, const Duco::RingingDatabase& database) const
{
    if (object.Id() == fieldValue)
    {
        return true;
    }
    return MatchTowerId(object.Id(), database);
}

bool
TSearchFieldLinkedTowerIdArgument::Match(const Duco::Peal& object, const Duco::RingingDatabase& database) const
{
    if (object.TowerId() == fieldValue)
    {
        return true;
    }
    return MatchTowerId(object.TowerId(), database);
}

bool
TSearchFieldLinkedTowerIdArgument::MatchTowerId(const Duco::ObjectId& objectId, const Duco::RingingDatabase& database) const
{
    if (objectId == fieldValue)
    {
        return true;
    }
    if (parentTowerIds == NULL)
    {
        parentTowerIds = new std::list<std::set<Duco::ObjectId> >;
        database.TowersDatabase().GetAllLinkedIds(*parentTowerIds);
    }

    std::list<std::set<Duco::ObjectId> >::const_iterator directParentLink = parentTowerIds->begin();
    while (directParentLink != parentTowerIds->end())
    {
        if (directParentLink->find(fieldValue) != directParentLink->end() && directParentLink->find(objectId) != directParentLink->end())
            return true;

        ++directParentLink;
    }

    return false;
}
