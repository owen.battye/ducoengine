#pragma once
#ifndef __FELSTEADCOUNTY_H__
#define __FELSTEADCOUNTY_H__
#include <string>
#include <list>
#include <thread>
#include "DucoEngineCommon.h"
#include <xercesc/parsers/SAXParser.hpp>
#include <xercesc/sax/DocumentHandler.hpp>
#include <xercesc/sax/ErrorHandler.hpp>
#include <xercesc/sax/SAXParseException.hpp>

namespace Duco
{
    class RingingDatabase;
    class DoveDatabase;

    struct FelsteadCountyItem
    {
        std::wstring towerbaseId;
        std::wstring place;
        int pealCount;
        int invalidPealCount;
    };

    class FelsteadCounty: XERCES_CPP_NAMESPACE_QUALIFIER ErrorHandler, XERCES_CPP_NAMESPACE_QUALIFIER DocumentHandler
    {
    public:
        DllExport FelsteadCounty(Duco::RingingDatabase& database, const std::wstring& felstedCountyId, const std::wstring& countyName);
        DllExport ~FelsteadCounty();

        DllExport bool StartDownload();
        DllExport void Wait();
        DllExport void Process(const std::wstring& filename);

        DllExport size_t NumberOfTowers() const;

        DllExport bool ResolveTowers(const Duco::DoveDatabase& doveDb);

        // from DocumentHandler
        virtual void characters(const XMLCh* const chars, const XMLSize_t length);
        virtual void endDocument();
        virtual void endElement(const XMLCh* const name);
        virtual void ignorableWhitespace(const XMLCh* const chars, const XMLSize_t length);
        virtual void processingInstruction(const XMLCh* const target, const XMLCh* const data);
        virtual void resetDocument();
        virtual void setDocumentLocator(const XERCES_CPP_NAMESPACE_QUALIFIER Locator* const locator);
        virtual void startDocument();
        virtual void startElement(const XMLCh* const name, XERCES_CPP_NAMESPACE_QUALIFIER AttributeList& attrs);

        // from ErrorHandler
        virtual void warning(const XERCES_CPP_NAMESPACE_QUALIFIER SAXParseException& exc);
        virtual void error(const XERCES_CPP_NAMESPACE_QUALIFIER SAXParseException& exc);
        virtual void fatalError(const XERCES_CPP_NAMESPACE_QUALIFIER SAXParseException& exc);
        virtual void resetErrors();

    protected:
        DllExport void Download();

    private:
        Duco::RingingDatabase&          database;
        std::jthread*                   downloadThread;

        bool                            startedDiv;
        bool                            startedTable;
        bool                            startedCountyRow;
        bool                            startedCell;
        size_t                          cellCount;

        const std::wstring              countyTowerbaseId;
        const std::wstring              countyName;

        std::wstring                    currentTowerbaseId;
        std::wstring                    currentPlace;
        std::wstring                    currentPealCount;
        std::wstring                    currentInvalidPealCount;

        std::list<FelsteadCountyItem>   towers;
    };

}

#endif __FELSTEADCOUNTY_H__