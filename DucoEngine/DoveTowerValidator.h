#ifndef __DOVETOWERVALIDATOR_H__
#define __DOVETOWERVALIDATOR_H__

namespace Duco
{
	class DoveObject;

class DoveTowerValidator
{
public:
    virtual bool IncludeTower(const Duco::DoveObject& nextTower) = 0;
};

}

#endif //#ifndef __DOVETOWERVALIDATOR_H__
