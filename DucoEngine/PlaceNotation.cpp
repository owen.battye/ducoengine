#include "PlaceNotation.h"

#include "DucoEngineUtils.h"
#include "Change.h"
#include "ChangeCollection.h"
#include "Lead.h"
#include "MethodDatabase.h"
#include "Method.h"
#include "PlaceNotationProcessor.h"
#include "PlaceNotationObject.h"
#include "LeadEndCollection.h"

#include <algorithm>

using namespace std;
using namespace Duco;

PlaceNotation::PlaceNotation(Duco::Method& newMethod, size_t newCallingId)
    :   currentMethod(newMethod), callingId(newCallingId), symetric(false), notationUpdated(false)
{
    PlaceNotationProcessor processor(newMethod);
    notationUpdated = processor.ProcessNotation(*this);
    SetLeadOrder();
}

PlaceNotation::PlaceNotation(Duco::PlaceNotation& other)
:   currentMethod(other.currentMethod), callingId(other.callingId), symetric(other.symetric),
    notationUpdated(other.notationUpdated), leadOrder(other.leadOrder)
{
    std::list<PlaceNotationObject*>::const_iterator it = other.notations.begin();
    while (it != other.notations.end())
    {
        notations.push_back((*it)->Realloc());
        ++it;
    }
}

PlaceNotation::~PlaceNotation()
{
    DeleteAllNotations();
}

const std::wstring&
PlaceNotation::Notation() const
{
    return currentMethod.PlaceNotation();
}

std::wstring
PlaceNotation::FullNotation(bool includeSymetricIndicator, bool includeLeadEnd) const
{
    std::wstring fullNotation;
    if (includeSymetricIndicator && symetric)
    {
        fullNotation.append(L"&");
    }
    
    std::list<PlaceNotationObject*>::const_iterator it = notations.begin();
    bool previousNotationRequiredSeperator (false);

    size_t count (0);
    while (it != notations.end())
    {
        const PlaceNotationObject* nextNotationObject = *it;

        ++it;
        ++count;
        bool dontAddFinalSeperator (false);
        if (symetric)
        {
            if (count > (notations.size()/2))
            {
                it = notations.end();
                nextNotationObject = *notations.rbegin();
                dontAddFinalSeperator = true;
            }
        }
        if (previousNotationRequiredSeperator &&
            nextNotationObject->RequiresSeperator() &&
            !dontAddFinalSeperator)
        {
            fullNotation += L".";
        }
        if (it == notations.end() && symetric && includeLeadEnd)
        {
            fullNotation += L"-";
        }
        if (it != notations.end() || includeLeadEnd)
        {
            fullNotation += nextNotationObject->Str();
        }
        previousNotationRequiredSeperator = nextNotationObject->RequiresSeperator();
    }
    
    return fullNotation;
}

std::wstring
PlaceNotation::PlainLeadEndNotation() const
{
    std::list<PlaceNotationObject*>::const_reverse_iterator it = notations.rbegin();
    
    if (it != notations.rend())
    {
        const PlaceNotationObject* nextNotationObject = *it;
        std::wstring temp = nextNotationObject->Str();
        return temp;
    }
    return L"";
}

unsigned int
PlaceNotation::BestStartingLead(MethodDatabase& methodDb) const
{
    unsigned int currentPrintFromBell = currentMethod.PrintFromBell();
    if (currentPrintFromBell <= 0 || currentPrintFromBell > currentMethod.Order())
    {
        unsigned int bestStartingLead (notations.back()->BestStartingLead());
        if (bestStartingLead != -1)
        {
            methodDb.SetStartingLead(currentMethod.Id(),bestStartingLead);
            return bestStartingLead;
        }
        else
        {
            methodDb.SetStartingLead(currentMethod.Id(),currentMethod.Order());
            return currentMethod.Order();
        }
    }
    return currentMethod.PrintFromBell();
}

void
PlaceNotation::DeleteAllNotations()
{
    std::list<PlaceNotationObject*>::iterator it = notations.begin();
    while (it != notations.end())
    {
        delete *it;
        ++it;
    }
    notations.clear();
}

void
PlaceNotation::ReverseAndDuplicate()
{
    symetric = true;
    std::list<PlaceNotationObject*>::reverse_iterator rit = notations.rbegin();
    for ( ++rit ; rit != notations.rend(); ++rit )
    {
        AddNotation((*rit)->Realloc());
    }
}

void
PlaceNotation::AddNotation(PlaceNotationObject* newObject)
{
    notations.push_back(newObject);
}

bool
PlaceNotation::SetLeadHeadAndEnd()
{
    if (notations.size() == 0)
    {
        return false;
    }
    std::list<Duco::PlaceNotationObject*>::reverse_iterator it = notations.rbegin();
    if (it != notations.rend())
    {
        (*it)->SetLeadEnd();
        ++it;
        if (it != notations.rend())
        {
            (*it)->SetLeadHead();
            return true;
        }
    }
    return false;
}

Duco::Lead*
PlaceNotation::CreateLead(size_t snapStartPos) const
{
    Change startWithRounds(currentMethod.Order());
    startWithRounds.SetChangeType(ELeadEnd);
    return CreateLead(startWithRounds, snapStartPos);
}

bool
PlaceNotation::CreateStartingChange(Duco::Change& fromChange, const size_t snapStartPos, const TLeadType leadType) const
{
    fromChange.Reset();

    if (snapStartPos <= 0 || snapStartPos > notations.size())
    {
        return false;
    }

    std::list<PlaceNotationObject*>::const_reverse_iterator it = notations.rbegin();
    if (it == notations.rend())
    {
        return false;
    }

    size_t noOfRowsProcessed(0);
    switch (leadType)
    {
        case EBobLead:
            {
                std::wstring bobLeadEndNotation;
                if (currentMethod.BobPlaceNotation(bobLeadEndNotation, callingId))
                {
                    PlaceNotationObject* bleObject = PlaceNotationProcessor::CreateNotationObject(bobLeadEndNotation, ELeadEnd);
                    fromChange = bleObject->ProcessRow(fromChange);
                    delete bleObject;
                    ++noOfRowsProcessed;
                }
            }
            ++it;
            break;
        case ESingleLead:
            {
                std::wstring singleLeadEndNotation;
                if (currentMethod.SinglePlaceNotation(singleLeadEndNotation, callingId))
                {
                    PlaceNotationObject* sleObject = PlaceNotationProcessor::CreateNotationObject(singleLeadEndNotation, ELeadEnd);
                    fromChange = sleObject->ProcessRow(fromChange);
                    delete sleObject;
                    ++noOfRowsProcessed;
                }
            }
            ++it;
            break;

        default:
            break;
    }

    while (it != notations.rend() && noOfRowsProcessed < snapStartPos)
    {
        fromChange = (*it)->ProcessRow(fromChange);
        ++noOfRowsProcessed;
        ++it;
    }

    return noOfRowsProcessed == snapStartPos;
}

Duco::Lead*
PlaceNotation::CreateLead(const Change& fromChange, size_t snapStartPos) const
{
    Lead* newLead = new Lead(currentMethod);
    std::list<PlaceNotationObject*>::const_iterator it = notations.begin();
    Change nextChange(fromChange);
    newLead->changes.push_back(nextChange.Realloc());
    while (it != notations.end())
    {
        if (nextChange.IsLeadHead() && currentMethod.ContainsBobOrSingleNotation())
        {
            std::wstring bobLeadEndNotation;
            if (currentMethod.BobPlaceNotation(bobLeadEndNotation, callingId))
            {
                PlaceNotationObject* leObject = PlaceNotationProcessor::CreateNotationObject(bobLeadEndNotation, ELeadEnd);
                newLead->SetLeadEndChange(EBobLead, leObject->ProcessRow(nextChange));
                delete leObject;
            }
            std::wstring singleLeadEndNotation;
            if (currentMethod.SinglePlaceNotation(singleLeadEndNotation, callingId))
            {
                PlaceNotationObject* leObject = PlaceNotationProcessor::CreateNotationObject(singleLeadEndNotation, ELeadEnd);
                newLead->SetLeadEndChange(ESingleLead, leObject->ProcessRow(nextChange));
                delete leObject;
            }
        }
        nextChange = (*it)->ProcessRow(nextChange);
        newLead->changes.push_back(nextChange.Realloc());

        if (snapStartPos == (newLead->changes.size()-1) && fromChange.IsRounds())
        {   // When snap start reset at the right point and go back to rounds.
            snapStartPos = 0;
            newLead->Clear();
            nextChange.Reset();
            newLead->changes.push_back(nextChange.Realloc());
        }
        ++it;
    }
    return newLead;
}

Duco::Lead*
PlaceNotation::CreateNextLead(Duco::Lead*& fromLead) const
{
    Duco::Lead* nextLead = NULL;
    Change leadEndChange(currentMethod.Order());
    if (fromLead->LeadEnd(leadEndChange))
    {
        nextLead = CreateLead(leadEndChange, -1);
    }
    delete fromLead;
    fromLead = NULL;
    return nextLead;
}

bool
PlaceNotation::CreateNextLeadEndChange(Duco::TLeadType type, Change& endChange) const
{
    Duco::Lead* lead = CreateLead(endChange, -1);
    bool success = false;
    if (lead->SetLeadEndType(type))
    {
        success = lead->LeadEnd(endChange);
    }
    delete lead;
    return success;
}

void
PlaceNotation::SetLeadOrder()
{
    leadOrder.Reset();
    bool useCallingOrder (CallsAffectHome());
    if (useCallingOrder)
    {
        leadOrder = LeadOrderByBob();
    }
    if (!useCallingOrder || leadOrder.ValidLeadOrder(Order()))
    {
        leadOrder = LeadOrderByPlain();
    }
    leadOrder.SetPositions();
}

Duco::LeadOrder
PlaceNotation::LeadOrderByBob() const
{
    Duco::LeadOrder returnLeadOrder;
    std::set<unsigned int> usedBells;
    unsigned int position (currentMethod.PrintFromBell());

    Duco::Lead* lead = CreateLead(0);
    size_t maxIterations (Order() * 2);
    while (returnLeadOrder.Size() < (Order() - 1) && maxIterations > 0)
    {
        if (lead->CallingPosition(EBobLead, position))
        {
            if (usedBells.find(position) == usedBells.end())
            {
                returnLeadOrder.Add(position);
                usedBells.insert(position);
            }
            else if (lead->CallingPosition(EPlainLead, position))
            {
                if (usedBells.find(position) == usedBells.end())
                {
                    returnLeadOrder.Add(position);
                    usedBells.insert(position);
                }
            }
        }
        --maxIterations;
    }
    delete lead;
    return returnLeadOrder;
}

Duco::LeadOrder
PlaceNotation::LeadOrderByPlain() const
{
    Duco::LeadOrder returnLeadOrder;

    Duco::Lead* lead = CreateLead(0);
    unsigned int position (currentMethod.PrintFromBell());
    if (position < 1 || position > Order())
        position = Order();
    unsigned int startPosition (position);
    if (lead->CallingPosition(EPlainLead, position))
    {
        while (startPosition != position)
        {
            returnLeadOrder.Add(position);
            lead->CallingPosition(EPlainLead, position);
        }
    }
    returnLeadOrder.Add(position);
    delete lead;
    return returnLeadOrder;
}

const ObjectId&
PlaceNotation::MethodId() const
{
    return currentMethod.Id();
}

unsigned int
PlaceNotation::Order() const
{
    return currentMethod.Order();
}

bool
PlaceNotation::AdvanceToRounds(Duco::ChangeCollection& changes, Duco::LeadEndCollection& leadEnds) const
{
    bool alreadyFalse (!changes.True());

    unsigned int noOfLeads (Order());
    while (noOfLeads > 0 && !changes.EndsWithRounds() && (alreadyFalse || changes.True()))
    {
        Duco::Lead* nextLead = CreateLead(changes.EndChange(), -1);
        leadEnds.Add(*nextLead, false);
        nextLead->Changes(changes, true, true);
        delete nextLead;
        --noOfLeads;
    }
    return changes.EndsWithRounds();
}

bool
PlaceNotation::CallsAffectHome() const
{
    Duco::Lead* sampleLead = CreateLead(0);
    bool homeAffected (false);
    Change leadEnd(Order());
    if (sampleLead->LeadEnd(leadEnd))
    {
		unsigned int bellAtHome (-1);
        if (leadEnd.Bell(Order(), bellAtHome))
        {
            if (sampleLead->LeadEnd(EBobLead, leadEnd))
            {
                unsigned int bellAtHomeInBobbedLead (-1);
                if (leadEnd.Bell(Order(), bellAtHomeInBobbedLead))
                {
                    homeAffected = bellAtHomeInBobbedLead != bellAtHome;
                }
            }
        }
    }

    delete sampleLead;

    return homeAffected;
}

bool
PlaceNotation::HasNotationForCallingType(TLeadType type) const
{
    switch (type)
    {
    case TLeadType::EBobLead:
        return currentMethod.ContainsBobNotation();

    case TLeadType::ESingleLead:
        return currentMethod.ContainsSingleNotation();

    default:
    case TLeadType::EPlainLead:
        break;
    }
    return true;
}

size_t
PlaceNotation::ChangesPerLead() const
{
    return notations.size();
}

const Duco::LeadOrder&
PlaceNotation::LeadOrder() const
{
    return leadOrder;
}

Duco::LeadOrder&
PlaceNotation::LeadOrder()
{
    return leadOrder;
}