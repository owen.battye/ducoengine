#ifndef __PLACENOTATIONCOMMON_H__
#define __PLACENOTATIONCOMMON_H__

namespace Duco
{
    const wchar_t KSymetricNotationSymbolChar = '&';
    const wchar_t KCrossSymbolChar = 'X';
    const wchar_t KTerminationSymbolChar = '.';
    const wchar_t KLeadEndSymbolChar = '-';
    const std::wstring KSymetricNotationSymbolStr = L"&";
    const std::wstring KCrossSymbolStr = L"X";
    const std::wstring KTerminationSymbolStr = L".";
    const std::wstring KLeadEndSymbolStr = L"-";
}

#endif //!__PLACENOTATIONCOMMON_H__
