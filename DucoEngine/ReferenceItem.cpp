#include "ReferenceItem.h"
#include "Peal.h"

using namespace Duco;

ReferenceItem::ReferenceItem(const Duco::Peal& newPeal)
    : towerId(newPeal.TowerId()), noOfPeals(0), noOfPealsWithBellboardId(0), noOfPealsWithRingingWorldPage(0)
{
    AddPerformance(newPeal);
}

bool
ReferenceItem::AddPerformance(const Duco::Peal& newPeal)
{
    if (newPeal.TowerId() != towerId)
        return false;

    ++noOfPeals;

    if (newPeal.ValidBellBoardId())
    {
        ++noOfPealsWithBellboardId;
    }
    if (newPeal.ValidRingingWorldLink())
    {
        ++noOfPealsWithRingingWorldPage;
    }

    return true;
}

float
ReferenceItem::PercentWithBellboardReference() const
{
    if (noOfPeals == 0 || noOfPealsWithBellboardId == 0)
        return 0.0f;

    return (((float)noOfPealsWithBellboardId) / ((float)noOfPeals)) * 100.0f;
}

float
ReferenceItem::PercentWithRingingWorldReference() const
{
    if (noOfPeals == 0 || noOfPealsWithRingingWorldPage == 0)
        return 0.0f;

    return (((float)noOfPealsWithRingingWorldPage) / ((float)noOfPeals)) * 100.0f;
}

bool ReferenceItem::Valid() const
{
    return noOfPeals > 0 && noOfPealsWithRingingWorldPage <= noOfPeals && noOfPealsWithBellboardId <= noOfPeals;
}
