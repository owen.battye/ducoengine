#include "Method.h"

#include "DatabaseReader.h"
#include "DatabaseSettings.h"
#include "DatabaseWriter.h"
#include "DucoEngineUtils.h"
#include "DucoXmlUtils.h"
#include "RingingDatabase.h"
#include "MethodDatabase.h"
#include "PlaceNotation.h"
#include "ExternalMethod.h"

#include <sstream>

using namespace std;
using namespace Duco;
XERCES_CPP_NAMESPACE_USE

Method::Method(const Duco::ObjectId& newId, unsigned int newNoOfBells, const std::wstring& newName, const std::wstring& newType, const std::wstring& newNotation, bool dualOrder)
: RingingObject(newId), noOfBells(newNoOfBells), dualNoOfBells(dualOrder), name(newName),
    type(newType), placeNotation(newNotation), noOfLeads(-1),
    printFromBell(-1), pealbaseLink(L""), spliced(false)
{
    SetName(newName);
}

Method::Method(const Method& other)
: RingingObject(other), noOfBells(other.noOfBells), dualNoOfBells(other.dualNoOfBells), name(other.name), type(other.type),
  placeNotation(other.placeNotation), bobLEPlaceNotations(other.bobLEPlaceNotations), singleLEPlaceNotations(other.singleLEPlaceNotations), 
  noOfLeads(other.noOfLeads), printFromBell(other.printFromBell),
  pealbaseLink(other.pealbaseLink), spliced(other.spliced)
{
}

Method::Method(const Duco::ExternalMethod& other)
    : RingingObject(), noOfBells(other.Stage()), name(other.Name()), type(other.Type()),
    dualNoOfBells(false), placeNotation(other.Notation()), printFromBell(-1), noOfLeads(-1),
    pealbaseLink(L""), spliced(false)
{
}

Method::Method(const Duco::ObjectId& newId, const Method& other)
: RingingObject(newId), noOfBells(other.noOfBells), name(other.name), type(other.type),
  placeNotation(other.placeNotation), bobLEPlaceNotations(other.bobLEPlaceNotations),
  singleLEPlaceNotations(other.singleLEPlaceNotations),
  dualNoOfBells(other.dualNoOfBells), noOfLeads(other.noOfLeads), printFromBell(other.printFromBell),
  pealbaseLink(other.pealbaseLink), spliced(other.spliced)
{
}


Method::Method(DatabaseReader& reader, unsigned int databaseVersion)
: RingingObject(), noOfBells(0), name(L""), type(L""), placeNotation(L""),
    noOfLeads(-1), dualNoOfBells(false), printFromBell(-1), spliced(false),
    pealbaseLink(L"")
{
    Method::Internalise(reader, databaseVersion);
}

Method::Method()
:   RingingObject(), noOfBells(0), name(L""), type(L""),
    placeNotation(L""), dualNoOfBells(false), printFromBell(-1), noOfLeads(-1),
    pealbaseLink(L""), spliced(false)
{
}

Method::~Method()
{
    bobLEPlaceNotations.clear();
    singleLEPlaceNotations.clear();
}

Duco::TObjectType 
Method::ObjectType() const
{
    return TObjectType::EMethod;
}

int 
Method::NoOfLeads() const
{
    if (noOfLeads <= 0 || noOfLeads > int(noOfBells))
        return int(noOfBells);

    return noOfLeads;
}

int 
Method::HalfWay() const
{
    float pastHalf = float(NoOfLeads()) / float(2);
    pastHalf += 0.5;

    return int(pastHalf);
}

bool
Method::DrawTreble(const Duco::DatabaseSettings& settings) const
{
    if (noOfLeads == -1)
        return false;
    else if (!settings.ShowTreble())
        return false;
    else if (noOfLeads < int(noOfBells))
        return true;

    return false;
}

bool
Method::operator==(const RingingObject& other) const
{
    if (other.ObjectType() != ObjectType())
        return false;

    const Duco::Method& otherMethod = static_cast<const Duco::Method&>(other);

    if (id != otherMethod.id ||
        noOfBells != otherMethod.noOfBells || 
        dualNoOfBells != otherMethod.dualNoOfBells ||
        name.compare(otherMethod.name) != 0 ||
        type.compare(otherMethod.type) != 0 ||
        noOfLeads != otherMethod.noOfLeads ||
        printFromBell != otherMethod.printFromBell ||
        placeNotation.compare(otherMethod.placeNotation) != 0 ||
        bobLEPlaceNotations != otherMethod.bobLEPlaceNotations ||
        singleLEPlaceNotations != otherMethod.singleLEPlaceNotations ||
        pealbaseLink.compare(otherMethod.pealbaseLink) != 0)
    {
        return false;
    }

    return true;
}

bool 
Method::operator!=(const RingingObject& other) const
{
    if (other.ObjectType() != ObjectType())
        return true;

    const Duco::Method& otherMethod = static_cast<const Duco::Method&>(other);

    if (id != otherMethod.id)
        return true;

    return false;
}

Method&
Method::operator=(const Duco::Method& other)
{
    id = other.id;
    noOfBells = other.noOfBells;
    name = other.name;
    type = other.type;
    placeNotation = other.placeNotation;
    bobLEPlaceNotations = other.bobLEPlaceNotations;
    singleLEPlaceNotations = other.singleLEPlaceNotations;
    noOfLeads = other.noOfLeads;
    dualNoOfBells = other.dualNoOfBells;
    printFromBell = other.printFromBell;
    errorCode = other.errorCode;
    spliced = other.spliced;
    pealbaseLink = other.pealbaseLink;

    return *this;
}

std::wostream& 
Method::Print(std::wostream& stream, const RingingDatabase& database) const
{
    stream << name;
    if (type.length() > 0)
    {
        stream << " " << type;
    }
    stream << " " << OrderName(database.MethodsDatabase());
    return stream;
}

DatabaseWriter& 
Method::Externalise(DatabaseWriter& writer) const
{
    writer.WriteInt(id.Id());
    writer.WriteInt(noOfBells);
    writer.WriteString(name);
    writer.WriteString(type);
    writer.WriteString(placeNotation);
    writer.WriteInt(bobLEPlaceNotations.size());
    for (auto const& [key, value] : bobLEPlaceNotations)
    {
        writer.WriteInt(key);
        writer.WriteString(value);
    }
    writer.WriteInt(singleLEPlaceNotations.size());
    for (auto const& [key, value] : singleLEPlaceNotations)
    {
        writer.WriteInt(key);
        writer.WriteString(value);
    }
    writer.WriteInt(noOfLeads);
    writer.WriteBool(dualNoOfBells);
    writer.WriteString(L""); // reserved for notes
    writer.WriteInt(printFromBell);
    writer.WriteString(pealbaseLink);

    return writer;
}

DatabaseReader& 
Method::Internalise(DatabaseReader& reader, unsigned int databaseVersion)
{
    id = reader.ReadUInt();
    noOfBells = reader.ReadUInt();
    reader.ReadString(name);
    spliced = DucoEngineUtils::MethodNameContainsSpliced(name);
    reader.ReadString(type);
    reader.ReadString(placeNotation);
    std::wstring bobLEPlaceNotation;
    std::wstring singleLEPlaceNotation;
    if (databaseVersion >= 14 && databaseVersion <= 26)
    {
        reader.ReadString(bobLEPlaceNotation);
        if (bobLEPlaceNotation.length() > 0)
        {
            std::pair<size_t, std::wstring> newPair(0, bobLEPlaceNotation);
            bobLEPlaceNotations.insert(newPair);
        }
        reader.ReadString(singleLEPlaceNotation);
        if (singleLEPlaceNotation.length() > 0)
        {
            std::pair<size_t, std::wstring> newPair(0, singleLEPlaceNotation);
            singleLEPlaceNotations.insert(newPair);
        }
    }
    else if (databaseVersion >= 27 && databaseVersion <= 41)
    {
        unsigned int noOfCallings = reader.ReadUInt();
        unsigned int count = 0;
        while (noOfCallings > 0)
        {
            reader.ReadString(bobLEPlaceNotation);
            if (bobLEPlaceNotation.length() > 0)
            {
                std::pair<size_t, std::wstring> newObject(count, bobLEPlaceNotation);
                bobLEPlaceNotations.insert(newObject);
            }
            noOfCallings--;
            ++count;
        }
        noOfCallings = reader.ReadUInt();
        count = 0;
        while (noOfCallings > 0)
        {
            reader.ReadString(singleLEPlaceNotation);
            if (singleLEPlaceNotation.length() > 0)
            {
                std::pair<size_t, std::wstring> newObject(count, singleLEPlaceNotation);
                singleLEPlaceNotations.insert(newObject);
            }
            noOfCallings--;
            ++count;
        }
    }
    else if (databaseVersion > 41)
    {
        unsigned int noOfCallings = reader.ReadUInt();
        while (noOfCallings > 0)
        {
            unsigned int callingNumber = reader.ReadInt();
            reader.ReadString(bobLEPlaceNotation);
            std::pair<size_t, std::wstring> newObject(callingNumber, bobLEPlaceNotation);
            bobLEPlaceNotations.insert(newObject);
            --noOfCallings;
        }
        noOfCallings = reader.ReadUInt();
        while (noOfCallings > 0)
        {
            unsigned int callingNumber = reader.ReadInt();
            reader.ReadString(singleLEPlaceNotation);
            std::pair<size_t, std::wstring> newObject(callingNumber, singleLEPlaceNotation);
            singleLEPlaceNotations.insert(newObject);
            --noOfCallings;
        }
    }

    noOfLeads = reader.ReadInt();
    if (databaseVersion >= 3)
    {
        dualNoOfBells = reader.ReadBool();
        std::wstring notes;
        reader.ReadString(notes);
    }
    if (databaseVersion >= 4)
    {
        printFromBell = reader.ReadUInt();
    }
    if (databaseVersion >= 26)
    {
        reader.ReadString(pealbaseLink);
        if (databaseVersion <= 42)
        {
            std::wstring disardOldPealsCoUkLink;
            reader.ReadString(disardOldPealsCoUkLink);
        }
    }
    return reader;
}

std::wofstream&
Method::ExportToRtf(std::wofstream& outputFile, const RingingDatabase& database) const
{
    outputFile << FullName(database);
    return outputFile;
}

std::wstring
Method::ShortName(const RingingDatabase& database) const
{
    return ShortName(database.MethodsDatabase());
}

std::wstring
Method::ShortName(const MethodDatabase& database) const
{
    std::wstring shortNameString (name);
    if (type.length() > 0)
    {
        if (shortNameString.length() > 0)
            shortNameString.append(L" ");
        shortNameString.append(DucoEngineUtils::GetInitials(type));
    }

    std::wstring bellsName;
    if (database.FindOrderName(noOfBells, bellsName))
    {
        if (shortNameString.length() > 0)
            shortNameString.append(L" ");
        shortNameString.append(DucoEngineUtils::GetInitials(bellsName));
    }

    return shortNameString;
}

std::wstring
Method::FullName(const RingingDatabase& database) const
{
	return FullName(database.MethodsDatabase());
}

std::wstring
Method::FullName(const MethodDatabase& database) const
{
    std::wstring fullNameString (name);
    if (type.length() > 0)
    {
        if (fullNameString.length() > 0)
            fullNameString.append(L" ");
        fullNameString.append(type);
    }

    std::wstring bellsName;
    if (dualNoOfBells && database.FindOrderName(noOfBells-1, bellsName))
    {
        if (fullNameString.length() > 0)
            fullNameString.append(L" ");
        fullNameString.append(bellsName);
        fullNameString.append(L" and");
    }
    if (database.FindOrderName(noOfBells, bellsName))
    {
        if (fullNameString.length() > 0)
            fullNameString.append(L" ");
        fullNameString.append(bellsName);
    }

    return fullNameString;
}

std::wstring
Method::FullNameForSort(bool unusedSetting) const
{
    std::wstring fullNameString;
    bool methodNameAlreadyAdded (false);
    try
    {
        if (name.length() > 0 && spliced)
        {
            fullNameString = DucoEngineUtils::PadStringWithZeros(name);
            methodNameAlreadyAdded = true;
        }
    }
    catch (exception*)
    {
    }
    if (!methodNameAlreadyAdded)
    {
        fullNameString.append(name);
    }
    if (type.length() > 0)
    {
        fullNameString.append(L",");
        fullNameString.append(type);
    }

    fullNameString.append(L",");
    if (noOfBells < 10)
        fullNameString.append(L"0");

    wostringstream strStream;
    if (dualNoOfBells)
    {
        strStream << (noOfBells-1);
        strStream << ".";
    }
    strStream << noOfBells;
    fullNameString.append(strStream.str());

    std::wstring lowercaseFullNameString;
    DucoEngineUtils::ToLowerCase(fullNameString, lowercaseFullNameString);

    if (placeNotation.length() > 0)
    {
        lowercaseFullNameString.append(placeNotation);
    }

    return lowercaseFullNameString;
}

std::wstring
Method::NameForWeb(const RingingDatabase& database) const
{
	return NameForWeb(database.MethodsDatabase());
}

std::wstring
Method::NameForWeb(const MethodDatabase& database) const
{
    std::wstring fullNameString (name);
    if (type.length() > 0)
    {
        fullNameString.append(L"+");
        fullNameString.append(type);
    }
    std::wstring bellsName;
    if (database.FindOrderName(noOfBells, bellsName))
    {
        fullNameString.append(L"+");
        fullNameString.append(bellsName);
    }

    return fullNameString;
}

bool
Method::Valid(const RingingDatabase& ringingDb, bool warningFatal, bool clearBeforeStart) const
{
    return Valid(ringingDb.MethodsDatabase(), ringingDb.Settings(), warningFatal, clearBeforeStart);
}

bool
Method::Valid(const MethodDatabase& methodDb, const Duco::DatabaseSettings& settings, bool warningFatal, bool clearBeforeStart) const
{
    if (clearBeforeStart)
    {
        errorCode.reset();
    }
    bool returnVal (true);
    if (!methodDb.ContainsOrderName(noOfBells))
    {
        returnVal = false;
        errorCode.set(EMethod_OrderNameMissing, 1);
    }
    if (name.length() <= 0)
    {
        if (warningFatal)
            returnVal = false;
        errorCode.set(EMethodWarning_NameMissing, 1);
    }
    if (type.length() <= 0 && !Spliced())
    {
        if (!DucoEngineUtils::FindSubstring(L"grandsire", name) &&
            !DucoEngineUtils::FindSubstring(L"stedman", name) &&
            !DucoEngineUtils::FindSubstring(L"erin", name))
        {
            if (warningFatal)
                returnVal = false;
            errorCode.set(EMethodWarning_TypeMissing, 1);
        }
    }
    if (placeNotation.length() <= 0)
    {
        if (!Spliced())
        {
            errorCode.set(EMethodWarning_MissingNotation, 1);
            if (warningFatal)
                returnVal = false;
        }
    }
    else
    {
        if (Spliced())
        {
            if (warningFatal)
                returnVal = false;
            errorCode.set(EMethodWarning_SplicedNotation, 1);
        }
        if (!ValidNotation(false, warningFatal, settings, false))
        {
            returnVal = false;
        }
    }

    return returnVal;
}

std::wstring
Method::ErrorString(const Duco::DatabaseSettings& settings, bool showWarnings) const
{
    std::wstring returnVal (L"");
    if (errorCode.test(EMethod_OrderNameMissing))
    {
        DucoEngineUtils::AddError(returnVal, L"Order name missing");
    }
    if (showWarnings && errorCode.test(EMethodWarning_NameMissing))
    {
        DucoEngineUtils::AddError(returnVal, L"Method name missing");
    }
    if (showWarnings && errorCode.test(EMethodWarning_TypeMissing))
    {
        DucoEngineUtils::AddError(returnVal, L"Method type missing");
    }
    if (errorCode.test(EMethod_InvalidNotation))
    {
        DucoEngineUtils::AddError(returnVal, L"Place notation invalid");
    }
    if (showWarnings && errorCode.test(EMethodWarning_MissingBobPlaceNotation))
    {
        DucoEngineUtils::AddError(returnVal, L"Missing bob place notation");
    }
    if (showWarnings && errorCode.test(EMethodWarning_MissingSinglePlaceNotation))
    {
        DucoEngineUtils::AddError(returnVal, L"Missing single place notation");
    }
    if (showWarnings && errorCode.test(EMethodWarning_MissingNotation))
    {
        DucoEngineUtils::AddError(returnVal, L"Place notation missing");
    }
    if (showWarnings && errorCode.test(EMethodWarning_SplicedNotation))
    {
        DucoEngineUtils::AddError(returnVal, L"Spliced with place notation");
    }
    if (errorCode.test(EMethod_DuplicateMethod))
    {
        DucoEngineUtils::AddError(returnVal, L"Method with duplicate notation or name");
    }

    return returnVal;
}

bool
Method::ValidNotation(bool forProve, bool enableWarnings, const Duco::DatabaseSettings& settings, bool clearBeforeStart) const
{
    if (clearBeforeStart)
    {
        ClearErrors();
    }
    bool valid = true;
    std::wstring::size_type firstPos = placeNotation.find('-');
    errorCode.reset(EMethod_InvalidNotation);
    errorCode.reset(EMethodWarning_MissingBobPlaceNotation);
    errorCode.reset(EMethodWarning_MissingSinglePlaceNotation);
    if (firstPos != std::wstring::npos)
    {
        size_t lastPos = placeNotation.rfind('-');
        if (lastPos != firstPos)
        {
            errorCode.set(EMethod_InvalidNotation, 1);
            valid = false; // more than one '-'
        }
    }
    if (bobLEPlaceNotations.size() <= 0 && (forProve || (enableWarnings && !settings.ValidationCheckDisabled(EMethodWarning_MissingBobPlaceNotation, TObjectType::EMethod))))
    {
        errorCode.set(EMethodWarning_MissingBobPlaceNotation, 1);
        valid = false;
    }
    if (singleLEPlaceNotations.size() <= 0 && (forProve || (enableWarnings && !settings.ValidationCheckDisabled(EMethodWarning_MissingSinglePlaceNotation, TObjectType::EMethod))))
    {
        errorCode.set(EMethodWarning_MissingSinglePlaceNotation, 1);
        valid = false;
    }

    return valid;
}

std::wstring
Method::OrderName(const RingingDatabase& database, bool giveDualOrderName) const
{
	return OrderName(database.MethodsDatabase(), giveDualOrderName);
}

std::wstring
Method::OrderName(const MethodDatabase& database, bool giveDualOrderName) const
{
    return AnotherOrderName(database, noOfBells, giveDualOrderName && dualNoOfBells);
}

std::wstring
Method::AnotherOrderName(const RingingDatabase& database, unsigned int numberOfBells, bool dualOrder)
{
	return AnotherOrderName(database.MethodsDatabase(), numberOfBells, dualOrder);
}

std::wstring
Method::AnotherOrderName(const MethodDatabase& database, unsigned int numberOfBells, bool dualOrder)
{
    std::wstring orderName;
    if (dualOrder)
    {
        std::wstring secondOrderName;
        if (database.FindOrderName(numberOfBells-1, secondOrderName))
        {
            orderName.append(secondOrderName);
            orderName.append(L" and ");
        }
    }
    std::wstring mainOrderName;
    database.FindOrderName(numberOfBells, mainOrderName);
    orderName.append(mainOrderName);

    return orderName;
}

bool
Method::Duplicate(const Duco::RingingObject& other, const Duco::RingingDatabase& database) const
{
    if (other.ObjectType() != ObjectType())
        return false;

    const Duco::Method& otherMethod = static_cast<const Duco::Method&>(other);

    if (placeNotation.length() != 0 && DucoEngineUtils::CompareString(otherMethod.PlaceNotation(), this->PlaceNotation(), true, false))
    { // If place notation matches - ignore name, type etc.
        return true;
    }
    if (noOfBells != otherMethod.noOfBells || dualNoOfBells != otherMethod.dualNoOfBells)
    {
        return false;
    }
    if (!DucoEngineUtils::CompareString(otherMethod.type, type, true, true))
    {
        return false;
    }
    std::wstring thisMethodName = this->FullName(database);
    std::wstring otherMethodName = otherMethod.FullName(database);
    if (!DucoEngineUtils::CompareString(otherMethodName, thisMethodName))
    {
        return false;
    }

    return true;
}

bool
Method::SetPlaceNotation(const std::wstring& newNotation) 
{
    return CheckNotation(newNotation, placeNotation);
}

bool
Method::SetPlaceNotation(const std::wstring& newNotation, std::wstring& savedNotation) 
{
    savedNotation.clear();
    if (CheckNotation(newNotation, placeNotation))
    {
        savedNotation = placeNotation;
        return true;
    }
    return false;
}

bool
Method::SetBobPlaceNotation(const std::wstring& newNotation, size_t index)
{
    std::wstring checkedNotation;
    return SetBobPlaceNotation(newNotation, index, checkedNotation);
}

bool
Method::SetBobPlaceNotation(const std::wstring& newNotation, size_t index, std::wstring& savedNotation)
{
    savedNotation.clear();
    std::wstring checkedNotation;
    bool notationUpdated (CheckNotation(newNotation, checkedNotation));
    if (notationUpdated)
    {
        savedNotation = checkedNotation;
    }
    else
    {
        savedNotation = newNotation;
    }
    if (bobLEPlaceNotations.contains(index))
    {
        bobLEPlaceNotations[index] = savedNotation;
    }
    else
    {
        std::pair<size_t, std::wstring> newObject(index, savedNotation);
        bobLEPlaceNotations.insert(newObject);
    }
    return notationUpdated;
}

bool
Method::SetSinglePlaceNotation(const std::wstring& newNotation, size_t index)
{
    std::wstring checkedNotation;
    return SetSinglePlaceNotation(newNotation, index, checkedNotation);
}

bool
Method::SetSinglePlaceNotation(const std::wstring& newNotation, size_t index, std::wstring& savedNotation)
{
    savedNotation.clear();
    std::wstring checkedNotation;
    bool notationUpdated (CheckNotation(newNotation, checkedNotation));
    if (notationUpdated)
    {
        savedNotation = checkedNotation;
    }
    else
    {
        savedNotation = newNotation;
    }
    if (singleLEPlaceNotations.contains(index))
    {
        singleLEPlaceNotations[index] = savedNotation;
    }
    else
    {
        std::pair<size_t, std::wstring> newObject(index, savedNotation);
        singleLEPlaceNotations.insert(newObject);
    }

    return notationUpdated;
}

bool
Method::BobPlaceNotation(std::wstring& returnNotation, size_t index) const
{
    returnNotation.clear();
    std::map<size_t, std::wstring>::const_iterator it = bobLEPlaceNotations.find(index);

    if (it == bobLEPlaceNotations.end())
        return false;

    returnNotation = it->second;
    return true;
}

bool
Method::SinglePlaceNotation(std::wstring& returnNotation, size_t index) const
{
    returnNotation.clear();
    std::map<size_t, std::wstring>::const_iterator it = singleLEPlaceNotations.find(index);

    if (it == singleLEPlaceNotations.end())
        return false;

    returnNotation = it->second;
    return true;
}

bool
Method::ContainsSingleNotation(size_t index) const
{
    return singleLEPlaceNotations.find(index) != singleLEPlaceNotations.end();
}

bool
Method::ContainsBobNotation(size_t index) const
{
    return bobLEPlaceNotations.find(index) != bobLEPlaceNotations.end();
}

bool
Method::ContainsBobOrSingleNotation(size_t index) const
{
    return ContainsSingleNotation(index) || ContainsBobNotation(index);
}

bool
Method::ContainsBobAndSingleNotation(size_t index) const
{
    return ContainsSingleNotation(index) && ContainsBobNotation(index);
}

bool
Method::CheckNotation(const std::wstring& newNotation, std::wstring& savePos) const
{
    bool changed(false);
    savePos.clear();
    for (unsigned int i = 0; i < newNotation.length(); ++i)
    {
        char nextChar = toupper(newNotation[i]);
        if (!changed && nextChar != newNotation[i])
            changed = true;
        if (nextChar != ' ')
            savePos.append(1, nextChar);
        else
            changed = true;
    }
    if (savePos.find(L"LE") != std::wstring::npos || DucoEngineUtils::Count(newNotation, '-') > 1)
    {   // Method Master format
        changed = true;
        std::wstring methodMasterStylePlaceNotation(savePos);
        DucoEngineUtils::ConvertFromMethodMaster(methodMasterStylePlaceNotation, savePos);
    }
    return changed;
}

bool
Method::operator!=(const Duco::Method& rhs) const
{
    if (noOfBells != rhs.noOfBells)
        return true;
    if (dualNoOfBells != rhs.dualNoOfBells)
        return true;
    if (name.compare(rhs.name) != 0)
        return true;
    if (type.compare(rhs.type) != 0)
        return true;
    if (placeNotation.compare(rhs.placeNotation) != 0)
        return true;
    if (bobLEPlaceNotations != rhs.bobLEPlaceNotations)
        return true;
    if (singleLEPlaceNotations != singleLEPlaceNotations)
        return true;
    if (noOfLeads != rhs.noOfLeads)
        return true;
    if (printFromBell != rhs.printFromBell)
        return true;
    if (pealbaseLink.compare(rhs.pealbaseLink) != 0)
        return true;

    return false;
}

bool
Method::operator==(const Duco::Method& rhs) const
{
    if (noOfBells != rhs.noOfBells)
        return false;
    if (dualNoOfBells != rhs.dualNoOfBells)
        return false;
    if (name.compare(rhs.name) != 0)
        return false;
    if (type.compare(rhs.type) != 0)
        return false;
    if (placeNotation.compare(rhs.placeNotation) != 0)
        return false;
    if (bobLEPlaceNotations != rhs.bobLEPlaceNotations)
        return false;
    if (singleLEPlaceNotations != rhs.singleLEPlaceNotations != 0)
        return false;
    if (noOfLeads != rhs.noOfLeads)
        return false;
    if (printFromBell != rhs.printFromBell)
        return false;
    if (pealbaseLink.compare(rhs.pealbaseLink) != 0)
        return true;

    return true;
}

unsigned int
Method::MatchProbability(const std::wstring& otherMethodName, const std::wstring& otherMethodNameOptionalText, const std::wstring& otherMethodType, unsigned int otherNoOfBells, bool allowTypeSubstr) const
{
    unsigned int matchProbability = 0;
    if (noOfBells == otherNoOfBells)
    {
        ++matchProbability;

        std::wstring methodNameWithoutOptionalBrackets = name;
        std::wstring optionalBracketedText;
        bool usedBracketedTest = false;
        if (otherMethodNameOptionalText.length() > 0 || methodNameWithoutOptionalBrackets.find(L"(") != wstring::npos)
        {
            if (name.find(otherMethodNameOptionalText) != std::wstring::npos)
            {
                usedBracketedTest = true;
                ++matchProbability;
            }
            else
            {
                usedBracketedTest = DucoEngineUtils::RemoveMethodCount(methodNameWithoutOptionalBrackets, optionalBracketedText);
            }
        }

        bool typeMatched = false;

        bool nameMatched = false;
        if (DucoEngineUtils::CompareString(methodNameWithoutOptionalBrackets, otherMethodName, true, true))
        {
            nameMatched = true;
            ++matchProbability; // Name matches exactly
            if (methodNameWithoutOptionalBrackets.length() > 1 && otherMethodNameOptionalText.length() == 0 && optionalBracketedText.length() == 0)
            {
                ++matchProbability;
                usedBracketedTest = false;
            }
            else
                usedBracketedTest = true;
        }
        else if (DucoEngineUtils::FindSubstring(methodNameWithoutOptionalBrackets, otherMethodName))
        {
            ++matchProbability;
            nameMatched = true;
        }
        else if (DucoEngineUtils::FindSubstring(otherMethodName, methodNameWithoutOptionalBrackets))
        {
            ++matchProbability;
            nameMatched = true;
        }
        if (usedBracketedTest && nameMatched)
        {
            std::wstring otherMethodNameOptionalTextTemp = DucoEngineUtils::RemoveInvalidChars(otherMethodNameOptionalText, L"(). ");
            optionalBracketedText = DucoEngineUtils::RemoveInvalidChars(optionalBracketedText, L"(). ");
            if (DucoEngineUtils::CompareString(otherMethodNameOptionalTextTemp, optionalBracketedText, true, true))
            {
                ++matchProbability;
            }
            else if (optionalBracketedText.length() > 0 && _wtoi(otherMethodNameOptionalTextTemp.c_str()) == _wtoi(optionalBracketedText.c_str()))
            { // Number of methods, but specified differently.
                ++matchProbability;
            }
        }
        if (type.length() == 0 && otherMethodType.length() == 0)
        {
            matchProbability += 1;
            typeMatched = true;
        }
        else if (DucoEngineUtils::CompareString(type, otherMethodType, true, true))
        {
            matchProbability += 2; // Type matches exactly
            typeMatched = true;
        }
        else if (allowTypeSubstr)
        {
            std::wstring lowerCaseType;
            DucoEngineUtils::ToLowerCase(type, lowerCaseType);
            std::wstring lowerCaseOtherMethodType;
            DucoEngineUtils::ToLowerCase(otherMethodType, lowerCaseOtherMethodType);
            if (lowerCaseType.find_first_of(lowerCaseOtherMethodType) == 0)
            {
                ++matchProbability; // Type matches
                typeMatched = true;
            }
        }

        if (!typeMatched && !nameMatched && (otherMethodName.length() == 0 || otherMethodType.length() == 0) && otherMethodName.length() != otherMethodType.length())
        {
            // if one but not both of the strings we're searching for are null and nothing has matched so far
            // try checking the other way round
            if ( (name.length() == 0 && otherMethodType.length() == 0) ||
                    DucoEngineUtils::CompareString(name, otherMethodType, true, true))
            {
                ++matchProbability;
            }
            if ((type.length() == 0 && otherMethodName.length() == 0) ||
                    DucoEngineUtils::CompareString(type, otherMethodName, true, true))
            {
                ++matchProbability;
            }
        }
        if (typeMatched && !nameMatched && otherMethodName.length() > 0)
        {
            // SUrprise Surprise Major in a db was causing a method with no name to match, e.g "surprise Major" - unit tests all exist for this.
            --matchProbability;
        }
    }

    return matchProbability;
}

bool
Method::Match(const std::wstring& otherMethodName, const std::wstring& otherMethodType, unsigned int otherNoOfBells) const
{
    if (otherNoOfBells != noOfBells)
        return false;

    if (DucoEngineUtils::CompareString(name, otherMethodName, true, true))
    {
        if (DucoEngineUtils::CompareString(type, otherMethodType, true, true))
        {
            return true;
        }
    }
    return false;
}

void
Method::SetName(const std::wstring& newName)
{
    name = newName;
    spliced = DucoEngineUtils::MethodNameContainsSpliced(name);
}

void
Method::SetError(TMethodErrorCodes code)
{
    errorCode.set(code,1);
}

bool
Method::IsPrinciple() const
{
    if (PlaceNotation().length() <= 1)
        return false;

    Method tempMethod (*this);
    Duco::PlaceNotation notation (tempMethod, 0);
    return notation.LeadOrder().IsPrinciple();
}

void 
Method::ExportToCsv(std::wofstream& file, const MethodDatabase& database) const
{
    file << "\"";
    file << FullName(database);
    file << "\"";
}

void
Method::ExportToXml(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& methodsElement, bool exportDucoObjects) const
{
    DOMElement* newMethod = outputFile.createElement(XMLStrL("Methods"));
    methodsElement.appendChild(newMethod);

    DucoXmlUtils::AddObjectId(*newMethod, id);
    if (spliced)
    {
        DucoXmlUtils::AddBoolAttribute(*newMethod, L"Spliced", spliced);
    }

    DucoXmlUtils::AddAttribute(*newMethod, L"NumberOfBells", noOfBells);
    if (dualNoOfBells)
    {
        DucoXmlUtils::AddBoolAttribute(*newMethod, L"DualStage", dualNoOfBells);
    }
    DucoXmlUtils::AddElement(outputFile, *newMethod, L"Name", name);
    DucoXmlUtils::AddElement(outputFile, *newMethod, L"Type", type);
    DucoXmlUtils::AddElement(outputFile, *newMethod, L"PlaceNotation", placeNotation);

    DucoXmlUtils::AddAttribute(*newMethod, L"PealBaseLink", pealbaseLink);
}

bool
Method::CapitaliseField(bool updateMethodName, bool updateMethodType)
{
    bool changed = false;

    if (updateMethodName)
    {
        std::wstring newName = DucoEngineUtils::ToCapitalised(name, true);
        if (name.compare(newName) != 0)
        {
            name = newName;
            changed = true;
        }
    }
    if (updateMethodType)
    {
        std::wstring newType = DucoEngineUtils::ToCapitalised(type, true);
        if (type.compare(newType) != 0)
        {
            type = newType;
            changed = true;
        }
    }

    return changed;
}
