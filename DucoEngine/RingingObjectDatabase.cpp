#include "RingingObjectDatabase.h"

#include "CountStringSortObject.h"
#include "DatabaseReader.h"
#include "DatabaseWriter.h"
#include "RenumberProgressCallback.h"
#include "RingingObject.h"
#include "ImportExportProgressCallback.h"
#include "SearchArgument.h"
#include "ProgressCallback.h"
#include "DucoEngineLog.h"
#include "SearchValidationArgument.h"

#include <assert.h>
#include <map>
#include <string>
#include <algorithm>
#include <chrono>
#include <mutex>
#include "DucoEngineLog.h"
using namespace std::chrono;

using namespace Duco;
using namespace std;

namespace Duco
{
    const Duco::ObjectId FIRST_OBJECT_ID(1);
}

std::mutex g_index_mutex;


RingingObjectDatabase::RingingObjectDatabase(TObjectType newObjectType)
:   objectType(newObjectType), dataChanged(false)
{
    objectsIndexIterator = objectsIndex.end();
}

RingingObjectDatabase::~RingingObjectDatabase()
{
    ClearObjects(false);
}

void
RingingObjectDatabase::ClearObjects(bool /*populateWithDefaults*/)
{
    for (auto& [key, value] : listOfObjects)
    {
        delete value;
        value = NULL;
    }
    listOfObjects.clear();
    objectsIndex.clear();
    objectsIndexIterator = objectsIndex.end();
    dataChanged = false;
}

size_t
RingingObjectDatabase::NumberOfObjects() const
{
    return listOfObjects.size();
}

namespace Duco
{
    bool sort_index_function(DucoIndex i, DucoIndex j) { return (i.Id() < j.Id()); }
}

bool
RingingObjectDatabase::AddOwnedObject(Duco::RingingObject*& newDatabaseObject)
{
    pair< DUCO_OBJECTS_CONTAINER::iterator, bool> objectInserted;

    if (!newDatabaseObject->Id().ValidId())
    {
        DUCO_OBJECTS_INDEX::const_reverse_iterator it = objectsIndex.rbegin();
        if (it != objectsIndex.rend())
        {
            newDatabaseObject->SetId((it->Id()) + 1);
            pair<Duco::ObjectId, Duco::RingingObject*> newObject(newDatabaseObject->Id(), newDatabaseObject);
            objectInserted = listOfObjects.insert(newObject);
        }
        else
        {
            newDatabaseObject->SetId(FIRST_OBJECT_ID);
            pair<Duco::ObjectId, Duco::RingingObject*> newObject(newDatabaseObject->Id(), newDatabaseObject);
            objectInserted = listOfObjects.insert(newObject);
        }
    }
    else
    {
        pair<Duco::ObjectId, Duco::RingingObject*> newObject(newDatabaseObject->Id(), newDatabaseObject);
        objectInserted = listOfObjects.insert(newObject);
    }  

    if (objectInserted.second)
    {
        DucoIndex newIndex (objectInserted.first->first.Id(), newDatabaseObject);
        objectsIndex.push_back(newIndex);
        std::sort(objectsIndex.begin(), objectsIndex.end(), sort_index_function);
        objectsIndexIterator = objectsIndex.end();
        SetDataChanged();
    }
    else
    {
        delete newDatabaseObject;
        newDatabaseObject = NULL;
    }

    return objectInserted.second;
}

bool
RingingObjectDatabase::DeleteObject(const Duco::ObjectId& objectId)
{
    DUCO_OBJECTS_CONTAINER::iterator it = listOfObjects.find(objectId);
    if (it != listOfObjects.end())
    {
        if (SetIndex(objectId))
        {
            objectsIndex.erase(objectsIndexIterator);
            objectsIndexIterator = objectsIndex.end();
        }
        delete it->second;
        it->second = NULL;
        listOfObjects.erase(it);
        SetDataChanged();
        return true;
    }
    return false;
}

bool
RingingObjectDatabase::DeleteObjects(const std::set<Duco::ObjectId>& objectIds)
{
    bool allDeleted = true;

    for (auto const& it : objectIds)
    {
        allDeleted &= DeleteObject(it);
    }
    return allDeleted;
}

bool
RingingObjectDatabase::UpdateObject(const Duco::RingingObject& updatedObject)
{
    DUCO_OBJECTS_CONTAINER::iterator it = listOfObjects.find(updatedObject.Id());
    if (it != listOfObjects.end())
    {
        if (SaveUpdatedObject(*it->second, updatedObject))
        {
            SetDataChanged();
            return true;
        }
    }
    return false;

}

bool
RingingObjectDatabase::FirstObject(Duco::ObjectId& objectId, bool setIndex) const
{
    if (objectsIndex.size() > 0)
    {
        if (setIndex)
        {
            RingingObjectDatabase* removedConst = const_cast<RingingObjectDatabase*>(this);
            objectsIndexIterator = removedConst->objectsIndex.begin();
            if (objectsIndexIterator != objectsIndex.end())
            {
                objectId = objectsIndexIterator->Id();
                return true;
            }
        }
        else
        {
            DUCO_OBJECTS_INDEX::const_iterator objectsIndexIt = objectsIndex.begin();
            if (objectsIndexIt != objectsIndex.end())
            {
                objectId = objectsIndexIt->Id();
                return true;
            }
        }
    }
    objectId.ClearId();
    return false;
}

bool
RingingObjectDatabase::LastObject(Duco::ObjectId& objectId, bool setIndex) const
{
    if (objectsIndex.size() > 0)
    {
        if (setIndex)
        {
            RingingObjectDatabase* removedConst = const_cast<RingingObjectDatabase*>(this);
            objectsIndexIterator = removedConst->objectsIndex.end();
            --objectsIndexIterator;
            if (objectsIndexIterator != objectsIndex.end())
            {
                objectId = objectsIndexIterator->Id();
                return true;
            }
        }
        else
        {
            DUCO_OBJECTS_INDEX::const_reverse_iterator objectsIndexIt = objectsIndex.rbegin();
            if (objectsIndexIt != objectsIndex.rend())
            {
                objectId = objectsIndexIt->Id();
                return true;
            }
        }
    }
    objectId.ClearId();
    return false;
}

bool
RingingObjectDatabase::NextObject(Duco::ObjectId& objectId, bool forwards, bool setIndex) const
{
    if (!setIndex)
    {
        if (forwards)
        {
            DUCO_OBJECTS_INDEX::const_iterator it = objectsIndex.begin();
            if (objectsIndexIterator != objectsIndex.end() && objectsIndexIterator->Id() == objectId)
            {
                it = objectsIndexIterator;
            }
            while (it != objectsIndex.end() && objectId != it->Id())
            {
                ++it;
            }
            if (it == objectsIndex.end())
            { // No objects at all, but a previous id was supplied
                objectId.ClearId();
                return false;
            }
            ++it;
            if (it != objectsIndex.end())
            {
                objectId = it->Id();
                return true;
            }
            objectId.ClearId();
            return false;
        }
        else
        {
            DUCO_OBJECTS_INDEX::const_reverse_iterator it = objectsIndex.rbegin();
            while (it != objectsIndex.rend() && objectId != it->Id())
            {
                ++it;
            }
            ++it;
            if (it != objectsIndex.rend())
            {
                objectId = it->Id();
                return true;
            }
            objectId.ClearId();
            return false;
        }
    }

    if (objectsIndexIterator != objectsIndex.end())
    {
        if (objectId == objectsIndexIterator->Id())
        {
            if (!forwards && objectsIndexIterator == objectsIndex.begin())
            {
                objectId.ClearId();
                return false;
            }
            forwards ? ++objectsIndexIterator : --objectsIndexIterator;
            if (objectsIndexIterator != objectsIndex.end())
            {
                objectId = objectsIndexIterator->Id();
                return true;
            }
            else
            {
                objectId.ClearId();
                return false;
            }
        }
    }

    if (SetIndex(objectId))
    {
        forwards ? ++objectsIndexIterator : --objectsIndexIterator;
        if (objectsIndexIterator != objectsIndex.end())
        {
            objectId = objectsIndexIterator->Id();
            return true;
        }
    }
    objectId.ClearId();
    return false;
}

bool
RingingObjectDatabase::ForwardMultipleObjects(Duco::ObjectId& objectId, bool forwards, size_t numberToMoveBy) const
{
    bool objectFound (true);
    
    size_t number(numberToMoveBy);
    Duco::ObjectId originalId (objectId);
    Duco::ObjectId previousId (objectId);
    while (number > 0 && objectFound)
    {
        previousId = objectId;
        objectFound = NextObject(objectId, forwards);
        --number;
    }

    if (!objectFound && number != numberToMoveBy && previousId.ValidId() && originalId != previousId)
    {
        objectId = previousId;
        objectFound = true;
    }

    return objectFound;
}

bool
RingingObjectDatabase::ObjectExists(const Duco::ObjectId& objectId) const
{
	return listOfObjects.find(objectId) != listOfObjects.end();
}

const Duco::RingingObject* const
RingingObjectDatabase::FindObject(const Duco::ObjectId& objectId, bool setIndex) const
{
    RingingObjectDatabase* removedConst = const_cast<RingingObjectDatabase*>(this);
    return removedConst->FindObject(objectId, setIndex);
}

Duco::RingingObject*
RingingObjectDatabase::FindObject(const Duco::ObjectId& objectId, bool setIndex)
{
    time_point<high_resolution_clock> start = high_resolution_clock::now();
    if (!objectId.ValidId())
    {
        if (setIndex)
        {
            SetIndex(objectId);
        }
        return NULL;
    }
    if (objectsIndexIterator != objectsIndex.end() && objectsIndexIterator->Id() == objectId)
    {
        DUCOENGINEDEBUGLOG3(start, "Found object in index %u", objectId.Id());
        return objectsIndexIterator->Object();
    }
#ifdef _DEBUG
    if (lastSearchedForId == objectId)
    {
        ++lastSearchedForCount;
        DUCOENGINEDEBUGLOG3("**** Object was the last searched for id, but the index wasnt saved %u; %u times", objectId.Id(), lastSearchedForCount);
    }
    else
    {
        lastSearchedForId = objectId;
        lastSearchedForCount = 1;
    }
#endif
    DUCO_OBJECTS_CONTAINER::const_iterator it = listOfObjects.find(objectId);
    if (it != listOfObjects.end())
    {
        if (!setIndex || SetIndex(objectId))
        {
            DUCOENGINEDEBUGLOG3(start, "Found object %u", objectId.Id());
            return it->second;
        }
    }
    if (setIndex)
    {
        objectsIndexIterator = objectsIndex.end();
    }
    DUCOENGINEDEBUGLOG3(start, "Didnt find object %u", objectId.Id());

    return NULL;
}

void
RingingObjectDatabase::Externalise(Duco::ImportExportProgressCallback* callback, DatabaseWriter& writer, size_t noOfObjectsInThisDatabase, size_t totalObjects, size_t objectsProcessedSoFar) const
{
    size_t totalProcessed = objectsProcessedSoFar;
    size_t count = 0;
    DUCO_OBJECTS_CONTAINER::const_iterator it = listOfObjects.begin();
    while (it != listOfObjects.end())
    {
        ++totalProcessed;
        ++count;
        float percentageComplete = ((float)totalProcessed / (float)totalObjects) * 100.0f;
        const Duco::RingingObject* const object = it++->second;
        object->Externalise(writer);
        if (callback != NULL)
        {
            callback->ObjectProcessed(false, objectType, object->Id(), (int)percentageComplete, count);
        }
    }
}

Duco::ObjectId
RingingObjectDatabase::FindDuplicateObject(const Duco::RingingObject& object, const Duco::RingingDatabase& database) const
{
    ObjectId objectId;
    DUCO_OBJECTS_CONTAINER::const_iterator objectIt =  listOfObjects.begin();
    while (objectIt != listOfObjects.end() && !objectId.ValidId())
    {
        if (objectIt->second->Duplicate(object, database))
        {
            objectId = objectIt->first;
        }
        ++objectIt;
    }
    return objectId;
}

void
RingingObjectDatabase::GetAllObjectIds(std::set<Duco::ObjectId>& allIds) const
{
    allIds.clear();
    for (auto const& [key, value] : listOfObjects)
    {
        allIds.insert(key);
    }
}

void
RingingObjectDatabase::ResetCurrentPointer()
{
    objectsIndexIterator = objectsIndex.end();
}

void
RingingObjectDatabase::GetNewObjectIdsByAlphabetic(std::map<Duco::ObjectId, Duco::ObjectId>& newIdsOldFirst, bool lastNameFirst) const
{
    bool updatesFound = false;
    newIdsOldFirst.clear();

    std::multimap<std::wstring, Duco::ObjectId> sortedObjectsByName;
    for (auto const& [key, value] : listOfObjects)
    {
        pair<std::wstring, Duco::ObjectId> newObject(value->FullNameForSort(lastNameFirst), key);
        sortedObjectsByName.insert(newObject);
    }
    unsigned int count = 1;
    for (auto const& [key, value] : sortedObjectsByName)
    {
        pair<Duco::ObjectId, Duco::ObjectId> newObject(value, count);
        if (newObject.first != newObject.second)
        {
            updatesFound = true;
        }
        newIdsOldFirst.insert(newObject);
        ++count;
    }
}

bool
RingingObjectDatabase::SwapObjects(const Duco::ObjectId& existingId, const Duco::ObjectId& newId)
{
    if (existingId == newId)
        return false;

    DUCO_OBJECTS_CONTAINER::iterator existingIt = listOfObjects.find(existingId);
    if (existingIt == listOfObjects.end())
    {
        return false;
    }

    DUCO_OBJECTS_CONTAINER::iterator newIt = listOfObjects.find(newId);
    if (newIt != listOfObjects.end())
    {
        // object exists at the new location, swap them
        RingingObject* firstMethod = newIt->second;
        RingingObject* secondMethod = existingIt->second;
        firstMethod->SetId(existingId);
        secondMethod->SetId(newId);

        newIt->second = secondMethod;
        existingIt->second = firstMethod;

        return true;
    }

    // Nothing exists at the new index, just move this one.
    RingingObject* method = existingIt->second;
    method->SetId(newId);
    pair<Duco::ObjectId, RingingObject*> newObject (newId, method);

    if (listOfObjects.insert(newObject).second)
    {
        listOfObjects.erase(existingIt);
        return true;
    }
    return false;
}

bool
RingingObjectDatabase::RenumberObjects(std::map<ObjectId, ObjectId> newObjectIds, Duco::RenumberProgressCallback* callback)
{
    bool objectsRenumbered = false;
    std::map<ObjectId, ObjectId>::reverse_iterator it = newObjectIds.rbegin();
    while(it != newObjectIds.rend())
    {
        ObjectId oldId = it->first;
        ObjectId newId = it->second;
        if (SwapObjects(oldId, newId))
        {
            SetDataChanged();
            objectsRenumbered = true;
            std::map<ObjectId, ObjectId>::iterator moveIt = newObjectIds.find(newId);
            if (moveIt != newObjectIds.end())
            {
                ObjectId otherObjectsNewId = moveIt->second;
                if (oldId != otherObjectsNewId)
                {
                    it->second = otherObjectsNewId;
                }
                else
                {
                    newObjectIds.erase(oldId);
                }
                newObjectIds.erase(newId);
            }
            else
                newObjectIds.erase(oldId);

        }
        else
        {
            newObjectIds.erase(newId);
        }
        it = newObjectIds.rbegin();
        if (callback != NULL)
        {
            callback->RenumberStep(NumberOfObjects() - newObjectIds.size(), NumberOfObjects());
        }
    }
    RebuildIndex();
    return objectsRenumbered;
}

void
RingingObjectDatabase::GetNewIdsByPealCountAndAlphabetic(const std::map<Duco::ObjectId, Duco::PealLengthInfo>& objectPealCounts, std::map<Duco::ObjectId, Duco::ObjectId>& newObjectIds, bool lastNameFirst) const
{
    std::multimap<CountStringSortObject, Duco::ObjectId> objectsByPealsAndAlphabetic;

    DUCO_OBJECTS_CONTAINER::const_iterator it = listOfObjects.begin();
    while (it != listOfObjects.end())
    {
        std::map<Duco::ObjectId, Duco::PealLengthInfo>::const_iterator countIt = objectPealCounts.find(it->first);
        assert(countIt != objectPealCounts.end());
        CountStringSortObject indexObj(countIt->second, it->second->FullNameForSort(lastNameFirst));
        std::pair<CountStringSortObject,Duco::ObjectId> newObject(indexObj, it->first);
        objectsByPealsAndAlphabetic.insert(newObject);
        ++it;
    }

    newObjectIds.clear();
    size_t count (0);
    std::multimap<CountStringSortObject, Duco::ObjectId>::const_iterator it2 = objectsByPealsAndAlphabetic.begin();
    while (it2 != objectsByPealsAndAlphabetic.end())
    {
        std::pair<Duco::ObjectId, Duco::ObjectId> newObject(it2->second, ++count);
        newObjectIds.insert(newObject);
        ++it2;
    }
}

bool
RingingObjectDatabase::RebuildRecommended() const
{
    DUCO_OBJECTS_INDEX::const_reverse_iterator it = objectsIndex.rbegin();
    if (it != objectsIndex.rend())
    {
        Duco::ObjectId lastObjectId (it->Id());
        size_t numberOfObjects (listOfObjects.size());
        if (lastObjectId != numberOfObjects)
            return true;
    }

    return false;
}

void
RingingObjectDatabase::SearchObjects(Duco::ProgressCallback& callback, std::set<ObjectId>& foundIds, const std::deque<Duco::TSearchArgument*>& searchArguments, const RingingDatabase& database, bool doubleProgressCallbacks) const
{
    if (searchArguments.size() == 0)
    {
        return;
    }
    float total = float(listOfObjects.size());
    if (doubleProgressCallbacks)
    {
        total += total;
    }
    float count (0);

    foundIds.clear();
    for (auto const& [key, value] : listOfObjects)
    {
        bool addPeal (true);
        std::deque<TSearchArgument*>::const_iterator argIt = searchArguments.begin();
        while (argIt != searchArguments.end() && addPeal)
        {
            addPeal = MatchObject(*(value), **argIt, database);
            ++argIt;
        }
        if (addPeal)
        {
            foundIds.insert(key);
        }
        ++count;
        callback.Step(int((count / total) * 100));
    }
}

void
RingingObjectDatabase::ValidateObjects(Duco::ProgressCallback& callback, std::set<ObjectId>& checkIds, const std::deque<Duco::TSearchValidationArgument*>& searchArguments, const RingingDatabase& database, bool doubleProgressCallbacks) const
{
    if (searchArguments.size() == 0)
    {
        return;
    }
    float total = float(listOfObjects.size());
    float count(0);
    if (doubleProgressCallbacks)
    {
        count = total;
        total += total;
    }

    std::set<ObjectId> objectsWithErrors;

    for (auto const& [key, value] : listOfObjects)
    {
        bool addPeal(true);
        std::deque<TSearchValidationArgument*>::const_iterator argIt = searchArguments.begin();
        while (argIt != searchArguments.end() && addPeal)
        {
            addPeal = ValidateObject(*(value), **argIt, checkIds, database);
            ++argIt;
        }
        if (addPeal)
        {
            objectsWithErrors.insert(key);
        }
        ++count;
        callback.Step(int((count / total) * 100));
    }
    {
        for (auto const& it : searchArguments)
        {
            std::set<Duco::ObjectId> errorIdsFromArgument = it->ErrorIds();
            objectsWithErrors.insert(errorIdsFromArgument.begin(), errorIdsFromArgument.end());
        }
    }

    checkIds = objectsWithErrors;
}

void
RingingObjectDatabase::ClearValidationFlags()
{
    for (auto& [key, value] : listOfObjects)
    {
        value->ClearErrors();
    }
}

bool
RingingObjectDatabase::UpdateObjects(ProgressCallback& callback, const std::set<Duco::ObjectId>& ids, const Duco::TUpdateArgument& updateArgument)
{
    bool success (false);
    float total = float(ids.size());
    float count (0);
    for (auto const& it : ids)
    {
        success |= UpdateObjectField(it, updateArgument);
        ++count;
        callback.Step(int((count / total) * 100));
    }

    if (success)
    {
        SetDataChanged();
    }
    return success;
}

void
RingingObjectDatabase::RebuildIndex()
{
    objectsIndex.clear();
    for (auto const& [key, value] : listOfObjects)
    {
		Duco::DucoIndex newIndex(key, value);
		objectsIndex.push_back(newIndex);
    }
    objectsIndexIterator = objectsIndex.end();
    std::sort(objectsIndex.begin(), objectsIndex.end(), sort_index_function);
}

bool
RingingObjectDatabase::SetIndex(const Duco::ObjectId& objectId) const
{
    if (!objectId.ValidId())
    {
        DUCOENGINELOG("Set Index failed - invalid id");
        return false;
    }
    time_point<high_resolution_clock> start = high_resolution_clock::now();
    if (objectsIndexIterator != objectsIndex.end() && objectId == objectsIndexIterator->Id())
    {
        return true;
    }
    else if (objectsIndexIterator == objectsIndex.end() && !objectId.ValidId())
    {
        return true;
    }

    std::lock_guard<std::mutex> guard(g_index_mutex);
    RingingObjectDatabase* removedConst = const_cast<RingingObjectDatabase*>(this);
    if (!objectId.ValidId())
    {
        objectsIndexIterator = removedConst->objectsIndex.end();
    }
    else
    {
        if (objectId.ValidId())
        {
            objectsIndexIterator = removedConst->objectsIndex.begin();
            while (objectsIndexIterator != objectsIndex.end())
            {
                if ((objectsIndexIterator->Id()) == objectId)
                {
                    DUCOENGINEDEBUGLOG2(start, "Set Index finished");
                    return true;
                }
                else
                {
                    ++objectsIndexIterator;
                }
            }
        }
        objectsIndexIterator = removedConst->objectsIndex.end();
    }
    DUCOENGINELOG("Set Index failed - couldnt find object");
    DUCOENGINEDEBUGLOG2(start, "Set Index failed - couldnt find object");
    return false;
}
