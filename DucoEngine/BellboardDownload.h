#ifndef __BELLBOARDDOWNLOAD_H__
#define __BELLBOARDDOWNLOAD_H__

#include <string>
#include <cpr/cpr.h>

namespace Duco
{
    class DatabaseSettings;

    class BellboardDownload
    {
    public:
        BellboardDownload();
        ~BellboardDownload();

        bool DownloadPerformance(const Duco::DatabaseSettings& settings, const std::wstring& bellboardId);
        std::string Performance() const;

    protected:
        cpr::Response response;

    private:

    };

}

#endif __BELLBOARDDOWNLOAD_H__
