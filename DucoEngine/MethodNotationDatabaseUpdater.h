#ifndef __METHODNOTATIONDATABASEUPDATER_H__
#define __METHODNOTATIONDATABASEUPDATER_H__

#include "MethodNotationDatabaseBase.h"
#include <map>

namespace Duco
{
class MethodNotationDatabaseUpdater : public MethodNotationDatabaseBase
{
public:
    DllExport MethodNotationDatabaseUpdater (Duco::MethodDatabase& newDatabase, const std::string& newDatabaseFilename, ProgressCallback* const newCallback, const std::map<std::wstring, Duco::ObjectId>& newMethodsToUpdate);
    DllExport ~MethodNotationDatabaseUpdater();

    inline unsigned int NoOfMethodsUpdated() const;

    //from MethodNotationDatabaseBase
    void AddMethod(const Duco::ObjectId& id, unsigned int stage, const std::wstring& name, const std::wstring& type, const std::wstring& title, const std::wstring& notation);

protected:
    bool FindMethodToUpdateNotation(const std::wstring& methodName, unsigned int numberOfBells, Duco::ObjectId& methodId);
    bool UpdateMethodNotation(const Duco::ObjectId& methodId) const;

private:
    const std::map<std::wstring, Duco::ObjectId>&  methodsToUpdate;

    unsigned int        noOfMethodsUpdated;
};

unsigned int
MethodNotationDatabaseUpdater::NoOfMethodsUpdated() const
{
    return noOfMethodsUpdated;
}

} // end namesapce Duco

#endif //!__METHODNOTATIONDATABASEUPDATER_H__
