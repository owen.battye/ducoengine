#include "DatabaseExporter.h"

#include "Association.h"
#include "AssociationDatabase.h"
#include "Composition.h"
#include "CompositionDatabase.h"
#include "DatabaseSettings.h"
#include "Method.h"
#include "MethodDatabase.h"
#include "MethodSeries.h"
#include "MethodSeriesDatabase.h"
#include "Picture.h"
#include "PictureDatabase.h"
#include "Peal.h"
#include "PealDatabase.h"
#include "Ringer.h"
#include "RingerDatabase.h"
#include "RingingDatabase.h"
#include "Tower.h"
#include "TowerDatabase.h"

using namespace std;
using namespace Duco;

DatabaseExporter::DatabaseExporter(const RingingDatabase& theDatabase)
    :   database (theDatabase)
{
    requiredAssociations.clear();
    requiredCompositions.clear();
    requiredMethods.clear();
    requiredMethodSeries.clear();
    requiredPictures.clear();
    requiredRingers.clear();
    requiredTowers.clear();
}

DatabaseExporter::~DatabaseExporter()
{

}

bool 
DatabaseExporter::AddAllObjectsForPeals(const std::set<ObjectId>& newPealIds)
{
    bool noError = true;
    std::set<ObjectId>::const_iterator pealIt = newPealIds.begin();
    while (pealIt != newPealIds.end())
    {
        const Peal* const thePeal = database.PealsDatabase().FindPeal(*pealIt);
        if (thePeal != NULL)
        {
            requiredAssociations.insert(thePeal->AssociationId());
            requiredCompositions.insert(thePeal->CompositionId());
            requiredMethods.insert(thePeal->MethodId());
            requiredMethodSeries.insert(thePeal->SeriesId());
            requiredPictures.insert(thePeal->PictureId());
            requiredTowers.insert(thePeal->TowerId());
            std::set<ObjectId> thisPealRingerIds;
            thePeal->RingerIds(thisPealRingerIds); // includes strappers (& conductors)
            requiredRingers.insert(thisPealRingerIds.begin(), thisPealRingerIds.end());
            pealIds.insert(*pealIt);
        }
        else
        {
            noError = false;
        }
        ++pealIt;
    }

    std::set<ObjectId>::const_iterator compositionIt = requiredCompositions.begin();
    while (compositionIt != requiredCompositions.end())
    {
        const Composition* const theComposition = database.CompositionsDatabase().FindComposition(*compositionIt);
        if (theComposition != NULL)
        {
            requiredRingers.insert(theComposition->ComposerId());
            requiredMethods.insert(theComposition->MethodId());
        }
        else
        {
            noError = false;
        }
        ++compositionIt;
    }

    std::set<ObjectId>::const_iterator towerIt = requiredTowers.begin();
    while (towerIt != requiredTowers.end())
    {
        const Tower* const theTower = database.TowersDatabase().FindTower(*towerIt);
        if (theTower != NULL)
        {
            requiredPictures.insert(theTower->PictureId());
            std::set<Duco::ObjectId> linkedIds;
            database.TowersDatabase().GetLinkedIds(*towerIt, linkedIds);
            requiredTowers.insert(linkedIds.begin(), linkedIds.end());
        }
        else
        {
            noError = false;
        }
        ++towerIt;
    }
    std::set<ObjectId>::const_iterator seriesIt = requiredMethodSeries.begin();
    while (seriesIt != requiredMethodSeries.end())
    {
        const MethodSeries* const theSeries = database.MethodSeriesDatabase().FindMethodSeries(*seriesIt);
        if (theSeries != NULL)
        {
            std::set<ObjectId> thisPealSeriesIds = theSeries->Methods();
            requiredMethods.insert(thisPealSeriesIds.begin(), thisPealSeriesIds.end());
        }
        else
        {
            noError = false;
        }
        ++seriesIt;
    }
    pealIds.erase(KNoId);
    requiredAssociations.erase(KNoId);
    requiredCompositions.erase(KNoId);
    requiredMethods.erase(KNoId);
    requiredMethodSeries.erase(KNoId);
    requiredPictures.erase(KNoId);
    requiredRingers.erase(KNoId);
    requiredTowers.erase(KNoId);
    return !noError;
}

bool
DatabaseExporter::Export(const char* fileName, Duco::ImportExportProgressCallback* newCallback)
{
    bool noErrors = true;
    RingingDatabase* newDatabase = new Duco::RingingDatabase();
    newDatabase->Settings().SetPealDatabase(database.Settings().PealDatabase());

    for (unsigned int count = database.MethodsDatabase().LowestValidOrderNumber(); count <= database.MethodsDatabase().HighestValidOrderNumber(); ++count)
    {
        std::wstring newName;
        if (database.MethodsDatabase().FindOrderName(count, newName))
        {
            noErrors &= !newDatabase->MethodsDatabase().AddOrderName(count, newName);
        }
    }
    {
        std::set<ObjectId>::const_iterator pealIt = pealIds.begin();
        while (pealIt != pealIds.end())
        {
            const Peal* const theObject = database.PealsDatabase().FindPeal(*pealIt);
            if (theObject == NULL)
            {
                noErrors = false;
            }
            else
            {
                noErrors &= newDatabase->PealsDatabase().AddObject(*theObject).ValidId();
            }
            ++pealIt;
        }
    }
    {
        std::set<ObjectId>::const_iterator associationIt = requiredAssociations.begin();
        while (associationIt != requiredAssociations.end())
        {
            const Association* const theObject = database.AssociationsDatabase().FindAssociation(*associationIt);
            if (theObject == NULL)
            {
                noErrors = false;
            }
            else
            {
                noErrors &= newDatabase->AssociationsDatabase().AddObject(*theObject).ValidId();
            }
            ++associationIt;
        }
    }
    {
        std::set<ObjectId>::const_iterator compositionIt = requiredCompositions.begin();
        while (compositionIt != requiredCompositions.end())
        {
            const Composition* const theObject = database.CompositionsDatabase().FindComposition(*compositionIt);
            if (theObject == NULL)
            {
                noErrors = false;
            }
            else
            {
                noErrors &= !newDatabase->CompositionsDatabase().AddObject(*theObject).ValidId();
            }
            ++compositionIt;
        }
    }
    {
        std::set<ObjectId>::const_iterator methodIt = requiredMethods.begin();
        while (methodIt != requiredMethods.end())
        {
            const Method* const theObject = database.MethodsDatabase().FindMethod(*methodIt);
            if (theObject == NULL)
            {
                noErrors = false;
            }
            else
            {
                noErrors &= newDatabase->MethodsDatabase().AddObject(*theObject).ValidId();
            }
            ++methodIt;
        }
    }
    {
        std::set<ObjectId>::const_iterator methodSeriesIt = requiredMethodSeries.begin();
        while (methodSeriesIt != requiredMethodSeries.end())
        {
            const MethodSeries* const theObject = database.MethodSeriesDatabase().FindMethodSeries(*methodSeriesIt);
            if (theObject == NULL)
            {
                noErrors = false;
            }
            else
            {
                noErrors &= newDatabase->MethodSeriesDatabase().AddObject(*theObject).ValidId();
            }
            ++methodSeriesIt;
        }
    }
    {
        std::set<ObjectId>::const_iterator pictureIt = requiredPictures.begin();
        while (pictureIt != requiredPictures.end())
        {
            const Picture* const theObject = database.PicturesDatabase().FindPicture(*pictureIt);
            if (theObject == NULL)
            {
                noErrors = false;
            }
            else
            {
                noErrors &= newDatabase->PicturesDatabase().AddObject(*theObject).ValidId();
            }
            ++pictureIt;
        }
    }
    {
        std::set<ObjectId>::const_iterator ringerIt = requiredRingers.begin();
        while (ringerIt != requiredRingers.end())
        {
            const Ringer* const theObject = database.RingersDatabase().FindRinger(*ringerIt);
            if (theObject == NULL)
            {
                noErrors = false;
            }
            else
            {
                noErrors &= newDatabase->RingersDatabase().AddObject(*theObject).ValidId();
            }
            ++ringerIt;
        }
    }
    {
        std::set<ObjectId>::const_iterator towerIt = requiredTowers.begin();
        while (towerIt != requiredTowers.end())
        {
            const Tower* const theObject = database.TowersDatabase().FindTower(*towerIt);
            if (theObject == NULL)
            {
                noErrors = false;
            }
            else
            {
                noErrors &= newDatabase->TowersDatabase().AddObject(*theObject).ValidId();
            }
            ++towerIt;
        }
    }

    newDatabase->Externalise(fileName, newCallback);
    delete newDatabase;
    return !noErrors;
}

size_t
DatabaseExporter::NumberOfPealsToImport() const
{
    return pealIds.size();
}

size_t
DatabaseExporter::TotalNumberOfObjectsToImport() const
{
    return pealIds.size() + requiredAssociations.size() + requiredCompositions.size() + requiredMethods.size() + requiredMethodSeries.size() + requiredPictures.size() + requiredRingers.size() + requiredTowers.size();
}
