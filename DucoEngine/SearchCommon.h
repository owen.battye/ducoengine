#ifndef __SEARCHCOMMON_H__
#define __SEARCHCOMMON_H__

namespace Duco
{

enum TSearchFieldId
{
    EAssociationName = 0,
    EAssociationId,
    ERingerId,
    ERingerName,
    ERingerPosition,
    EAllRingers,
    EStrapper,
    EConductorType,
    EConductorId,
    EConductorName,
    EComposer,
    ETowerId,
    ETowerName,
    ETowerBells,
    ERemovedTower,
    ERingName,
    EMultipleRings,
    ECountyField,
    ECity,
    ERingId,
    EMethodId,
    EMethodIdIncludingInSplicedSeries,
    EMethodIsSpliced,
    EMethodName,
    EFullMethodName,
    EMethodMultiStr,
    EMethodType,
    EMethodPN,
    EMethodDualOrder,
    EMethodSeriesId,
    EMethodSeriesStr,
    EMethodSeriesCount,
    EMethodSeriesPresent,
    EMethodSeriesCompleted,
    EWithdrawn,
    EChanges,
    EPealTime,
    EPealDate,
    ENoOfBells,
    EFootnotes,
    EBlankFootnote,
    ENonBlankFootnote,
    EForename,
    ESurname,
    EContainsNameChanges,
    ETenorWeight,
    ETenorKey,
    EDoveReference,
    ETowerbaseId,
    EBlankRWReference,
    EValidRWReference,
    EBellBoardReference,
    ENonBlankRingerReferences,
    EAllBlankRingerReferences,
    EPealBaseReference,
    ENotes,
    ENotValid,
    EDuplicate,
    EExtraBells,
    EGenderField,
    ETowerAka,
    ETowerContainsValidLocation, 
    EComposition_Name,
    ECompositionSnapStart,
    ECompositionSnapFinish,
    ECompositionId,
    ECompositionStr,
    EComposerId,
    ETrueCompositionAndEndsWithRounds,
    EUnpaidFees,
    EInconsistantFees,
    ELinkedTowerId,
    EHandbellTower,
    EWithLinkedTower,
    ELinkedAssociationId,
    EDoubleHanded,
    EEuropeOnly,
    EContainsPicture,
    ENonHumanRinger,
    EPictureId,
    ENotablePerformance,
    EAntiClockwise,
    ESimulatedSound,
    EDeceasedRingers,
    ELinkedRinger,

    EAndGroup,
    EOrGroup,
    ENot
};

enum TSearchType
{
    ELessThan = 0,
    ELessThanOrEqualTo,
    EEqualTo,
    EMoreThanOrEqualTo,
    EMoreThan,

    ENotEqualTo
};

enum class TFieldType
{
    ETimeField = 0,
    EStringField,
    EIdField,
    ENumberField,
    EBoolField,
    EDateField,
    EGroupField,

    EInvalidField, // This is a specific search for invalid objects.
    EDuplicateField
};

enum TUpdateFieldId
{
    EPeal_Composer,
    EPeal_Footnotes,
    EMethod_Name,
    EMethod_Type,
    ETower_Name,
    ETower_City
};

}

#endif //!__SEARCHCOMMON_H__
