#include "Gender.h"

#include "DatabaseReader.h"
#include "DatabaseWriter.h"

using namespace std;
using namespace Duco;

Gender::Gender()
{
    genderFlags.reset();
}

Gender::Gender(const Duco::Gender& other)
:   genderFlags(other.genderFlags)
{
}

Gender::~Gender()
{
}

DatabaseWriter&
Gender::Externalise(DatabaseWriter& writer) const
{
    std::wstring flagsAsString = genderFlags.to_string<wchar_t>();
    writer.WriteString(flagsAsString);
    return writer;
}

DatabaseReader&
Gender::Internalise(DatabaseReader& reader, unsigned int databaseVersion)
{
    if (databaseVersion >= 18)
    {
        std::wstring bitStr;
        reader.ReadString(bitStr);
        std::bitset<KGenderBitsetLength> newFlags (bitStr);
        genderFlags = newFlags;
    }
    else
    {
        genderFlags.reset();
    }
    return reader;
}

std::wstring
Gender::Str() const
{
    if (Male())
        return L"Male";
    else if (Female())
        return L"Female";

    return L"";
}

Gender&
Gender::operator=(const Gender& rhs)
{
    genderFlags = rhs.genderFlags;
    return *this;
}

bool
Gender::operator==(const Gender& rhs) const
{
    return genderFlags == rhs.genderFlags;
}

bool
Gender::operator!=(const Gender& rhs) const
{
    return genderFlags != rhs.genderFlags;
}