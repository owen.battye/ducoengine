#ifndef __PLACENOTATIONCROSSOBJECT_H__
#define __PLACENOTATIONCROSSOBJECT_H__

#include <string>
#include "PlaceNotationObject.h"

namespace Duco
{
class PlaceNotationCrossObject : public PlaceNotationObject
{
public:
    PlaceNotationCrossObject(TChangeType newType);
    PlaceNotationCrossObject(const PlaceNotationCrossObject& other);
    virtual ~PlaceNotationCrossObject();

    virtual PlaceNotationObject* Realloc() const;
    virtual bool RequiresSeperator() const;
    virtual unsigned int BestStartingLead() const;

    virtual Change ProcessRow(const Change& change) const;

private:

};

}

#endif //!__PLACENOTATIONCROSSOBJECT_H__
