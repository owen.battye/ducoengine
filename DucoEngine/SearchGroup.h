#ifndef __SEARCHGROUP_H__
#define __SEARCHGROUP_H__

#include "SearchArgument.h"
#include "DucoEngineCommon.h"

#include <vector>
#include <cstddef>

namespace Duco
{
    class Peal;
    class Tower;
    class Ringer;
    class Method;

class TSearchGroup : public TSearchArgument
{
public:
    DllExport explicit TSearchGroup(Duco::TSearchFieldId fieldId);
    DllExport ~TSearchGroup();
    Duco::TFieldType FieldType() const;

    virtual bool Match(const Duco::Peal& peal, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Tower& tower, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Ringer& ringer, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Method& method, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::MethodSeries& series, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Composition& object, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Association& object, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Picture& object, const Duco::RingingDatabase& database) const;

    DllExport size_t AddArgument(TSearchArgument* newArgument);
 
protected:
    std::vector<TSearchArgument*> searchArguments;
};

}

#endif //__SEARCHGROUP_H__

