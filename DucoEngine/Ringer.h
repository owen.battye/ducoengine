#ifndef __RINGER_H__
#define __RINGER_H__

#include <string>
#include <map>
#include <vector>
#include "DucoEngineCommon.h"
#include "DucoTypes.h"
#include "RingingObject.h"

namespace Duco
{
class DatabaseWriter;
class DatabaseReader;
class DatabaseSettings;
class PerformanceDate;
class Gender;
struct RingerNameChange;

class Ringer : public RingingObject
{
    friend class RingerDatabase;

public:
    DllExport Ringer(const Duco::ObjectId& newId, const std::wstring& newForename, const std::wstring& newSurname);
    DllExport Ringer(const Duco::ObjectId& newId, const Ringer& other);
    DllExport Ringer(const Ringer& other);
    Ringer(DatabaseReader& reader, unsigned int databaseVersion);
    DllExport Ringer();
    DllExport ~Ringer();

    Duco::TObjectType ObjectType() const;
    DllExport virtual bool Duplicate(const Duco::RingingObject& other, const Duco::RingingDatabase& database) const;

    // Accessors
    DllExport const std::wstring& Forename() const;
    DllExport const std::wstring& Surname() const;
    DllExport std::wstring OriginalName(bool lastNameFirst) const;
    DllExport std::wstring FullName(bool lastNameFirst) const;
    DllExport std::wstring FullName(const Duco::PerformanceDate& date, bool lastNameFirst) const;
    DllExport size_t NoOfNameChanges() const;
    DllExport const Duco::RingerNameChange& NameChange(size_t index) const;
    DllExport std::wstring NameChange(size_t index, bool lastNameFirst) const;
    DllExport bool Male() const;
    DllExport bool Female() const;
    DllExport bool NonHuman() const;
    DllExport const std::wstring& Notes() const;
    DllExport const std::wstring& PealbaseReference() const;
    DllExport const std::wstring& AlsoKnownAs() const;
    DllExport bool AnyMatch(const std::wstring& fullNameToMatchWith) const;
    DllExport unsigned int MatchProbability(const std::wstring& fullNameToMatchWith, bool partialMatchAllowed) const;
    DllExport unsigned int MatchProbability(const std::wstring& firstNamesToMatchWith, const std::wstring& surnameNameToMatchWith, bool partialMatchAllowed) const;
    DllExport bool IdenticalRinger(const std::wstring& firstNames, const std::wstring& surname) const;
    DllExport bool Deceased() const;
    DllExport bool DateOfDeath(PerformanceDate& dateOfDeath) const;

    // queries
    bool NameContainsSubstring(const DatabaseSettings& settings, const std::wstring& substring, bool subStr) const;
    bool AkaRequired(const Duco::PerformanceDate& pealDate, size_t& akaNo) const;
    bool ForenameContains(const std::wstring& substring, bool subStr, bool includeAka) const;
    bool SurnameContains(const std::wstring& substring, bool subStr, bool includeAka) const;

    // Settors
    DllExport void SetForename(unsigned int number, const std::wstring& newName);
    DllExport void SetSurname(unsigned int number, const std::wstring& newName);
    DllExport void SetDateChanged(unsigned int number, const Duco::PerformanceDate& newDtae);
    DllExport void AddNameChange(const Ringer& ringer, const Duco::PerformanceDate& dateChanged);
    DllExport void AddNameChange(const std::wstring& newForname, const std::wstring& newSurname, const Duco::PerformanceDate& dateChanged);
    DllExport void InsertNameChange(const std::wstring& newForname, const std::wstring& newSurname, const Duco::PerformanceDate& dateChanged);
    DllExport void InsertNameChange(const Duco::Ringer& ringer, const Duco::PerformanceDate& dateChanged);
    DllExport void DeleteNameChange(size_t index);
    DllExport void SetMale();
    DllExport void SetFemale();
    DllExport void SetNonHuman(bool newSetNonHuman);
    DllExport void SetNotes(const std::wstring& newNotes);
    DllExport void SetPealbaseReference(const std::wstring& newRef);
    DllExport void SetAlsoKnownAs(const std::wstring& newName);
    bool CapitaliseField();
    DllExport void SetDeceased(bool deceased);
    DllExport void SetDateOfDeath(const PerformanceDate& dateOfDeath);


    DllExport Ringer& operator=(const Duco::Ringer& other);
    DllExport bool operator==(const Duco::RingingObject& other) const;
    DllExport bool operator!=(const Duco::RingingObject& other) const;

    DllExport bool Valid(const RingingDatabase& database, bool warningFatal, bool clearBeforeStart) const;
    void SetError(TRingerErrorCodes code);
    DllExport std::wstring ErrorString(const Duco::DatabaseSettings& /*settings*/, bool showWarnings) const;

    std::wostream& Print(std::wostream& stream, const Duco::PerformanceDate& date, const RingingDatabase& database) const;
    std::wostream& Print(std::wostream& stream, const RingingDatabase& database) const;
    DatabaseReader& Internalise(DatabaseReader& reader, unsigned int databaseVersion);
    DatabaseWriter& Externalise(DatabaseWriter& writer) const;
    virtual void ExportToXml(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& parent, bool exportDucoObjects) const;
    void ExportToCsv(std::wofstream& file) const;
    
protected:
    void AddNameChange(RingerNameChange*const& nameChange);
    std::wstring FullNameForSort(bool lastNameFirst) const;
    static unsigned int MatchProbability(const std::wstring& firstNames, const std::wstring& lastName, const std::wstring& firstNamesToMatchWith, const std::wstring& surnamesNamesToMatchWith, const std::wstring& initialsToMatchWith, bool partialMatchAllowed);

    // Accessors
    const std::wstring& MostRecentForename() const;
    const std::wstring& MostRecentSurname() const;

private:
    void DeleteAllNameChanges();

private:
    std::wstring                     surname;
    std::wstring                     forename;
    Duco::Gender*                    gender;
    std::wstring                     notes;
    bool                             nonHuman;

    std::wstring                     alsoKnownAs;
    std::wstring                     pealbaseRingerId;

    std::vector<RingerNameChange*>   nameChanges;

    bool                             deceased;
    Duco::PerformanceDate*           dateOfDeath;
};

} // end namespace Duco

#endif //!__RINGER_H__
