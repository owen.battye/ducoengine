#ifndef __SEARCHFIELDDATEARGUMENT_H__
#define __SEARCHFIELDDATEARGUMENT_H__

#include "SearchArgument.h"
#include "PerformanceDate.h"

namespace Duco
{
    class Peal;
    class Tower;

class TSearchFieldDateArgument : public TSearchArgument
{
public:
    DllExport explicit TSearchFieldDateArgument(Duco::TSearchFieldId fieldId, const Duco::PerformanceDate& newFieldValue, Duco::TSearchType newSearchType = EEqualTo);
    virtual bool Match(const Duco::Peal& peal, const Duco::RingingDatabase& database) const;
    Duco::TFieldType FieldType() const;
 
protected:
    bool CompareDate(const Duco::PerformanceDate& pealDate) const;

protected:
    const Duco::PerformanceDate fieldValue;
    const Duco::TSearchType     searchType;
};

}
#endif //__SEARCHFIELDDATEARGUMENT_H__
