#ifndef __METHOD_H__
#define __METHOD_H__

#include <string>
#include <map>
#include "RingingObject.h"

namespace Duco
{
    class DatabaseSettings;
    class DatabaseWriter;
    class DatabaseReader;
    class MethodDatabase;
    class ExternalMethod;

class Method : public RingingObject
{
public:
    DllExport Method(const Duco::ObjectId& newId, unsigned int newNoOfBells, const std::wstring& newName, const std::wstring& newType, const std::wstring& newNotation, bool dualOrder = false);
    DllExport Method(const Method& other);
    DllExport Method(const Duco::ExternalMethod& other);
    DllExport Method(const Duco::ObjectId& newId, const Method& other);
    DllExport Method(DatabaseReader& reader, unsigned int databaseVersion);
    DllExport Method();
    DllExport ~Method();

    // from RingingObject
    DllExport virtual TObjectType ObjectType() const;
    DllExport virtual bool Duplicate(const Duco::RingingObject& other, const Duco::RingingDatabase& database) const;
    std::wstring FullNameForSort(bool unusedSetting) const;

    //Accessors
    inline const std::wstring& Name() const;
    inline const std::wstring& Type() const;
    DllExport std::wstring ShortName(const RingingDatabase& database) const;
    DllExport std::wstring ShortName(const MethodDatabase& database) const;
	DllExport std::wstring FullName(const RingingDatabase& database) const;
    DllExport std::wstring FullName(const MethodDatabase& database) const;
	DllExport std::wstring NameForWeb(const RingingDatabase& database) const;
    DllExport std::wstring NameForWeb(const MethodDatabase& database) const;
    inline const std::wstring& PlaceNotation() const;
    DllExport bool BobPlaceNotation(std::wstring& returnNotation, size_t index = 0) const;
    DllExport bool SinglePlaceNotation(std::wstring& returnNotation, size_t index = 0) const;
    DllExport bool ContainsSingleNotation(size_t index = 0) const;
    DllExport bool ContainsBobNotation(size_t index = 0) const;
    DllExport bool ContainsBobOrSingleNotation(size_t index = 0) const;
    DllExport bool ContainsBobAndSingleNotation(size_t index = 0) const;
    inline const std::wstring& PealbaseLink() const;
    inline unsigned int Order() const;
    inline bool DualOrder() const;
    inline bool Spliced() const;
	DllExport std::wstring OrderName(const RingingDatabase& database, bool giveDualOrderName = false) const;
    DllExport std::wstring OrderName(const MethodDatabase& database, bool giveDualOrderName = false) const;
    DllExport static std::wstring AnotherOrderName(const RingingDatabase& database, unsigned int numberOfBells, bool dualOrder);
    DllExport static std::wstring AnotherOrderName(const MethodDatabase& database, unsigned int numberOfBells, bool dualOrder);

    DllExport int NoOfLeads() const;
    DllExport int HalfWay() const;
    DllExport bool DrawTreble(const Duco::DatabaseSettings& settings) const;
    inline unsigned int PrintFromBell() const;
    unsigned int MatchProbability(const std::wstring& otherMethodName, const std::wstring& otherMethodNameOptionalText, const std::wstring& otherMethodType, unsigned int otherNoOfBells, bool allowTypeSubstr) const;
    bool Match(const std::wstring& otherMethodName, const std::wstring& otherMethodType, unsigned int otherNoOfBells) const;

    //Settors
    DllExport void SetName(const std::wstring& newName);
    inline void SetType(const std::wstring& newType);
    DllExport bool SetPlaceNotation(const std::wstring& newNotation);
    DllExport bool SetBobPlaceNotation(const std::wstring& newNotation, size_t index = 0);
    DllExport bool SetSinglePlaceNotation(const std::wstring& newNotation, size_t index = 0);
    DllExport bool SetPlaceNotation(const std::wstring& newNotation, std::wstring& savedNotation);
    DllExport bool SetBobPlaceNotation(const std::wstring& newNotation, size_t index, std::wstring& savedNotation);
    DllExport bool SetSinglePlaceNotation(const std::wstring& newNotation, size_t index, std::wstring& savedNotation);
    inline void SetPealbaseLink(const std::wstring& newLink);
    inline void SetOrder(unsigned int newOrder);
    inline void SetDualOrder(bool dualOrder);
    inline void SetNoOfLeads(int newNoOfLeads);
    inline void SetPrintFromBell(unsigned int newBell);
    void SetError(TMethodErrorCodes code);
    bool CapitaliseField(bool methodName, bool methodType);

    // Operators
    DllExport bool operator==(const Duco::RingingObject& other) const;
    DllExport bool operator!=(const Duco::RingingObject& other) const;
    DllExport bool operator!=(const Duco::Method& rhs) const;
    DllExport bool operator==(const Duco::Method& rhs) const;
    DllExport Method& operator=(const Duco::Method& other);

    // Validity checking
    DllExport bool Valid(const MethodDatabase& methodDb, const Duco::DatabaseSettings& settings, bool warningFatal, bool clearBeforeStart) const;
    DllExport bool Valid(const RingingDatabase& ringingDb, bool warningFatal, bool clearBeforeStart) const;
    DllExport bool ValidNotation(bool forProve, bool enableWarnings, const Duco::DatabaseSettings& settings, bool clearBeforeStart) const;
    DllExport std::wstring ErrorString(const Duco::DatabaseSettings& settings, bool showWarnings) const;

    //Queries
    bool IsPrinciple() const;

    // I/O
    DllExport std::wostream& Print(std::wostream& stream, const RingingDatabase& database) const;
    DllExport DatabaseWriter& Externalise(DatabaseWriter& writer) const;
    std::wofstream& ExportToRtf(std::wofstream& outputFile, const RingingDatabase& database) const;
#ifndef __ANDROID__
    virtual void ExportToXml(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& parent, bool exportDucoObjects) const;
#endif
    void ExportToCsv(std::wofstream& file, const MethodDatabase& database) const;

protected:
    DatabaseReader& Internalise(DatabaseReader& reader, unsigned int databaseVersion);
    bool CheckNotation(const std::wstring& newNotation, std::wstring& savePos) const;

private:
    unsigned int noOfBells;
    bool         dualNoOfBells;
    std::wstring  name;
    std::wstring  type;
    std::wstring  placeNotation;
    std::map<size_t, std::wstring> bobLEPlaceNotations;
    std::map<size_t, std::wstring> singleLEPlaceNotations;
    int          noOfLeads;
    unsigned int printFromBell;
    std::wstring  pealbaseLink;

    // unstored data
    bool         spliced;
};

const std::wstring&
Method::Name() const
{
    return name;
}

const std::wstring&
Method::Type() const
{
    return type;
}

const std::wstring& 
Method::PlaceNotation() const
{
    return placeNotation;
}

unsigned int
Method::Order() const
{
    return noOfBells;
}

bool
Method::DualOrder() const
{
    return dualNoOfBells;
}

void
Method::SetType(const std::wstring& newType) 
{
    type = newType;
}

void
Method::SetOrder(unsigned int newOrder) 
{
    noOfBells = newOrder;
}

void
Method::SetDualOrder(bool dualOrder)
{
    dualNoOfBells = dualOrder;
}

void
Method::SetNoOfLeads(int newNoOfLeads)
{
    noOfLeads = newNoOfLeads;
}

unsigned int
Method::PrintFromBell() const
{
    return printFromBell;
}

void
Method::SetPrintFromBell(unsigned int newBell)
{
    printFromBell = newBell;
}

bool
Method::Spliced() const
{
    return spliced;
}

const std::wstring&
Method::PealbaseLink() const
{
    return pealbaseLink;
}

void
Method::SetPealbaseLink(const std::wstring& newLink)
{
    pealbaseLink = newLink;
}

} // end namespace Duco

#endif // __METHOD_H__
