#ifndef __UPDATEARGUMENT_H__
#define __UPDATEARGUMENT_H__

#include "SearchCommon.h"
#include "DucoEngineCommon.h"

namespace Duco
{
    class Method;
    class Peal;
    class Tower;

class TUpdateArgument
{
public:
    //virtual bool Update(Duco::MethodSeries& object) const = 0;
    virtual bool Update(Duco::Method& object) const = 0;
    virtual bool Update(Duco::Peal& object) const = 0;
    //virtual bool Update(Duco::Ringer& object) const = 0;
    virtual bool Update(Duco::Tower& object) const = 0;
    //virtual bool Update(Duco::Composition& object) const = 0;
    //virtual bool Update(Duco::Association& object) const = 0;

    virtual Duco::TFieldType FieldType() const = 0;
    DllExport virtual const TUpdateFieldId FieldId() const;
 
    DllExport virtual ~TUpdateArgument();

protected:
    TUpdateArgument(Duco::TUpdateFieldId newFieldId);

    const TUpdateFieldId fieldId;
};
 
}
#endif //__UPDATEARGUMENT_H__

