#ifndef __IMPORTEXPORTPROGRESSCALLBACK_H__
#define __IMPORTEXPORTPROGRESSCALLBACK_H__

#include "ObjectType.h"
#include "ObjectId.h"

namespace Duco
{
class ImportExportProgressCallback
    {
    public:
        virtual void InitialisingImportExport(bool internalising, unsigned int versionNumber, size_t totalNumberOfObjects) = 0;
        virtual void ObjectProcessed(bool internalising, Duco::TObjectType objectType, Duco::ObjectId objectId, int totalPercentage, size_t numberInThisSubDatabase) = 0;
        virtual void ImportExportComplete(bool internalising) = 0;
        virtual void ImportExportFailed(bool internalising, int errorCode) = 0;
    };
} // end namespace Duco

#endif //! __IMPORTEXPORTPROGRESSCALLBACK_H__
