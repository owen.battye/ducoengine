#include "DatabaseRtfExporter.h"

#include "Peal.h"
#include "ImportExportProgressCallback.h"
#include "PealDatabase.h"
#include "RingingDatabase.h"

using namespace std;
using namespace Duco;

DatabaseRtfExporter::DatabaseRtfExporter(const RingingDatabase& theDatabase, std::set<Duco::ObjectId> newPealIds)
    :   database (theDatabase), pealIds(newPealIds)
{

}

DatabaseRtfExporter::~DatabaseRtfExporter()
{

}

void 
DatabaseRtfExporter::Export(const char* fileName, Duco::ImportExportProgressCallback* newCallback)
{
    wofstream* outputFile = new wofstream(fileName, ios_base::trunc);

    newCallback->InitialisingImportExport(false, 0, pealIds.size());

    *outputFile << "{\\rtf1\\ansi" << std::endl;

    size_t total = pealIds.size();
    size_t count (0);
    try
    {
        std::set<Duco::ObjectId>::const_iterator pealIt = pealIds.begin();
        while (pealIt != pealIds.end())
        {
            const Peal* const thePeal = database.PealsDatabase().FindPeal(*pealIt);
            if (thePeal != NULL)
            {
                thePeal->ExportToRtf(*outputFile, database) << KRtfLineEnd << std::endl;
            }
            ++count;
            float percentageComplete = (count / (float)total) * 100;
            if (thePeal != NULL)
            {
                newCallback->ObjectProcessed(false, TObjectType::EPeal, thePeal->Id(), (int)percentageComplete, (size_t)count);
            }
            ++pealIt;
            if (pealIt != pealIds.end())
            {
                // Add gap between peals
                (*outputFile) << KRtfLineEnd << std::endl << KRtfLineEnd << std::endl << KRtfLineEnd << std::endl;
            }
        }
    }
    catch (...)
    {

    }
    *outputFile << "}";
    newCallback->ImportExportComplete(false);
    delete outputFile;
}
