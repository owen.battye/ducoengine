#include "MethodSeriesDatabase.h"

#include <algorithm>
#include "MethodSeries.h"
#include "ImportExportProgressCallback.h"
#include "PealDatabase.h"
#include "DatabaseSettings.h"
#include "RenumberProgressCallback.h"
#include "RingingDatabase.h"
#include "SearchArgument.h"
#include "SearchValidationArgument.h"
#include "StatisticFilters.h"
#include "DucoEngineUtils.h"
#include <xercesc\dom\DOMElement.hpp>

using namespace Duco;
using namespace std;

XERCES_CPP_NAMESPACE_USE

MethodSeriesDatabase::MethodSeriesDatabase()
:   RingingObjectDatabase(TObjectType::EMethodSeries)
{
}

MethodSeriesDatabase::~MethodSeriesDatabase()
{

}

Duco::ObjectId
MethodSeriesDatabase::AddObject(const Duco::RingingObject& newObject)
{
    Duco::ObjectId addedId;

    if (newObject.ObjectType() == TObjectType::EMethodSeries)
    {
        const Duco::MethodSeries& newMethodSeries = static_cast<const Duco::MethodSeries&>(newObject);

        Duco::RingingObject* newMethodSeriesWithId = new Duco::MethodSeries(newMethodSeries);
        if (AddOwnedObject(newMethodSeriesWithId))
        {
            addedId = newMethodSeriesWithId->Id();
        }
        // object is deleted by RingingObjectDatabase::AddObject if not added.
    }
    return addedId;
}

bool
MethodSeriesDatabase::SaveUpdatedObject(Duco::RingingObject& lhs, const Duco::RingingObject& rhs)
{
    Duco::TObjectType rhsType = rhs.ObjectType();
    if (lhs.ObjectType() == rhsType && rhsType == TObjectType::EMethodSeries)
    {
        Duco::MethodSeries& lhsObject = static_cast<Duco::MethodSeries&>(lhs);
        const Duco::MethodSeries& rhsObject = static_cast<const Duco::MethodSeries&>(rhs);
        lhsObject = rhsObject;
        return true;
    }
    return false;
}

#ifndef __ANDROID__
void
MethodSeriesDatabase::ExternaliseToXml(Duco::ImportExportProgressCallback* newCallback, XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, bool exportDucoObjects) const
{
    size_t count = 0;
    DOMElement* rootElem = outputFile.getDocumentElement();
    DOMElement* newSeries = outputFile.createElement(XMLStrL("MethodSeries"));
    rootElem->appendChild(newSeries);

    DUCO_OBJECTS_CONTAINER::const_iterator seriesIt = listOfObjects.begin();
    while (seriesIt != listOfObjects.end())
    {
        const Duco::MethodSeries* theSeries = static_cast<const Duco::MethodSeries*>(seriesIt->second);
        theSeries->ExportToXml(outputFile, *newSeries, exportDucoObjects);

        newCallback->ObjectProcessed(false, TObjectType::EMethodSeries, seriesIt->second->Id(), 0, ++count);
        ++seriesIt;
    }
}
#endif

bool
MethodSeriesDatabase::Internalise(DatabaseReader& reader, Duco::ImportExportProgressCallback* callback, int databaseVersionNumber, size_t noOfObjectsInThisDatabase, size_t totalObjects, size_t objectsProcessedSoFar)
{
    size_t totalProcessed = objectsProcessedSoFar;
    size_t count = 0;

    while (noOfObjectsInThisDatabase-- > 0)
    {
        ++totalProcessed;
        ++count;
        float percentageComplete = ((float)totalProcessed / (float)totalObjects) * 100;
        Duco::RingingObject* newMethodSeries = new Duco::MethodSeries(reader, databaseVersionNumber);
        if (newMethodSeries == NULL)
            return false;

        AddOwnedObject(newMethodSeries);
        if (callback != NULL)
        {
            callback->ObjectProcessed(true, TObjectType::EMethodSeries, newMethodSeries->Id(), (int)percentageComplete, count);
        }
    }
    return true;
}

bool 
MethodSeriesDatabase::FindMethodInMethodSeries(const Duco::ObjectId& methodId, Duco::ObjectId& seriesId) const
{
    DUCO_OBJECTS_CONTAINER::const_iterator it = listOfObjects.begin();
    seriesId.ClearId();
    while (!seriesId.ValidId() && it != listOfObjects.end())
    {
        const Duco::MethodSeries* const theSeries = static_cast<const Duco::MethodSeries* const >(it->second);

        if (theSeries->ContainsMethod(methodId))
        {
            seriesId = it->first;
        }
        ++it;
    }
    return seriesId.ValidId();
}

bool
MethodSeriesDatabase::RenumberMethodsInMethodSeries(const std::map<Duco::ObjectId, Duco::ObjectId>& newMethodIds, Duco::RenumberProgressCallback* callback)
{
    bool changesMade (false);
    DUCO_OBJECTS_CONTAINER::iterator it = listOfObjects.begin();
    size_t count = 0;
    while (it != listOfObjects.end())
    {
        Duco::MethodSeries* theSeries = static_cast<Duco::MethodSeries*>(it->second);
        if (theSeries->ReplaceMethods(newMethodIds))
        {
            changesMade = true;
            SetDataChanged();
        }
        callback->RenumberStep(++count, listOfObjects.size());
        ++it;
    }
    return changesMade;
}

void
MethodSeriesDatabase::RemoveUsedIds(std::set<Duco::ObjectId>& unusedMethodsIds) const
{
    // Remove methods from list where the method is in a series
    std::set<Duco::ObjectId>::const_iterator it = unusedMethodsIds.begin();
    while (it != unusedMethodsIds.end())
    {
        Duco::ObjectId seriesId;
        if (FindMethodInMethodSeries(*it, seriesId))
        {
            Duco::ObjectId oldMethodId = *it;
            ++it;
            unusedMethodsIds.erase(oldMethodId);
        }
        else
            ++it;
    }
}

bool
MethodSeriesDatabase::RebuildObjects(Duco::RenumberProgressCallback* callback, Duco::RingingDatabase& database)
{
    if (listOfObjects.size() <= 0)
        return false;

    callback->RenumberInitialised(RenumberProgressCallback::EReindexMethodSeries);
    std::map<Duco::ObjectId, Duco::ObjectId> newSeriesIds;
    {
        if (database.Settings().AlphabeticReordering())
            GetNewObjectIdsByAlphabetic(newSeriesIds,database.Settings().LastNameFirst());
        else
        {
            StatisticFilters filters(database);
            std::map<Duco::ObjectId, Duco::PealLengthInfo> methodSeriesPealCounts;
            database.PealsDatabase().GetMethodSeriesPealCount(filters, methodSeriesPealCounts);
            GetNewIdsByPealCountAndAlphabetic(methodSeriesPealCounts, newSeriesIds, database.Settings().LastNameFirst());
        }
    }

    // Renumber all methods first.
    callback->RenumberInitialised(RenumberProgressCallback::ERenumberMethodsSeries);
    bool changesMade = RenumberObjects(newSeriesIds, callback);

    // Renumber methodids in Peals
    callback->RenumberInitialised(RenumberProgressCallback::ERebuildMethodsSeries);
    changesMade |= database.PealsDatabase().RenumberMethodSeriesInPeals(newSeriesIds, callback);
    return changesMade;

}

bool
MethodSeriesDatabase::MatchObject(const Duco::RingingObject& object, const Duco::TSearchArgument& searchArg, const Duco::RingingDatabase& database) const
{
    if (object.ObjectType() != TObjectType::EMethodSeries)
        return false;
    return searchArg.Match(static_cast<const Duco::MethodSeries&>(object), database);
}

bool
MethodSeriesDatabase::ValidateObject(const Duco::RingingObject& object, const Duco::TSearchValidationArgument& searchArg, const std::set<Duco::ObjectId>& idsToCheckAgainst, const Duco::RingingDatabase& database) const
{
    if (object.ObjectType() != TObjectType::EMethodSeries)
        return false;
    return searchArg.Match(static_cast<const Duco::MethodSeries&>(object), idsToCheckAgainst, database);
}

const MethodSeries* const
MethodSeriesDatabase::FindMethodSeries(const Duco::ObjectId& seriesId, bool setIndex) const
{
    const Duco::RingingObject* const objectPtr = FindObject(seriesId, setIndex);

    return static_cast<const MethodSeries* const>(objectPtr);
}

void
MethodSeriesDatabase::GetMethodSeriesByNoOfBells(std::vector<Duco::ObjectId>& methodIds, unsigned int noOfBells) const
{
    methodIds.clear();

    DUCO_OBJECTS_CONTAINER::const_iterator it = listOfObjects.begin();
    while (it != listOfObjects.end())
    {
        const MethodSeries* const theMethodSeries = static_cast<const MethodSeries* const>(it->second);

        if (theMethodSeries != NULL)
        {
            if (noOfBells == -1 || DucoEngineUtils::NoOfBellsMatch(theMethodSeries->NoOfBells(),noOfBells))
            {
                // method must be on the number of bells or the number of bells - 1 for odd bell methods
                methodIds.push_back(theMethodSeries->Id());
            }
        }
        ++it;
    }
    std::sort(methodIds.begin(), methodIds.end());
}

void
MethodSeriesDatabase::AddMissingSeries(std::map<Duco::ObjectId, Duco::PealLengthInfo>& sortedMethodSeriesIds, unsigned int noOfBells) const
{
    DUCO_OBJECTS_CONTAINER::const_iterator methodSeriesIt = listOfObjects.begin();
    while (methodSeriesIt != listOfObjects.end())
    {
        const MethodSeries* nextMethodSeries = static_cast<const MethodSeries*>(methodSeriesIt->second);

        if (noOfBells == -1 || nextMethodSeries->NoOfBells() == noOfBells)
        {
            std::map<Duco::ObjectId, Duco::PealLengthInfo>::const_iterator alreadyAdded = sortedMethodSeriesIds.find(methodSeriesIt->first);
            if (alreadyAdded == sortedMethodSeriesIds.end())
            {
                Duco::PealLengthInfo newInfo;
                pair<Duco::ObjectId, Duco::PealLengthInfo> newObject(methodSeriesIt->first, newInfo);
                sortedMethodSeriesIds.insert(newObject);
            }
        }

        ++methodSeriesIt;
    }
}

bool
MethodSeriesDatabase::FindMethodSeries(std::set<Duco::MethodSeries*>& series, unsigned int order) const
{
    series.clear();
    DUCO_OBJECTS_CONTAINER::const_iterator it = listOfObjects.begin();

    while (it != listOfObjects.end())
    {
        Duco::MethodSeries* nextMethodSeries = static_cast<Duco::MethodSeries*>(it->second);
        if (nextMethodSeries->NoOfBells() == order)
        {
            series.insert(nextMethodSeries);
        }
        ++it;
    }
    return !series.empty();
}

bool
MethodSeriesDatabase::UpdateObjectField(const Duco::ObjectId&, const Duco::TUpdateArgument&)
{
    return false;
}
