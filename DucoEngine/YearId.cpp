#include "YearId.h"

using namespace Duco;

YearId::YearId(unsigned int newYearId, const ObjectId& newId)
    : year(newYearId)
{
    ids.insert(newId);
}

bool
YearId::operator<(const YearId& rhs) const
{
    return year < rhs.year;
}

void
YearId::Add(const ObjectId& newId)
{
    ids.insert(newId);
}

size_t
YearId::Count() const
{
    return ids.size();
}

namespace Duco
{
    bool
    operator<(const YearId& lhs, const YearId& rhs)
    {
        return lhs.operator<(rhs);
    }
}
