#include "PlaceNotationObject.h"

using namespace Duco;

PlaceNotationObject::PlaceNotationObject(const std::wstring& newStringRepresentation, TChangeType newType)
:   stringRepresentation(newStringRepresentation), changeType(newType)
{
}

PlaceNotationObject::PlaceNotationObject(const PlaceNotationObject& other)
    :   stringRepresentation(other.stringRepresentation), changeType(other.changeType)
{

}

PlaceNotationObject::~PlaceNotationObject()
{
}


