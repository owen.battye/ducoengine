#ifndef __RINGERPEALDATES_H__
#define __RINGERPEALDATES_H__

#include "DucoEngineCommon.h"
#include "PerformanceDate.h"
#include "PealLengthInfo.h"
#include "ObjectId.h"

namespace Duco
{
    class Peal;

    class RingerPealDates : Duco::PealLengthInfo
    {
    public:
        DllExport RingerPealDates(const Duco::ObjectId& ringerId, const Duco::PerformanceDate& dateOfNewPeal);
        const Duco::ObjectId ringerId;
        const Duco::PerformanceDate dateOfNewPeal;
        DllExport bool operator<(const Duco::RingerPealDates& rhs) const;
        DllExport bool operator>(const Duco::RingerPealDates& rhs) const;

        DllExport void AddPeal(const Duco::Peal& nextpeal);
        DllExport std::wstring Description() const;
        DllExport bool BeforeAndAfter() const;

    private:
        Duco::PerformanceDate nearestDateBefore;
        Duco::ObjectId nearestIdBefore;
        Duco::PerformanceDate nearestDateAfter;
        Duco::ObjectId nearestIdAfter;
    };
}

#endif //!__RINGERPEALDATES_H__
