#include "SearchFieldDateArgument.h"
#include "Peal.h"
#include "Tower.h"

using namespace Duco;

TSearchFieldDateArgument::TSearchFieldDateArgument(Duco::TSearchFieldId newFieldId, const Duco::PerformanceDate& newFieldValue, Duco::TSearchType newSearchType)
:   TSearchArgument(newFieldId), fieldValue(newFieldValue), searchType(newSearchType)
{
}

bool
TSearchFieldDateArgument::Match(const Duco::Peal& peal, const Duco::RingingDatabase& database) const
{
    switch (fieldId)
    {
        case Duco::EPealDate:
            return CompareDate(peal.Date());

        default:
            return false;
    }
}

Duco::TFieldType
TSearchFieldDateArgument::FieldType() const
{
    return TFieldType::EDateField;
}

bool TSearchFieldDateArgument::CompareDate(const Duco::PerformanceDate& pealDate) const
{
    bool returnVal (false);
    switch (searchType)
    {
    case ELessThan:
        returnVal = pealDate < fieldValue;
        break;
    case ELessThanOrEqualTo:
        returnVal = pealDate <= fieldValue;
        break;
    case EEqualTo:
        returnVal = pealDate == fieldValue;
        break;
    case EMoreThanOrEqualTo:
        returnVal = pealDate >= fieldValue;
        break;
    case EMoreThan:
        returnVal = pealDate > fieldValue;
        break;
    }

    return returnVal;
}
