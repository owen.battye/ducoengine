#ifndef __OBJECTID_H__
#define __OBJECTID_H__

#include "DucoEngineCommon.h"
#include <ostream>
#include <istream>
#include <limits>

#define KNoId UINT_MAX

namespace Duco
{
class ObjectId
{
public:
    inline ObjectId();
    inline ObjectId(const ObjectId& rhs);
    DllExport ObjectId(const std::wstring& rhs);
    DllExport ObjectId(size_t newId);
    inline ~ObjectId();

    DllExport Duco::ObjectId& operator=(const Duco::ObjectId& rhs);
    DllExport bool operator==(const Duco::ObjectId& rhs) const;
    DllExport bool operator!=(const Duco::ObjectId& rhs) const;

    DllExport bool operator<(const Duco::ObjectId& rhs) const;
    DllExport bool operator>(const Duco::ObjectId& rhs) const;
    DllExport bool operator<=(const Duco::ObjectId& rhs) const;

    DllExport Duco::ObjectId& operator++();
    DllExport Duco::ObjectId& operator--();
    DllExport Duco::ObjectId operator+(size_t num) const;
    DllExport Duco::ObjectId operator-(size_t num) const;

    DllExport std::wostream& operator<<(std::wostream& strm) const;
    DllExport std::wistream& operator>>(std::wistream& strm);
    DllExport std::wstring Str() const;

    inline size_t Id() const;
    DllExport bool ValidId() const;
    DllExport void ClearId();

protected:
    void ConvertFrom32Bit();

protected:
    size_t id;
};

inline std::wostream& operator<<(std::wostream& strm, const Duco::ObjectId& id)
{
    id.operator<<(strm);
    return strm;
}

inline std::wistream& operator>>(std::wistream& strm, ObjectId& id)
{
    id.operator>>(strm);
    return strm;
}

size_t
ObjectId::Id() const
{
    return id;
}

ObjectId::ObjectId()
: id(KNoId)
{

}

ObjectId::ObjectId(const ObjectId& rhs)
: id(rhs.id)
{

}

ObjectId::~ObjectId()
{
    id = KNoId;
}
} // end namespace Duco

#endif // !__OBJECTID_H__
