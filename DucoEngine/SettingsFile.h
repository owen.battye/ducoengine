#ifndef __SETTINGSFILE_H__
#define __SETTINGSFILE_H__

#include <string>
#include <map>
#include "DucoEngineCommon.h"

namespace Duco
{

class SettingsFile
{
public:
    DllExport SettingsFile(const SettingsFile& other);
    DllExport SettingsFile(const std::string& installDir, const std::string& fileName);
    DllExport ~SettingsFile();

    DllExport bool Setting(const std::wstring& settingId, int& settingValue) const;
    DllExport bool Setting(const std::wstring& settingId, std::wstring& settingValue) const;
    DllExport bool Setting(const std::wstring& settingId, bool& settingValue) const;

    DllExport bool Set(const std::wstring& settingId, int settingValue);
    DllExport bool Set(const std::wstring& settingId, const std::wstring& settingValue);

    inline void ClearSettingsChanged();
    DllExport void ClearAllSettings();
    DllExport bool Save() const;
    DllExport void SetFilename(const std::string& installDir, const std::string& fileName);
    inline unsigned int ExpectedNoOfFields() const;
    inline size_t NoOfSettings() const;

    DllExport bool operator==(const SettingsFile& rhs) const;
    DllExport bool operator!=(const SettingsFile& rhs) const;

protected:
    void ProcessLine(const std::wstring& fileLine);

private:
    bool                                settingsChanged;
    std::string                         fileName;
    mutable unsigned int                expectedNoOfFields;

    std::map<std::wstring,std::wstring>   strSettings;
};

unsigned int
SettingsFile::ExpectedNoOfFields() const
{
    return expectedNoOfFields;
}

size_t
SettingsFile::NoOfSettings() const
{
    return strSettings.size();
}

void
SettingsFile::ClearSettingsChanged()
{
    settingsChanged = false;
}

}

#endif //!__SETTINGSFILE_H__
