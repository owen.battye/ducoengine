#include "SearchFieldIdArgument.h"

#include "Peal.h"
#include "Ringer.h"
#include "RingingDatabase.h"
#include "Tower.h"
#include "Picture.h"
#include "MethodSeriesDatabase.h"
#include "MethodSeries.h"
#include "Association.h"
#include "Composition.h"

using namespace Duco;

TSearchFieldIdArgument::TSearchFieldIdArgument(Duco::TSearchFieldId newFieldId, const Duco::ObjectId& newFieldValue, Duco::TSearchType newSearchType)
: TSearchArgument(newFieldId), fieldValue(newFieldValue), searchType(newSearchType)
{

}

Duco::TFieldType TSearchFieldIdArgument::FieldType() const
{
    return TFieldType::EIdField;
}

bool
TSearchFieldIdArgument::Match(const Duco::MethodSeries& object, const Duco::RingingDatabase& database) const
{
    switch (fieldId)
    {
    case EMethodId:
        return object.ContainsMethod(fieldValue);

    default:
        return false;
    }
}

bool
TSearchFieldIdArgument::Match(const Duco::Tower& object, const Duco::RingingDatabase& database) const
{
    switch (fieldId)
    {
    case ELinkedTowerId:
        return object.LinkedTowerId() == fieldValue;

    case EPictureId:
        return object.PictureId() == fieldValue;

    default:
        break;
    }
    return false;
}

bool
TSearchFieldIdArgument::Match(const Duco::Ringer& object, const Duco::RingingDatabase& database) const
{
    switch (fieldId)
    {
    case EAllRingers:
        return true;
    case ERingerId:
        return object.Id() == fieldValue;

    default:
        break;
    }
    return false;
}

bool
TSearchFieldIdArgument::Match(const Duco::Peal& peal, const Duco::RingingDatabase& database) const
{
    switch (fieldId)
    {
    case Duco::EAssociationId:
        return peal.AssociationId() == fieldValue;

    case Duco::EMethodId:
        return peal.MethodId() == fieldValue;

    case Duco::EMethodIdIncludingInSplicedSeries:
        return (peal.MethodId() == fieldValue) || CheckSeriesContainsMethod(peal, database);

    case Duco::EMethodSeriesId:
        return peal.SeriesId() == fieldValue;

    case ERingerId:
        return CheckRingerIds(peal);

    case ETowerId:
        return peal.TowerId() == fieldValue;

    case ERingId:
        return peal.RingId() == fieldValue;

    case EConductorId:
        return peal.ContainsConductor(fieldValue);

    case EStrapper:
        return CheckStrapperId(peal);

    case ECompositionId:
        return peal.CompositionId() == fieldValue;

    case EPictureId:
        return peal.PictureId() == fieldValue;

    default:
        return false;
    }
}

bool
TSearchFieldIdArgument::Match(const Duco::Composition& object, const Duco::RingingDatabase& database) const
{
    switch (fieldId)
    {
    case EComposerId:
        return object.ComposerId() == fieldValue;

    case EMethodId:
        return object.MethodId() == fieldValue;

    default:
        return false;
    }
}

bool
TSearchFieldIdArgument::Match(const Duco::Association& object, const Duco::RingingDatabase& database) const
{
    switch (fieldId)
    {
    case Duco::EAssociationId:
        return object.Id() == fieldValue;

    case Duco::ELinkedAssociationId:
        return object.LinkedToAssociation(fieldValue);

    default:
        return false;
    }
}


bool
TSearchFieldIdArgument::Match(const Duco::Picture& object, const Duco::RingingDatabase& database) const
{
    switch (fieldId)
    {
    case Duco::EPictureId:
        return object.Id() == fieldValue;

    default:
        return false;
    }
}


bool
TSearchFieldIdArgument::CheckRingerIds(const Duco::Peal& peal) const
{
    bool includePeal = false;
    if (searchType == ENotEqualTo)
    {
        includePeal = true;
    }
    bool asStrapper (false);
    bool pealIncludesRinger = peal.ContainsRinger(fieldValue, asStrapper);
    switch (searchType)
    {
    default:
    case EEqualTo:
        includePeal = pealIncludesRinger;
        break;
    case ENotEqualTo:
        includePeal = !pealIncludesRinger;
        break;
    }
    
    return includePeal;
}

bool
TSearchFieldIdArgument::CheckStrapperId(const Duco::Peal& peal) const
{
    bool asStrapper (false);
    if (!fieldValue.ValidId())
    {
        return peal.AnyStrappers();
    }
    else if (searchType == EEqualTo)
    {
        if (peal.ContainsRinger(fieldValue, asStrapper) && asStrapper)
        {
            return true;
        }
    }
    else if (searchType == ENotEqualTo)
    {
        if (peal.ContainsRinger(fieldValue, asStrapper) && !asStrapper)
             return true;
    }
    return false;
}

bool
TSearchFieldIdArgument::CheckSeriesContainsMethod(const Duco::Peal& peal, const Duco::RingingDatabase& database) const
{
    bool found (false);
    if (peal.SeriesId().ValidId())
    {
        const MethodSeries* theSeries = database.MethodSeriesDatabase().FindMethodSeries(peal.SeriesId());
        if (theSeries != NULL && theSeries->ContainsMethod(fieldValue))
        {
            found = true;
        }
    }

    return found;
}
