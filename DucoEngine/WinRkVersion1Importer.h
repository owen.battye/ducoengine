#ifndef __WINRKVERSION1IMPORTER_H__
#define __WINRKVERSION1IMPORTER_H__

#include <string>
#include <vector>
#include "DucoEngineCommon.h"
#include "FileImporter.h"

namespace Duco
{
    class RingingDatabase;
    class Peal;
    class ProgressCallback;

class WinRkVersion1Importer : public FileImporter
{
public:
    DllExport WinRkVersion1Importer(Duco::ProgressCallback& theCallback,
                            Duco::RingingDatabase& theDatabase);
    DllExport ~WinRkVersion1Importer();

protected: // from FileImporter
    virtual void GetTotalFileSize(const std::string& fileName);
    virtual bool ProcessPeal(const std::wstring& nextLine);
    virtual bool EnforceQuotes() const;
    virtual std::wifstream& GetLine(std::wifstream& inputFile, std::wstring& nextLine);

protected: // new functions
    unsigned int CreateRingers(std::vector<std::wstring>& tokens, Duco::Peal& thePeal) const;
    void CreateTower(const std::vector<std::wstring>& tokens, Duco::Peal& thePeal, unsigned int noOfRingers) const;
    void FindMethod(const std::vector<std::wstring>& tokens, Duco::Peal& thePeal, unsigned int& noOfRingers) const;
    void FindDate(const std::vector<std::wstring>& tokens, Duco::Peal& thePeal) const;
    void FindTime(const std::vector<std::wstring>& tokens, Duco::Peal& thePeal) const;
    //void SplitRingerName(std::wstring& ringerStr, std::wstring& forename, std::wstring& surname) const;

    bool GetFieldValue(const std::vector<std::wstring>& tokens, size_t fieldNo, size_t& fieldValue) const;
    bool GetFieldValue(const std::vector<std::wstring>& tokens, size_t fieldNo, std::wstring& fieldValue) const;
    std::wstring CreateTenorField(const std::vector<std::wstring>& tokens) const;
    std::wstring RecodeRingerName(std::wstring& ringerStr) const;
};

}

#endif //!__WINRKVERSION1IMPORTER_H__
