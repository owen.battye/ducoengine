#include "DatabaseWriter.h"
#include "PerformanceTime.h"
#include "PerformanceDate.h"
#include "ObjectId.h"
#include <sstream>
#include "DucoEngineUtils.h"
#include <locale>
#include <codecvt>

using namespace std;
using namespace Duco;

DatabaseWriter::DatabaseWriter(const char* filename)
{
    output.exceptions(wifstream::failbit);
    std::locale loc(std::locale("C"), new std::codecvt_utf8<wchar_t>());
    output.imbue(loc);
    output.open(filename, ios::trunc | ios::binary);
}

DatabaseWriter::~DatabaseWriter()
{
    output.close();
}

void
DatabaseWriter::WriteInt(int number)
{
    output << number << ends;
}

void
DatabaseWriter::WriteInt(unsigned int number)
{
    output << number << ends;
}

void
DatabaseWriter::WriteInt(unsigned long long number)
{
    output << number << ends;
}

void
DatabaseWriter::WriteId(const ObjectId& id)
{
    output << id << ends;
}

void
DatabaseWriter::WriteBool(bool number)
{
    output << number << ends;
}

void
DatabaseWriter::WriteString(const std::wstring& str)
{
    size_t stringLength = str.length();
    WriteInt(stringLength);
    streamoff startPos = output.tellp();
    output << str.c_str();
    streamoff endPos = output.tellp();
    if ( (startPos + (streamoff)stringLength) > endPos)
    {
        output << " ";
    }
}

void
DatabaseWriter::WritePictureBuffer(const std::string& pictureBuffer)
{
    std::wstring temp;
    std::string::const_iterator it = pictureBuffer.begin();
    while (it != pictureBuffer.end())
    {
        temp += (output.widen(*it));
        ++it;
    }
    size_t stringLength = temp.length();
    WriteInt(stringLength);
    output.write(temp.c_str(), stringLength);
}

void
DatabaseWriter::WriteTime(const time_t& date)
{
    long long tempDate = date;
    output << tempDate << ends;
}

void
DatabaseWriter::WriteTime(const Duco::PerformanceTime& dateTime)
{
    WriteInt(dateTime.pealTimeInMinutes);
}

void
DatabaseWriter::WriteDate(const Duco::PerformanceDate& dateTime)
{
    WriteInt(dateTime.pealDateYear);
    WriteInt(dateTime.pealDateMonth);
    WriteInt(dateTime.pealDateDay);
}
