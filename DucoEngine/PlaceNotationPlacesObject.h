#ifndef __PLACENOTATIONPLACESOBJECT_H__
#define __PLACENOTATIONPLACESOBJECT_H__

#include <set>
#include "PlaceNotationObject.h"

namespace Duco
{
class PlaceNotationPlacesObject : public PlaceNotationObject
{
public:
    PlaceNotationPlacesObject(const std::wstring& newStringRepresentation, TChangeType newType);
    PlaceNotationPlacesObject(const PlaceNotationPlacesObject& other);
    virtual ~PlaceNotationPlacesObject();

    virtual PlaceNotationObject* Realloc() const;
    virtual bool RequiresSeperator() const;
    virtual unsigned int BestStartingLead() const;

    virtual Change ProcessRow(const Change& change) const;

private:
    std::set<unsigned int> places;
};

}

#endif //!__PLACENOTATIONPLACESOBJECT_H__
