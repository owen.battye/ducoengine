#include "DucoEngineLog.h"
#include <exception>
#include <stdarg.h>

using namespace std::chrono;

namespace Duco
{

void
DucoEngineLog::Print(const char* filename, int linenumber, const char* text, ...)
{
    if (text != NULL)
    {
        static FILE* ducoLogFileHandle = fopen("DucoEngineDebug.log", "a+");
        if (ducoLogFileHandle != NULL)
        {
            char buffer[256];
            va_list args;
            va_start(args, text);
            std::vsnprintf(buffer, 256, text, args);
            va_end(args);

            auto t = std::time(nullptr);
            auto tm = *std::localtime(&t);
            std::ostringstream oss;
            oss << filename << ":" << linenumber << ": ";
            oss << std::put_time(&tm, "%d-%m-%Y %H:%M:%S: ");

            int returnVal = fputs(oss.str().c_str(), ducoLogFileHandle);
            if (returnVal >= 0)
                returnVal = fputs(buffer, ducoLogFileHandle);
            if (returnVal >= 0)
            {
                fputc('\n', ducoLogFileHandle);
                fflush(ducoLogFileHandle);
            }
            if (returnVal < 0)
            {
                _RPT0(_CRT_WARN, "Error writing to log file\r\n");
            }
#ifdef _DEBUG
            _RPT_BASE(_CRT_WARN, filename, linenumber, NULL, "%ls\r\n", buffer);
#endif
        }
    }
}

void
DucoEngineLog::Print(const char* filename, int linenumber, const wchar_t* text)
{
    if (text != NULL)
    {
        static FILE* ducoLogFileHandle = fopen("DucoEngineDebug.log", "a+");
        if (ducoLogFileHandle != NULL)
        {
            auto t = std::time(nullptr);
            auto tm = *std::localtime(&t);
            std::ostringstream oss;
            oss << filename << ":" << linenumber << ": ";
            oss << std::put_time(&tm, "%d-%m-%Y %H:%M:%S: ");

            int returnVal = fputs(oss.str().c_str(), ducoLogFileHandle);
            if (returnVal >= 0)
                returnVal = fputws(text, ducoLogFileHandle);
            if (returnVal >= 0)
            {
                fputc('\n', ducoLogFileHandle);
                fflush(ducoLogFileHandle);
            }
            if (returnVal < 0)
            {
                _RPT0(_CRT_WARN, "Error writing to log file\r\n");
            }
        }
#ifdef _DEBUG
        _RPT_BASE(_CRT_WARN, filename, linenumber, NULL, "%ls\r\n", text);
#endif
    }
}

void
DucoEngineLog::Debug(const char* filename, int linenumber, const std::chrono::time_point<std::chrono::high_resolution_clock>& start, char const* format, ...)
{
#ifdef _DEBUG
    char buffer[256];
    va_list args;
    va_start(args, format);
    std::vsnprintf(buffer, 256, format, args);
    va_end(args);

    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);

    if (duration.count() > 20000) //20 microseconds / 0.02 seconds
    {
        std::string message = std::format("{}; took: {}\r\n", buffer, duration);
        _RPT_BASE(_CRT_WARN, filename, linenumber, NULL, "%s", message.c_str());
    }
#endif
}

void
DucoEngineLog::Debug(const char* filename, int linenumber, char const* format, ...)
{
#ifdef _DEBUG
    char buffer[256];
    va_list args;
    va_start(args, format);
    std::vsnprintf(buffer, 256, format, args);
    va_end(args);

    std::string message = std::format("{}\r\n", buffer);
    _RPT_BASE(_CRT_WARN, filename, linenumber, NULL, "%s", message.c_str());
#endif
}

}