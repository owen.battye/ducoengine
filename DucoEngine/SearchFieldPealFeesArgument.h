#ifndef __SEARCHFIELDPEALFEESARGUMENT_H__
#define __SEARCHFIELDPEALFEESARGUMENT_H__

#include "SearchArgument.h"
#include "DucoEngineCommon.h"
#include <map>
#include <string>

namespace Duco
{
    class Association;
    class Peal;
    class Tower;
    class Ringer;
    class Method;

class TSearchFieldPealFeesArgument : public TSearchArgument
{
public:
    DllExport explicit TSearchFieldPealFeesArgument(const Duco::ObjectId& newAssociationId);
    Duco::TFieldType FieldType() const;
    ~TSearchFieldPealFeesArgument();

    inline std::map<unsigned int, unsigned int>& AvgPealFees();
    inline const Duco::ObjectId& AssociationId() const;

    virtual bool Match(const Duco::Peal& peal, const Duco::RingingDatabase& database) const;

protected:
    std::map<unsigned int, unsigned int> avgPealFeePerYear;
    Duco::ObjectId associationId;
};

std::map<unsigned int, unsigned int>&
TSearchFieldPealFeesArgument::AvgPealFees()
{
    return avgPealFeePerYear;
}

const Duco::ObjectId&
TSearchFieldPealFeesArgument::AssociationId() const
{
    return associationId;
}

}

#endif // __SEARCHFIELDPEALFEESARGUMENT_H__

