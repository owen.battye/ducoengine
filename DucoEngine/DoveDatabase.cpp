#include "DoveDatabase.h"

#include "AbbreviationList.h"
#include "DoveObject.h"
#include "DucoEngineUtils.h"
#include "Tower.h"
#include "TowerDatabase.h"
#include <string>
#include "DoveTowerValidator.h"
#include <istream>
#include <codecvt>

using namespace std;
using namespace Duco;

#define VALID_DOVE_TOWER_ID_CHARS L"1234567890"

DoveDatabase::DoveDatabase(const std::string& filename, ProgressCallback* newCallback, const std::string& installationDir, bool newConvertToLong)
    : inputFile(NULL), callback(newCallback), fileSize(0), validator(NULL), cancellationCheck(NULL), convertToLong(newConvertToLong),
    saintAbbreviations(NULL)
{
    std::string doveSaintsFile(installationDir);
#ifdef _WINS
    doveSaintsFile.append("\\DoveSaints.txt");
#else
    doveSaintsFile.append("DoveSaints.txt");
#endif
    saintAbbreviations = new Duco::AbbreviationList(doveSaintsFile);

    inputFile = new wifstream(filename.c_str());
    if (inputFile != NULL && inputFile->is_open())
    {
        std::locale loc(std::locale("C"), new std::codecvt_utf8<wchar_t>());
        inputFile->imbue(loc);

        inputFile->seekg (0, ios_base::end);
        streampos tempfileSize (inputFile->tellg());
        fileSize = float(tempfileSize);
        inputFile->seekg (0, ios_base::beg);
    }
}

DoveDatabase::~DoveDatabase()
{
    Clear();
    delete inputFile;
    delete saintAbbreviations;
}

bool
DoveDatabase::Import()
{
    if (callback)
    {
        callback->Initialised();
    }
    if (inputFile == NULL || !inputFile->is_open())
        return false;

    bool firstLine = true;

    while (!inputFile->eof())
    {
        std::wstring nextLine;
        getline(*inputFile, nextLine);

        if (nextLine.length() > 0)
        {
            if (firstLine)
            {
                fileConfig.Configure(nextLine);
                firstLine = false;
            }
            else
            {
                DoveObject* newObject = new DoveObject(nextLine, fileConfig);
                bool keepObject = false;
                if (newObject->Valid())
                {
                    if (convertToLong)
                    {
                        std::wstring longEquivalent;
                        if (ProperSaint(newObject->Dedication(), longEquivalent))
                        {
                            newObject->SetDedication(longEquivalent);
                        }
                    }

                    if (validator == NULL || validator->IncludeTower(*newObject))
                    {
                        std::pair<std::wstring, Duco::DoveObject* > newTowerPair(newObject->DoveId(), newObject);
                        auto insertedIt = objects.insert(newTowerPair);
                        keepObject = insertedIt->second;

                        if (keepObject && newObject->OldDoveId().length() > 0)
                        {
                            std::pair<std::wstring, std::wstring> newPair(newObject->OldDoveId(), newObject->DoveId());
                            oldDoveIdMap.insert(newPair).second;
                            // This only means two towers had the same old dove ID - Rugby maybe?
                        }
                    }
                }
                if (!keepObject)
                {
                    delete newObject;
                }
            }
        }
        float currentPos = float(inputFile->tellg());

        int progress = int(currentPos / fileSize * 100);

        if (callback)
        {
            callback->Step(progress);
        }

        if (cancellationCheck != NULL && !cancellationCheck->ContinueImport())
        {
            break;
        }
    }
    delete inputFile;
    inputFile = NULL;

    return true;
}

bool
DoveDatabase::SuggestTower(const Duco::TowerDatabase& towersDatabase, const Duco::Tower& theTowerToFindInDove, Duco::DoveObject& foundTower, Duco::ObjectId& existingTowerWithId) const
{
    existingTowerWithId.ClearId();
    foundTower.Clear();

    int bestMatchProbability (0);
    bool found = false;
    for (auto const& [key, value] : objects)
    {
        int matchProbability (theTowerToFindInDove.MatchProbability(*value, *this));
        if (matchProbability >= 4 && matchProbability > bestMatchProbability)
        {
            found = true;
            foundTower = *value;
            bestMatchProbability = matchProbability;
        }
        else if (matchProbability == bestMatchProbability && matchProbability > 0)
        {
            foundTower.Clear();
            found = false;
        }
    }
    if (found)
    {
        existingTowerWithId = towersDatabase.FindTowerByDoveId(foundTower.DoveId());
    }
    return found;
}

const Duco::DoveObject* const
DoveDatabase::SuggestTower(unsigned int noOfBells, const std::wstring& towerDedication, const std::wstring& towerName, const std::wstring& towerCounty, const std::wstring& tenorWeight, const std::wstring& tenorKey) const
{
    unsigned int bestMatchProbability(0);
    Duco::DoveObject* bestMatch = NULL;

    Tower towerToFind(KNoId, towerDedication, towerName, towerCounty, noOfBells);
    size_t index = tenorWeight.find(L"cwt");
    if (index != string::npos)
    {
        index = index - 1;
    }
    towerToFind.AddRing(noOfBells, DucoEngineUtils::Trim(tenorWeight.substr(0, index)), tenorKey);

    for (auto const& [key, value] : objects)
    {
        unsigned int currentMatch = towerToFind.MatchProbability(*value, *this);
        if (currentMatch > bestMatchProbability)
        {
            bestMatchProbability = currentMatch;
            bestMatch = value;
        }
        else if (currentMatch > bestMatchProbability)
        {
            bestMatch = NULL;
        }
    }
    return bestMatch;
}

bool
DoveDatabase::ImportMissingTenorKeyAndIds(Duco::Tower& tower) const
{
    if (!tower.DoveRefSet())
        return false;

    std::wstring towerRef = tower.DoveRef();
    bool useOldDoveIdFormat = towerRef.find_first_not_of(VALID_DOVE_TOWER_ID_CHARS) != wstring::npos;
    if (useOldDoveIdFormat)
    {
        std::map<std::wstring, std::wstring>::const_iterator mapping = oldDoveIdMap.find(towerRef);
        if (mapping != oldDoveIdMap.begin())
        {
            towerRef = mapping->second;
            tower.SetDoveRef(towerRef);
        }
    }
    std::multimap<std::wstring, Duco::DoveObject*>::const_iterator it = objects.find(towerRef);
    if (it != objects.end())
    {
        return tower.UpdateTenorFromDoveAndOtherIds(*it->second);
    }
    return false;
}

size_t
DoveDatabase::ImportTowerIdAndPosition(Duco::TowerDatabase& towers, const std::map<std::wstring, Duco::ObjectId>& doveIdAndTowerIds) const
{
    size_t noOfUpdatedTowers = 0;
    for (auto const& [key, value] : objects)
    {
        std::map<std::wstring, Duco::ObjectId>::const_iterator towerId = doveIdAndTowerIds.find(value->DoveId());
        if (towerId == doveIdAndTowerIds.end())
        {
            towerId = doveIdAndTowerIds.find(value->OldDoveId());
        }
        if (towerId != doveIdAndTowerIds.end())
        {
            Duco::Tower updatedTower = *towers.FindTower(towerId->second, false);
            updatedTower.UpdateDoveLocationAndOtherIds(*value);
            towers.UpdateObject(updatedTower);
            ++noOfUpdatedTowers;
        }
    }
    return noOfUpdatedTowers;
}

size_t
DoveDatabase::ImportPositionAndIds(Duco::TowerDatabase& towers, std::set<Duco::ObjectId>& ducoTowerIds) const
{
    size_t noOfUpdatedTowers = 0;
    for (auto const& it : ducoTowerIds)
    {
        Duco::Tower updatedTower = *towers.FindTower(it, false);
        std::wstring doveRefForTower = updatedTower.DoveRef();
        std::map<std::wstring, std::wstring>::const_iterator mapping = oldDoveIdMap.find(doveRefForTower);
        if (mapping != oldDoveIdMap.end())
        {
            doveRefForTower = mapping->second;
        }

        std::multimap<std::wstring, Duco::DoveObject*>::const_iterator it2 = objects.find(doveRefForTower);
        if (it2 != objects.end())
        {
            if (updatedTower.UpdateDoveLocationAndOtherIds(*it2->second))
            {
                towers.UpdateObject(updatedTower);
                ++noOfUpdatedTowers;
            }
        }
    }
    return noOfUpdatedTowers;
}


void
DoveDatabase::Clear()
{
    for (auto const& [key, value] : objects)
    {
        delete value;
    }
    objects.clear();
}

bool
DoveDatabase::CompareSaint(const std::wstring& saint1, const std::wstring& saint2, bool substring) const
{
    if (saintAbbreviations == NULL)
    {
        return DucoEngineUtils::CompareString(saint1, saint2);
    }
    std::wstring updatedSaint1;
    saintAbbreviations->ProperName(saint1, updatedSaint1);
    std::wstring updatedSaint2;
    saintAbbreviations->ProperName(saint2, updatedSaint2);


    return DucoEngineUtils::CompareString(updatedSaint1, updatedSaint2);
}

bool
DoveDatabase::ProperSaint(const std::wstring& saint, std::wstring& properName) const
{
    if (saintAbbreviations == NULL)
        return false;
    if (saintAbbreviations->ProperName(saint, properName))
    {
        return true;
    }
    properName = saint;
    return false;
}

bool
DoveDatabase::FindTower(const std::wstring& doveId, std::set<Duco::DoveObject>& foundTowers) const
{
    foundTowers.clear();
    bool useOldDoveIdFormat = doveId.find_first_not_of(VALID_DOVE_TOWER_ID_CHARS) != wstring::npos;
    for (auto const& [key, value] : objects)
    {
        if (doveId.compare(value->DoveId()) == 0)
        {
            foundTowers.insert(*value);
        }
        else if (useOldDoveIdFormat && doveId.compare(value->OldDoveId()) == 0)
        {
            foundTowers.insert(*value);
        }
    }

    return foundTowers.size() > 0;
}

bool
DoveDatabase::FindTower(const std::wstring& doveId, size_t numberOfBells, Duco::DoveObject& foundTower) const
{
    std::set<Duco::DoveObject> foundTowers;
    if (!FindTower(doveId, foundTowers))
        return false;

    if (foundTowers.size() == 1)
    {
        foundTower = *foundTowers.begin();
        return true;
    }

    unsigned int bellsAway = 9999;

    for (auto const& it : foundTowers)
    {
        if (it.Bells() >= numberOfBells)
        {
            foundTower = it;
            bellsAway = it.Bells() - numberOfBells;
            if (bellsAway == 0)
                return true;
        }
    }

    return bellsAway != 9999;

}

bool
DoveDatabase::FindTowerByTowerbaseId(const std::wstring& towerbaseId, std::set<Duco::DoveObject>& foundTowers) const
{
    foundTowers.clear();
    for (auto const& [key, value] : objects)
    {
        if (towerbaseId.compare(value->TowerbaseId()) == 0)
        {
            foundTowers.insert(*value);
        }
    }

    return foundTowers.size() > 0;
}


size_t
DoveDatabase::NumberOfTowersInCountry(const std::wstring& country) const
{
    size_t returnVal (0);

    for (auto const& [key, value] : objects)
    {
        if (value->Country().compare(country) == 0)
        {
            ++returnVal;
        }
    }

    return returnVal;
}

size_t
DoveDatabase::NumberOfCountries() const
{
    std::set<std::wstring> countries;

    for (auto const& [key, value] : objects)
    {
        countries.insert(value->Country());
    }

    return countries.size();
}

size_t
DoveDatabase::SaveSaintsNotUpdated(const std::string& filename)
{
    if (saintAbbreviations == NULL)
        return 0;

    return saintAbbreviations->SaveAbbreviationsNotUpdated(filename);
}

size_t 
DoveDatabase::NumberOfSaintAbbreviations() const
{
    return saintAbbreviations->NoOfObjects();
}

size_t
DoveDatabase::NoOfTowers() const
{
    return objects.size();
}

bool
DoveDatabase::ConfigValid() const
{
    return fileConfig.ConfigValid();
}
