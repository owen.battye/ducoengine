#ifndef __DUCOXMLUTILS_H__
#define __DUCOXMLUTILS_H__

#include <string>
#include <set>
#include <xercesc\dom\DOMDocument.hpp>

namespace Duco
{
    class ObjectId;

class DucoXmlUtils
{
public:
    static XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* AddElement(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile,
                                                          XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& parentElement,
                                                          const std::wstring& elementName, const std::wstring& elementValue,
                                                          bool addIfValueBlank = true);
    static XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* AddBoolElement(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile,
                                                          XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& parentElement,
                                                          const std::wstring& elementName, bool elementValue);
    static XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* AddElement(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile,
                                                          XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& parentElement,
                                                          const std::wstring& elementName, size_t elementValue);

    static XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* AddElements(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile,
                                                          XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& parentElement,
                                                          const std::wstring& elementsName, const std::wstring& elementName,
                                                          const std::set<Duco::ObjectId>& ids);

    static void AddAttribute(XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& parentElement,
                                                          const std::wstring& attributeName, const std::wstring& attributeValue,
                                                          bool addIfValueBlank = false);
    static void AddBoolAttribute(XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& parentElement,
                                                          const std::wstring& elementName, bool attributeValue);
    static void AddAttribute(XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& parentElement,
                                                          const std::wstring& elementName, size_t attributeValue);
    static void AddObjectId(XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& parentElement,
                                                          const Duco::ObjectId& objectId,
                                                          bool addIfValueBlank = true);

};

}

#endif //!__DUCOXMLUTILS_H__
