#ifndef __SEARCHVALIDATIONARGUMENT_H__
#define __SEARCHVALIDATIONARGUMENT_H__

#include <set>
#include "SearchCommon.h"
#include "DucoEngineCommon.h"

namespace Duco
{
    class Association;
    class Composition;
    class Method;
    class MethodSeries;
    class Peal;
    class Ringer;
    class RingingDatabase;
    class Tower;
    class Picture;
    class ObjectId;

class TSearchValidationArgument
{
public:
    virtual bool Match(const Duco::Peal& object, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const = 0;
    virtual bool Match(const Duco::MethodSeries& object, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Method& object, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Ringer& object, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const = 0;
    virtual bool Match(const Duco::Tower& object, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Composition& object, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Association& object, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Picture& object, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const;

    virtual Duco::TFieldType FieldType() const = 0;
    virtual void Reset();
    DllExport virtual TSearchFieldId FieldId() const;

    DllExport virtual ~TSearchValidationArgument();
    virtual std::set<Duco::ObjectId> ErrorIds() const = 0;

protected:
    TSearchValidationArgument(Duco::TSearchFieldId newFieldId);

    const TSearchFieldId fieldId;
};
 
}
#endif //__SEARCHVALIDATIONARGUMENT_H__

