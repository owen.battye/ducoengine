#include "SearchFieldPealFeesArgument.h"

#include "DucoEngineUtils.h"
#include "Method.h"
#include "MethodDatabase.h"
#include "MethodSeries.h"
#include "MethodSeriesDatabase.h"
#include "Peal.h"
#include "Ringer.h"
#include "RingingDatabase.h"
#include "Tower.h"

using namespace Duco;

TSearchFieldPealFeesArgument::TSearchFieldPealFeesArgument(const Duco::ObjectId& newAssociationId)
: TSearchArgument(EInconsistantFees), associationId(newAssociationId)
{

}

TSearchFieldPealFeesArgument::~TSearchFieldPealFeesArgument()
{

}

Duco::TFieldType 
TSearchFieldPealFeesArgument::FieldType() const
{
    return TFieldType::ENumberField;
}

bool
TSearchFieldPealFeesArgument::Match(const Duco::Peal& peal, const Duco::RingingDatabase& database) const
{
    switch (fieldId)
    {
        default:
            return false;

        case EInconsistantFees:
            break;
    }

    if (associationId != peal.AssociationId())
        return false;

    std::map<unsigned int, unsigned int>::const_iterator it = avgPealFeePerYear.find(peal.Date().Year());
    if (it == avgPealFeePerYear.end())
        return false;

    return !peal.CheckPaidFeeAgainstMedian(it->second);
}
