#ifndef __CHANGETYPE_H__
#define __CHANGETYPE_H__

namespace Duco
{
    enum TChangeType
    {
        EOther = 0,
        ELeadHead,
        ELeadEnd
    };
}

#endif //!__CHANGETYPE_H__
