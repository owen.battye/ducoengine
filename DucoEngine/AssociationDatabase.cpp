#include "AssociationDatabase.h"

#include "DucoEngineUtils.h"
#include "Association.h"
#include "ImportExportProgressCallback.h"
#include "RenumberProgressCallback.h"
#include "SearchArgument.h"
#include "SearchValidationArgument.h"
#include "ProgressCallback.h"
#include "RingingDatabase.h"
#include "DatabaseSettings.h"
#include "PealDatabase.h"
#include "StatisticFilters.h"
#include <xercesc\dom\DOMElement.hpp>

using namespace Duco;
using namespace std;
XERCES_CPP_NAMESPACE_USE

AssociationDatabase::AssociationDatabase()
: RingingObjectDatabase(TObjectType::EAssociationOrSociety)
{
}

AssociationDatabase::~AssociationDatabase()
{
}

Duco::ObjectId
AssociationDatabase::AddObject(const Duco::RingingObject& newObject)
{
    Duco::ObjectId foundObjectId;
    if (newObject.ObjectType() == TObjectType::EAssociationOrSociety)
    {
        const Duco::Association& newAssociation = static_cast<const Duco::Association&>(newObject);

        Duco::RingingObject* newAssociationWithId = new Duco::Association(newAssociation);
        if (AddOwnedObject(newAssociationWithId))
        {
            foundObjectId = newAssociationWithId->Id();
        }
        // object is deleted by RingingObjectDatabase::AddObject if not added.
    }
    return foundObjectId;
}

bool
AssociationDatabase::SaveUpdatedObject(Duco::RingingObject& lhs, const Duco::RingingObject& rhs)
{
    if (lhs.ObjectType() == rhs.ObjectType() && rhs.ObjectType() == TObjectType::EAssociationOrSociety)
    {
        Duco::Association& lhsAssociation = static_cast<Duco::Association&>(lhs);
        const Duco::Association& rhsAssociation = static_cast<const Duco::Association&>(rhs);
        lhsAssociation = rhsAssociation;
        return true;
    }
    return false;
}

void
AssociationDatabase::ExternaliseToXml(Duco::ImportExportProgressCallback* newCallback, XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, bool exportDucoObjects) const
{
    DOMElement* rootElem = outputFile.getDocumentElement();

    DOMElement* newAssociations = outputFile.createElement(XMLStrL("Association"));
    rootElem->appendChild(newAssociations);

    size_t count = 0;
    for (const auto& [key, value] : listOfObjects)
    {
        const Duco::Association* theAssociation = static_cast<const Duco::Association*>(value);
        theAssociation->ExportToXml(outputFile, *newAssociations, exportDucoObjects);

        newCallback->ObjectProcessed(false, TObjectType::EAssociationOrSociety, value->Id(), 0, ++count);
    }
}

bool
AssociationDatabase::Internalise(DatabaseReader& reader, Duco::ImportExportProgressCallback* callback, int databaseVersionNumber, size_t noOfObjectsInThisDatabase, size_t totalObjects, size_t objectsProcessedSoFar)
{
    size_t totalProcessed = objectsProcessedSoFar;
    size_t count = 0;

    while (noOfObjectsInThisDatabase-- > 0)
    {
        ++totalProcessed;
        ++count;
        float percentageComplete = ((float)totalProcessed / (float)totalObjects) * 100;

        Duco::RingingObject* newComposition = new Duco::Association(reader, databaseVersionNumber);
        if (newComposition == NULL)
            return false;
        AddOwnedObject(newComposition);
        if (callback != NULL)
        {
            callback->ObjectProcessed(true, TObjectType::EAssociationOrSociety, newComposition->Id(), (int)percentageComplete, count);
        }
    }
    return true;
}

const Duco::Association* const
AssociationDatabase::FindAssociation(const Duco::ObjectId& associationId, bool setIndex) const
{
    const RingingObject* const object = FindObject(associationId, setIndex);

    return static_cast<const Duco::Association* const>(object);
}

Duco::ObjectId
AssociationDatabase::SuggestAssociation(const std::wstring& name, bool fromImport) const
{
    std::wstring processedName;
    DucoEngineUtils::ToLowerCase(DucoEngineUtils::Trim(name), processedName);
    Duco::ObjectId nearestId;
    Duco::ObjectId substringId;
    size_t ignored;

    for (const auto& [key, value] : listOfObjects)
    {
        const Duco::Association* theAssociation = static_cast<const Duco::Association*>(value);
        if (DucoEngineUtils::CompareString(theAssociation->Name(), name, true, true))
        {
            return theAssociation->Id();
        }
        else if (!fromImport && name.length() != 0 && DucoEngineUtils::FindSubstring(theAssociation->Name(), name, ignored, true))
        {
            substringId = theAssociation->Id();
        }
    }

    return substringId;
}

std::wstring
AssociationDatabase::FindAssociationName(const Duco::ObjectId& associationId) const
{
    const Duco::Association* const assoc = FindAssociation(associationId);
    if (assoc != NULL)
    {
        return assoc->Name();
    }
    return L"Unknown association";
}

Duco::ObjectId
AssociationDatabase::SuggestAssociationOrCreate(const std::wstring& name, bool fromImport)
{
    Duco::ObjectId objectId = SuggestAssociation(name, fromImport);

    if (!objectId.ValidId())
    {
        objectId = CreateAssociation(name);
    }
    return objectId;
}

Duco::ObjectId
AssociationDatabase::CreateAssociation(const std::wstring& name)
{
    Association newAssociation;
    newAssociation.SetName(name);
    return AddObject(newAssociation);
}

bool
AssociationDatabase::MatchObject(const Duco::RingingObject& object, const Duco::TSearchArgument& searchArg, const Duco::RingingDatabase& database) const
{
    if (object.ObjectType() != TObjectType::EAssociationOrSociety)
        return false;
    return searchArg.Match(static_cast<const Duco::Association&>(object), database);
}

bool
AssociationDatabase::ValidateObject(const Duco::RingingObject& object, const Duco::TSearchValidationArgument& searchArg, const std::set<Duco::ObjectId>& idsToCheckAgainst, const Duco::RingingDatabase& database) const
{
    if (object.ObjectType() != TObjectType::EAssociationOrSociety)
        return false;
    return searchArg.Match(static_cast<const Duco::Association&>(object), idsToCheckAgainst, database);
}

//***********************************************************************
// Sorting / reordering Methods
//***********************************************************************
bool
AssociationDatabase::RebuildObjects(Duco::RenumberProgressCallback* callback, Duco::RingingDatabase& database)
{
    if (listOfObjects.size() <= 0)
    {
        return false;
    }

    callback->RenumberInitialised(RenumberProgressCallback::EReindexAssociations);
    std::map<Duco::ObjectId, Duco::ObjectId> newAssociationIds;
    {
        if (database.Settings().AlphabeticReordering())
            GetNewObjectIdsByAlphabetic(newAssociationIds, database.Settings().LastNameFirst());
        else
        {
            Duco::StatisticFilters filters(database);
            std::map<Duco::ObjectId, Duco::PealLengthInfo> associationPealCounts;
            database.PealsDatabase().GetAssociationsPealCount(filters, associationPealCounts, false);
            GetNewIdsByPealCountAndAlphabetic(associationPealCounts, newAssociationIds, database.Settings().LastNameFirst());
        }
    }

    // Renumber all methods first.
    callback->RenumberInitialised(RenumberProgressCallback::ERenumberAssociations);
    bool changesMade = RenumberObjects(newAssociationIds, callback);

    // Renumber Association links
    for (const auto& [key, value] : listOfObjects)
    {
        Duco::Association* theAssociation = static_cast<Duco::Association*>(value);
        changesMade |= theAssociation->RenumberAssociationLinks(newAssociationIds);
    }

    // Renumber Associations in Peals
    callback->RenumberInitialised(RenumberProgressCallback::ERebuildAssociations);
    changesMade |= database.PealsDatabase().RenumberAssociationsInPeals(newAssociationIds, callback);
    return changesMade;
}

bool
AssociationDatabase::UpdateObjectField(const Duco::ObjectId&, const Duco::TUpdateArgument&)
{
    return false;
}

void
AssociationDatabase::FindLinksToAssociation(const Duco::ObjectId& associationId, std::set<Duco::ObjectId>& links)
{
    links.clear();

    for (const auto& [key, value] : listOfObjects)
    {
        const Duco::Association* theAssociation = static_cast<const Duco::Association*>(value);
        if (theAssociation->Links().contains(associationId))
        {
            links.insert(theAssociation->Id());
        }
    }
}

bool
AssociationDatabase::UppercaseAssociations(ProgressCallback*const callback)
{
    size_t totalObjects = NumberOfObjects();
    size_t stepNumber = 0;
    bool changesMade(false);
    for (auto& [key, value] : listOfObjects)
    {
        Duco::Association* theAssociation = static_cast<Duco::Association*>(value);

        if (theAssociation->UppercaseAssociation())
        {
            changesMade = true;
            SetDataChanged();
        }
        ++stepNumber;
        if (callback != NULL)
        {
            callback->Step(int(stepNumber / totalObjects));
        }
    }

    return changesMade;
}

void
AssociationDatabase::FindDuplicateIds(std::map<Duco::ObjectId, Duco::ObjectId>& objectIds) const
{
    objectIds.clear();

    std::map<std::wstring, Duco::ObjectId> namesToIds;

    for (const auto& [key, value] : listOfObjects)
    {
        const Association* const theAssociation = static_cast<const Association* const>(value);
        std::pair<std::wstring, Duco::ObjectId> theObject(theAssociation->Name(), theAssociation->Id());

        std::pair<std::map<std::wstring, Duco::ObjectId>::iterator, bool> insertIt = namesToIds.insert(theObject);
        if (!insertIt.second)
        { // Not inserted, this string must already exist
            std::pair<Duco::ObjectId, Duco::ObjectId> replacementId(theAssociation->Id(), insertIt.first->second);
            objectIds.insert(replacementId);
        }
    }
}

void
AssociationDatabase::MergeLinkedAssociationsPealCounts(std::map<Duco::ObjectId, Duco::PealLengthInfo>& sortedPealCounts) const
{
    std::set<Duco::ObjectId> associationIdsToRemoveAfterMerge;

    for (const auto& [key, value] : listOfObjects)
    {
        const Association* const theAssociation = static_cast<const Association* const>(value);
        for (const auto& linkedId : theAssociation->linkedAssociations)
        {
            if (sortedPealCounts.find(linkedId) != sortedPealCounts.end() && sortedPealCounts.find(key) != sortedPealCounts.end())
            {
                sortedPealCounts[linkedId] += sortedPealCounts[key];
                if (!associationIdsToRemoveAfterMerge.contains(key))
                {
                    associationIdsToRemoveAfterMerge.insert(key);
                }
            }
        }
    }

    for (const auto& id : associationIdsToRemoveAfterMerge)
    {
        sortedPealCounts.erase(id);
    }
}

void
AssociationDatabase::RemoveUsedIds(std::set<Duco::ObjectId>& unusedAssociationsIds) const
{
    // Remove association from list where the association is used as a linked association
    for (const auto& [key, value] : listOfObjects)
    {
        const Association* const theAssociation = static_cast<const Association* const>(value);
        for (const auto& linkedId : theAssociation->linkedAssociations)
        {
            unusedAssociationsIds.erase(linkedId);
        }
        if (theAssociation->linkedAssociations.size() > 0)
        {
            unusedAssociationsIds.erase(key);
        }
    }
}

bool
AssociationDatabase::ReplaceAssociationLinks(ProgressCallback* callback, const Duco::ObjectId& oldAssociationId, const Duco::ObjectId& newAssociationId)
{
    bool changed = false;
    for (const auto& [key, value] : listOfObjects)
    {
        Association* const theAssociation = static_cast<Association* const>(value);
        changed |= theAssociation->ReplaceAssociationLink(oldAssociationId, newAssociationId);
    }

    return changed;
}
