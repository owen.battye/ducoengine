#ifndef __PEALRINGERDATA_H__
#define __PEALRINGERDATA_H__

#include "ObjectId.h"

namespace Duco
{
class PealRingerData
{
public:
    DllExport PealRingerData(Duco::ObjectId ringerId, unsigned int pealFee, bool fullyPaid);
    DllExport PealRingerData();
    DllExport PealRingerData(const PealRingerData& other);
    DllExport ~PealRingerData();

    DllExport bool SetPealFee(unsigned int newPealFee);
    DllExport unsigned int PealFee() const;

    DllExport const Duco::ObjectId& RingerId() const;
    DllExport void SetRingerId(const Duco::ObjectId& newId);

    DllExport bool FullyPaid() const;
    DllExport void SetFullyPaid(bool newFullyPaid);

    PealRingerData& operator=(const PealRingerData& rhs);
    DllExport bool operator==(const PealRingerData& rhs) const;
    bool operator!=(const PealRingerData& rhs) const;

protected:
    Duco::ObjectId  ringerId;
    unsigned int    pealFeeInPence;
    bool            fullyPaid;
};

}

#endif //!__PEALRINGERDATA_H__
