#include "FileImporter.h"

#include "DucoEngineUtils.h"
#include "DucoEngineLog.h"
#include "RingingDatabase.h"
#include "AssociationDatabase.h"
#include "RingerDatabase.h"
#include "MethodDatabase.h"
#include "TowerDatabase.h"
#include "PealDatabase.h"
#include "ProgressCallback.h"
#include <fstream>
#include <locale>
#include <codecvt>
#include <chrono>

using namespace std::chrono;
using namespace std;
using namespace Duco;

FileImporter::FileImporter(Duco::ProgressCallback& theCallback, Duco::RingingDatabase& theDatabase)
:   callback(theCallback), database(theDatabase), totalFileSize(0), processedFileSize(0), cancelled(false), setLocale(true)
{
}

FileImporter::~FileImporter()
{
}

void
FileImporter::Cancel() const
{
    cancelled = true;
}

bool
FileImporter::Import(const std::string& fileName)
{
    callback.Initialised();
    GetTotalFileSize(fileName);
    if (totalFileSize <= 0)
    {
        callback.Complete(true);
        return false;
    }

    database.ClearDatabase(false, true);

    ReadFiles(fileName);

    if (!cancelled)
    {
        callback.Complete(false);
    }
    return !cancelled;
}

void
FileImporter::ReadFiles(const std::string& fileName)
{
    ReadFile(fileName);
    DUCOENGINEDEBUGLOG2("Number of Associations: %u", database.AssociationsDatabase().NumberOfObjects());
    DUCOENGINEDEBUGLOG2("Number of Ringers: %u", database.RingersDatabase().NumberOfObjects());
    DUCOENGINEDEBUGLOG2("Number of Methods: %u", database.MethodsDatabase().NumberOfObjects());
    DUCOENGINEDEBUGLOG2("Number of Towers: %u", database.TowersDatabase().NumberOfObjects());
    DUCOENGINEDEBUGLOG2("Number of Peals: %u", database.PealsDatabase().NumberOfObjects());
}

void
FileImporter::ReadFile(const std::string& fileName)
{
    wifstream* inputFile = new wifstream(fileName.c_str());
    if (setLocale)
    {
        std::locale loc(std::locale("C"), new std::codecvt_utf8<wchar_t>());
        inputFile->imbue(loc);
    }

    time_point<high_resolution_clock> start = high_resolution_clock::now();

    if (inputFile != NULL && inputFile->is_open())
    {
        while (!inputFile->eof() && !cancelled && !inputFile->bad())
        {
            std::wstring nextLine (L"");
            GetLine(*inputFile, nextLine);
            if (nextLine.length() > 0 && !cancelled)
            {
                processedFileSize += float(nextLine.length()+1);
                ProcessPeal(nextLine);
                int progress = int((processedFileSize / totalFileSize) * 100);
                callback.Step(progress);
                DUCOENGINEDEBUGLOG3(start, "Peal processed (%d%%)", progress);
                start = high_resolution_clock::now();
            }
        }
    }
    else
    {
        DUCOENGINEDEBUGLOG2("Error opening file: %ls", fileName.c_str());
    }
    delete inputFile;
    inputFile = NULL;
}

std::wifstream&
FileImporter::GetLine(std::wifstream& inputFile, std::wstring& nextLine)
{
    nextLine.clear();

    std::wstring thisLine;
    int quoteCount = 0;
    do {
        if (thisLine.length() > 0)
        {
            nextLine += KNewlineChar;
            thisLine.clear();
        }
        getline(inputFile, thisLine);
        if (!inputFile.good())
        {
            std::wifstream::int_type x = inputFile.peek();
            wchar_t y = inputFile.peek();
            break;
        }
        if (thisLine.length() > 0)
        {
            nextLine += thisLine;
        }
        if (EnforceQuotes())
        {
            for (wchar_t& c : thisLine)  // count quotes
                if (c == L'"')
                    quoteCount++;
        }
    } while (quoteCount % 2);
    return inputFile;
}


void
FileImporter::TokeniseLine(const std::wstring& fileLine, std::vector<std::wstring>& tokens, bool useComma, bool useTab, bool useAdditionalChar, wchar_t additionalChar) const
{
    tokens.clear();
    std::wstring nextToken (L"");
    bool insideQuotes (false);

    std::wstring::const_iterator it = fileLine.begin();
    while (it != fileLine.end() && !cancelled)
    {
        switch (*it)
        {
            case '"':
                insideQuotes = !insideQuotes;
                if (!EnforceQuotes())
                {
                    nextToken += *it;
                }
                break;
            case ' ':
                nextToken += *it;
                break;
            case '\t':
                {
                    if (useTab)
                    {
                        if (!EnforceQuotes() || !insideQuotes)
                        {
                            tokens.push_back(DucoEngineUtils::Trim(nextToken));
                            nextToken.clear();
                        }
                        else
                        {
                            nextToken += *it;
                        }
                    }
                    else
                    {
                        nextToken += *it;
                    }
                }
                break;
            case ',':
                if (!useComma || insideQuotes)
                {
                    nextToken += *it;
                }
                else
                {
                    tokens.push_back(DucoEngineUtils::Trim(nextToken));
                    nextToken.clear();
                }
                break;
            default:
                if (useAdditionalChar && additionalChar == *it)
                {
                    tokens.push_back(DucoEngineUtils::Trim(nextToken));
                    nextToken.clear();
                }
                else
                {
                    nextToken += *it;
                }
                break;
        }
        ++it;
    }
    tokens.push_back(DucoEngineUtils::Trim(nextToken));
}

float
FileImporter::GetFileSize(const std::string& fileName) const
{
    FILE* pFile = fopen(fileName.c_str(), "r");
    if (pFile == NULL)
        return 0;

    fseek (pFile, 0, SEEK_END);
    float fileSize = float(ftell(pFile));
    fclose (pFile);
    return fileSize;
}
