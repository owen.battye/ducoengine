#ifndef __SEARCHFIELDBOOLARGUMENT_H__
#define __SEARCHFIELDBOOLARGUMENT_H__

#include "SearchArgument.h"
#include "DucoEngineCommon.h"

namespace Duco
{
    class Peal;
    class Tower;
    class Ringer;
    class Method;

class TSearchFieldBoolArgument : public Duco::TSearchArgument
{
public:
    DllExport explicit TSearchFieldBoolArgument(Duco::TSearchFieldId fieldId, bool newFieldValue = true);
    virtual bool Match(const Duco::Peal& peal, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Tower& tower, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Ringer& ringer, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Method& method, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::MethodSeries& methodSeries, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Composition& object, const Duco::RingingDatabase& database) const;
    virtual Duco::TFieldType FieldType() const;
    ~TSearchFieldBoolArgument();

protected:
    bool CompareDualOrder(const Duco::Peal& peal, const Duco::RingingDatabase& database) const;
    bool CheckForStrapper(const Duco::Peal& peal) const;
    bool ProveComposition(const Duco::Composition& object, const Duco::RingingDatabase& database) const;

protected:
    const bool         fieldValue;
};

}

#endif //!__SEARCHFIELDBOOLARGUMENT_H__
