#ifndef __ALPHABETOBJECT_H__
#define __ALPHABETOBJECT_H__

#include "DucoEngineCommon.h"
#include "PerformanceDate.h"
#include "ObjectId.h"

namespace Duco
{
class MethodDatabase;

class AlphabetObject
{
public:
    DllExport AlphabetObject(wchar_t newLetter, const Duco::ObjectId& newPealId, const Duco::ObjectId& newMethodId, const Duco::PerformanceDate& newDate);
    DllExport ~AlphabetObject();

    DllExport bool operator<(const Duco::AlphabetObject& rhs) const;
    AlphabetObject& operator=(const Duco::AlphabetObject& other);
    inline wchar_t Letter() const;
    inline const Duco::ObjectId& PealId() const;
    inline const Duco::ObjectId& MethodId() const;
    DllExport std::wstring MethodName(const Duco::MethodDatabase& methodDb) const;
    DllExport const PerformanceDate& Date() const;

protected:
    wchar_t letter;
    Duco::ObjectId pealId;
    Duco::ObjectId methodId;
    Duco::PerformanceDate date;
};

wchar_t
AlphabetObject::Letter() const
{
    return letter;
}

const Duco::ObjectId&
AlphabetObject::PealId() const
{
    return pealId;
}

const Duco::ObjectId&
AlphabetObject::MethodId() const
{
    return methodId;
}
}

inline bool operator<(const Duco::AlphabetObject& lhs, const Duco::AlphabetObject& rhs)
{
    return lhs.operator<(rhs);
}

#endif //!__ALPHABETOBJECT_H__
