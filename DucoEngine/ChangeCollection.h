#ifndef __CHANGECOLLECTION_H__
#define __CHANGECOLLECTION_H__

#include "DucoEngineCommon.h"
#include "Change.h"
#include <set>

namespace Duco
{

struct ChangeComparer
{
    bool operator() (const Duco::Change* const& lhs, const Duco::Change* const& rhs) const
    {
        return (*lhs)<(*rhs);
    }
};

class ChangeCollection
{
    friend class Music;

public:
    DllExport ChangeCollection(const Duco::Change& firstChange);
    DllExport ChangeCollection(const Duco::ChangeCollection& rhs);
    DllExport virtual ~ChangeCollection();

    // Accessors
    DllExport bool True() const;
    DllExport size_t NoOfChanges() const;
    DllExport void Clear(const Duco::Change& firstChange);
    DllExport std::wstring FalseDetails() const;
    DllExport bool EndsWithRounds() const;
    DllExport const Duco::Change& EndChange() const;
    DllExport const Duco::Change& StartingChange() const;
    DllExport bool FalseChange(Duco::Change& falseChange) const;
    DllExport bool FalseChangeNumber(size_t& falseChangeNo) const;

    inline size_t SnapStart() const;
    inline size_t SnapFinish() const;

    // Settors
    DllExport bool Add(const Duco::Change& newChange);
    inline void SetSnapStart(size_t newStart);
    inline void SetSnapFinish(size_t newFinish);

    DllExport bool operator==(const ChangeCollection& other) const;
    DllExport Duco::ChangeCollection& operator=(const Duco::ChangeCollection& other);

protected:
    void DeleteAllChanges();

protected:
    std::set<Duco::Change*, ChangeComparer>*    changes;

    size_t                    snapStartChangesMissing;
    size_t                    snapFinishChangesMissing;
    bool                      isTrue;
    bool                      endsWithRounds;
    size_t                    firstFalseChangeNumber;
    unsigned int              noOfFalseChanges;
    Duco::Change*             falseChange;
    Duco::Change*             lastChange;
    Duco::Change*             startingChange;
};

void
ChangeCollection::SetSnapStart(size_t newStart)
{
    snapStartChangesMissing = newStart;
}

size_t
ChangeCollection::SnapStart() const
{
    return snapStartChangesMissing;
}

void
ChangeCollection::SetSnapFinish(size_t newFinish)
{
    snapFinishChangesMissing = newFinish;
}

size_t
ChangeCollection::SnapFinish() const
{
    return snapFinishChangesMissing;
}

}

#endif //!__CHANGECOLLECTION_H__

