#include "TowerDatabase.h"

#include <algorithm>

#include "DatabaseSettings.h"
#include "DoveDatabase.h"
#include "DoveObject.h"
#include "DucoEngineUtils.h"
#include "FelsteadPealCount.h"
#include "ImportExportProgressCallback.h"
#include "ProgressCallback.h"
#include "PealDatabase.h"
#include "RenumberProgressCallback.h"
#include "RingingDatabase.h"
#include "StatisticFilters.h"
#include "SearchArgument.h"
#include "SearchValidationArgument.h"

#include "Tower.h"
#include "UpdateArgument.h"
#include <chrono>
#include "DucoEngineLog.h"

using namespace std::chrono;
using namespace Duco;
using namespace std;
#include <xercesc\dom\DOMElement.hpp>
XERCES_CPP_NAMESPACE_USE

TowerDatabase::TowerDatabase()
:   RingingObjectDatabase(TObjectType::ETower)
{
}

TowerDatabase::~TowerDatabase()
{
}

Duco::ObjectId
TowerDatabase::AddObject(const Duco::RingingObject& newObject)
{
    ObjectId newObjectId;
    if (newObject.ObjectType() == TObjectType::ETower)
    {
        const Duco::Tower& newTower = static_cast<const Duco::Tower&>(newObject);

        Duco::RingingObject* newTowerWithId = new Duco::Tower(newTower);
        if (AddOwnedObject(newTowerWithId))
        {
            newObjectId = newTowerWithId->Id();
        }
        // object is deleted by RingingObjectDatabase::AddObject if not added.
    }
    return newObjectId;
}

Duco::ObjectId
TowerDatabase::AddTower(const Duco::DoveObject& newTower)
{
    Tower newTowerWithoutId (newTower);
    return AddObject(newTowerWithoutId);
}

bool
TowerDatabase::SaveUpdatedObject(Duco::RingingObject& lhs, const Duco::RingingObject& rhs)
{
    if (lhs.ObjectType() == rhs.ObjectType() && rhs.ObjectType() == TObjectType::ETower)
    {
        Duco::Tower& lhsObject = static_cast<Duco::Tower&>(lhs);
        const Duco::Tower& rhsObject = static_cast<const Duco::Tower&>(rhs);
        lhsObject = rhsObject;
        return true;
    }
    return false;
}

void
TowerDatabase::ExternaliseToXml(Duco::ImportExportProgressCallback* newCallback, XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, bool exportDucoObjects) const
{
    size_t count = 0;
    DOMElement* rootElem = outputFile.getDocumentElement();
    DOMElement* newTowers = outputFile.createElement(XMLStrL("Towers"));
    rootElem->appendChild(newTowers);

    for (auto const& [key, value] : listOfObjects)
    {
        ++count;
        const Duco::Tower* theTower = static_cast<const Duco::Tower*>(value);
        theTower->ExportToXml(outputFile, *newTowers, exportDucoObjects);

        newCallback->ObjectProcessed(false, TObjectType::ETower, value->Id(), 0, count);
    }
}

bool
TowerDatabase::Internalise(DatabaseReader& reader, Duco::ImportExportProgressCallback* callback, int databaseVersionNumber, size_t noOfObjectsInThisDatabase, size_t totalObjects, size_t objectsProcessedSoFar)
{
    size_t totalProcessed = objectsProcessedSoFar;
    size_t count = 0;

    while (noOfObjectsInThisDatabase-- > 0)
    {
        ++totalProcessed;
        ++count;
        float percentageComplete = ((float)totalProcessed / (float)totalObjects) * 100;
        Duco::RingingObject* newTower = new Duco::Tower(reader, databaseVersionNumber);
        if (newTower != NULL)
        {
            if (AddOwnedObject(newTower))
            {
                if (callback != NULL)
                {
                    callback->ObjectProcessed(true, TObjectType::ETower, newTower->Id(), (int)percentageComplete, count);
                }
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    return true;
}

bool
TowerDatabase::DeleteRing(const Duco::ObjectId& towerId, const Duco::ObjectId& ringId)
{
    DUCO_OBJECTS_CONTAINER::iterator it = listOfObjects.find(towerId);
    if (it != listOfObjects.end())
    {
        Duco::Tower* theTower = static_cast<Duco::Tower*>(it->second);

        if (theTower->DeleteRing(ringId))
        {
            SetDataChanged();
            return true;
        }
    }
    return false;
}

bool
TowerDatabase::DeleteRings(const Duco::ObjectId& towerId, const std::vector<Duco::ObjectId>& ringIdsToDelete)
{
    bool changeMade (false);
    DUCO_OBJECTS_CONTAINER::iterator it = listOfObjects.find(towerId);
    if (it != listOfObjects.end())
    {
        Duco::Tower* theTower = static_cast<Duco::Tower*>(it->second);
        if (theTower->DeleteRings(ringIdsToDelete))
        {
            changeMade = true;
            SetDataChanged();
        }
    }
    return changeMade;
}

bool
TowerDatabase::ClearInvalidTenorKeys(Duco::ProgressCallback* const callback)
{
    size_t totalObjects = NumberOfObjects();
    size_t stepNumber = 0;
    bool changeMade(false);
    for (auto const& [key, value] : listOfObjects)
    {
        Duco::Tower* theTower = static_cast<Duco::Tower*>(value);
        if (theTower->ClearTenorKeysIfInvalidChars())
        {
            changeMade = true;
            SetDataChanged();
        }
        ++stepNumber;
        if (callback != NULL)
        {
            callback->Step(int(stepNumber / totalObjects));
        }
    }
    return changeMade;
}

const Tower* const
TowerDatabase::FindTower(const Duco::ObjectId& towerId, bool setIndex) const
{
    const RingingObject* const object = FindObject(towerId, setIndex);

    return static_cast<const Tower* const>(object);
}

bool
TowerDatabase::CheckTowerDetails(const Duco::Tower& theTower, unsigned int noOfBells, const std::wstring& tenorWeight, const std::wstring& tenorKey) const
{
    ObjectId ringId;
    if ((noOfBells == -1 || noOfBells == theTower.Bells()) && tenorWeight.length() == 0 && tenorKey.length() == 0)
    {
        if (!theTower.Removed())
        {
            return true;
        }
    }
    else if (theTower.SuggestRing(noOfBells, tenorWeight, tenorKey, ringId, false))
    {
        return true;
    }
    return false;
}

Duco::ObjectId
TowerDatabase::FindTowerByDoveId(const std::wstring& doveId, unsigned int noOfBells, const std::wstring& tenorWeight, const std::wstring& tenorKey) const
{
    Duco::ObjectId towerId;
    if (doveId.length() < 1)
    {
        return towerId;
    }
    for (auto const& [key, value] : listOfObjects)
    {
        Duco::Tower* theTower = static_cast<Duco::Tower*>(value);

        if (theTower != NULL && DucoEngineUtils::CompareString(theTower->DoveRef(), doveId, true))
        {
            if (CheckTowerDetails(*theTower, noOfBells, tenorWeight, tenorKey))
            {
                SetIndex(theTower->Id());
                return theTower->Id();
            }
            else
            {
                towerId = theTower->Id();
            }
        }
    }
    SetIndex(towerId);
    return towerId;
}

Duco::ObjectId
TowerDatabase::FindTowerByTowerbaseId(const std::wstring& towerbaseIdOrig, unsigned int noOfBells, const std::wstring& tenorWeight, const std::wstring& tenorKey) const
{
    const std::wstring towerbaseId = DucoEngineUtils::TrimTowerBaseId(towerbaseIdOrig);
    Duco::ObjectId towerId;
    if (towerbaseId.length() < 1)
    {
        return towerId;
    }

    for (auto const& [key, value] : listOfObjects)
    {
        Duco::Tower* theTower = static_cast<Duco::Tower*>(value);

        if (theTower != NULL && DucoEngineUtils::CompareString(theTower->TowerbaseId(), towerbaseId))
        {
            if (CheckTowerDetails(*theTower, noOfBells, tenorWeight, tenorKey))
            {
                SetIndex(theTower->Id());
                return theTower->Id();
            }
            else
            {
                towerId = theTower->Id();
            }
        }
    }
    SetIndex(towerId);
    return towerId;
}

Duco::ObjectId
TowerDatabase::SuggestTower(const std::wstring& originalFullName, unsigned int noOfBells, bool duringImport) const
{
    std::wstring name;
    std::wstring townOrCity;
    std::wstring county;
    if (!DucoEngineUtils::SplitTowerName(originalFullName, name, townOrCity, county))
    {
        return KNoId;
    }

    return SuggestTower(name, townOrCity, county, noOfBells, L"", L"", duringImport);
}

Duco::ObjectId
TowerDatabase::SuggestTower(const std::wstring& name, const std::wstring& town, const std::wstring& county, unsigned int noOfBells, const std::wstring& tenorWeight, const std::wstring& tenorKey, bool duringImport) const
{
    time_point<high_resolution_clock> start = high_resolution_clock::now();
    unsigned int bestMatchProbability(0);
    Duco::ObjectId bestMatchId;

    for (auto const& [key, value] : listOfObjects)
    {
        const Duco::Tower* theTower = static_cast<const Duco::Tower*>(value);
        unsigned int nextMatch = theTower->MatchProbability(name, town, county, noOfBells, tenorWeight, tenorKey, duringImport);
        if (nextMatch > bestMatchProbability)
        {
            bestMatchProbability = nextMatch;
            bestMatchId = theTower->Id();
        }
        else if (nextMatch == bestMatchProbability)
        {
            bestMatchId.ClearId();
        }
    }
    unsigned int requiredMatchProbability = 7;
    if (tenorWeight.length() == 0)
    {
        requiredMatchProbability -= 1;
    }
    if (tenorKey.length() == 0)
    {
        requiredMatchProbability -= 1;
    }
    if (bestMatchId.ValidId() && (bestMatchProbability < requiredMatchProbability))
    {
        bestMatchId.ClearId();
    }
    DUCOENGINEDEBUGLOG3(start, "SuggestTower from %u objects", NumberOfObjects());
    return bestMatchId;
}

void 
TowerDatabase::GetAllPlaceOptions(std::set<std::wstring>& counties, std::set<std::wstring>& cities, std::set<std::wstring>& names) const
{
    counties.clear();
    cities.clear();
    names.clear();
    for (auto const& [key, value] : listOfObjects)
    {
        const Duco::Tower* const theTower = static_cast<const Duco::Tower* const>(value);

        counties.insert(theTower->County());
        cities.insert(theTower->City());
        names.insert(theTower->Name());
    }
}

void
TowerDatabase::GetAllObjectIds(std::set<Duco::ObjectId>& parentTowerIds, std::map<Duco::ObjectId, Duco::ObjectId>& linksToParent) const
{
    parentTowerIds.clear();
    linksToParent.clear();

    for (auto const& [key, value] : listOfObjects)
    {
        const Duco::Tower* const theTower = static_cast<const Duco::Tower* const>(value);
        if (theTower->LinkedTowerId().ValidId())
        {
            std::pair<Duco::ObjectId, Duco::ObjectId> newPair (theTower->Id(), theTower->LinkedTowerId());
            linksToParent.insert(newPair);
        }
        else
        {
            parentTowerIds.insert(theTower->Id());
        }
    }

    bool changesFound (true);
    while (changesFound)
    {
        changesFound = false;
        std::map<Duco::ObjectId, Duco::ObjectId>::iterator it = linksToParent.begin();
        while (it != linksToParent.end())
        {
            std::map<Duco::ObjectId, Duco::ObjectId>::iterator it2 = linksToParent.find(it->second);
            if (it2 != linksToParent.end())
            {
                changesFound = true;
                it->second = it2->second;
            }
            ++it;
        }
    }
}

void
TowerDatabase::GetLinkedIds(const Duco::ObjectId& parentTowerIds, std::set<Duco::ObjectId>& linkedIds) const
{
    linkedIds.clear();
    std::list<std::set<Duco::ObjectId> > allLinkedTowerIds;
    GetAllLinkedIds(allLinkedTowerIds);

    std::list<std::set<Duco::ObjectId> >::iterator foundId = DucoEngineUtils::FindId(allLinkedTowerIds, parentTowerIds);
    if (foundId == allLinkedTowerIds.end())
    {
        linkedIds.insert(parentTowerIds);
    }
    else
    {
        linkedIds.insert(foundId->begin(), foundId->end());
    }
}

Duco::ObjectId
TowerDatabase::GetMasterTowerId(const std::set<Duco::ObjectId>& linkedIds) const
{
    Duco::ObjectId masterTowerId = KNoId;
    if (linkedIds.size() == 0)
    {
        return KNoId;
    }
    else if (linkedIds.size() == 1)
    {
        return *linkedIds.begin();
    }

    bool lastTowerMissingPositon = true;
    bool lastTowerRemoved = true;

    std::set<Duco::ObjectId>::const_iterator it = linkedIds.begin();
    while (it != linkedIds.end() && lastTowerMissingPositon && lastTowerRemoved)
    {
        const Tower* const theTower = FindTower(*it);
        if (!theTower->Removed() && theTower->PositionValid())
        {
            masterTowerId = theTower->Id();
            lastTowerRemoved = false;
            lastTowerMissingPositon = false;
        }
        else if (lastTowerMissingPositon && theTower->PositionValid() && !theTower->Removed())
        {
            lastTowerMissingPositon = false;
            lastTowerRemoved = theTower->Removed();
            masterTowerId = theTower->Id();
        }
        else if (lastTowerMissingPositon && lastTowerRemoved)
        {
            lastTowerMissingPositon = !theTower->PositionValid();
            lastTowerRemoved = theTower->Removed();
            masterTowerId = theTower->Id();
        }
        
        ++it;
    }

    return masterTowerId;
}

void
TowerDatabase::GetAllLinkedIds(std::list<std::set<Duco::ObjectId> >& linkedTowerIdsOnly) const
{
    linkedTowerIdsOnly.clear();
    
    std::list<std::set<Duco::ObjectId> > linkedTowerIds;
    for (auto const& [key, value] : listOfObjects)
    {
        const Duco::Tower* const theTower = static_cast<const Duco::Tower* const>(value);

        std::list<std::set<Duco::ObjectId> >::iterator towerId = DucoEngineUtils::FindId(linkedTowerIds, theTower->Id());
        std::list<std::set<Duco::ObjectId> >::iterator linkedTowerId = DucoEngineUtils::FindId(linkedTowerIds, theTower->LinkedTowerId());
        if (towerId != linkedTowerIds.end())
        {
            if (linkedTowerId != linkedTowerIds.end() && linkedTowerId != towerId)
            {
                towerId->insert(linkedTowerId->begin(), linkedTowerId->end());
                linkedTowerIds.erase(linkedTowerId);
            }
        }
        else if (linkedTowerId != linkedTowerIds.end())
        {
            linkedTowerId->insert(theTower->Id());
        }
        else
        {
            std::set<Duco::ObjectId> newList;
            Duco::ObjectId copyId = theTower->Id();
            newList.insert(copyId);
            if (theTower->LinkedTowerId().ValidId())
            {
                Duco::ObjectId copyLinkedId = theTower->LinkedTowerId();
                newList.insert(copyLinkedId);
            }
            linkedTowerIds.push_back(newList);
        }
    }
    
    // Remove all but the towers which actuall have linked ids.
    std::list<std::set<Duco::ObjectId> >::const_iterator it2 = linkedTowerIds.begin();
    while (it2 != linkedTowerIds.end())
    {
        if (it2->size() > 1)
        {
            linkedTowerIdsOnly.push_back(*it2);
        }
        ++it2;
    }
}

void
TowerDatabase::MergeTowersWithSameLocation(std::map<Duco::ObjectId, Duco::PealLengthInfo>& towerPerformanceInfo) const
{
    std::list<std::set<Duco::ObjectId>> linkedTowerIds;
    GetAllLinkedIds(linkedTowerIds);

    std::list<std::set<Duco::ObjectId> >::const_iterator linkedTowerSet = linkedTowerIds.begin();
    while (linkedTowerSet != linkedTowerIds.end())
    {
        Duco::ObjectId masterTowerId = GetMasterTowerId(*linkedTowerSet);
        const Duco::Tower* masterTower = FindTower(masterTowerId);

        std::map<Duco::ObjectId, Duco::PealLengthInfo>::iterator masterTowerData = towerPerformanceInfo.find(masterTowerId);

        std::set<Duco::ObjectId>::const_iterator towerId = linkedTowerSet->begin();
        while (masterTower != NULL && towerId != linkedTowerSet->end())
        {
            if (*towerId != masterTowerId)
            {
                const Duco::Tower* thisTower = FindTower(*towerId);
                if (!masterTower->HasDifferentPosition(*thisTower))
                {
                    // Towers have the same position - so merge data
                    std::map<Duco::ObjectId, Duco::PealLengthInfo>::iterator thisTowerData = towerPerformanceInfo.find(*towerId);
                    if (masterTowerData != towerPerformanceInfo.end() && thisTowerData != towerPerformanceInfo.end())
                    {
                        if (masterTower->TowerbaseIdMatchorNull(*thisTower))
                        {
                            masterTowerData->second += thisTowerData->second;
                            towerPerformanceInfo.erase(thisTowerData);
                        }
                    }
                }
            }
            ++towerId;
        }

        ++linkedTowerSet;
    }
}

void
TowerDatabase::GetAllObjectIds(std::set<Duco::ObjectId>& allTowerIds, std::multimap<Duco::ObjectId, Duco::ObjectId>& allRingIds) const
{
    allTowerIds.clear();
    allRingIds.clear();

    for (auto const& [key, value] : listOfObjects)
    {
        allTowerIds.insert(key);
        const Duco::Tower* const theTower = static_cast<const Duco::Tower* const>(value);

        if (theTower->NoOfRings() != 1)
        {
            std::vector<Duco::ObjectId> ringIds;
            theTower->GetRingIds(ringIds);
            std::vector<Duco::ObjectId>::const_iterator ringsIt = ringIds.begin();
            while (ringsIt != ringIds.end())
            {
                pair<Duco::ObjectId, Duco::ObjectId> newObject (key, *ringsIt);
                allRingIds.insert(newObject);
                ++ringsIt;
            }
        }
    }
}

size_t
TowerDatabase::NumberOfUniqueTowers() const
{
    size_t noOfTowers(0);
    for (auto const& [key, value] : listOfObjects)
    {
        const Duco::Tower* const theTower = static_cast<const Duco::Tower* const>(value);

        if (!theTower->Removed())
        {
            ++noOfTowers;
        }
    }
    return noOfTowers;
}


size_t
TowerDatabase::NumberOfActiveTowers(size_t& noOfLinkedTowers) const
{
    size_t noOfTowers(NumberOfObjects());
    noOfLinkedTowers = 0;

    std::list<std::set<Duco::ObjectId> > linkedTowerIds;
    GetAllLinkedIds(linkedTowerIds);

    std::list<std::set<Duco::ObjectId> >::const_iterator linkedTowerIterator = linkedTowerIds.begin();
    while (linkedTowerIterator != linkedTowerIds.end())
    {
        if ((*linkedTowerIterator).size() > 1)
        {
            noOfLinkedTowers += (*linkedTowerIterator).size();
            noOfLinkedTowers -= 1;
        }
        ++linkedTowerIterator;
    }

    noOfTowers -= noOfLinkedTowers;

    return noOfTowers;
}

size_t
TowerDatabase::NumberOfActiveTowersWithoutDoveId() const
{
    std::set<Duco::ObjectId> towerIds;
    return TowersWithoutDoveId(towerIds);
}

bool
TowerDatabase::TowersWithoutDoveId(std::set<Duco::ObjectId>& towerIds) const
{
    towerIds.clear();
    for (auto const& [key, value] : listOfObjects)
    {
        const Duco::Tower* const theTower = static_cast<const Duco::Tower* const>(value);

        if (!theTower->Removed() && theTower->DoveRef().length() <= 0)
        {
            towerIds.insert(theTower->Id());
        }
    }

    return towerIds.size() > 0;
}

bool
TowerDatabase::TowersWithDoveId(std::set<Duco::ObjectId>& towerIds, bool andBlankLocationOrTowerbaseId) const
{
    towerIds.clear();
    for (auto const& [key, value] : listOfObjects)
    {
        const Duco::Tower* const theTower = static_cast<const Duco::Tower* const>(value);

        if (!theTower->Removed() && theTower->DoveRef().length() > 0 && (!andBlankLocationOrTowerbaseId || (!theTower->PositionValid() || !theTower->ValidTowerbaseId())))
        {
            towerIds.insert(theTower->Id());
        }
    }

    return towerIds.size() > 0;
}

bool
TowerDatabase::ImportMissingTenorKeys(const Duco::DoveDatabase& doveDatabase)
{
    bool anythingChanged = false;
    for (auto const& [key, value] : listOfObjects)
    {
        Duco::Tower* theTower = static_cast<Duco::Tower*>(value);

        if (!theTower->Removed() && theTower->DoveRef().length() > 0)
        {
            Duco::DoveObject doveTower;
            if (doveDatabase.FindTower(theTower->DoveRef(), theTower->BellsForDove(), doveTower))
            {
                anythingChanged |= theTower->UpdateTenorFromDoveAndOtherIds(doveTower);
            }
        }
    }
    if (anythingChanged)
    {
        SetDataChanged();
    }

    return anythingChanged;
}

size_t
TowerDatabase::NumberOfRemovedTowers() const
{
    size_t noOfTowers (0);
    for (auto const& [key, value] : listOfObjects)
    {
        const Duco::Tower* const theTower = static_cast<const Duco::Tower* const>(value);

        if (theTower->Removed())
        {
            ++noOfTowers;
        }
    }
    return noOfTowers;
}

bool
TowerDatabase::UppercaseCities(ProgressCallback*const callback)
{
    size_t totalObjects = NumberOfObjects();
    size_t stepNumber = 0;
    bool changesMade (false);
    for (auto const& [key, value] : listOfObjects)
    {
        Duco::Tower* theTower = static_cast<Duco::Tower*>(value);

        if (theTower->UppercaseCity())
        {
            changesMade = true;
            SetDataChanged();
        }
        ++stepNumber;
        if (callback != NULL)
        {
            callback->Step(int(stepNumber / totalObjects));
        }
    }

    return changesMade;
}

void
TowerDatabase::GetTowersWithoutRings(std::set<Duco::ObjectId>& towersWithOutRings) const
{
    for (auto const& [key, value] : listOfObjects)
    {
        const Duco::Tower* const theTower = static_cast<const Duco::Tower* const>(value);
        if (theTower->NoOfRings() == 0)
        {
            towersWithOutRings.insert(key);
        }
    }
}

bool
TowerDatabase::MergeTowerRings(const Duco::ObjectId& towerId, const Duco::Tower& otherTower)
{
    const Tower* const theTower = FindTower(towerId, false);
    if (theTower == NULL)
        return false;

    Tower updatedTower (*theTower);
    if (!updatedTower.AddRings(otherTower))
        return false;

    return UpdateObject(updatedTower);
}

//***********************************************************************
// Sorting / reordering Towers
//***********************************************************************
bool
TowerDatabase::RebuildObjects(Duco::RenumberProgressCallback* callback, Duco::RingingDatabase& database)
{
    if (listOfObjects.size() <= 0)
        return false;

    bool changesMade (false);
    callback->RenumberInitialised(RenumberProgressCallback::EReindexTowers);
    std::map<Duco::ObjectId, Duco::ObjectId> newTowerIds;
    {
        if (database.Settings().AlphabeticReordering())
        {
            GetNewObjectIdsByAlphabetic(newTowerIds, database.Settings().LastNameFirst());
        }
        else
        {
            // Tower Id & Peal count.
            std::map<Duco::ObjectId, Duco::PealLengthInfo> unsortedTowerIds;
            Duco::StatisticFilters filters (database);
            database.PealsDatabase().GetTowersPealCount(filters, unsortedTowerIds);
            GetNewIdsByPealCountAndAlphabetic(unsortedTowerIds, newTowerIds, database.Settings().LastNameFirst());
        }
    }

    // Renumber all towers first.
    callback->RenumberInitialised(RenumberProgressCallback::ERenumberTowers);
    RenumberObjects(newTowerIds, callback);

    // Renumber all linkedId Towers
    {
        for (auto const& [key, value] : listOfObjects)
        {
            Duco::Tower* theTower = static_cast<Duco::Tower*>(value);
            if (theTower->LinkedTowerId().ValidId())
            {
                std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator it3 = newTowerIds.find(theTower->LinkedTowerId());
                if (it3 != newTowerIds.end())
                {
                    theTower->SetLinkedTowerId(it3->second);
                }
            }
        }
    }

    // Renumber towerids in Peals
    callback->RenumberInitialised(RenumberProgressCallback::ERebuildTowers);
    database.PealsDatabase().RenumberTowersInPeals(newTowerIds, callback);

    callback->RenumberInitialised(Duco::RenumberProgressCallback::ERebuildRings);
    {
        for (auto const& [key, value] : listOfObjects)
        {
            Duco::Tower* theTower = static_cast<Duco::Tower*>(value);

            if (theTower->NoOfRings() >= 1)
            {
                std::map<Duco::ObjectId, Duco::ObjectId> oldRingIdsToNewRingIds;
                if (theTower->RenumberRings(oldRingIdsToNewRingIds))
                {
                    SetDataChanged();
                    changesMade |= true;
                    database.PealsDatabase().RenumberRingsInPeals(key, oldRingIdsToNewRingIds);
                }
            }
            callback->RenumberStep(key.Id(), listOfObjects.size());
        }
    }
    return changesMade;
}

void
TowerDatabase::AddMissingTowers(std::map<Duco::ObjectId, Duco::PealLengthInfo>& towerIds, const Duco::StatisticFilters& filters) const
{
    for (auto const& [key, value] : listOfObjects)
    {
        std::map<Duco::ObjectId, Duco::PealLengthInfo>::const_iterator alreadyAdded = towerIds.find(key);
        if (alreadyAdded == towerIds.end())
        {
            bool addTower = true;
            if (filters.EuropeOnly())
            {
                const Tower* theTower = FindTower(key, false);
                if (theTower != NULL && !theTower->InEurope())
                {
                    addTower = false;
                }
            }

            if (addTower)
            {
                Duco::PealLengthInfo newInfo;
                pair<Duco::ObjectId, Duco::PealLengthInfo> newObject(key, newInfo);
                towerIds.insert(newObject);
            }
        }
    }
}

bool
TowerDatabase::MatchObject(const Duco::RingingObject& object, const Duco::TSearchArgument& searchArg, const Duco::RingingDatabase& database) const
{
    if (object.ObjectType() != TObjectType::ETower)
        return false;
    return searchArg.Match(static_cast<const Duco::Tower&>(object), database);
}

bool
TowerDatabase::ValidateObject(const Duco::RingingObject& object, const Duco::TSearchValidationArgument& searchArg, const std::set<Duco::ObjectId>& idsToCheckAgainst, const Duco::RingingDatabase& database) const
{
    if (object.ObjectType() != TObjectType::ETower)
        return false;
    return searchArg.Match(static_cast<const Duco::Tower&>(object), idsToCheckAgainst, database);
}

unsigned int
TowerDatabase::NoOfBellsInRing(const Duco::ObjectId& towerId, const Duco::ObjectId& ringId)
{
    const Duco::Tower* const theTower = FindTower(towerId, true);
    if (theTower != NULL)
    {
        return theTower->NoOfBellsInRing(ringId);
    }
    return -1;
}

bool
TowerDatabase::RebuildRecommended() const
{
    if (RingingObjectDatabase::RebuildRecommended())
        return true;

    for (auto const& [key, value] : listOfObjects)
    {
        if (value->RebuildRecommended())
            return true;

    }
    return false;
}

void
TowerDatabase::RemoveUsedIds(std::set<Duco::ObjectId>& unusedTowers, const DatabaseSettings& settings) const
{
    for (auto const& [key, value] : listOfObjects)
    {
        const Tower* const theTower = static_cast<const Tower* const>(value);
        if (settings.KeepTowersWithRungDate() && theTower->FirstRungDateSet())
        {
            set<Duco::ObjectId>::iterator it2 = unusedTowers.find(theTower->Id());
            if (it2 != unusedTowers.end())
            {
                unusedTowers.erase(it2);
            }
        }
        if (theTower->LinkedTowerId().ValidId())
        {
            set<Duco::ObjectId>::iterator it3 = unusedTowers.find(theTower->LinkedTowerId());
            if (it3 != unusedTowers.end())
            {
                unusedTowers.erase(it3);
            }
            it3 = unusedTowers.find(theTower->Id());
            if (it3 != unusedTowers.end())
            {
                unusedTowers.erase(it3);
            }
        }
    }
}

bool
TowerDatabase::RemoveDuplicateRings(PealDatabase& pealsDb, ProgressCallback*const callback)
{
    size_t stepNumber = 0;
    const size_t totalObjects = NumberOfObjects();
    bool modified (false);
    for (auto const& [key, value] : listOfObjects)
    {
        Tower* const theTower = static_cast<Tower* const>(value);
        std::map<Duco::ObjectId, Duco::ObjectId> duplicateRingReplacements; // old id, new id.
        if (theTower->RemoveDuplicateRings(duplicateRingReplacements))
        {
            modified = true;
            SetDataChanged();
            pealsDb.UpdateRingIds(theTower->Id(), duplicateRingReplacements);
        }
        ++stepNumber;
        if (callback)
        {
            callback->Step(int(stepNumber / totalObjects));
        }
    }
    return modified;
}

bool
TowerDatabase::CheckLinkedTowerId(const Duco::ObjectId& towerId, const Duco::ObjectId& towerIdToLink, std::wstring& errorString) const
{
    errorString.clear();
    if (!towerId.ValidId())
        return true;
    if (!towerIdToLink.ValidId())
    {
        errorString = L"Invalid tower id";
        return false;
    }

    const Tower* nextTower = FindTower(towerIdToLink, false);
    if (nextTower == NULL)
    {
        errorString = L"Cannot find that tower";
        return false;
    }
    else if (nextTower->Removed())
    {
        errorString = L"Cannot link to a removed tower";
        return false;
    }
    while (nextTower != NULL)
    {
        if (nextTower->LinkedTowerId() == towerId)
        {
            errorString = L"That would create a circular tower reference";
            return false;
        }
        else if (!nextTower->LinkedTowerId().ValidId())
        {
            return true;
        }
        nextTower = FindTower(nextTower->LinkedTowerId(), false);
    }

    return true;
}

bool
TowerDatabase::UpdateObjectField(const Duco::ObjectId& id, const Duco::TUpdateArgument& updateArgument)
{
    DUCO_OBJECTS_CONTAINER::iterator it = listOfObjects.find(id);
    if (it == listOfObjects.end())
        return false;

    Duco::Tower* obj = static_cast<Duco::Tower*>(it->second);

    return updateArgument.Update(*obj);
}

bool
TowerDatabase::CapitaliseField(Duco::ProgressCallback& progressCallback, bool county, bool dedication, size_t& numberOfObjectsUpdated, size_t numberOfObjectsToUpdate)
{
    bool changes = false;
    for (auto const& [key, value] : listOfObjects)
    {
        Tower* const theTower = static_cast<Tower* const>(value);
        if (theTower->CapitaliseField(county, dedication))
        {
            SetDataChanged();
            changes = true;
        }
        ++numberOfObjectsUpdated;
        progressCallback.Step(int(((float)numberOfObjectsUpdated / (float)numberOfObjectsToUpdate) * 100));
    }
    return changes;
}

bool
TowerDatabase::RingsIdentical(const Duco::ObjectId& lhs, const Duco::ObjectId& rhs) const
{
    std::map<Duco::ObjectId, Duco::ObjectId> otherRingIds;
    if (!RingTranslations(lhs, rhs, otherRingIds))
        return false;

    const Tower* lhsTower = FindTower(lhs, false);
    if (lhsTower == NULL || lhsTower->NoOfRings() != otherRingIds.size())
        return false;

    const Tower* rhsTower = FindTower(rhs, false);
    if (rhsTower == NULL || rhsTower->NoOfRings() != otherRingIds.size())
        return false;

    return true;
}

bool
TowerDatabase::RingTranslations(const Duco::ObjectId& lhs, const Duco::ObjectId& rhs, std::map<Duco::ObjectId, Duco::ObjectId>& otherRingIds) const
{
    const Tower* lhsTower = FindTower(lhs, false);
    const Tower* rhsTower = FindTower(rhs, false);
    if (lhsTower == NULL || rhsTower == NULL || (*lhsTower) == *rhsTower)
    {
        return false;
    }
    return lhsTower->RingTranslations(*rhsTower, otherRingIds);
}

void
TowerDatabase::FindPicture(const Duco::ObjectId& pictureId, std::set<Duco::ObjectId>& towerIds) const
{
    towerIds.clear();
    if (pictureId.ValidId())
    {
        for (auto const& [key, value] : listOfObjects)
        {
            const Duco::Tower* theTower = static_cast<const Duco::Tower*>(value);
            if (theTower->PictureId() == pictureId)
            {
                towerIds.insert(theTower->Id());
            }
        }
    }
}

bool
TowerDatabase::RemovePictures(const std::set<Duco::ObjectId>& pictureIds)
{
    for (auto const& [key, value] : listOfObjects)
    {
        Duco::Tower* theTower = static_cast<Duco::Tower*>(value);
        if (pictureIds.find(theTower->PictureId()) != pictureIds.end())
        {
            theTower->SetPictureId(KNoId);
            SetDataChanged();
        }
    }

    return true;
}

void
TowerDatabase::GetPictureIds(std::map<Duco::ObjectId, Duco::ObjectId>& sortedPictureIds) const
{
    sortedPictureIds.clear();
    for (auto const& [key, value] : listOfObjects)
    {
        const Duco::Tower* theTower = static_cast<const Duco::Tower*>(value);
        if (theTower->PictureId().ValidId())
        {
            std::pair<Duco::ObjectId, Duco::ObjectId> newObject(theTower->Id(), theTower->PictureId());
            sortedPictureIds.insert(newObject);
        }
    }
}

bool
TowerDatabase::GetPictureIdForTower(const Duco::ObjectId& towerId, Duco::ObjectId& pictureId) const
{
    pictureId.ClearId();
    const Tower* const theTower = FindTower(towerId);
    if (theTower != NULL)
    {
        if (theTower->PictureId().ValidId())
        {
            pictureId = theTower->PictureId();
            return true;
        }
    }
    return false;
}


void
TowerDatabase::RemoveUsedPictureIds(std::set<Duco::ObjectId>& unusedPictureIds) const
{
    // Remove used towers / methods and rings from lists.
    for (auto const& [key, value] : listOfObjects)
    {
        const Tower* const theTower = static_cast<const Tower* const>(value);
        std::set<Duco::ObjectId>::iterator it = unusedPictureIds.find(theTower->PictureId());
        if (it != unusedPictureIds.end())
        {
            unusedPictureIds.erase(it);
        }
        if (unusedPictureIds.size() == 0)
            return;
    }
}

bool
TowerDatabase::Position(const Duco::ObjectId& towerId, std::wstring& latitudeLongitude) const
{
    latitudeLongitude.clear();
    const Tower* const theTower = FindTower(towerId);
    if (theTower != NULL)
    {
        if (theTower->Latitude().length() > 0 && theTower->Longitude().length() > 0)
        {
            latitudeLongitude = theTower->Latitude();
            latitudeLongitude += L",";
            latitudeLongitude += theTower->Longitude();
            return true;
        }
    }
    return false;
}

bool
TowerDatabase::RenumberPicturesInTowers(const std::map<Duco::ObjectId, Duco::ObjectId>& sortedPictureIds, Duco::RenumberProgressCallback* callback)
{
    bool anyChangesMade(false);

    callback->RenumberInitialised(RenumberProgressCallback::ERebuildPictures);
    for (auto const& [key, value] : listOfObjects)
    {
        Duco::Tower* theTower = static_cast<Duco::Tower*>(value);
        std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator renumberPictureObj = sortedPictureIds.find(theTower->PictureId());
        if (renumberPictureObj != sortedPictureIds.end() && renumberPictureObj->first != renumberPictureObj->second)
        {
            anyChangesMade = true;
            theTower->SetPictureId(renumberPictureObj->second);
        }
    }

    return anyChangesMade;
}

void
TowerDatabase::GetFelsteadPealCount(std::map<Duco::ObjectId, Duco::PealLengthInfo>& sortedTowerIds, const Duco::DatabaseSettings& settings, const Duco::StatisticFilters& filters, bool combineLinkedTowers) const
{
    sortedTowerIds.clear();
    std::list<Duco::FelsteadPealCount*> threads;
    std::map<Duco::ObjectId, std::wstring> towerIdsToFelsteadId;

    for (auto const& [key, value] : listOfObjects)
    {
        const Duco::Tower* theTower = static_cast<const Duco::Tower*>(value);
        if (filters.Match(*theTower) && theTower->PositionValid() && theTower->ValidTowerbaseId())
        {
            FelsteadPealCount* pealCounter = new FelsteadPealCount(settings, std::ref(*theTower));
            pealCounter->StartDownload();
            threads.push_back(pealCounter);

            towerIdsToFelsteadId.insert(std::pair<Duco::ObjectId, std::wstring>(theTower->Id(), theTower->TowerbaseId()));
        }
    }
    std::list<Duco::FelsteadPealCount*>::iterator threadIt = threads.begin();
    while (threadIt != threads.end())
    {
        (*threadIt)->Wait();
        if ((*threadIt)->Success())
        {
            Duco::PealLengthInfo newInfo(**threadIt);
            std::pair<Duco::ObjectId, Duco::PealLengthInfo> newObject((*threadIt)->TowerId(), newInfo);
            sortedTowerIds.insert(newObject);
        }
        delete (*threadIt);
        ++threadIt;
    }

    if (combineLinkedTowers && sortedTowerIds.size() > 0)
    {
        CombineLinkedTowerData(sortedTowerIds);
    }
}

void
TowerDatabase::CombineLinkedTowerData(std::map<ObjectId, Duco::PealLengthInfo>& towersPealData) const
{
    std::list<std::set<Duco::ObjectId> > linkedTowerIds;
    GetAllLinkedIds(linkedTowerIds);
    std::list<std::set<Duco::ObjectId> >::const_iterator linkedTowerSet = linkedTowerIds.begin();
    while (linkedTowerSet != linkedTowerIds.end())
    {
        std::set<Duco::ObjectId>::const_reverse_iterator towerIds = linkedTowerSet->rbegin();
        Duco::ObjectId parentId = GetMasterTowerId(*linkedTowerSet);
        std::map<Duco::ObjectId, Duco::PealLengthInfo>::iterator parentTowerData = towersPealData.find(parentId);
        while (towerIds != linkedTowerSet->rend())
        {
            if (*towerIds != parentId)
            {
                std::map<Duco::ObjectId, Duco::PealLengthInfo>::iterator linkedTowerData = towersPealData.find(*towerIds);
                if (parentTowerData != towersPealData.end() && linkedTowerData != towersPealData.end())
                {
                    parentTowerData->second += linkedTowerData->second;
                    towersPealData.erase(linkedTowerData);
                }
            }
            ++towerIds;
        }

        ++linkedTowerSet;
    }
}


bool
TowerDatabase::SetTowersAsHandbell(const std::set<Duco::ObjectId>& towerIds)
{
    bool towerChanged = false;
    for (auto const& [key, value] : listOfObjects)
    {
        Duco::Tower* theTower = static_cast<Duco::Tower*>(value);
        if (towerIds.find(theTower->Id()) != towerIds.end())
        {
            if (!theTower->Handbell())
            {
                theTower->SetHandbell(true);
                SetDataChanged();
                towerChanged = true;
            }
        }
    }
    return towerChanged;
}
