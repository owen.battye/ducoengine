#ifndef __SEARCHFIELDTIMEARGUMENT_H__
#define __SEARCHFIELDTIMEARGUMENT_H__

#include "SearchArgument.h"
#include "PerformanceTime.h"

namespace Duco
{
    class Peal;
    class Tower;

class TSearchFieldTimeArgument : public TSearchArgument
{
public:
    DllExport explicit TSearchFieldTimeArgument(Duco::TSearchFieldId fieldId, const Duco::PerformanceTime& newFieldValue, Duco::TSearchType newSearchType = EEqualTo);
    virtual bool Match(const Duco::Peal& object, const Duco::RingingDatabase& database) const;
    Duco::TFieldType FieldType() const;
 
protected:
    bool CompareTime(const Duco::PerformanceTime& pealTime) const;

protected:
    const Duco::PerformanceTime fieldValue;
    const Duco::TSearchType     searchType;
};

}
#endif // __SEARCHFIELDTIMEARGUMENT_H__
