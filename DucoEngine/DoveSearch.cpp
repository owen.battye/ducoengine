#include "DoveDatabase.h"
#include "DoveObject.h"
#include "DoveSearch.h"
#include "RingingDatabase.h"
#include "DucoEngineUtils.h"
#include "ImportCancellationCallback.h"

using namespace std;
using namespace Duco;

DoveSearch::DoveSearch(const Duco::RingingDatabase& theDatabase, const std::string& filename, Duco::DoveSearchCallback* newCallback, const std::string& installationDir)
:   doveDatabase(NULL), database(theDatabase), callback(newCallback), doveFilename(filename), ducoInstallationDir(installationDir), noOfBells(-1),
    atLeastStage(false), ignoreTowersRung(false), onlyIgnoreTowersWhereAllBellsRung(false), ukOnly(false), cathedralsOnly(false), antiClockwiseOnly(false),
    cancellationCheck(NULL)
{
}

DoveSearch::~DoveSearch()
{
    delete doveDatabase;
}

bool
DoveSearch::Search(const std::wstring& searchStr,
                  unsigned int noOfBellsInTower,
                  bool newAtLeastStage,
                  bool ignoreTowersAlreadyRung,
                  bool newOnlyIgnoreTowersWhereAllBellsRung,
                  bool newUkOnly,
                  bool newCathedralsOnly,
                  bool newIgnoreUnringable,
                  bool newAntiClockwiseOnly)
{
    delete doveDatabase;
    doveDatabase = new Duco::DoveDatabase(doveFilename, callback, ducoInstallationDir, true);
    doveDatabase->RegisterCallBack(this);

    if (cancellationCheck != NULL)
    {
        doveDatabase->RegisterCancellationCallBack(cancellationCheck);
    }

    searchString = searchStr;
    noOfBells = noOfBellsInTower;
    ignoreTowersRung = ignoreTowersAlreadyRung;
    onlyIgnoreTowersWhereAllBellsRung = newOnlyIgnoreTowersWhereAllBellsRung;
    ukOnly = newUkOnly;
    cathedralsOnly = newCathedralsOnly;
    atLeastStage = newAtLeastStage;
    ignoreUnringable = newIgnoreUnringable;
    antiClockwiseOnly = newAntiClockwiseOnly;

    doveDatabase->Import();
    if (callback != NULL)
    {
        callback->Complete(false);
    }

    return doveDatabase->NoOfTowers() > 0;
}

void
DoveSearch::RegisterCancellationCallBack(Duco::ImportCancellationCallback* newCallback)
{
    cancellationCheck = newCallback;
    if (doveDatabase != NULL)
    {
        doveDatabase->RegisterCancellationCallBack(cancellationCheck);
    }
}

bool
DoveSearch::IncludeTower(const Duco::DoveObject& nextTower)
{
    bool includeTower(true);

    if (includeTower && ignoreUnringable)
    {
        includeTower = !nextTower.Unringable();
    }
    if (includeTower && noOfBells > 0 && noOfBells != -1)
    {
        if (atLeastStage)
        {
            includeTower = nextTower.Bells() >= noOfBells;
        }
        else
        {
            includeTower = nextTower.Bells() == noOfBells;
        }
    }
    if (includeTower && ukOnly)
    {
        includeTower = nextTower.UKorIreland();
    }
    if (includeTower && cathedralsOnly)
    {
        includeTower = (DucoEngineUtils::FindSubstring(L"cathedral", nextTower.Dedication()) || DucoEngineUtils::FindSubstring(L"cath ", nextTower.Dedication()));
    }
    if (includeTower && searchString.length() > 0)
    {
        includeTower = DucoEngineUtils::FindSubstring(searchString, nextTower.FullName());
    }
    if (includeTower && ignoreTowersRung)
    {
        includeTower = !database.FindPealInTower(nextTower, onlyIgnoreTowersWhereAllBellsRung).ValidId();
    }
    if (includeTower && antiClockwiseOnly)
    {
        includeTower = nextTower.AntiClockwise();
    }

    if (includeTower)
    {
        try
        {
            if (callback != NULL)
            {
                callback->TowerFound(nextTower);
            }
        }
        catch (exception*)
        {
        }
    }

    return includeTower;
}

bool
DoveSearch::FindTower(const std::wstring& doveId, std::set<Duco::DoveObject>& foundTowers) const
{
    foundTowers.clear();
    if (doveDatabase != NULL)
    {
        return doveDatabase->FindTower(doveId, foundTowers);
    }
    return false;
}

const Duco::DoveDatabase&
DoveSearch::DoveDatabase() const
{
    return *doveDatabase;
}
