#ifndef __PLACENOTATIONPROCESSOR_H__
#define __PLACENOTATIONPROCESSOR_H__

#include "DucoEngineCommon.h"
#include <string>
#include "ChangeType.h"

namespace Duco
{
    class Method;
    class PlaceNotation;
    class PlaceNotationObject;

class PlaceNotationProcessor
{
    public:
        DllExport PlaceNotationProcessor(Duco::Method& newMethod);
        DllExport ~PlaceNotationProcessor();
        /* Returns true if the notation has to be modified to process */
        DllExport bool ProcessNotation(PlaceNotation& notations);

        static PlaceNotationObject* CreateNotationObject(const std::wstring& notation, TChangeType newType = EOther);

    protected:
        bool MoreNotation();
        std::wstring NextNotation();

        bool IsSpecialCharacter(const wchar_t& aNextCharacter) const;
        bool IsEndCharacter(unsigned int aPreviousCharacter, const wchar_t& aNextCharacter) const;

        // Member data
        Duco::Method&           currentMethod;

        unsigned int	        conversionPosition;
	};

} // end namespace Duco

#endif //!__PLACENOTATIONPROCESSOR_H__
