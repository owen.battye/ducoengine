#ifndef __PLACENOTATION_H__
#define __PLACENOTATION_H__

#include <string>
#include <list>

#include "DucoEngineCommon.h"
#include "LeadType.h"
#include "LeadOrder.h"

namespace Duco
{
    class Change;
    class ChangeCollection;
    class Lead;
    class LeadEndCollection;
    class Method;
    class MethodDatabase;
    class PlaceNotationObject;
    class ObjectId;

class PlaceNotation
{
    public:
        DllExport PlaceNotation(Duco::Method& newMethod, size_t callingId);
        DllExport PlaceNotation(Duco::PlaceNotation& other);
        DllExport ~PlaceNotation();

        DllExport const std::wstring& Notation() const;
        DllExport std::wstring FullNotation(bool includeSymetricIndicator = false, bool includeLeadEnd = true) const;
        DllExport size_t ChangesPerLead() const;

        DllExport unsigned int BestStartingLead(Duco::MethodDatabase& methodDb) const;
        inline bool NotationUpdated() const;
        DllExport bool CallsAffectHome() const;

        DllExport bool CreateStartingChange(Duco::Change& fromChange, const size_t snapStartPos, const TLeadType leadType) const;
        DllExport Duco::Lead* CreateLead(size_t snapStartPos) const;
        DllExport Duco::Lead* CreateLead(const Duco::Change& fromChange, size_t snapStartPos) const;
        DllExport Duco::Lead* CreateNextLead(Duco::Lead*& fromLead) const;
        DllExport bool CreateNextLeadEndChange(Duco::TLeadType type, Duco::Change& endChange) const;
        DllExport bool AdvanceToRounds(Duco::ChangeCollection& changes, Duco::LeadEndCollection& leadEnds) const;

        void AddNotation(Duco::PlaceNotationObject* newObject);
        bool SetLeadHeadAndEnd();
        void ReverseAndDuplicate();
        void DeleteAllNotations();

        DllExport const Duco::LeadOrder& LeadOrder() const;
        DllExport Duco::LeadOrder& LeadOrder();

        DllExport const Duco::ObjectId&   MethodId() const;
        DllExport unsigned int      Order() const;
        DllExport std::wstring       PlainLeadEndNotation() const;
        DllExport bool              HasNotationForCallingType(TLeadType type) const;

    protected:
        void SetLeadOrder();
        Duco::LeadOrder LeadOrderByPlain() const;
        Duco::LeadOrder LeadOrderByBob() const;

        // Member data
        Duco::Method&           currentMethod;
        size_t                  callingId;

        Duco::LeadOrder         leadOrder;
        std::list<Duco::PlaceNotationObject*> notations;

        bool                    symetric;
        bool                    notationUpdated;
    };

bool
PlaceNotation::NotationUpdated() const
{
    return notationUpdated;
}

} // end namespace Duco

#endif //!__PLACENOTATION_H__
