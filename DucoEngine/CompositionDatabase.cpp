#include "CompositionDatabase.h"

#include "DucoEngineUtils.h"
#include "Composition.h"
#include "ImportExportProgressCallback.h"
#include "RenumberProgressCallback.h"
#include "SearchArgument.h"
#include "SearchValidationArgument.h"
#include "ProgressCallback.h"
#include "RingingDatabase.h"
#include "DatabaseSettings.h"
#include "PealDatabase.h"
#include <xercesc\dom\DOMElement.hpp>

using namespace Duco;
using namespace std;

XERCES_CPP_NAMESPACE_USE

CompositionDatabase::CompositionDatabase()
:   RingingObjectDatabase(TObjectType::EComposition)
{
}

CompositionDatabase::~CompositionDatabase()
{
}

Duco::ObjectId
CompositionDatabase::AddObject(const Duco::RingingObject& newObject)
{
    Duco::ObjectId foundObjectId;
    if (newObject.ObjectType() == TObjectType::EComposition)
    {
        const Duco::Composition& newComposition = static_cast<const Duco::Composition&>(newObject);

        Duco::RingingObject* newCompositionWithId = new Duco::Composition(newComposition);
        if (AddOwnedObject(newCompositionWithId))
        {
            foundObjectId = newCompositionWithId->Id();
        }
        // object is deleted by RingingObjectDatabase::AddObject if not added.
    }
    return foundObjectId;
}

bool
CompositionDatabase::SaveUpdatedObject(Duco::RingingObject& lhs, const Duco::RingingObject& rhs)
{
    if (lhs.ObjectType() == rhs.ObjectType() && rhs.ObjectType() == TObjectType::EComposition)
    {
        Duco::Composition& lhsComposition = static_cast<Duco::Composition&>(lhs);
        const Duco::Composition& rhsComposition = static_cast<const Duco::Composition&>(rhs);
        lhsComposition = rhsComposition;
        return true;
    }
    return false;
}

#ifndef __ANDROID__
void
CompositionDatabase::ExternaliseToXml(Duco::ImportExportProgressCallback* newCallback, XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, bool exportDucoObjects) const
{
    DOMElement* rootElem = outputFile.getDocumentElement();

    DOMElement* newCompositions = outputFile.createElement(XMLStrL("Compositions"));
    rootElem->appendChild(newCompositions);

    size_t count = 0;
    DUCO_OBJECTS_CONTAINER::const_iterator compositionIt = listOfObjects.begin();
    while (compositionIt != listOfObjects.end())
    {
        const Duco::Composition* theComposition = static_cast<const Duco::Composition*>(compositionIt->second);
        theComposition->ExportToXml(outputFile, *newCompositions, exportDucoObjects);
        newCallback->ObjectProcessed(false, TObjectType::EComposition, compositionIt->second->Id(), 0, ++count);
        ++compositionIt;
    }
}
#endif

bool
CompositionDatabase::Internalise(DatabaseReader& reader, Duco::ImportExportProgressCallback* callback, int databaseVersionNumber, size_t noOfObjectsInThisDatabase, size_t totalObjects, size_t objectsProcessedSoFar)
{
    size_t totalProcessed = objectsProcessedSoFar;
    size_t count = 0;

    while (noOfObjectsInThisDatabase-- > 0)
    {
        ++totalProcessed;
        ++count;
        float percentageComplete = ((float)totalProcessed / (float)totalObjects) * 100;

        Duco::RingingObject* newComposition = new Duco::Composition(reader, databaseVersionNumber);
        if (newComposition == NULL)
            return false;
        AddOwnedObject(newComposition);
        if (callback != NULL)
        {
            callback->ObjectProcessed(true, TObjectType::EComposition, newComposition->Id(), (int)percentageComplete, count);
        }
    }
    return true;
}

const Composition* const
CompositionDatabase::FindComposition(const Duco::ObjectId& compositionId, bool setIndex) const
{
    const RingingObject* const object = FindObject(compositionId, setIndex);

    return static_cast<const Composition* const>(object);
}

bool
CompositionDatabase::MatchObject(const Duco::RingingObject& object, const Duco::TSearchArgument& searchArg, const Duco::RingingDatabase& database) const
{
    if (object.ObjectType() != TObjectType::EComposition)
        return false;
    return searchArg.Match(static_cast<const Duco::Composition&>(object), database);
}

bool
CompositionDatabase::ValidateObject(const Duco::RingingObject& object, const Duco::TSearchValidationArgument& searchArg, const std::set<Duco::ObjectId>& idsToCheckAgainst, const Duco::RingingDatabase& database) const
{
    if (object.ObjectType() != TObjectType::EComposition)
        return false;
    return searchArg.Match(static_cast<const Duco::Composition&>(object), idsToCheckAgainst, database);
}

//***********************************************************************
// Sorting / reordering Methods
//***********************************************************************
bool
CompositionDatabase::RebuildObjects(Duco::RenumberProgressCallback* callback, Duco::RingingDatabase& database)
{
    if (listOfObjects.size() <= 0)
        return false;

    callback->RenumberInitialised(RenumberProgressCallback::EReindexCompositions);
    std::map<Duco::ObjectId, Duco::ObjectId> newCompositionIds;
    {
        if (database.Settings().AlphabeticReordering())
            GetNewObjectIdsByAlphabetic(newCompositionIds, database.Settings().LastNameFirst());
        else
        {
            std::map<Duco::ObjectId, Duco::PealLengthInfo> compositionPealCounts;
            database.PealsDatabase().GetCompositionsPealCount(compositionPealCounts);
            GetNewIdsByPealCountAndAlphabetic(compositionPealCounts, newCompositionIds, database.Settings().LastNameFirst());
        }
    }

    // Renumber all compositions first.
    callback->RenumberInitialised(RenumberProgressCallback::ERenumberCompositions);
    bool changesMade = RenumberObjects(newCompositionIds, callback);

    // Renumber methodids in Peals
    callback->RenumberInitialised(RenumberProgressCallback::ERebuildCompositions);
    changesMade |= database.PealsDatabase().RenumberCompositionsInPeals(newCompositionIds, callback);
    return changesMade;
}

void
CompositionDatabase::RemoveUsedIds(std::set<Duco::ObjectId>& unusedRingers, std::set<Duco::ObjectId>& unusedMethods) const
{
    DUCO_OBJECTS_CONTAINER::const_iterator it = listOfObjects.begin();
    while (it != listOfObjects.end())
    {
        const Composition* const theComposition = static_cast<const Composition* const>(it->second);

        std::set<Duco::ObjectId>::iterator it2 = unusedRingers.find(theComposition->ComposerId());
        if (it2 != unusedRingers.end())
        {
            unusedRingers.erase(it2);
        }
        std::set<Duco::ObjectId>::iterator it3 = unusedMethods.find(theComposition->MethodId());
        if (it3 != unusedMethods.end())
        {
            unusedMethods.erase(it3);
        }

        ++it;
    }
}

bool
CompositionDatabase::RenumberMethodsInCompositions(const std::map<Duco::ObjectId, Duco::ObjectId>& newMethodIds, Duco::RenumberProgressCallback* callback)
{
    bool changesMade (false);
    DUCO_OBJECTS_CONTAINER::iterator it = listOfObjects.begin();
    size_t count (0);
    while (it != listOfObjects.end())
    {
        Composition* nextComposition = static_cast<Composition*>(it->second);

        Duco::ObjectId oldMethodId = nextComposition->MethodId();
        std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator oldMethodIdIt = newMethodIds.find(oldMethodId);
        if (oldMethodIdIt != newMethodIds.end())
        {
            Duco::ObjectId newMethodId = oldMethodIdIt->second;
            if (newMethodId != oldMethodId)
            {
                nextComposition->SetMethodId(newMethodId);
                SetDataChanged();
                changesMade = true;
            }
        }
        callback->RenumberStep(++count, NumberOfObjects());
        ++it;
    }
    return changesMade;
}

bool
CompositionDatabase::RenumberRingersInCompositions(const std::map<Duco::ObjectId, Duco::ObjectId>& newRingerIds, Duco::RenumberProgressCallback* callback)
{
    bool changesMade (false);
    DUCO_OBJECTS_CONTAINER::iterator it = listOfObjects.begin();
    size_t count (0);
    while (it != listOfObjects.end())
    {
        Composition* nextComposition = static_cast<Composition*>(it->second);

        Duco::ObjectId oldRingerId = nextComposition->ComposerId();
        std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator oldRingerIdIt = newRingerIds.find(oldRingerId);
        if (oldRingerIdIt != newRingerIds.end())
        {
            Duco::ObjectId newRingerId = oldRingerIdIt->second;
            if (newRingerId != oldRingerId)
            {
                nextComposition->SetComposerId(newRingerId);
                SetDataChanged();
                changesMade = true;
            }
        }
        callback->RenumberStep(++count, NumberOfObjects());
        ++it;
    }
    return changesMade;
}

void
CompositionDatabase::GetCompositionsByNoOfBells(std::set<Duco::ObjectId>& compositionIds, const Duco::MethodDatabase& methodDb, unsigned int noOfBells) const
{
    compositionIds.clear();

    DUCO_OBJECTS_CONTAINER::const_iterator it = listOfObjects.begin();
    while (it != listOfObjects.end())
    {
        const Composition* const theComposition = static_cast<const Composition* const>(it->second);

        if (theComposition != NULL)
        {
            if (noOfBells == -1 || DucoEngineUtils::NoOfBellsMatch(theComposition->Order(methodDb), noOfBells))
            {
                // method must be on the number of bells or the number of bells -1 for odd bell methods
                compositionIds.insert(theComposition->Id());
            }
        }
        ++it;
    }
}

void
CompositionDatabase::AddMissingCompositions(std::map<Duco::ObjectId, Duco::PealLengthInfo>& compositionIds) const
{
    DUCO_OBJECTS_CONTAINER::const_iterator compositionIt = listOfObjects.begin();
    while (compositionIt != listOfObjects.end())
    {
        std::map<Duco::ObjectId, Duco::PealLengthInfo>::const_iterator alreadyAdded = compositionIds.find(compositionIt->first);
        if (alreadyAdded == compositionIds.end())
        {
            Duco::PealLengthInfo emptyInfo;
            pair<Duco::ObjectId, Duco::PealLengthInfo> newObject(compositionIt->first, emptyInfo);
            compositionIds.insert(newObject);
        }
        ++compositionIt;
    }
}

bool
CompositionDatabase::UpdateObjectField(const Duco::ObjectId&, const Duco::TUpdateArgument&)
{
    return false;
}

