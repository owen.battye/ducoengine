#include "PealLengthInfo.h"

#include <limits>
#include <format>

#include "Peal.h"
#include "FelsteadPealCount.h"

using namespace Duco;
using namespace std;

PealLengthInfo::PealLengthInfo()
:   longestChangesId (KNoId),
    longestMinutesId (KNoId),
    slowestCPMId (KNoId),
    shortestChangesId (KNoId),
    shortestMinutesId (KNoId),
    fastestCPMId(KNoId),
    totalPeals(0),
    totalPealsWithChanges(0),
    totalPealsWithTime(0),
    totalChanges (0),
    totalMinutes(0),
    totalMinutesWithTimeAndMinutes(0),
    totalChangesWithTimeAndMinutes(0),
    firstPealId(KNoId),
    lastPealId(KNoId),
    fastestNumberOfCPM(0),
    longestNumberOfChanges(0),
    longestNumberOfMinutes(0),
    shortestNumberOfChanges(numeric_limits<unsigned int>::max()),
    shortestNumberOfMinutes (numeric_limits<unsigned int>::max()),
    slowestNumberOfCPM (numeric_limits<float>::max())

{
    firstPealDate.Clear();
    lastPealDate.ResetToEarliest();
}

PealLengthInfo::PealLengthInfo(const Duco::Peal& firstPeal)
    : longestChangesId(KNoId),
    longestMinutesId(KNoId),
    slowestCPMId(KNoId),
    shortestChangesId(KNoId),
    shortestMinutesId(KNoId),
    fastestCPMId(KNoId),
    totalPeals(0),
    totalPealsWithChanges(0),
    totalPealsWithTime(0),
    totalChanges(0),
    totalMinutes(0),
    totalMinutesWithTimeAndMinutes(0),
    totalChangesWithTimeAndMinutes(0),
    firstPealId(KNoId),
    lastPealId(KNoId),
    fastestNumberOfCPM(0),
    longestNumberOfChanges(0),
    longestNumberOfMinutes(0),
    shortestNumberOfChanges(numeric_limits<unsigned int>::max()),
    shortestNumberOfMinutes(numeric_limits<unsigned int>::max()),
    slowestNumberOfCPM(numeric_limits<float>::max())

{
    firstPealDate.Clear();
    lastPealDate.ResetToEarliest();
    AddPeal(firstPeal);
}

PealLengthInfo::PealLengthInfo(const Duco::FelsteadPealCount& pealInfo)
    : longestChangesId(KNoId),
    longestMinutesId(KNoId),
    slowestCPMId(KNoId),
    shortestChangesId(KNoId),
    shortestMinutesId(KNoId),
    fastestCPMId(KNoId),
    totalPeals(pealInfo.NumberOfPeals()),
    totalPealsWithChanges(0),
    totalPealsWithTime(0),
    totalChanges(0),
    totalMinutes(0),
    totalMinutesWithTimeAndMinutes(0),
    totalChangesWithTimeAndMinutes(0),
    firstPealId(KNoId),
    lastPealId(KNoId),
    fastestNumberOfCPM(0),
    longestNumberOfChanges(0),
    longestNumberOfMinutes(0),
    shortestNumberOfChanges(numeric_limits<unsigned int>::max()),
    shortestNumberOfMinutes(numeric_limits<unsigned int>::max()),
    slowestNumberOfCPM(numeric_limits<float>::max())

{
    firstPealDate.Clear();
    lastPealDate.ResetToEarliest();
}


PealLengthInfo::~PealLengthInfo()
{

}

void
PealLengthInfo::Clear()
{
    longestChangesId.ClearId();
    longestMinutesId.ClearId();
    slowestCPMId.ClearId();
    shortestChangesId.ClearId();
    shortestMinutesId.ClearId();
    fastestCPMId.ClearId();
    firstPealId.ClearId();
    lastPealId.ClearId();
    totalPeals = 0;
    totalPealsWithChanges = 0,
    totalPealsWithTime = 0,
    totalChanges = 0;
    totalMinutes = 0;
    totalMinutesWithTimeAndMinutes = 0,
    totalChangesWithTimeAndMinutes = 0,
    firstPealDate.Clear();
    lastPealDate.ResetToEarliest();

    longestNumberOfChanges = 0;
    longestNumberOfMinutes = 0;
    fastestNumberOfCPM = 0;
    shortestNumberOfChanges = numeric_limits<unsigned int>::max();
    shortestNumberOfMinutes = numeric_limits<unsigned int>::max();
    slowestNumberOfCPM = numeric_limits<float>::max();
}

bool
PealLengthInfo::Empty() const
{
    return (totalPeals == 0 &&
        totalChanges == 0 &&
        totalMinutes == 0);
}

bool
PealLengthInfo::operator==(const Duco::PealLengthInfo& rhs) const
{
    return longestChangesId == rhs.longestChangesId &&
        longestMinutesId == rhs.longestMinutesId &&
        slowestCPMId == rhs.slowestCPMId &&
        shortestChangesId == rhs.shortestChangesId &&
        shortestMinutesId == rhs.shortestMinutesId &&
        fastestCPMId == rhs.fastestCPMId &&
        totalPeals == rhs.totalPeals &&
        totalPealsWithChanges == rhs.totalPealsWithChanges &&
        totalPealsWithTime == rhs.totalPealsWithTime &&
        totalMinutesWithTimeAndMinutes == rhs.totalMinutesWithTimeAndMinutes &&
        totalChangesWithTimeAndMinutes == rhs.totalChangesWithTimeAndMinutes &&
        totalChanges == rhs.totalChanges &&
        totalMinutes == rhs.totalMinutes &&
        firstPealId == rhs.firstPealId &&
        lastPealId == rhs.lastPealId;
}

bool
PealLengthInfo::operator<(const Duco::PealLengthInfo& rhs) const
{
    return std::tie(totalPeals, totalMinutes, totalChanges, firstPealDate, lastPealDate, totalPealsWithChanges, totalPealsWithTime, totalMinutesWithTimeAndMinutes, totalChangesWithTimeAndMinutes) < std::tie(rhs.totalPeals, rhs.totalMinutes, rhs.totalChanges, rhs.firstPealDate, rhs.lastPealDate, rhs.totalPealsWithChanges, rhs.totalPealsWithTime, rhs.totalMinutesWithTimeAndMinutes, rhs.totalChangesWithTimeAndMinutes);
}

bool
PealLengthInfo::operator>(const Duco::PealLengthInfo& rhs) const
{
    return std::tie(totalPeals, totalMinutes, totalChanges, firstPealDate, lastPealDate, totalPealsWithChanges, totalPealsWithTime, totalMinutesWithTimeAndMinutes, totalChangesWithTimeAndMinutes) > std::tie(rhs.totalPeals, rhs.totalMinutes, rhs.totalChanges, rhs.firstPealDate, rhs.lastPealDate, rhs.totalPealsWithChanges, rhs.totalPealsWithTime, rhs.totalMinutesWithTimeAndMinutes, rhs.totalChangesWithTimeAndMinutes);
}

Duco::PealLengthInfo
PealLengthInfo::operator-=(const Duco::PealLengthInfo& rhs)
{
    longestChangesId.ClearId();
    longestMinutesId.ClearId();
    fastestCPMId.ClearId();
    slowestCPMId.ClearId();
    shortestChangesId.ClearId();
    shortestMinutesId.ClearId();
    firstPealId.ClearId();
    firstPealDate.ResetToEarliest();
    lastPealId.ClearId();
    lastPealDate.ResetToEarliest();

    totalPeals -= rhs.totalPeals;
    totalPealsWithChanges -= rhs.totalPealsWithChanges;
    totalPealsWithTime -= rhs.totalPealsWithTime;
    totalMinutesWithTimeAndMinutes -= rhs.totalMinutesWithTimeAndMinutes;
    totalChangesWithTimeAndMinutes -= rhs.totalChangesWithTimeAndMinutes;

    totalChanges -= rhs.totalChanges;
    totalMinutes -= rhs.totalMinutes;

    return *this;
}

PealLengthInfo&
PealLengthInfo::operator+=(const Duco::PealLengthInfo& rhs)
{
    if (rhs.longestNumberOfChanges > longestNumberOfChanges)
    {
        longestChangesId = rhs.longestChangesId;
    }
    if (rhs.longestNumberOfMinutes > longestNumberOfMinutes)
    {
        longestMinutesId = rhs.longestMinutesId;
    }
    if (rhs.fastestNumberOfCPM > fastestNumberOfCPM)
    {
        fastestCPMId = rhs.fastestCPMId;
    }
    if (rhs.slowestNumberOfCPM < slowestNumberOfCPM)
    {
        slowestCPMId = rhs.slowestCPMId;
    }
    if (rhs.shortestNumberOfChanges < shortestNumberOfChanges)
    {
        shortestChangesId = rhs.shortestChangesId;
    }
    if (rhs.shortestNumberOfMinutes < shortestNumberOfMinutes)
    {
        shortestMinutesId = rhs.shortestMinutesId;
    }
    if (rhs.firstPealDate < firstPealDate)
    {
        firstPealId = rhs.firstPealId;
        firstPealDate = rhs.firstPealDate;
    }
    if (rhs.lastPealDate > lastPealDate)
    {
        lastPealId = rhs.lastPealId;
        lastPealDate = rhs.lastPealDate;
    }

    totalPeals += rhs.totalPeals;
    totalPealsWithChanges += rhs.totalPealsWithChanges;
    totalPealsWithTime += rhs.totalPealsWithTime;
    totalMinutesWithTimeAndMinutes += rhs.totalMinutesWithTimeAndMinutes;
    totalChangesWithTimeAndMinutes += rhs.totalChangesWithTimeAndMinutes;
    totalChanges += rhs.totalChanges;
    totalMinutes += rhs.totalMinutes;

    return *this;
}

bool
PealLengthInfo::SetLongestChanges(const ObjectId& theId, unsigned int noOfChanges)
{
    if (noOfChanges > longestNumberOfChanges)
    {
        longestNumberOfChanges = noOfChanges;
        longestChangesId = theId;
        return true;
    }
    return false;
}

bool
PealLengthInfo::SetLongestMinutes(const ObjectId& theId, unsigned int noOfMinutes)
{
    if (noOfMinutes > longestNumberOfMinutes)
    {
        longestNumberOfMinutes = noOfMinutes;
        longestMinutesId = theId;
        return true;
    }
    return false;
}

bool 
PealLengthInfo::SetSlowestCpm(const ObjectId& theId, float speed)
{
    if (speed < slowestNumberOfCPM)
    {
        slowestNumberOfCPM = speed;
        slowestCPMId = theId;
        return true;
    }
    return false;
}

bool 
PealLengthInfo::SetShortestChanges(const ObjectId& theId, unsigned int noOfChanges)
{
    if (noOfChanges < shortestNumberOfChanges)
    {
        shortestNumberOfChanges = noOfChanges;
        shortestChangesId = theId;
        return true;
    }
    return false;
}

bool 
PealLengthInfo::SetShortestMinutes(const ObjectId& theId, unsigned int noOfMinutes)
{
    if (noOfMinutes < shortestNumberOfMinutes)
    {
        shortestNumberOfMinutes = noOfMinutes;
        shortestMinutesId = theId;
        return true;
    }
    return false;
}

bool 
PealLengthInfo::SetFastestCpm(const ObjectId& theId, float speed)
{
    if (speed > fastestNumberOfCPM)
    {
        fastestNumberOfCPM = speed;
        fastestCPMId = theId;
        return true;
    }
    return false;
}

float
PealLengthInfo::FastestChangesPerMinute() const
{
    return fastestNumberOfCPM;
}

float
PealLengthInfo::SlowestChangesPerMinute() const
{
    return slowestNumberOfCPM;
}

bool
PealLengthInfo::SetPealDates(const ObjectId& theId, const Duco::PerformanceDate& pealDate)
{
    bool changed (false);
    if (pealDate < firstPealDate)
    {
        firstPealDate = pealDate;
        firstPealId = theId;
        changed = true;
    }
    if (pealDate > lastPealDate)
    {
        lastPealDate = pealDate;
        lastPealId = theId;
        changed = true;
    }
    return changed;
}

Duco::PealLengthInfo&
PealLengthInfo::operator+=(const Duco::Peal& rhs)
{
    this->AddPeal(rhs);
    return *this;
}

void
PealLengthInfo::AddPeal(const Duco::Peal& nextPeal)
{
    ++totalPeals;

    unsigned int minutes = nextPeal.Time().pealTimeInMinutes;

    SetPealDates(nextPeal.Id(), nextPeal.Date());

    if (nextPeal.NoOfChanges() > 0)
    {
        ++totalPealsWithChanges;
        SetShortestChanges(nextPeal.Id(), nextPeal.NoOfChanges());
        SetLongestChanges(nextPeal.Id(), nextPeal.NoOfChanges());
        totalChanges += nextPeal.NoOfChanges();
    }

    if (minutes > 0)
    {
        ++totalPealsWithTime;
        SetShortestMinutes(nextPeal.Id(), minutes);
        SetLongestMinutes(nextPeal.Id(), minutes);
        totalMinutes += minutes;
    }


    if (minutes > 0 && nextPeal.NoOfChanges() > 0)
    {
        float changesPerMinute = nextPeal.ChangesPerMinute();
        totalMinutesWithTimeAndMinutes += minutes;
        totalChangesWithTimeAndMinutes += nextPeal.NoOfChanges();
        SetFastestCpm(nextPeal.Id(), changesPerMinute);
        SetSlowestCpm(nextPeal.Id(), changesPerMinute);
    }
}

std::wstring
PealLengthInfo::DateOfFirstPealString() const
{
    if (!firstPealId.ValidId())
        return L"";

    return firstPealDate.Str();
}

std::wstring
PealLengthInfo::DateOfLastPealString() const
{
    if (!lastPealId.ValidId())
        return L"";

    return lastPealDate.Str();
}

const Duco::PerformanceDate&
PealLengthInfo::DateOfFirstPeal() const
{
    return firstPealDate;
}

const Duco::PerformanceDate&
PealLengthInfo::DateOfLastPeal() const
{
    return lastPealDate;
}

size_t
PealLengthInfo::DaysSinceLastPeal() const
{
    if (!lastPealId.ValidId())
        return SIZE_MAX;

    PerformanceDate now;
    return lastPealDate.NumberOfDaysUntil(now);
}

unsigned int
PealLengthInfo::DaysBetweenFirstAndLastPeal() const
{
    if (!firstPealId.ValidId() || !lastPealId.ValidId())
        return 0;

    return lastPealDate.NumberOfDaysUntil(firstPealDate);
}

std::wstring
PealLengthInfo::DurationBetweenFirstAndLastPealString() const
{
    if (totalPeals == 1)
        return L"";

    unsigned long int days = DaysBetweenFirstAndLastPeal();
    if (days == 0)
    {
        return L"Zero days";
    }

    unsigned long int years = days / 365;
    unsigned long int remainingDays = days - (years * 365);
    std::wstring dayString = L"day";
    std::wstring yearString = L"year";

    if (remainingDays != 1)
    {
        dayString += L"s";
    }
    if (years == 0)
    {
        return std::format(L"{} {}", remainingDays, dayString);
    }
    if (years != 1)
    {
        yearString += L"s";
    }
    if (remainingDays == 0)
    {
        return std::format(L"{} {}", years, yearString);
    }

    return std::format(L"{} {} and {} {}", years, yearString, remainingDays, dayString);
}

const Duco::ObjectId&
PealLengthInfo::LongestChangesId() const
{
    return longestChangesId;
}

const Duco::ObjectId&
PealLengthInfo::LongestMinutesId() const
{
    return longestMinutesId;
}

const Duco::ObjectId&
PealLengthInfo::SlowestCpmId() const
{
    return slowestCPMId;
}

const Duco::ObjectId&
PealLengthInfo::ShortestChangesId() const
{
    return shortestChangesId;
}

const Duco::ObjectId&
PealLengthInfo::ShortestMinutesId() const
{
    return shortestMinutesId;
}

const Duco::ObjectId&
PealLengthInfo::FastestCpmId() const
{
    return fastestCPMId;
}

size_t
PealLengthInfo::TotalPeals() const
{
    return totalPeals;
}

size_t
PealLengthInfo::TotalChanges() const
{
    return totalChanges;
}

size_t
PealLengthInfo::TotalMinutes() const
{
    return totalMinutes;
}

unsigned int
PealLengthInfo::AverageChanges() const
{
    if (totalPealsWithChanges == 0)
    {
        return 0;
    }

    return int((double(totalChanges) / double(totalPealsWithChanges)) + 0.5);
}

unsigned int
PealLengthInfo::AverageMinutes() const
{
    if (totalMinutes == 0)
        return 0;

    return int(double(totalMinutes) / double(totalPealsWithTime));
}

const ObjectId&
PealLengthInfo::FirstPealId() const
{
    return firstPealId;
}

const ObjectId&
PealLengthInfo::LastPealId() const
{
    return lastPealId;
}

float
PealLengthInfo::AveragePealSpeed() const
{
    if (totalChangesWithTimeAndMinutes == 0 || totalMinutesWithTimeAndMinutes == 0)
    {
        return 0.0;
    }
    return float(totalChangesWithTimeAndMinutes) / float(totalMinutesWithTimeAndMinutes);
}
