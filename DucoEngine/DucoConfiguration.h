#ifndef __DUCOCONFIGURATION_H__
#define __DUCOCONFIGURATION_H__

#include "DucoEngineCommon.h"
#include "DucoVersionNumber.h"
#include <string>

#define KDEFAULT_CONFIG_FILENAME  "ducosettings.ini"

namespace rude
{
    class Config;
}

namespace Duco
{
    class DucoConfiguration
    {
    public:
        DllExport DucoConfiguration();
        DllExport ~DucoConfiguration();

        DllExport bool Save();
        DllExport bool Save(const std::string& newFilename);
        DllExport void ClearUISettings();
        DllExport bool Load(const std::string& filePath = "", const std::string& fileName = KDEFAULT_CONFIG_FILENAME);

        DllExport void SetImportNewMethod(bool newImportNewMethod);
        DllExport bool ImportNewMethod() const;

        DllExport void SetXmlMethodCollectionFile(const std::string& newXmlMethodsFile);
        DllExport const std::string& XmlMethodCollectionFile() const;

        DllExport void SetDoveDataFile(const std::string& newDoveDataFile);
        DllExport const std::string& DoveDataFile() const;

        DllExport void DisableNextVersion();
        DllExport void EnableNextVersion();
        DllExport bool CheckForNextVersion() const;

        DllExport void SetIncludeWarningsInValidation(bool newValidationSetting);
        DllExport bool IncludeWarningsInValidation() const;

        DllExport void SetExportDucoIdsWithXml(bool newValue);
        DllExport bool ExportDucoIdsWithXml() const;

        DllExport bool DisableFeaturesForPerformance() const;
        DllExport bool DisableFeaturesForPerformance(size_t noOfObjects) const;
        DllExport void SetDisableFeaturesForPerformance(bool newValue);

        DllExport unsigned long long  DatabaseMaximumSize() const;
        DllExport unsigned long long  MaximumImageSize() const;
        DllExport double              ImageMaxDimension() const;
        DllExport double              ImageMaxResolution() const;
        DllExport int                 ImageColourDepth() const;

        //DllExport std::wstring        GoogleApi() const;
        //DllExport void                SetGoogleApi(const std::string& newSettings);
        DllExport std::wstring        MapBoxApi() const;
        DllExport void                SetMapBoxApi(const std::string& newSettings);
        DllExport double              ChromeZoomLevel() const;
        DllExport void                SetChromeZoomLevel(double zoomLevel);

        DllExport static Duco::DucoVersionNumber VersionNumber();
        DllExport static unsigned int LatestDatabaseVersion();

    protected:
        bool CheckNonDefaultAndWrite(rude::Config& configFile, const char* sectionName, const char* settingName, const int value, const int defaultValue);
        bool CheckNonDefaultAndWrite(rude::Config& configFile, const char* sectionName, const char* settingName, const unsigned long long value, const unsigned long long defaultValue);
        bool CheckNonDefaultAndWrite(rude::Config& configFile, const char* sectionName, const char* settingName, const double value, const double defaultValue);

    private:
        rude::Config*       configFile;
        std::string         configFileName;

        bool                includeWarningsInValidation;
        bool                disablePerformance;
        bool                importNewMethod;
        bool                checkForNextVersion;
        bool                exportDucoIdsWithXml;
        std::string         xmlMethodCollectionFile;
        std::string         doveDataFile;
        unsigned long long  databaseMaximumSize;
        unsigned long long  maximumImageSize;
        double              imageMaxDimension;
        double              imageMaxResolution;
        //std::string         googleApiKey;
        std::string         mapBoxApiKey;
        double              chromeZoomLevel;
        mutable int         imageColourDepth;
    };
}

#endif //!__DUCOCONFIGURATION_H__