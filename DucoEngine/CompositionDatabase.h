#ifndef __COMPOSITIONDATABASE_H__
#define __COMPOSITIONDATABASE_H__

#include "RingingObjectDatabase.h"
#include <xercesc/dom/DOMDocument.hpp>

namespace Duco
{
	class Composition;
    class RenumberProgressCallback;
    class MethodDatabase;

class CompositionDatabase: public RingingObjectDatabase
{
public:
    DllExport CompositionDatabase();
    DllExport ~CompositionDatabase();

    // from RingingObjectDatabase
    DllExport virtual Duco::ObjectId AddObject(const Duco::RingingObject& newObject);
    virtual bool Internalise(DatabaseReader& reader, Duco::ImportExportProgressCallback* newCallback, int databaseVersionNumber, size_t noOfObjectsInThisDatabase, size_t totalObjects, size_t objectsProcessedSoFar);
    virtual bool RebuildObjects(Duco::RenumberProgressCallback* callback, Duco::RingingDatabase& database);
    virtual bool MatchObject(const Duco::RingingObject& object, const Duco::TSearchArgument& searchArg, const Duco::RingingDatabase& database) const;
    virtual bool ValidateObject(const Duco::RingingObject& object, const Duco::TSearchValidationArgument& searchArg, const std::set<Duco::ObjectId>& idsToCheckAgainst, const Duco::RingingDatabase& database) const;
    virtual void ExternaliseToXml(Duco::ImportExportProgressCallback* newCallback, XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, bool exportDucoObjects) const;
    virtual bool UpdateObjectField(const Duco::ObjectId& id, const Duco::TUpdateArgument& updateArgument);

    // new functions
    DllExport const Composition* const FindComposition(const Duco::ObjectId& compositionId, bool setIndex = false) const;
    DllExport void GetCompositionsByNoOfBells(std::set<Duco::ObjectId>& compositionIds, const Duco::MethodDatabase& methodDb, unsigned int noOfBells) const;

    DllExport void AddMissingCompositions(std::map<Duco::ObjectId, Duco::PealLengthInfo>& compositionIds) const;
    DllExport void RemoveUsedIds(std::set<Duco::ObjectId>& unusedRingers, std::set<Duco::ObjectId>& unusedMethods) const;
    bool RenumberMethodsInCompositions(const std::map<Duco::ObjectId, Duco::ObjectId>& newMethodIds, Duco::RenumberProgressCallback* callback);
    bool RenumberRingersInCompositions(const std::map<Duco::ObjectId, Duco::ObjectId>& newRingerIds, Duco::RenumberProgressCallback* callback);

protected:
    // from RingingObjectDatabase
    bool SaveUpdatedObject(Duco::RingingObject& lhs, const Duco::RingingObject& rhs);
};

}
#endif //!__COMPOSITIONDATABASE_H__
