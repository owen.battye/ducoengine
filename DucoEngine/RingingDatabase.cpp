#include "RingingDatabase.h"

#include "AssociationDatabase.h"
#include "Composition.h"
#include "CompositionDatabase.h"
#include "DatabaseReader.h"
#include "DatabaseSettings.h"
#include "DatabaseSearch.h"
#include "DatabaseWriter.h"
#include "DoveObject.h"
#include "DucoEngineUtils.h"
#include "DucoEngineLog.h"
#include "DucoConfiguration.h"
#include "DummyProgressCallback.h"
#include "StatisticFilters.h"
#include "ProgressCallback.h"
#include "InvalidDatabaseVersionException.h"
#include "Method.h"
#include "MethodDatabase.h"
#include "MethodSeries.h"
#include "MethodSeriesDatabase.h"
#include "Peal.h"
#include "PealDatabase.h"
#include "Picture.h"
#include "PictureDatabase.h"
#include "ImportExportProgressCallback.h"
#include "RenumberProgressCallback.h"
#include "Ring.h"
#include "Ringer.h"
#include "RingerDatabase.h"
#include "Tower.h"
#include "TowerDatabase.h"
#include "SearchFieldStringArgument.h"
#include "SearchFieldBoolArgument.h"
#include "SearchValidObject.h"
#include "SearchGroup.h"

#include <numeric>
#include <ctime>
#include <cstring>
#include <cerrno>
#include <limits>
#include <sys/stat.h>
#include <algorithm>

#include "DucoXmlUtils.h"
#include <xercesc/dom/DOMComment.hpp>
#include <xercesc/dom/DOMDocument.hpp>
#include <xercesc/dom/DOMElement.hpp>
#include <xercesc/dom/DOMImplementation.hpp>
#include <xercesc/dom/DOMImplementationRegistry.hpp>
#include <xercesc/dom/DOMLSSerializer.hpp>
#include <xercesc/dom/DOMLSOutput.hpp>
#include <xercesc/util/IOException.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>
#include <xercesc/util/XMLString.hpp>
using namespace std::chrono;

XERCES_CPP_NAMESPACE_USE
const int ABSOLUTE_PATH_FILENAME_PREFIX_SIZE = 9;

using namespace std;
using namespace Duco;

RingingDatabase::RingingDatabase()
    : databaseVersion(Duco::DucoConfiguration::LatestDatabaseVersion()), createdDate(0), rebuildInProgress(false)
{
    settings = new DatabaseSettings();
    RingerDatabase* theRingersDatabase = new RingerDatabase();
    ringersDatabase = theRingersDatabase;
    TowerDatabase* theTowersDatabase = new TowerDatabase();
    towersDatabase = theTowersDatabase;
    MethodDatabase* theMethodsDatabase = new MethodDatabase();
    methodsDatabase = theMethodsDatabase;
    PictureDatabase* thePicturesDatabase = new PictureDatabase();
    picturesDatabase = thePicturesDatabase;
    Duco::MethodSeriesDatabase* seriesDatabase = new Duco::MethodSeriesDatabase();
    methodSeriesDatabase = seriesDatabase;
    compositionsDatabase = new CompositionDatabase();
    associationsDatabase = new AssociationDatabase();
    pealsDatabase = new PealDatabase(*theRingersDatabase, *theMethodsDatabase, *theTowersDatabase, *seriesDatabase, *compositionsDatabase, *associationsDatabase, *settings);
}

RingingDatabase::~RingingDatabase()
{
    DeleteDatabaseContents(false);
    delete ringersDatabase;
    delete towersDatabase;
    delete methodsDatabase;
    delete pealsDatabase;
    delete picturesDatabase;
    delete methodSeriesDatabase;
    delete compositionsDatabase;
    delete associationsDatabase;
    settings->ClearSettings(false);
    delete settings;
}

void
RingingDatabase::DeleteDatabaseContents(bool populateWithDefaults)
{
    ringersDatabase->ClearObjects(populateWithDefaults);
    towersDatabase->ClearObjects(populateWithDefaults);
    methodsDatabase->ClearObjects(populateWithDefaults);
    pealsDatabase->ClearObjects(populateWithDefaults);
    picturesDatabase->ClearObjects(populateWithDefaults);
    methodSeriesDatabase->ClearObjects(populateWithDefaults);
    compositionsDatabase->ClearObjects(populateWithDefaults);
    associationsDatabase->ClearObjects(populateWithDefaults);
    databaseVersion = Duco::DucoConfiguration::LatestDatabaseVersion();
    createdDate = 0;
}

bool
RingingDatabase::DataChanged() const
{
    return ringersDatabase->DataChanged() || towersDatabase->DataChanged() ||
            methodsDatabase->DataChanged() || pealsDatabase->DataChanged() ||
            picturesDatabase->DataChanged() ||
            methodSeriesDatabase->DataChanged() || compositionsDatabase->DataChanged() || associationsDatabase->DataChanged() ||
            settings->SettingsChanged();
}

void
RingingDatabase::ResetDataChanged() const
{
    ringersDatabase->ResetDataChanged();
    towersDatabase->ResetDataChanged();
    methodsDatabase->ResetDataChanged();
    pealsDatabase->ResetDataChanged();
    methodSeriesDatabase->ResetDataChanged();
    compositionsDatabase->ResetDataChanged();
    picturesDatabase->ResetDataChanged();
    associationsDatabase->ResetDataChanged();
    settings->ResetSettingsChanged();
}

void
RingingDatabase::ClearDatabase(bool quarterPealDb, bool createDefaultBellNames)
{
    DeleteDatabaseContents(createDefaultBellNames);
    ResetCurrentPointers();
    settings->ClearSettings(quarterPealDb);
    ResetDataChanged();
}

std::string
RingingDatabase::BackupFileName(const std::string& fileName) const
{
    std::string backupFileName;
    if (fileName.length() > 4)
    {
        if (fileName.substr(fileName.length() - 4).compare(".bak") == 0)
        {
            backupFileName.append(fileName.substr(0, fileName.length() - 4));
            backupFileName.append(".ba2");
        }
        else
        {
            backupFileName.append(fileName);
            backupFileName.append(".bak");
        }
    }
    return backupFileName;
}

bool
RingingDatabase::Externalise(const char* fileName, ImportExportProgressCallback* callback) const
{
    FILE* pFile = NULL;
    pFile = fopen(fileName, "r");
    if (pFile != NULL)
    {
        fseek (pFile, 0, SEEK_END);
        long size = ftell(pFile);
        fclose (pFile);
        if (size > 0)
        {
            std::string backupFilename (BackupFileName(fileName));
            if (backupFilename.length() > 4)
            {
                struct stat fileAttribs;
                bool copyFile (false);
                if (stat(backupFilename.c_str(), &fileAttribs) == 0)
                {
                    time_t fileModifiedFile = (fileAttribs.st_mtime);
                    time_t timeNow = time (NULL);

                    double dif = difftime (timeNow, fileModifiedFile);
                    if (dif > (double)(60 * 60 * 24)) // if backup file is a day old replace it.
                    {
                        std::string secondBackupFilename(BackupFileName(backupFilename));
                        rename(backupFilename.c_str(), secondBackupFilename.c_str());
                        remove(backupFilename.c_str());
                    }
                    copyFile = true;
                }
                if (errno == ENOENT)
                    copyFile = true;
                if (copyFile)
                {
                    rename(fileName, backupFilename.c_str());
                }
            }
        }
    }

    try
    {
        DatabaseWriter writer(fileName);
        writer.WriteInt(Duco::DucoConfiguration::LatestDatabaseVersion());
        writer.WriteInt(ringersDatabase->NumberOfObjects());
        writer.WriteInt(towersDatabase->NumberOfObjects());
        writer.WriteInt(methodsDatabase->NumberOfObjects());
        writer.WriteInt(pealsDatabase->NumberOfObjects());
        writer.WriteInt(methodSeriesDatabase->NumberOfObjects());
        writer.WriteInt(compositionsDatabase->NumberOfObjects());
        writer.WriteInt(associationsDatabase->NumberOfObjects());
        writer.WriteInt(picturesDatabase->NumberOfObjects());
        writer.WriteInt(methodsDatabase->NoOfOrderNames());
        size_t totalNumberOfObjects = ringersDatabase->NumberOfObjects() + towersDatabase->NumberOfObjects() + methodsDatabase->NumberOfObjects() + pealsDatabase->NumberOfObjects() + methodSeriesDatabase->NumberOfObjects() + picturesDatabase->NumberOfObjects() + methodsDatabase->NoOfOrderNames() + compositionsDatabase->NumberOfObjects() + associationsDatabase->NumberOfObjects();
        if (callback != NULL)
        {
            callback->InitialisingImportExport(false, Duco::DucoConfiguration::LatestDatabaseVersion(), totalNumberOfObjects);
        }
        ResetDataChanged();

        writer.WriteTime(createdDate);
        writer.WriteString(L"");
        size_t totalProcessedSoFar = 0;

        // Ringers
        ringersDatabase->Externalise(callback, writer, ringersDatabase->NumberOfObjects(), totalNumberOfObjects, totalProcessedSoFar);
        totalProcessedSoFar += ringersDatabase->NumberOfObjects();
        // Towers
        towersDatabase->Externalise(callback, writer, towersDatabase->NumberOfObjects(), totalNumberOfObjects, totalProcessedSoFar);
        totalProcessedSoFar += towersDatabase->NumberOfObjects();
        // Methods
        methodsDatabase->Externalise(callback, writer, methodsDatabase->NumberOfObjects(), totalNumberOfObjects, totalProcessedSoFar);
        totalProcessedSoFar += methodsDatabase->NumberOfObjects();
        // Peals
        pealsDatabase->Externalise(callback, writer, pealsDatabase->NumberOfObjects(), totalNumberOfObjects, totalProcessedSoFar);
        totalProcessedSoFar += pealsDatabase->NumberOfObjects();
        // MethodSeries
        methodSeriesDatabase->Externalise(callback, writer, methodSeriesDatabase->NumberOfObjects(), totalNumberOfObjects, totalProcessedSoFar);
        totalProcessedSoFar += methodSeriesDatabase->NumberOfObjects();
        // Compositions
        compositionsDatabase->Externalise(callback, writer, compositionsDatabase->NumberOfObjects(), totalNumberOfObjects, totalProcessedSoFar);
        totalProcessedSoFar += compositionsDatabase->NumberOfObjects();
        // Associations
        associationsDatabase->Externalise(callback, writer, associationsDatabase->NumberOfObjects(), totalNumberOfObjects, totalProcessedSoFar);
        totalProcessedSoFar += associationsDatabase->NumberOfObjects();
        // Pictures
        picturesDatabase->Externalise(callback, writer, picturesDatabase->NumberOfObjects(), totalNumberOfObjects, totalProcessedSoFar);
        totalProcessedSoFar += picturesDatabase->NumberOfObjects();

        // BellNames
        static_cast<Duco::MethodDatabase*>(methodsDatabase)->ExternaliseOrderNames(callback, writer);

        writer.WriteString(notes);
        settings->Externalise(writer);
        if (callback != NULL)
        {
            callback->ImportExportComplete(false);
        }
        ResetDataChanged();
    }
    catch (exception& ex)
    {
        DUCOENGINELOG(L"Error saving Duco Database");
        DUCOENGINELOG(ex.what());
        return false;
    }
    return true;
}

bool
RingingDatabase::ExternaliseToCsv(const char* fileName, Duco::ImportExportProgressCallback* callback, const std::string& originalFileName) const
{
    bool error(false);
    try
    {
        wofstream myfile;
        myfile.open(fileName);
        myfile << "Number,Association,Date,Tower,Tenor,Time,NoOfChanges,Method,Composer,Treble,Ringer2,Ringer3,Ringer4,Ringer5,Ringer6,Ringer7,Ringer8,Ringer9,Ringer10,Ringer11,Ringer12,Ringer13,Ringer14,Ringer15,Ringer16,Ringer17,Ringer18,Ringer19,Ringer20,Ringer21,Ringer22,Strapper,HandBell,Withdrawn,DoubleHanded,Footnotes,MultiMethods,RingingWorld,BellBoard,DoveId,TowerbaseId,\n";
        callback->InitialisingImportExport(false, KNoId, pealsDatabase->NumberOfObjects());
        pealsDatabase->ExternaliseToCsv(callback, myfile);

        myfile.close();
        callback->ImportExportComplete(false);
    }
    catch (exception& ex)
    {
        DUCOENGINELOG(L"Error externalising to CSV");
        DUCOENGINELOG(ex.what());
        error = true;
    }
    return !error;
}

bool
RingingDatabase::ExternaliseToXml(const char* fileName, Duco::ImportExportProgressCallback* callback, const std::string& originalFileName, bool includeDucoObjects, bool onlyIncludeDatabaseVersion) const
{
    bool error (false);
    try
    {
        XMLPlatformUtils::Initialize();
        DOMImplementation* p_DOMImplementation = DOMImplementationRegistry::getDOMImplementation(XMLStrL("core"));
        DOMDocument* p_DOMDocument = p_DOMImplementation->createDocument(0, L"RingingDatabase", 0);
        DOMElement* rootElem = p_DOMDocument->getDocumentElement();

        DOMComment* newComment = p_DOMDocument->createComment(XMLStrL("Created by Duco - download from www.rocketpole.co.uk"));
        rootElem->appendChild(newComment);
        time_t rawtime;
        time (&rawtime);
        struct tm* timeinfo = localtime (&rawtime);
        if (!onlyIncludeDatabaseVersion)
        {
            DucoXmlUtils::AddAttribute(*rootElem, L"TimeStamp", _wasctime(timeinfo));
        }
        DucoXmlUtils::AddAttribute(*rootElem, L"DatabaseVersion", DucoEngineUtils::ToString(databaseVersion));
        if (!onlyIncludeDatabaseVersion)
        {
            DucoXmlUtils::AddAttribute(*rootElem, L"DatabaseCreatedDate", _wctime(&createdDate));
        }

        if (includeDucoObjects)
        {
            callback->InitialisingImportExport(false, KNoId, pealsDatabase->NumberOfObjects() + ringersDatabase->NumberOfObjects() + towersDatabase->NumberOfObjects()
                                        + methodsDatabase->NumberOfObjects() + methodSeriesDatabase->NumberOfObjects() + compositionsDatabase->NumberOfObjects());
        }
        else
        {
            callback->InitialisingImportExport(false, KNoId, pealsDatabase->NumberOfObjects());
        }
        pealsDatabase->ExternaliseToXml(callback, *p_DOMDocument, includeDucoObjects);
        if (includeDucoObjects)
        {
            ringersDatabase->ExternaliseToXml(callback, *p_DOMDocument, includeDucoObjects);
            towersDatabase->ExternaliseToXml(callback, *p_DOMDocument, includeDucoObjects);
            methodsDatabase->ExternaliseToXml(callback, *p_DOMDocument, includeDucoObjects);
            methodSeriesDatabase->ExternaliseToXml(callback, *p_DOMDocument, includeDucoObjects);
            compositionsDatabase->ExternaliseToXml(callback, *p_DOMDocument, includeDucoObjects);
            associationsDatabase->ExternaliseToXml(callback, *p_DOMDocument, includeDucoObjects);
        }

        DOMLSSerializer* serializer = ((DOMImplementationLS*)p_DOMImplementation)->createLSSerializer();
        if (serializer->getDomConfig()->canSetParameter(XMLUni::fgDOMWRTFormatPrettyPrint, true))
            serializer->getDomConfig()->setParameter(XMLUni::fgDOMWRTFormatPrettyPrint, true);
        serializer->setNewLine(XMLStrL("\r\n")); 

        XMLFormatTarget* formatTarget = new LocalFileFormatTarget(fileName);
        DOMLSOutput* output = ((DOMImplementationLS*)p_DOMImplementation)->createLSOutput();
        output->setByteStream(formatTarget);
        serializer->write(p_DOMDocument, output);

        serializer->release();
        delete formatTarget;
        output->release();
        p_DOMDocument->release();

        callback->ImportExportComplete(false);
    }
    catch (XERCES_CPP_NAMESPACE_QUALIFIER IOException& ex)
    {
        DUCOENGINELOG(L"Error externalising to XML");
        DUCOENGINELOG(ex.getMessage());
        error = true;
    }
    catch (...)
    {
        DUCOENGINELOG(L"Unhandled exception externalised to XML");
        error = true;
    }
    XMLPlatformUtils::Terminate();
    return !error;
}


bool
RingingDatabase::ExternaliseToBellboardXml(const char* fileName, std::set<Duco::ObjectId> performanceIds, Duco::ImportExportProgressCallback* callback) const
{
    bool error(false);
    try
    {
        XMLPlatformUtils::Initialize();
        DOMImplementation* p_DOMImplementation = DOMImplementationRegistry::getDOMImplementation(XMLStrL("core"));
        DOMDocument* p_DOMDocument = p_DOMImplementation->createDocument(L"", L"performances", 0);
        DOMElement* rootElem = p_DOMDocument->getDocumentElement();
        DucoXmlUtils::AddAttribute(*rootElem, L"xmlns", L"http://bb.ringingworld.co.uk/NS/performances#");

        DOMComment* newComment = p_DOMDocument->createComment(XMLStrL("Created by Duco - download from www.rocketpole.co.uk"));
        rootElem->appendChild(newComment);
        time_t rawtime;
        time(&rawtime);
        struct tm* timeinfo = localtime(&rawtime);

        callback->InitialisingImportExport(false, KNoId, pealsDatabase->NumberOfObjects());
        pealsDatabase->ExternaliseToBellboardXml(callback, performanceIds, *p_DOMDocument);

        DOMLSSerializer* serializer = ((DOMImplementationLS*)p_DOMImplementation)->createLSSerializer();
        if (serializer->getDomConfig()->canSetParameter(XMLUni::fgDOMWRTFormatPrettyPrint, true))
            serializer->getDomConfig()->setParameter(XMLUni::fgDOMWRTFormatPrettyPrint, true);
        serializer->setNewLine(XMLStrL("\r\n"));

        XMLFormatTarget* formatTarget = new LocalFileFormatTarget(fileName);
        DOMLSOutput* output = ((DOMImplementationLS*)p_DOMImplementation)->createLSOutput();
        output->setByteStream(formatTarget);
        serializer->write(p_DOMDocument, output);

        serializer->release();
        delete formatTarget;
        output->release();
        p_DOMDocument->release();

        callback->ImportExportComplete(false);
    }
    catch (exception& ex)
    {
        DUCOENGINELOG(L"Error creating bellboard XML");
        DUCOENGINELOG(ex.what());
        error = true;
    }
    XMLPlatformUtils::Terminate();
    return !error;
}

void
RingingDatabase::Internalise(const char* fileName, ImportExportProgressCallback* callback, unsigned int& databaseVersionNumber)
{
    ClearDatabase(false, false);
    DatabaseReader* reader = new DatabaseReader(fileName);

    if (databaseVersionNumber < 41)
        databaseVersion = reader->ReadInt();
    else
        databaseVersion = reader->ReadUInt();

    databaseVersionNumber = databaseVersion;
    if (databaseVersion > Duco::DucoConfiguration::LatestDatabaseVersion() || databaseVersion == 0 || databaseVersion == 40 || databaseVersion == 39)
    {
        throw InvalidDatabaseVersionException(databaseVersion, Duco::DucoConfiguration::LatestDatabaseVersion());
    }
    unsigned int noOfRingers = reader->ReadInt();
    unsigned int noOfTowers = reader->ReadInt();
    unsigned int noOfMethods = reader->ReadInt();
    unsigned int noOfPeals = reader->ReadInt();
    unsigned int noOfMethodSeries = 0;
    unsigned int noOfPictures = 0;
    unsigned int noOfAssociations = 0;

    if (databaseVersion >= 10)
    {
        noOfMethodSeries = reader->ReadInt();
    }
    unsigned int noOfCompositions = 0;
    if (databaseVersion >= 15)
    {
        noOfCompositions = reader->ReadInt();
    }
    if (databaseVersion >= 29)
    {
        if (databaseVersion >= 36)
        {
            noOfAssociations = reader->ReadInt();
        }
        noOfPictures = reader->ReadInt();
    }
    unsigned int noOfBellNames = reader->ReadInt();
    size_t totalNumberOfObjects = (size_t)noOfRingers + (size_t)noOfTowers + (size_t)noOfMethods + (size_t)noOfPeals + (size_t)noOfBellNames + (size_t)noOfMethodSeries
                                + (size_t)noOfCompositions + (size_t)noOfAssociations + (size_t)noOfPictures;
    size_t totalNumberOfObjectsProcessed = 0;
    if (callback != NULL)
    {
        callback->InitialisingImportExport(true, databaseVersion, totalNumberOfObjects);
    }

    if (databaseVersion >= 6)
    {
        createdDate = reader->ReadTime();
        if (createdDate <= 0 || createdDate == -1)
        {
            createdDate = time(NULL);
        }
        std::wstring originatingComputerNotUsed;
        reader->ReadString(originatingComputerNotUsed);
    }
    else
    {
        createdDate = time(NULL);
    }

    time_point<high_resolution_clock> start = high_resolution_clock::now();

    // Ringers
    bool success = ringersDatabase->Internalise(*reader, callback, databaseVersionNumber, noOfRingers, totalNumberOfObjects, totalNumberOfObjectsProcessed);
    if (!success)
    {
        if (callback != NULL)
        {
            callback->ImportExportFailed(true, 0);
        }
        return;
    }
    totalNumberOfObjectsProcessed += noOfRingers;
    DUCOENGINEDEBUGLOG2(start, "Completed internalising ringers");
    time_point<high_resolution_clock> startTowers = high_resolution_clock::now();

    // Towers
    success = towersDatabase->Internalise(*reader, callback, databaseVersionNumber, noOfTowers, totalNumberOfObjects, totalNumberOfObjectsProcessed);
    if (!success)
    {
        if (callback != NULL)
        {
            callback->ImportExportFailed(true, 0);
        }
        return;
    }
    totalNumberOfObjectsProcessed += noOfTowers;

    DUCOENGINEDEBUGLOG2(startTowers, "Completed internalising tower");
    time_point<high_resolution_clock> startMethods = high_resolution_clock::now();

    // Methods
    success = methodsDatabase->Internalise(*reader, callback, databaseVersionNumber, noOfMethods, totalNumberOfObjects, totalNumberOfObjectsProcessed);
    if (!success)
    {
        if (callback != NULL)
        {
            callback->ImportExportFailed(true, 0);
        }
        return;
    }
    totalNumberOfObjectsProcessed += noOfMethods;

    DUCOENGINEDEBUGLOG2(startMethods, "Completed internalising methods");
    time_point<high_resolution_clock> startPeals = high_resolution_clock::now();

    // Peals
    success = pealsDatabase->Internalise(*reader, callback, databaseVersionNumber, noOfPeals, totalNumberOfObjects, totalNumberOfObjectsProcessed);
    if (!success)
    {
        if (callback != NULL)
        {
            callback->ImportExportFailed(true, 0);
        }
        return;
    }
    totalNumberOfObjectsProcessed += noOfPeals;

    DUCOENGINEDEBUGLOG2(startPeals, "Completed internalising peals");
    time_point<high_resolution_clock> startMethodSeries = high_resolution_clock::now();

    // MethodSeries
    success = methodSeriesDatabase->Internalise(*reader, callback, databaseVersionNumber, noOfMethodSeries, totalNumberOfObjects, totalNumberOfObjectsProcessed);
    if (!success)
    {
        if (callback != NULL)
        {
            callback->ImportExportFailed(true, 0);
        }
        return;
    }
    totalNumberOfObjectsProcessed += noOfMethodSeries;

    DUCOENGINEDEBUGLOG2(startMethodSeries, "Completed internalising method series");
    time_point<high_resolution_clock> startCompositions = high_resolution_clock::now();

    // Compositions
    success = compositionsDatabase->Internalise(*reader, callback, databaseVersionNumber, noOfCompositions, totalNumberOfObjects, totalNumberOfObjectsProcessed);
    if (!success)
    {
        if (callback != NULL)
        {
            callback->ImportExportFailed(true, 0);
        }
        return;
    }
    DUCOENGINEDEBUGLOG2(startCompositions, "Completed internalising compositions");
    totalNumberOfObjectsProcessed += noOfCompositions;
    if (databaseVersion >= 29)
    {
        if (databaseVersion >= 36)
        {
            time_point<high_resolution_clock> startAssociations = high_resolution_clock::now();

            // Associations
            success = associationsDatabase->Internalise(*reader, callback, databaseVersionNumber, noOfAssociations, totalNumberOfObjects, totalNumberOfObjectsProcessed);
            if (!success)
            {
                if (callback != NULL)
                {
                    callback->ImportExportFailed(true, 0);
                }
                return;
            }
            totalNumberOfObjectsProcessed += noOfAssociations;
            DUCOENGINEDEBUGLOG2(startAssociations, "Completed internalising associations");
        }
        time_point<high_resolution_clock> startPictures = high_resolution_clock::now();
        // Pictures
        success = picturesDatabase->Internalise(*reader, callback, databaseVersionNumber, noOfPictures, totalNumberOfObjects, totalNumberOfObjectsProcessed);
        if (!success)
        {
            if (callback != NULL)
            {
                callback->ImportExportFailed(true, 0);
            }
            return;
        }
        totalNumberOfObjectsProcessed += noOfPictures;
        DUCOENGINEDEBUGLOG2(startPictures, "Completed internalising pictures");
    }

    // BellNames
    while (noOfBellNames-- > 0)
    {
        totalNumberOfObjectsProcessed += 1;
        unsigned int noOfBells = reader->ReadInt();
        std::wstring bellName;
        reader->ReadString(bellName);
        static_cast<Duco::MethodDatabase*>(methodsDatabase)->AddOrderName(noOfBells, bellName);
        if (callback != NULL)
        {
            float percentageComplete = ((float)totalNumberOfObjectsProcessed / (float)totalNumberOfObjects) * 100;
            callback->ObjectProcessed(true, TObjectType::EOrderName, noOfBells, (int)percentageComplete, totalNumberOfObjectsProcessed);
        }
    }
    if (databaseVersion >= 48)
    {
        reader->ReadString(notes);
    }
    settings->Internalise(*reader, databaseVersion);
    ResetDataChanged();
    if (callback != NULL)
    {
        callback->ImportExportComplete(true);
    }
    delete reader;
}

void
RingingDatabase::GetMethodCircling(const Duco::StatisticFilters& filters, std::map<unsigned int, Duco::CirclingData>& sortedBellPealCounts, size_t& numberOfTimesCircled, PerformanceDate& firstCircledDate) const
{
    sortedBellPealCounts.clear();
    numberOfTimesCircled = 0;
    firstCircledDate.ResetToEarliest();

    Duco::ObjectId methodId;
    if (!filters.Method(methodId))
    {
        return;
    }

    const Duco::Method* const theMethod = methodsDatabase->FindMethod(methodId, true);
    if (theMethod == NULL)
    {
        return;
    }

    pealsDatabase->GetMethodCircling(filters, theMethod->Order(), sortedBellPealCounts, firstCircledDate);
    // Add All bells to the array first.
    for (unsigned int orderNumber(1); orderNumber <= theMethod->Order(); ++orderNumber)
    {
        pair<unsigned int, unsigned int> newObject(orderNumber, 0);
        sortedBellPealCounts.insert(newObject);
    }

    if (sortedBellPealCounts.size() > 0)
    {
        numberOfTimesCircled = numeric_limits<size_t>::max();
        std::map<unsigned int, Duco::CirclingData>::const_iterator countsIt = sortedBellPealCounts.begin();
        while (countsIt != sortedBellPealCounts.end() && numberOfTimesCircled > 0)
        {
            numberOfTimesCircled = min(numberOfTimesCircled, countsIt->second.PealCount());
            ++countsIt;
        }
    }
}

void
RingingDatabase::GetTowerCircling(const Duco::StatisticFilters& filters, std::map<unsigned int, Duco::CirclingData>& sortedBellPealCounts, Duco::PealLengthInfo& noOfPeals, size_t& numberOfTimesCircled, PerformanceDate& firstCircledDate, size_t& noOfDaysToFirstCircle) const
{
    noOfPeals.Clear();
    sortedBellPealCounts.clear();
    numberOfTimesCircled = numeric_limits<size_t>::max();
    noOfDaysToFirstCircle = numeric_limits<size_t>::max();
    const Tower* theTower = NULL;
    Duco::ObjectId towerId;
    if (filters.Tower(towerId))
    {
        theTower = towersDatabase->FindTower(towerId, true);
        if (theTower == NULL)
            return;
    }
    else
    {
        return;
    }

    pealsDatabase->GetTowerCircling(filters, sortedBellPealCounts, noOfPeals, firstCircledDate, noOfDaysToFirstCircle);
    if (noOfPeals.TotalPeals() > 0)
    {
        unsigned int maxNumberOfBells = theTower->Bells();
        Duco::ObjectId ringId;

        if (filters.Ring(ringId))
        {
            const Ring* const theRing = theTower->FindRing(ringId);
            if (theRing == NULL)
                return;

            maxNumberOfBells = theRing->NoOfBells();
        }
        for (unsigned int i = 1; i <= maxNumberOfBells; ++i)
        {
            if (sortedBellPealCounts.find(i) == sortedBellPealCounts.end())
            {
                std::pair<unsigned int, Duco::CirclingData> newObject(i, Duco::CirclingData());
                sortedBellPealCounts.insert(newObject);
            }
        }

        std::map<unsigned int, Duco::CirclingData>::const_iterator countsIt = sortedBellPealCounts.begin();
        while (countsIt != sortedBellPealCounts.end() && numberOfTimesCircled > 0)
        {
            numberOfTimesCircled = min(numberOfTimesCircled, countsIt->second.PealCount());
            ++countsIt;
        }
    }
    else
    {
        numberOfTimesCircled = 0;
        noOfDaysToFirstCircle = 0;
    }
}

//***********************************************************************
// Sorting / reordering the entire database
//***********************************************************************

bool
RingingDatabase::RebuildDatabase(Duco::RenumberProgressCallback* callback)
{
    rebuildInProgress = true;
    bool changesMade(false);

    try
    {
        changesMade = pealsDatabase->RebuildObjects(callback, *this);
        changesMade |= towersDatabase->RebuildObjects(callback, *this);
        changesMade |= methodsDatabase->RebuildObjects(callback, *this);
        changesMade |= ringersDatabase->RebuildObjects(callback, *this);
        changesMade |= methodSeriesDatabase->RebuildObjects(callback, *this);
        changesMade |= compositionsDatabase->RebuildObjects(callback, *this);
        changesMade |= associationsDatabase->RebuildObjects(callback, *this);
        changesMade |= picturesDatabase->RebuildObjects(callback, *this);
        ResetCurrentPointers();
    }
    catch (exception& ex)
    {
        DUCOENGINELOG(L"Error reindexing database");
        DUCOENGINELOG(ex.what());
    }
    rebuildInProgress = false;
    return changesMade;
}

bool
RingingDatabase::RebuildInProgress() const
{
    return rebuildInProgress;
}

bool
RingingDatabase::FindUnusedTowers(std::set<ObjectId>& unusedTowers) const
{
    std::set<ObjectId> unusedMethods;
    std::set<ObjectId> unusedRingers;
    std::set<ObjectId> unusedMethodSeries;
    std::set<ObjectId> unusedCompositions;
    std::set<ObjectId> unusedAssociations;
    std::multimap<ObjectId, ObjectId> unusedRings;
    FindUnused(unusedMethods, unusedRingers, unusedTowers, unusedRings, unusedMethodSeries, unusedCompositions, unusedAssociations);

    return unusedTowers.size() > 0;
}

bool
RingingDatabase::FindUnused(std::set<ObjectId>& unusedMethods, std::set<ObjectId>& unusedRingers, std::set<ObjectId>& unusedTowers, std::multimap<ObjectId, ObjectId>& unusedRings, std::set<ObjectId>& unusedMethodSeries, std::set<ObjectId>& unusedCompositions, std::set<ObjectId>& unusedAssociations) const
{
    // Build id lists of all methods, towers, rings
    ringersDatabase->GetAllObjectIds(unusedRingers);
    methodsDatabase->GetAllObjectIds(unusedMethods);
    towersDatabase->GetAllObjectIds(unusedTowers, unusedRings);
    methodSeriesDatabase->GetAllObjectIds(unusedMethodSeries);
    compositionsDatabase->GetAllObjectIds(unusedCompositions);
    associationsDatabase->GetAllObjectIds(unusedAssociations);

    // Remove used ids
    pealsDatabase->RemoveUsedIds(unusedMethods, unusedRingers, unusedTowers, unusedRings, unusedMethodSeries, unusedCompositions, unusedAssociations);
    methodSeriesDatabase->RemoveUsedIds(unusedMethods);
    compositionsDatabase->RemoveUsedIds(unusedRingers, unusedMethods);
    towersDatabase->RemoveUsedIds(unusedTowers, *settings);
    associationsDatabase->RemoveUsedIds(unusedAssociations);
    ringersDatabase->RemoveUsedIds(unusedRingers);

    if (settings->DefaultRingerSet())
    {
        std::set<Duco::ObjectId>::iterator it = unusedRingers.find(settings->DefaultRinger());
        if (it != unusedRingers.end())
            unusedRingers.erase(it);
    }

    // Remove rings where the tower is listed in the unusedTowers
    std::set<Duco::ObjectId>::const_iterator unusedTowersIt = unusedTowers.begin();
    while (unusedTowersIt != unusedTowers.end())
    {
        unusedRings.erase(*unusedTowersIt);
        ++unusedTowersIt;
    }

    return false;
}

bool
RingingDatabase::FindUnusedPictures(std::set<ObjectId>& unusedPictures) const
{
    // Build id lists of all methods, towers, rings
    picturesDatabase->GetAllObjectIds(unusedPictures);

    // Remove used ids
    pealsDatabase->RemoveUsedPictureIds(unusedPictures);

    // Remove used ids
    towersDatabase->RemoveUsedPictureIds(unusedPictures);

    return false;
}

bool 
RingingDatabase::RemoveDuplicateAssociations()
{
    std::map<Duco::ObjectId, Duco::ObjectId> objectIds;
    associationsDatabase->FindDuplicateIds(objectIds);

    bool updated = false;

    for (const auto & [key, value] : objectIds)
    {
        if (pealsDatabase->ReplaceAssociation(NULL, key, value))
        {
            updated = true;
            associationsDatabase->ReplaceAssociationLinks(NULL, key, value);
            associationsDatabase->DeleteObject(key);
        }
    }

    return updated;
}

void
RingingDatabase::ResetCurrentPointers()
{
    ringersDatabase->ResetCurrentPointer();
    towersDatabase->ResetCurrentPointer();
    methodsDatabase->ResetCurrentPointer();
    pealsDatabase->ResetCurrentPointer();
    methodSeriesDatabase->ResetCurrentPointer();
    picturesDatabase->ResetCurrentPointer();
    compositionsDatabase->ResetCurrentPointer();
    associationsDatabase->ResetCurrentPointer();
}

void
RingingDatabase::PostWinRkImportChecks()
{
    std::set<Duco::ObjectId> towersWithOutRings;
    towersDatabase->GetTowersWithoutRings(towersWithOutRings);

    std::set<Duco::ObjectId> unusedTowers;
    if (FindUnusedTowers(unusedTowers))
    {
        for (auto const& towerIt : towersWithOutRings)
        {
            std::set<Duco::ObjectId>::const_iterator it = unusedTowers.find(towerIt);
            if (it != unusedTowers.end())
            {
                towersDatabase->DeleteObject(towerIt);
            }
        }
    }
}

size_t
RingingDatabase::NumberOfRingers() const
{
    return ringersDatabase->NumberOfObjects();
}

size_t
RingingDatabase::NumberOfTowers() const
{
    return towersDatabase->NumberOfObjects();
}

size_t
RingingDatabase::NumberOfActiveTowers(size_t& noOfLinkedTowers) const
{
    return towersDatabase->NumberOfActiveTowers(noOfLinkedTowers);
}

size_t
RingingDatabase::NumberOfRemovedTowers() const
{
    return towersDatabase->NumberOfRemovedTowers();
}

size_t
RingingDatabase::NumberOfMethods() const
{
    return methodsDatabase->NumberOfObjects();
}

size_t
RingingDatabase::NumberOfCompositions() const
{
    return compositionsDatabase->NumberOfObjects();
}

size_t
RingingDatabase::NumberOfAssociations() const
{
    return associationsDatabase->NumberOfObjects();
}

size_t
RingingDatabase::NumberOfMethodSeries() const
{
    return methodSeriesDatabase->NumberOfObjects();
}

size_t
RingingDatabase::NumberOfPictures() const
{
    return picturesDatabase->NumberOfObjects();
}

PealLengthInfo
RingingDatabase::PerformanceInfo(const Duco::StatisticFilters& filters) const
{
    return pealsDatabase->PerformanceInfo(filters);
}

const RingerDatabase&
RingingDatabase::RingersDatabase() const
{
    return *ringersDatabase;
}

RingerDatabase&
RingingDatabase::RingersDatabase()
{
    return *ringersDatabase;
}

const MethodDatabase&
RingingDatabase::MethodsDatabase() const
{
    return *methodsDatabase;
}

MethodDatabase&
RingingDatabase::MethodsDatabase()
{
    return *methodsDatabase;
}

const PictureDatabase&
RingingDatabase::PicturesDatabase() const
{
    return *picturesDatabase;
}

PictureDatabase&
RingingDatabase::PicturesDatabase()
{
    return *picturesDatabase;
}

const PealDatabase&
RingingDatabase::PealsDatabase() const
{
    return *pealsDatabase;
}

PealDatabase&
RingingDatabase::PealsDatabase()
{
    return *pealsDatabase;
}

const TowerDatabase&
RingingDatabase::TowersDatabase() const
{
    return *towersDatabase;
}

TowerDatabase&
RingingDatabase::TowersDatabase()
{
    return *towersDatabase;
}

const MethodSeriesDatabase&
RingingDatabase::MethodSeriesDatabase() const
{
    return *methodSeriesDatabase;
}

MethodSeriesDatabase&
RingingDatabase::MethodSeriesDatabase()
{
    return *methodSeriesDatabase;
}

const CompositionDatabase&
RingingDatabase::CompositionsDatabase() const
{
    return *compositionsDatabase;
}

CompositionDatabase&
RingingDatabase::CompositionsDatabase()
{
    return *compositionsDatabase;
}

AssociationDatabase&
RingingDatabase::AssociationsDatabase()
{
    return *associationsDatabase;
}

const AssociationDatabase&
RingingDatabase::AssociationsDatabase() const
{
    return *associationsDatabase;
}

bool
RingingDatabase::ReplaceRinger(const ObjectId& oldRingerId, const ObjectId& newRingerId, bool deleteOldRinger)
{
    if (!oldRingerId.ValidId() || !newRingerId.ValidId() || oldRingerId == newRingerId)
        return false;
    if (ringersDatabase->FindRinger(newRingerId) == NULL)
    {
        return false;
    }

    bool changesMade (false);
    if (settings->DefaultRinger() == oldRingerId)
    {
        changesMade = true;
        settings->SetDefaultRinger(newRingerId);
    }

    changesMade |= pealsDatabase->ReplaceRinger(oldRingerId, newRingerId, deleteOldRinger, *this);
    return changesMade;
}

bool
RingingDatabase::ReplaceTower(const ObjectId& oldTowerId, const ObjectId& newTowerId)
{
    if (!oldTowerId.ValidId() || !newTowerId.ValidId() || oldTowerId == newTowerId)
        return false;

    if (towersDatabase->FindTower(newTowerId) == NULL)
    {
        return false;
    }

    bool changesMade(false);
    changesMade |= pealsDatabase->ReplaceTower(oldTowerId, newTowerId);

    return changesMade;
}

bool
RingingDatabase::RebuildRecommended() const
{
    return ringersDatabase->RebuildRecommended() || methodsDatabase->RebuildRecommended() ||
        pealsDatabase->RebuildRecommended() || methodSeriesDatabase->RebuildRecommended() || picturesDatabase->RebuildRecommended() ||
        compositionsDatabase->RebuildRecommended() || towersDatabase->RebuildRecommended() || associationsDatabase->RebuildRecommended();
}


bool
RingingDatabase::SuggestMethodSeries(const std::wstring& methodsStr, unsigned int order, Duco::ObjectId& methodSeriesId, std::set<Duco::ObjectId>& methodIds) const
{
    methodSeriesId.ClearId();
    methodIds.clear();

    std::wstring methodNames = methodsStr;
    size_t position = methodsStr.find_first_of(L";:");
    if (position != string::npos && position < 5)
    {
        methodNames = methodsStr.substr(position+1);
        methodNames = DucoEngineUtils::Trim(methodNames);
    }

    // Work out method names.
    std::set<std::wstring> allMethodNames;
    size_t lastPos (0);
    position = methodNames.find_first_of(L",;. ", lastPos);
    while (position != string::npos)
    {
        std::wstring nextPossibleMethodStr (DucoEngineUtils::Trim(methodNames.substr(lastPos, position - lastPos)));
        CheckPotentialMethodNameAndAdd(nextPossibleMethodStr, allMethodNames);

        lastPos = position + 1;
        position = methodNames.find_first_of(L",;. ", lastPos);
    }
    if (lastPos < methodNames.length())
    {
        std::wstring lastPossibleMethodStr (DucoEngineUtils::Trim(methodNames.substr(lastPos + 1)));
        if (lastPossibleMethodStr.length() > 0)
        {
            CheckPotentialMethodNameAndAdd(lastPossibleMethodStr, allMethodNames);
        }
    }
    
    size_t possibleNumberOfMethods (0);
    std::set<MethodSeries*> potentialSeries;
    if (methodSeriesDatabase->FindMethodSeries(potentialSeries, order))
    {
        for (auto const& methodName : allMethodNames)
        {
            std::set<Duco::ObjectId> objectIds;
            Duco::ObjectId mostLikelyMethodId;
            if (methodsDatabase->SuggestMethodBySeriesString(methodName, order, objectIds, mostLikelyMethodId))
            {
                CheckMethodSeriesAllContainMethodFromList(objectIds, potentialSeries);
                methodIds.insert(mostLikelyMethodId);
                ++possibleNumberOfMethods;
            }
        }
    }

    if (potentialSeries.size() == 1 && (*potentialSeries.begin())->NoOfMethods() <= possibleNumberOfMethods)
    {
        methodSeriesId = (*potentialSeries.begin())->Id();
    }
    else if (potentialSeries.size() > 1 && possibleNumberOfMethods == allMethodNames.size())
    {
        CheckMethodSeriesForNumberOfMethods(possibleNumberOfMethods, potentialSeries);
        if (potentialSeries.size() == 1)
        {
            methodSeriesId = (*potentialSeries.begin())->Id();
        }
    }

    return methodSeriesId.ValidId();
}

void
RingingDatabase::CheckMethodSeriesAllContainMethodFromList(const std::set<Duco::ObjectId>& methodIds, std::set<MethodSeries*>& methodSeries) const
{
    std::set<MethodSeries*>::iterator it = methodSeries.begin();
    while (it != methodSeries.end())
    {
        std::set<MethodSeries*>::iterator it2 = it;
        bool remove(false);
        if (!(*it)->ContainsOneMethodFromList(methodIds))
        {
            remove = true;
        }
        ++it;
        if (remove)
            methodSeries.erase(it2);
    }
}

void
RingingDatabase::CheckMethodSeriesForNumberOfMethods(size_t noOfMethods, std::set<MethodSeries*>& methodSeries) const
{
    std::set<MethodSeries*>::iterator it = methodSeries.begin();
    while (it != methodSeries.end())
    {
        std::set<MethodSeries*>::iterator it2 = it;
        bool remove(false);
        if ((*it)->NoOfMethods() != noOfMethods)
        {
            remove = true;
        }
        ++it;
        if (remove)
            methodSeries.erase(it2);
    }
}

void
RingingDatabase::CheckPotentialMethodNameAndAdd(const std::wstring& nextPossibleMethodStr, std::set<std::wstring>& allMethodNames) const
{
    if (nextPossibleMethodStr.length() < 4)
        return;

    bool allCharsIntegers = true;
    for (unsigned int i = 0; i < nextPossibleMethodStr.length() && allCharsIntegers; ++i)
    {
        allCharsIntegers = isdigit(nextPossibleMethodStr[i]) != 0;
    }
    if (allCharsIntegers)
        return;

    std::set<std::wstring> wordsOftenUsedInSplicedDescriptions = { L"changes", L"work", L"method", L"each"};

    std::wstring nextPossibleMethodStrLowerCase;
    DucoEngineUtils::ToLowerCase(nextPossibleMethodStr, nextPossibleMethodStrLowerCase);

    if (wordsOftenUsedInSplicedDescriptions.find(nextPossibleMethodStrLowerCase) != wordsOftenUsedInSplicedDescriptions.end())
    {
        return;
    }

    allMethodNames.insert(nextPossibleMethodStr);
}

Duco::ObjectId
RingingDatabase::FindPealInTower(const Duco::DoveObject& doveTower, bool onAllBells) const
{
    Duco::ObjectId towerId = towersDatabase->FindTowerByDoveId(doveTower.DoveId());

    Duco::ObjectId pealId;
    if (towerId.ValidId())
    {
        Duco::StatisticFilters filters(*this);
        filters.SetTower(true, towerId);
        if (onAllBells)
            filters.SetStage(true, doveTower.Bells());

        pealId = pealsDatabase->AnyMatch(filters);
    }

    return pealId;
}

bool
RingingDatabase::RemovePictures(const std::set<Duco::ObjectId>& pictureIds)
{
    pealsDatabase->RemovePictures(pictureIds);
    towersDatabase->RemovePictures(pictureIds);
    picturesDatabase->DeleteObjects(pictureIds);
    return true;
}

std::wstring
RingingDatabase::MapBoxMap(size_t& noOfTowersPlotted, size_t& noOfTowersNotPlotted, const std::wstring& staticMapKey,
    bool zeroPerformances, bool onePerformance, bool multiplePerformances, bool combineLinkedWithSamePosition, bool useFelsteadPealCount, const Duco::StatisticFilters& filters) const
{
    noOfTowersPlotted = 0;
    noOfTowersNotPlotted = 0;
    std::map<Duco::ObjectId, Duco::PealLengthInfo> towerIds;
    towerIds.clear();

    if (useFelsteadPealCount)
    {
        towersDatabase->GetFelsteadPealCount(towerIds, *settings, filters, false);
    }
    else
    {
        pealsDatabase->GetTowersPealCount(filters, towerIds, false);
    }

    if (combineLinkedWithSamePosition)
    {
        towersDatabase->MergeTowersWithSameLocation(towerIds);
    }

    std::map<Duco::ObjectId, Duco::PealLengthInfo> filteredTowerIds;
    {
        std::map<Duco::ObjectId, Duco::PealLengthInfo>::const_iterator it = towerIds.begin();
        while (it != towerIds.end())
        {
            if (zeroPerformances && it->second.TotalPeals() == 0)
            {
                filteredTowerIds.insert(*it);
            }
            else if (onePerformance && it->second.TotalPeals() == 1)
            {
                filteredTowerIds.insert(*it);
            }
            else if (multiplePerformances && it->second.TotalPeals() > 1)
            {
                filteredTowerIds.insert(*it);
            }

            ++it;
        }
    }

    std::wstring returnValue = L"<!DOCTYPE html>\n<html>\n<head>\n<meta charset=\"utf-8\">\n<title>Performance map</title>\n" \
        L"<meta name=\"viewport\" content=\"initial-scale=1,maximum-scale=1,user-scalable=no\">\n" \
        L"<link href=\"https://api.mapbox.com/mapbox-gl-js/v2.11.0/mapbox-gl.css\" rel=\"stylesheet\">\n" \
        L"<script src=\"https://api.mapbox.com/mapbox-gl-js/v2.11.0/mapbox-gl.js\"></script>\n" \
        L"<style>body { margin: 0; padding: 0; } " \
        L"#map { position: absolute; top: 0; bottom: 0; width: 100%; }</style>\n</head>\n" \
        L"<body>\n<div id=\"map\"></div>\n<script>\nmapboxgl.accessToken = '";

    returnValue += staticMapKey + L"';\n";

    returnValue += L"const map = new mapboxgl.Map({\ncontainer: 'map',\nstyle: 'mapbox://styles/mapbox/dark-v11',\ncenter: [";
    
    std::wstring features;
    {
        std::map<Duco::ObjectId, Duco::PealLengthInfo>::const_iterator it = filteredTowerIds.begin();
        while (it != filteredTowerIds.end())
        {
            const Tower* theTower = towersDatabase->FindTower(it->first);
            if (theTower != NULL)
            {
                if (theTower->PositionValid())
                {
                    noOfTowersPlotted++;
                    if (features.length() > 0)
                    {
                        features += L"\n";
                    }
                    features += MapBoxMarkerString(0, it->second.TotalPeals(), it->second.DaysSinceLastPeal(), *theTower);
                }
                else
                {
                    noOfTowersNotPlotted++;
                }
            }
            ++it;
        }
    }

    double latitudeMiddle;
    double longitudeMiddle;
    int zoomLevel;
    MapBoxZoomAndMiddle(filteredTowerIds, latitudeMiddle, longitudeMiddle, zoomLevel);

    returnValue += std::to_wstring(longitudeMiddle) + L"," + std::to_wstring(latitudeMiddle);
    returnValue += L"],\nzoom: ";
    returnValue += std::to_wstring(zoomLevel);
    returnValue += L"\n});\nmap.on('load', () => {\n// Add a new source from our GeoJSON data and\n        // set the 'cluster' option to true. GL-JS will\n// add the point_count property to your source data.\nmap.addSource('performances', {\ntype: 'geojson',\ndata: {\n\"type\": \"FeatureCollection\",\n\"crs\": { \"type\": \"name\", \"properties\": { \"name\": \"urn:ogc:def:crs:OGC:1.3:CRS84\" } },\n\"features\": [\n";

    returnValue += features;

    returnValue += L"\n]\n},\ncluster: true,\nclusterMaxZoom: 8, // Max zoom to cluster points on\nclusterRadius: 15 // Radius of each cluster when clustering points (defaults to 50)\n});\n\nmap.addLayer({\nid: 'clusters',\ntype: 'circle',\nsource: 'performances',\nfilter: ['has', 'point_count'],\npaint: {\n// Use step expressions (https://docs.mapbox.com/mapbox-gl-js/style-spec/#expressions-step)\n// with three steps to implement three types of circles:\n//   * Blue, 20px circles when point count is less than 100\n//   * Yellow, 30px circles when point count is between 100 and 750\n//   * Pink, 40px circles when point count is greater than or equal to 750\n'circle-color': [\n'step',\n['get', 'point_count'],\n'#51bbd6',\n100,\n'#f1f075',\n750,\n'#f28cb1'\n],\n'circle-radius': [\n'step',\n['get', 'point_count'],\n20,\n100,\n30,\n750,\n40\n]\n}\n});\n\nmap.addLayer({\nid: 'cluster-count',\ntype: 'symbol',\nsource: 'performances',\nfilter: ['has', 'point_count'],\nlayout: {\n'text-field': ['get', 'point_count_abbreviated'],\n'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],\n'text-size': 12\n}\n});\n\nmap.addLayer(\n{\nid: 'unclustered-background',\ntype: 'circle',\nsource: 'performances',\nfilter: ['!', ['has', 'point_count']],\npaint: {\n'circle-color': ['get', 'colour'],\n'circle-radius': 10,\n'circle-stroke-width': 1,\n'circle-stroke-color': '#fff'\n}\n});\nmap.addLayer(\n{\nid: 'unclustered-point',\ntype: 'symbol',\nsource: 'performances',\nfilter: ['!', ['has', 'point_count']],\nlayout: {\n'text-field': ['get', 'count'],\n'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],\n'text-size': 12\n}\n});\n\n// inspect a cluster on click\nmap.on('click', 'clusters', (e) => {\nconst features = map.queryRenderedFeatures(e.point, {\nlayers: ['clusters']\n});\nconst clusterId = features[0].properties.cluster_id;\nmap.getSource('performances').getClusterExpansionZoom(\nclusterId,\n(err, zoom) => {\nif (err) return;\n\nmap.easeTo({\ncenter: features[0].geometry.coordinates,\nzoom: zoom\n});\n}\n);\n});\n\n// When a click event occurs on a feature in\n// the unclustered-point layer, open a popup at\n// the location of the feature, with\n// description HTML from its properties.\nmap.on('click', 'unclustered-point', (e) => {\nconst coordinates = e.features[0].geometry.coordinates.slice();\nconst bells = e.features[0].properties.bells;\nconst tower = e.features[0].properties.tower;\n\n// Ensure that if the map is zoomed out such that\n// multiple copies of the feature are visible, the\n// popup appears over the copy being pointed to.\nwhile (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {\ncoordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;\n}\n\nnew mapboxgl.Popup()\n.setLngLat(coordinates)\n.setHTML(\n`${tower}: ${bells} bells`\n)\n.addTo(map);\n});\n\nmap.on('mouseenter', 'clusters', () => {\nmap.getCanvas().style.cursor = 'pointer';\n});\nmap.on('mouseleave', 'clusters', () => {\nmap.getCanvas().style.cursor = '';\n});\n});\n</script>\n\n</body>\n</html>";
    return returnValue;
}


bool
RingingDatabase::MapBoxZoomAndMiddle(const std::map<Duco::ObjectId, Duco::PealLengthInfo>& filteredTowerIds, double& latitudeMiddle, double& longitudeMiddle, int& zoomLevel) const
{
    bool positionFound = false;
    latitudeMiddle = 53.946;
    longitudeMiddle = -2.520;
    zoomLevel = 3;

    std::vector<double> latitudes;
    std::vector<double> longitues;
    {
        std::map<Duco::ObjectId, Duco::PealLengthInfo>::const_iterator it = filteredTowerIds.begin();
        while (it != filteredTowerIds.end())
        {
            const Tower* theTower = towersDatabase->FindTower(it->first);
            if (theTower != NULL)
            {
                if (theTower->PositionValid())
                {
                    latitudes.push_back(theTower->LatitudeDegrees());
                    longitues.push_back(theTower->LongitudeDegrees());
                }
            }
            ++it;
        }
    }

    if (latitudes.size() > 0 && longitues.size() > 0)
    {
        positionFound = true;
        const auto [minlat, maxlat] = std::minmax_element(latitudes.begin(), latitudes.end());
        const auto [minlong, maxlong] = std::minmax_element(longitues.begin(), longitues.end());

        if (latitudes.size() == 1 && longitues.size() == 1)
        {
            latitudeMiddle = *maxlat;
            longitudeMiddle = *maxlong;
        }
        else
        {
            latitudeMiddle = (*maxlat + *minlat) / 2;
            longitudeMiddle = (*maxlong + *minlong) / 2;
        }

        double latitudeRange = *maxlat - *minlat;
        double longitudeRange = *maxlong - *minlong;
        double largestRange = std::max(latitudeRange, longitudeRange);

        if (largestRange <= 0)
        {
            zoomLevel = 16;
        }
        else if (largestRange <= 3.0f)
        { // Manc DB
            zoomLevel = 11;
        }
        else if (largestRange <= 4.0f)
        {
            zoomLevel = 7;
        }
        else if (largestRange <= 6.6f)
        {
            zoomLevel = 6;
        }
    }
    return positionFound;
}

std::wstring
RingingDatabase::MapBoxMarkerString(size_t markerId, size_t noOfperformances, size_t daysSinceLastPeal, const Duco::Tower& theTower) const
{
    std::wstring marker = L"{" \
        L"\"type\": \"Feature\", " \
        L"\"properties\" : {" \
        L"\"id\": \"";
    marker += theTower.Id().Str();
    marker += L"\", \"count\": ";
    marker += DucoEngineUtils::ToString(noOfperformances);
    marker += L", \"tower\": '";
    if (theTower.DoveRefSet())
    {
        marker += L"<a href=\"" + theTower.DoveUrl(Settings()) + L"\">";
    }
    marker += DucoEngineUtils::EncodeString(theTower.FullName());
    if (theTower.DoveRefSet())
    {
        marker += L"</a>";
    }
    marker += L"', \"bells\": ";
    marker += DucoEngineUtils::ToString(theTower.BellsForDove());
    marker += L", \"colour\": '";
    if (theTower.Removed())
    {
        marker += L"red";
    }
    else if (daysSinceLastPeal > (365 * 10))
    {
        marker += L"orange";
    }
    else
    {
        marker += L"green";
    }
    marker += L"'}, \"geometry\": { \"type\": \"Point\", \"coordinates\": [";
    marker += theTower.Longitude() + L", " + theTower.Latitude();
    marker += L"] } },";

    return marker;
}

size_t
RingingDatabase::NumberOfObjectsWithErrors(bool includeWarnings, TObjectType objectType)
{
    DatabaseSearch search(*this);
    DummyProgressCallback callback;
    Duco::TSearchValidObject* validArgument = new Duco::TSearchValidObject(includeWarnings);
    search.AddValidationArgument(validArgument);
    std::set<Duco::ObjectId> matchObjectIds;
    search.Search(callback, matchObjectIds, objectType);
    return matchObjectIds.size();
}

Duco::ObjectId
RingingDatabase::FindPicture(const std::wstring& description, const Duco::ObjectId& currentPictureId)
{
    std::set<Duco::ObjectId> matchingPictureIds;
    DummyProgressCallback callback;

    DatabaseSearch search(*this);
    Duco::TSearchFieldStringArgument* pictureArgument = new Duco::TSearchFieldStringArgument(TSearchFieldId::ENotes, description, true, false);
    search.AddArgument(pictureArgument);
    search.Search(callback, matchingPictureIds, TObjectType::EPicture);

    search.Clear();
    Duco::TSearchGroup* group = new Duco::TSearchGroup(Duco::TSearchFieldId::EOrGroup);
    search.AddArgument(group);
    Duco::TSearchFieldStringArgument* towerCityArgument = new Duco::TSearchFieldStringArgument(TSearchFieldId::ECity, description, true, false);
    group->AddArgument(towerCityArgument);
    Duco::TSearchFieldStringArgument* towerNameArgument = new Duco::TSearchFieldStringArgument(TSearchFieldId::ETowerName, description, true, false);
    group->AddArgument(towerNameArgument);
    std::set<Duco::ObjectId> towersMatching;
    search.Search(callback, towersMatching, TObjectType::ETower);

    for (auto const& nextTowerId : towersMatching)
    {
        Duco::ObjectId towerPictureId;
        if (pealsDatabase->FindPictureForTower(nextTowerId, towerPictureId))
        {
            matchingPictureIds.insert(towerPictureId);
        }
        if (towersDatabase->GetPictureIdForTower(nextTowerId, towerPictureId))
        {
            matchingPictureIds.insert(towerPictureId);
        }
    }

    if (matchingPictureIds.size() > 0)
    {
        if (!currentPictureId.ValidId())
        {
            return *matchingPictureIds.begin();
        }

        std::set<Duco::ObjectId>::const_iterator foundPictureId = matchingPictureIds.find(currentPictureId);
        if (foundPictureId != matchingPictureIds.end())
        {
            ++foundPictureId;
        }
        if (foundPictureId != matchingPictureIds.end())
        {
            return *foundPictureId;
        }
    }
    return KNoId;
}

bool
RingingDatabase::SetTowersWithHandbellPealsAsHandbells()
{
    std::set<Duco::ObjectId> towerIds;
    pealsDatabase->FindTowerIdForHandbellPeals(towerIds);

    return towersDatabase->SetTowersAsHandbell(towerIds);
}

size_t
RingingDatabase::CheckBellBoardForUpdatedReferences(Duco::ProgressCallback& progressCallback, bool onlyThoseWithoutRWPages, bool& cancellationFlag)
{
    return pealsDatabase->CheckBellBoardForUpdatedReferences(progressCallback, *this, onlyThoseWithoutRWPages, cancellationFlag);
}

bool
RingingDatabase::CapitaliseFields(Duco::ProgressCallback& progressCallback, bool methodName, bool methodType, bool county, bool dedication, bool ringerNames)
{
    bool changes = false;
    progressCallback.Initialised();
    //virtual void Step(int progressPercent) = 0;
    size_t numberOfObjectsToUpdate = 0;
    if (methodName || methodType)
    {
        numberOfObjectsToUpdate += methodsDatabase->NumberOfObjects();
    }
    if (county || dedication)
    {
        numberOfObjectsToUpdate += towersDatabase->NumberOfObjects();
    }
    if (ringerNames)
    {
        numberOfObjectsToUpdate += ringersDatabase->NumberOfObjects();
    }
    size_t numberOfObjectsUpdated = 0;
    if (methodName || methodType)
    {
        changes |= methodsDatabase->CapitaliseField(progressCallback, methodName, methodType, numberOfObjectsUpdated, numberOfObjectsToUpdate);
    }
    if (county || dedication)
    {
        changes |= towersDatabase->CapitaliseField(progressCallback, county, dedication, numberOfObjectsUpdated, numberOfObjectsToUpdate);
    }
    if (ringerNames)
    {
        changes |= ringersDatabase->CapitaliseField(progressCallback, numberOfObjectsUpdated, numberOfObjectsToUpdate);
    }
    progressCallback.Complete(false);
    return changes;
}

Duco::ObjectId
RingingDatabase::SuggestRingers(const std::wstring& ringerFullname, const PerformanceDate& performanceDate, bool partialMatchAllowed, std::set<Duco::RingerPealDates>& nearMatches) const
{
    nearMatches.clear();
    std::set<Duco::ObjectId> ringerIds;
    ObjectId ringerId = ringersDatabase->SuggestRinger(ringerFullname, partialMatchAllowed, ringerIds);
    if (!ringerId.ValidId() && ringerIds.size() > 0)
    {
        for (auto const& nextRingerId : ringerIds)
        {
            nearMatches.insert(RingerPealDates(nextRingerId, performanceDate));
        }
        pealsDatabase->FindRingerPerformancesNearDate(performanceDate, ringerIds, nearMatches);
    }
    return ringerId;
}
