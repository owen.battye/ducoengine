#include <cassert>
#include <stdlib.h>
#include "CompositionTable.h"
#include "Calling.h"
#include "PlaceNotation.h"

using namespace Duco;
using namespace std;

CompositionTable::CompositionTable(const Duco::LeadOrder& newLeadOrder)
    : leadOrder(newLeadOrder), lastBell(-1)
{

}

CompositionTable::~CompositionTable()
{

}

bool
CompositionTable::AddCall(const Duco::Calling& nextCall, const Duco::PlaceNotation& notation)
{
    size_t noOfChanges = 0;
    unsigned int nextIndex = leadOrder.Index(nextCall.Position());
    if (lastBell == -1)
    {
        Duco::Course newCourse(notation);
        newCourse.AddCalling(nextCall, noOfChanges, 0);
        courses.push_back(newCourse);
    }
    else
    {
        if (nextIndex < lastBell)
        {
            Duco::Course newCourse(notation, courses.rbegin()->CourseEnd());
            newCourse.AddCalling(nextCall, noOfChanges, 0);
            courses.push_back(newCourse);
        }
        else
        {
            courses.rbegin()->AddCalling(nextCall, noOfChanges, 0);
        }
    }
    lastBell = nextIndex;

    return true;
}

size_t
CompositionTable::NoOfCourses() const
{
    return courses.size();
}

const Duco::Course&
CompositionTable::Course(size_t index) const
{
    return courses[index];
}

const Duco::LeadOrder&
CompositionTable::LeadOrder() const
{
    return leadOrder;
}

void
CompositionTable::CompletedGeneration()
{
    std::deque<Duco::Course>::iterator it = courses.begin();
    while (it != courses.end())
    {
        it->MoveToHome();
        ++it;
    }
}
