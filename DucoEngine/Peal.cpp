#include "Peal.h"

#include <algorithm>
#include <sstream>

#include "Association.h"
#include "AssociationDatabase.h"
#include "AlphabetData.h"
#include "Composition.h"
#include "CompositionDatabase.h"
#include "DatabaseReader.h"
#include "DatabaseWriter.h"
#include "DucoEngineUtils.h"
#include "PerformanceDate.h"
#include "Method.h"
#include "MethodDatabase.h"
#include "MethodSeries.h"
#include "MethodSeriesDatabase.h"
#include "Ringer.h"
#include "Ring.h"
#include "RingerDatabase.h"
#include "RingerCirclingData.h"
#include "RingingDatabase.h"
#include "Tower.h"
#include "TowerDatabase.h"
#include <regex>

#include "DucoXmlUtils.h"
#include <xercesc\dom\DOMElement.hpp>
#include <xercesc\util\XMLString.hpp>
XERCES_CPP_NAMESPACE_USE

using namespace std;
using namespace Duco;

const unsigned int KOldStrapperId = 99;

Peal::Peal(const Duco::ObjectId& newId, const Duco::ObjectId& newAssociationId, const ObjectId& newTowerId, const ObjectId& newRingId,
           const ObjectId& newMethodId, const std::wstring& newComposer, const std::wstring& newFootnotes, const Duco::PerformanceDate& newDate, const Duco::PerformanceTime& newTime,
           unsigned int newChanges, bool newHandBell, const std::map<unsigned int, ObjectId>& newRingerIds, TConductorType newConductorType,
           const std::set<ObjectId>& newConductorIds, unsigned int newDayOrder)
:   RingingObject(newId), associationId(newAssociationId), towerId(newTowerId), ringId(newRingId), methodId(newMethodId),
    multiMethods(L""), noOfMultiMethods(0), compositionId(KNoId), seriesId(KNoId), composer(newComposer), footnotes(newFootnotes), performanceDate(newDate), performanceTime(newTime),
    handBell(newHandBell), noOfChanges(newChanges), conductedType(newConductorType),
    conductorIds(newConductorIds), bellBoardId(L""), ringingWorldReference (L""),
    notes(L""), wrongStage(false), dayOrder(newDayOrder), withdrawn(false), doubleHanded(false), notable(false), containsSimulatedSound(false)
{
    std::map<unsigned int, ObjectId>::const_iterator it = newRingerIds.begin();
    while (it != newRingerIds.end())
    {
        PealRingerData newRinger(it->second, 0, false);
        std::pair<unsigned int, PealRingerData> newObject (it->first, newRinger);
        ringerIds.insert(newObject);
        ++it;
    }
}

Peal::Peal()
:   RingingObject(KNoId), associationId(KNoId), towerId(KNoId), ringId(KNoId), methodId(KNoId),  multiMethods(L""),
    noOfMultiMethods(0), compositionId(KNoId), seriesId(KNoId), composer(L""), footnotes(L""), noOfChanges(0),
    handBell(false), conductedType(ESingleConductor), bellBoardId(L""), ringingWorldReference (L""),
    notes(L""), dayOrder(0), withdrawn(false), wrongStage(false), doubleHanded(false), notable(false),
    containsSimulatedSound(false)
{
}

Peal::Peal(DatabaseReader& reader, unsigned int databaseVersion)
: RingingObject(), multiMethods(L""),
    noOfMultiMethods(0), composer(L""), footnotes(L""), handBell(false), noOfChanges(0),
    conductedType(ESingleConductor), bellBoardId(L""), ringingWorldReference (L""),
    notes(L""), dayOrder(0), withdrawn(false), wrongStage(false), doubleHanded(false), notable(false), containsSimulatedSound(false)
{
    associationId.ClearId();
    pictureId.ClearId();
    towerId.ClearId();
    ringId.ClearId();
    methodId.ClearId();
    compositionId.ClearId();
    seriesId.ClearId();
    Internalise(reader, databaseVersion);
}

Peal::Peal(const Peal& other)
    : RingingObject(other), associationId(other.associationId), pictureId(other.pictureId), towerId(other.towerId), ringId(other.ringId), methodId(other.methodId),
    multiMethods(other.multiMethods), noOfMultiMethods(other.noOfMultiMethods), compositionId(other.compositionId), seriesId(other.seriesId), composer(other.composer),
    footnotes(other.footnotes), performanceDate(other.performanceDate), performanceTime(other.performanceTime), handBell(other.handBell), wrongStage(other.wrongStage),
    noOfChanges(other.noOfChanges), ringerIds(other.ringerIds), strapperIds(other.strapperIds), conductedType(other.conductedType),
    conductorIds(other.conductorIds), bellBoardId(other.bellBoardId), ringingWorldReference (other.ringingWorldReference),
    dayOrder(other.dayOrder), notes(other.notes), withdrawn(other.withdrawn), doubleHanded(other.doubleHanded), notable(other.notable),
    containsSimulatedSound(other.containsSimulatedSound), enteredBy(other.enteredBy)
{

}

Peal::Peal(const Duco::ObjectId& newId, const Peal& other)
    : RingingObject(newId), associationId(other.associationId), pictureId(other.pictureId), towerId(other.towerId), ringId(other.ringId), methodId(other.methodId),
    multiMethods(other.multiMethods), noOfMultiMethods(other.noOfMultiMethods), compositionId(other.compositionId), seriesId(other.seriesId), composer(other.composer),
    footnotes(other.footnotes), performanceDate(other.performanceDate), performanceTime(other.performanceTime), handBell(other.handBell), wrongStage(other.wrongStage),
    noOfChanges(other.noOfChanges), ringerIds(other.ringerIds), strapperIds(other.strapperIds), conductedType(other.conductedType),
    conductorIds(other.conductorIds), bellBoardId(other.bellBoardId), ringingWorldReference (other.ringingWorldReference),
    dayOrder(other.dayOrder), notes(other.notes), withdrawn(other.withdrawn), doubleHanded(other.doubleHanded), notable(other.notable),
    containsSimulatedSound(other.containsSimulatedSound), enteredBy(other.enteredBy)
{

}

Peal::~Peal(void)
{
    ringerIds.clear();
    strapperIds.clear();
}

void 
Peal::Clear()
{
    id.ClearId();
    associationId.ClearId();
    pictureId.ClearId();
    towerId.ClearId();
    ringId.ClearId();
    methodId.ClearId();
    multiMethods.clear();
    noOfMultiMethods = 0;
    compositionId.ClearId();
    seriesId.ClearId();
    composer.clear();
    footnotes.clear();
    performanceDate.Clear();
    performanceTime.Clear();
    handBell = false;
    wrongStage = false;
    noOfChanges = 0;
    ringerIds.clear();
    strapperIds.clear();
    conductedType = ESingleConductor;
    conductorIds.clear();
    bellBoardId.clear();
    ringingWorldReference.clear();
    notes.clear();
    dayOrder = 0;
    withdrawn = false;
    doubleHanded = false;
    notable = false;
    containsSimulatedSound = false;
    enteredBy.clear();
    ClearErrors();
}

Duco::TObjectType 
Peal::ObjectType() const
{
    return TObjectType::EPeal;
}

std::wstring
Peal::AssociationName(const Duco::AssociationDatabase& associationDatabase) const
{
    const Duco::Association* const theAssociation = associationDatabase.FindAssociation(associationId);
    if (theAssociation != NULL)
    {
        return theAssociation->Name();
    }
    return L"";
}

void
Peal::SetAssociationId(const Duco::ObjectId& newAssociationId)
{
    associationId = newAssociationId;
}

std::wstring
Peal::AssociationName(const Duco::RingingDatabase& ringingDatabase) const
{
    return AssociationName(ringingDatabase.AssociationsDatabase());
}

void
Peal::SetAssociationIdAndClearOldAssociation(const Duco::ObjectId& newAssociationId)
{
    SetAssociationId(newAssociationId);
    oldAssociationAsString.clear();
}

void
Peal::ExportToCsv(std::wofstream& file, const Duco::RingerDatabase& ringersDatabase, const Duco::MethodDatabase& methodsDatabase, const Duco::TowerDatabase& towersDatabase, const Duco::AssociationDatabase& associationDatabase) const
{
    file << Id().Id() << ",";
    file << "\"" << AssociationName(associationDatabase) << "\",";
    file << performanceDate.Str() << ",";
    const Duco::Tower* nextTowerObj = towersDatabase.FindTower(towerId);
    if (nextTowerObj != NULL)
    {
        nextTowerObj->ExportToCsv(file);
        file << ",";
        WriteStringWithReplaceToCsv(file, nextTowerObj->TenorWeight(ringId));
    }
    else
    {
        file << "\"!Missing tower!\",\"Missing tenor\",";
    }
    file << performanceTime.PrintPealTime(true) << ",";
    file << noOfChanges << ",";
    const Duco::Method* nextMethodObj = methodsDatabase.FindMethod(methodId);
    if (nextMethodObj != NULL)
    {
        nextMethodObj->ExportToCsv(file, methodsDatabase);
    }
    else
    {
        file << "\"!Missing method!\"";
    }
    file << ",";
    if (composer.length() > 0)
    {
        file << "\"" << composer << "\"";
    }
    file << ",";
    int numberOfSupportedRingers = 22;
    std::map<unsigned int, PealRingerData>::const_iterator ringers = ringerIds.begin();
    while (ringers != ringerIds.end())
    {
        std::wstring ringerName = L"!Missing ringer";
        const Duco::Ringer* nextRingerObj = ringersDatabase.FindRinger(ringers->second.RingerId());
        if (nextRingerObj != NULL)
        {
            ringerName = nextRingerObj->FullName(performanceDate, false);
        }
        file << "\"" << ringerName;
        if (conductorIds.find(ringers->second.RingerId()) != conductorIds.end())
        {
            file << " (C)";
        }

        file << "\",";
        --numberOfSupportedRingers;
        ++ringers;
    }
    for (; numberOfSupportedRingers > 0; --numberOfSupportedRingers)
    {
        file << ",";
    }
    //Only one strapper supported
    if (strapperIds.size() > 0)
    {
        std::map<unsigned int, PealRingerData>::const_iterator strappers = strapperIds.begin();
        std::wstring ringerName = L"!Missing ringer";
        const Duco::Ringer* nextRingerObj = ringersDatabase.FindRinger(strappers->second.RingerId());
        if (nextRingerObj != NULL)
        {
            ringerName = nextRingerObj->FullName(performanceDate, false);
        }
        file << "\"" << ringerName;
        if (conductorIds.find(strappers->second.RingerId()) != conductorIds.end())
        {
            file << " (C)";
        }
        file << "\",";
    }
    else
    {
        file << ",";
    }
    WriteBooleanToCsv(file, handBell);
    WriteBooleanToCsv(file, withdrawn);
    WriteBooleanToCsv(file, doubleHanded);
    WriteStringWithReplaceToCsv(file, footnotes);
    WriteStringWithReplaceToCsv(file, multiMethods);
    WriteStringWithReplaceToCsv(file, ringingWorldReference);
    WriteStringWithReplaceToCsv(file, bellBoardId);
    if (nextTowerObj != NULL)
    {
        WriteStringWithReplaceToCsv(file, nextTowerObj->DoveRef());
        WriteStringWithReplaceToCsv(file, nextTowerObj->TowerbaseId());
    }
    else
    {
        file << "\"!Missing tower!\",\"Missing tower\",";
    }
}

void
Peal::WriteBooleanToCsv(std::wofstream& file, bool state) const
{
    if (state)
    {
        file << "true";
    }
    file << ",";
}

void
Peal::WriteStringWithReplaceToCsv(std::wofstream& file, const std::wstring& originalString, const std::wstring& strToFind, const std::wstring& strToReplaceWith) const
{
    if (originalString.length() > 0)
    {
        file << "\"";
        std::wstring stringToWrite = originalString;
        for (string::size_type i = 0; (i = stringToWrite.find(strToFind, i)) != string::npos;)
        {
            stringToWrite.replace(i, strToFind.length(), strToReplaceWith);
            i += strToReplaceWith.length();
        }

        for (size_t count = 0; count < stringToWrite.length(); ++count)
        {
            wofstream::char_type nextChar = stringToWrite[count];
            if (nextChar == 8216 || nextChar == 8217)
            {
                file.put('\'');
            }
            else if (nextChar <= 256)
            {
                file.put(nextChar);
            }
        }
        file << "\"";
    }
    file << ",";
}

void
Peal::ExternaliseToBellboardXml(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& document, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& rootElement, const Duco::RingerDatabase& ringersDatabase, const Duco::MethodDatabase& methodsDatabase, const Duco::TowerDatabase& towersDatabase, const Duco::AssociationDatabase& associationDatabase) const
{
    DOMElement* performanceElement = DucoXmlUtils::AddElement(document, rootElement, L"performance", L"");
    if (bellBoardId.length() > 0)
    {
        DucoXmlUtils::AddAttribute(*performanceElement, L"id", L"P" + bellBoardId);
    }
    
    if (associationId.ValidId())
    {
        std::wstring associationName = AssociationName(associationDatabase);
        if (associationName.length() > 0)
        {
            DucoXmlUtils::AddElement(document, *performanceElement, L"association", associationName);
        }
    }

    DOMElement* placeRootElement = DucoXmlUtils::AddElement(document, *performanceElement, L"place", L"");
    if (towerId.ValidId())
    {
        const Duco::Tower* nextTowerObj = towersDatabase.FindTower(towerId);
        if (nextTowerObj != NULL)
        {
            if (nextTowerObj->ValidTowerbaseId())
            {
                DucoXmlUtils::AddAttribute(*placeRootElement, L"towerbase-id", nextTowerObj->TowerbaseId());
            }
            DOMElement* placeElement = DucoXmlUtils::AddElement(document, *placeRootElement, L"place-name", nextTowerObj->City());
            DucoXmlUtils::AddAttribute(*placeElement, L"type", L"place");
            placeElement = DucoXmlUtils::AddElement(document, *placeRootElement, L"place-name", nextTowerObj->Name());
            DucoXmlUtils::AddAttribute(*placeElement, L"type", L"dedication");
            placeElement = DucoXmlUtils::AddElement(document, *placeRootElement, L"place-name", nextTowerObj->County());
            DucoXmlUtils::AddAttribute(*placeElement, L"type", L"county");
            placeElement = DucoXmlUtils::AddElement(document, *placeRootElement, L"ring", L"");
            DucoXmlUtils::AddAttribute(*placeElement, L"tenor", nextTowerObj->TenorWeight(ringId));
            if (nextTowerObj->Handbell())
                placeElement->setAttribute(XMLStrL("type"), L"hand");
            else
                placeElement->setAttribute(XMLStrL("type"), L"tower");
        }
    }
    DucoXmlUtils::AddElement(document, *performanceElement, L"date", performanceDate.StrForBBExport());
    DucoXmlUtils::AddElement(document, *performanceElement, L"duration", performanceTime.PrintShortPealTime());

    DOMElement* titleElement = DucoXmlUtils::AddElement(document, *performanceElement, L"title", L"");
    DucoXmlUtils::AddElement(document, *titleElement, L"changes", noOfChanges);
    DucoXmlUtils::AddElement(document, *titleElement, L"method", MethodName(methodsDatabase));

    DOMElement* ringersElement = DucoXmlUtils::AddElement(document, *performanceElement, L"ringers", L"");
    {
        std::map<unsigned int, PealRingerData>::const_iterator ringers = ringerIds.begin();
        while (ringers != ringerIds.end())
        {
            std::wstring ringerName;
            const Duco::Ringer* nextRingerObj = ringersDatabase.FindRinger(ringers->second.RingerId());
            if (nextRingerObj != NULL)
            {
                ringerName = nextRingerObj->FullName(performanceDate, false);
            }
            DOMElement* nextRinger = DucoXmlUtils::AddElement(document, *ringersElement, L"ringer", ringerName);
            nextRinger->setAttribute(XMLStrL("bell"), DucoEngineUtils::ToString(ringers->first).c_str());
            if (ContainsConductor(ringers->second.RingerId()))
            {
                nextRinger->setAttribute(XMLStrL("conductor"), L"true");
            }
            ++ringers;
        }
    }

    std::list<std::wstring> tokens;
    DucoEngineUtils::Tokenise(footnotes, tokens, KNewlineChar);
    std::list<std::wstring>::const_iterator it = tokens.begin();
    while (it != tokens.end())
    {
        if (it->length() > 0)
            DucoXmlUtils::AddElement(document, *performanceElement, L"footnote", *it);
        ++it;
    }
    if (ValidRingingWorldReferenceForUpload())
        DucoXmlUtils::AddElement(document, *performanceElement, L"rwref", ringingWorldReference);
}

void
Peal::ExportToXml(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& performances, bool includeDucoObjects, 
                  const Duco::RingerDatabase& ringersDatabase, const Duco::MethodDatabase& methodsDatabase, const Duco::TowerDatabase& towersDatabase, const Duco::AssociationDatabase& associationDatabase) const
{
    DOMElement* newPerformance = outputFile.createElement(XMLStrL("Performance"));
    performances.appendChild(newPerformance);
    if (includeDucoObjects)
    {
        DucoXmlUtils::AddObjectId(*newPerformance, Id());
    }
    if (handBell)
    {
        DucoXmlUtils::AddBoolAttribute(*newPerformance, L"Handbell", handBell);
    }
    if (withdrawn)
    {
        DucoXmlUtils::AddBoolAttribute(*newPerformance, L"Withdrawn", withdrawn);
    }
    if (doubleHanded)
    {
        DucoXmlUtils::AddBoolAttribute(*newPerformance, L"DoubleHanded", doubleHanded);
    }

    DucoXmlUtils::AddElement(outputFile, *newPerformance, L"Association", AssociationName(associationDatabase));
    DucoXmlUtils::AddElement(outputFile, *newPerformance, L"Date", performanceDate.Str());
    DucoXmlUtils::AddElement(outputFile, *newPerformance, L"Duration", performanceTime.PrintPealTime(true));

    {
        DOMElement* composition = DucoXmlUtils::AddElement(outputFile, *newPerformance, L"Composition", L"");
        if (seriesId.ValidId() && includeDucoObjects)
        {
            DucoXmlUtils::AddObjectId(*composition, compositionId, includeDucoObjects);
        }
        DucoXmlUtils::AddElement(outputFile, *composition, L"Changes", DucoEngineUtils::ToString(noOfChanges));
        const Duco::Method* nextMethodObj = methodsDatabase.FindMethod(methodId);
        if (nextMethodObj != NULL)
        {
            DOMElement* method = DucoXmlUtils::AddElement(outputFile, *composition, L"Method", nextMethodObj->FullName(methodsDatabase));
            if (includeDucoObjects)
            {
                DucoXmlUtils::AddObjectId(*method, methodId);
            }
        }
        DOMElement* multiMethodsElement = DucoXmlUtils::AddElement(outputFile, *composition, L"MultiMethods", multiMethods, false);
        if (multiMethodsElement != NULL && noOfMultiMethods > 0)
        {
            DucoXmlUtils::AddAttribute(*multiMethodsElement, L"NoOfMethods", noOfMultiMethods);
        }
        if (seriesId.ValidId() && includeDucoObjects)
        {
            DOMElement* methodSeriesElement = DucoXmlUtils::AddElement(outputFile, *composition, L"MethodSeries", L"", true);
            DucoXmlUtils::AddObjectId(*methodSeriesElement, seriesId, includeDucoObjects);
        }
        if (compositionId.ValidId() && includeDucoObjects)
        {
            DOMElement* compositionIdElement = DucoXmlUtils::AddElement(outputFile, *composition, L"CompositionId", L"", true);
            DucoXmlUtils::AddObjectId(*compositionIdElement, compositionId, includeDucoObjects);
        }
        DucoXmlUtils::AddElement(outputFile, *composition, L"Composer", composer, false);
    }

    {
        DOMElement* newLocation = DucoXmlUtils::AddElement(outputFile, *newPerformance, L"Location", L"");
        const Duco::Tower* nextTowerObj = towersDatabase.FindTower(towerId);
        DOMElement* towerElement = DucoXmlUtils::AddElement(outputFile, *newLocation, L"Tower", L"", true);
        if (nextTowerObj != NULL)
        {
            nextTowerObj->ExportToXmlInPeal(outputFile, *towerElement, ringId, includeDucoObjects);
        }
    }
    {
        DOMElement* newRingers = DucoXmlUtils::AddElement(outputFile, *newPerformance, L"Ringers", L"");
        {
            std::map<unsigned int, PealRingerData>::const_iterator ringers = ringerIds.begin();
            while (ringers != ringerIds.end())
            {
                std::wstring ringerName;
                const Duco::Ringer* nextRingerObj = ringersDatabase.FindRinger(ringers->second.RingerId());
                if (nextRingerObj != NULL)
                {
                    ringerName = nextRingerObj->FullName(performanceDate, false);
                }
                DOMElement* nextRinger = DucoXmlUtils::AddElement(outputFile, *newRingers, L"Ringer", ringerName);
                if (includeDucoObjects)
                {
                    DucoXmlUtils::AddObjectId(*nextRinger, ringers->second.RingerId());
                }
                nextRinger->setAttribute(XMLStrL("Bell"), DucoEngineUtils::ToString(ringers->first).c_str());
                ++ringers;
            }
        }

        {
            std::map<unsigned int, PealRingerData>::const_iterator strappers = strapperIds.begin();
            while (strappers != strapperIds.end())
            {
                std::wstring ringerName;
                const Duco::Ringer* nextRingerObj = ringersDatabase.FindRinger(strappers->second.RingerId());
                if (nextRingerObj != NULL)
                {
                    ringerName = nextRingerObj->FullName(performanceDate, false);
                }
                DOMElement* nextRinger = DucoXmlUtils::AddElement(outputFile, *newRingers, L"Strapper", ringerName);
                if (includeDucoObjects)
                {
                    DucoXmlUtils::AddObjectId(*nextRinger, strappers->second.RingerId());
                }
                nextRinger->setAttribute(XMLStrL("Bell"), DucoEngineUtils::ToString(strappers->first).c_str());
                ++strappers;
            }
        }
    }

    {
        DOMElement* newConductors = outputFile.createElement(XMLStrL("Conductors"));
        newPerformance->appendChild(newConductors);
        switch (conductedType)
        {
        case ESilentAndNonConducted:
            DucoXmlUtils::AddAttribute(*newConductors, L"ConductedType", L"Silent");
            break;
        case EJointlyConducted:
            DucoXmlUtils::AddAttribute(*newConductors, L"ConductedType", L"JointlyConducted");
            break;
        }

        std::set<Duco::ObjectId>::const_iterator conductors = conductorIds.begin();
        while (conductors != conductorIds.end())
        {
            std::wstring ringerName;
            const Duco::Ringer* nextRingerObj = ringersDatabase.FindRinger(*conductors);
            if (nextRingerObj != NULL)
            {
                ringerName = nextRingerObj->FullName(performanceDate, false);
            }
            DOMElement* nextConductor = DucoXmlUtils::AddElement(outputFile, *newConductors, L"Conductor", ringerName);
            if (includeDucoObjects)
            {
                DucoXmlUtils::AddObjectId(*nextConductor, *conductors);
            }
            ++conductors;
        }
    }

    DucoXmlUtils::AddElement(outputFile, *newPerformance, L"Footnote", footnotes, false);
    DucoXmlUtils::AddElement(outputFile, *newPerformance, L"Notes", notes, false);
    {
        DOMElement* sources = DucoXmlUtils::AddElement(outputFile, *newPerformance, L"Sources", L"");
        DucoXmlUtils::AddElement(outputFile, *sources, L"BellBoard", bellBoardId, false);
        DucoXmlUtils::AddElement(outputFile, *sources, L"RingingWorld", ringingWorldReference, false);
    }
}

std::wofstream&
Peal::ExportToRtf(std::wofstream& outputFile, const RingingDatabase& database) const
{
    outputFile << id.Id() << KRtfLineEnd << std::endl;

    const Tower* theTower = database.TowersDatabase().FindTower(towerId);
    if (theTower == NULL)
    {
        outputFile << "Error finding tower" << KRtfLineEnd << std::endl;
    }
    else
    {
        theTower->ExportToRtf(outputFile, database) << KRtfLineEnd << std::endl;
    }
    
    outputFile << performanceDate.WeekDay(true) << ", ";
    performanceDate.PrintDate(outputFile);
    outputFile << ", " << performanceTime.PrintPealTime(false) << KRtfLineEnd << std::endl;

    outputFile << "{\\b " << DucoEngineUtils::ToString(noOfChanges) << " ";
    const Method* theMethod = database.MethodsDatabase().FindMethod(methodId);
    if (theMethod == NULL)
    {
        outputFile << "Error finding method}" << KRtfLineEnd << std::endl;
    }
    else
    {
        theMethod->ExportToRtf(outputFile, database) << "}" << KRtfLineEnd << std::endl;
    }

    if (multiMethods.length() > 0)
    {
        outputFile << DucoEngineUtils::PrintToRtf(multiMethods) << KRtfLineEnd << std::endl;
    }
    if (composer.length() > 0)
    {
        outputFile << "Composed by: " << composer << KRtfLineEnd << std::endl;
    }
    std::map<unsigned int, PealRingerData>::const_iterator it = ringerIds.begin();
    while (it != ringerIds.end())
    {
        ObjectId ringerId = it->second.RingerId();
        if (handBell)
        {
            unsigned int ringerFirstBell ((it->first*2) -1);
            outputFile << ringerFirstBell++ << "-" << ringerFirstBell << "\\tab ";
        }
        else
        {
            outputFile << it->first << "\\tab ";
        }

        const Duco::Ringer* ringer = database.RingersDatabase().FindRinger(ringerId);
        if (ringer != NULL)
        {
            ringer->Print(outputFile, performanceDate, database);
            if (ContainsConductor(ringerId))
                outputFile << " (C)";
        }
        std::map<unsigned int, PealRingerData>::const_iterator strapperIt = strapperIds.find(it->first);
        if (strapperIt != strapperIds.end())
        {
            const Duco::Ringer* strapper = database.RingersDatabase().FindRinger(strapperIt->second.RingerId());
            if (strapper != NULL)
            {
                outputFile << " & ";
                strapper->Print(outputFile, performanceDate, database);
                if (ContainsConductor(strapperIt->second.RingerId()))
                    outputFile << " (C)";
            }
        }
        outputFile << KRtfLineEnd << std::endl;
        ++it;
    }
    if (footnotes.length() > 0)
    {
        outputFile << DucoEngineUtils::PrintToRtf(footnotes) << KRtfLineEnd << std::endl;
    }

    return outputFile;
}

std::wostream& 
Peal::Print(std::wostream& stream, const RingingDatabase& database) const
{
    stream << id << ", " << AssociationName(database) << endl;
    const Tower* const tower = database.TowersDatabase().FindTower(towerId);
    if (tower != NULL)
    {
        tower->Print(stream, database);
        stream << endl;
    }
    else
    {
        stream << "Error: cannot find Tower " << towerId << endl;
    }

    performanceDate.PrintDate(stream);
    stream << " in ";
    performanceTime.PrintTime(stream);

    if (tower != NULL)
    {
        stream << " ";
        tower->PrintTenorDescription(stream, ringId);
        stream << endl;
    }

    stream << noOfChanges << " ";
    const Method* const method = database.MethodsDatabase().FindMethod(methodId);
    if (method != NULL)
    {
        method->Print(stream, database);
        stream << endl;
        if (noOfMultiMethods > 0)
        {
            stream << "In " << noOfMultiMethods << "Methods: ";
        }
        if (ContainsMultiMethods())
        {
            stream << multiMethods;
        }
        if (noOfMultiMethods > 0 || ContainsMultiMethods())
            stream << endl;
    }
    else
    {
        stream << "Error: Cannot find method " << methodId << endl;
    }
    if (composer.length() > 0)
        stream << "Composed by " << composer << endl;

    std::map<unsigned int, PealRingerData>::const_iterator it = ringerIds.begin();
    while (it != ringerIds.end())
    {
        ObjectId ringerId = it->second.RingerId();
        stream << it->first << ". ";

        if (it->first < 10)
        {
            stream << " ";
        }

        const Duco::Ringer* ringer = database.RingersDatabase().FindRinger(ringerId);
        if (ringer != NULL)
        {
            ringer->Print(stream, performanceDate, database);
            if (ContainsConductor(ringerId))
                stream << " (C)";
        }
        std::map<unsigned int, PealRingerData>::const_iterator strapperIt = strapperIds.find(it->first);
        if (strapperIt != strapperIds.end())
        {
            const Duco::Ringer* strapper = database.RingersDatabase().FindRinger(strapperIt->second.RingerId());
            if (strapper != NULL)
            {
                stream << " & ";
                strapper->Print(stream, performanceDate, database);
                if (ContainsConductor(strapperIt->second.RingerId()))
                    stream << " (C)";
            }
        }
        stream << endl;
        ++it;
    }
    if (conductedType == ESilentAndNonConducted)
    {
        stream << "Silent and Non Conducted" << endl;
    }
    else if (conductedType == EJointlyConducted)
    {
        stream << "Joint conductors" << endl;
    }

    if (footnotes.length() > 0)
        stream << endl << footnotes << endl;
    return stream;
}

DatabaseWriter& 
Peal::Externalise(DatabaseWriter& writer) const
{
    writer.WriteId(id);
    writer.WriteId(associationId);
    writer.WriteId(pictureId);
    writer.WriteId(towerId);
    writer.WriteId(ringId);
    writer.WriteId(methodId);
    writer.WriteString(multiMethods);
    writer.WriteInt(noOfMultiMethods);
    writer.WriteId(compositionId);
    writer.WriteId(seriesId);
    writer.WriteString(composer);
    writer.WriteString(footnotes);
    writer.WriteTime(performanceTime);
    writer.WriteDate(performanceDate);
    writer.WriteString(notes);
    writer.WriteBool(handBell);
    writer.WriteBool(wrongStage);
    writer.WriteInt(noOfChanges);

    writer.WriteInt(ringerIds.size());
{
    for (auto const& it : ringerIds)
    {
        writer.WriteInt(it.first);
        writer.WriteId(it.second.RingerId());
        writer.WriteBool(it.second.FullyPaid());
        writer.WriteInt(it.second.PealFee());
    }
}
    writer.WriteInt(strapperIds.size());
    for (auto const& strapperIt : strapperIds)
    {
        writer.WriteInt(strapperIt.first);
        writer.WriteId(strapperIt.second.RingerId());
        writer.WriteBool(strapperIt.second.FullyPaid());
        writer.WriteInt(strapperIt.second.PealFee());
    }

    writer.WriteInt(conductedType);
    writer.WriteInt(conductorIds.size());
    for (auto const& it2 : conductorIds)
    {
        writer.WriteId(it2);
    }
    writer.WriteString(bellBoardId);
    writer.WriteInt(dayOrder);
    writer.WriteString(ringingWorldReference);
    writer.WriteBool(withdrawn);
    writer.WriteBool(doubleHanded);
    writer.WriteBool(notable);
    writer.WriteBool(containsSimulatedSound);
    writer.WriteString(enteredBy);

    return writer;
}

DatabaseReader& 
Peal::Internalise(DatabaseReader& reader, unsigned int databaseVersion)
{
    id = reader.ReadUInt();
    if (databaseVersion >= 36)
    {
        associationId = reader.ReadUInt();
    }
    else
    {
        reader.ReadString(oldAssociationAsString);
    }
    if (databaseVersion >= 29)
    {
        pictureId = reader.ReadUInt();
    }
    towerId = reader.ReadUInt();
    ringId = reader.ReadUInt();
    methodId = reader.ReadUInt();
    reader.ReadString(multiMethods);
    noOfMultiMethods = reader.ReadUInt();
    if (databaseVersion >= 21)
    {
        compositionId = reader.ReadUInt();
        seriesId = reader.ReadUInt();
    }
    reader.ReadString(composer);
    reader.ReadString(footnotes);
    if (databaseVersion == 1)
    {
        time_t date = reader.ReadTime();
        time_t time = reader.ReadTime();
        //dateTime.SetDateAndTime(date, time);
        // Risk it - noone in the field will have v1
    }
    else if (databaseVersion >= 2)
    {
        reader.ReadTime(performanceTime);
        reader.ReadDate(performanceDate);
        if (databaseVersion > 3)
        {
            reader.ReadString(notes);
        }
    }
    handBell = reader.ReadBool();
    if (databaseVersion >= 28)
    {
        wrongStage = reader.ReadBool();
    }
    noOfChanges = reader.ReadUInt();

    const unsigned int noOfRingers (reader.ReadUInt());
    for (unsigned int count(noOfRingers); count > 0; --count)
    {
        unsigned int bellNumber (reader.ReadUInt());
        ObjectId ringerId;
        reader.ReadId(ringerId);
        bool fullyPaid (false);
        unsigned int paidAmountInPence (0);

        if (databaseVersion >= 26)
        {
            fullyPaid = reader.ReadBool(); // extra - could be used to indicate fully paid
            paidAmountInPence = reader.ReadUInt();
        }
        if ( (ringerId != -1) && ((bellNumber == KOldStrapperId) || (bellNumber >= 1 && bellNumber <= noOfRingers)) )
        { // This prevents strange bell no when imported from winRk
            PealRingerData newRinger(ringerId, paidAmountInPence, fullyPaid);
            std::pair<unsigned int, PealRingerData> newObject(bellNumber, newRinger);
            if (bellNumber == KOldStrapperId)
            {
                std::pair<unsigned int, PealRingerData> strapperObject(noOfRingers-1, newRinger);
                strapperIds.insert(strapperObject);
            }
            else
            {
                ringerIds.insert(newObject);
            }
        }
    }
    if (databaseVersion >= 24)
    {
        const unsigned int noOfStrappers (reader.ReadUInt());
        for (unsigned int count(noOfStrappers); count > 0; --count)
        {
            unsigned int bellNumber (reader.ReadInt());
            ObjectId ringerId;
            reader.ReadId(ringerId);
            bool fullyPaid (false);
            unsigned int paidAmountInPence (0);
            if (databaseVersion >= 26)
            {
                fullyPaid = reader.ReadBool();
                paidAmountInPence = reader.ReadInt();
            }
            if ( (ringerId != -1) )
            {
                PealRingerData newRinger(ringerId, paidAmountInPence, fullyPaid);
                std::pair<unsigned int, PealRingerData> newObject(bellNumber, newRinger);
                strapperIds.insert(newObject);
            }
        }
    }
    conductedType = TConductorType(reader.ReadInt());
    unsigned int noOfConductors = reader.ReadUInt();
    while (noOfConductors-- > 0)
    {
        unsigned int conductorId (reader.ReadUInt());
        bool asStrapper(false);
        if (ContainsRinger(conductorId, asStrapper))
            conductorIds.insert(conductorId);
    }
    if (databaseVersion <= 34)
    {
        std::wstring campanophileId;
        reader.ReadString(campanophileId);
    }
    if (databaseVersion >= 23)
        reader.ReadString(bellBoardId);
    else
        bellBoardId.clear();
    
    dayOrder = reader.ReadUInt();
    if (databaseVersion >= 8)
        reader.ReadString(ringingWorldReference);
    if (databaseVersion >= 17 && databaseVersion <= 42)
    {
        std::wstring discardOldPealsCoUkId;
        reader.ReadString(discardOldPealsCoUkId);
    }
    if (databaseVersion >= 12)
    {
        withdrawn = reader.ReadBool();
        if (databaseVersion < 16)
        {
            withdrawn = false;
        }
    }
    if (databaseVersion >= 31)
    {
        doubleHanded = reader.ReadBool();
    }
    if (databaseVersion >= 48)
    {
        notable = reader.ReadBool();
    }
    if (databaseVersion >= 52)
    {
        containsSimulatedSound = reader.ReadBool();
        reader.ReadString(enteredBy);
    }

    return reader;
}

bool
Peal::operator==(const RingingObject& other) const
{
    if (other.ObjectType() != ObjectType())
        return false;

    const Duco::Peal& otherPeal = static_cast<const Duco::Peal&>(other);

    if (id == otherPeal.id && 
        associationId == otherPeal.associationId &&
        pictureId == otherPeal.pictureId &&
        towerId == otherPeal.towerId &&
        ringId == otherPeal.ringId &&
        methodId == otherPeal.methodId &&
        multiMethods == otherPeal.multiMethods &&
        noOfMultiMethods == otherPeal.noOfMultiMethods &&
        compositionId == otherPeal.compositionId &&
        seriesId == otherPeal.seriesId &&
        composer == otherPeal.composer &&
        footnotes == otherPeal.footnotes &&
        performanceDate == otherPeal.performanceDate &&
        performanceTime == otherPeal.performanceTime &&
        handBell == otherPeal.handBell &&
        wrongStage == otherPeal.wrongStage &&
        noOfChanges == otherPeal.noOfChanges &&
        ringerIds == otherPeal.ringerIds &&
        strapperIds == otherPeal.strapperIds &&
        conductedType == otherPeal.conductedType &&
        conductorIds == otherPeal.conductorIds &&
        bellBoardId == otherPeal.bellBoardId &&
        ringingWorldReference == otherPeal.ringingWorldReference &&
        dayOrder == otherPeal.dayOrder &&
        withdrawn == otherPeal.withdrawn &&
        doubleHanded == otherPeal.doubleHanded &&
        containsSimulatedSound == otherPeal.containsSimulatedSound &&
        notable == otherPeal.notable)
    {
        return true;
    }

    return false;
}

bool
Peal::operator!=(const RingingObject& other) const
{
    if (other.ObjectType() != ObjectType())
        return true;

    const Duco::Peal& otherPeal = static_cast<const Duco::Peal&>(other);

    if (id != otherPeal.id)
        return true;
    if (pictureId != otherPeal.pictureId)
        return true;
    else if (associationId != otherPeal.associationId)
        return true;
    else if (towerId != otherPeal.towerId)
        return true;
    else if (ringId != otherPeal.ringId)
        return true;
    else if (methodId != otherPeal.methodId)
        return true;
    else if (multiMethods != otherPeal.multiMethods)
        return true;
    else if (noOfMultiMethods != otherPeal.noOfMultiMethods)
        return true;
    else if (compositionId != otherPeal.compositionId)
        return true;
    else if (seriesId != otherPeal.seriesId) 
        return true;
    else if (composer != otherPeal.composer)
        return true;
    else if (footnotes != otherPeal.footnotes)
        return true;
    else if (performanceDate != otherPeal.performanceDate)
        return true;
    else if (performanceTime != otherPeal.performanceTime)
        return true;
    else if (handBell != otherPeal.handBell)
        return true;
    else if (wrongStage != otherPeal.wrongStage)
        return true;
    else if (noOfChanges != otherPeal.noOfChanges)
        return true;
    else if (ringerIds != otherPeal.ringerIds)
        return true;
    else if (strapperIds != otherPeal.strapperIds)
        return true;
    else if (conductedType != otherPeal.conductedType)
        return true;
    else if (conductorIds != otherPeal.conductorIds)
        return true;
    else if (dayOrder != otherPeal.dayOrder)
        return true;
    else if (withdrawn != otherPeal.withdrawn)
        return true;
    else if (doubleHanded != otherPeal.doubleHanded)
        return true;
    else if (notable != otherPeal.notable)
        return true;
    else if (containsSimulatedSound != otherPeal.containsSimulatedSound)
        return true;

    return false;
}

Peal&
Peal::operator=(const Duco::Peal& otherPeal)
{
    id = otherPeal.id;
    associationId = otherPeal.associationId;
    pictureId = otherPeal.pictureId;
    towerId = otherPeal.towerId;
    ringId = otherPeal.ringId;
    methodId = otherPeal.methodId;
    multiMethods = otherPeal.multiMethods;
    noOfMultiMethods = otherPeal.noOfMultiMethods;
    compositionId = otherPeal.compositionId;
    seriesId = otherPeal.seriesId;
    composer = otherPeal.composer;
    footnotes = otherPeal.footnotes;
    performanceTime = otherPeal.performanceTime;
    performanceDate = otherPeal.performanceDate;
    handBell = otherPeal.handBell;
    wrongStage = otherPeal.wrongStage;
    noOfChanges = otherPeal.noOfChanges;
    ringerIds = otherPeal.ringerIds;
    strapperIds = otherPeal.strapperIds;
    conductedType = otherPeal.conductedType;
    conductorIds = otherPeal.conductorIds;
    bellBoardId = otherPeal.bellBoardId;
    ringingWorldReference = otherPeal.ringingWorldReference;
    dayOrder = otherPeal.dayOrder;
    notes = otherPeal.notes;
    errorCode = otherPeal.errorCode;
    withdrawn = otherPeal.withdrawn;
    doubleHanded = otherPeal.doubleHanded;
    notable = otherPeal.notable;
    containsSimulatedSound = otherPeal.containsSimulatedSound;
    enteredBy = otherPeal.enteredBy;

    return *this;
}

bool 
Peal::operator<(const Peal& other) const
{
    if (performanceDate == other.performanceDate)
        return dayOrder < other.dayOrder;

    return performanceDate.operator<(other.performanceDate);
}

bool 
Peal::operator>(const Peal& other) const
{
    if (performanceDate == other.performanceDate)
        return dayOrder > other.dayOrder;

    return performanceDate.operator>(other.performanceDate);
}

void
Peal::RingerIds(std::set<ObjectId>& allIds) const
{
    allIds.clear();
    for (auto const& [key, value] : ringerIds)
    {
        allIds.insert(value.RingerId());
    }
    for (auto const& [key, value] : strapperIds)
    {
        allIds.insert(value.RingerId());
    }
}

bool
Peal::CompareRingerIds(const Duco::Peal& other, const Duco::RingerDatabase& allRingers, const Duco::DatabaseSettings& settings) const
{
    float numberMatch = 0;
    float numberDontMatch = 0;
    {
        for (auto const& [key, value] : ringerIds)
        {
            std::map<unsigned int, PealRingerData>::const_iterator otherIt = other.ringerIds.find(key);
            if (otherIt == other.ringerIds.end())
            {
                // if this bell doesnt exist - ignore it - dont even count it
            }
            else if (value.RingerId() != otherIt->second.RingerId())
            {
                std::wstring thisName = allRingers.RingerFullName(value.RingerId(), performanceDate, settings);
                std::wstring otherName = allRingers.RingerFullName(otherIt->second.RingerId(), performanceDate, settings);
                if (thisName.length() > 0 && otherName.length() > 0 && thisName.compare(otherName) == 0)
                {
                    ++numberMatch;
                }
                else
                {
                    ++numberDontMatch;
                    if (numberDontMatch >= 5)
                    {
                        return false;
                    }
                }
            }
            else
            {
                ++numberMatch;
            }
        }
    }
    {
        for (auto const& [key, value] : strapperIds)
        {
            std::map<unsigned int, PealRingerData>::const_iterator otherIt = other.strapperIds.find(key);

            if (otherIt == other.ringerIds.end())
            {
                // if this bell doesnt exist - ignore it - dont even count it
            }
            else if (value.RingerId() != otherIt->second.RingerId())
                ++numberDontMatch;
            else
                ++numberMatch;
        }
    }
    float total = numberMatch + numberDontMatch;
    if (numberMatch == 0)
        return false;

    if (total < 8)
        return (numberMatch / total) >= 0.66; // 66% of the ringers must be the same. (4 in 6 e.g. 2/3rds)

    return (numberMatch / total) >= 0.75; // 75% of the ringers must be the same.
}

bool 
Peal::RingerId(unsigned int bellNo, bool strapper, ObjectId& ringerId) const
{
    ringerId.ClearId();
    if (strapper)
    {
        std::map<unsigned int, PealRingerData>::const_iterator strapperIt = strapperIds.find(bellNo);
        if (strapperIt != strapperIds.end())
        {
            ringerId = strapperIt->second.RingerId();
        }
    }
    else
    {
        std::map<unsigned int, PealRingerData>::const_iterator it = ringerIds.find(bellNo);
        if (it != ringerIds.end())
        {
            ringerId = it->second.RingerId();
        }
    }
    return ringerId.ValidId();
}

bool
Peal::CheckForDoubleHandedRingers() const
{
    std::set<Duco::ObjectId> ringerCount;

    for (auto const& [key, value] : ringerIds)
    {
        if (!ringerCount.insert(value.RingerId()).second)
        {
            return true;
        }
    }
    return false;
}

void 
Peal::AddMultiMethods(unsigned int noOfMethods, const std::wstring& methods)
{
    multiMethods = methods;
    noOfMultiMethods = noOfMethods;
}

bool
Peal::Valid(const RingingDatabase& database, bool warningFatal, bool clearBeforeStart) const
{
    return ValidPeal(database.RingersDatabase(), database.MethodsDatabase(), database.TowersDatabase(), database.CompositionsDatabase(), database.MethodSeriesDatabase(), database.AssociationsDatabase(), database.Settings(), warningFatal, clearBeforeStart);
}

bool 
Peal::ValidPeal(const Duco::RingerDatabase& ringersDatabase, const MethodDatabase& methodsDatabase, const TowerDatabase& towersDatabase,
                const CompositionDatabase& compostionsDb, const MethodSeriesDatabase& methodSeriesDb, const AssociationDatabase& associationDb,
                const Duco::DatabaseSettings& settings, bool warningFatal, bool clearBeforeStart) const
{
    bool valid (ValidPealDetails(methodsDatabase, towersDatabase, compostionsDb, methodSeriesDb, associationDb, settings, warningFatal, clearBeforeStart));
    valid &= ValidPealConductors(warningFatal, false);
    valid &= ValidPealRingers(ringersDatabase, methodsDatabase, towersDatabase, settings, warningFatal, false);
    return valid;
}

bool
Peal::ValidPealDetails(const RingingDatabase& database, bool warningFatal, bool clearBeforeStart) const
{
    return ValidPealDetails(database.MethodsDatabase(), database.TowersDatabase(), database.CompositionsDatabase(), database.MethodSeriesDatabase(), database.AssociationsDatabase(), database.Settings(), warningFatal, clearBeforeStart);
}

bool
Peal::ValidPealRingers(const RingingDatabase& database, bool warningFatal, bool clearBeforeStart) const
{
    return ValidPealRingers(database.RingersDatabase(), database.MethodsDatabase(), database.TowersDatabase(), database.Settings(), warningFatal, clearBeforeStart);
}

bool 
Peal::ValidPealDetails(const MethodDatabase& methodDb, const TowerDatabase& towerDb, const CompositionDatabase& compDb, const MethodSeriesDatabase& methodSeriesDb, const AssociationDatabase& associationDb, const Duco::DatabaseSettings& settings, bool warningFatal, bool clearBeforeStart) const
{
// These fields are OK blank
//    composer
//    multimethods
//    noOfMultiMethods
//    association
    if (clearBeforeStart)
    {
        ClearErrors();
    }

    bool valid (true);
    if (!associationId.ValidId() && !settings.ValidationCheckDisabled(EPealWarning_AssociationMissing, TObjectType::EPeal))
    {
        if (warningFatal)
            valid = false;
        errorCode.set(EPealWarning_AssociationMissing, 1);
    }
    else if (warningFatal && settings.PealDatabase() && !settings.ValidationCheckDisabled(EPealWarning_AssociationBlank, TObjectType::EPeal))
    {
        if (associationDb.FindAssociationName(associationId).length() == 0)
        {
            errorCode.set(EPealWarning_AssociationBlank, 1);
        }
    }

    if (!towerId.ValidId())
    {
        valid = false;
        errorCode.set(EPeal_TowerMissing, 1);
    }
    if (!ringId.ValidId())
    {
        valid = false;
        errorCode.set(EPeal_RingMissing, 1);
    }
    bool splicedMethod (false);
    if (!methodId.ValidId())
    {
        valid = false;
        errorCode.set(EPeal_MethodMissing, 1);
    }
    else
    {
        const Method* const theMethod = methodDb.FindMethod(methodId, true);
        if (theMethod == NULL)
        {
            errorCode.set(EPeal_MethodMissing, 1);
            valid = false;
        }
        else
        {
            if (theMethod->Spliced() && !ContainsMultiMethods())
            {
                errorCode.set(EPeal_MultiMethodsMissing, 1);
                valid = false;
            }
            if (theMethod->Spliced())
            {
                splicedMethod = true;
                if (!seriesId.ValidId() && !settings.ValidationCheckDisabled(EPealWarning_SeriesMissing, TObjectType::EPeal))
                {
                    errorCode.set(EPealWarning_SeriesMissing, 1);
                    if (warningFatal)
                        valid = false;
                }
            }
            else
            {
                if (ContainsMultiMethods() && theMethod->Order() > 6)
                {
                    errorCode.set(EPealWarning_MultiMethodsNotRequired, 1);
                    if (warningFatal)
                        valid = false;
                }
                if (seriesId.ValidId())
                {
                    errorCode.set(EPealWarning_SeriesNotRequired, 1);
                    if (warningFatal)
                        valid = false;
                }
            }
        }
    }
    if (settings.PealDatabase())
    {
        if (noOfChanges < 5000)
        {
            valid = false;
            errorCode.set(EPeal_TooFewChanges, 1);
        }
    }
    else
    {
        if (noOfChanges < 1250)
        {
            valid = false;
            errorCode.set(EPeal_TooFewChanges, 1);
        }
        else if (noOfChanges >= 5000)
        {
            valid = false;
            errorCode.set(EPeal_TooManyChanges, 1);
        }
    }
    if (!performanceDate.Valid())
    {
        valid = false;
        errorCode.set(EPeal_DateInvalid, 1);
    }
    if (!performanceTime.Valid() && !settings.ValidationCheckDisabled(EPeal_TimeInvalid, TObjectType::EPeal))
    {
        if (settings.PealDatabase())
        {
            valid = false;
            errorCode.set(EPeal_TimeInvalid, 1);
        }
        else
        {
            if (warningFatal) // If a quarter database and warnings disabled, this doesn't matter
                valid = false;
            errorCode.set(EPeal_TimeInvalid, 1);
        }
    }
    if (compositionId.ValidId())
    {
        const Composition* const theComposition = compDb.FindComposition(compositionId);
        if (theComposition == NULL)
        {
            errorCode.set(EPeal_CompositionInvalid, 1);
            valid = false;
        }
        else if (!splicedMethod && methodId.ValidId())
        {
            if (theComposition->MethodId() != methodId)
            {
                errorCode.set(EPealWarning_CompositionHasDifferentMethod, 1);
                if (warningFatal)
                    valid = false;
            }
        }
    }
    if (seriesId.ValidId())
    {
        if (splicedMethod)
        {
            const MethodSeries* const theSeries = methodSeriesDb.FindMethodSeries(seriesId);
            if (theSeries == NULL)
            {
                errorCode.set(EPeal_SeriesInvalid, 1);
                valid = false;
            }
            else
            {
                bool dualOrderMethod;
                if (theSeries->NoOfBells() != NoOfBellsInMethod(methodDb, dualOrderMethod) && !dualOrderMethod)
                {
                    errorCode.set(EPeal_SeriesHasDifferentNoOfBells, 1);
                    valid = false;
                }
            }
        }
    }
    return valid;
}

bool
Peal::ValidPealConductors(bool warningFatal, bool clearBeforeStart) const
{
    if (clearBeforeStart)
    {
        ClearErrors();
    }
    size_t noOfConductors (conductorIds.size());
    bool valid (true);
    
    if (conductedType == ESingleConductor)
    {
        if (noOfConductors != 1)
        {
            valid = false;
            if (noOfConductors == 0)
                errorCode.set(EPeal_TooFewConductors, 1);
            else
                errorCode.set(EPeal_TooManyConductors, 1);
        }
    }
    else if (conductedType == ESilentAndNonConducted)
    {
        if (noOfConductors != 0)
        {
            valid = false;
            errorCode.set(EPeal_TooManyConductors, 1);
        }
    }
    else if (conductedType == EJointlyConducted)
    {
        if (noOfConductors <= 1)
        {
            valid = false;
            errorCode.set(EPeal_TooFewConductors, 1);
        }
    }

    if (valid && conductedType != ESilentAndNonConducted)
    {
        std::set<ObjectId>::const_iterator it = conductorIds.begin();
        while (it != conductorIds.end() && valid)
        {
            bool asStrapper(false);
            if (!ContainsRinger(*it, asStrapper))
            {
                valid = false;
                errorCode.set(EPeal_ConductorNotRinging, 1);
            }

            ++it;
        }
    }

    return valid;
}

bool 
Peal::ValidPealRingers(const Duco::RingerDatabase& ringersDatabase, const MethodDatabase& methodDatabase, const TowerDatabase& towerDatabase, const DatabaseSettings& settings, bool warningFatal, bool clearBeforeStart) const
{
    if (clearBeforeStart)
    {
        ClearErrors();
    }
    bool valid (true);
    const unsigned int noOfRingers (Peal::NoOfRingers(false));

    const Method* const theMethod = methodDatabase.FindMethod(methodId);
    if (theMethod != NULL)
    {
        if (wrongStage)
        {
            if (noOfRingers < theMethod->Order())
            {
                // number of ringers must be at least equal to the number 
                // of bells in the method. (odd bell methods are a pain!)
                errorCode.set(EPeal_NoOfRingersIncorrect, 1);
                valid = false;
            }
        }
        else if (doubleHanded)
        {
            if (noOfRingers > theMethod->Order())
            {
                // If someone (or more) is double handling, there shouldbe more bells that ringers.
                errorCode.set(EPeal_NoOfRingersIncorrect, 1);
                valid = false;
            }
        }
        else if (!handBell)
        {
            if (noOfRingers != theMethod->Order() && noOfRingers != 1+theMethod->Order())
            {
                // number of ringers must be at least equal to the number 
                // of bells in the method. (odd bell methods are a pain!)
                errorCode.set(EPeal_NoOfRingersIncorrect, 1);
                valid = false;
            }
        }
    }
    else
    {
        errorCode.set(EPeal_MethodMissing, 1);
        valid = false;
    }
    // Check for strappers.
    unsigned int lastStrapperBell(0);
    std::map<unsigned int, PealRingerData>::const_iterator strapIt = strapperIds.begin();
    while (strapIt != strapperIds.end() && !errorCode.test(EPeal_InvalidStrapper))
    {
        lastStrapperBell = max<unsigned int>(lastStrapperBell, strapIt->first);
        if (!strapIt->second.RingerId().ValidId())
        {
            valid = false;
            errorCode.set(EPeal_InvalidStrapper, 1);
        }
        else if (strapIt->first < 1 || strapIt->first > noOfRingers)
        {
            valid = false;
            errorCode.set(EPeal_InvalidStrapper, 1);
        }
        ++strapIt;
    }
    if (lastStrapperBell > 0 && lastStrapperBell != noOfRingers)
    {
        valid = false;
        errorCode.set(EPeal_StrapperMissing, 1);
    }

    const Tower* const theTower = towerDatabase.FindTower(towerId);
    if (theTower == NULL)
    {
        valid = false;
        errorCode.set(EPeal_TowerMissing, 1);
    }
    else
    {
        const Ring* const theRing = theTower->FindRing(ringId);
        if (theRing == NULL)
        {
            valid = false;
            errorCode.set(EPeal_RingMissing, 1);
        }
        else
        {
            if (theMethod != NULL)
            {
                if (!wrongStage && (theMethod->Order() != theRing->NoOfBells()) && ((1+theMethod->Order()) != theRing->NoOfBells()) )
                {
                    errorCode.set(EPeal_NoOfRingersAndBellsMismatch);
                    valid = false;
                }
            }
            // number of ringers must be at equal to the number of
            // bells in the towers ring.
            if (!doubleHanded && !handBell && noOfRingers != theRing->NoOfBells())
            {
                // number of bells rung must be at equal to the number 
                // of bells in the tower.
                valid = false;
                errorCode.set(EPeal_NoOfRingersIncorrect, 1);
            }
            else if (handBell && (2*noOfRingers) != theRing->NoOfBells())
            {
                // number of bells rung must be at equal to the number 
                // of bells in the tower.
                valid = false;
                errorCode.set(EPeal_NoOfRingersIncorrect, 1);
            }
        }
        if (theTower->Handbell() != this->Handbell())
        {
            valid = false;
            errorCode.set(EPeal_TowerAndHandbellMismatch, 1);
        }
    }

    // Check for containing ringers which who are linked
    if (!doubleHanded)
    {
        bool strapper;
        bool errorInLinkedRingersFound = false;
        for (const auto& ringerGroup : ringersDatabase.linkedRingerIds)
        {
            //for (const auto& it1 : ringerGroup)
            std::set<Duco::ObjectId>::const_iterator it1 = ringerGroup.begin();
            while (it1 != ringerGroup.end() && !errorInLinkedRingersFound)
            {
                //for (const auto& it2 = it1 + 1; it2 != ringerGroup.end(); it2 + 1)
                std::set<Duco::ObjectId>::const_iterator it2 = ringerGroup.begin();
                while (it2 != ringerGroup.end() && !errorInLinkedRingersFound)
                {
                    if (it1 != it2 && ContainsRinger(*it1, strapper) && ContainsRinger(*it2, strapper))
                    {
                        valid = false;
                        errorInLinkedRingersFound = true;
                        errorCode.set(EPeal_LinkedRingerTwice, 1);
                    }
                    ++it2;
                }
                ++it1;
            }
        }
    }

    if (warningFatal)
    {
        bool asStrapper(false);
        if (!ContainsDefaultRinger(settings, asStrapper))
            valid = false;
    }
    if (warningFatal || errorCode.test(EPeal_NoOfRingersIncorrect))
    {
        std::map<ObjectId, unsigned int> ringers;
        if (DuplicateRingers(ringers))
            valid = false;
    }

    return valid;
}

bool 
Peal::BellRung(const ObjectId& ringerId, std::set<unsigned int>& bellsRung, bool& strapper) const
{
    bellsRung.clear();
    strapper = false;
    if (!ringerId.ValidId())
        return false;

    for (auto const& [key, value] : ringerIds)
    {
        if (value.RingerId() == ringerId)
        {
            bellsRung.insert(key);
        }
    }
    for (auto const& [key, value] : strapperIds)
    {
        if (value.RingerId() == ringerId)
        {
            bellsRung.insert(key);
            strapper = true;
        }
    }
    return bellsRung.size() > 0;
}

bool
Peal::HandBellsRung(const ObjectId& ringerId, std::wstring& bellNumbers) const
{
    bool asStrapper(false);
    std::set<unsigned int> bellsRung;
    if (BellRung(ringerId, bellsRung, asStrapper))
    {
        unsigned int bellRung = *bellsRung.begin();
        bellRung *= 2;
        const int bufferSize (20);
        wchar_t buffer [bufferSize];
        swprintf(buffer, 20, L"%d-%d", bellRung-1, bellRung);
        bellNumbers = buffer;

        return true;
    }
    return false;
}

bool 
Peal::ContainsRinger(const ObjectId& ringerId, bool& strapper) const
{
    strapper = false;
    if (!ringerId.ValidId())
        return false;

    for (auto const& [key, value] : ringerIds)
    {
        if (value.RingerId() == ringerId)
            return true;
    }
    for (auto const& [key, value] : strapperIds)
    {
        if (value.RingerId() == ringerId)
        {
            strapper = true;
            return true;
        }
    }

    return false;
}

bool
Peal::ContainsAnyRinger(const std::set<ObjectId>& ringerIds) const
{
    bool strapper;
    for (auto const& ringerId : ringerIds)
    {
        if (ContainsRinger(ringerId, strapper))
            return true;
    }
    return false;
}

bool 
Peal::ContainsDefaultRinger(const DatabaseSettings& settings, bool& asStrapper) const
{
    if (settings.DefaultRingerSet() && !ContainsRinger(settings.DefaultRinger(), asStrapper))
    {
        errorCode.set(EPeal_DefaultRingerMissing, 1);
        return false;
    }
    errorCode.reset(EPeal_DefaultRingerMissing);
    return true;
}

bool 
Peal::DuplicateRingers(std::map<ObjectId, unsigned int>& duplicateRingers) const
{
    duplicateRingers.clear();

    for (auto const& [key, value] : ringerIds)
    {
        std::pair<ObjectId, unsigned int> newObject(value.RingerId(), 1);
        pair< std::map<ObjectId, unsigned int>::iterator,bool> insertedit = duplicateRingers.insert(newObject);
        if (!insertedit.second && value.RingerId().ValidId())
        {
            ++(insertedit.first->second);
        }
    }
    for (auto const& [key, value] : strapperIds)
    {
        std::pair<ObjectId, unsigned int> newObject(value.RingerId(), 1);
        pair< std::map<ObjectId, unsigned int>::iterator,bool> insertedit = duplicateRingers.insert(newObject);
        if (!insertedit.second && value.RingerId().ValidId())
        {
            ++(insertedit.first->second);
        }
    }

    // remove those with only one peal
    std::map<ObjectId, unsigned int>::const_iterator ringerIt = duplicateRingers.begin();
    while (ringerIt != duplicateRingers.end())
    {
        if (ringerIt->second <= 1)
        {
            duplicateRingers.erase(ringerIt->first);
            ringerIt = duplicateRingers.begin();
        }
        else
        {
            ++ringerIt;
        }
    }

    if (DoubleHanded())
    {
        if (duplicateRingers.size() == 0)
        {
            errorCode.set(EPeal_MissingDuplicateRingers, 1);
            return true;
        }
    }
    else if (duplicateRingers.size() > 0)
    {
        errorCode.set(EPeal_DuplicateRingers);
        return true;
    }
    return false;
}

void 
Peal::SetRingerId(const ObjectId& newRingerId, unsigned int newBellNumber, bool asStrapper)
{
    if (asStrapper)
    {
        std::map<unsigned int, PealRingerData>::iterator strapperIt = strapperIds.find(newBellNumber);
        if (strapperIt != strapperIds.end())
        {
            ObjectId oldRingerId = strapperIt->second.RingerId();
            strapperIt->second.SetRingerId(newRingerId);

            if (ContainsConductor(oldRingerId))
            {
                RemoveConductor(oldRingerId);
                SetConductorId(newRingerId);
            }
        }
        else
        {
            PealRingerData newRinger(newRingerId, 0, false);
            std::pair<unsigned int, PealRingerData> newRingerObject(newBellNumber, newRinger);
            strapperIds.insert(newRingerObject);
        }
    }
    else
    {
        std::map<unsigned int, PealRingerData>::iterator it = ringerIds.find(newBellNumber);
        if (it != ringerIds.end())
        {
            ObjectId oldRingerId = it->second.RingerId();
            it->second.SetRingerId(newRingerId);

            if (ContainsConductor(oldRingerId))
            {
                RemoveConductor(oldRingerId);
                SetConductorId(newRingerId);
            }
        }
        else
        {
            PealRingerData newRinger(newRingerId, 0, false);
            std::pair<unsigned int, PealRingerData> newRingerObject(newBellNumber, newRinger);
            ringerIds.insert(newRingerObject);
        }
    }
}

void
Peal::RemoveRinger(unsigned int bellNumber, bool asStrapper)
{
    if (asStrapper)
    {
        std::map<unsigned int, PealRingerData>::iterator strapperIt = strapperIds.find(bellNumber);
        if (strapperIt != strapperIds.end())
        {
            strapperIds.erase(strapperIt);
        }
    }
    else
    {
        std::map<unsigned int, PealRingerData>::iterator it = ringerIds.find(bellNumber);
        if (it != ringerIds.end())
        {
            ringerIds.erase(it);
        }
    }
}

bool
Peal::RemoveConductor(const Duco::ObjectId& conductorId)
{
    std::set<ObjectId>::iterator it = conductorIds.find(conductorId);
    if (it != conductorIds.end())
    {
        conductorIds.erase(it);
        return true;
    }
    return false;
}

bool
Peal::ReplaceRingers(const std::map<ObjectId, ObjectId>& newRingerIds)
{
    bool ringerReplaced (false);
    std::set<ObjectId> oldConductors = conductorIds;
    std::set<ObjectId> newConductors;
    {
        for (auto& [key, value] : ringerIds)
        {
            std::map<ObjectId, ObjectId>::const_iterator replacementId = newRingerIds.find(value.RingerId());
            if (replacementId != newRingerIds.end() && value.RingerId() != replacementId->second)
            {
                std::set<ObjectId>::iterator condIt = oldConductors.find(value.RingerId());
                if (condIt != oldConductors.end())
                {
                    oldConductors.erase(condIt);
                    newConductors.insert(replacementId->second);
                }
                ringerReplaced = true;
                value.SetRingerId(replacementId->second);
            }
        }
    }
    
    for (auto& [key, value] : strapperIds)
    {
        std::map<ObjectId, ObjectId>::const_iterator replacementId = newRingerIds.find(value.RingerId());
        if (replacementId != newRingerIds.end() && value.RingerId() != replacementId->second)
        {
            std::set<ObjectId>::iterator condIt = oldConductors.find(value.RingerId());
            if (condIt != oldConductors.end())
            {
                oldConductors.erase(condIt);
                newConductors.insert(replacementId->second);
            }
            ringerReplaced = true;
            value.SetRingerId(replacementId->second);
        }
    }
    if (ringerReplaced)
    {
        if (oldConductors.size() > 0)
        {
            newConductors.insert(oldConductors.begin(), oldConductors.end());
        }
        conductorIds = newConductors;
    }

    return ringerReplaced;
}

bool
Peal::ReplaceRingerId(const ObjectId& oldRingerId, const ObjectId& newRingerId)
{
    bool ringerReplaced (false);
    if (oldRingerId != newRingerId)
    {
        for (auto & [key, value] : ringerIds)
        {
            if (value.RingerId() == oldRingerId)
            {
                value.SetRingerId(newRingerId);
                ringerReplaced = true;
            }
        }
        for (auto & [key, value] : strapperIds)
        {
            if (value.RingerId() == oldRingerId)
            {
                value.SetRingerId(newRingerId);
                ringerReplaced = true;
            }
        }

        if (ringerReplaced)
        {   // check conductors
            std::set<ObjectId>::iterator condIt = conductorIds.find(oldRingerId);
            if (condIt != conductorIds.end())
            {
                conductorIds.erase(condIt);
                conductorIds.insert(newRingerId);
            }
        }
    }

    return ringerReplaced;
}

void 
Peal::SetConductedType(TConductorType type)
{
    conductedType = type;
    if (conductedType == ESilentAndNonConducted)
    {
        conductorIds.clear();
    }
}

void 
Peal::SetConductorIdFromBell(unsigned int newBellId, bool checked, bool strapper)
{
    ObjectId conductorId;
    if (strapper)
    {
        std::map<unsigned int, PealRingerData>::const_iterator it2 = strapperIds.find(newBellId);
        if (it2 != strapperIds.end())
            conductorId = it2->second.RingerId();
    }
    else
    {
        std::map<unsigned int, PealRingerData>::const_iterator it = ringerIds.find(newBellId);
        if (it != ringerIds.end())
            conductorId = it->second.RingerId();
    }
    if (conductedType == ESingleConductor && checked)
        {
        // clear conductors first if single conductor
        conductorIds.clear();
    }
    if (conductorId.ValidId())
    {
        if (checked)
            conductorIds.insert(conductorId);
        else
        {
            conductorIds.erase(conductorId);
        }
    }
}

void 
Peal::SetConductorId(const Duco::ObjectId& newRingerId)
{
    if (conductedType == ESingleConductor)
        conductorIds.clear();

    conductorIds.insert(newRingerId);
}

bool 
Peal::ContainsConductor(const Duco::ObjectId& ringerId) const
{
    std::set<ObjectId>::const_iterator it = conductorIds.begin();
    while (it != conductorIds.end())
    {
        if (*it == ringerId)
            return true;
        ++it;
    }
    return false;
}

bool
Peal::ConductorOnBell(unsigned int bellNo, bool strapper) const
{
    if (strapper)
    {
        std::map<unsigned int, PealRingerData>::const_iterator strapperIt = strapperIds.find(bellNo);
        if (strapperIt == strapperIds.end())
            return false;
        return conductorIds.find(strapperIt->second.RingerId()) != conductorIds.end();
    }
    else
    {
        std::map<unsigned int, PealRingerData>::const_iterator ringerIt = ringerIds.find(bellNo);
        if (ringerIt == ringerIds.end())
            return false;
        return conductorIds.find(ringerIt->second.RingerId()) != conductorIds.end();
    }
}

ObjectId
Peal::FirstConductor() const
{
    ObjectId firstCond;

    std::set<ObjectId>::const_iterator it = conductorIds.begin();
    if (it != conductorIds.end())
    {
        firstCond = *it;
    }
    return firstCond;
}


void
Peal::ShortTitle(std::wstring& title) const
{
    const int bufferSize (20);
    wchar_t buffer [bufferSize];
    swprintf (buffer, 20, L"Peal number: %zd", id.Id());
    title = buffer;
}

std::wstring
Peal::ShortTitleWithDate() const
{
    const int bufferSize(20);
    wchar_t buffer[bufferSize];
    swprintf(buffer, 20, L"peal no: %zd; on ", id.Id());
    std::wstring title = buffer;
    title += performanceDate.Str();
    return title;
}

void
Peal::SummaryTitle(std::wstring& title, const RingingDatabase& database) const
{
    title.clear();

    const int bufferSize (20);
    wchar_t buffer [bufferSize];
    if (id.ValidId())
    {
        swprintf(buffer, 20, L"%zd: %d of ", id.Id(), noOfChanges);
    }
    else
    {
        swprintf(buffer, 20, L"%d of ", noOfChanges);
    }
    title = buffer;

    const Method* const method = database.MethodsDatabase().FindMethod(methodId);
    if (method != NULL)
    {
        if (method->Name().length() > 0)
        {
            title.append(method->Name());
            title.append(L" ");
        }
        if (method->Type().length() > 0)
        {
            title.append(method->Type());
            title.append(L" ");
        }
        title.append(method->OrderName(database.MethodsDatabase()));
    }
    else
    {
        title += L" a new method ";
    }
    title += L" in ";
    title += performanceTime.PrintPealTime(false);
    title += L" at ";

    const Tower* const tower = database.TowersDatabase().FindTower(towerId);
    if (tower != NULL)
    {
        title.append(tower->Name());
        title.append(L", ");
        title.append(tower->City());
    }
    else
    {
        title.append(L"a new tower");
    }
}

std::wstring
Peal::OnThisDayTitle(const Duco::RingingDatabase& database) const
{
    std::wstring title;

    {
        const int bufferSize(20);
        wchar_t buffer[bufferSize];
        swprintf(buffer, 20, L"%zd: On ", id.Id());
        title = buffer;
    }

    title += performanceDate.FullDate();

    {
        const int bufferSize(20);
        wchar_t buffer[bufferSize];
        swprintf(buffer, 20, L", %d changes of ", noOfChanges);
        title += buffer;
    }

    const Method* const method = database.MethodsDatabase().FindMethod(methodId);
    if (method != NULL)
    {
        if (method->Name().length() > 0)
        {
            title.append(method->Name());
            title.append(L" ");
        }
        if (method->Type().length() > 0)
        {
            title.append(method->Type());
            title.append(L" ");
        }
        title.append(method->OrderName(database.MethodsDatabase()));
    }
    else
    {
        title += L" a new method ";
    }
    title += L" at ";

    const Tower* const tower = database.TowersDatabase().FindTower(towerId);
    if (tower != NULL)
    {
        title.append(tower->City());
        title.append(L" (");
        title.append(tower->Name());
        title.append(L")");
    }
    else
    {
        title.append(L"a new tower");
    }
    return title;
}

void
Peal::Title(std::wstring& title, const RingingDatabase& database) const
{
    Title(title, database.TowersDatabase(), database.MethodsDatabase());
}

void 
Peal::Title(std::wstring& title, const TowerDatabase& towerDatabase, const MethodDatabase& methodDatabase) const
{
    wstringstream str;
    str << id;
    title = str.str();
    title.append(L": ");
    const Tower* const tower = towerDatabase.FindTower(towerId);
    if (tower != NULL)
    {
        title.append(tower->Name());
        title.append(L", ");
        title.append(tower->City());
        title.append(L", ");
        title.append(tower->County());
        title.append(L". ");
    }

    const Method* const method = methodDatabase.FindMethod(methodId);
    if (method != NULL)
    {
        if (method->Name().length() > 0)
        {
            title.append(method->Name());
            title.append(L" ");
        }
        if (method->Type().length() > 0)
        {
            title.append(method->Type());
            title.append(L" ");
        }
        title.append(method->OrderName(methodDatabase));
        title.append(L".");
    }
}

std::wstring
Peal::Details(const RingingDatabase& database) const
{
    wostringstream returnVal;
    Print(returnVal, database);

    return returnVal.str();
}

void
Peal::AddStrapper(unsigned int bellNo, bool remove)
{
    std::map<unsigned int, PealRingerData>::iterator it = strapperIds.find(bellNo);
    if (remove)
    {
        if (it != strapperIds.end())
        {
            it->second.SetRingerId(KNoId);
        }
    }
    else if (it == strapperIds.end())
    {
        SetRingerId(KNoId, bellNo, true);
    }
}

bool
Peal::StrapperOnBell(unsigned int bellNo) const
{
    return strapperIds.find(bellNo) != strapperIds.end();
}

bool
Peal::ShowStrapperOption(const Duco::RingingDatabase& ringingDb, unsigned int bellNo) const
{
    unsigned int noOfBellsRung = NoOfBellsRung(ringingDb);
    if (bellNo == noOfBellsRung || (bellNo+1) == noOfBellsRung)
    {
        return true;
    }
    return false;
}

bool
Peal::AnyStrappers() const
{
    return !strapperIds.empty();
}

unsigned int 
Peal::NoOfBellsInMethod(const Duco::MethodDatabase& methodDatabase, bool& dualOrderMethod) const
{
    unsigned int noOfBellsByMethod = 0;
    dualOrderMethod = false;
    if (methodId.ValidId())
    {
        const Method* const theMethod = static_cast<const Method* const>(methodDatabase.FindObject(methodId, false));
        if (theMethod != NULL)
        {
            noOfBellsByMethod = theMethod->Order();
            dualOrderMethod = theMethod->DualOrder();
        }
    }
    return noOfBellsByMethod;
}

unsigned int 
Peal::NoOfBellsInTower(const Duco::TowerDatabase& towerDatabase) const
{
    unsigned int noOfBells = 0;
    // When a new peal is download and where some of the ringers are unknown, you get too few people.
    if (towerId.ValidId() && ringId.ValidId())
    {
        const Tower* const theTower = towerDatabase.FindTower(towerId);
        if (theTower != NULL)
        {
            const Duco::Ring* const theRing = theTower->FindRing(ringId);
            if (theRing != NULL)
                noOfBells = theRing->NoOfBells();
        }
    }

    return noOfBells;
}

unsigned int
Peal::NoOfRingers(bool includeStrappers) const
{
    std::set<Duco::ObjectId> uniqueRingerIds;

    for (auto const& [key, value] : ringerIds)
    {
        if (value.RingerId().ValidId())
        {
            uniqueRingerIds.insert(value.RingerId());
        }
    }
    if (includeStrappers)
    {
        for (auto const& [key, value] : strapperIds)
        {
            if (value.RingerId().ValidId())
            {
                uniqueRingerIds.insert(value.RingerId());
            }
        }
    }
    return (unsigned int)uniqueRingerIds.size();
}

unsigned int
Peal::NoOfBellsRung(const Duco::RingingDatabase& database) const
{
    return NoOfBellsRung(database.TowersDatabase(), database.MethodsDatabase());
}

unsigned int
Peal::NoOfBellsRung(const Duco::TowerDatabase& towersDb, const Duco::MethodDatabase& methodsDb) const
{
    // Do this in order of tower, method, ringers - coz that matches the UI.
    unsigned int noOfBellsInTower = NoOfBellsInTower(towersDb);
    if (noOfBellsInTower != 0)
    {
        return noOfBellsInTower;
    }

    bool dualOrder = false;
    unsigned int noOfBellsInMethod = NoOfBellsInMethod(methodsDb, dualOrder);
    if (noOfBellsInMethod == noOfBellsInTower && noOfBellsInMethod != 0)
        return noOfBellsInTower;

    // Odd bell methods with covers are the problem

    unsigned int noOfRingers = NoOfRingers(false);
    handBell ? noOfRingers *= 2 : false;
    if (noOfBellsInMethod == noOfRingers && noOfBellsInMethod != 0)
        return noOfBellsInMethod;


    return std::max<unsigned int>({ noOfRingers, noOfBellsInTower, noOfBellsInMethod });
}

bool
Peal::ValidRingingWorldLink() const
{
    std::wregex e(L"([0-9]){0,5}([a-b]){0,1}\\.([0-9]){1,4}");
    return std::regex_match(ringingWorldReference.begin(), ringingWorldReference.end(), e);
}

bool
Peal::ValidRingingWorldReferenceForUpload() const
{
    std::wregex e(L"([0-9]){1,5}([a-b]){0,1}\\.([0-9]){1,4}");
    return std::regex_match(ringingWorldReference.begin(), ringingWorldReference.end(), e);
}

std::wstring
Peal::RingingWorldLink(const Duco::DatabaseSettings& settings) const
{
    std::wstring pageNumberStr = RingingWorldPage();
    unsigned int year = performanceDate.Year();
    if (performanceDate.Month() >= 11 && DucoEngineUtils::ToInteger(pageNumberStr) <= 50)
    {
        ++year;
    }
    return settings.BellBoardURL() + L"/issues/" + DucoEngineUtils::ToString(year) + L"/" + pageNumberStr;
}

std::wstring
Peal::FindReference() const
{
    if (ValidWebsiteLink())
        return BellBoardId();
    else if (ValidRingingWorldLink())
        return RingingWorldReference();
    else
    {
        std::wstring searchForBellNews = L"bell news";
        std::wstring searchForChurchBells = L"church bells";

        std::wstring lowerCaseNotes = notes;
        std::transform(notes.cbegin(), notes.cend(), lowerCaseNotes.begin(), ::tolower);

        std::wstring::size_type index = lowerCaseNotes.find(searchForBellNews);
        if (index != std::wstring::npos)
        {
            size_t endIndex = lowerCaseNotes.find(L"\n", index);
            return notes.substr(index, endIndex-index);
        }
        index = lowerCaseNotes.find(searchForChurchBells);
        if (index != std::wstring::npos)
        {
            size_t endIndex = lowerCaseNotes.find(L"\n", index);
            return notes.substr(index, endIndex - index);
        }
    }
    return L"";
}

bool
Peal::ValidWebsiteLink() const
{
    return ValidBellBoardId();
}

bool
Peal::ValidBellBoardId() const
{
    return DucoEngineUtils::RemoveInvalidChars(DatabaseSettings::EViewOnBellBoard, bellBoardId).length() > 0;
}

std::wstring
Peal::WebsiteLink(const Duco::DatabaseSettings& settings) const
{
    if (settings.PreferredOnlineWebsite() == DatabaseSettings::EViewOnBellBoard && ValidBellBoardId())
        return BellBoardLink(settings);
    else if (ValidBellBoardId())
        return BellBoardLink(settings);

    return L"";
}

std::wstring
Peal::BellBoardLink(const Duco::DatabaseSettings& settings) const
{
    std::wstring link (settings.BellBoardURL());
    link += settings.BellBoardViewSuffixURL();
    link += bellBoardId;
    return link;
}

void
Peal::SetBellBoardId(const std::wstring& newBellBoardId)
{
    bellBoardId = DucoEngineUtils::RemoveInvalidChars(DatabaseSettings::EViewOnBellBoard, newBellBoardId);
}

void
Peal::SetRingingWorldReference(const std::wstring& newRWRef)
{
    ringingWorldReference = newRWRef;
}

const std::wstring&
Peal::BellBoardId() const
{
    return bellBoardId;
}


const std::wstring&
Peal::RingingWorldReference() const
{
    return ringingWorldReference;
}

std::wstring
Peal::RingingWorldIssue() const
{
    return DucoEngineUtils::RingingWorldIssue(ringingWorldReference);
}

std::wstring 
Peal::RingingWorldPage() const
{
    return DucoEngineUtils::RingingWorldPage(ringingWorldReference);
}

const std::wstring&
Peal::Notes() const
{
    return notes;
}

void
Peal::SetNotes(const std::wstring& newNotes)
{
    notes = newNotes;
}

std::wstring
Peal::ConductorName(const RingingDatabase& database) const
{
    return ConductorName(database.RingersDatabase(), database.Settings());
}

std::wstring
Peal::ConductorName(const RingerDatabase& database, const Duco::DatabaseSettings& settings) const
{
    if (ConductedType() == ESilentAndNonConducted)
    {
        return L"Silent and Non conducted";
    }

    std::wstring conductorsName;
    size_t count (1);
    for (auto const& it : conductorIds)
    {
        if (count > 1)
        {
            conductorsName += L"; ";
        }
        const Duco::Ringer* nextConductor = database.FindRinger(it);
        if (nextConductor != NULL)
        {
            conductorsName += nextConductor->FullName(performanceDate, settings.LastNameFirst());
            ++count;
        }
    }
    return conductorsName;
}

std::wstring
Peal::ErrorString(const DatabaseSettings& settings, bool showWarnings) const
{
    std::wstring errorString;
    if (showWarnings && errorCode.test(EPealWarning_AssociationMissing))
    {
        DucoEngineUtils::AddError(errorString, L"Association missing");
    }
    else if (showWarnings && errorCode.test(EPealWarning_AssociationBlank))
    {
        DucoEngineUtils::AddError(errorString, L"Association blank");
    }

    if (errorCode.test(EPeal_TowerMissing))
    {
        DucoEngineUtils::AddError(errorString, L"Invalid tower");
    }
    if (errorCode.test(EPeal_RingMissing))
    {
        DucoEngineUtils::AddError(errorString, L"Invalid ring");
    }
    if (errorCode.test(EPeal_MethodMissing))
    {
        DucoEngineUtils::AddError(errorString, L"Invalid method");
    }
    if (errorCode.test(EPeal_TooManyChanges))
    {
        DucoEngineUtils::AddError(errorString, L"Too many changes - less than 5000 required");
    }
    else if (errorCode.test(EPeal_TooFewChanges))
    {
        if (settings.PealDatabase())
        {
            DucoEngineUtils::AddError(errorString, L"Not enough changes - 5000 or more required");
        }
        else
        {
            DucoEngineUtils::AddError(errorString, L"Not enough changes - 1250 or more required");
        }
    }
    if (errorCode.test(EPeal_DateInvalid))
    {
        DucoEngineUtils::AddError(errorString, L"Date invalid");
    }
    if (errorCode.test(EPeal_TimeInvalid))
    {
        DucoEngineUtils::AddError(errorString, L"Time invalid");
    }
    if (errorCode.test(EPeal_TooManyConductors))
    {
        DucoEngineUtils::AddError(errorString, L"Too many conductors");
    }
    if (errorCode.test(EPeal_TooFewConductors))
    {
        DucoEngineUtils::AddError(errorString, L"Missing conductors");
    }
    if (errorCode.test(EPeal_ConductorNotRinging))
    {
        DucoEngineUtils::AddError(errorString, L"Conductor isn't ringing");
    }
    if (errorCode.test(EPeal_NoOfRingersAndBellsMismatch))
    {
        DucoEngineUtils::AddError(errorString, L"Number of ringers doesn't match the number of bells");
    }
    else if (errorCode.test(EPeal_NoOfRingersIncorrect))
    {
        DucoEngineUtils::AddError(errorString, L"Ringers incomplete");
    }
    if (errorCode.test(EPeal_InvalidStrapper))
    {
        DucoEngineUtils::AddError(errorString, L"Invalid strapper");
    }
    if (errorCode.test(EPeal_StrapperMissing))
    {
        DucoEngineUtils::AddError(errorString, L"Strapper missing");
    }
    if (errorCode.test(EPeal_DefaultRingerMissing))
    {
        DucoEngineUtils::AddError(errorString, L"Default ringer missing");
    }
    if (errorCode.test(EPeal_DuplicateRingers)) // Just stop long error messages
    {
        DucoEngineUtils::AddError(errorString, L"Duplicate ringer");
    }
    if (errorCode.test(EPeal_MissingDuplicateRingers))
    {
        DucoEngineUtils::AddError(errorString, L"Missing duplicate ringers");
    }
    if (errorCode.test(EPeal_LinkedRingerTwice))
    {
        DucoEngineUtils::AddError(errorString, L"Linked ringers in the same peal");
    }
    if (errorCode.test(EPeal_MultiMethodsMissing))
    {
        DucoEngineUtils::AddError(errorString, L"Multi methods missing");
    }
    else if (showWarnings && errorCode.test(EPealWarning_MultiMethodsNotRequired))
    {
        DucoEngineUtils::AddError(errorString, L"Multi methods detail exists but method isn't spliced");
    }
    if (showWarnings && errorCode.test(EPealWarning_SeriesMissing))
    {
        DucoEngineUtils::AddError(errorString, L"Consider adding a method series to this peal");
    }
    if (showWarnings && errorCode.test(EPealWarning_SeriesNotRequired))
    {
        DucoEngineUtils::AddError(errorString, L"Method series set but not required");
    }
    if (errorCode.test(EPeal_CompositionInvalid))
    {
        DucoEngineUtils::AddError(errorString, L"Composition id is invalid");
    }
    if (showWarnings && errorCode.test(EPealWarning_CompositionHasDifferentMethod))
    {
        DucoEngineUtils::AddError(errorString, L"Composition is in a different method");
    }
    if (errorCode.test(EPeal_SeriesInvalid))
    {
        DucoEngineUtils::AddError(errorString, L"Invalid series");
    }
    if (errorCode.test(EPeal_SeriesHasDifferentNoOfBells))
    {
        DucoEngineUtils::AddError(errorString, L"Series has a different number of bells");
    }
    if (errorCode.test(EPeal_DuplicatePeal))
    {
        DucoEngineUtils::AddError(errorString, L"Peal could be a duplicate of another");
    }
    if (errorCode.test(EPeal_TowerAndHandbellMismatch))
    {
        DucoEngineUtils::AddError(errorString, L"Handbell setting in Tower and Peal do not match");
    }

    return errorString;
}

bool
Peal::Duplicate(const Duco::RingingObject& other, const Duco::RingingDatabase& database) const
{
    if (other.ObjectType() != ObjectType())
        return false;

    const Duco::Peal& otherPeal = static_cast<const Duco::Peal&>(other);

    if (DucoEngineUtils::CompareString(otherPeal.BellBoardId(), this->BellBoardId(), true, false))
    {
        return true;
    }

    size_t count = 0;
    otherPeal.performanceDate == performanceDate ? ++count : NULL;
    otherPeal.performanceTime.PealTimeClose(performanceTime) ? ++count : NULL;
    otherPeal.noOfChanges == noOfChanges ? ++count : NULL;

    if (count < 3)
    {
        return false;
    }
    {
        if (TowerId() == otherPeal.TowerId())
        {
            count += 1;
        }
        else
        {
            const Tower* thisTower = database.TowersDatabase().FindTower(TowerId());
            const Tower* otherTower = database.TowersDatabase().FindTower(otherPeal.TowerId(), true);

            if (thisTower != NULL && otherTower != NULL && (thisTower->Id() == otherTower->Id() || thisTower->Duplicate(*otherTower, database)))
            {
                count += 1;
            }
        }
    }

    if (count > 3) 
    {
        if (MethodId() == otherPeal.MethodId())
        {
            count += 1; 
        }
        else
        {
            const Method* thisMethod = database.MethodsDatabase().FindMethod(MethodId(), false);
            const Method* otherMethod = database.MethodsDatabase().FindMethod(otherPeal.MethodId(), true);

            if (thisMethod != NULL && otherMethod != NULL && (thisMethod->Id() == otherMethod->Id() || thisMethod->Duplicate(*otherMethod, database)))
            {
                count += 1;
            }
        }
    }

    if (count >= 5)
    {
        return CompareRingerIds(otherPeal, database.RingersDatabase(), database.Settings());
    }

    return false;
}


void
Peal::SetError(TPealErrorCodes code)
{
    errorCode.set(code, 1);
}

bool
Peal::AlphabetLetter(const MethodDatabase& methodsDatabase,
                     const std::wstring& methodType, unsigned int requiredNoOfBells,
                     AlphabetData& alphabets,
                     bool uniqueMethods) const
{
    const Method* const theMethod = methodsDatabase.FindMethod(MethodId());

    if ( (methodType.length() <= 0 || (theMethod != NULL && DucoEngineUtils::CompareString(methodType, theMethod->Type(), true, true))) &&
            (requiredNoOfBells == -1 || (theMethod != NULL && requiredNoOfBells == theMethod->Order())) &&
            (theMethod != NULL && !theMethod->Spliced()) )
    {
        alphabets.AddPeal(theMethod->Name(), id, methodId, performanceDate, uniqueMethods);
    }
    return false;
}

bool
Peal::FindRingerBellByName(const Duco::RingingDatabase& database, const std::wstring& fieldValue, bool subStr, unsigned int& bellNo) const
{
    return FindRingerBellByName(database.RingersDatabase(), database.Settings(), fieldValue, subStr, bellNo);
}

bool
Peal::FindRingerBellByName(const Duco::RingerDatabase& ringersDb, const Duco::DatabaseSettings& settings,
                     const std::wstring& fieldValue, bool subStr, unsigned int& bellNo) const
{
    bellNo = 0;
    bool includePeal (false);
    std::map<unsigned int, PealRingerData>::const_iterator it = ringerIds.begin();
    while (it != ringerIds.end() && !includePeal)
    {
        const Ringer* const theRinger = ringersDb.FindRinger(it->second.RingerId());
        if (theRinger != NULL)
        {
            includePeal = theRinger->NameContainsSubstring(settings, fieldValue, subStr);
            if (includePeal)
                bellNo = it->first;
        }   
        ++it;
    }
    it = strapperIds.begin();
    while (it != strapperIds.end() && includePeal)
    {
        const Ringer* const theRinger = ringersDb.FindRinger(it->second.RingerId());
        if (theRinger != NULL)
        {
            includePeal = theRinger->NameContainsSubstring(settings, fieldValue, subStr);
            if (includePeal)
                bellNo = it->first;
        }   
        ++it;
    }
    
    return includePeal;
}

bool
Peal::CheckRingerNames(const Duco::RingingDatabase& database, bool excluding, const std::wstring& fieldValue, bool subStr) const
{
    return CheckRingerNames(database.RingersDatabase(), database.Settings(), excluding, fieldValue, subStr);
}

bool
Peal::CheckRingerNames(const Duco::RingerDatabase& ringersDb, const Duco::DatabaseSettings& settings, bool excluding, const std::wstring& fieldValue, bool subStr) const
{
    bool includePeal (excluding);
    std::map<unsigned int, PealRingerData>::const_iterator it = ringerIds.begin();
    while (it != ringerIds.end() &&
                ((!includePeal && !excluding) || (includePeal && excluding)))
    {
        const Ringer* const theRinger = ringersDb.FindRinger(it->second.RingerId());
        if (theRinger != NULL)
        {
            if (excluding)
                includePeal = !theRinger->NameContainsSubstring(settings, fieldValue, subStr);
            else
                includePeal = theRinger->NameContainsSubstring(settings, fieldValue, subStr);
        }   
        ++it;
    }
    it = strapperIds.begin();
    while (it != strapperIds.end() &&
                ((!includePeal && !excluding) || (includePeal && excluding)))
    {
        const Ringer* const theRinger = ringersDb.FindRinger(it->second.RingerId());
        if (theRinger != NULL)
        {
            if (excluding)
                includePeal = !theRinger->NameContainsSubstring(settings, fieldValue, subStr);
            else
                includePeal = theRinger->NameContainsSubstring(settings, fieldValue, subStr);
        }   
        ++it;
    }
    
    return includePeal;
}

bool 
Peal::CheckStrapperName(const Duco::RingingDatabase& database, const std::wstring& fieldValue, bool subStr) const
{
    bool strapperFound (false);
    std::map<unsigned int, PealRingerData>::const_iterator it = strapperIds.begin();
    while (it != strapperIds.end() && !strapperFound)
    {
        const Ringer* const theRinger = database.RingersDatabase().FindRinger(it->second.RingerId());
        if (theRinger != NULL)
        {
            strapperFound = theRinger->NameContainsSubstring(database.Settings(), fieldValue, subStr);
        }
    }
    return strapperFound;
}

void
Peal::AddPealCount(std::map<ObjectId, Duco::PealLengthInfo>& sortedRingerIds) const
{
    std::set<ObjectId> ringersInThisPeal;
    {
        for (auto const& [key, value] : ringerIds)
        {
            if (ringersInThisPeal.find(value.RingerId()) == ringersInThisPeal.end())
            {
                Duco::PealLengthInfo newInfo(*this);
                pair<ObjectId, Duco::PealLengthInfo> newObject(value.RingerId(), newInfo);
                pair<std::map<ObjectId, Duco::PealLengthInfo>::iterator, bool> ringerInserted = sortedRingerIds.insert(newObject);
                if (!ringerInserted.second)
                {
                    ringerInserted.first->second += newInfo;
                }
                ringersInThisPeal.insert(value.RingerId());
            }
        }
    }
    for (auto const& [key, value] : strapperIds)
    {
        if (ringersInThisPeal.find(value.RingerId()) == ringersInThisPeal.end())
        {
            Duco::PealLengthInfo newInfo(*this);
            pair<ObjectId, Duco::PealLengthInfo> newObject(value.RingerId(), newInfo);
            pair<std::map<ObjectId, Duco::PealLengthInfo>::iterator, bool> ringerInserted = sortedRingerIds.insert(newObject);
            if (!ringerInserted.second)
            {
                ringerInserted.first->second += newInfo;
            }
            ringersInThisPeal.insert(value.RingerId());
        }
    }
}

unsigned int
Peal::PealFee(unsigned int bellNo, bool strapper) const
{
    unsigned int value(0);
    if (strapper)
    {
        std::map<unsigned int, PealRingerData>::const_iterator strapperIt = strapperIds.find(bellNo);
        if (strapperIt != strapperIds.end())
        {
            value = strapperIt->second.PealFee();
        }
    }
    else
    {
        std::map<unsigned int, PealRingerData>::const_iterator it = ringerIds.find(bellNo);
        if (it != ringerIds.end())
        {
            value = it->second.PealFee();
        }
    }
    return value;
}

Duco::ObjectId
Peal::GetFeePayerId() const
{
    Duco::ObjectId feePayerId;
    for (auto const& [key, value] : strapperIds)
    {
        if (value.PealFee() > 0)
        {
            if (feePayerId.ValidId())
            {
                feePayerId.ClearId();
                return feePayerId;
            }
            feePayerId = value.RingerId();
        }
    }

    for (auto const& [key, value] : ringerIds)
    {
        if (value.PealFee() > 0)
        {
            if (feePayerId.ValidId())
            {
                feePayerId.ClearId();
                return feePayerId;
            }
            feePayerId = value.RingerId();
        }
    }
    if (!feePayerId.ValidId() && conductorIds.size() == 1)
    {
        feePayerId = *conductorIds.begin();
    }

    return feePayerId;
}

Duco::ObjectId
Peal::SuggestFeePayerId(const std::set<ObjectId>& commonFeePayers) const
{
    std::set<ObjectId> commonIds;
    set_intersection(commonFeePayers.begin(), commonFeePayers.end(), conductorIds.begin(), conductorIds.end(), std::inserter(commonIds, commonIds.end()));
    if (!commonIds.empty())
        return *commonIds.begin();

    std::set<ObjectId> ids;
    DucoEngineUtils::ToSet(ringerIds, ids);

    commonIds.clear();
    set_intersection(commonFeePayers.begin(), commonFeePayers.end(), ids.begin(), ids.end(), std::inserter(commonIds, commonIds.end()));
    if (!commonIds.empty())
        return *commonIds.begin();

    commonIds.clear();
    set_intersection(commonFeePayers.begin(), commonFeePayers.end(), ids.begin(), ids.end(), std::inserter(commonIds, commonIds.end()));
    if (!commonIds.empty())
        return *commonIds.begin();

    return ObjectId();
}

bool
Peal::SetPealFee(unsigned int bellNo, bool strapper, unsigned int pealFeeInPence)
{
    bool valueChanged(false);
    if (strapper)
    {
        std::map<unsigned int, PealRingerData>::iterator strapperIt = strapperIds.find(bellNo);
        if (strapperIt != strapperIds.end())
        {
            if (strapperIt->second.PealFee() != pealFeeInPence)
            {
                valueChanged = true;
                strapperIt->second.SetPealFee(pealFeeInPence);
            }
        }
        else
        {
            PealRingerData newRingerData;
            newRingerData.SetPealFee(pealFeeInPence);
            std::pair<unsigned int, PealRingerData> newRinger(bellNo, newRingerData);
            strapperIds.insert(newRinger);
            valueChanged = true;
        }
    }
    else
    {
        std::map<unsigned int, PealRingerData>::iterator it = ringerIds.find(bellNo);
        if (it != ringerIds.end())
        {
            if (it->second.PealFee() != pealFeeInPence)
            {
                valueChanged = true;
                it->second.SetPealFee(pealFeeInPence);
            }
        }
        else
        {
            PealRingerData newRingerData;
            newRingerData.SetPealFee(pealFeeInPence);
            std::pair<unsigned int, PealRingerData> newRinger(bellNo, newRingerData);
            ringerIds.insert(newRinger);
            valueChanged = true;
        }
    }
    return valueChanged;
}

unsigned int
Peal::TotalPealFeePaid() const
{
    unsigned int totalPealFee (0);
    std::map<unsigned int, PealRingerData>::const_iterator strapperIt = strapperIds.begin();
    while (strapperIt != strapperIds.end())
    {
        totalPealFee += strapperIt->second.PealFee();
        ++strapperIt;
    }
    std::map<unsigned int, PealRingerData>::const_iterator ringerIt = ringerIds.begin();
    while (ringerIt != ringerIds.end())
    {
        totalPealFee += ringerIt->second.PealFee();
        ++ringerIt;
    }

    return totalPealFee;
}

unsigned int
Peal::AvgPealFeePaidPerPerson() const
{
    unsigned int total (TotalPealFeePaid());
    if (NoOfRingers() != 0)
    {
        total /= NoOfRingers(true);
    }
    return total;
}

bool
Peal::CheckPaidFeeAgainstMedian(unsigned int minPealFeeInPencePerPerson) const
{
    return AvgPealFeePaidPerPerson() >= minPealFeeInPencePerPerson;
}

unsigned int
Peal::MissingPealFee(unsigned int avgPealFeePaidPerPersonThisYear) const
{
    if (avgPealFeePaidPerPersonThisYear < 0 || avgPealFeePaidPerPersonThisYear == -1)
        return 0;

    int missing (NoOfRingers() * int (avgPealFeePaidPerPersonThisYear));
    missing -= TotalPealFeePaid();
    if (missing < 0)
        return 0;
    
    return missing;
}

bool
Peal::PossibleHandbellPeal() const
{
    for (auto const& [key, value] : strapperIds)
    {
        if (value.RingerId().ValidId())
            return false;
    }

    std::map<unsigned int, Duco::PealRingerData>::const_iterator ringer = ringerIds.begin();
    while (ringer != ringerIds.end())
    {
        std::map<unsigned int, Duco::PealRingerData>::const_iterator nextRinger = ringer++;
        if (nextRinger == ringerIds.end() || ringer == ringerIds.end() || nextRinger->second.RingerId() != ringer->second.RingerId())
        {
            return false;
        }
        ++ringer;
    }

    return true;
}

void
Peal::ConvertToHandbellPeal()
{
    strapperIds.clear();
    SetHandbell(true);

    std::map<unsigned int, Duco::PealRingerData> newRingerIds;
    std::map<unsigned int, Duco::PealRingerData>::const_iterator ringer = ringerIds.begin();
    while (ringer != ringerIds.end())
    {
        unsigned int noOfRingers (((unsigned int)newRingerIds.size()) + 1);
        std::pair<unsigned int, Duco::PealRingerData> newObject(noOfRingers, ringer->second);
        newRingerIds.insert(newObject);
        ++ringer;
        if (ringer != ringerIds.end())
            ++ringer;
    }
    ringerIds = newRingerIds;
}

void
Peal::ExportToXml(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& parent, bool exportDucoObjects) const
{
}

bool
Peal::ReplaceTowerId(const ObjectId& oldTowerId, const ObjectId& newTowerId, const std::map<Duco::ObjectId, Duco::ObjectId>& otherRingIds)
{
    if (towerId != oldTowerId)
        return false;

    if (ringId.ValidId())
    {
        std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator it = otherRingIds.find(ringId);
        if (it == otherRingIds.end())
            return false;

        ringId = it->second;
    }

    towerId = newTowerId;

    return true;
}

void
Peal::AddRingersToCirclingData(std::map<Duco::ObjectId, Duco::RingerCirclingData*>& data, const Duco::Ring& theRing) const
{
    std::map<unsigned int, PealRingerData>::const_iterator it = ringerIds.begin();
    while (it != ringerIds.end())
    {
        unsigned int bellNumber = it->first;
        if (theRing.RealBellNumber(bellNumber))
        {
            Duco::ObjectId ringerId = it->second.RingerId();
            std::map<Duco::ObjectId, Duco::RingerCirclingData*>::iterator ringerData = data.find(ringerId);
            if (ringerData == data.end())
            {
                RingerCirclingData* newRingerData = new RingerCirclingData(ringerId);
                newRingerData->AddPeal(bellNumber, performanceDate);
                std::pair< Duco::ObjectId, Duco::RingerCirclingData*> newData(ringerId, newRingerData);
                data.insert(newData);
            }
            else
            {
                ringerData->second->AddPeal(bellNumber, performanceDate);
            }
        }

        ++it;
    }
    it = strapperIds.begin();
    while (it != strapperIds.end())
    {
        unsigned int bellNumber = it->first;
        if (theRing.RealBellNumber(bellNumber))
        {
            Duco::ObjectId ringerId = it->second.RingerId();
            std::map<Duco::ObjectId, Duco::RingerCirclingData*>::iterator ringerData = data.find(ringerId);
            if (ringerData == data.end())
            {
                RingerCirclingData* newRingerData = new RingerCirclingData(ringerId);
                newRingerData->AddPeal(bellNumber, performanceDate);
                std::pair<Duco::ObjectId, Duco::RingerCirclingData*> newData(ringerId, newRingerData);
                data.insert(newData);
            }
            else
            {
                ringerData->second->AddPeal(bellNumber, performanceDate);
            }
        }

        ++it;
    }
}

void
Peal::SetTowerId(const ObjectId& newTowerId)
{
    towerId = newTowerId;
}

void
Peal::SetRingId(const ObjectId& newRingId)
{
    ringId = newRingId;
}

std::wstring
Peal::MethodName(const Duco::MethodDatabase& methodDatabase) const
{
    const Duco::Method* const theMethod = methodDatabase.FindMethod(methodId,false);
    if (theMethod != NULL)
    {
        return theMethod->FullName(methodDatabase);
    }
    return L"";
}
std::wstring
Peal::MethodName(const Duco::RingingDatabase& ringingDatabase) const
{
    return MethodName(ringingDatabase.MethodsDatabase());
}
std::wstring
Peal::RingerName(unsigned int bellNo, bool strapper, const Duco::RingerDatabase& ringerDatabase, const Duco::DatabaseSettings& settings) const
{
    Duco::ObjectId ringerId;
    if (RingerId(bellNo, strapper, ringerId) && ringerId.ValidId())
    {
        const Duco::Ringer* const theRinger = ringerDatabase.FindRinger(ringerId, false);
        if (theRinger != NULL)
        {
            return theRinger->FullName(this->performanceDate, settings.LastNameFirst());
        }
    }
    return L"";
}
std::wstring
Peal::RingerName(unsigned int bellNo, bool strapper, const Duco::RingingDatabase& ringingDatabase) const
{
    return RingerName(bellNo, strapper, ringingDatabase.RingersDatabase(), ringingDatabase.Settings());
}

std::wstring
Peal::TowerName(const Duco::TowerDatabase& towerDatabase) const
{
    const Duco::Tower* const theTower = towerDatabase.FindTower(towerId, false);
    if (theTower != NULL)
    {
        return theTower->FullName();
    }
    return L"";
}

std::wstring
Peal::TowerName(const Duco::RingingDatabase& ringingDatabase) const
{
    return TowerName(ringingDatabase.TowersDatabase());
}

float
Peal::ChangesPerMinute() const
{
    if (ChangesAndTimeValid())
    {
        return performanceTime.ChangesPerMinute(noOfChanges);
    }
    return 0;
}

bool
Peal::ChangesAndTimeValid() const
{
    if (noOfChanges == 0 || !performanceTime.Valid())
    {
        return false;
    }
    return true;
}

bool
Peal::ContainsNonHumanRinger(const RingerDatabase& database) const
{
    for (auto const& [key, value] : ringerIds)
    {
        const Ringer* const nextRinger = database.FindRinger(value.RingerId());
        if (nextRinger != NULL && nextRinger->NonHuman())
        {
            return true;
        }
    }
    for (auto const& [key, value] : strapperIds)
    {
        const Ringer* const nextRinger = database.FindRinger(value.RingerId());
        if (nextRinger != NULL && nextRinger->NonHuman())
        {
            return true;
        }
    }

    return false;
}

bool
Peal::ContainsDeceasedRinger(const RingerDatabase& database) const
{
    for (auto const& [key, value] : ringerIds)
    {
        const Ringer* const nextRinger = database.FindRinger(value.RingerId());
        if (nextRinger != NULL && nextRinger->Deceased())
        {
            return true;
        }
    }
    for (auto const& [key, value] : strapperIds)
    {
        const Ringer* const nextRinger = database.FindRinger(value.RingerId());
        if (nextRinger != NULL && nextRinger->Deceased())
        {
            return true;
        }
    }

    return false;
}

const std::wstring&
Peal::EnteredBy() const
{
    return enteredBy;
}

void
Peal::SetEnteredBy(const std::wstring& newName)
{
    enteredBy = newName;
}


//EOF

