#pragma once

#include <string>
#include <list>
#include <set>
#include "DucoEngineCommon.h"
#include "ObjectId.h"
#include "PerformanceDate.h"

namespace Duco
{
    class Peal;
    class Tower;
    class AssociationDatabase;
    class RingingDatabase;
    class RingerDatabase;
    class MethodDatabase;
    class TowerDatabase;
    class DatabaseSettings;

    class StatisticFilters
    {
    public:
        DllExport StatisticFilters(const Duco::RingingDatabase& theDatabase);
        DllExport ~StatisticFilters();
        DllExport StatisticFilters(const Duco::StatisticFilters& other);

        DllExport const Duco::StatisticFilters& operator=(const Duco::StatisticFilters& other);

        DllExport void Clear();
        DllExport void SetDefaultRinger();
        DllExport void SetDefaultConductor();

        DllExport std::wstring Description() const;

        inline void SetExcludeValid(bool state);
        inline bool ExcludeValid() const;
        inline void SetIncludeWithdrawn(bool state);
        inline bool IncludeWithdrawn() const;
        inline void SetExcludeInvalidTimeOrChanges(bool state);
        inline bool ExcludeInvalidTimeOrChanges() const;
        inline void SetIncludeTowerBell(bool state);
        inline bool IncludeTowerBell() const;
        inline void SetIncludeHandBell(bool state);
        inline bool IncludeHandBell() const;
        DllExport bool Association(Duco::ObjectId& association) const;
        DllExport void SetAssociation(bool state, const Duco::ObjectId& newAssociation);
        DllExport const Duco::ObjectId& RingerId() const;
        DllExport bool Ringer(Duco::ObjectId& ringerId) const;
        DllExport void SetRinger(bool state, const Duco::ObjectId& ringerId);
        inline void SetIncludeLinkedRingers(bool state);
        inline bool IncludeLinkedRingers() const;
        DllExport bool Conductor(Duco::ObjectId& conductorId) const;
        DllExport void SetConductor(bool state, const Duco::ObjectId& conductorId);
        DllExport bool Method(Duco::ObjectId& methodId) const;
        DllExport void SetMethod(bool state, const Duco::ObjectId& methodId);
        DllExport bool Type(std::wstring& type) const;
        DllExport void SetType(bool state, const std::wstring& type);
        DllExport bool Stage(unsigned int& stage) const;
        DllExport void SetStage(bool state, unsigned int newStage);
        DllExport void SetEuropeOnly(bool state);
        DllExport bool EuropeOnly() const;
        DllExport bool Tower(Duco::ObjectId& towerId) const;
        DllExport void SetTower(bool state, const Duco::ObjectId& towerId);
        DllExport bool Ring(Duco::ObjectId& ringId) const;
        DllExport void SetRing(bool state, const Duco::ObjectId& ringId);
        DllExport bool County(std::wstring& towerId) const;
        DllExport void SetCounty(bool state, const std::wstring& newCounty);
        inline void SetIncludeLinkedTowers(bool state);
        inline bool IncludeLinkedTowers() const;
        DllExport void SetStartDate(bool state, const Duco::PerformanceDate& newStartDate);
        DllExport bool StartDateEnabled(Duco::PerformanceDate& startDate) const;
        DllExport const Duco::PerformanceDate& StartDate() const;
        DllExport void SetEndDate(bool state, const Duco::PerformanceDate& newEndDate);
        DllExport bool EndDateEnabled(Duco::PerformanceDate& endDate) const;
        DllExport const Duco::PerformanceDate& EndDate() const;

        bool Match(const Duco::Peal& peal) const;
        bool Match(const Duco::Tower& tower) const;

        DllExport bool Enabled() const;

    protected:
        const Duco::RingerDatabase&         ringers;
        const Duco::MethodDatabase&         methods;
        const Duco::TowerDatabase&          towers;
        const Duco::AssociationDatabase&    associations;
        const Duco::DatabaseSettings&       settings;
        mutable std::list< std::set<Duco::ObjectId> >*  allLinkedTowerIds;

        Duco::PerformanceDate               startDate;
        bool                                startDateEnabled;
        Duco::PerformanceDate               endDate;
        bool                                endDateEnabled;
        bool                                excludeValid;
        bool                                excludeInvalidTimeOrChanges;
        bool                                includeWithdrawn;
        bool                                includeTowerBell;
        bool                                includeHandBell;

        bool associationEnabled;
        Duco::ObjectId associationId;

        bool ringerEnabled;
        Duco::ObjectId ringerId;
        bool includeLinkedRingers;

        bool conductorEnabled;
        Duco::ObjectId conductorId;

        bool methodEnabled;
        Duco::ObjectId methodId;

        bool typeEnabled;
        std::wstring type;

        bool stageEnabled;
        unsigned int stage;

        bool countyEnabled;
        std::wstring county;

        bool europeOnly;

        bool towerEnabled;
        Duco::ObjectId towerId;
        bool includeLinkedTowers;

        bool ringEnabled;
        Duco::ObjectId ringId;
    };

    void StatisticFilters::SetExcludeValid(bool state)
    {
        excludeValid = state;
    }
    bool StatisticFilters::ExcludeValid() const
    {
        return excludeValid;
    }
    void StatisticFilters::SetIncludeWithdrawn(bool state)
    {
        includeWithdrawn = state;
    }
    bool StatisticFilters::IncludeWithdrawn() const
    {
        return includeWithdrawn;
    }
    void StatisticFilters::SetExcludeInvalidTimeOrChanges(bool state)
    {
        excludeInvalidTimeOrChanges = state;
    }
    bool StatisticFilters::ExcludeInvalidTimeOrChanges() const
    {
        return excludeInvalidTimeOrChanges;
    }
    void StatisticFilters::SetIncludeTowerBell(bool state)
    {
        includeTowerBell = state;
    }
    bool StatisticFilters::IncludeTowerBell() const
    {
        return includeTowerBell;
    }
    void StatisticFilters::SetIncludeHandBell(bool state)
    {
        includeHandBell = state;
    }
    bool StatisticFilters::IncludeHandBell() const
    {
        return includeHandBell;
    }
    void StatisticFilters::SetIncludeLinkedTowers(bool state)
    {
        includeLinkedTowers = state;
    }
    bool StatisticFilters::IncludeLinkedTowers() const
    {
        return includeLinkedTowers;
    }
    void StatisticFilters::SetIncludeLinkedRingers(bool state)
    {
        includeLinkedRingers = state;
    }
    bool StatisticFilters::IncludeLinkedRingers() const
    {
        return includeLinkedRingers;
    }

}
