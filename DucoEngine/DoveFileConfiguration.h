#ifndef __DOVEFILECONFIGURATION_H__
#define __DOVEFILECONFIGURATION_H__

#include <string>

namespace Duco
{

    class DoveFileConfiguration
    {
    public:
        DoveFileConfiguration();
        ~DoveFileConfiguration();

        void Configure(const std::wstring& fileLine);
        bool ConfigValid() const;

        unsigned int DoveIdFieldNumber() const;
        unsigned int RingIdFieldNumber() const;
        unsigned int OldDoveIdFieldNumber() const;
        unsigned int TowerbaseIdFieldNumber() const;
        unsigned int CountyFieldNumber() const;
        unsigned int CountryFieldNumber() const;
        unsigned int CountryIso3166FieldNumber() const; // ISO3166
        unsigned int TownFieldNumber() const;
        unsigned int NameFieldNumber() const;
        unsigned int DedicatationFieldNumber() const;
        unsigned int BellsFieldNumber() const;
        unsigned int TenorFieldNumber() const;
        unsigned int ApproxTenorFieldNumber() const;
        unsigned int KeyFieldNumber() const;
        unsigned int UnringableFieldNumber() const;
        unsigned int AltNameFieldNumber() const; //(T)
        unsigned int LatitudeFieldNumber() const;
        unsigned int LongitudeFieldNumber() const;
        unsigned int SemiTonesFieldNumber() const;

        unsigned int GroundFloorFieldNumber() const;
        unsigned int WebsiteFieldNumber() const;
        unsigned int ExtraInfoFieldNumber() const;

    private:
        unsigned int doveIdFieldNumber;
        unsigned int ringIdFieldNumber;
        unsigned int oldDoveIdFieldNumber;
        unsigned int towerbaseIdFieldNumber;
        unsigned int countyFieldNumber;
        unsigned int countryFieldNumber;
        unsigned int countryIso3166FieldNumber; // ISO3166
        unsigned int townFieldNumber;
        unsigned int nameFieldNumber;
        unsigned int dedicatationFieldNumber;
        unsigned int bellsFieldNumber;
        unsigned int tenorFieldNumber;
        unsigned int approxTenorFieldNumber;
        unsigned int keyFieldNumber;
        unsigned int unringableFieldNumber;
        unsigned int altNameFieldNumber; //(T)
        unsigned int latitudeFieldNumber;
        unsigned int longitudeFieldNumber;
        unsigned int groundFloorFieldNumber;
        unsigned int websiteFieldNumber;
        unsigned int extraInfoFieldNumber;
        unsigned int semiTonesFieldNumber;
    };

}
#endif //!__DOVEFILECONFIGURATION_H__
