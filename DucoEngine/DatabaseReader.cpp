#include "DatabaseReader.h"

#include "DucoConfiguration.h"
#include "PerformanceDate.h"
#include "PerformanceTime.h"
#include "DucoEngineUtils.h"
#include "ObjectId.h"
#include <stdexcept>
#include <cstring>
#include <locale>
#include <codecvt>

using namespace std;
using namespace Duco;

DatabaseReader::DatabaseReader(const char* filename)
{
    input = new std::wifstream(filename, ios::in | ios::binary);
    if (!input->is_open())
    {
        throw std::invalid_argument("File doesn't exist");
    }
    std::wistream::int_type firstChar = input->peek();
    if (firstChar == 0)
    {
        std::locale loc(std::locale("C"), new std::codecvt_utf8<wchar_t>());
        input->imbue(loc);
    }
    input->seekg(0, input->beg);
    int databaseVersion = ReadInt();
    if (databaseVersion <= 28)
    {
        delete input;
        input = new std::wifstream(filename);
        int databaseVersion = ReadInt();
        input->seekg(0, input->beg);
        if (databaseVersion == 40)
        {
            std::locale loc(std::locale("C"), new std::codecvt_utf16<wchar_t>());
            input->imbue(loc);
        }
    }
    else
    {
        if (databaseVersion >= 41)
        {
            std::locale loc(std::locale("C"), new std::codecvt_utf8<wchar_t>());
            input->imbue(loc);
        }
        input->seekg(0, input->beg);
    }
}

DatabaseReader::~DatabaseReader()
{
    if (input != NULL)
    {
        input->close();
        delete input;
        input = NULL;
    }
}

int
DatabaseReader::ReadInt()
{
    if (!input->is_open())
    {
        throw std::invalid_argument("File not open");
    }
    int tempInput = 0;
    *input >> tempInput;
    input->ignore();
    return tempInput;
}

unsigned int 
DatabaseReader::ReadUInt()
{
    if (!input->is_open())
    {
        throw std::invalid_argument("File not open");
    }
    unsigned int tempInput = 0;
    *input >> tempInput;
    input->ignore();
    return tempInput;
}

bool
DatabaseReader::ReadBool()
{
    if (!input->is_open())
    {
        throw std::invalid_argument("File not open");
    }
    bool tempInput = false;
    *input >> tempInput;
    input->ignore();
    return tempInput;
}

time_t
DatabaseReader::ReadTime()
{
    if (!input->is_open())
    {
        throw std::invalid_argument("File not open");
    }
    long long tempTime = 0;
    *input >> tempTime;
    input->ignore();

    return tempTime;
}

void
DatabaseReader::ReadString(std::wstring& str)
{
    if (!input->is_open())
    {
        throw std::invalid_argument("File not open");
    }
    str.clear();
    unsigned int stringLength = ReadUInt();
    noskipws(*input);
    while (stringLength > 0)
    {
        wchar_t inputChar;
        *input >> inputChar;
        if (input->gcount() == 0)
        {
            input->ignore();
        }
        str.push_back(inputChar);
        --stringLength;
    }
    skipws(*input);
}

void
DatabaseReader::ReadPictureBuffer(std::string*& outputStr)
{
    if (!input->is_open())
    {
        throw std::invalid_argument("File not open");
    }
    if (outputStr == NULL)
    {
        outputStr = new std::string();
    }
    else
    {
        outputStr->clear();
    }

    streamsize remainingStringLength = (streamsize)ReadUInt();
    while (remainingStringLength > 0)
    {
        int temp = input->peek();
        if (temp == EOF)
        {
            break;
        }
        if (input->fail())
        {
            break;
        }
        if (input->bad())
        {
            break;
        }
        wchar_t* buffer = new wchar_t[remainingStringLength];
        memset(buffer, 0, remainingStringLength);
        input->read(buffer, remainingStringLength);
        streamsize readFromFile = input->gcount();
        for (streamsize count = 0; count < readFromFile; ++count)
        {
            wchar_t temp = buffer[count];
            char temp2 = input->narrow(temp);
            (*outputStr) += temp2;
        }
        remainingStringLength -= readFromFile;
        delete[] buffer;
    }
}

void
DatabaseReader::ReadTime(Duco::PerformanceTime& performanceTime)
{
    unsigned int minutes = ReadUInt();
    PerformanceTime newTime(minutes);
    performanceTime = newTime;
}

void
DatabaseReader::ReadDate(Duco::PerformanceDate& performanceDate)
{
    unsigned int year = ReadUInt();
    unsigned int month = ReadUInt();
    unsigned int day = ReadUInt();

    PerformanceDate newDate(year, month, day);
    performanceDate = newDate;
}

void
DatabaseReader::ReadId(Duco::ObjectId& id)
{
    if (!input->is_open())
    {
        throw std::invalid_argument("File not open");
    }
    id.operator>>(*input);
}
