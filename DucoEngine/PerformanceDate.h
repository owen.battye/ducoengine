#ifndef __PERFORMANCEDATE_H__
#define __PERFORMANCEDATE_H__

#include <array>
#include <ostream>
#include <string>
#include "DucoEngineCommon.h"
#include "PerformanceTime.h"

namespace Duco
{
class PerformanceDate
{
    friend class DatabaseWriter;

public:
    DllExport PerformanceDate();
    DllExport PerformanceDate(const tm& newDate);
    DllExport PerformanceDate(const std::wstring& dateStringInput, bool includeSpaceAsSeperator = false);
    DllExport PerformanceDate(unsigned int pealDateYear, unsigned int pealDateMonth, unsigned int pealDateDay);
    DllExport PerformanceDate(const Duco::PerformanceDate& other);
    DllExport ~PerformanceDate();

    DllExport std::wstring Str() const;
    DllExport std::wstring StrForBBExport() const;
    DllExport std::wstring DateWithLongerMonth() const;
    DllExport std::wstring DateForAssociationReports() const;
    DllExport std::wstring FullDate() const;
    DllExport unsigned int Year() const;
    DllExport unsigned int Month() const;
    DllExport unsigned int Day() const;
    DllExport std::wstring WeekDay(bool longFormat = false) const;
    int DayNumber() const;

    DllExport void Clear();
    DllExport Duco::PerformanceDate& ResetToEarliest();
    DllExport Duco::PerformanceDate& ResetToLatest();

    DllExport void RemoveMonths(unsigned int numberOfMonths);
    DllExport unsigned int NumberOfDaysUntil(const Duco::PerformanceDate& lhs) const;
    DllExport bool DaysMatch(const Duco::PerformanceDate& lhs) const;
    DllExport Duco::PerformanceDate AddDays(unsigned int noOfDays) const;

    DllExport bool operator>=(const Duco::PerformanceDate& lhs) const;
    DllExport bool operator<=(const Duco::PerformanceDate& lhs) const;
    DllExport bool operator>(const Duco::PerformanceDate& lhs) const;
    DllExport bool operator<(const Duco::PerformanceDate& lhs) const;
    DllExport bool operator==(const Duco::PerformanceDate& lhs) const;
    DllExport bool operator!=(const Duco::PerformanceDate& lhs) const;

    DllExport std::wostream& PrintDate(std::wostream& stream, bool longMonth = false, wchar_t seperator = ' ') const;

    DllExport bool IsYearValid() const;
    DllExport bool Valid() const;
    DllExport static std::array<unsigned int, 12> DaysInMonths(unsigned int currentYear);

private:
    unsigned int ConvertMonthString(const std::wstring& monthStr) const;
    time_t time() const;
    void ReadTimet(time_t newDate);

protected:
    unsigned int pealDateYear;
    unsigned int pealDateMonth;
    unsigned int pealDateDay;
};

const Duco::PerformanceDate KFirstPealDate(1500, 1, 1);
const Duco::PerformanceDate KFirstJanuary2000(2000, 1, 1);
const Duco::PerformanceDate KSecondJanuary2000(2000, 1, 2);
const Duco::PerformanceDate KFirstJanuary1900(1900, 1, 1);

}

#endif // __PERFORMANCEDATE_H__
