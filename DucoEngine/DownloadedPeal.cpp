#include "DownloadedPeal.h"

#include "MissingRinger.h"
#include "Tower.h"
#include "DatabaseSettings.h"

using namespace Duco;

void
DownloadedPeal::SetAssociationId(const Duco::ObjectId& newAssociationId)
{
    if (newAssociationId.ValidId())
    {
        missingAssociation.clear();
        Peal::SetAssociationId(newAssociationId);
    }
}

void
DownloadedPeal::SaveMissingAssociation(const std::wstring& newAssociationName)
{
    if (newAssociationName.length() > 0)
    {
        missingAssociation = newAssociationName;
        Peal::SetAssociationId(KNoId);
    }
}

const std::wstring&
DownloadedPeal::MissingAssociation() const
{
    return missingAssociation;
}

bool
DownloadedPeal::IsMissingRingerConductor(unsigned int bellNo, bool strapper) const
{
    auto found = missingRingers.begin();
    while (found != missingRingers.end())
    {
        if (found->bellNo == bellNo && found->strapper == strapper)
        {
            return found->conductor;
        }
        ++found;
    }
    return false;
}

bool
DownloadedPeal::IsMissingRinger(unsigned int bellNo, bool strapper) const
{
    auto found = missingRingers.begin();
    while (found != missingRingers.end())
    {
        if (found->bellNo == bellNo && found->strapper == strapper)
        {
            return true;
        }
        ++found;
    }
    return false;
}

bool
DownloadedPeal::SaveMissingRinger(const Duco::MissingRinger& missingRingerInfo)
{
    return missingRingers.insert(missingRingerInfo).second;
}

std::wstring
DownloadedPeal::MissingRinger(unsigned int bellNo, bool strapper, const DatabaseSettings& settings) const
{
    auto found = missingRingers.begin();
    while (found != missingRingers.end())
    {
        if (found->bellNo == bellNo && found->strapper == strapper)
        {
            return found->Name(settings.LastNameFirst());
        }
        ++found;
    }

    return L"";
}

std::wstring
DownloadedPeal::MissingRingers(const Duco::DatabaseSettings& settings) const
{
    std::wstring returnValue = L"";
    size_t numberOfRingers = missingRingers.size();
    size_t ringerNumber = 0;
    auto found = missingRingers.begin();
    while (found != missingRingers.end())
    {
        ++ringerNumber;
        if (returnValue.length() > 0)
        {
            if (ringerNumber == numberOfRingers)
            {
                returnValue += L" and ";
            }
            else
            {
                if (settings.LastNameFirst())
                    returnValue += L"; ";
                else
                    returnValue += L", ";
            }
        }
        returnValue += found->Name(settings.LastNameFirst());
        ++found;
    }
    return returnValue;
}

bool
DownloadedPeal::HasMissingRingers() const
{
    return missingRingers.size() > 0;
}

void
DownloadedPeal::SaveMissingMethod(const std::wstring& methodName)
{
    missingMethod = methodName;
}

const std::wstring&
DownloadedPeal::MissingMethod() const
{
    return missingMethod;
}

void
DownloadedPeal::SetTowerId(const ObjectId& newTowerId)
{
    Peal::SetTowerId(newTowerId);
    missingTower.clear();
}

void
DownloadedPeal::SetRingId(const ObjectId& newRingId)
{
    Peal::SetRingId(newRingId);
    missingTenorWeight.clear();
    missingTenorKey.clear();
}


void
DownloadedPeal::SaveMissingTower(const std::wstring& towerDedication, const std::wstring& towerName, const std::wstring& towerCounty, const std::wstring& towerbaseId, const std::wstring& doveId)
{
    missingTower = towerName + L", " + towerCounty + L". (" + towerDedication + L")";
    missingTowerbaseId = towerbaseId;
    missingDoveId = doveId;
}

const std::wstring&
DownloadedPeal::MissingTower() const
{
    return missingTower;
}

const std::wstring&
DownloadedPeal::MissingTowerbaseId() const
{
    return missingTowerbaseId;
}

const std::wstring&
DownloadedPeal::MissingDoveId() const
{
    return missingDoveId;
}


void
DownloadedPeal::SaveMissingRing(const std::wstring& tenorWeight, const std::wstring& tenorKey)
{
    missingTenorWeight = tenorWeight;
    missingTenorKey = tenorKey;
}

std::wstring
DownloadedPeal::MissingRing(const Duco::RingingDatabase& ringingDb) const
{
    if (missingTenorWeight.length() == 0 && missingTenorKey.length() == 0)
    {
        return L"";
    }

    return Tower::DefaultRingName(NoOfBellsRung(ringingDb));
}

bool
DownloadedPeal::MissingRingOnly() const
{
    return missingTower.length() == 0 && (missingTenorWeight.length() > 0 || missingTenorKey.length() > 0);
}

const std::wstring&
DownloadedPeal::MissingTenorWeight() const
{
    return missingTenorWeight;
}

const std::wstring&
DownloadedPeal::MissingTenorKey() const
{
    return missingTenorKey;
}

unsigned int
DownloadedPeal::NoOfRingers(bool includeStrappers) const
{
    unsigned int noOfRingersFromBaseClass = Peal::NoOfRingers(includeStrappers);
    unsigned int noOfRingerCount = 0;

    {
        std::set<unsigned int> bells;
        std::set<Duco::MissingRinger>::const_iterator found = missingRingers.begin();
        while (found != missingRingers.end())
        {
            if (includeStrappers || !found->strapper)
            {
                bells.insert(found->bellNo);
                ++noOfRingerCount;
            }
            ++found;
        }
        std::map<unsigned int, PealRingerData>::const_iterator it = ringerIds.begin();
        while (it != ringerIds.end())
        {
            if (it->second.RingerId().ValidId())
            {
                bells.insert(it->first);
                ++noOfRingerCount;
            }
            ++it;
        }
        if (includeStrappers)
        {
            it = strapperIds.begin();
            while (it != strapperIds.end())
            {
                if (it->second.RingerId().ValidId())
                {
                    bells.insert(it->first);
                    ++noOfRingerCount;
                }
                ++it;
            }
        }
        unsigned int highestBellNumber = 0;
        if (bells.size() > 0)
        {
            highestBellNumber = std::max<unsigned int>(*bells.rbegin(), (unsigned int)bells.size());
        }

        noOfRingerCount = std::max<unsigned int>(noOfRingerCount, highestBellNumber);
    }
    return std::max<unsigned int>(noOfRingersFromBaseClass, noOfRingerCount);
}

bool
DownloadedPeal::ConductorOnBell(unsigned int bellNo, bool strapper) const
{
    if (Peal::ConductorOnBell(bellNo, strapper))
        return true;

    std::set<Duco::MissingRinger>::const_iterator found = missingRingers.begin();
    while (found != missingRingers.end())
    {
        if (found->bellNo == bellNo && found->strapper == strapper)
        {
            return found->conductor;
        }
        ++found;
    }
    return false;
}

std::wstring 
DownloadedPeal::ConductorName(const RingerDatabase& database, const Duco::DatabaseSettings& settings) const
{
    std::wstring conductorName = Peal::ConductorName(database, settings);

    std::set<Duco::MissingRinger>::const_iterator found = missingRingers.begin();
    while (found != missingRingers.end())
    {
        if (found->conductor)
        {
            if (settings.LastNameFirst())
            {
                conductorName = found->surname + L", " + found->forename;
            }
            else
            {
                conductorName = found->forename + L" " + found->surname;
            }
        }
        ++found;
    }

    return conductorName;
}

void
DownloadedPeal::SetRingerId(const ObjectId& newRingerId, unsigned int newBellNumber, bool asStrapper)
{
    Peal::SetRingerId(newRingerId, newBellNumber, asStrapper);
    if (newRingerId.ValidId())
    {
        std::set<Duco::MissingRinger>::iterator it = missingRingers.begin();
        while (it != missingRingers.end())
        {
            if (it->bellNo == newBellNumber && it->strapper == asStrapper)
            {
                if (it->conductor)
                {
                    SetConductorId(newRingerId);
                }
                missingRingers.erase(it);
                break;
            }
            ++it;
        }
    }
}

void
DownloadedPeal::Clear()
{
    Peal::Clear();
    missingAssociation.clear();
    missingTower.clear();
    missingMethod.clear();
    missingTenorWeight.clear();
    missingTenorKey.clear();
    missingRingers.clear();
}
