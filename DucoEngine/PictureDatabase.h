#ifndef __PICTUREDATABASE_H__
#define __PICTUREDATABASE_H__

#include "RingingObjectDatabase.h"

#include <string>
#include <map>

namespace Duco
{
    class Picture;
    class RenumberProgressCallback;

class PictureDatabase: public RingingObjectDatabase
{
public:
    PictureDatabase();
    ~PictureDatabase();

    // from RingingObjectDatabase
    DllExport virtual Duco::ObjectId AddObject(const Duco::RingingObject& newObject);
    virtual bool Internalise(DatabaseReader& reader, Duco::ImportExportProgressCallback* newCallback, int databaseVersionNumber, size_t noOfObjectsInThisDatabase, size_t totalObjects, size_t objectsProcessedSoFar);
    virtual bool RebuildObjects(Duco::RenumberProgressCallback* callback, Duco::RingingDatabase& database);
    virtual bool MatchObject(const Duco::RingingObject& object, const Duco::TSearchArgument& searchArg, const Duco::RingingDatabase& database) const;
    virtual bool ValidateObject(const Duco::RingingObject& object, const Duco::TSearchValidationArgument& searchArg, const std::set<Duco::ObjectId>& idsToCheckAgainst, const Duco::RingingDatabase& database) const;
    virtual void ExternaliseToXml(Duco::ImportExportProgressCallback*, XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument&, bool) const;
    virtual bool UpdateObjectField(const Duco::ObjectId& id, const Duco::TUpdateArgument& updateArgument);

    // new functions
    DllExport virtual bool DeletePictures(const std::set<Duco::ObjectId>& unusedPictureIds);
    DllExport const Duco::Picture* const FindPicture(const ObjectId& pictureId, bool setIndex = false) const;
    DllExport unsigned long long TotalSizeOfAllPictures() const;

protected:
    // from RingingObjectDatabase
    bool SaveUpdatedObject(Duco::RingingObject& lhs, const Duco::RingingObject& rhs);

    void CreateNewPictureIds(std::map<Duco::ObjectId, Duco::ObjectId>& newObjectIds, const std::map<Duco::ObjectId, Duco::ObjectId>& pictureIdsByPeal) const;
};

}

#endif //!__PICTUREDATABASE_H__
