#ifndef __DOVEOBJECT_H__
#define __DOVEOBJECT_H__

#include <map>
#include <string>
#include "DucoTypes.h"
#include "DucoEngineCommon.h"
#include "ObjectId.h"

namespace Duco
{
    class DoveFileConfiguration;

class DoveObject
{
public:
    DllExport DoveObject();
    DllExport DoveObject(const std::wstring& doveLine, const Duco::DoveFileConfiguration& fileConfig);
    DllExport ~DoveObject();

    DllExport bool Valid() const;
    DllExport void Clear();
	inline bool UKorIreland() const;

    DllExport std::wstring FullName() const;
    DllExport std::wstring FullNameAndDescription() const;

    inline void SetDedication(const std::wstring& newDedication);
    inline void SetCounty(const std::wstring& newDedication);

    inline const std::wstring& Dedication() const;
    inline const std::wstring& Name() const;
    inline const std::wstring& AltName() const;
    inline const std::wstring& City() const;
    inline const std::wstring& County() const;
    inline const std::wstring& Country() const;
    inline const std::wstring& Tenor() const;
    inline const std::wstring& AltTenor() const;
    inline const std::wstring& TenorKey() const;
    inline int TenorWeight() const;
    inline unsigned int Bells() const;
    inline const std::wstring& DoveId() const;
    inline const std::wstring& RingId() const;
    inline const std::wstring& OldDoveId() const;
    inline const std::wstring& TowerbaseId() const;
    inline double Latitude() const;
    inline double Longitude() const;
    inline bool Unringable() const;
    inline bool GroundFloor() const;
    inline bool AntiClockwise() const;
    inline const std::wstring& WebPage() const;
    bool HasSemiTones() const;
    unsigned int NumberOfSemiTones() const;
    const std::map<unsigned int, Duco::TRenameBellType>& SemiToneBells() const;

    inline bool TenorApprox() const;

    bool operator<(const Duco::DoveObject& rhs) const;

protected:
    void SetTenorWeight(bool approx);
    void SetTenorWeight(std::wstring& tenorStr, bool approx) const;
    void SetTowerBaseId(const std::wstring newTowerbaseId);
    void GetSemiTones(const std::wstring& semiTones);

private:
    std::wstring     doveId;
    std::wstring     oldDoveId;
    std::wstring     ringId;
    std::wstring     towerbaseId;

    int              tenorWeightLbls;
    bool             tenorApprox;
    bool             unringable;
    bool             unitedKingdomOrIreland;
    bool             groundfloor;
    bool             antiClockwise;

    std::wstring    dedication;
    std::wstring    name;
    std::wstring    altName;
    std::wstring    city;
    std::wstring    county;
    std::wstring    country;
    std::wstring    tenor;
    std::wstring    altTenor;
    std::wstring    tenorKey;
    std::wstring    webpage;

    std::map<unsigned int, Duco::TRenameBellType> semiToneBells;
    unsigned int    noOfBells;
    double          latitude;
    double          longitude;
};

const std::wstring&
DoveObject::DoveId() const
{
    return doveId;
}

const std::wstring&
DoveObject::RingId() const
{
    return ringId;
}

const std::wstring&
DoveObject::OldDoveId() const
{
    return oldDoveId;
}

const std::wstring&
DoveObject::TowerbaseId() const
{
    return towerbaseId;
}

const std::wstring&
DoveObject::Name() const
{
    return name;
}

const std::wstring&
DoveObject::AltName() const
{
    return altName;
}

double
DoveObject::Latitude() const
{
    return latitude;
}

double
DoveObject::Longitude() const
{
    return longitude;
}

const std::wstring&
DoveObject::Dedication() const
{
    return dedication;
}

const std::wstring&
DoveObject::City() const
{
    return city;
}

const std::wstring&
DoveObject::County() const
{
    return county;
}

const std::wstring&
DoveObject::Country() const
{
    return country;
}

unsigned int
DoveObject::Bells() const
{
    return noOfBells;
}

const std::wstring&
DoveObject::TenorKey() const
{
    return tenorKey;
}

const std::wstring&
DoveObject::Tenor() const
{
    return tenor;
}

const std::wstring&
DoveObject::AltTenor() const
{
    return altTenor;
}

int
DoveObject::TenorWeight() const
{
    return tenorWeightLbls;
}

bool
DoveObject::TenorApprox() const
{
    return tenorApprox;
}

void
DoveObject::SetDedication(const std::wstring& newDedication)
{
    dedication = newDedication;
}
void
DoveObject::SetCounty(const std::wstring& newCounty)
{
    county = newCounty;
}

bool
DoveObject::Unringable() const
{
    return unringable;
}

bool
DoveObject::GroundFloor() const
{
    return groundfloor;
}

bool
DoveObject::AntiClockwise() const
{
    return antiClockwise;
}

const std::wstring&
DoveObject::WebPage() const
{
    return webpage;
}

bool
DoveObject::UKorIreland() const
{
    return unitedKingdomOrIreland;
}

}

#endif //!__DOVEOBJECT_H__
