﻿#include "Ring.h"

#include "DucoEngineUtils.h"
#include "DatabaseWriter.h"
#include "DatabaseReader.h"
#include <ostream>
#include <sstream>
#include "DoveObject.h"

using namespace std;
using namespace Duco;

#include "DucoXmlUtils.h"
XERCES_CPP_NAMESPACE_USE
#define VALID_TENOR_KEYS L"ABCDEFG♯#♭b"

Ring::Ring()
: RingingObject(), noOfBells(0), tenorWeight(L""), name(L""), tenorKey(L"")
{
}

Ring::Ring(DatabaseReader& reader, unsigned int databaseVersion)
: RingingObject(), noOfBells(0), tenorWeight(L""), name(L""), tenorKey(L"")
{
    Internalise(reader, databaseVersion);
}

Ring::Ring(const Duco::ObjectId& newId, unsigned int newNoOfBells, const std::wstring& newName, const std::set<unsigned int>& newBells, const std::wstring& newTenorWeight, const std::wstring& newTenorKey)
: RingingObject(newId), noOfBells(newNoOfBells), bells(newBells), name(newName)
{
    SetTenorKey(newTenorKey);
    SetTenorWeight(newTenorWeight);
}

Ring::Ring(const Ring& other)
:   RingingObject(other), noOfBells(other.noOfBells), bells(other.bells), tenorWeight(other.tenorWeight), name(other.name), tenorKey(other.tenorKey)
{
    SetTenorKey(other.tenorKey);
    SetTenorWeight(other.tenorWeight);
}

Ring::Ring(const Duco::ObjectId& newId, const Ring& other)
:   RingingObject(newId), noOfBells(other.noOfBells), bells(other.bells), tenorWeight(other.tenorWeight), name(other.name), tenorKey(other.tenorKey)
{
    SetTenorKey(other.tenorKey);
    SetTenorWeight(other.tenorWeight);
}

Ring::~Ring(void)
{
    bells.clear();
}

Duco::TObjectType
Ring::ObjectType() const
{
    return TObjectType::ERing;
}

bool
Ring::Duplicate(const Duco::RingingObject& other, const Duco::RingingDatabase&) const
{
    if (other.ObjectType() != ObjectType())
        return false;

    const Duco::Ring& otherRing = static_cast<const Duco::Ring&>(other);

    return Duplicate(otherRing);
}

bool
Ring::Duplicate(const Duco::Ring& otherRing) const
{
    if (noOfBells == otherRing.noOfBells)
    {
        if (bells.size() > 0 &&
            bells.size() == otherRing.bells.size() &&
            bells == otherRing.bells)
        {
            return true;
        }
        size_t minMatchLength = min(tenorWeight.length(), otherRing.tenorWeight.length());
        if (DucoEngineUtils::CompareTenorWeight(tenorWeight, otherRing.tenorWeight) &&
            DucoEngineUtils::CompareString(tenorKey, otherRing.tenorKey, true, L""))
        {
            return true;
        }
    }

    return false;
}

bool
Ring::Match(const Duco::Ring& otherRing) const
{
    if (noOfBells != otherRing.noOfBells)
        return false;
    if (bells != otherRing.bells)
        return false;
    if (!DucoEngineUtils::CompareString(tenorWeight, otherRing.tenorWeight, true, true))
        return false;
    if (!DucoEngineUtils::CompareString(name, otherRing.name, true, true))
        return false;
    if (!DucoEngineUtils::CompareString(tenorKey, otherRing.tenorKey, false, true))
        return false;

    return true;
}

bool
Ring::operator==(const Duco::RingingObject& other) const
{
    if (other.ObjectType() != ObjectType())
        return false;

    const Duco::Ring& otherRing = static_cast<const Duco::Ring&>(other);

    if (id != otherRing.id)
        return false;

    return Match(otherRing);
}

bool
Ring::operator!=(const Duco::RingingObject& other) const
{
    if (other.ObjectType() != ObjectType())
        return true;

    const Duco::Ring& otherRing = static_cast<const Duco::Ring&>(other);

    if (id != otherRing.id)
        return true;

    return !Match(otherRing);
}

Ring&
Ring::operator=(const Duco::Ring& otherRing)
{
    id = otherRing.id;
    noOfBells = otherRing.noOfBells;
    tenorWeight = otherRing.tenorWeight;
    tenorKey = otherRing.tenorKey;
    name = otherRing.name;
    bells = otherRing.bells;
    errorCode = otherRing.errorCode;

    return *this;
}

DatabaseWriter&
Ring::Externalise(DatabaseWriter& writer) const
{
    writer.WriteId(id);
    writer.WriteInt(noOfBells);

    writer.WriteInt(bells.size());
    set<unsigned int>::const_iterator it = bells.begin();
    while (it != bells.end())
    {
        writer.WriteInt(*it);
        ++it;
    }
    writer.WriteString(tenorWeight);
    writer.WriteString(tenorKey);
    writer.WriteString(name);
    writer.WriteString(L""); // reserved for notes

    return writer;
}

DatabaseReader&
Ring::Internalise(DatabaseReader& reader, unsigned int databaseVersion)
{
    id = reader.ReadUInt();
    noOfBells = reader.ReadUInt();

    unsigned int count = reader.ReadUInt();
    while (count > 0)
    {
        bells.insert(reader.ReadUInt());
        --count;
    }
    reader.ReadString(tenorWeight);
    if (databaseVersion >= 40)
    {
        std::wstring temp;
        reader.ReadString(temp);
        SetTenorKey(temp);
    }
    reader.ReadString(name);
    if (databaseVersion >= 3)
    {
        std::wstring notes;
        reader.ReadString(notes);
    }

    return reader;
}

std::wostream&
Ring::Print(std::wostream& stream, const RingingDatabase& /*database*/) const
{
    stream << name << " (";
    std::wstring::const_iterator it = tenorWeight.begin();
    while (it != tenorKey.end())
    {
        stream << stream.narrow(*it);
        ++it;
    }
    if (tenorKey.length() > 0)
    {
        if (tenorWeight.length() > 0)
        {
            stream << " ";
        }
        stream << "in ";
        std::wstring::const_iterator it = tenorKey.begin();
        while (it != tenorKey.end())
        {
            stream << stream.narrow(*it);
            ++it;
        }
        stream << endl;
    }
    stream << ")" << endl;
    return stream;
}

bool
Ring::RealBellNumber(unsigned int& bellNumber) const
{
    int count (1);
    std::set<unsigned int>::const_iterator it = bells.begin();
    while (it != bells.end())
    {
        if(count == bellNumber)
        {
            bellNumber = *it;
            return true;
        }
        ++count;
        ++it;
    }
    return false;
}

unsigned int
Ring::TenorBellNumber() const
{
    if (bells.size() == 0)
    {
        return 0;
    }
    return *bells.rbegin();
}

bool
Ring::ContainsBell(unsigned int realBellNumber) const
{
    std::set<unsigned int>::const_iterator it = bells.find(realBellNumber);
    if (it == bells.end())
    {
        return false;
    }
    return true;
}

bool
Ring::BellNumberInRing(unsigned int bellNumber) const
{
    std::set<unsigned int>::const_iterator it = bells.begin();
    while (it != bells.end())
    {
        if (*it == bellNumber)
        {
            return true;
        }
        ++it;
    }

    return false;
}

bool
Ring::IncludesBells(unsigned int firstBell, unsigned int secondBell) const
{
    std::set<unsigned int>::const_iterator it = bells.find(firstBell);
    if (it == bells.end())
        return false; // First bell not found

    it = bells.find(secondBell);
    if (it == bells.end())
        return false; // Second bell not found

    errorCode.set(ERing_InvalidRingWithExtraBells);
    return true; // both bells found.
}

bool
Ring::BellsMatch(const Duco::Ring& otherObject) const
{
    if (bells.size() != otherObject.bells.size())
        return false;

    bool match (true);

    std::set<unsigned int>::const_iterator it = bells.begin();
    std::set<unsigned int>::const_iterator it2 = otherObject.bells.begin();
    while (it != bells.end() && it2 != otherObject.bells.end() && match)
    {
        match = (*it) == (*it2);
        ++it;
        ++it2;
    }

    if (it != bells.end() || it2 != otherObject.bells.end())
        return false;

    return true;
}

bool
Ring::Valid(const RingingDatabase& ringingDb, bool warningFatal, bool clearBeforeStart) const
{
    if (clearBeforeStart)
    {
        ClearErrors();
    }
    if (warningFatal)
        return errorCode.none();

    return !(errorCode.to_ulong() & (ERing_InvalidRingWithExtraBells | ERing_InvalidRingNoOfBells | ERing_InvalidRingTenorWeight |
                                     ERing_MissingRingTenorKey | ERing_InvalidRingName | ERing_InvalidRingTenorKeyChars | ERing_PossibleInvalidRingBells));
}


bool
Ring::Valid(const std::map<unsigned int, Duco::TRenameBellType>& renamedBells, unsigned int maxNoOfBellsInRing, unsigned int noOfBellsInTower, bool includeWarning, bool clearBeforeStart) const
{
    if (clearBeforeStart)
    {
        ClearErrors();
    }
    unsigned int count (0);
    std::set<unsigned int>::const_iterator it = bells.begin();
    bool ringBellsValid (true);
    while (it != bells.end() && ringBellsValid)
    {
        ringBellsValid &= (*it != -1) && (*it <= noOfBellsInTower);
        ++count;
        ++it;
    }
    if (!ringBellsValid || count != noOfBells || maxNoOfBellsInRing < noOfBells)
    {
        errorCode.set(ERing_InvalidRingNoOfBells);
    }

    if (tenorWeight.length() <= 0 && includeWarning)
    {
        errorCode.set(ERing_InvalidRingTenorWeight);
    }
    if (tenorKey.length() <= 0 && includeWarning)
    {
        errorCode.set(ERing_MissingRingTenorKey);
    }
    else if (includeWarning)
    {
        if (tenorKey.find_first_not_of(VALID_TENOR_KEYS) != wstring::npos)
        {
            errorCode.set(ERing_InvalidRingTenorKeyChars);
        }
    }
    if (name.length() <= 0 && includeWarning)
    {
        errorCode.set(ERing_InvalidRingName);
    }

    if (includeWarning)
    {
        std::map<unsigned int, Duco::TRenameBellType>::const_iterator it = renamedBells.begin();
        while (it != renamedBells.end())
        {// Check for a ring that might have invalid bells.
            if (it->second == EFlat)
            {
                if (bells.find(it->first) != bells.end() && bells.find(it->first-1) != bells.end())
                {
                    errorCode.set(ERing_PossibleInvalidRingBells);
                }
            }
            else if (it->second == ESharp)
            {
                if (bells.find(it->first) != bells.end() && bells.find(it->first+1) != bells.end())
                {
                    errorCode.set(ERing_PossibleInvalidRingBells);
                }
            }
            ++it;
        }
    }

    return errorCode.none();
}

bool
Ring::ClearTenorKeyIfInvalidChars()
{
    if (tenorKey.find_first_not_of(VALID_TENOR_KEYS) != wstring::npos)
    {
        tenorKey.clear();
        return true;
    }
    return false;
}

void
Ring::ChangeBell(unsigned int bellId, bool add)
{
    std::set<unsigned int>::iterator it = bells.find(bellId);
    if (it == bells.end())
    {
        if (add)
        {
            bells.insert(it, bellId);
        }
    }
    else if (!add)
    {
        bells.erase(it);
    }
}

void
Ring::SetMaxBellCount(unsigned int maxBellCount)
{
    std::set<unsigned int>::iterator it = bells.begin();
    while (it != bells.end())
    {
        if ( (*it) > maxBellCount)
        {
            bells.erase(it);
            it = bells.begin();
        }
        else
        {
            ++it;
        }
    }
}

bool
Ring::ChangeBellsBy(int delta, unsigned int totalNoOfBells)
{
    bool allBellsValid (true);
    std::set<unsigned int> newBells;

    std::set<unsigned int>::iterator it = bells.begin();
    while (it != bells.end() && allBellsValid)
    {
        unsigned int newBell = (*it) + delta;
        if (newBell < 1 || newBell > totalNoOfBells)
        {
            allBellsValid = false;
        }
        newBells.insert(newBell);
        ++it;
    }

    if (allBellsValid)
    {
        bells.clear();
        bells = newBells;
    }
    return allBellsValid;
}

void
Ring::UpdateTenorWeightToDove(const std::wstring& newTenor, int noOfBellsInTower)
{
    std::set<unsigned int>::const_iterator it = bells.find(noOfBellsInTower);
    if (it != bells.end())
    {
        // Tenor is in this ring so the tenor can be updated to match dove.
        tenorWeight = newTenor;
    }
}

bool
Ring::UpdateTenorKeyFromDove(const Duco::DoveObject& dove, unsigned int noOfBells)
{
    if (bells.find(noOfBells) != bells.end() && tenorWeight.compare(dove.Tenor()) == 0)
    {
        if (tenorKey.length() == 0)
        {
            tenorKey = dove.TenorKey();
            return true;
        }
    }
    return false;
}

std::wstring
Ring::ErrorString(const Duco::DatabaseSettings& /*settings*/, bool /*showWarning*/) const
{
    return L"";
}

void
Ring::ExportToXml(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& parent, bool exportDucoObjects) const
{
    DucoXmlUtils::AddObjectId(parent, id);
    DucoXmlUtils::AddAttribute(parent, L"NumberOfBells", noOfBells);
    DucoXmlUtils::AddAttribute(parent, L"TenorWeight", tenorWeight);
    DucoXmlUtils::AddAttribute(parent, L"TenorKey", tenorKey);
    DucoXmlUtils::AddElement(outputFile, parent, L"Name", name);

    DOMElement* bellsElement = DucoXmlUtils::AddElement(outputFile, parent, L"Bells", L"", true);
    
    std::set<unsigned int>::const_iterator it = bells.begin();
    while (it != bells.end())
    {
        DOMElement* nextBell = DucoXmlUtils::AddElement(outputFile, *bellsElement, L"Bell", L"", true);
        DucoXmlUtils::AddAttribute(*nextBell, L"NumberInTower", *it);
        ++it;
    }
}

void
Ring::SetTenorKey(const std::wstring& newTenorKey)
{
    if (newTenorKey == L"HB")
    {
        tenorKey = newTenorKey;
        return;
    }

    tenorKey = DucoEngineUtils::ReplaceTenorKeySymbols(newTenorKey);
}


void
Ring::SetTenorWeight(const std::wstring& newTenorWeight)
{
    tenorWeight = DucoEngineUtils::RemoveInvalidCharsFromTenorWeight(newTenorWeight);
}

std::wstring
Ring::TenorDescription() const
{
    wostringstream ostr;
    ostr << tenorWeight;
    if (tenorKey.length() > 0)
    {
        ostr << " in ";
        ostr << tenorKey;
    }

    return ostr.str();
}
