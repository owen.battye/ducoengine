#include "MethodNotationDatabaseBase.h"

#include "DucoEngineUtils.h"
#include "ProgressCallback.h"
#include "Method.h"
#include "MethodDatabase.h"
#include <xercesc/sax2/Attributes.hpp>
#include <xercesc/sax2/SAX2XMLReader.hpp>
#include <xercesc/sax2/XMLReaderFactory.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/framework/LocalFileInputSource.hpp>

#include <cassert>

using namespace std;
using namespace Duco;
XERCES_CPP_NAMESPACE_USE
#define TOTAL_NUMBER_IN_KNOWN_DATABASE 18609

MethodNotationDatabaseBase::MethodNotationDatabaseBase(Duco::MethodDatabase& newDatabase, const std::string& newDatabaseFilename, ProgressCallback* const newCallback)
:   database (newDatabase), databaseFilename(newDatabaseFilename), callback(newCallback), noOfMethodsInXmlFile(), currentElement(EUnknownElement), currentStage(0),
    currentMethodType(EUnknownMethodType), errorParsing(false), stopProcessing(false)
{
    XMLPlatformUtils::Initialize();
}

MethodNotationDatabaseBase::~MethodNotationDatabaseBase()
{
    XMLPlatformUtils::Terminate();
}

void
MethodNotationDatabaseBase::ReadMethods()
{
    if (callback)
        callback->Initialised();

    ParseXml();

    if (callback)
        callback->Complete(false);
}

void
MethodNotationDatabaseBase::FixCurrentPlaceNotation()
{
    std::wstring oldPlaceNotation (currentMethodNotation);
    wstring::size_type symmetricalIndicator (oldPlaceNotation.rfind (L","));
    bool resolvePreSymetry (false);
    if (symmetricalIndicator < (currentMethodNotation.length() - symmetricalIndicator))
    {
        resolvePreSymetry = true;
    }

    if (resolvePreSymetry)
    {
        currentMethodNotation = oldPlaceNotation.substr(0, symmetricalIndicator);
        currentMethodNotation.append(L".");
    }
    else if (symmetricalIndicator != wstring::npos)
    {
        currentMethodNotation = L"&";
    }
    else
    {
        currentMethodNotation.clear();
    }
    
    std::wstring mainNotationPart (oldPlaceNotation);
    std::wstring otherNotationPart (L"");
    if (symmetricalIndicator != string::npos)
    {
        size_t firstHalfLength (symmetricalIndicator);
        size_t secondHalfLength (oldPlaceNotation.length() - symmetricalIndicator - 1);

        if (secondHalfLength > firstHalfLength)
        {
            mainNotationPart = oldPlaceNotation.substr(symmetricalIndicator+1);
            otherNotationPart = oldPlaceNotation.substr(0, firstHalfLength);
        }
    }

    for (size_t i (0); i < mainNotationPart.length(); ++i)
    {
        wchar_t nextChar = mainNotationPart[i];
        if (nextChar == '-')
        {
            currentMethodNotation.append(L"x");
        }
        else if (nextChar == ',')
        {
            currentMethodNotation.append(L"-");
        }
        else
        {
            currentMethodNotation += nextChar;
        }
    }

    if (resolvePreSymetry)
    { // need to reverse the place notation and add it on again.
        currentMethodNotation += ReverseNotation(mainNotationPart);
    }
    else if (otherNotationPart.length() > 0)
    {
        currentMethodNotation += L"-";
        wstring::size_type nextDashChar (otherNotationPart.find(L"-"));
        while (nextDashChar != string::npos)
        {
            otherNotationPart = otherNotationPart.replace(nextDashChar, 1, L"x");
            nextDashChar = otherNotationPart.find(L"-");
        }
        currentMethodNotation += otherNotationPart;
    }
}

std::wstring
MethodNotationDatabaseBase::ReverseNotation(const std::wstring& originalNotation) const
{
    std::list<std::wstring> notations;

    unsigned int lengthOfPreviousNotation = 0;
    for (unsigned int position = 0; position <= originalNotation.length(); ++position)
    {
        wchar_t nextChar = originalNotation[position];
        if (nextChar == 'X' || nextChar == 'x')
        {
            notations.push_back(L"x");
            lengthOfPreviousNotation = 0;
        }
        else if (nextChar == '.')
        {
            std::wstring nextNotation = originalNotation.substr(position-lengthOfPreviousNotation,lengthOfPreviousNotation);
            notations.push_back(nextNotation);
            lengthOfPreviousNotation = 0;
        }
        else if (nextChar == '\0')
        {
            std::wstring nextNotation = originalNotation.substr(position-lengthOfPreviousNotation,lengthOfPreviousNotation);
            notations.push_back(nextNotation);
        }
        else
        {
            ++lengthOfPreviousNotation;
        }
    }

    std::wstring reversedNotation;

    std::list<std::wstring>::const_reverse_iterator it = notations.rbegin();
    if (it != notations.rend())
    {
        bool lastNotationRequiredSeperator = RequiresSeperator(*it);
        ++it; // ignore last notation
        while (it != notations.rend())
        {
            bool thisNotationRequiredSeperator = RequiresSeperator(*it);
            if (thisNotationRequiredSeperator && lastNotationRequiredSeperator)
            {
                reversedNotation.append(L".");
            }
            reversedNotation.append(*it);
            lastNotationRequiredSeperator = thisNotationRequiredSeperator;
            ++it;
        }
    }

    return reversedNotation;
}

bool
MethodNotationDatabaseBase::RequiresSeperator(const std::wstring& notation) const
{
    if (wcscmp(notation.c_str(), L"x") == 0 || wcscmp(notation.c_str(), L"X") == 0)
        return false;

    return true;
}

void
MethodNotationDatabaseBase::ParseXml()
{
    stopProcessing = false;
    SAX2XMLReader* parser = XMLReaderFactory::createXMLReader();
    parser->setFeature(XMLUni::fgXercesSchema, false);
    parser->setFeature(XMLUni::fgXercesLoadExternalDTD, false);
    parser->setContentHandler(this);
    parser->setErrorHandler(this);

    XMLCh* fileName = XMLString::transcode(databaseFilename.c_str());
    LocalFileInputSource* inputFile = new LocalFileInputSource(fileName);

    try
    {
        XMLPScanToken nextToken;
        if (parser->parseFirst(*inputFile, nextToken))
        {
            while (!stopProcessing && parser->parseNext(nextToken))
            {

            }
        }
    }
    catch (...)
    {
        errorParsing = true;
    }

    delete parser;
    delete inputFile;
    XMLString::release(&fileName);
}

void
MethodNotationDatabaseBase::characters(const XMLCh *const chars, const XMLSize_t length)
{
    switch (currentElement)
    {
    case EStageElement:
        XMLString::textToBin(chars, currentStage);
        break;

    case ETitleElement:
        SetString(currentMethodTitle, chars);
        break;

    case ENameElement:
        SetString(currentMethodName, chars);
        break;

    case ETypeElement:
        {
        SetMethodType(chars);
        }
        break;

    case ENotationElement:
        SetString(currentMethodNotation, chars);
        break;

    default:
        break;
    }
}

void
MethodNotationDatabaseBase::startDocument()
{
    currentStage = 0;
    currentElement = EUnknownElement;
    noOfMethodsInXmlFile = 0;
}

void 
MethodNotationDatabaseBase::endDocument()
{

}

void
MethodNotationDatabaseBase::ignorableWhitespace(const XMLCh *const chars, const XMLSize_t length)
{

}

void
MethodNotationDatabaseBase::processingInstruction(const XMLCh *const target, const XMLCh *const data)
{

}

void
MethodNotationDatabaseBase::setDocumentLocator(const Locator* const locator)
{

}

void
MethodNotationDatabaseBase::startElement(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname, const Attributes& attrs)
{
    currentElement = ElementType(localname);
    switch (currentElement)
    {
        case EMethodElement:
        {
            XMLCh* idName = XMLString::transcode("id");
            XMLSize_t idIndex = attrs.getIndex(idName);
            XMLString::release(&idName);
            currentMethodId = KNoId;
            if (idIndex != -1)
            {
                const XMLCh* methodIdStr = attrs.getValue(idIndex);
                XMLCh* methodIdStrCopy = XMLString::replicate(methodIdStr);
                XMLString::cut(methodIdStrCopy, 2);
                unsigned int newMethodId;
                XMLString::textToBin(methodIdStrCopy, newMethodId);
                currentMethodId = newMethodId;
                XMLString::release(&methodIdStrCopy);
            }
        }
        break;
        
        case ETypeElement:
        {
            FindAttribute(attrs, ELittleAttribute, L"little");
            FindAttribute(attrs, EDifferentialAttribute, L"differential");
            FindAttribute(attrs, EPlainAttribute, L"plain");
            FindAttribute(attrs, ETrebleDodgingAttribute, L"trebleDodging");
        }
        break;

        default:
        break;
    }
}

bool
MethodNotationDatabaseBase::FindAttribute(const XERCES_CPP_NAMESPACE_QUALIFIER Attributes& attrs, const TMethodLibraryAttributeType type, const wchar_t* attributeName)
{
    bool returnVal (false);
    XMLSize_t attributeIndex = attrs.getIndex(attributeName);
    if (attributeIndex != -1)
    {
        const XMLCh* attributeStr = attrs.getValue(attributeIndex);
        if (XMLString::equals(attributeStr, L"true"))
        {
            currentMethodTypeAttributeType.set(type, 1);
            returnVal = true;
        }
    }
    return returnVal;
}

void
MethodNotationDatabaseBase::endElement(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname)
{
    switch (ElementType(localname))
    {
    case EMethodElement:
        {
            ++noOfMethodsInXmlFile;
            if (currentMethodId.ValidId() && currentStage > 0 && (currentMethodName.length() > 0 || currentMethodTitle.length() > 0))
            {
                if (currentMethodNotation.length() > 0)
                {
                    FixCurrentPlaceNotation();
                }
                AddMethod(currentMethodId, currentStage, currentMethodName, GetMethodType(), currentMethodTitle, currentMethodNotation);
                if (callback)
                {
                    callback->Step(int((double(noOfMethodsInXmlFile) / double(TOTAL_NUMBER_IN_KNOWN_DATABASE)) * 100));
                }
            }
        }
        currentMethodName.clear();
        currentMethodTitle.clear();
        currentMethodNotation.clear();
        currentMethodId = 0;
        break;

    case EMethodSetElement:
        currentMethodName.clear();
        currentMethodType = EUnknownMethodType;
        currentMethodTitle.clear();
        currentMethodNotation.clear();
        currentMethodTypeAttributeType.reset();
        currentStage = 0;
        currentMethodId = 0;

    default:
        currentElement = EUnknownElement;
        break;
    }
}

void
MethodNotationDatabaseBase::startPrefixMapping(const XMLCh *const prefix, const XMLCh *const uri)
{

}

void
MethodNotationDatabaseBase::endPrefixMapping(const XMLCh *const prefix)
{

}

void
MethodNotationDatabaseBase::skippedEntity(const XMLCh *const name)
{

}

Duco::TMethodLibraryElementType
MethodNotationDatabaseBase::ElementType(const XMLCh *const elementName) const
{
    char* elementNameStr = XMLString::transcode(elementName);
    Duco::TMethodLibraryElementType returnType (EUnknownElement);

    if (strcmp(elementNameStr, "stage") == 0)
        returnType = EStageElement;
    else if (strcmp(elementNameStr, "title") == 0)
        returnType = ETitleElement;
    else if (strcmp(elementNameStr, "classification") == 0)
        returnType = ETypeElement;
    else if (strcmp(elementNameStr, "name") == 0)
        returnType = ENameElement;
    else if (strcmp(elementNameStr, "notation") == 0)
        returnType = ENotationElement;
    else if (strcmp(elementNameStr, "method") == 0)
        returnType = EMethodElement;
    else if (strcmp(elementNameStr, "methodSet") == 0)
        returnType = EMethodSetElement;

    XMLString::release(&elementNameStr);
    return returnType;
}

void
MethodNotationDatabaseBase::SetString(std::wstring& theString, const XMLCh *const chars, bool clearBeforeAdd) const
{
    if (clearBeforeAdd)
    {
        theString = chars;
    }
    else
    {
        theString += chars;
    }
}

void
MethodNotationDatabaseBase::warning(const SAXParseException& exc)
{
    errorParsing = true;
}

void
MethodNotationDatabaseBase::error(const SAXParseException& exc)
{
    errorParsing = true;
}

void
MethodNotationDatabaseBase::fatalError(const SAXParseException& exc)
{
    errorParsing = true;
}

void
MethodNotationDatabaseBase::resetErrors()
{
    errorParsing = false;
}

void
MethodNotationDatabaseBase::SetMethodType(const XMLCh *const methodType)
{
    char* methodTypeStr = XMLString::transcode(methodType);
    currentMethodType = EUnknownMethodType;

    if (strcmp(methodTypeStr, "Place") == 0)
        currentMethodType = EPlaceMethodType;
    else if (strcmp(methodTypeStr, "Bob") == 0)
        currentMethodType = EBobMethodType;
    else if (strcmp(methodTypeStr, "Slow Course") == 0)
        currentMethodType = ESlowCourseMethodType;
    else if (strcmp(methodTypeStr, "Treble Bob") == 0)
        currentMethodType = ETrebleBobMethodType;
    else if (strcmp(methodTypeStr, "Delight") == 0)
        currentMethodType = EDelightMethodType;
    else if (strcmp(methodTypeStr, "Surprise") == 0)
        currentMethodType = ESurpriseMethodType;
    else if (strcmp(methodTypeStr, "Alliance") == 0)
        currentMethodType = EAllianceMethodType;
    else if (strcmp(methodTypeStr, "Treble Place") == 0)
        currentMethodType = ETreblePlaceMethodType;
    else if (strcmp(methodTypeStr, "Hybrid") == 0)
        currentMethodType = EHybridMethodType;
    else
    {
        assert(false);
        currentMethodType = EUnknownMethodType;
    }
}

std::wstring
MethodNotationDatabaseBase::GetMethodType() const
{
    std::wstring returnVal(L"");

    switch (currentMethodType)
    {
        case EPlaceMethodType:
            returnVal = L"Place";
            break;
        case EBobMethodType:
            if (DucoEngineUtils::IsEven(currentStage))
            {
                returnVal = L"Bob";
            }
            break;
        case ESlowCourseMethodType:
            returnVal = L"Slow Course";
            break;
        case ETrebleBobMethodType:
            returnVal = L"Treble Bob";
            break;
        case EDelightMethodType:
            returnVal = L"Delight";
            break;
        case ESurpriseMethodType:
            returnVal = L"Surprise";
            break;
        case EAllianceMethodType:
            returnVal = L"Alliance";
            break;
        case ETreblePlaceMethodType:
            returnVal = L"Treble Place";
            break;
        case EHybridMethodType:
            returnVal = L"Hybrid";
            break;
        default:
            break;
    }
    if (currentMethodTypeAttributeType[ELittleAttribute])
    {
        returnVal = L"Little " + returnVal;
    }
    if (currentMethodTypeAttributeType[EDifferentialAttribute])
    {
        returnVal = L"Differential " + returnVal;
    }
    if (currentMethodTypeAttributeType[EPlainAttribute])
    {
        //returnVal = "Plain " + returnVal;
    }
    if (currentMethodTypeAttributeType[ETrebleDodgingAttribute] &&
            !(currentMethodType == ETrebleBobMethodType ||
              currentMethodType == EDelightMethodType ||
              currentMethodType == ESurpriseMethodType) )
    {
        returnVal = L"Treble Bob " + returnVal;
    }

    return returnVal;
}
