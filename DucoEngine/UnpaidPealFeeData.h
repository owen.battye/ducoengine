#ifndef __UNPAIDPEALFEEDATA_H__
#define __UNPAIDPEALFEEDATA_H__

#include "ObjectId.h"
#include "DucoEngineCommon.h"

namespace Duco
{
class UnpaidPealFeeData
{
public:
    DllExport UnpaidPealFeeData(const Duco::ObjectId& newPealId, const Duco::ObjectId& newFeePayerId, unsigned int paidInPence, unsigned int remainingInPence);

    inline const Duco::ObjectId& PealId() const;
    inline const Duco::ObjectId& FeePayerId() const;
    inline unsigned int AmountPaidInPence() const;
    inline unsigned int AmountUnPaidInPence() const;

    bool operator<(const Duco::UnpaidPealFeeData& rhs) const;

protected:
    const Duco::ObjectId pealId;
    const Duco::ObjectId feePayersRingerId;
    unsigned int paidInPence;
    unsigned int remainingInPence;
};

const Duco::ObjectId&
UnpaidPealFeeData::PealId() const
{
    return pealId;
}

const Duco::ObjectId&
UnpaidPealFeeData::FeePayerId() const
{
    return feePayersRingerId;
}

unsigned int
UnpaidPealFeeData::AmountPaidInPence() const
{
    return paidInPence;
}

unsigned int
UnpaidPealFeeData::AmountUnPaidInPence() const
{
    return remainingInPence;
}

}

#endif //__UNPAIDPEALFEEDATA_H__
