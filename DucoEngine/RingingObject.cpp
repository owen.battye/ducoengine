#include "RingingObject.h"
#include <cassert>

using namespace Duco;

RingingObject::RingingObject(const RingingObject& other)
: id(other.id), errorCode(other.errorCode)
{

}

RingingObject::RingingObject(const ObjectId& newId)
: id(newId)
{
    ClearErrors();
}


RingingObject::RingingObject()
{
    id.ClearId();
    ClearErrors();
}

RingingObject::~RingingObject()
{
}

std::wstring
RingingObject::FullNameForSort(bool unusedSetting) const
{
    assert(false);
    return L"";
}

bool
RingingObject::RebuildRecommended() const
{
    return false;
}

void
RingingObject::ClearErrors() const
{
    errorCode.reset();
}

const Duco::ObjectId&
RingingObject::Id() const
{
    return id;
}

void
RingingObject::SetId(const Duco::ObjectId& newId)
{
    id = newId;
}

const std::bitset<KMaxErrorBitsetSize>
RingingObject::ErrorCode() const
{
    return errorCode;
}