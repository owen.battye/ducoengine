#ifndef __CHANGE_H__
#define __CHANGE_H__

#include <string>
#include <set>
#include <vector>
#include "DucoEngineCommon.h"
#include "ChangeType.h"

namespace Duco
{
class Change
{
    friend class MusicGroup;
    friend class MusicObject;

public:
    DllExport Change(unsigned int noOfBells);
    DllExport Change(const std::wstring& change);
    DllExport Change(const std::wstring& change, unsigned int noOfBells);
    DllExport Change(const Change& other);
    DllExport virtual ~Change();
    DllExport Duco::Change* Realloc() const;
    DllExport void Reset();

    // Accessors
    DllExport std::wstring Str(bool all = false, bool showTreble = false) const;
    DllExport bool Position(unsigned int bellNumber, unsigned int& position) const;
    DllExport bool TreblePosition(unsigned int& position) const;
    DllExport unsigned int CallingPosition() const;
    inline size_t Order() const;
    DllExport bool Bell(unsigned int position, unsigned int& bellNumber) const;
    DllExport void Bells(std::set<unsigned int>& bells) const;
    DllExport bool Differences(const Duco::Change& other, std::vector<unsigned int>& affectedBells) const;
    static bool DifferencesFromRounds(const Duco::Change& other, std::vector<unsigned int>& affectedBells);

    DllExport bool IsBefore() const;
    DllExport bool IsHome() const;
    inline    bool IsLeadEnd() const;
    inline    bool IsLeadHead() const;
    DllExport bool IsRounds() const;

    // Settors
    inline void SetChangeType(TChangeType newChangeType);

    // Music
    DllExport void SetContainsRollup(unsigned int betweenStart, unsigned int betweenEnd);
    DllExport void SetContainsMusic(unsigned int betweenStart, unsigned int betweenEnd);
    inline bool MusicalBell(unsigned int position) const;
    inline bool Rollup() const;

    // Modifiers
    /* Swaps two bells over, position should be the lower of the two positions, eg 1 to swap 1 & 2.*/
    DllExport bool SwapPair(unsigned int position);

    // Operators
    DllExport Duco::Change& operator=(const Duco::Change& other);
    DllExport bool operator<(const Duco::Change& other) const;
    DllExport bool operator==(const Duco::Change& other) const;
    DllExport bool operator!=(const Duco::Change& other) const;
    DllExport unsigned int operator[](unsigned int position) const;

protected:
    void GenerateRounds();

protected:
    size_t numberOfBells;
    std::vector<unsigned int> row;

    TChangeType changeType;
    bool rollup;
    unsigned int startingPosOfMusic;
    unsigned int endingPosOfMusic;
};

size_t
Change::Order() const
{
    return numberOfBells;
}

bool
Change::IsLeadEnd() const
{
    return changeType == ELeadEnd;
}

bool
Change::IsLeadHead() const
{
    return changeType == ELeadHead;
}

void
Change::SetChangeType(TChangeType newChangeType)
{
    changeType = newChangeType;
}

bool
Change::MusicalBell(unsigned int position) const
{
    return position >= startingPosOfMusic && position <= endingPosOfMusic;
}

bool
Change::Rollup() const
{
    return rollup;
}


}

#endif //!__CHANGE_H__
