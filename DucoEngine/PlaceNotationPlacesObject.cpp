#include "PlaceNotationPlacesObject.h"

using namespace std;
using namespace Duco;

#include "DucoEngineUtils.h"
#include "Change.h"

PlaceNotationPlacesObject::PlaceNotationPlacesObject(const std::wstring& newStringRepresentation, TChangeType newType)
:    PlaceNotationObject(newStringRepresentation, newType)
{
    std::wstring::const_iterator it = newStringRepresentation.begin();
    while (it != newStringRepresentation.end())
    {
        wchar_t nextChar = *it;
        places.insert(DucoEngineUtils::ToInteger(nextChar));
        ++it;
    }
}

PlaceNotationPlacesObject::~PlaceNotationPlacesObject()
{
}

PlaceNotationPlacesObject::PlaceNotationPlacesObject(const PlaceNotationPlacesObject& other)
    : PlaceNotationObject(other), places(other.places)
{

}

PlaceNotationObject*
PlaceNotationPlacesObject::Realloc() const
{
    PlaceNotationPlacesObject* newObj = new PlaceNotationPlacesObject(*this);
    return newObj;
}

bool
PlaceNotationPlacesObject::RequiresSeperator() const
{
    return true;
}

Change
PlaceNotationPlacesObject::ProcessRow(const Change& change) const
{
    Change nextChange(change);
    bool previousNumberOKToSwap (false);
    for (unsigned int i(1); i <= change.Order(); ++i)
    {
        bool thisNumberOkToSwap (places.find(i) == places.end());
        if (thisNumberOkToSwap && previousNumberOKToSwap)
        {
            previousNumberOKToSwap = false;
            nextChange.SwapPair(i-1);
        }
        else
            previousNumberOKToSwap = thisNumberOkToSwap;
    }
    nextChange.SetChangeType(changeType);
    return nextChange;
}

unsigned int
PlaceNotationPlacesObject::BestStartingLead() const
{
    const wchar_t lastChar = *stringRepresentation.rbegin();
    return DucoEngineUtils::ToInteger(lastChar);
}
