#ifndef __SEARCHGENDERARGUMENT_H__
#define __SEARCHGENDERARGUMENT_H__

#include "SearchFieldBoolArgument.h"
#include "DucoEngineCommon.h"

namespace Duco
{
    class Peal;
    class Tower;
    class Ringer;
    class Method;

class TSearchGenderArgument : public TSearchFieldBoolArgument
{
public:
    DllExport explicit TSearchGenderArgument(bool newMale, bool newFemale);
    bool Match(const Duco::Peal& peal, const Duco::RingingDatabase& database) const;
    bool Match(const Duco::Tower& tower, const Duco::RingingDatabase& database) const;
    bool Match(const Duco::Ringer& ringer, const Duco::RingingDatabase& database) const;
    bool Match(const Duco::Method& method, const Duco::RingingDatabase& database) const;
    bool Match(const Duco::Composition& object, const Duco::RingingDatabase& database) const;
    ~TSearchGenderArgument();
 
protected:
    const bool         female;
    //fieldValue in base class used for male.
};

}

#endif //!__SEARCHFIELDBOOLARGUMENT_H__
