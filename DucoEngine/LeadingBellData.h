#ifndef _LEADINGBELLDATA_H__
#define _LEADINGBELLDATA_H__
#include "ObjectId.h"

namespace Duco
{
    class LeadingBellData
    {
    public:
        DllExport explicit LeadingBellData(const Duco::ObjectId& towerId, size_t bellNumber, size_t pealCount);
        DllExport virtual ~LeadingBellData();

        DllExport const Duco::ObjectId& TowerId() const;
        DllExport size_t PealCount() const;
        DllExport size_t BellNumber() const;

        DllExport bool operator<(const Duco::LeadingBellData& rhs) const;

    private:
        Duco::ObjectId towerId;
        size_t bellNumber;
        size_t pealCount;
    };
}

#endif //! _LEADINGBELLDATA_H__