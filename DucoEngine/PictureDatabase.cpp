#include "PictureDatabase.h"

#include "DatabaseSettings.h"
#include "DucoEngineUtils.h"
#include "ImportExportProgressCallback.h"
#include "PealDatabase.h"
#include "TowerDatabase.h"
#include "Picture.h"
#include "RenumberProgressCallback.h"
#include "RingingDatabase.h"
#include "DucoEngineLog.h"
#include "SearchArgument.h"
#include "SearchValidationArgument.h"

#include <fstream>

using namespace Duco;
using namespace std;

PictureDatabase::PictureDatabase()
:   RingingObjectDatabase(TObjectType::EPicture)
{
}

PictureDatabase::~PictureDatabase()
{

}

Duco::ObjectId
PictureDatabase::AddObject(const Duco::RingingObject& newObject)
{
    Duco::ObjectId addedId;

    if (newObject.ObjectType() == TObjectType::EPicture)
    {
        const Duco::Picture& newPictureObject = static_cast<const Duco::Picture&>(newObject);

        Duco::RingingObject* newObjectWithId = new Duco::Picture(newPictureObject);
        if (AddOwnedObject(newObjectWithId))
        {
            addedId = newObjectWithId->Id();
        }
        // object is deleted by RingingObjectDatabase::AddObject if not added.
    }
    return addedId;
}

bool
PictureDatabase::SaveUpdatedObject(Duco::RingingObject& lhs, const Duco::RingingObject& rhs)
{
    if (lhs.ObjectType() == rhs.ObjectType() && rhs.ObjectType() == TObjectType::EPicture)
    {
        Duco::Picture& lhsObject = static_cast<Duco::Picture&>(lhs);
        const Duco::Picture& rhsObject = static_cast<const Duco::Picture&>(rhs);
        lhsObject = rhsObject;
        return true;
    }
    return false;
}

bool
PictureDatabase::Internalise(DatabaseReader& reader, Duco::ImportExportProgressCallback* callback, int databaseVersionNumber, size_t noOfObjectsInThisDatabase, size_t totalObjects, size_t objectsProcessedSoFar)
{
    size_t totalProcessed = objectsProcessedSoFar;
    size_t count = 0;

    while (noOfObjectsInThisDatabase-- > 0)
    {
        ++totalProcessed;
        ++count;
        float percentageComplete = ((float)totalProcessed / (float)totalObjects) * 100;
        Duco::RingingObject* newObject = new Duco::Picture(reader, databaseVersionNumber);
        if (newObject == NULL)
            return false;

        if (AddOwnedObject(newObject))
        {
            if (callback != NULL)
            {
                callback->ObjectProcessed(true, TObjectType::EPicture, newObject->Id(), (int)percentageComplete, count);
            }
        }
        else
        {
            DUCOENGINELOG(L"Error reading picture from database");
            if (callback != NULL)
            {
                callback->ImportExportFailed(true, 0);
            }
            return false;
        }
    }
    return true;
}

bool
PictureDatabase::RebuildObjects(Duco::RenumberProgressCallback* callback, Duco::RingingDatabase& database)
{
    bool changesMade = false;
    if (listOfObjects.size() <= 0)
        return false;

    callback->RenumberInitialised(RenumberProgressCallback::EReindexPictures);
    std::map<Duco::ObjectId, Duco::ObjectId> newObjectIds;
    {
        std::map<Duco::ObjectId, Duco::ObjectId> pictureIdsByPeal;
        database.PealsDatabase().GetPictureIds(pictureIdsByPeal);
        CreateNewPictureIds(newObjectIds, pictureIdsByPeal);
    }

    callback->RenumberInitialised(RenumberProgressCallback::ERenumberPictures);
    // Renumber all pictures first.
    changesMade |= RenumberObjects(newObjectIds, callback);

    callback->RenumberInitialised(RenumberProgressCallback::ERebuildPictures);
    // Renumber pictureids in Peals
    changesMade |= database.PealsDatabase().RenumberPicturesInPeals(newObjectIds, callback);
    changesMade |= database.TowersDatabase().RenumberPicturesInTowers(newObjectIds, callback);

    newObjectIds.clear();
    return changesMade;
}

bool
PictureDatabase::MatchObject(const Duco::RingingObject& object, const Duco::TSearchArgument& searchArg, const Duco::RingingDatabase& database) const
{
    if (object.ObjectType() != TObjectType::EPicture)
        return false;
    return searchArg.Match(static_cast<const Duco::Picture&>(object), database);
}

bool
PictureDatabase::ValidateObject(const Duco::RingingObject& object, const Duco::TSearchValidationArgument& searchArg, const std::set<Duco::ObjectId>& idsToCheckAgainst, const Duco::RingingDatabase& database) const
{
    if (object.ObjectType() != TObjectType::EPicture)
        return false;
    return searchArg.Match(static_cast<const Duco::Picture&>(object), idsToCheckAgainst, database);
}

bool
PictureDatabase::DeletePictures(const std::set<Duco::ObjectId>& unusedPictureIds)
{
    bool allDeleted = true;
    std::set<Duco::ObjectId>::const_iterator it = unusedPictureIds.begin();
    while (it != unusedPictureIds.end())
    {
        allDeleted &= DeleteObject(*it);
        ++it;
    }
    return allDeleted;
}

void
PictureDatabase::CreateNewPictureIds(std::map<Duco::ObjectId, Duco::ObjectId>& newObjectIds, const std::map<Duco::ObjectId, Duco::ObjectId>& pictureIdsByPeal) const
{
    newObjectIds.clear();
    size_t nextId (1);
    std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator it = pictureIdsByPeal.begin();
    while (it != pictureIdsByPeal.end())
    {
        std::pair<Duco::ObjectId, Duco::ObjectId> oldToNewId (it->second, nextId);
        if (newObjectIds.insert(oldToNewId).second)
        {
            ++nextId;
        }
        ++it;
    }

    DUCO_OBJECTS_CONTAINER::const_iterator pictureIt = listOfObjects.begin();
    while (newObjectIds.size() < this->NumberOfObjects() && pictureIt != listOfObjects.end())
    {
        const Duco::Picture* thePicture = static_cast<const Duco::Picture*>(pictureIt->second);
        std::map<Duco::ObjectId, Duco::ObjectId>::const_iterator pictureRenamePresent = newObjectIds.find(thePicture->Id());
        if (pictureRenamePresent == newObjectIds.end())
        {
            std::pair<Duco::ObjectId, Duco::ObjectId> oldToNewId (thePicture->Id(), nextId);
            if (newObjectIds.insert(oldToNewId).second)
            {
                ++nextId;
            }
        }
        ++pictureIt;
    }
}

#ifndef __ANDROID__
void
PictureDatabase::ExternaliseToXml(Duco::ImportExportProgressCallback* /*newCallback*/, XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& /*outputFile*/, bool /*exportDucoObjects*/) const
{

}
#endif

const Picture* const
PictureDatabase::FindPicture(const ObjectId& pictureId, bool setIndex) const
{
    const RingingObject* const object = FindObject(pictureId, setIndex);
    return static_cast<const Picture* const>(object);
}

bool
PictureDatabase::UpdateObjectField(const Duco::ObjectId&, const Duco::TUpdateArgument&)
{
    return false;
}

unsigned long long
PictureDatabase::TotalSizeOfAllPictures() const
{
    unsigned long long totalPictureSize = 0;
    DUCO_OBJECTS_CONTAINER::const_iterator pictureIt = listOfObjects.begin();
    while (pictureIt != listOfObjects.end())
    {
        const Duco::Picture* thePicture = static_cast<const Duco::Picture*>(pictureIt->second);
        totalPictureSize += thePicture->PictureSize();
        ++pictureIt;
    }

    return totalPictureSize;
}
