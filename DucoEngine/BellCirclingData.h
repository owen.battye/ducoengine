#ifndef __BELLCIRCLINGDATA_H__
#define __BELLCIRCLINGDATA_H__

#include "PerformanceDate.h"
#include <vector>

namespace Duco
{
    class BellCirclingData
    {
    public:
        BellCirclingData() = delete;
        DllExport explicit BellCirclingData(const Duco::PerformanceDate& pealDate);
        DllExport ~BellCirclingData();
        void AddPeal(const Duco::PerformanceDate& pealDate);

        DllExport Duco::PerformanceDate FirstPealDate() const;
        DllExport Duco::PerformanceDate LastPealDate() const;
        DllExport Duco::PerformanceDate PealDate(size_t pealNumber) const;
        size_t PealCount() const;

    private:
        std::vector<Duco::PerformanceDate> pealCounts;
    };
}

#endif //__BELLCIRCLINGDATA_H__