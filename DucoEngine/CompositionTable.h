#ifndef __COMPOSITIONTABLE_H__
#define __COMPOSITIONTABLE_H__

#include "DucoEngineCommon.h"
#include <deque>
#include "Course.h"
#include "LeadOrder.h"

namespace Duco
{
    class Calling;

class CompositionTable
{
    friend class Composition;

public:
    DllExport virtual ~CompositionTable();
    DllExport size_t NoOfCourses() const;
    DllExport const Duco::Course& Course(size_t index) const;
    DllExport const Duco::LeadOrder& LeadOrder() const;

protected:
    CompositionTable(const Duco::LeadOrder& newLeadOrder);
    bool AddCall(const Duco::Calling& nextCall, const Duco::PlaceNotation& notation);
    void CompletedGeneration();

private:
    std::deque<Duco::Course> courses;
    Duco::LeadOrder leadOrder;
    unsigned int lastBell;
};

}

#endif //!__COMPOSITIONTABLE_H__

