#ifndef __SEARCHVALIDATIONGROUP_H__
#define __SEARCHVALIDATIONGROUP_H__

#include "SearchValidationArgument.h"
#include "DucoEngineCommon.h"

#include <vector>
#include <cstddef>

namespace Duco
{
class TSearchValidationGroup : public TSearchValidationArgument
{
public:
    DllExport explicit TSearchValidationGroup(Duco::TSearchFieldId fieldId);
    DllExport ~TSearchValidationGroup();
    Duco::TFieldType FieldType() const;

    virtual bool Match(const Duco::Peal& peal, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Tower& tower, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Ringer& ringer, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Method& method, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::MethodSeries& series, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Composition& object, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Association& object, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Picture& object, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const;

    DllExport size_t AddArgument(TSearchValidationArgument* newArgument);
    virtual std::set<Duco::ObjectId> ErrorIds() const;
    virtual void Reset();
 
protected:
    std::vector<TSearchValidationArgument*> searchArguments;
};

}

#endif //__SEARCHVALIDATIONGROUP_H__

