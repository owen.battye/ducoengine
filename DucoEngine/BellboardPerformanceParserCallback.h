#ifndef __BELLBOARDPERFORMANCEPARSERCALLBACK_H__
#define __BELLBOARDPERFORMANCEPARSERCALLBACK_H__

#include <string>
#include <set>
#include "RingerPealDates.h"

namespace Duco
{
    class ObjectId;

    class BellboardPerformanceParserCallback
    {
    public:
        virtual void Cancelled(const std::wstring& bellboardId, const char* message) = 0;
        virtual void Completed(const std::wstring& bellboardId, bool errors) = 0;
    };

    class BellboardPerformanceParserConfirmRingerCallback
    {
    public:
        virtual Duco::ObjectId ChooseRinger(const std::wstring& missingRingerName, const std::set<Duco::RingerPealDates>& nearMatches) = 0;
    };

} // end namespace Duco


#endif // __BELLBOARDPERFORMANCEPARSERCALLBACK_H__
