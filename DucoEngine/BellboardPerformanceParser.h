#ifndef __BELLBOARDPERFORMANCEPARSER_H__
#define __BELLBOARDPERFORMANCEPARSER_H__

#include <string>
#include "DownloadedPeal.h"
#include <xercesc/sax/SAXParseException.hpp>
#include <xercesc/sax2/DefaultHandler.hpp>
#include <xercesc/sax2/SAX2XMLReader.hpp>

namespace Duco
{
    class DoveDatabase;
    class RingingDatabase;
    class MethodDatabase;
    class BellboardPerformanceParserCallback;
    class BellboardPerformanceParserConfirmRingerCallback;
    class MethodNotationDatabaseSingleImporter;

    enum TBellboardAttribute
    {
        EBBAttributeNone = 0,
        EBBAttributeId,
        EBBAttributePlace,
        EBBAttributeTowerbaseId,
        EBBAttributeDoveId,
        EBBAttributeDedication,
        EBBAttributeCounty
    };

class BellboardPerformanceParser : public XERCES_CPP_NAMESPACE_QUALIFIER DefaultHandler
{
public:
    DllExport BellboardPerformanceParser(Duco::RingingDatabase& theDatabase, BellboardPerformanceParserCallback& callback, Duco::BellboardPerformanceParserConfirmRingerCallback* ringerCallback);
    DllExport ~BellboardPerformanceParser();

    DllExport bool DownloadPerformance(const std::wstring& bellboardId);
    DllExport bool ParseFile(const std::string& xmlFilename);
    inline const Duco::DownloadedPeal& CurrentPerformance() const { return currentPerformance; }
    DllExport void EnableMethodsDatabase(Duco::MethodDatabase& methodDb, const std::string& methodDatabaseFile);
    DllExport void EnableDoveDatabase(const std::string& doveDatabaseFile, const std::string& installDir);
    DllExport bool ImportedTower() const;
    DllExport bool ImportedMethod() const;
    DllExport const std::wstring& TowerBaseId() const;
    inline bool ParseInProgress() const;
    inline void DisableUpdates();
    inline void DisableSearches();

protected:
    virtual void startElement(
            const XERCES_XMLCH_T* const    uri,
            const XERCES_XMLCH_T* const    localname,
            const XERCES_XMLCH_T* const    qname,
            const XERCES_CPP_NAMESPACE_QUALIFIER Attributes&     attrs);
    virtual void characters(const XERCES_XMLCH_T* const chars, const XMLSize_t length);
    virtual void ignorableWhitespace(const XMLCh* const chars, const XMLSize_t length);
    virtual void endElement(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname);
    virtual void endDocument();
    virtual void warning(const XERCES_CPP_NAMESPACE_QUALIFIER SAXParseException& exc);
    virtual void error(const XERCES_CPP_NAMESPACE_QUALIFIER SAXParseException& exc);
    virtual void fatalError(const XERCES_CPP_NAMESPACE_QUALIFIER SAXParseException& exception);

    void FindTowerWithWebReferences();
    void SuggestTowerFromFields();
    Duco::ObjectId FindMethodInExternalDatabase(const std::wstring& fullMethodName);
    Duco::ObjectId FindTowerInExternalDatabase();
    void SetMethod(const std::wstring& methodName);
    void Reset();

private:
    Duco::RingingDatabase&                          database;
    Duco::BellboardPerformanceParserCallback&       callback;
    Duco::BellboardPerformanceParserConfirmRingerCallback* confirmRingerCallback;

    XERCES_CPP_NAMESPACE_QUALIFIER  SAX2XMLReader*  parser;
    bool                                            parseInProgress;
    Duco::MethodNotationDatabaseSingleImporter*     methodImporter;
    Duco::DoveDatabase*                             doveDatabase;

    bool                            allowUpdates;
    bool                            allowSearches;
    bool                            anyErrors;
    bool                            importedTower;
    bool                            importedMethod;

    std::wstring                    currentValue;
    Duco::TBellboardAttribute       attribute;

    Duco::DownloadedPeal            currentPerformance;

    bool                            suggestRingers;
    bool                            conductor;
    unsigned int                    bellNo;
    unsigned int                    lastRingerBellNo;

    std::wstring                    towerDedication;
    std::wstring                    towerName;
    std::wstring                    towerCounty;
    std::wstring                    tenorWeight;
    std::wstring                    tenorKey;
    std::wstring                    towerbaseId;
    std::wstring                    doveId;
    std::wstring                    flagType;
};

bool
BellboardPerformanceParser::ParseInProgress() const
{
    return parseInProgress;
}

void
BellboardPerformanceParser::DisableUpdates()
{
    allowUpdates = false;
}
void
BellboardPerformanceParser::DisableSearches()
{
    allowSearches = false;
}

} // end namespace Duco

#endif // __BELLBOARDPERFORMANCEPARSER_H__
