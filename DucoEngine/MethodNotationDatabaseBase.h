#ifndef __METHODNOTATIONDATABASEBASE_H__
#define __METHODNOTATIONDATABASEBASE_H__

#include "DucoEngineCommon.h"
#include <string>
#include <bitset>
#include "ObjectId.h"
#include <xercesc/sax2/ContentHandler.hpp>
#include <xercesc/sax/ErrorHandler.hpp>

namespace Duco
{
    class MethodDatabase;
    class ProgressCallback;

    enum TMethodLibraryElementType
    {
        EUnknownElement = -1,
        EStageElement,
        ETitleElement, 
        ENameElement, 
        ETypeElement, 
        ENotationElement,
        EMethodElement,
        EMethodSetElement
    };

    enum TMethodLibraryAttributeType
    {
        EUnknownAttribute = -1,
        ELittleAttribute,
        EDifferentialAttribute,
        EPlainAttribute,
        ETrebleDodgingAttribute,

        ETotalMethodAttributes // Must go last
    };

    enum TMethodLibraryMethodType
    {
        EUnknownMethodType,
        EPlaceMethodType,
        EBobMethodType,
        ESlowCourseMethodType,
        ETrebleBobMethodType,
        EDelightMethodType,
        ESurpriseMethodType,
        EAllianceMethodType,
        ETreblePlaceMethodType,
        EHybridMethodType
    };

class MethodNotationDatabaseBase : public XERCES_CPP_NAMESPACE_QUALIFIER ContentHandler, public XERCES_CPP_NAMESPACE_QUALIFIER ErrorHandler
{
public:
    DllExport MethodNotationDatabaseBase(Duco::MethodDatabase& newDatabase, const std::string& newDatabaseFilename, ProgressCallback* const newCallback);
    DllExport ~MethodNotationDatabaseBase();

    DllExport virtual void ReadMethods();
    inline bool ErrorParsing() const;
    inline unsigned int MethodCount() const;
    inline void StopProcessing();

    virtual void AddMethod(const Duco::ObjectId& id, unsigned int stage, const std::wstring& name, const std::wstring& type, const std::wstring& title, const std::wstring& notation) = 0;

    // from  ContentHandler
    virtual void characters(const XMLCh *const chars, const XMLSize_t length);
    virtual void endDocument();
    virtual void ignorableWhitespace(const XMLCh *const chars, const XMLSize_t length);
    virtual void processingInstruction(const XMLCh *const target, const XMLCh *const data);
    virtual void setDocumentLocator(const XERCES_CPP_NAMESPACE_QUALIFIER Locator*const locator);
    virtual void startDocument();
    virtual void startElement(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname, const XERCES_CPP_NAMESPACE_QUALIFIER Attributes &attrs);
    virtual void startPrefixMapping(const XMLCh *const prefix, const XMLCh *const uri);
    virtual void endElement(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname);
    virtual void endPrefixMapping(const XMLCh *const prefix);
    virtual void skippedEntity(const XMLCh *const name);

    //from ErrorHandler
    void warning(const XERCES_CPP_NAMESPACE_QUALIFIER SAXParseException& exc);
    void error(const XERCES_CPP_NAMESPACE_QUALIFIER SAXParseException& exc);
    void fatalError(const XERCES_CPP_NAMESPACE_QUALIFIER SAXParseException& exc);
    void resetErrors();

protected:
    void ParseXml();
    Duco::TMethodLibraryElementType ElementType(const XMLCh *const elementName) const;
    bool FindAttribute(const XERCES_CPP_NAMESPACE_QUALIFIER Attributes& attrs, const TMethodLibraryAttributeType type, const wchar_t* attributeValue);
    void SetString(std::wstring& theString, const XMLCh *const chars, bool clearBeforeAdd = false) const;

    std::wstring ReverseNotation(const std::wstring& originalNotation) const;
    bool RequiresSeperator(const std::wstring& notation) const;

    void FixCurrentPlaceNotation();
    void SetMethodType(const XMLCh *const chars);
    std::wstring GetMethodType() const;

protected:
    Duco::MethodDatabase&               database;
    ProgressCallback* const             callback;
    const std::string                   databaseFilename;

    unsigned int                        noOfMethodsInXmlFile;
    TMethodLibraryElementType           currentElement;

    Duco::ObjectId                      currentMethodId;
    unsigned int                        currentStage;
    std::wstring                        currentMethodName;
    TMethodLibraryMethodType            currentMethodType;
    std::wstring                        currentMethodTitle;
    std::wstring                        currentMethodNotation;
    std::bitset<ETotalMethodAttributes> currentMethodTypeAttributeType;

    bool                                errorParsing;
    bool                                stopProcessing;
};

bool
MethodNotationDatabaseBase::ErrorParsing() const
{
    return errorParsing;
}

unsigned int
MethodNotationDatabaseBase::MethodCount() const
{
    return noOfMethodsInXmlFile;
}

void
MethodNotationDatabaseBase::StopProcessing()
{
    stopProcessing = true;
}

} // end namesapce Duco

#endif //!__METHODNOTATIONDATABASEBASE_H__
