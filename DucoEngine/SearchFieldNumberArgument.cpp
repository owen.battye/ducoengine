#include "SearchFieldNumberArgument.h"

#include "RingingDatabase.h"
#include "MethodDatabase.h"
#include "MethodSeriesDatabase.h"
#include "Peal.h"
#include "Tower.h"
#include "Ringer.h"
#include "Method.h"
#include "MethodSeries.h"

using namespace Duco;

TSearchFieldNumberArgument::TSearchFieldNumberArgument(Duco::TSearchFieldId newFieldId, size_t newFieldValue, Duco::TSearchType newSearchType)
: TSearchArgument(newFieldId), fieldValue(newFieldValue), searchType(newSearchType)
{

}

TSearchFieldNumberArgument::~TSearchFieldNumberArgument()
{

}

Duco::TFieldType 
TSearchFieldNumberArgument::FieldType() const
{
    return TFieldType::ENumberField;
}

bool 
TSearchFieldNumberArgument::Match(const Duco::Ringer& ringer, const Duco::RingingDatabase& database) const
{
    switch (fieldId)
    {
        case EContainsNameChanges:
            return ringer.NoOfNameChanges() > 0;

        default:
            return false;
    }
}

bool 
TSearchFieldNumberArgument::Match(const Duco::Tower& tower, const Duco::RingingDatabase& database) const
{
    switch (fieldId)
    {
        case ENoOfBells:
            return CompareNumber(tower.Bells());

        default:
            return false;
    }
}

bool
TSearchFieldNumberArgument::Match(const Duco::Method& method, const Duco::RingingDatabase& database) const
{
    switch (fieldId)
    {
        case ENoOfBells:
            return CompareNumber(method.Order());

        default:
            return false;
    }
}

bool
TSearchFieldNumberArgument::Match(const Duco::Peal& peal, const Duco::RingingDatabase& database) const
{
    switch (fieldId)
    {
        case EChanges:
            return CompareNumber(peal.NoOfChanges());

        case ENoOfBells:
            return CompareNumber(peal.NoOfBellsRung(database.TowersDatabase(), database.MethodsDatabase()));

        case EMethodSeriesCount:
            return CompareSeriesCount(peal, database);

        case EMethodDualOrder:
            {
            return CompareDualOrder(peal, database);
            }

        case EConductorType:
            return peal.ConductedType() == fieldValue;

        default:
            return false;
    }
}

bool
TSearchFieldNumberArgument::Match(const Duco::MethodSeries& object, const Duco::RingingDatabase& database) const
{
    switch (fieldId)
    {
        case ENoOfBells:
            return CompareNumber(size_t(object.NoOfBells()));

        case EMethodSeriesCount:
            return CompareNumber(object.NoOfMethods());

        default:
            return false;
    }
}

bool 
TSearchFieldNumberArgument::CompareNumber(size_t numberToCompare) const
{
    bool returnVal (false);
    switch (searchType)
    {
    case ELessThan:
        returnVal = numberToCompare < fieldValue;
        break;
    case ELessThanOrEqualTo:
        returnVal = numberToCompare <= fieldValue;
        break;
    case EEqualTo:
        returnVal = numberToCompare == fieldValue;
        break;
    case EMoreThanOrEqualTo:
        returnVal = numberToCompare >= fieldValue;
        break;
    case EMoreThan:
        returnVal = numberToCompare > fieldValue;
        break;
    }

    return returnVal;
}

bool 
TSearchFieldNumberArgument::CompareDualOrder(const Duco::Peal& peal, const Duco::RingingDatabase& database) const
{
    bool returnVal (false);

    const Method* const theMethod = database.MethodsDatabase().FindMethod(peal.MethodId());
    if (theMethod != NULL)
    {
        returnVal = theMethod->DualOrder();
    }

    return returnVal;
}

bool
TSearchFieldNumberArgument::CompareSeriesCount(const Duco::Peal& peal, const Duco::RingingDatabase& database) const
{
    bool returnVal (false);

    const MethodSeries* const theMethod = database.MethodSeriesDatabase().FindMethodSeries(peal.SeriesId());
    if (theMethod != NULL)
    {
        returnVal = CompareNumber(theMethod->NoOfMethods());
    }

    return returnVal;
}
