#include "PerformanceDate.h"
#include "DucoEngineCommon.h"
#include "DucoEngineUtils.h"
#include <sstream>

using namespace Duco;
using namespace std;

namespace Duco
{
    const unsigned int KFirstPealDateYear = 1753;
    const unsigned int KFirstPealDateMonth = 1;
    const unsigned int KFirstPealDateDay = 1;
    const unsigned int KMaxYear = 3999;
    const std::wstring dateSeperatorCharacters = L"/-";
}

PerformanceDate::PerformanceDate()
{
    Clear();
}

PerformanceDate::PerformanceDate(const tm& newDate)
:    pealDateYear(newDate.tm_year + 1900), pealDateMonth(newDate.tm_mon + 1), pealDateDay(newDate.tm_mday)
{
}

PerformanceDate::PerformanceDate(unsigned int newPealDateYear, unsigned int newPealDateMonth, unsigned int newPealDateDay)
 :  pealDateYear(newPealDateYear), pealDateMonth(newPealDateMonth), pealDateDay(newPealDateDay)
{

}

PerformanceDate::PerformanceDate(const std::wstring& dateStringInput, bool includeSpaceAsSeperator)
{
    std::wstring dateSepChars(dateSeperatorCharacters);
    if (includeSpaceAsSeperator)
    {
        dateSepChars.append(L" ");
    }

    std::wstring dateString = DucoEngineUtils::Trim(dateStringInput);
    size_t firstSeperator = dateString.find_first_of(dateSepChars);
    size_t secondSeperator = dateString.find_first_of(dateSepChars, firstSeperator + 1);

    pealDateDay = 0;
    pealDateMonth = 1;
    pealDateYear = 1900;
    if (firstSeperator > 0)
    {
        pealDateDay = _wtoi(dateString.substr(0, firstSeperator).c_str());
        if (secondSeperator > 0)
        {
            std::wstring monthStr(dateString.substr(firstSeperator + 1, secondSeperator - firstSeperator - 1));
            pealDateMonth = _wtoi(monthStr.c_str());
            if (pealDateMonth == 0)
            {
                pealDateMonth = ConvertMonthString(monthStr);
            }
            pealDateYear = _wtoi(dateString.substr(secondSeperator + 1).c_str());

        }
    }
}

PerformanceDate::PerformanceDate(const PerformanceDate& other)
 :  pealDateYear(other.pealDateYear), pealDateMonth(other.pealDateMonth), pealDateDay(other.pealDateDay)
{

}


PerformanceDate::~PerformanceDate()
{

}

void
PerformanceDate::Clear()
{
    ReadTimet(std::time(NULL));
}

void
PerformanceDate::ReadTimet(time_t newDate)
{
    tm* dateStr = gmtime(&newDate);
    pealDateYear = dateStr->tm_year + 1900;
    pealDateMonth = dateStr->tm_mon + 1;
    pealDateDay = dateStr->tm_mday;
}


Duco::PerformanceDate&
PerformanceDate::ResetToEarliest()
{
    pealDateYear = KFirstPealDateYear;
    pealDateMonth = KFirstPealDateMonth;
    pealDateDay = KFirstPealDateDay;
    return *this;
}

Duco::PerformanceDate&
PerformanceDate::ResetToLatest()
{
    pealDateYear = 9999;
    pealDateMonth = 12;
    pealDateDay = 31;
    return *this;
}

std::wstring
PerformanceDate::Str() const
{
    std::wstring date;
    wchar_t datebuf[12];
    swprintf(datebuf, 12, L"%d/%d/%d", pealDateDay, pealDateMonth, pealDateYear);
    date = datebuf;
    return date;
}

std::wstring
PerformanceDate::StrForBBExport() const
{
    std::wstring date;
    wchar_t datebuf[12];
    swprintf(datebuf, 12, L"%d-%02d-%02d", pealDateYear, pealDateMonth, pealDateDay);
    date = datebuf;
    return date;
}

std::wstring
PerformanceDate::DateWithLongerMonth() const
{
    wostringstream stream;
    PerformanceDate::PrintDate(stream, false, '-');

    std::wstring finalValue;
    DucoEngineUtils::ToUpperCase(stream.str(), finalValue);
    if (pealDateDay < 10)
    {
        finalValue = L" " + finalValue;
    }
    return finalValue;
}

std::wstring
PerformanceDate::DateForAssociationReports() const
{
    struct tm tmInfo = { 0 };
    tmInfo.tm_year = pealDateYear - 1900;
    tmInfo.tm_mon = pealDateMonth - 1;
    tmInfo.tm_mday = pealDateDay;
    mktime(&tmInfo);

    wchar_t dateBuffer[80];
    wcsftime(dateBuffer, 80, L"%A %e %B %Y", &tmInfo);

    std::wstring fullDateFormat;
    fullDateFormat += dateBuffer;
    return fullDateFormat;
}

std::wstring
PerformanceDate::FullDate() const
{
    wostringstream stream;
    PerformanceDate::PrintDate(stream, true);
    return WeekDay(true) + L" " + stream.str();
}

std::wostream&
PerformanceDate::PrintDate(wostream& stream, bool longMonth, wchar_t seperator) const
{
    stream << pealDateDay << seperator;

    if (longMonth)
    {
        switch (pealDateMonth)
        {
        case 1:
            stream << "January";
            break;
        case 2:
            stream << "February";
            break;
        case 3:
            stream << "March";
            break;
        case 4:
            stream << "April";
            break;
        case 5:
            stream << "May";
            break;
        case 6:
            stream << "June";
            break;
        case 7:
            stream << "July";
            break;
        case 8:
            stream << "August";
            break;
        case 9:
            stream << "September";
            break;
        case 10:
            stream << "October";
            break;
        case 11:
            stream << "November";
            break;
        case 12:
            stream << "December";
            break;
        default:
            break;
        }
    }
    else
    {
        switch (pealDateMonth)
        {
        case 1:
            stream << "Jan";
            break;
        case 2:
            stream << "Feb";
            break;
        case 3:
            stream << "Mar";
            break;
        case 4:
            stream << "Apr";
            break;
        case 5:
            stream << "May";
            break;
        case 6:
            stream << "Jun";
            break;
        case 7:
            stream << "Jul";
            break;
        case 8:
            stream << "Aug";
            break;
        case 9:
            stream << "Sep";
            break;
        case 10:
            stream << "Oct";
            break;
        case 11:
            stream << "Nov";
            break;
        case 12:
            stream << "Dec";
            break;
        default:
            break;
        }
    }
    stream << seperator << pealDateYear;
    return stream;
}

bool
PerformanceDate::operator>(const PerformanceDate& lhs) const
{
    if (pealDateYear > lhs.pealDateYear)
        return true;
    else if (pealDateYear == lhs.pealDateYear && pealDateMonth > lhs.pealDateMonth)
        return true;
    else if (pealDateYear == lhs.pealDateYear && pealDateMonth == lhs.pealDateMonth && pealDateDay > lhs.pealDateDay)
        return true;
    return false;
}

bool
PerformanceDate::operator<(const PerformanceDate& lhs) const
{
    if (pealDateYear < lhs.pealDateYear)
        return true;
    else if (pealDateYear == lhs.pealDateYear && pealDateMonth < lhs.pealDateMonth)
        return true;
    else if (pealDateYear == lhs.pealDateYear && pealDateMonth == lhs.pealDateMonth && pealDateDay < lhs.pealDateDay)
        return true;
    return false;
}

bool
PerformanceDate::operator==(const PerformanceDate& lhs) const
{
    if (pealDateYear == lhs.pealDateYear && 
        pealDateMonth == lhs.pealDateMonth && 
        pealDateDay == lhs.pealDateDay)
        return true;
    return false;
}

bool
PerformanceDate::operator!=(const PerformanceDate& lhs) const
{
    if (pealDateYear != lhs.pealDateYear ||
        pealDateMonth != lhs.pealDateMonth ||
        pealDateDay != lhs.pealDateDay)
        return true;
    return false;
}

bool
PerformanceDate::operator>=(const PerformanceDate& lhs) const
{
    return *this == lhs || *this > lhs;
}

bool
PerformanceDate::operator<=(const PerformanceDate& lhs) const
{
    return *this == lhs || *this < lhs;
}

time_t
PerformanceDate::time() const
{
    struct tm tmInfo = { 0 };
    tmInfo.tm_year = pealDateYear - 1900;
    tmInfo.tm_mon = pealDateMonth - 1;
    tmInfo.tm_mday = pealDateDay;
    /*tmInfo.tm_hour = 0;
    tmInfo.tm_isdst = 0;
    tmInfo.tm_min = 0;
    tmInfo.tm_sec = 0;
    tmInfo.tm_wday = 0;
    tmInfo.tm_yday = 0;*/

    time_t epoc = mktime(&tmInfo);

    return epoc;
}

bool
PerformanceDate::Valid() const
{
    if (pealDateYear <= 0 || pealDateYear > KMaxYear)
        return false;
    if (pealDateMonth < 1 || pealDateMonth > 12)
    {
        return false;
    }
    if (pealDateDay < 1 || pealDateDay > DaysInMonths(pealDateYear)[pealDateMonth-1])
    {
        return false;
    }
    if (pealDateYear == KFirstPealDateYear &&
        pealDateMonth == KFirstPealDateMonth &&
        pealDateDay == KFirstPealDateDay)
    {
        return false;
    }
    return true;
}

bool
PerformanceDate::IsYearValid() const
{
    return Year() <= KMaxYear;
}

std::array<unsigned int, 12>
PerformanceDate::DaysInMonths(unsigned int currentYear)
{
    unsigned int febDays(28);
    if (currentYear % 400 == 0 || (currentYear % 100 != 0 && currentYear % 4 == 0))
    {
        febDays += 1;
    }

    return std::array<unsigned int, 12> { 31,febDays,31,30,31,30,31,31,30,31,30,31 };
}

void
PerformanceDate::RemoveMonths(unsigned int numberOfMonths)
{
    while (numberOfMonths > 0)
    {
        pealDateMonth -= 1;
        if (pealDateMonth == 0)
        {
            pealDateMonth = 12;
            --pealDateYear;
        }
        --numberOfMonths;
    }

    if (pealDateDay < 1 || pealDateDay > DaysInMonths(pealDateYear)[pealDateMonth - 1])
    {
        pealDateDay = DaysInMonths(pealDateYear)[pealDateMonth - 1];
    }
}

int
PerformanceDate::DayNumber() const
{ /* Rata Die day one is 0001-01-01 */
    unsigned int y (pealDateYear);
    unsigned int m (pealDateMonth);
    unsigned int d (pealDateDay);

    if (m < 3)
        y--, m += 12;

    return 365 * y + y / 4 - y / 100 + y / 400 + (153 * m - 457) / 5 + d - 306;
}

bool
PerformanceDate::DaysMatch(const Duco::PerformanceDate& lhs) const
{
    return lhs.pealDateMonth == pealDateMonth && lhs.pealDateDay == pealDateDay;
}

unsigned int
PerformanceDate::NumberOfDaysUntil(const Duco::PerformanceDate& lhs) const
{
    int lhsDayNumber = lhs.DayNumber();
    int thisDayNumber = DayNumber();
    if (lhsDayNumber < thisDayNumber)
        return thisDayNumber - lhsDayNumber;

    return lhsDayNumber - thisDayNumber;
}

PerformanceDate
PerformanceDate::AddDays(unsigned int noOfDays) const
{
    PerformanceDate newDate (*this);

    while (noOfDays > 0)
    {
        std::array<unsigned int, 12> daysInMonths = PerformanceDate::DaysInMonths(newDate.pealDateYear);
        unsigned int daysLeftInThisMonth = daysInMonths[newDate.pealDateMonth - 1] - newDate.pealDateDay;
        if (noOfDays >= (daysLeftInThisMonth+1))
        {
            newDate.pealDateDay = 1;
            newDate.pealDateMonth += 1;
            noOfDays -= (daysLeftInThisMonth+1);
            if (newDate.pealDateMonth > 12)
            {
                newDate.pealDateMonth = 1;
                newDate.pealDateYear += 1;
                if (newDate.Year() > KMaxYear)
                {
                    newDate.pealDateDay = 1;
                    newDate.pealDateMonth = 1;
                    newDate.pealDateYear = KMaxYear + 1;
                    return newDate;
                }
            }
        }
        else
        {
            newDate.pealDateDay += noOfDays;
            noOfDays = 0;
        }
    }

    if (newDate.Year() > KMaxYear)
    {
        newDate.pealDateYear = KMaxYear;
    }

    return newDate;
}

std::wstring
PerformanceDate::WeekDay(bool longFormat) const
{
    unsigned int y (pealDateYear);
    static int t[] = {0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4};
    y -= (pealDateMonth < 3);

    int weekday (y + y/4 - y/100 + y/400 + t[pealDateMonth-1] + pealDateDay);
    weekday = weekday % 7;

    if (longFormat)
    {
        switch (weekday)
        {
        case 0:
            return L"Sunday";
        case 1:
            return L"Monday";
        case 2:
            return L"Tuesday";
        case 3:
            return L"Wednesday";
        case 4:
            return L"Thursday";
        case 5:
            return L"Friday";
        case 6:
            return L"Saturday";
        }
    }
    else
    {
        switch (weekday)
        {
        case 0:
            return L"Sun";
        case 1:
            return L"Mon";
        case 2:
            return L"Tue";
        case 3:
            return L"Wed";
        case 4:
            return L"Thu";
        case 5:
            return L"Fri";
        case 6:
            return L"Sat";
        }
    }
    return L"Err";
}

unsigned int
PerformanceDate::Year() const
{
    return pealDateYear;
}

unsigned int
PerformanceDate::Month() const
{
    return pealDateMonth;
}

unsigned int
PerformanceDate::Day() const
{
    return pealDateDay;
}

unsigned int
PerformanceDate::ConvertMonthString(const std::wstring& monthString) const
{
    unsigned int month(0); // returns 1 - 12;
    std::wstring monthStr;
    DucoEngineUtils::ToLowerCase(DucoEngineUtils::Trim(monthString), monthStr);
    std::wstring::const_iterator it = monthStr.begin();
    while (it != monthStr.end() && month == 0)
    {
        switch (*it)
        {
        case 'j': // Jan1, Jun6, Jul7, 
        {
            ++it;
            if (it != monthStr.end())
            {
                if (*it == 'a')
                    month = 1;
                else
                {
                    ++it;
                    if (it != monthStr.end())
                    {
                        if (*it == 'n')
                            month = 6;
                        else if (*it == 'l')
                            month = 7;
                    }
                }
            }
        }
        break;
        case 'f':
            month = 2;
            break;
        case 'm': // may & march
        {
            ++it;
            if (it != monthStr.end())
            {
                ++it;
                if (it != monthStr.end())
                {
                    if (*it == 'r')
                        month = 3;
                    else if (*it == 'y')
                        month = 5;
                }
            }
        }
        break;
        case 'a': // April4, Aug8,
        {
            ++it;
            if (it != monthStr.end())
            {
                if (*it == 'p')
                    month = 4;
                else if (*it == 'u')
                    month = 8;
            }
        }
        break;
        case 's':
            month = 9;
            break;
        case 'o':
            month = 10;
            break;
        case 'n':
            month = 11;
            break;
        case 'd':
            month = 12;
            break;
        default:
            break;
        }
        ++it;
    }
    return month;
}

