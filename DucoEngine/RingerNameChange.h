#ifndef __RINGERNAMECHANGE_H__
#define __RINGERNAMECHANGE_H__

#include <string>
#include "PerformanceDate.h"

namespace Duco
{
    struct RingerNameChange
    {
        std::wstring surname;
        std::wstring forename;
        PerformanceDate date;
    };

    struct OldRingerNameChange : public Duco::RingerNameChange
    {
        Duco::ObjectId otherRingerId;
    };

} // end namespace Duco

#endif //!__RINGERNAMECHANGE_H__
