#ifndef __ABBREVIATIONLIST_H__
#define __ABBREVIATIONLIST_H__

#include <string>
#include <vector>
#include <set>
#include "DucoEngineCommon.h"

namespace Duco
{
    class AbbreviationObject;

class AbbreviationList
{
public:
    DllExport AbbreviationList(const std::string& fileName);
    DllExport ~AbbreviationList();

    DllExport size_t SaveAbbreviationsNotUpdated(const std::string& fileName);
    DllExport size_t NoOfObjects() const;

    DllExport bool ProperName(const std::wstring& abbreviationToFind, std::wstring& realName) const;

private:
    std::vector<Duco::AbbreviationObject*> objects;
    mutable std::set<std::wstring> notUpdatedAbbreviations;
};

}

#endif //!__ABBREVIATIONLIST_H__
