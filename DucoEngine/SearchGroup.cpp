#include "SearchGroup.h"

#include "Peal.h"
#include "Method.h"
#include <cassert>

using namespace Duco;

TSearchGroup::TSearchGroup(Duco::TSearchFieldId newFieldId)
: TSearchArgument(newFieldId)
{
    assert(newFieldId == EAndGroup || newFieldId == EOrGroup);
}

TSearchGroup::~TSearchGroup()
{
    std::vector<TSearchArgument*>::iterator it = searchArguments.begin();
    while (it != searchArguments.end())
    {
        delete *it;
        ++it;
    }
    searchArguments.clear();
}

Duco::TFieldType
TSearchGroup::FieldType() const
{
    return TFieldType::EGroupField;
}

size_t
TSearchGroup::AddArgument(Duco::TSearchArgument* newArgument)
{
    searchArguments.push_back(newArgument);

    return searchArguments.size();
}

bool
TSearchGroup::Match(const Duco::Peal& peal, const Duco::RingingDatabase& database) const
{
    bool evaluatesToTrue (false);

    std::vector<TSearchArgument*>::const_iterator argIt = searchArguments.begin();

    if (FieldId() == EAndGroup)
    {
        evaluatesToTrue = !searchArguments.empty();
        while (argIt != searchArguments.end() && evaluatesToTrue)
        {
            evaluatesToTrue &= (*argIt)->Match(peal, database);
            ++argIt;
        }
    }
    else if (FieldId() == EOrGroup)
    {
        while (argIt != searchArguments.end() && !evaluatesToTrue)
        {
            evaluatesToTrue |= (*argIt)->Match(peal, database);
            ++argIt;
        }
    }

    return evaluatesToTrue;
}

bool
TSearchGroup::Match(const Duco::Tower& tower, const Duco::RingingDatabase& database) const
{
    bool evaluatesToTrue (false);

    std::vector<TSearchArgument*>::const_iterator argIt = searchArguments.begin();

    if (FieldId() == EAndGroup)
    {
        evaluatesToTrue = !searchArguments.empty();
        while (argIt != searchArguments.end() && evaluatesToTrue)
        {
            evaluatesToTrue &= (*argIt)->Match(tower, database);
            ++argIt;
        }
    }
    else if (FieldId() == EOrGroup)
    {
        while (argIt != searchArguments.end() && !evaluatesToTrue)
        {
            evaluatesToTrue |= (*argIt)->Match(tower, database);
            ++argIt;
        }
    }

    return evaluatesToTrue;
}

bool
TSearchGroup::Match(const Duco::Ringer& ringer, const Duco::RingingDatabase& database) const
{
    bool evaluatesToTrue (false);

    std::vector<TSearchArgument*>::const_iterator argIt = searchArguments.begin();

    if (FieldId() == EAndGroup)
    {
        evaluatesToTrue = !searchArguments.empty();
        while (argIt != searchArguments.end() && evaluatesToTrue)
        {
            evaluatesToTrue &= (*argIt)->Match(ringer, database);
            ++argIt;
        }
    }
    else if (FieldId() == EOrGroup)
    {
        while (argIt != searchArguments.end() && !evaluatesToTrue)
        {
            evaluatesToTrue |= (*argIt)->Match(ringer, database);
            ++argIt;
        }
    }

    return evaluatesToTrue;
}

bool
TSearchGroup::Match(const Duco::Method& method, const Duco::RingingDatabase& database) const
{
    bool evaluatesToTrue (false);

    std::vector<TSearchArgument*>::const_iterator argIt = searchArguments.begin();

    if (FieldId() == EAndGroup)
    {
        evaluatesToTrue = !searchArguments.empty();
        while (argIt != searchArguments.end() && evaluatesToTrue)
        {
            evaluatesToTrue &= (*argIt)->Match(method, database);
            ++argIt;
        }
    }
    else if (FieldId() == EOrGroup)
    {
        while (argIt != searchArguments.end() && !evaluatesToTrue)
        {
            evaluatesToTrue |= (*argIt)->Match(method, database);
            ++argIt;
        }
    }

    return evaluatesToTrue;
}

bool
TSearchGroup::Match(const Duco::MethodSeries& series, const Duco::RingingDatabase& database) const
{
    bool evaluatesToTrue (false);

    std::vector<TSearchArgument*>::const_iterator argIt = searchArguments.begin();

    if (FieldId() == EAndGroup)
    {
        evaluatesToTrue = !searchArguments.empty();
        while (argIt != searchArguments.end() && evaluatesToTrue)
        {
            evaluatesToTrue &= (*argIt)->Match(series, database);
            ++argIt;
        }
    }
    else if (FieldId() == EOrGroup)
    {
        while (argIt != searchArguments.end() && !evaluatesToTrue)
        {
            evaluatesToTrue |= (*argIt)->Match(series, database);
            ++argIt;
        }
    }

    return evaluatesToTrue;
}

bool
TSearchGroup::Match(const Duco::Composition& object, const Duco::RingingDatabase& database) const
{
    bool evaluatesToTrue(false);

    std::vector<TSearchArgument*>::const_iterator argIt = searchArguments.begin();

    if (FieldId() == EAndGroup)
    {
        evaluatesToTrue = !searchArguments.empty();
        while (argIt != searchArguments.end() && evaluatesToTrue)
        {
            evaluatesToTrue &= (*argIt)->Match(object, database);
            ++argIt;
        }
    }
    else if (FieldId() == EOrGroup)
    {
        while (argIt != searchArguments.end() && !evaluatesToTrue)
        {
            evaluatesToTrue |= (*argIt)->Match(object, database);
            ++argIt;
        }
    }

    return evaluatesToTrue;
}

bool
TSearchGroup::Match(const Duco::Association& object, const Duco::RingingDatabase& database) const
{
    bool evaluatesToTrue(false);

    std::vector<TSearchArgument*>::const_iterator argIt = searchArguments.begin();

    if (FieldId() == EAndGroup)
    {
        evaluatesToTrue = !searchArguments.empty();
        while (argIt != searchArguments.end() && evaluatesToTrue)
        {
            evaluatesToTrue &= (*argIt)->Match(object, database);
            ++argIt;
        }
    }
    else if (FieldId() == EOrGroup)
    {
        while (argIt != searchArguments.end() && !evaluatesToTrue)
        {
            evaluatesToTrue |= (*argIt)->Match(object, database);
            ++argIt;
        }
    }

    return evaluatesToTrue;
}

bool
TSearchGroup::Match(const Duco::Picture& peal, const Duco::RingingDatabase& database) const
{
    bool evaluatesToTrue (false);

    std::vector<TSearchArgument*>::const_iterator argIt = searchArguments.begin();

    if (FieldId() == EAndGroup)
    {
        evaluatesToTrue = !searchArguments.empty();
        while (argIt != searchArguments.end() && evaluatesToTrue)
        {
            evaluatesToTrue &= (*argIt)->Match(peal, database);
            ++argIt;
        }
    }
    else if (FieldId() == EOrGroup)
    {
        while (argIt != searchArguments.end() && !evaluatesToTrue)
        {
            evaluatesToTrue |= (*argIt)->Match(peal, database);
            ++argIt;
        }
    }

    return evaluatesToTrue;
}