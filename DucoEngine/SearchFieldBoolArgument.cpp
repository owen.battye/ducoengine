#include "SearchFieldBoolArgument.h"

#include "RingingDatabase.h"
#include "MethodDatabase.h"
#include "TowerDatabase.h"
#include "RingerDatabase.h"
#include "ChangeCollection.h"
#include "PealDatabase.h"
#include "Peal.h"
#include "Tower.h"
#include "Ringer.h"
#include "Method.h"
#include "Composition.h"
#include "DummyProgressCallback.h"

using namespace Duco;

TSearchFieldBoolArgument::TSearchFieldBoolArgument(Duco::TSearchFieldId newFieldId, bool newFieldValue)
: TSearchArgument(newFieldId), fieldValue(newFieldValue)
{

}

TSearchFieldBoolArgument::~TSearchFieldBoolArgument()
{

}

Duco::TFieldType 
TSearchFieldBoolArgument::FieldType() const
{
    return TFieldType::EBoolField;
}

bool
TSearchFieldBoolArgument::Match(const Duco::Ringer& ringer, const Duco::RingingDatabase& database) const
{
    switch (fieldId)
    {
    case ENotValid:
        return !ringer.Valid(database, true, true);
    case ENotes:
        return ringer.Notes().length() > 0;
    case EAllBlankRingerReferences:
        return ringer.PealbaseReference().length() == 0;
    case ENonBlankRingerReferences:
        return ringer.PealbaseReference().length() != 0;
    case ENonHumanRinger:
        return ringer.NonHuman() == fieldValue;
    case EDeceasedRingers:
        return ringer.Deceased() == fieldValue;
    case ELinkedRinger:
        return database.RingersDatabase().LinkedRinger(ringer.Id()) == fieldValue;
    default:
        break;
    }

    return false;
}

bool
TSearchFieldBoolArgument::Match(const Duco::Tower& tower, const Duco::RingingDatabase& database) const
{
    switch (fieldId)
    {
        case ENotValid:
            return !tower.Valid(database, true, true);

        case ERemovedTower:
            return tower.Removed() && fieldValue;

        case EMultipleRings:
            return fieldValue == tower.NoOfRings() > 1;

        case EExtraBells:
            return fieldValue == tower.ContainsSpecialBells();

        case ETowerContainsValidLocation:
            return fieldValue == tower.PositionValid();

        case EWithLinkedTower:
            return fieldValue == tower.LinkedTowerId().ValidId();

        case EEuropeOnly:
            return tower.PositionValid() && fieldValue == tower.InEurope();

        case EContainsPicture:
            return fieldValue == tower.PictureId().ValidId();

        case EHandbellTower:
            return fieldValue == tower.Handbell();

        case EAntiClockwise:
            return fieldValue == tower.AntiClockwise();

        case EDoveReference:
            return (tower.DoveRef().length() == 0) == fieldValue;
        case ETowerbaseId:
            return (tower.TowerbaseId().length() == 0) == fieldValue;

        default:
            return false;
    }
}

bool 
TSearchFieldBoolArgument::Match(const Duco::Method& method, const Duco::RingingDatabase& database) const
{
    switch (fieldId)
    {
        case ENotValid:
            return !method.Valid(database, true, true);

        case EMethodDualOrder:
            return method.DualOrder() && fieldValue;

        case Duco::EMethodPN:
            return (method.PlaceNotation().length() == 0) && !fieldValue;

        case Duco::EMethodIsSpliced:
            return method.Spliced();

        default:
            return false;
    }
}

bool
TSearchFieldBoolArgument::Match(const Duco::MethodSeries& methodSeries, const Duco::RingingDatabase& database) const
{
    switch (fieldId)
    {
        case EMethodSeriesCompleted:
            return database.PealsDatabase().CompletedSeries(methodSeries) && fieldValue;

        default:
            return false;
    }
}

bool 
TSearchFieldBoolArgument::Match(const Duco::Peal& peal, const Duco::RingingDatabase& database) const
{
    switch (fieldId)
    {
        case ENotValid:
            return !peal.Valid(database, true, true);

        case EMethodDualOrder:
            return CompareDualOrder(peal, database);

        case ETowerBells:
            return peal.Handbell() != fieldValue;

        case EWithdrawn:
            return peal.Withdrawn() == fieldValue;

        case EUnpaidFees:
            return peal.TotalPealFeePaid() > 0 == fieldValue;

        case EDoubleHanded:
            return peal.CheckForDoubleHandedRingers() == fieldValue;

        case ENotablePerformance:
            return peal.Notable() == fieldValue;

        case EMethodSeriesPresent:
            return peal.SeriesId().ValidId() == fieldValue;

        case Duco::EMethodIsSpliced:
        {
            const Method* const theMethod = database.MethodsDatabase().FindMethod(peal.MethodId());
            if (theMethod != NULL)
            {
                return theMethod->Spliced() == fieldValue;
            }
            return false;
        }

        case EContainsPicture:
            return fieldValue == peal.PictureId().ValidId();

        case ENonHumanRinger:
            return peal.ContainsNonHumanRinger(database.RingersDatabase()) == fieldValue;

        case EAntiClockwise:
        {
            const Tower* const theTower = database.TowersDatabase().FindTower(peal.TowerId());
            if (theTower != NULL)
            {
                return theTower->AntiClockwise() == fieldValue;
            }
            return false;
        }

        case EBlankFootnote:
            return peal.Footnotes().length() == 0;

        case ENonBlankFootnote:
            return peal.Footnotes().length() != 0;

        case EStrapper:
            return CheckForStrapper(peal);

        case Duco::EValidRWReference:
            return fieldValue == peal.ValidRingingWorldReferenceForUpload();
            
        case Duco::ESimulatedSound:
            return fieldValue == peal.SimulatedSound();

        case EDeceasedRingers:
            return fieldValue == peal.ContainsDeceasedRinger(database.RingersDatabase());

        default:
            return false;
    }
}

bool
TSearchFieldBoolArgument::Match(const Duco::Composition& object, const Duco::RingingDatabase& database) const
{
    switch (fieldId)
    {
        case ECompositionSnapStart:
            return object.IsSnapStart() == fieldValue;

        case ECompositionSnapFinish:
            return object.IsSnapFinish() == fieldValue;

        case ENotValid:
            return object.Valid(database, true, true) != fieldValue;

        case ETrueCompositionAndEndsWithRounds:
            return !ProveComposition(object, database) && !fieldValue;

        default:
            return false;
    }

    return false;
}

bool 
TSearchFieldBoolArgument::CompareDualOrder(const Duco::Peal& peal, const Duco::RingingDatabase& database) const
{
    bool returnVal = false;

    const Method* const theMethod = database.MethodsDatabase().FindMethod(peal.MethodId());
    if (theMethod != NULL)
    {
        returnVal = theMethod->DualOrder() && fieldValue;
    }

    return returnVal;
}

bool 
TSearchFieldBoolArgument::ProveComposition(const Duco::Composition& object, const Duco::RingingDatabase& database) const
{
    Composition copy (object);

    if (copy.PrepareToProve(database.MethodsDatabase(), database.Settings()))
    {
        Duco::DummyProgressCallback dummy;
        return copy.Prove(dummy, "", false, true) && copy.Changes().EndsWithRounds();
    }
    return false;
}


bool
TSearchFieldBoolArgument::CheckForStrapper(const Duco::Peal& peal) const
{
    return peal.AnyStrappers() == fieldValue;
}