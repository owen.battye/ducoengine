#ifndef __SEARCHFIELDRINGERPOSITION_H__
#define __SEARCHFIELDRINGERPOSITION_H__

#include "DucoEngineCommon.h"
#include <string>

namespace Duco
{
    class TSearchFieldRingerPosition
    {
    public:
        DllExport TSearchFieldRingerPosition(const std::wstring& description);

        bool Match(const unsigned int position, unsigned int order) const;
        std::wstring Description() const;

    protected:

    protected:
        unsigned int  bellNumber;
        bool          fromBack;
    };
}

#endif // __SEARCHFIELDRINGERARGUMENT_H__
