#include "Course.h"
#include "Calling.h"
#include "Change.h"
#include "Lead.h"
#include "PlaceNotation.h"

using namespace Duco;

Course::Course(const Duco::PlaceNotation& theNotation)
    : notation(theNotation), noOfChangesInCourse(0)
{
    courseEnd = new Change(notation.Order());
}

Course::Course(const Duco::PlaceNotation& theNotation, const Duco::Change& lastCourseEnd)
    : notation(theNotation), noOfChangesInCourse(0)
{
    courseEnd = new Change(lastCourseEnd);
}

Course::Course(const Course& rhs)
    :   notation(rhs.notation), noOfChangesInCourse(rhs.noOfChangesInCourse)
{
    courseEnd = new Change(*rhs.courseEnd);
    callings = rhs.callings;
}

Course::~Course()
{
    delete courseEnd;
}

void
Course::Clear(bool includeCourseEnd)
{
    if (includeCourseEnd)
    {
        courseEnd->Reset();
    }
    noOfChangesInCourse = 0;
    callings.clear();
}

bool
Course::AddCalling(const Duco::Calling& newCalling, size_t& noOfChanges, size_t snapStartPos)
{
    size_t snapStartPosition = callings.empty() ? snapStartPos : 0;
    size_t noOfNewChanges (0);

    const Calling* previousCalling = NULL;
    std::vector<Duco::Calling>::const_reverse_iterator lastPos = callings.rbegin();
    if (lastPos != callings.rend())
    {
        previousCalling = &(*lastPos);
    }

    if (previousCalling != NULL && previousCalling->Position() != newCalling.Position())
    {
        ++lastPos;
        while (lastPos != callings.rend())
        {
            if (newCalling.Position() == lastPos->Position())
                return false;
            ++lastPos;
        }
    }

    if (newCalling.AdvanceToCall(*courseEnd, notation, previousCalling, snapStartPosition, noOfNewChanges, true))
    {
        callings.push_back(newCalling);
        noOfChanges += noOfNewChanges;
        noOfChangesInCourse += noOfNewChanges;
        return true;
    }
    return false;
}

void
Course::MoveToHome()
{
    if (!courseEnd->IsHome())
    {
        Duco::Calling* newCall = new Duco::Calling(notation.Order(), EPlainLead);
        size_t noOfExtraChanges (0);
        const Duco::Calling* previousCalling = NULL;
        std::vector<Duco::Calling>::const_reverse_iterator lastPos = callings.rbegin();
        if (lastPos != callings.rend())
        {
            previousCalling = &(*lastPos);
        }

        if (newCall->AdvanceToCall(*courseEnd, notation, previousCalling, 0, noOfExtraChanges, true))
        {
            noOfChangesInCourse += noOfExtraChanges;
        }
        delete newCall;
    }
}

const Duco::Change&
Course::CourseEnd() const
{
    return *courseEnd;
}

std::wstring
Course::CallingStr(wchar_t position) const
{
    std::vector<Duco::Calling>::const_iterator it = callings.begin();
    std::wstring callingStr;
    while (it != callings.end())
    {
        if (it->MatchPosition(position, notation))
        {
            bool multiCallingLine (callingStr.length() > 0);
            if (!multiCallingLine)
            {
                std::vector<Duco::Calling>::const_iterator it2 (it);
                ++it2;
                if (it2 != callings.end())
                {
                    if ((*it2).MatchPosition(*it))
                        multiCallingLine = true;
                }
            }
            callingStr += it->Str(multiCallingLine);
        }
        ++it;
    }
    if (callingStr == L"---")
        return L"3";
    if (callingStr == L"--")
        return L"2";
    if (callingStr == L"sss")
        return L"3s";
    return callingStr;
}

std::wstring
Course::CallingStr(unsigned int thePosition) const
{
    std::wstring newPositon = notation.LeadOrder().CheckPosition(thePosition);
    return CallingStr(newPositon[0]);
}
