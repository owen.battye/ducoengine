#include "MethodNotationDatabaseUpdater.h"

#include "Method.h"
#include "MethodDatabase.h"
#include "DucoEngineUtils.h"

using namespace std;
using namespace Duco;

MethodNotationDatabaseUpdater::MethodNotationDatabaseUpdater(Duco::MethodDatabase& newDatabase, const std::string& newDatabaseFilename, ProgressCallback* const newCallback, const std::map<std::wstring, Duco::ObjectId>& newMethodsToUpdate)
    : MethodNotationDatabaseBase(newDatabase, newDatabaseFilename, newCallback), methodsToUpdate(newMethodsToUpdate), noOfMethodsUpdated(0)
{
}

MethodNotationDatabaseUpdater::~MethodNotationDatabaseUpdater()
{
}

bool
MethodNotationDatabaseUpdater::FindMethodToUpdateNotation(const std::wstring& methodName, unsigned int numberOfBells, Duco::ObjectId& methodId)
{
    std::wstring lowerCaseName;
    DucoEngineUtils::ToLowerCase(DucoEngineUtils::MethodComparisionCharacters(methodName), lowerCaseName);

    std::map<std::wstring, Duco::ObjectId>::const_iterator it = methodsToUpdate.find(lowerCaseName);
    if (it != methodsToUpdate.end())
    {
        const Method* const theMethod = database.FindMethod(it->second);
        if (theMethod != NULL && numberOfBells == theMethod->Order())
        {
            methodId = it->second;
            return true;
        }
    }
    return false;
}

bool
MethodNotationDatabaseUpdater::UpdateMethodNotation(const Duco::ObjectId& methodId) const
{
    bool methodUpdated (false);

    const Method* const theMethod = database.FindMethod(methodId);
    if (theMethod != NULL)
    {
        Method updatedMethod (*theMethod);
        updatedMethod.SetPlaceNotation(currentMethodNotation);
        if (database.UpdateObject(updatedMethod))
        {
            methodUpdated = true;
        }
    }
    return methodUpdated;
}


void
MethodNotationDatabaseUpdater::AddMethod(const Duco::ObjectId& id, unsigned int stage, const std::wstring& name, const std::wstring& type, const std::wstring& title, const std::wstring& notation)
{
    Duco::ObjectId methodId;
    if (FindMethodToUpdateNotation(title, currentStage, methodId))
    {
        UpdateMethodNotation(methodId);
        ++noOfMethodsUpdated;
    }
}
