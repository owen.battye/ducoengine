#ifndef __METHODDATABASE_H__
#define __METHODDATABASE_H__

#include "RingingObjectDatabase.h"

#include <map>
#include <set>
#include <xercesc/dom/DOMDocument.hpp>
#include "PealLengthInfo.h"

namespace Duco
{
    class Method;
    class RenumberProgressCallback;
    class ProgressCallback;
    class SettingsFile;

class MethodDatabase: public RingingObjectDatabase
{
public:
    DllExport MethodDatabase();
    DllExport virtual ~MethodDatabase();

    // from RingingObjectDatabase
    DllExport virtual Duco::ObjectId AddObject(const Duco::RingingObject& newObject);
    virtual bool Internalise(DatabaseReader& reader, Duco::ImportExportProgressCallback* newCallback, int databaseVersionNumber, size_t noOfObjectsInThisDatabase, size_t totalObjects, size_t objectsProcessedSoFar);
    DllExport virtual void ClearObjects(bool populateWithDefaults);
    virtual bool RebuildObjects(Duco::RenumberProgressCallback* callback, Duco::RingingDatabase& database);
    virtual bool MatchObject(const Duco::RingingObject& object, const Duco::TSearchArgument& searchArg, const Duco::RingingDatabase& database) const;
    virtual bool ValidateObject(const Duco::RingingObject& object, const Duco::TSearchValidationArgument& searchArg, const std::set<Duco::ObjectId>& idsToCheckAgainst, const Duco::RingingDatabase& database) const;
    virtual void ExternaliseToXml(Duco::ImportExportProgressCallback* newCallback, XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, bool exportDucoObjects) const;
    virtual bool UpdateObjectField(const Duco::ObjectId& id, const Duco::TUpdateArgument& updateArgument);
    
    // new functions
    DllExport const Duco::Method* const FindMethod(const Duco::ObjectId& methodId, bool setIndex = false) const;
    DllExport const Duco::Method* const FindMethod(unsigned int noOfBells, const std::wstring& placeNotation) const;
    DllExport Duco::ObjectId FindMethod(const std::wstring& name, const std::wstring& type, unsigned int order) const;
    DllExport Duco::ObjectId SuggestMethod(const std::wstring& name, const std::wstring& type, unsigned int order, unsigned int requiredProbability = 3) const;
    DllExport Duco::ObjectId SuggestMethod(const std::wstring& methodName, unsigned int requiredProbability = 3) const;
    bool SuggestMethodBySeriesString(const std::wstring& methodName, unsigned int noOfBells, std::set<Duco::ObjectId>& objectIds, Duco::ObjectId& mostLikelyMethodId) const;
    DllExport Duco::ObjectId FindOrCreateMethod(const std::wstring& name, unsigned int& noOfBells);
    DllExport Duco::ObjectId FindOrCreateMethod(const std::wstring& newMethodName, const std::wstring& newMethodType, const std::wstring& newMethodOrder, unsigned int& order);
    DllExport Duco::ObjectId AddMethod(const std::wstring& fullMethodName);
    DllExport Duco::ObjectId AddMethod(const std::wstring& methodName, const std::wstring& methodType, unsigned int noOfBells, bool dualOrderMethod);
    DllExport bool MethodNameMatchesOrderInName(const std::wstring& fullMethodName, unsigned int noOfBellsExpected) const;
    DllExport bool MethodIsSpliced(const Duco::ObjectId& methodId) const;

    DllExport bool SetNumberOfLeads(const Duco::ObjectId& methodId, unsigned int leadNumber);
    DllExport bool SetStartingLead(const Duco::ObjectId& methodId, unsigned int startingLead);
    DllExport bool UpdateNotation(const Duco::ObjectId& methodId, const std::wstring& newFullNotation);
    DllExport bool ReplaceMethodTypes(Duco::ProgressCallback*const callback, const std::string& installDir);
    bool CapitaliseField(Duco::ProgressCallback& progressCallback, bool methodName, bool methodType, size_t& numberOfObjectsUpdated, size_t numberOfObjectsToUpdate);

    DllExport void GetMethodsWithoutPlaceNotation(std::map<Duco::ObjectId, std::wstring>& methods) const;
    DllExport void GetAllMethodsByNumberOfBells(std::set<Duco::ObjectId>& methodIds, unsigned int noOfBells, bool anyStage, bool excludeSpliced) const;
    DllExport void GetAllMethodNames(std::set<std::wstring>& names) const;
    DllExport void GetAllMethodTypes(std::set<std::wstring>& methods) const;
    DllExport void GetAllMethodOrders(std::set<unsigned int>& methodOrders) const;
    DllExport void GetAllPlaceNotations(std::set<std::wstring>& placeNotations) const;
    DllExport void AddMissingMethods(std::map<Duco::ObjectId, Duco::PealLengthInfo>& sortedMethodIds, unsigned int noOfBells) const;
	std::map<Duco::ObjectId, Duco::ObjectId> RemoveDuplicateMethods(ProgressCallback*const callback, size_t& stepNumber, const size_t totalObjects);

    // Order names
    DllExport bool AddOrderName(unsigned int order, const std::wstring& newName);
    DllExport bool RemoveOrderName(unsigned int order);
    DllExport bool UpdateOrderName(unsigned int order, const std::wstring& newName);
    DllExport bool FindOrderNumber(const std::wstring& name, unsigned int& order, bool& dualOrder) const;
    DllExport bool FindOrderName(unsigned int order, std::wstring& foundOrderName) const;
    DllExport bool FindOrderName(const std::wstring& orderName, unsigned int& foundOrder) const;
    DllExport bool ContainsOrderName(unsigned int order) const;
    DllExport unsigned int HighestValidOrderNumber() const;
    DllExport unsigned int LowestValidOrderNumber() const;
    DllExport void GetAllOrderNames(std::vector<std::wstring>& names) const;
    DllExport void GetAllOrders(std::vector<unsigned int>& orders) const;
    DllExport void CreateDefaultBellNames();
    void ExternaliseOrderNames(Duco::ImportExportProgressCallback* newCallback, Duco::DatabaseWriter& writer) const;
    DllExport size_t NoOfOrderNames() const;
    DllExport bool SplitOrderName(const std::wstring& orderName, unsigned int& orderNumber) const;
    DllExport bool FindAndRemoveOrderName(unsigned int& order, std::wstring& completeMethodName, bool& dualOrder) const;
    DllExport bool FindAndRemoveMethodType(std::wstring& type, std::wstring& fullMethodName) const;
    DllExport bool DualOrder(const std::wstring& orderNameToFind) const;
    DllExport bool RemoveOrderNameFromMethodName(const std::wstring& fullMethodName, std::wstring& processedMethodName, unsigned int& possibleOrderNumber, bool& dualOrderMethod) const;

protected:
    static void CreateDefaultMethodAbbreviationReplacements(Duco::SettingsFile& settings);

    // from RingingObjectDatabase
    bool SaveUpdatedObject(Duco::RingingObject& lhs, const Duco::RingingObject& rhs);

private:
    std::map<unsigned int, std::wstring> bellNames;
};
}

#endif //!__METHODDATABASE_H__
