#include "BellboardPerformanceParser.h"

#include "AssociationDatabase.h"
#include "BellboardPerformanceParserCallback.h"
#include "BellboardDownload.h"
#include "DoveObject.h"
#include "DoveDatabase.h"
#include "DucoEngineUtils.h"
#include "DucoEngineLog.h"
#include "MethodDatabase.h"
#include "MethodNotationDatabaseSingleImporter.h"
#include "RingerDatabase.h"
#include "RingingDatabase.h"
#include "Tower.h"
#include "TowerDatabase.h"
#include <xercesc/sax2/Attributes.hpp>
#include <xercesc/sax2/XMLReaderFactory.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>
#include <iomanip>
#include <sstream>
#include <mutex>

using namespace std;
using namespace Duco;
XERCES_CPP_NAMESPACE_USE
std::mutex g_xerces_mutex;

BellboardPerformanceParser::BellboardPerformanceParser(Duco::RingingDatabase& theDatabase, Duco::BellboardPerformanceParserCallback& theCallback, Duco::BellboardPerformanceParserConfirmRingerCallback* theRingerCallback)
:   database(theDatabase), callback(theCallback), confirmRingerCallback(theRingerCallback), parseInProgress(false), methodImporter(NULL), doveDatabase(NULL), anyErrors(false),
    importedTower(false), importedMethod(false), attribute(EBBAttributeNone), conductor(false), bellNo(0), lastRingerBellNo(0), allowUpdates(true), allowSearches(true)
{
    std::lock_guard<std::mutex> guard(g_xerces_mutex);
    XMLPlatformUtils::Initialize();
    parser = XMLReaderFactory::createXMLReader();
    parser->setContentHandler(this);
    parser->setErrorHandler(this);
    parser->setFeature(XMLUni::fgXercesSchema, false);
    parser->setFeature(XMLUni::fgXercesLoadExternalDTD, false);
}

bool
BellboardPerformanceParser::DownloadPerformance(const std::wstring& bellboardId)
{
    if (parseInProgress)
    {
        return false;
    }
    parseInProgress = true;
    try
    {
        BellboardDownload downloader;
        if (downloader.DownloadPerformance(database.Settings(), bellboardId))
        {
            Reset();

            const std::string& pealString = downloader.Performance();
            MemBufInputSource* pMemBufIS = new MemBufInputSource((const XMLByte*)pealString.c_str(), pealString.length(), "SysID", false);
            parser->parse(*pMemBufIS);
            delete pMemBufIS;
        }
        else
        {
            anyErrors = true;
            callback.Cancelled(bellboardId, "Download failed");
            parseInProgress = false;
            return false;
        }
    }
    catch (std::exception& ex)
    {
        callback.Cancelled(bellboardId, ex.what());
        parseInProgress = false;
    }
    parseInProgress = false;
    return true;
}

bool
BellboardPerformanceParser::ParseFile(const std::string& xmlFilename)
{
    if (parseInProgress)
    {
        return false;
    }
    Reset();

    try
    {
        parseInProgress = true;
        parser->parse(xmlFilename.c_str());
    }
    catch (std::exception& ex)
    {
        anyErrors = true;
        callback.Cancelled(L"", ex.what());
    }
    parseInProgress = false;
    return true;
}

void
BellboardPerformanceParser::Reset()
{
    currentPerformance.Clear();
    anyErrors = false;
    importedTower = false;
    importedMethod = false;

    currentValue.clear();
    attribute = EBBAttributeNone;

    conductor = false;
    bellNo = 0;
    lastRingerBellNo = 0;

    towerDedication.clear();
    towerName.clear();
    towerCounty.clear();
    tenorWeight.clear();
    tenorKey.clear();
    towerbaseId.clear();
    doveId.clear();
    flagType.clear();
    suggestRingers = true;
}

BellboardPerformanceParser::~BellboardPerformanceParser()
{
    delete methodImporter;
    delete parser;
    delete doveDatabase;
    std::lock_guard<std::mutex> guard(g_xerces_mutex);
    XMLPlatformUtils::Terminate();
}

void BellboardPerformanceParser::startElement(const XERCES_XMLCH_T* const uri,
    const XERCES_XMLCH_T* const localname,
    const XERCES_XMLCH_T* const qname,
    const XERCES_CPP_NAMESPACE_QUALIFIER Attributes& attrs)
{
    currentValue.clear();
    attribute = TBellboardAttribute::EBBAttributeNone;

    for (unsigned int i = 0; i < attrs.getLength(); ++i)
    {
        const XMLCh* attributeName = attrs.getLocalName(i);
        const XMLCh* attributeValue = attrs.getValue(i);
        if (XMLString::compareIString(attributeName, L"id") == 0)
        {
            currentPerformance.SetBellBoardId(DucoEngineUtils::RemoveAllButValidChars(attributeValue, L"1234567890"));
        }
        else if (XMLString::compareIString(attributeName, L"type") == 0)
        {
            if (XMLString::compareIString(attributeValue, L"place") == 0)
            {
                attribute = TBellboardAttribute::EBBAttributePlace;
            }
            else if (XMLString::compareIString(attributeValue, L"dedication") == 0)
            {
                attribute = TBellboardAttribute::EBBAttributeDedication;
            }
            else if (XMLString::compareIString(attributeValue, L"county") == 0)
            {
                attribute = TBellboardAttribute::EBBAttributeCounty;
            }
            else if (XMLString::compareIString(attributeValue, L"hand") == 0)
            {
                currentPerformance.SetHandbell(true);
            }
            else if (XMLString::compareIString(localname, L"flag") == 0)
            {
                flagType = attributeValue;
                if (flagType.compare(L"duplicate") == 0)
                {
                    currentPerformance.SetBellBoardId(L"");
                }
                else if (flagType.compare(L"false") == 0)
                {
                    currentPerformance.SetWithdrawn(true);
                }
                else if (flagType.compare(L"simulated-sound") == 0)
                {
                    currentPerformance.SetSimulatedSound(true);
                }
                else if (flagType.compare(L"first-peal") != 0)
                {
                    DUCOENGINELOG3("Unrecognised bellboard flag type: %ls (%ls)", flagType.c_str(), currentPerformance.BellBoardId().c_str());
                }
            }
        }
        else if (XMLString::compareIString(attributeName, L"towerbase-id") == 0)
        {
            attribute = TBellboardAttribute::EBBAttributeTowerbaseId;
            towerbaseId = attributeValue;
        }
        else if (XMLString::compareIString(attributeName, L"dove-tower-id") == 0)
        {
            attribute = TBellboardAttribute::EBBAttributeDoveId;
            doveId = attributeValue;
        }
        else if (XMLString::compareIString(attributeName, L"bell") == 0)
        {
            int nextBellNo = DucoEngineUtils::ToInteger(attributeValue);
            if (currentPerformance.Handbell())
            {
                nextBellNo = (nextBellNo + 1) / 2;
            }
            bellNo = nextBellNo;
        }
        else if (XMLString::compareIString(attributeName, L"conductor") == 0)
        {
            conductor = true;
        }
        else if (XMLString::compareIString(attributeName, L"tenor") == 0)
        {
            std::wstring attributeValueStr = attributeValue;
            size_t tenorWeightIndex = attributeValueStr.find_first_of(L" ");
            if (tenorWeightIndex == -1)
            {
                tenorWeightIndex = attributeValueStr.length();
            }
            else
            {
                tenorKey = attributeValueStr.substr(tenorWeightIndex+1);
                if (tenorKey.find(L"in ") != string::npos && tenorKey.length() > 3)
                {
                    tenorKey = tenorKey.substr(3);
                }
            }
            tenorWeight = attributeValueStr.substr(0, tenorWeightIndex);
        }
    }
}

void
BellboardPerformanceParser::characters(const XERCES_XMLCH_T* const characters, const XMLSize_t length)
{
    currentValue.append(characters);
}

void
BellboardPerformanceParser::ignorableWhitespace(const XMLCh *const chars, const XMLSize_t length)
{

}

void
BellboardPerformanceParser::endElement(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname)
{
    char* elementName = XMLString::transcode(localname);
    if (XMLString::compareIString(elementName, "association") == 0)
    {
        Duco::ObjectId associationId;
        if (allowSearches)
        {
           associationId = database.AssociationsDatabase().SuggestAssociation(currentValue);
        }
        if (associationId.ValidId())
            currentPerformance.SetAssociationId(associationId);
        else
        {
            currentPerformance.SaveMissingAssociation(currentValue);
            anyErrors = true;
        }
    }
    else if (XMLString::compareIString(elementName, "place-name") == 0)
    {
        if (attribute == TBellboardAttribute::EBBAttributeDedication)
        {
            towerDedication = currentValue;
        }
        else if (attribute == TBellboardAttribute::EBBAttributePlace)
        {
            towerName = currentValue;
        }
        else if (attribute == TBellboardAttribute::EBBAttributeCounty)
        {
            towerCounty = currentValue;
        }
    }
    else if (XMLString::compareIString(elementName, "date") == 0)
    {
        std::tm t = {};
        std::wistringstream ss(currentValue);
        ss >> std::get_time(&t, L"%Y-%m-%d");
        PerformanceDate pealDate(t);
        currentPerformance.SetDate(pealDate);
    }
    else if (XMLString::compareIString(elementName, "duration") == 0)
    {
        PerformanceTime pealTime(currentValue);
        currentPerformance.SetTime(pealTime);
    }
    else if (XMLString::compareIString(elementName, "changes") == 0)
    {
        unsigned int noOfChanges = DucoEngineUtils::ToInteger(currentValue);
        currentPerformance.SetChanges(noOfChanges);
    }
    else if (XMLString::compareIString(elementName, "method") == 0)
    {
        SetMethod(currentValue);
    }
    else if (XMLString::compareIString(elementName, "details") == 0)
    {
        currentPerformance.SetMultiMethods(currentValue);
    }
    else if (XMLString::compareIString(elementName, "footnote") == 0)
    {
        if (currentPerformance.Footnotes().length() > 0)
        {
            std::wstring oldFootnotes = currentPerformance.Footnotes();
            oldFootnotes.append(KNewlineChar);
            oldFootnotes.append(currentValue);
            currentPerformance.SetFootnotes(oldFootnotes);
        }
        else
        {
            currentPerformance.SetFootnotes(currentValue);
        }
    }
    else if (XMLString::compareIString(elementName, "composer") == 0)
    {
        currentPerformance.SetComposer(currentValue);
    }
    else if (XMLString::compareIString(elementName, "rwref") == 0)
    {
        currentPerformance.SetRingingWorldReference(currentValue);
    }
    else if (XMLString::compareIString(elementName, "ringer") == 0)
    {
        Duco::ObjectId ringerId;
        bool strapper = false;
        if (lastRingerBellNo == bellNo)
        {
            strapper = true;
        }
        lastRingerBellNo = bellNo;

        if (allowSearches)
        {
            std::set<Duco::RingerPealDates> nearMatches;
            ringerId = database.SuggestRingers(currentValue, currentPerformance.Date(), confirmRingerCallback != NULL, nearMatches);
            if (!ringerId.ValidId() && confirmRingerCallback != NULL && suggestRingers && nearMatches.size() > 0)
            {
                ringerId = confirmRingerCallback->ChooseRinger(currentValue, nearMatches);
            }
        }
        if (!ringerId.ValidId())
        {
            suggestRingers = false;
            std::wstring forename;
            std::wstring surname;
            bool tempConductor = false;
            DucoEngineUtils::SplitRingerName(currentValue, forename, surname, tempConductor);
            Duco::MissingRinger missingRinger;
            missingRinger.bellNo = bellNo;
            missingRinger.forename = forename;
            missingRinger.surname = surname;
            missingRinger.conductor = conductor;
            missingRinger.strapper = strapper;
            anyErrors = true;
            currentPerformance.SaveMissingRinger(missingRinger);
        }
        else
        {
            currentPerformance.SetRingerId(ringerId, bellNo, strapper);
            if (conductor)
            {
                currentPerformance.SetConductorId(ringerId);
            }
        }
        conductor = false;
    }
    else if (XMLString::compareIString(elementName, "ringers") == 0)
    {
        SuggestTowerFromFields();
    }
    XMLString::release(&elementName);
}

void
BellboardPerformanceParser::warning(const XERCES_CPP_NAMESPACE_QUALIFIER SAXParseException& exc)
{
    char* message = XMLString::transcode(exc.getMessage());
    DUCOENGINELOG(std::format("Xerces warning: {}", message).c_str());
    XMLString::release(&message);

}

void
BellboardPerformanceParser::error(const XERCES_CPP_NAMESPACE_QUALIFIER SAXParseException& exc)
{
    char* message = XMLString::transcode(exc.getMessage());
    DUCOENGINELOG(std::format("Xerces error: {}", message).c_str());
    XMLString::release(&message);
}

void
BellboardPerformanceParser::fatalError(const XERCES_CPP_NAMESPACE_QUALIFIER SAXParseException& exception)
{
    char* message = XMLString::transcode(exception.getMessage());
    callback.Cancelled(currentPerformance.BellBoardId(), message);
    DUCOENGINELOG(std::format("Xerces fatal error: {}", message).c_str());
    XMLString::release(&message);
}

void 
BellboardPerformanceParser::FindTowerWithWebReferences()
{
    unsigned int noOfBellsRung = currentPerformance.NoOfBellsRung(database);
    ObjectId towerId = database.TowersDatabase().FindTowerByTowerbaseId(towerbaseId, noOfBellsRung, tenorWeight, tenorKey);
    if (!towerId.ValidId())
    {
        towerId = database.TowersDatabase().FindTowerByDoveId(doveId, noOfBellsRung, tenorWeight, tenorKey);
    }
    if (!towerId.ValidId() && allowSearches)
    {
        towerId = database.TowersDatabase().SuggestTower(towerDedication, towerName, towerCounty, noOfBellsRung, tenorWeight, tenorKey, true);
        if (towerId.ValidId() && towerbaseId.length() > 0)
        {
            Tower theTower = *(database.TowersDatabase().FindTower(towerId, true));
            if (theTower.ValidTowerbaseId() && theTower.TowerbaseId().compare(towerbaseId) != 0)
            {
                // If both towers have a tower base ID and they dont match, this can't be the right tower.
                towerId.ClearId();
            }
        }
    }

    ObjectId ringId;
    if (!towerId.ValidId() && !currentPerformance.Handbell())
    {
        towerId = FindTowerInExternalDatabase();
    }

    if (towerId.ValidId())
    {
        currentPerformance.SetTowerId(towerId);
        towerDedication.clear();
        towerName.clear();
        towerCounty.clear();
    }
    else
    {
        suggestRingers = false;
    }
}

void
BellboardPerformanceParser::SuggestTowerFromFields()
{
    FindTowerWithWebReferences();
    if (currentPerformance.TowerId().ValidId())
    {
        return;
    }
    Duco::ObjectId towerId;
    if (allowSearches)
    {
        towerId = database.TowersDatabase().SuggestTower(towerDedication, towerName, towerCounty, currentPerformance.NoOfBellsRung(database), tenorWeight, tenorKey, true);
        if (towerId.ValidId() && towerbaseId.length() > 0)
        {
            Tower theTower = *(database.TowersDatabase().FindTower(towerId, true));
            if (theTower.ValidTowerbaseId() && theTower.TowerbaseId().compare(towerbaseId) != 0)
            {
                // If both towers have a tower base ID and they dont match, this can't be the right tower.
                towerId.ClearId();
            }
        }
    }

    ObjectId ringId;
    if (!towerId.ValidId() && !currentPerformance.Handbell())
    {
        towerId = FindTowerInExternalDatabase();
    }

    if (towerId.ValidId())
    {
        currentPerformance.SetTowerId(towerId);
        towerDedication.clear();
        towerName.clear();
        towerCounty.clear();
    }
    else
    {
        anyErrors = true;
        currentPerformance.SaveMissingTower(towerDedication, towerName, towerCounty, towerbaseId, doveId);
        currentPerformance.SaveMissingRing(tenorWeight, tenorKey);
    }
}

void
BellboardPerformanceParser::endDocument()
{
    Duco::ObjectId ringId;
    if (currentPerformance.TowerId().ValidId())
    {
        const Tower* const theTower = database.TowersDatabase().FindTower(currentPerformance.TowerId(), true);
        if (theTower != NULL)
        {
            if (theTower->SuggestRing(currentPerformance.NoOfBellsRung(database), tenorWeight, tenorKey, ringId, true))
            {
                currentPerformance.SetRingId(ringId);
                tenorWeight.clear();
                tenorKey.clear();
            }
            else if (currentPerformance.NoOfBellsRung(database) < theTower->Bells() && importedTower)
            {
                Tower updatedTower(*theTower);
                ringId = updatedTower.AddRing(currentPerformance.NoOfBellsRung(database), tenorWeight, tenorKey);
                if (allowUpdates && database.TowersDatabase().UpdateObject(updatedTower))
                {
                    currentPerformance.SetRingId(ringId);
                    tenorWeight.clear();
                    tenorKey.clear();
                }
            }
            else
            {
                ringId.ClearId();
            }
        }
        else
        {
            anyErrors = true;
        }
    }
    if (!ringId.ValidId())
    {
        anyErrors = true;
        currentPerformance.SaveMissingRing(tenorWeight, tenorKey);
    }
    callback.Completed(currentPerformance.BellBoardId(), anyErrors);
}

void
BellboardPerformanceParser::EnableMethodsDatabase(Duco::MethodDatabase& methodDb, const std::string& methodDatabaseFile)
{
    methodImporter = new MethodNotationDatabaseSingleImporter(methodDb, methodDatabaseFile, nullptr);
    methodImporter->ReadMethods();
}

void
BellboardPerformanceParser::EnableDoveDatabase(const std::string& doveDatabaseFile, const std::string& installDir)
{
    doveDatabase = new Duco::DoveDatabase(doveDatabaseFile, NULL, installDir, true);
    doveDatabase->Import();
}

Duco::ObjectId
BellboardPerformanceParser::FindMethodInExternalDatabase(const std::wstring& fullMethodName)
{
    if (methodImporter != NULL)
    {
        Duco::ObjectId newMethodId = methodImporter->FindMethod(fullMethodName);
        importedMethod = newMethodId.ValidId();
        return newMethodId;
    }

    return KNoId;
}

Duco::ObjectId
BellboardPerformanceParser::FindTowerInExternalDatabase()
{
    if (doveDatabase != NULL)
    {
        std::set<Duco::DoveObject> foundTowers;
        if (doveDatabase->FindTowerByTowerbaseId(towerbaseId, foundTowers))
        {
            if (foundTowers.size() == 1 && allowUpdates)
            {
                importedTower = true;
                return database.TowersDatabase().AddTower(*foundTowers.begin());
            }
        }
        if (doveDatabase->FindTower(doveId, foundTowers))
        {
            if (foundTowers.size() == 1 && allowUpdates)
            {
                importedTower = true;
                return database.TowersDatabase().AddTower(*foundTowers.begin());
            }
        }
        if (allowSearches)
        {
            const Duco::DoveObject* const newTower = doveDatabase->SuggestTower(currentPerformance.NoOfBellsRung(database), towerDedication, towerName, towerCounty, tenorWeight, tenorKey);
            if (newTower != NULL && allowUpdates)
            {
                if (towerbaseId.length() == 0 || newTower->TowerbaseId().length() == 0 || towerbaseId.compare(newTower->TowerbaseId()) == 0)
                { // If a tower base id is present we really oought to have used that - else something isnt right!
                    importedTower = true;
                    return database.TowersDatabase().AddTower(*newTower);
                }
            }
        }
    }

    return KNoId;
}

const std::wstring&
BellboardPerformanceParser::TowerBaseId() const
{
    return towerbaseId;
}

bool
BellboardPerformanceParser::ImportedMethod() const
{
    return importedMethod;
}

bool
BellboardPerformanceParser::ImportedTower() const
{
    return importedTower;
}

void
BellboardPerformanceParser::SetMethod(const std::wstring& methodName)
{
    ObjectId methodId;
    if (allowSearches)
    {
        methodId = database.MethodsDatabase().SuggestMethod(methodName, 4);
        size_t startIndex = methodName.find_first_of('(');
        size_t endIndex = methodName.find_first_of(')', startIndex);
        if (!methodId.ValidId() && startIndex != -1 && endIndex != -1)
        {
            std::wstring methodNameWithoutStuffInBrackets = methodName.substr(0, startIndex);
            methodNameWithoutStuffInBrackets = DucoEngineUtils::Trim(methodNameWithoutStuffInBrackets);
            methodId = database.MethodsDatabase().SuggestMethod(methodNameWithoutStuffInBrackets, 5);
        }
        if (!methodId.ValidId())
        {
            // Missing the s on purpose, incase its case is different.
            if (methodName.find_first_of(L"pliced") != string::npos)
            {
                methodId = FindMethodInExternalDatabase(methodName);
            }
        }
    }
    if (methodId.ValidId())
    {
        currentPerformance.SetMethodId(methodId);
    }
    else
    {
        suggestRingers = false;
        anyErrors = true;
        currentPerformance.SaveMissingMethod(DucoEngineUtils::Trim(methodName));
    }
}
