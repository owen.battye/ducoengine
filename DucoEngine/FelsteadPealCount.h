#ifndef __FELSTEADPEALCOUNT_H__
#define __FELSTEADPEALCOUNT_H__

#include <string>
#include "DucoEngineCommon.h"
#include <thread>
#include "ObjectId.h"


namespace Duco
{
    class DatabaseSettings;
    class Tower;

    class FelsteadPealCount
    {
    public:
        DllExport FelsteadPealCount(const Duco::DatabaseSettings& newSettings, const Duco::Tower& tower);
        DllExport ~FelsteadPealCount();

        DllExport bool StartDownload();
        DllExport void Wait();
        DllExport void ImportFile(const char* towerFilename);

        DllExport size_t NumberOfPeals() const;
        DllExport const Duco::ObjectId& TowerId() const;
        inline bool Success() const;
        DllExport void Download();

    protected:
        bool SearchString(const std::string& data);

    private:
        bool                            success;
        std::jthread*                   downloadThread;

        const Duco::DatabaseSettings&   settings;
        Duco::ObjectId                  towerId;
        std::wstring                    felsteadId;
        size_t                          numberOfPeals;
    };

    bool FelsteadPealCount::Success() const
    {
        return success;
    }

}

#endif __FELSTEADPEALCOUNT_H__
