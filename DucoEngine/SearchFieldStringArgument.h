#ifndef __SEARCHFIELDSTRINGARGUMENT_H__
#define __SEARCHFIELDSTRINGARGUMENT_H__

#include "SearchArgument.h"

#include "DucoEngineCommon.h"
#include "ObjectId.h"
#include <string>

namespace Duco
{
    class Peal;
    class Tower;
    class Ringer;
    class Method;

class TSearchFieldStringArgument : public TSearchArgument
{
public:
    DllExport explicit TSearchFieldStringArgument(Duco::TSearchFieldId fieldId, const std::wstring& newFieldValue, bool subStr = true, bool excluding = false);
    DllExport Duco::TFieldType FieldType() const;
    inline const std::wstring& FieldValue() const;
 
    virtual bool Match(const Duco::Peal& peal, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Tower& tower, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Ringer& ringer, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Method& method, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::MethodSeries& object, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Composition& object, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Association& object, const Duco::RingingDatabase& database) const;
    virtual bool Match(const Duco::Picture& object, const Duco::RingingDatabase& database) const;

protected:
    bool CheckMethodFullName(const ObjectId& methodId, const Duco::RingingDatabase& database) const;
    bool CheckMethodSeriesName(const Duco::Peal& peal, const Duco::RingingDatabase& database) const;
    bool CheckMethodSeriesName(const Duco::MethodSeries& theSeries, const Duco::RingingDatabase& database) const;
    bool CheckMultiMethods(const Duco::Peal& peal) const;
    bool CheckTowerName(const Duco::Peal& peal, const Duco::RingingDatabase& database) const;
    bool CheckRingerName(const ObjectId& ringerId, const Duco::RingingDatabase& database) const;
    bool CheckConductorName(const Duco::Peal& peal, const Duco::RingingDatabase& database) const;
    bool CheckComposerName(const Duco::Peal& peal, const Duco::RingingDatabase& database) const;
    bool CheckRingNames(const Duco::Tower& tower, const Duco::RingingDatabase& database) const;
    bool TenorWeight(const Duco::Tower& tower, const Duco::RingingDatabase& database) const;
    bool TenorWeight(const Duco::ObjectId& towerId, const Duco::ObjectId& ringId, const Duco::RingingDatabase& database) const;
    bool TenorKey(const Duco::ObjectId& towerId, const Duco::ObjectId& ringId, const Duco::RingingDatabase& database) const;
    bool CompareSeriesMethodNames(const Duco::MethodSeries& methodSeries, const Duco::RingingDatabase& database) const;
    bool CheckCompositionName(const ObjectId& compositionId, const Duco::RingingDatabase& database) const;

protected:
    const std::wstring   fieldValue;
    const bool          subStr;
    const bool          excluding;
};

const std::wstring&
TSearchFieldStringArgument::FieldValue() const
{
    return fieldValue;
}

}

#endif // __SEARCHFIELDSTRINGARGUMENT_H__
