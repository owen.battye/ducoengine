#ifndef __DATABASESETTINGS_H__
#define __DATABASESETTINGS_H__

#include <string>
#include <bitset>
#include "DucoEngineCommon.h"
#include "ObjectId.h"
#include "ObjectType.h"
#include "DucoErrorCodes.h"

namespace Duco
{
    class DatabaseReader;
    class DatabaseWriter;

class DatabaseSettings
{
public:
    enum TBlueLineWebsite
    {
        EVisualMethodArchive = 0,
        EPaulGraupnerBlueLine
    };
    enum TPealPrintoutFormat
    {
        ERingingWorldStyle = 0,
        EPealboardStyle
    };
    enum TLineType
    {
        ENormalLine = 0,
        ELongLine,
        EAnnotated
    };
    enum TPreferredViewWebsite
    {
        EViewOnBellBoard = 1
    };

    DllExport DatabaseSettings();
    DllExport ~DatabaseSettings();
    DllExport bool operator==(const Duco::DatabaseSettings& rhs) const;

    
    DllExport void ResetWebsiteLinks();
    DllExport void ClearSettings(bool quarterPealDb);

    void Internalise(DatabaseReader& reader, unsigned int databaseVersion);
    void Externalise(DatabaseWriter& writer) const;

    inline const std::wstring& BellBoardURL() const;
    DllExport void SetBellBoardURL(const std::wstring& newUrl);

    inline const std::wstring& BellBoardViewSuffixURL() const;
    DllExport void SetBellBoardViewSuffixURL(const std::wstring& newSuffix);

    inline bool BellBoardAutoSetRingingWorldPage() const;
    inline void SetBellBoardAutoSetRingingWorldPage(bool newSetting);

    inline TPreferredViewWebsite PreferredOnlineWebsite() const;
    DllExport void SetPreferredOnlineWebsite(TPreferredViewWebsite newWebsite);

    DllExport const std::wstring& PreferedDownloadWebsiteUrl() const;

    inline bool LastNameFirst() const;
    DllExport void SetLastNameFirst(bool newSetting);

    inline const Duco::ObjectId& DefaultRinger() const;
    inline bool DefaultRingerSet() const;
    DllExport void SetDefaultRinger(const Duco::ObjectId& newRinger);

    inline bool AlphabeticReordering() const;
    DllExport void SetAlphabeticReordering(bool newordering);

    inline bool ShowTreble() const;
    DllExport void SetShowTreble(bool newShowTreble);

    inline bool ShowGridLines() const;
    DllExport void SetShowGridLines(bool newShowTreble);

    inline bool ShowFullGrid() const;
    DllExport void SetShowFullGrid(bool newShowFullGrid);

    inline bool ShowBobsAndSingles() const;
    DllExport void SetShowBobsAndSingles(bool newShowBobsAndSingles);

    inline TLineType LineType() const;
    DllExport void SetLineType(const TLineType& newLineType);

    inline TBlueLineWebsite BlueLineWebsite() const;
    DllExport void SetBlueLineWebsite(TBlueLineWebsite newWebSite);

    inline float PrintFontSize() const;
    
    inline bool UsePrintPreview() const;
    DllExport void SetPrintPreview(bool newPrintPreview);

    inline bool AllowErrors() const;
    DllExport void SetAllowErrors(bool newAllowErrors);

    inline TPealPrintoutFormat PealPrintFormat() const;
    DllExport void SetPealPrintFormat(TPealPrintoutFormat newFormat);

    inline bool PrintSurnameFirst() const;
    DllExport void SetPrintSurnameFirst(bool newPrintSurnameFirst);

    inline bool PealDatabase() const;
    DllExport void SetPealDatabase(bool newPealDatabase);

    DllExport const std::wstring& DoveURL() const;
    DllExport void SetDoveURL(const std::wstring& newDoveURL);

    inline const std::wstring& GoogleURL() const;
    DllExport void SetGoogleURL(const std::wstring& newGoogleURL);

    inline bool UseMapBox() const;
    DllExport void SetUseMapBox(bool newUseMapBox);

    inline bool KeepTowersWithRungDate() const;
    DllExport void SetKeepTowersWithRungDate(bool newKeepTowersWithRungDate);

    inline const std::wstring& PealbaseTowerURL() const;
    DllExport void SetPealbaseTowerURL(const std::wstring& newUrl);

    DllExport std::wstring FelsteadTowerURL() const;
    DllExport std::wstring FelsteadCountyURL() const;
    DllExport const std::wstring& FelsteadURL() const;
    DllExport void SetFelsteadURL(const std::wstring& newUrl);

    inline const std::wstring& PealbaseMethodURL() const;
    DllExport void SetPealbaseMethodURL(const std::wstring& newUrl);

    inline const std::wstring& PealbaseAssociationURL() const;
    DllExport void SetPealbaseAssociationURL(const std::wstring& newUrl);

    inline const std::wstring& CompLibURL() const;
    DllExport void SetCompLibURL(const std::wstring& newUrl);

    inline bool ShowAssociationColumn() const;
    DllExport void SetShowAssociationColumn(bool newValue);
    inline bool ShowComposerColumn() const;
    DllExport void SetShowComposerColumn(bool newValue);
    inline bool ShowConductorColumn() const;
    DllExport void SetShowConductorColumn(bool newValue);
    inline bool ShowRingColumn() const;
    DllExport void SetShowRingColumn(bool newValue);
    inline bool ShowBellRungColumn() const;
    DllExport void SetShowBellRungColumn(bool newValue);
    inline bool ShowReferencesColumn() const;
    DllExport void SetShowReferencesColumn(bool newValue);

    DllExport void SetValidationCheck(std::size_t pos, const Duco::TObjectType& objectType, bool disabled);
    DllExport bool ValidationCheckDisabled(std::size_t pos, const Duco::TObjectType& objectType) const;
        
    inline bool SettingsChanged() const;
    DllExport void SetSettingsChanged() const;
    DllExport void ResetSettingsChanged() const;

    DllExport bool SettingsValid() const;
    DllExport bool SettingsAllDefault() const;

    DllExport std::wstring BellBoardDownloadUrl(const std::wstring& bellboardId) const;
    
private:
    // Links for Peals
    std::wstring        bellBoardURL;
    std::wstring        bellBoardViewSuffixURL;
    bool                bellBoardAutoSetRingingWorldPage;

    // Links for towers
    std::wstring        pealbaseTowerURL;
    std::wstring        felsteadURL;
    std::wstring        compLibURL;
    // Links for methods
    std::wstring        pealbaseMethodURL;
    // Links for associations
    std::wstring        pealbaseAssociation;
    // Other links
    std::wstring        doveURL;
    std::wstring        googleURL;
    bool                useMapBox; // for peal map
    // General settings
    TPreferredViewWebsite viewWebsite;
    bool                lastNameFirst;
    Duco::ObjectId      defaultRinger;
    bool                alphabeticReordering;
    bool                showTrebleInDrawing;
    bool                showGridLinesInDrawing;
    bool                showFullGrid;
    bool                showBobsAndSingles;
    TLineType           lineType;
    TBlueLineWebsite    webSite;
    unsigned int        printFontSize;
    bool                usePrintPreview;
    bool                allowErrors;
    TPealPrintoutFormat pealPrintFormat;
    bool                printSurnamesFirst;
    bool                quarterpealDatabase;
    bool                keepTowersWithRungDate;

    bool                showAssociationColumn;
    bool                showComposerColumn;
    bool                showConductorColumn;
    bool                showRingColumn;
    bool                showBellRungColumn;
    bool                showReferencesColumn;

    std::bitset<KPeal_ErrorBitsetSize>           disabledPealChecks;
    std::bitset<KTower_ErrorBitsetSize>          disabledTowerChecks;
    std::bitset<KMethod_ErrorBitsetSize>         disabledMethodChecks;
    std::bitset<KMethodSeries_ErrorBitsetSize>   disabledMethodSeriesChecks;
    std::bitset<KComposition_ErrorBitsetSize>    disabledCompositionChecks;
    std::bitset<KRinger_ErrorBitsetSize>         disabledRingerChecks;
    std::bitset<KAssociation_ErrorBitsetSize>    disabledAssociationChecks;

    mutable bool        settingsChanged;
};

const std::wstring&
DatabaseSettings::BellBoardURL() const
{
    return bellBoardURL;
}

const std::wstring&
DatabaseSettings::BellBoardViewSuffixURL() const
{
    return bellBoardViewSuffixURL;
}

bool
DatabaseSettings::BellBoardAutoSetRingingWorldPage() const
{
    return bellBoardAutoSetRingingWorldPage;
}

void
DatabaseSettings::SetBellBoardAutoSetRingingWorldPage(bool newSetting)
{
    bellBoardAutoSetRingingWorldPage = newSetting;
}


const std::wstring&
DatabaseSettings::PealbaseTowerURL() const
{
    return pealbaseTowerURL;
}

const std::wstring&
DatabaseSettings::PealbaseMethodURL() const
{
    return pealbaseMethodURL;
}

DatabaseSettings::TPreferredViewWebsite
DatabaseSettings::PreferredOnlineWebsite() const
{
    return viewWebsite;
}

bool
DatabaseSettings::LastNameFirst() const
{
    return lastNameFirst;
}

const Duco::ObjectId&
DatabaseSettings::DefaultRinger() const
{
    return defaultRinger;
}

bool
DatabaseSettings::DefaultRingerSet() const
{
    return defaultRinger.ValidId();
}

bool
DatabaseSettings::AlphabeticReordering() const
{
    return alphabeticReordering;
}

bool
DatabaseSettings::ShowTreble() const
{
    return showTrebleInDrawing;
}

bool
DatabaseSettings::ShowFullGrid() const
{
    return showFullGrid;
}

bool
DatabaseSettings::ShowGridLines() const
{
    return showGridLinesInDrawing;
}

DatabaseSettings::TLineType
DatabaseSettings::LineType() const
{
    return lineType;
}

DatabaseSettings::TBlueLineWebsite
DatabaseSettings::BlueLineWebsite() const
{
    return webSite;
}

float
DatabaseSettings::PrintFontSize() const
{
    return float(printFontSize);
}

bool
DatabaseSettings::UsePrintPreview() const
{
    return usePrintPreview;
}

bool
DatabaseSettings::SettingsChanged() const
{
    return settingsChanged;
}

bool
DatabaseSettings::AllowErrors() const
{
    return allowErrors;
}

DatabaseSettings::TPealPrintoutFormat
DatabaseSettings::PealPrintFormat() const
{
    return pealPrintFormat;
}

bool
DatabaseSettings::PrintSurnameFirst() const
{
    return printSurnamesFirst;
}

bool
DatabaseSettings::PealDatabase() const
{
    return !quarterpealDatabase;
}

const std::wstring&
DatabaseSettings::GoogleURL() const
{
    return googleURL;
}

bool
DatabaseSettings::UseMapBox() const
{
    return useMapBox;
}

bool 
DatabaseSettings::ShowBobsAndSingles() const
{
    return showBobsAndSingles;
}

bool
DatabaseSettings::KeepTowersWithRungDate() const
{
    return keepTowersWithRungDate;
}

const std::wstring&
DatabaseSettings::PealbaseAssociationURL() const
{
    return pealbaseAssociation;
}

const std::wstring&
DatabaseSettings::CompLibURL() const
{
    return compLibURL;
}

bool
DatabaseSettings::ShowAssociationColumn() const
{
    return showAssociationColumn;
}

bool
DatabaseSettings::ShowComposerColumn() const
{
    return showComposerColumn;
}

bool
DatabaseSettings::ShowConductorColumn() const
{
    return showConductorColumn;
}

bool
DatabaseSettings::ShowRingColumn() const
{
    return showRingColumn;
}

bool
DatabaseSettings::ShowBellRungColumn() const
{
    return showBellRungColumn;
}

bool
DatabaseSettings::ShowReferencesColumn() const
{
    return showReferencesColumn;
}

}
#endif //! __DATABASESETTINGS_H__
