#ifndef __SEARCHFIELDRINGERARGUMENT_H__
#define __SEARCHFIELDRINGERARGUMENT_H__

#include "SearchArgument.h"

#include "DucoEngineCommon.h"
#include "SearchFieldRingerPosition.h"
#include "ObjectId.h"
#include <string>

namespace Duco
{
    class Peal;

class TSearchFieldRingerArgument : public TSearchArgument
{
public:
    DllExport explicit TSearchFieldRingerArgument(const Duco::ObjectId& newRingerId, const Duco::TSearchFieldRingerPosition& posn);
    DllExport explicit TSearchFieldRingerArgument(const std::wstring& newRingerName, const Duco::TSearchFieldRingerPosition& posn);
    DllExport TSearchFieldRingerArgument(const Duco::TSearchFieldRingerArgument& rhs);

    DllExport Duco::TFieldType FieldType() const;
    DllExport const std::wstring& RingerName() const;
    DllExport const Duco::ObjectId& RingerId() const;
    DllExport std::wstring Position() const;
 
    DllExport virtual bool Match(const Duco::Peal& peal, const Duco::RingingDatabase& database) const;

protected:
    bool MatchById(const Duco::Peal& peal, const Duco::RingingDatabase& database) const;
    bool MatchByString(const Duco::Peal& peal, const Duco::RingingDatabase& database) const;

protected:
    const Duco::ObjectId                ringerId;
    const std::wstring                   fieldValue;
    const TSearchFieldRingerPosition    position;
};

}

#endif // __SEARCHFIELDRINGERARGUMENT_H__
