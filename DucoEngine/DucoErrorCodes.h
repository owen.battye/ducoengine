#ifndef __DUCOERRORCODES_H__
#define __DUCOERRORCODES_H__

namespace Duco
{
    enum TPealErrorCodes
    {
        EPealWarning_AssociationMissing       = 0,
        EPealWarning_AssociationBlank         = 1,
        EPeal_TowerMissing                    = 2,
        EPeal_RingMissing                     = 3,
        EPeal_MethodMissing                   = 4,
        EPeal_TooManyChanges                  = 5,
        EPeal_TooFewChanges                   = 6,
        EPeal_DateInvalid                     = 7,
        EPeal_TimeInvalid                     = 8,
        EPeal_TooManyConductors               = 9,
        EPeal_TooFewConductors                = 10,
        EPeal_ConductorNotRinging             = 11,
        EPeal_NoOfRingersIncorrect            = 12,
        EPeal_NoOfRingersAndBellsMismatch     = 13,
        EPeal_InvalidStrapper                 = 14,
        EPeal_StrapperMissing                 = 15,
        EPeal_DefaultRingerMissing            = 16,
        EPeal_DuplicateRingers                = 17,
        EPeal_MissingDuplicateRingers         = 18,
        EPeal_MultiMethodsMissing             = 19,
        EPealWarning_MultiMethodsNotRequired  = 20,
        EPealWarning_SeriesMissing            = 21,
        EPealWarning_SeriesNotRequired        = 22,
        EPeal_CompositionInvalid              = 23,
        EPealWarning_CompositionHasDifferentMethod = 24,
        EPeal_SeriesInvalid                   = 25,
        EPeal_SeriesHasDifferentNoOfBells     = 26,
        EPeal_DuplicatePeal                   = 27,
        EPeal_TowerAndHandbellMismatch        = 28,
        EPeal_LinkedRingerTwice               = 29,

        KPeal_ErrorBitsetSize // this must go last
    };

    enum TTowerErrorCodes
    {
        ETower_TowerNameInvalid              = 0,
        ETower_TowerCountyAndCity            = 1,
        ETower_TooFewBells                   = 2,
        ETower_TooManyBells                  = 3,
        ERing_InvalidRingName                = 4,
        ERing_InvalidRingTenorWeight         = 5,
        ERing_MissingRingTenorKey            = 6,
        ERing_InvalidRingTenorKeyChars       = 7,
        ERing_InvalidRingNoOfBells           = 8,
        ERingWarning_PossibleInvalidTenorKey = 9,
        ERing_PossibleInvalidRingBells       = 10,
        ERing_InvalidRingWithExtraBells      = 11,
        ETower_DuplicateTower                = 12,
        ETowerWarning_DuplicateRingBells     = 13,
        ETower_NoRings                       = 14,
        ETowerWarning_TowerbaseIdMissing     = 15,
        ETowerWarning_DoveContainsOldChars   = 16,

        KTower_ErrorBitsetSize // this must go last
    };

    enum TMethodErrorCodes
    {
        EMethod_OrderNameMissing                  = 0,
        EMethodWarning_NameMissing                = 1,
        EMethodWarning_TypeMissing                = 2,
        EMethodWarning_MissingNotation            = 3,
        EMethod_InvalidNotation                   = 4,
        EMethodWarning_SplicedNotation            = 5,
        EMethod_DuplicateMethod                   = 6,
        EMethodWarning_MissingBobPlaceNotation    = 7,
        EMethodWarning_MissingSinglePlaceNotation = 8,

        KMethod_ErrorBitsetSize // this must go last
    };

    enum TMethodSeriesErrorCodes
    {
        ESeries_NameMissing                             = 0,

        ESeries_InvalidMethodInSeries                   = 1,
        ESeries_MethodInSeriesWithMismatchingNoOfBells  = 2,

        ESeriesWarning_MoreMethodsThanDefined           = 3,
        ESeries_DuplicateSeries                         = 4,

        KMethodSeries_ErrorBitsetSize // this must go last
    };

    enum TCompositionErrorCodes
    {
        EComposition_InvalidComposer        = 0,
        EComposition_InvalidMethod          = 1,
        EComposition_CompositionName        = 2,
        EComposition_TooFewCallings         = 3,

        KComposition_ErrorBitsetSize // this must go last
    };

    enum TRingerErrorCodes
    {
        ERinger_InvalidName             = 0,
        ERingerWarning_NoGender         = 1,
        ERinger_DuplicateName           = 2,

        KRinger_ErrorBitsetSize // this must go last
    };

    enum TAssociationErrorCodes
    {
        EAssociationWarning_NameMissing         = 0,
        EAssociation_DuplicateAssociation       = 1,
        EAssociationWarning_PealbaseLinkMissing = 2,

        KAssociation_ErrorBitsetSize // this must go last
    };
    //#define KMaxErrorBitsetSize std::max<unsigned int>(KRinger_ErrorBitsetSize, KComposition_ErrorBitsetSize, KMethod_ErrorBitsetSize, KMethod_SeriesErrorBitsetSize, KPeal_ErrorBitsetSize, KTower_ErrorBitsetSize);
    #define KMaxErrorBitsetSize 30

}

#endif //__DUCOTYPES_H__

