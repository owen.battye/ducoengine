#include "MilestoneData.h"

using namespace Duco;
using namespace std;

MilestoneData::MilestoneData(size_t newPealCount)
:   pealCount(newPealCount), lastPeal(false),
    sixMonthAverageDaysToMilestone (-1), twelveMonthAverageDaysToMilestone(-1), allTimeDaysToMilestone(-1),
    pealsInSixMonthWindow(0), pealsInTwelveMonthWindow(0), pealsInAllTimeWindow(0)
{
    pealId.ClearId();
}

MilestoneData::~MilestoneData()
{

}

bool
MilestoneData::Prediction() const
{
    return !pealId.ValidId();
}

bool
MilestoneData::LastPeal() const
{
    return lastPeal;
}

bool
MilestoneData::operator<(const Duco::MilestoneData& rhs) const
{
    return pealCount < rhs.pealCount;
}

void
MilestoneData::SetPeal(const Duco::ObjectId& thePealId, const Duco::PerformanceDate& dateOfPeal)
{
    pealId = thePealId;
    dateAchieved = dateOfPeal;
    sixMonthAverageDaysToMilestone = 0;
    twelveMonthAverageDaysToMilestone = 0;
    allTimeDaysToMilestone = 0;
}

const Duco::ObjectId&
MilestoneData::PealId() const
{
    return pealId;
}

const Duco::PerformanceDate&
MilestoneData::DateAchieved() const
{
    return dateAchieved;
}

size_t
MilestoneData::DaysToMilestoneUsing6MonthAverage() const
{
    return sixMonthAverageDaysToMilestone;
}

size_t
MilestoneData::DaysToMilestoneUsing12MonthAverage() const
{
    return twelveMonthAverageDaysToMilestone;
}

size_t
MilestoneData::DaysToMilestoneUsingAllTimeAverage() const
{
    return allTimeDaysToMilestone;
}

size_t
MilestoneData::Milestone() const
{
    return pealCount;
}

Duco::PerformanceDate
MilestoneData::StartDate(Duco::TMilestoneAverage type) const
{
    switch (type)
    {
    case TMilestoneAverage::E6MonthAverage:
        return StartDateOf6MonthAverageCalculation();

    case TMilestoneAverage::E12MonthAverage:
        return StartDateOf12MonthAverageCalculation();

    default:
        break;
    }
    return StartDateOfAllTimeCalculation();
}

Duco::PerformanceDate
MilestoneData::StartDateOf6MonthAverageCalculation() const
{
    Duco::PerformanceDate currentDate;
    currentDate.RemoveMonths(6);
    return currentDate;
}

Duco::PerformanceDate
MilestoneData::StartDateOf12MonthAverageCalculation() const
{
    Duco::PerformanceDate currentDate;
    currentDate.RemoveMonths(12);
    return currentDate;
}

Duco::PerformanceDate
MilestoneData::StartDateOfAllTimeCalculation() const
{
    return firstPealDate;
}

Duco::PerformanceDate
MilestoneData::DateOfMilestoneUsing6MonthAverage() const
{
    PerformanceDate currentDate;
    return currentDate.AddDays(sixMonthAverageDaysToMilestone);
}

Duco::PerformanceDate
MilestoneData::DateOfMilestoneUsing12MonthAverage() const
{
    PerformanceDate currentDate;
    return currentDate.AddDays(twelveMonthAverageDaysToMilestone);
}

Duco::PerformanceDate
MilestoneData::DateOfMilestoneUsingAllTimeAverage() const
{
    PerformanceDate currentDate;
    return currentDate.AddDays(allTimeDaysToMilestone);
}

size_t
MilestoneData::PealsIn6MonthWindow() const
{
    return pealsInSixMonthWindow;
}

size_t
MilestoneData::PealsIn12MonthWindow() const
{
    return pealsInTwelveMonthWindow;
}

size_t
MilestoneData::PealsInAllTimeWindow() const
{
    return pealsInAllTimeWindow;
}
