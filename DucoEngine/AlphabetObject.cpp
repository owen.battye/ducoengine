#include "AlphabetObject.h"
#include "Method.h"
#include "MethodDatabase.h"

using namespace Duco;

AlphabetObject::AlphabetObject(wchar_t newLetter, const ObjectId& newPealId, const ObjectId& newMethodId, const Duco::PerformanceDate& newDate)
:   letter(newLetter), pealId(newPealId), methodId(newMethodId), date(newDate)
{

}

AlphabetObject::~AlphabetObject()
{

}

bool
AlphabetObject::operator<(const AlphabetObject& rhs) const
{
    if (letter != rhs.letter)
        return letter < rhs.letter;

    return date.operator<(rhs.date);
}

AlphabetObject& AlphabetObject::operator=(const AlphabetObject& other)
{
    this->letter = other.letter;
    this->pealId = other.pealId;
    this->methodId = other.methodId;
    this->date = other.date;

    return *this;
}

const Duco::PerformanceDate&
AlphabetObject::Date() const
{
    return date;
}

std::wstring
AlphabetObject::MethodName(const Duco::MethodDatabase& methodDb) const
{
    const Duco::Method* theMethod = methodDb.FindMethod(MethodId(), false);
    if (theMethod != NULL)
    {
        return theMethod->FullName(methodDb);
    }
    return L"Unknown";
}
