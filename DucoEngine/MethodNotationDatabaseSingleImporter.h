#ifndef __METHODNOTATIONDATABASESINGLEIMPORTER_H__
#define __METHODNOTATIONDATABASESINGLEIMPORTER_H__

#include "MethodNotationDatabaseBase.h"
#include <map>

namespace Duco
{
class ExternalMethod;

class MethodNotationDatabaseSingleImporter : public MethodNotationDatabaseBase
{
public:
    DllExport MethodNotationDatabaseSingleImporter (Duco::MethodDatabase& newDatabase, const std::string& newDatabaseFilename, ProgressCallback* const newCallback);
    DllExport ~MethodNotationDatabaseSingleImporter();

    // new functions
    DllExport std::pair<std::multimap<unsigned int,Duco::ExternalMethod*>::const_iterator,std::multimap<unsigned int,Duco::ExternalMethod*>::const_iterator> FindMethods(unsigned int stage) const;
    DllExport std::multimap<unsigned int,Duco::ExternalMethod*>::const_iterator Begin() const;
    DllExport std::multimap<unsigned int,Duco::ExternalMethod*>::const_iterator End() const;

    DllExport const ExternalMethod* const FindMethod(const Duco::ObjectId& methodId) const;
    DllExport Duco::ObjectId FindMethod(const std::wstring& fullMethodName) const;

protected:
    // from MethodNotationDatabaseBase
    void AddMethod(const Duco::ObjectId& id, unsigned int stage, const std::wstring& name, const std::wstring& type, const std::wstring& title, const std::wstring& notation);

private:
    // order / method
    std::multimap<unsigned int, ExternalMethod*>  methods;
};

} // end namesapce Duco

#endif //!__METHODNOTATIONDATABASEUPDATER_H__
