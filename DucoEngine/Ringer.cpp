#include "Ringer.h"

#include "DatabaseWriter.h"
#include "DatabaseReader.h"
#include "DatabaseSettings.h"
#include "RingerDatabase.h"
#include "DucoEngineUtils.h"
#include "RingingDatabase.h"
#include "RingerNameChange.h"
#include "Gender.h"

#include <algorithm>
#include <ostream>
#include <time.h>

using namespace std;
using namespace Duco;

#include "DucoXmlUtils.h"
XERCES_CPP_NAMESPACE_USE

Ringer::Ringer()
: RingingObject(), forename(L""), surname(L""), notes(L""), nonHuman(false), deceased(false)
{
    gender = new Gender();
    dateOfDeath = new PerformanceDate();
    dateOfDeath->ResetToEarliest();
}

Ringer::Ringer(const Duco::ObjectId& newId, const std::wstring& newForename, const std::wstring& newSurname)
: RingingObject(newId), forename(newForename), surname(newSurname), alsoKnownAs(L""), pealbaseRingerId(L""), notes(L""), nonHuman(false), deceased(false)
{
    gender = new Gender();
    dateOfDeath = new PerformanceDate();
    dateOfDeath->ResetToEarliest();
}

Ringer::Ringer(const Ringer& other)
:   RingingObject(other), forename(other.forename), surname(other.surname),
    alsoKnownAs(other.alsoKnownAs), pealbaseRingerId(other.pealbaseRingerId),
    notes(other.notes), nonHuman(other.nonHuman), deceased(other.deceased)
{
    DeleteAllNameChanges();
    for (auto const& it : other.nameChanges)
    {
        RingerNameChange* newName = new RingerNameChange();
        newName->surname = it->surname;
        newName->forename = it->forename;
        newName->date = it->date;
        AddNameChange(newName);
    }
    gender = new Gender(*other.gender);
    dateOfDeath = new PerformanceDate(*other.dateOfDeath);

}

Ringer::Ringer(const Duco::ObjectId& ringerId, const Ringer& other)
:   RingingObject(ringerId), forename(other.forename), surname(other.surname),
    alsoKnownAs(other.alsoKnownAs), pealbaseRingerId(other.pealbaseRingerId),
    notes(other.notes), nonHuman(other.nonHuman), deceased(other.deceased)
{
    DeleteAllNameChanges();
    for (auto const& it : other.nameChanges)
    {
        RingerNameChange* newName = new RingerNameChange;
        newName->surname = it->surname;
        newName->forename = it->forename;
        newName->date = it->date;
        AddNameChange(newName);
    }
    gender = new Gender(*other.gender);
    dateOfDeath = new PerformanceDate(*other.dateOfDeath);
}

Ringer::Ringer(DatabaseReader& reader, unsigned int databaseVersion)
: RingingObject(), forename(L""), surname(L""), alsoKnownAs(L""), pealbaseRingerId(L""), notes(L""), nonHuman(false), deceased(false)
{
    gender = new Gender();
    dateOfDeath = new PerformanceDate();
    dateOfDeath->ResetToEarliest();
    Internalise(reader, databaseVersion);
}

Ringer::~Ringer()
{
    DeleteAllNameChanges();
    delete gender;
    delete dateOfDeath;
}

void
Ringer::DeleteAllNameChanges()
{
    for (auto const& it : nameChanges)
    {
        delete it;
    }
    nameChanges.clear();
}

Duco::TObjectType
Ringer::ObjectType() const
{
    return TObjectType::ERinger;
}

bool
Ringer::Duplicate(const Duco::RingingObject& other, const Duco::RingingDatabase& database) const
{
    if (other.ObjectType() != TObjectType::ERinger)
    {
        return false;
    }
    if (id == other.Id())
    {
        return false;
    }
    if (database.RingersDatabase().RingersLinked(id, other.Id()))
    {
        return false;
    }
    const Duco::Ringer& otherRinger = static_cast<const Duco::Ringer&>(other);
    if (this->gender->Defined() && otherRinger.gender->Defined() && *this->gender != *otherRinger.gender)
    {
        return false;
    }
    if (otherRinger.MatchProbability(forename, surname, true) > 2)
    {
        return true;
    }
    for (auto const& it : nameChanges)
    {
        if (otherRinger.MatchProbability(it->forename, it->surname, true) > 2)
        {
            return true;
        }
    }
    return false;
}


void
Ringer::AddNameChange(const Ringer& ringer, const PerformanceDate& dateChanged)
{
    AddNameChange(ringer.forename, ringer.surname, dateChanged);
}

void
Ringer::AddNameChange(const std::wstring& newForname, const std::wstring& newSurname, const PerformanceDate& dateChanged)
{
    RingerNameChange* newName = new RingerNameChange;
    newName->surname = newSurname;
    newName->forename = newForname;
    newName->date = dateChanged;
    AddNameChange(newName);
}

void
Ringer::InsertNameChange(const Duco::Ringer& otherRinger, const Duco::PerformanceDate& dateChanged)
{
    if (this == &otherRinger)
    {
        return;
    }
    InsertNameChange(otherRinger.Forename(), otherRinger.Surname(), dateChanged);
    for (auto const& it : otherRinger.nameChanges)
    {
        InsertNameChange((*it).forename, (*it).surname, (*it).date);
    }
}

void
Ringer::InsertNameChange(const std::wstring& newForname, const std::wstring& newSurname, const PerformanceDate& dateChanged)
{
    RingerNameChange* newName = new RingerNameChange;
    newName->surname = newSurname;
    newName->forename = newForname;
    newName->date = dateChanged;

    std::vector<RingerNameChange*>::iterator it = nameChanges.begin();

    bool akaAdded (false);
    while (!akaAdded && it != nameChanges.end())
    {
        if ((*it)->date > dateChanged)
        {
            nameChanges.insert(it, newName);
            akaAdded = true;
        }
        else
        {
            ++it;
        }
    }
    if (!akaAdded)
    {
        nameChanges.push_back(newName);
    }
}

void
Ringer::AddNameChange(RingerNameChange*const& newName)
{
    nameChanges.push_back(newName);
}

void
Ringer::DeleteNameChange(size_t index)
{
    unsigned int count (0);
    std::vector<RingerNameChange*>::iterator it = nameChanges.begin();
    while (count < index && it != nameChanges.end())
    {
        ++it;
    }
    if (it != nameChanges.end() && count == index)
        nameChanges.erase(it);
}

const Duco::RingerNameChange&
Ringer::NameChange(size_t index) const
{
    return *nameChanges.at(index);
}

std::wstring
Ringer::NameChange(size_t index, bool lastNameFirst) const
{
    const Duco::RingerNameChange* aka = nameChanges.at(index);
    std::wstring name(aka->forename);
    if (lastNameFirst)
    {
        name = aka->surname;
        name.append (L", ");
        name.append (aka->forename);
    }
    else
    {
        name.append (L" ");
        name.append (aka->surname);
    }
    return name;
}

std::wstring
Ringer::OriginalName(bool lastNameFirst) const
{
    std::wstring name(forename);
    if (lastNameFirst)
    {
        name = surname;
        name.append (L", ");
        name.append (forename);
    }
    else
    {
        name.append (L" ");
        name.append (surname);
    }

    return name;
}

std::wostream& 
Ringer::Print(std::wostream& stream, const RingingDatabase& database) const
{
    return Print(stream, PerformanceDate(), database);
}

std::wostream& 
Ringer::Print(std::wostream& stream, const PerformanceDate& date, const RingingDatabase& database) const
{
    std::wstring tempForename = forename;
    std::wstring tempSurname = surname;

    std::vector<RingerNameChange*>::const_iterator it = nameChanges.begin();
    while (it != nameChanges.end())
    {
        RingerNameChange* nextChange = *it;
        if (date >= nextChange->date)
        {
            tempForename = nextChange->forename;
            tempSurname = nextChange->surname;
            ++it;
        }
        else
        {
            it = nameChanges.end();
        }
    }
    if (database.Settings().PrintSurnameFirst())
        stream << tempSurname << ", " << tempForename;
    else
        stream << tempForename << " " << tempSurname;

    return stream;
}

DatabaseWriter& 
Ringer::Externalise(DatabaseWriter& writer) const
{
    writer.WriteId(id);
    writer.WriteString(surname);
    writer.WriteString(forename);

    writer.WriteInt(nameChanges.size());
    std::vector<RingerNameChange*>::const_iterator it =  nameChanges.begin();
    while (it != nameChanges.end())
    {
        writer.WriteString((*it)->surname);
        writer.WriteString((*it)->forename);
        writer.WriteDate((*it)->date);
        ++it;
    }
    writer.WriteString(alsoKnownAs);
    writer.WriteString(pealbaseRingerId);
    writer.WriteString(notes);
    gender->Externalise(writer);
    writer.WriteBool(nonHuman);
    writer.WriteBool(deceased);
    writer.WriteDate(*dateOfDeath);

    return writer;
}

DatabaseReader& 
Ringer::Internalise(DatabaseReader& reader, unsigned int databaseVersion)
{
    id = reader.ReadUInt();
    reader.ReadString(surname);
    reader.ReadString(forename);

    unsigned int count = reader.ReadUInt();
    while (count-- > 0)
    {
        RingerNameChange* newName = new RingerNameChange;
        reader.ReadString(newName->surname);
        reader.ReadString(newName->forename);
        if (databaseVersion == 1)
        {
            time_t date = reader.ReadTime();
            //newName->date.SetDate(dateTime);
        }
        else
        {
            if (databaseVersion <= 44)
            {
                reader.ReadUInt(); // old peal time
            }
            reader.ReadDate(newName->date);
        }

        AddNameChange(newName);
    }
    if (databaseVersion >= 20)
    {
        reader.ReadString(alsoKnownAs);
        reader.ReadString(pealbaseRingerId);
        if (databaseVersion <= 42)
        {
            std::wstring discardOldPealsCoUkRingerId;
            reader.ReadString(discardOldPealsCoUkRingerId);
        }
    }
    if (databaseVersion >= 3)
    {
        reader.ReadString(notes);
    }
    gender->Internalise(reader, databaseVersion);
    if (databaseVersion >= 44)
    {
        nonHuman = reader.ReadBool();
    }
    if (databaseVersion >= 52)
    {
        deceased = reader.ReadBool();
        reader.ReadDate(*dateOfDeath);
    }

    return reader;
}

Ringer&
Ringer::operator=(const Duco::Ringer& otherRinger)
{
    id  = otherRinger.id;
    forename = otherRinger.forename;
    surname = otherRinger.surname;

    DeleteAllNameChanges();

    for (auto const& nextNameChange : otherRinger.nameChanges)
    {
        RingerNameChange* copiedNameChange = new RingerNameChange;
        copiedNameChange->surname = nextNameChange->surname;
        copiedNameChange->forename = nextNameChange->forename;
        copiedNameChange->date = nextNameChange->date;
        nameChanges.push_back(copiedNameChange);
    }
    alsoKnownAs = otherRinger.alsoKnownAs;
    notes = otherRinger.notes;
    pealbaseRingerId = otherRinger.pealbaseRingerId;
    gender->operator=(*otherRinger.gender);
    nonHuman = otherRinger.nonHuman;
    deceased = otherRinger.deceased;
    *dateOfDeath = *otherRinger.dateOfDeath;
    errorCode = otherRinger.errorCode;

    return *this;
}

bool 
Ringer::operator==(const Duco::RingingObject& other) const
{
    if (other.ObjectType() != ObjectType())
        return false;

    const Duco::Ringer& otherRinger = static_cast<const Duco::Ringer&>(other);
    return otherRinger.id == id;
}

bool 
Ringer::operator!=(const Duco::RingingObject& other) const
{
    if (other.ObjectType() != ObjectType())
        return true;

    const Duco::Ringer& otherRinger = static_cast<const Duco::Ringer&>(other);
    return otherRinger.id != id;
}

void
Ringer::SetForename(unsigned int number, const std::wstring& newName)
{
    if (number == 0)
        forename = newName;
    else
    {
        if (nameChanges.size() >= number )
        {
            RingerNameChange* nameChange = nameChanges.at(number-1);
            nameChange->forename = newName;
        }
    }
}

const std::wstring&
Ringer::MostRecentForename() const
{
    if (!nameChanges.empty())
    {
        RingerNameChange* nameChange = nameChanges.at(nameChanges.size()-1);
        return nameChange->forename;
    }
    return forename;
}

const std::wstring&
Ringer::MostRecentSurname() const
{
    if (!nameChanges.empty())
    {
        RingerNameChange* nameChange = nameChanges.at(nameChanges.size()-1);
        return nameChange->surname;
    }
    return surname;
}

void
Ringer::SetSurname(unsigned int number, const std::wstring& newName)
{
    if (number == 0)
        surname = newName;
    else
    {
        if (nameChanges.size() >= number)
        {
            RingerNameChange* nameChange = nameChanges.at(number-1);
            nameChange->surname = newName;
        }
    }
}

void
Ringer::SetDateChanged(unsigned int number, const PerformanceDate& newDate)
{
    if (number > 0)
    {
        if (nameChanges.size() >= number )
        {
            RingerNameChange* nameChange = nameChanges.at(number-1);
            nameChange->date = newDate;
        }
    }
}

bool 
Ringer::NameContainsSubstring(const DatabaseSettings& settings, const std::wstring& substring, bool subStr) const
{
    std::wstring fullname;
    if (settings.LastNameFirst())
    {
        fullname = surname;
        fullname.append (L", ");
        fullname.append (forename);
    }
    else
    {
        fullname = forename;
        fullname.append (L" ");
        fullname.append (surname);
    }
    if (DucoEngineUtils::MatchString(fullname, substring, subStr))
        return true;

    std::vector<RingerNameChange*>::const_reverse_iterator it = nameChanges.rbegin();
    bool nameFound = false;
    while (it != nameChanges.rend() && !nameFound)
    {
        const RingerNameChange* nextNameChange = *it;
        if (settings.LastNameFirst())
        {
            fullname = nextNameChange->surname;
            fullname.append (L", ");
            fullname.append (nextNameChange->forename);
        }
        else
        {
            fullname = nextNameChange->forename;
            fullname.append (L" ");
            fullname.append (nextNameChange->surname);
        }

        nameFound = DucoEngineUtils::MatchString(fullname, substring, subStr);

        ++it;
    }
    return nameFound;
}

bool
Ringer::ForenameContains(const std::wstring& substring, bool subStr, bool includeAka) const
{
    if (alsoKnownAs.length() > 0 && DucoEngineUtils::FindSubstring(alsoKnownAs, substring))
        return true;

    if (DucoEngineUtils::MatchString(forename, substring, subStr))
        return true;

    std::vector<RingerNameChange*>::const_reverse_iterator it = nameChanges.rbegin();
    bool nameFound = false;
    while (it != nameChanges.rend() && !nameFound)
    {
        nameFound = DucoEngineUtils::MatchString((*it)->forename, substring, subStr);
        ++it;
    }
    return nameFound;
}

bool
Ringer::SurnameContains(const std::wstring& substring, bool subStr, bool includeAka) const
{
    if (alsoKnownAs.length() > 0 && DucoEngineUtils::FindSubstring(alsoKnownAs, substring))
        return true;

    if (DucoEngineUtils::MatchString(surname, substring, subStr))
        return true;

    std::vector<RingerNameChange*>::const_reverse_iterator it = nameChanges.rbegin();
    bool nameFound = false;
    while (it != nameChanges.rend() && !nameFound)
    {
        nameFound = DucoEngineUtils::MatchString((*it)->surname, substring, subStr);
        ++it;
    }
    return nameFound;
}

std::wstring
Ringer::FullName(const PerformanceDate& date, bool lastNameFirst) const
{
    std::wstring fullname (L"");
    if (lastNameFirst)
    {
        fullname = surname;
        if (surname.length() > 0 && forename.length() > 0)
        {
            fullname.append(L", ");
        }
        fullname.append (forename);
    }
    else
    {
        fullname = forename;
        if (surname.length() > 0 && forename.length() > 0)
        {
            fullname.append(L" ");
        }
        fullname.append (surname);
    }

    std::vector<RingerNameChange*>::const_reverse_iterator it = nameChanges.rbegin();
    bool nameFound (false);
    while (it != nameChanges.rend() && !nameFound)
    {
        const RingerNameChange* nextNameChange = *it;
        if (nextNameChange->date <= date)
        {
            if (lastNameFirst)
            {
                fullname = nextNameChange->surname;
                fullname.append (L", ");
                fullname.append (nextNameChange->forename);
            }
            else
            {
                fullname = nextNameChange->forename;
                fullname.append (L" ");
                fullname.append (nextNameChange->surname);
            }
            nameFound= true;
        }

        ++it;
    }
    return DucoEngineUtils::Trim(fullname);
}

std::wstring
Ringer::FullName(bool lastNameFirst) const
{
    PerformanceDate currentDate;
    currentDate.Clear();
    return FullName(currentDate, lastNameFirst);
}

std::wstring
Ringer::FullNameForSort(bool lastNameFirst) const
{
    PerformanceDate currentDate;
    currentDate.Clear();
    const std::wstring fullName (FullName(currentDate, lastNameFirst));
    std::wstring lowerCaseFullName;
    DucoEngineUtils::ToLowerCase(fullName, lowerCaseFullName);

    lowerCaseFullName.append(Id().Str());

    return lowerCaseFullName;
}

bool
Ringer::Valid(const RingingDatabase& database, bool warningFatal, bool clearBeforeStart) const
{
    if (clearBeforeStart)
    {
        ClearErrors();
    }
    bool errorFound (false);
    bool warningFound (false);

    if ((surname.length() <= 0 || forename.length() <= 0))
    {
        errorFound = true;
        errorCode.set(ERinger_InvalidName);
    }

    if (!gender->Defined() && !nonHuman)
    {
        if (warningFatal)
            warningFound = true;
        errorCode.set(ERingerWarning_NoGender);
    }

    if (errorFound || (warningFatal && warningFound))
        return false;

    return true;
}

std::wstring
Ringer::ErrorString(const Duco::DatabaseSettings& /*settings*/, bool showWarnings) const
{
    std::wstring errorStr;
    if (errorCode.test(ERinger_InvalidName))
    {
        DucoEngineUtils::AddError(errorStr, L"Missing forename or surname");
    }
    if (showWarnings && errorCode.test(ERingerWarning_NoGender))
    {
        DucoEngineUtils::AddError(errorStr, L"Gender not set");
    }
    if (errorCode.test(ERinger_DuplicateName))
    {
        DucoEngineUtils::AddError(errorStr, L"Ringer similar to another");
    }

    return errorStr;
}

void
Ringer::SetError(TRingerErrorCodes code)
{
    errorCode.set(code, 1);
}

bool
Ringer::Male() const
{
    return gender->Male();
}

bool
Ringer::Female() const
{
    return gender->Female();
}

bool
Ringer::NonHuman() const
{
    return nonHuman;
}

void
Ringer::SetMale()
{
    gender->SetMale();
}

void
Ringer::SetFemale()
{
    gender->SetFemale();
}

void
Ringer::SetNonHuman(bool newSetNonHuman)
{
    nonHuman = newSetNonHuman;
}

void
Ringer::SetAlsoKnownAs(const std::wstring& newName)
{
    alsoKnownAs = DucoEngineUtils::Trim(newName);
}

bool
Ringer::IdenticalRinger(const std::wstring& forenamesToFind, const std::wstring& surnameToFind) const
{ // Doesnt even check case, because this is from import - names should be IDENTICAL.
    if (surnameToFind.compare(surname) == 0 && forenamesToFind.compare(forename) == 0)
    {
        return true;
    }

    for (auto const& it : nameChanges)
    {
        if (surnameToFind.compare(it->surname) == 0 && forenamesToFind.compare(it->forename) == 0)
        {
            return true;
        }
    }
    if (alsoKnownAs.length() > 0)
    {
        std::wstring existingRingerFirstname;
        std::wstring existingRingerSurname;
        bool ignore;
        DucoEngineUtils::SplitRingerName(alsoKnownAs, existingRingerFirstname, existingRingerSurname, ignore);
        if (surnameToFind.compare(existingRingerSurname) == 0 && forenamesToFind.compare(existingRingerFirstname) == 0)
        {
            return true;
        }
    }
    return false;
}

bool
Ringer::CapitaliseField()
{
    bool changed = false;
    {
        std::wstring newForename = DucoEngineUtils::ToCapitalised(forename, true);
        if (forename.compare(newForename) != 0)
        {
            forename = newForename;
            changed = true;
        }
        std::wstring newSurname = DucoEngineUtils::ToCapitalised(surname, true);
        if (surname.compare(newSurname) != 0)
        {
            surname = newSurname;
            changed = true;
        }
    }

    for (auto const& it : nameChanges)
    {
        std::wstring newForename = DucoEngineUtils::ToCapitalised(it->forename, true);
        if (it->forename.compare(newForename) != 0)
        {
            it->forename = newForename;
            changed = true;
        }
        std::wstring newSurname = DucoEngineUtils::ToCapitalised(it->surname, true);
        if (it->surname.compare(newSurname) != 0)
        {
            it->surname = newSurname;
            changed = true;
        }
    }
    return changed;
}

bool
Ringer::AnyMatch(const std::wstring& fullNameToMatchWith) const
{
    if (fullNameToMatchWith.length() < 2)
    {
        return false;
    }   
    if (fullNameToMatchWith.find_first_of(' ') == std::wstring::npos)
    {
        if (DucoEngineUtils::FindSubstring(fullNameToMatchWith, forename))
        {
            return true;
        }
        else if (DucoEngineUtils::FindSubstring(fullNameToMatchWith, surname))
        {
            return true;
        }
        else if (DucoEngineUtils::FindSubstring(fullNameToMatchWith, alsoKnownAs))
        {
            return true;
        }   
    }
    else
    {
        std::wstring firstNamesToMatchWith;
        std::wstring surnameNameToMatchWith;
        bool isConductor;
        DucoEngineUtils::SplitRingerName(fullNameToMatchWith, firstNamesToMatchWith, surnameNameToMatchWith, isConductor);
        bool forenameMatch = DucoEngineUtils::FindSubstring(firstNamesToMatchWith, forename);
        bool surnameMatch = DucoEngineUtils::FindSubstring(surnameNameToMatchWith, surname);
        if (forenameMatch && surnameMatch)
        {
            return true;
        }
        if (forenameMatch && alsoKnownAs.length() > 0 && DucoEngineUtils::FindSubstring(surnameNameToMatchWith, surname))
        {
            return true;
        }
        if (surnameMatch && alsoKnownAs.length() > 0 && DucoEngineUtils::FindSubstring(firstNamesToMatchWith, forename))
        {
            return true;
        }
    }
    return false;
}

unsigned int
Ringer::MatchProbability(const std::wstring& firstNames, const std::wstring& lastName, const std::wstring& firstnamesToMatchWithoutPunctuation, const std::wstring& surnamesToMatchWithoutPunctuation, const std::wstring& initialsFromNameToMatch, bool partialMatchAllowed)
{
    unsigned int returnVal = 0;
    const std::wstring firstNamesWithoutPunctuation = DucoEngineUtils::RemovePunctuationFromFirstNames(firstNames);
    const std::wstring lastNameWithoutPunctuation = DucoEngineUtils::RemovePunctuationFromFirstNames(lastName);

    const std::wstring fullNameWithoutPunctuation = firstNamesWithoutPunctuation + L" " + lastNameWithoutPunctuation;
    const std::wstring fullNameToMatchWithoutPunctuation = firstnamesToMatchWithoutPunctuation + L" " + surnamesToMatchWithoutPunctuation;
    if (DucoEngineUtils::CompareString(fullNameToMatchWithoutPunctuation, fullNameWithoutPunctuation))
    {
        returnVal += 5;
    }
    else
    {
        const std::wstring fullNamelastNameFirstWithoutPunctuation = lastNameWithoutPunctuation + L" " + firstNamesWithoutPunctuation;
        if (DucoEngineUtils::CompareString(fullNameToMatchWithoutPunctuation, fullNamelastNameFirstWithoutPunctuation))
        {
            returnVal += 4;
        }
        else if (partialMatchAllowed)
        {
            // Min no of chars to match at 3 was a guess - to prevent short names - or names without a sur/forname matching to easily.
            bool lastNameFound = DucoEngineUtils::FindSubstring(lastNameWithoutPunctuation, surnamesToMatchWithoutPunctuation, L"", false, 3);
            if (lastNameFound)
            {
                bool firstNameFound = DucoEngineUtils::FindSubstring(firstNamesWithoutPunctuation, firstnamesToMatchWithoutPunctuation, L"", true, 3);
                std::wstring initials = DucoEngineUtils::GetInitialsFromName(firstNames);
                if (firstNameFound)
                {
                    if (DucoEngineUtils::FindSubstring(initials, initialsFromNameToMatch, L"", true, 1))
                    {
                        returnVal += 3;
                    }
                    else
                    {
                        returnVal += 1;
                    }
                }
                else if (partialMatchAllowed)
                {
                    // Check first name Initials
                    if (DucoEngineUtils::FindSubstring(initials, initialsFromNameToMatch, L"", true, min(initials.length(), initialsFromNameToMatch.length())))
                    {
                        returnVal += 2;
                    }
                }
            }
        }
    }
    return returnVal;
}

unsigned int
Ringer::MatchProbability(const std::wstring& fullNameToMatchWith, bool partialMatchAllowed) const
{
    std::wstring firstNamesToMatchWith;
    std::wstring surnameNameToMatchWith;
    bool isConductor;
    DucoEngineUtils::SplitRingerName(fullNameToMatchWith, firstNamesToMatchWith, surnameNameToMatchWith, isConductor);
    return MatchProbability(firstNamesToMatchWith, surnameNameToMatchWith, partialMatchAllowed);
}

unsigned int
Ringer::MatchProbability(const std::wstring& firstNamesToMatchWith, const std::wstring& surnameNameToMatchWith, bool partialMatchAllowed) const
{

    unsigned int returnVal = 0;

    const std::wstring firstnamesToMatchWithoutPunctuation = DucoEngineUtils::RemovePunctuationFromFirstNames(firstNamesToMatchWith);
    const std::wstring surnamesToMatchWithoutPunctuation = DucoEngineUtils::RemovePunctuationFromFirstNames(surnameNameToMatchWith);
    const std::wstring initialsFromNameToMatch = DucoEngineUtils::GetInitialsFromName(firstNamesToMatchWith);

    returnVal = MatchProbability(forename, surname, firstnamesToMatchWithoutPunctuation, surnamesToMatchWithoutPunctuation, initialsFromNameToMatch, partialMatchAllowed);
    for (auto const& it : nameChanges)
    {
        unsigned int nameChangesMatchProbability = MatchProbability(it->forename, it->surname, firstNamesToMatchWith, surnameNameToMatchWith, initialsFromNameToMatch, partialMatchAllowed);
        returnVal = std::max<unsigned int>(nameChangesMatchProbability, returnVal);
    }
    if (alsoKnownAs.length() > 0)
    {
        std::wstring existingRingerFirstname;
        std::wstring existingRingerSurname;
        bool ignore;
        DucoEngineUtils::SplitRingerName(alsoKnownAs, existingRingerFirstname, existingRingerSurname, ignore);
        unsigned int akaMatchProbability = MatchProbability(existingRingerFirstname, existingRingerSurname, firstNamesToMatchWith, surnameNameToMatchWith, initialsFromNameToMatch, partialMatchAllowed);
        if (akaMatchProbability > 2)
        {
            returnVal += 3;
        }
        else if (existingRingerFirstname.length() == 0)
        {
            existingRingerFirstname = existingRingerSurname;
            existingRingerSurname = surname;
            akaMatchProbability = MatchProbability(existingRingerFirstname, existingRingerSurname, firstNamesToMatchWith, surnameNameToMatchWith, initialsFromNameToMatch, partialMatchAllowed);
            if (akaMatchProbability > 1)
            {
                returnVal += 2;
            }
        }
    }
    if (!partialMatchAllowed && returnVal <= 2)
    {
        returnVal = 0;
    }

    return returnVal;
}

bool
Ringer::AkaRequired(const Duco::PerformanceDate& pealDate, size_t& akaNo) const
{
    bool akaRequired (false);
    akaNo = 0;

    std::vector<RingerNameChange*>::const_iterator it =  nameChanges.begin();
    bool stop (false);
    while (it != nameChanges.end() && !stop)
    {
        if ((*it)->date > pealDate)
        {
            stop = true;
        }
        else
        {
            akaRequired = true;
            ++akaNo;
        }
        ++it;
    }
    return akaRequired;
}

void Ringer::ExportToCsv(std::wofstream& file) const
{

}

void
Ringer::ExportToXml(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& ringersParent, bool exportDucoObjects) const
{
    DOMElement* newRinger = outputFile.createElement(XMLStrL("Ringer"));
    ringersParent.appendChild(newRinger);

    DucoXmlUtils::AddObjectId(*newRinger, id);
    DucoXmlUtils::AddElement(outputFile, *newRinger, L"Forename", forename);
    DucoXmlUtils::AddElement(outputFile, *newRinger, L"Surname", surname);
    if (nonHuman)
    {
        DucoXmlUtils::AddAttribute(*newRinger, L"NonHuman", L"True");
    }
    else if (gender->Male())
    {
        DucoXmlUtils::AddAttribute(*newRinger, L"Gender", L"Male");
    }
    else if (gender->Female())
    {
        DucoXmlUtils::AddAttribute(*newRinger, L"Gender", L"Female");
    }
    DucoXmlUtils::AddElement(outputFile, *newRinger, L"Notes", notes, false);
    DucoXmlUtils::AddElement(outputFile, *newRinger, L"AlsoKnownAs", alsoKnownAs, false);

    DucoXmlUtils::AddAttribute(*newRinger, L"PealbaseRingerId", pealbaseRingerId, false);

    if (nameChanges.size() > 0)
    {
        DOMElement* nameChangesElement = outputFile.createElement(XMLStrL("NameChanges"));
        newRinger->appendChild(nameChangesElement);

        for (auto const& it : nameChanges)
        {
            DOMElement* nameChange = DucoXmlUtils::AddElement(outputFile, *nameChangesElement, L"NameChange", L"", true);
            DucoXmlUtils::AddElement(outputFile, *nameChange, L"Forename", it->forename, true);
            DucoXmlUtils::AddElement(outputFile, *nameChange, L"Surname", it->surname, true);
            DucoXmlUtils::AddAttribute(*nameChange, L"Date", it->date.Str());
        }
    }
}

const std::wstring&
Ringer::Forename() const
{
    return forename;
}

const std::wstring&
Ringer::Surname() const
{
    return surname;
}

size_t
Ringer::NoOfNameChanges() const
{
    return nameChanges.size();
}

const std::wstring&
Ringer::Notes() const
{
    return notes;
}

void
Ringer::SetNotes(const std::wstring& newNotes)
{
    notes = newNotes;
}

void
Ringer::SetPealbaseReference(const std::wstring& newRef)
{
    pealbaseRingerId = newRef;
}

const std::wstring&
Ringer::PealbaseReference() const
{
    return pealbaseRingerId;
}

const std::wstring&
Ringer::AlsoKnownAs() const
{
    return alsoKnownAs;
}

bool
Ringer::Deceased() const
{
    return deceased;
}

bool
Ringer::DateOfDeath(PerformanceDate& dateOfDeath) const
{
    if (deceased && this->dateOfDeath->Valid())
    {
        dateOfDeath = *this->dateOfDeath;
        return true;
    }
    dateOfDeath.ResetToEarliest();
    return false;
}
void
Ringer::SetDeceased(bool deceased)
{
    this->deceased = deceased;
}

void
Ringer::SetDateOfDeath(const PerformanceDate& dateOfDeath)
{
    deceased = dateOfDeath.Valid();
    *this->dateOfDeath = dateOfDeath;
}

