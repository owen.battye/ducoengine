#ifndef __INVALIDDATABASEVERSIONEXCEPTION_H__
#define __INVALIDDATABASEVERSIONEXCEPTION_H__

#include <exception>
#include "DucoEngineCommon.h"

const int KInvalidDatabaseVersionError = -2;

namespace Duco
{
    class InvalidDatabaseVersionException : public std::exception 
    {
    public:
        DllExport InvalidDatabaseVersionException(int dbVersion, int expectedDBVersion);
        DllExport virtual const char* what() const throw();

        inline int ErrorCode() const;

    protected:
        int actualDatabaseVersion;
        int expectedMaximumDatabaseVersion;
    };

int
InvalidDatabaseVersionException::ErrorCode() const
{
    return KInvalidDatabaseVersionError;
}

}
#endif //__INVALIDDATABASEVERSIONEXCEPTION_H__
