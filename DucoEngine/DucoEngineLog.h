#ifndef __DUCOENGINELOG_H__
#define __DUCOENGINELOG_H__

#include <cstdio>
#include <string>
#include <chrono>

#define DUCOENGINELOG(a) DucoEngineLog::Print(__FILE__, __LINE__, a)
#define DUCOENGINELOG2(a, b) DucoEngineLog::Print(__FILE__, __LINE__, a, b)
#define DUCOENGINELOG3(a, b, c) DucoEngineLog::Print(__FILE__, __LINE__, a, b, c)
#ifdef _DEBUG
#define DUCOENGINEDEBUGLOG2(a, b) DucoEngineLog::Debug(__FILE__, __LINE__, a, b )
#define DUCOENGINEDEBUGLOG3(a, b, c) DucoEngineLog::Debug(__FILE__, __LINE__, a, b, c )
#define DUCOENGINEDEBUGLOG4(a, b, c, d) DucoEngineLog::Debug(__FILE__, __LINE__, a, b, c, d )
#define DUCOENGINEDEBUGLOG5(a, b, c, d, e) DucoEngineLog::Debug(__FILE__, __LINE__, a, b, c, d, e )
#else
#define DUCOENGINEDEBUGLOG2(a, b)
#define DUCOENGINEDEBUGLOG3(a, b, c)
#define DUCOENGINEDEBUGLOG4(a, b, c, d)
#define DUCOENGINEDEBUGLOG5(a, b, c, d, e)
#endif

namespace Duco
{

class DucoEngineLog
{
public:
    static void Print(const char* filename, int linenumber, const wchar_t* text);
    static void Print(const char* filename, int linenumber, const char* text, ...);
    static void Debug(const char* filename, int linenumber, char const* _Format, ...);
    static void Debug(const char* filename, int linenumber, const std::chrono::time_point<std::chrono::high_resolution_clock>& start, char const* _Format, ...);

};

} // end namespace Duco

#endif //!__DucoEngineLog_H__
