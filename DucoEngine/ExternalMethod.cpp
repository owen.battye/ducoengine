#include "ExternalMethod.h"

using namespace Duco;

ExternalMethod::ExternalMethod(const Duco::ObjectId& newId, const std::wstring& newName, const std::wstring& newType, const std::wstring& newTitle, const std::wstring& newNotation, unsigned int newStage)
:   id(newId), stage(newStage)
{
    name = new std::wstring(newName);
    type = new std::wstring(newType);
    title = new std::wstring(newTitle);
    notation = new std::wstring(newNotation);
}

ExternalMethod::~ExternalMethod()
{
    delete name;
    delete type;
    delete title;
    delete notation;
}

const Duco::ObjectId&
ExternalMethod::Id() const
{
    return id;
}

const std::wstring&
ExternalMethod::Name() const
{
    if (name == NULL || name->length() == 0)
    {
        return *title;
    }
    return *name;
}


const std::wstring&
ExternalMethod::Type() const
{
    return *type;
}

const std::wstring&
ExternalMethod::Title() const
{
    return *title;
}

const std::wstring&
ExternalMethod::Notation() const
{
    return *notation;
}
