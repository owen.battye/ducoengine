#include "RingerCirclingData.h"
#include "PerformanceDate.h"
#include <algorithm>

using namespace Duco;
using namespace std;

RingerCirclingData::RingerCirclingData(const Duco::ObjectId& newRingerId)
    : ringerId(newRingerId)
{
}

RingerCirclingData::~RingerCirclingData()
{
}

bool
RingerCirclingData::Circled(size_t numberOfBellsInTower) const
{
    if (pealCounts.size() != numberOfBellsInTower)
        return false;

    return true;
}

bool
RingerCirclingData::FirstCircleDate(Duco::PerformanceDate& dateCircled, size_t numberOfBellsInTower) const
{
    dateCircled.Clear();
    if (!Circled(numberOfBellsInTower))
        return false;

    PerformanceDate firstCircledDate;
    firstCircledDate.ResetToEarliest();

    std::map<size_t, BellCirclingData>::const_iterator it = pealCounts.begin();
    while (it != pealCounts.end())
    {
        PerformanceDate bellFirstCircledDate = it->second.FirstPealDate();
        if (firstCircledDate < bellFirstCircledDate)
        {
            firstCircledDate = bellFirstCircledDate;
        }
        ++it;
    }

    dateCircled = firstCircledDate;
    return true;
}

bool
RingerCirclingData::LastCircleDate(Duco::PerformanceDate& dateCircled, size_t& numberOfCircles, size_t numberOfBellsInTower) const
{
    dateCircled.Clear();
    numberOfCircles = 0;

    if (!Circled(numberOfBellsInTower))
        return false;

    numberOfCircles = 99;
    std::map<size_t, BellCirclingData>::const_iterator it = pealCounts.begin();
    while (it != pealCounts.end())
    {
        numberOfCircles = std::min<size_t>(numberOfCircles, it->second.PealCount());
        ++it;
    }

    if (numberOfCircles <= 1)
        return false;

    PerformanceDate lastCircledDate;
    lastCircledDate.ResetToEarliest();

    it = pealCounts.begin();
    while (it != pealCounts.end())
    {
        PerformanceDate bellLastCircledDate = it->second.PealDate(numberOfCircles);
        if (lastCircledDate < bellLastCircledDate)
        {
            lastCircledDate = bellLastCircledDate;
        }
        ++it;
    }

    dateCircled = lastCircledDate;
    return true;
}

void
RingerCirclingData::AddPeal(size_t realBellNumber, const Duco::PerformanceDate& pealDate)
{
    std::map<size_t, BellCirclingData>::iterator oldValue = pealCounts.find(realBellNumber);
    if (oldValue == pealCounts.end())
    {
        BellCirclingData newData(pealDate);
        pealCounts.insert(std::pair<size_t, BellCirclingData>(realBellNumber, newData));
    }
    else
    {
        oldValue->second.AddPeal(pealDate);
    }
}

const Duco::ObjectId&
RingerCirclingData::RingerId() const
{
    return ringerId;
}
