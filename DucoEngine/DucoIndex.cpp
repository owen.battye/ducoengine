#include "DucoIndex.h"

#include "RingingObject.h"

using namespace Duco;
using namespace std;

DucoIndex::DucoIndex(const Duco::ObjectId& objectId, Duco::RingingObject* object)
    : objId(objectId), obj(object)
{
}

DucoIndex::~DucoIndex()
{
    objId.ClearId();
    obj = NULL;
}

const Duco::ObjectId&
DucoIndex::Id() const
{
    return objId;
}

Duco::RingingObject*
DucoIndex::Object() const
{
    return obj;
}