#include "DummyProgressCallback.h"

using namespace Duco;

void
DummyProgressCallback::Initialised()
{

}

void
DummyProgressCallback::Step(int progressPercent)
{

}

void
DummyProgressCallback::Complete(bool error)
{

}
