#ifndef __SEARCHVALIDOBJECT_H__
#define __SEARCHVALIDOBJECT_H__

#include "SearchValidationArgument.h"

namespace Duco
{
class TSearchValidObject : public TSearchValidationArgument
{
public:
    DllExport explicit TSearchValidObject(bool includeWarnings);
    bool Match(const Duco::Peal& peal, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const;
    bool Match(const Duco::Tower& tower, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const;
    bool Match(const Duco::Ringer& ringer, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const;
    bool Match(const Duco::Method& method, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const;
    bool Match(const Duco::MethodSeries& series, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const;
    virtual Duco::TFieldType FieldType() const;
    ~TSearchValidObject();

    inline bool IncludeWarnings() const;
    virtual std::set<Duco::ObjectId> ErrorIds() const;

private:
    bool includeWarnings;
};

bool
TSearchValidObject::IncludeWarnings() const
{
    return includeWarnings;
}

}

#endif //!__SEARCHVALIDOBJECT_H__
