#include "Rollup.h"

#include "DucoEngineUtils.h"
#include "Change.h"
#include <cstdlib>

using namespace Duco;
using namespace std;

Rollup::Rollup(const std::wstring& description)
:   noOfBells(-1), noOfMatches(0)
{
    size_t endOfNoOfBells = description.find('.', 0);
    if (endOfNoOfBells != std::wstring::npos)
    {
        noOfBells = _wtoi(description.substr(1, endOfNoOfBells - 1).c_str());
    }
    size_t endOfBells = description.find('.', endOfNoOfBells+1);
    SetBells(description.substr(endOfNoOfBells+1, endOfBells-endOfNoOfBells-1));

    name = new std::wstring(description.substr(endOfBells+1));
}

Rollup::~Rollup()
{
    delete name;
}

bool
Rollup::Match(Change& change) const
{
    if (change.Order() != Order())
        return false;

    std::list<unsigned int>::const_iterator it = bells.begin();
    unsigned int startingPosition (-1);
    for (unsigned int i(0); i < change.Order(); ++i)
    {
        unsigned int nextBell = change[i];
        if (nextBell == *it)
        {
            if (startingPosition == -1)
                startingPosition = i;

            ++it;
            if (it == bells.end())
            {
                ++noOfMatches;
                change.SetContainsRollup(startingPosition, i);
                return true;
            }
        }
        else if (it != bells.begin())
            return false;
    }
    return false;
}

void
Rollup::SetBells(const std::wstring& str)
{
    bells.clear();
    std::wstring::const_iterator it = str.begin();
    while (it != str.end())
    {
        unsigned int bellNo = DucoEngineUtils::ToInteger(*it);
        bells.push_back(bellNo);
        ++it;
    }
}

std::wstring
Rollup::Print() const
{
    std::wstring returnStr (DucoEngineUtils::ToString(noOfMatches));
    returnStr += L": ";
    returnStr += *name;

    returnStr += L" (";
    std::list<unsigned int>::const_iterator it = bells.begin();
    while (it != bells.end())
    {
        returnStr += DucoEngineUtils::ToChar(*it);
        ++it;
    }
    returnStr += L")";

    return returnStr;
}

unsigned int
Rollup::Count() const
{
    return noOfMatches;
}
