#ifndef __LEADTYPE_H__
#define __LEADTYPE_H__

namespace Duco
{
    enum TLeadType
    {
        EPlainLead,
        EBobLead,
        ESingleLead
    };
}

#endif //!__LEADTYPE_H__
