#include "UppercaseField.h"

#include "Peal.h"
#include "Tower.h"
#include "Method.h"
#include "DucoEngineUtils.h"

using namespace Duco;

TUppercaseField::TUppercaseField(Duco::TUpdateFieldId newFieldId)
: TUpdateArgument(newFieldId)
{

}

TUppercaseField::~TUppercaseField()
{

}

Duco::TFieldType
TUppercaseField::FieldType() const
{
    return TFieldType::EStringField;
}

bool
TUppercaseField::Update(Duco::Method& object) const
{
    switch (fieldId)
    {
    case EMethod_Name:
        {
        std::wstring inUpper;
        DucoEngineUtils::ToUpperCase(object.Name(), inUpper);
        object.SetName(inUpper);
        return true;
        }
    case EMethod_Type:
        {
        std::wstring inUpper;
        DucoEngineUtils::ToUpperCase(object.Type(), inUpper);
        object.SetType(inUpper);
        return true;
        }
    default:
        break;       
    }
    return false;
}

bool
TUppercaseField::Update(Duco::Peal& object) const
{
    switch (fieldId)
    {
    case EPeal_Composer:
        {
        std::wstring inUpper;
        DucoEngineUtils::ToUpperCase(object.Composer(), inUpper);
        object.SetComposer(inUpper);
        return true;
        }
    case EPeal_Footnotes:
        {
        std::wstring inUpper;
        DucoEngineUtils::ToUpperCase(object.Footnotes(), inUpper);
        object.SetFootnotes(inUpper);
        return true;
        }
    default:
        return false;
    }
    return false;
}

bool
TUppercaseField::Update(Duco::Tower& object) const
{
    switch (fieldId)
    {
    case ETower_Name:
        {
            std::wstring inUpper;
            DucoEngineUtils::ToUpperCase(object.Name(), inUpper);
            object.SetName(inUpper);
            return true;
        }
        break;
    case ETower_City:
        {
            std::wstring inUpper;
            DucoEngineUtils::ToUpperCase(object.City(), inUpper);
            object.SetCity(inUpper);
            return true;
        }
        break;

    default:
        return false;
    }
    return false;
}
