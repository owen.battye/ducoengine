#include "PlaceNotationCrossObject.h"
#include "PlaceNotationCommon.h"

using namespace std;
using namespace Duco;

#include "DucoEngineUtils.h"
#include "Change.h"

PlaceNotationCrossObject::PlaceNotationCrossObject(TChangeType newType)
:   PlaceNotationObject(KCrossSymbolStr, newType)
{
}

PlaceNotationCrossObject::~PlaceNotationCrossObject()
{
}

PlaceNotationCrossObject::PlaceNotationCrossObject(const PlaceNotationCrossObject& other)
    : PlaceNotationObject(other)
{

}

PlaceNotationObject*
PlaceNotationCrossObject::Realloc() const
{
    PlaceNotationCrossObject* newObj = new PlaceNotationCrossObject(*this);
    return newObj;
}

bool
PlaceNotationCrossObject::RequiresSeperator() const
{
    return false;
}

unsigned int
PlaceNotationCrossObject::BestStartingLead() const
{
    return -1;
}

Change
PlaceNotationCrossObject::ProcessRow(const Change& change) const
{
    Change nextChange(change);

    unsigned int i(1);
    while (nextChange.SwapPair(i))
    {
        i += 2;
    }

    nextChange.SetChangeType(changeType);
    return nextChange;
}
