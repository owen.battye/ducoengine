#ifndef __RINGERCIRCLINGDATA_H__
#define __RINGERCIRCLINGDATA_H__

#include "ObjectId.h"
#include "BellCirclingData.h"
#include <map>

namespace Duco
{
    class PerformanceDate;

    class RingerCirclingData
    {
    public:
        RingerCirclingData() = delete;
        DllExport explicit RingerCirclingData(const Duco::ObjectId& ringerId);
        DllExport ~RingerCirclingData();
        void AddPeal(size_t realBellNumber, const Duco::PerformanceDate& pealDate);

        DllExport bool Circled(size_t numberOfBellsInTower) const;
        DllExport bool FirstCircleDate(Duco::PerformanceDate& dateCircled, size_t numberOfBellsInTower) const;
        DllExport bool LastCircleDate(Duco::PerformanceDate& dateCircled, size_t& numberOfCircles, size_t numberOfBellsInTower) const;
        DllExport const Duco::ObjectId& RingerId() const;

    private:
        Duco::ObjectId ringerId;
        std::map<size_t, Duco::BellCirclingData> pealCounts;
    };
}

#endif //! __RINGERCIRCLINGDATA_H__