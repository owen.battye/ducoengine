#ifndef __DOVEDATABASE_H__
#define __DOVEDATABASE_H__

#include <fstream>
#include <map>
#include <set>
#include "DoveFileConfiguration.h"
#include "DucoEngineCommon.h"
#include "ImportCancellationCallback.h"
#include "ProgressCallback.h"

namespace Duco
{
    class AbbreviationList;
    class DoveObject;
    class DoveTowerValidator;
    class ObjectId;
    class Tower;
    class TowerDatabase;

class DoveDatabase
{
public:
    DllExport DoveDatabase(const std::string& filename, Duco::ProgressCallback* callback, const std::string& installationDir, bool convertToFullNames);
    DllExport ~DoveDatabase();

    DllExport void Clear();
    DllExport bool ConfigValid() const;
    DllExport bool Import();
    DllExport size_t NoOfTowers() const;
    // Find tower by dove id is used by the DoveSearch class
    DllExport bool FindTower(const std::wstring& doveId, std::set<Duco::DoveObject>& foundTowers) const;
    DllExport bool FindTower(const std::wstring& doveId, size_t numberOfBells, Duco::DoveObject& foundTower) const;
    DllExport bool FindTowerByTowerbaseId(const std::wstring& towerbaseId, std::set<Duco::DoveObject>& foundTowers) const;
    //SuggestTower is used by the UI to import dove data to existing towers
    DllExport bool SuggestTower(const Duco::TowerDatabase& towersDatabase, const Duco::Tower& theTowerToFindInDove, Duco::DoveObject& foundTower, Duco::ObjectId& existingTowerWithId) const;
    // SuggestTower with individual parameters is used by BellboardPerformanceParser
    DllExport const Duco::DoveObject* const SuggestTower(unsigned int noOfBells, const std::wstring& towerDedication, const std::wstring& towerName, const std::wstring& towerCounty, const std::wstring& tenorWeight, const std::wstring& tenorKey) const;
    DllExport size_t ImportTowerIdAndPosition(Duco::TowerDatabase& towers, const std::map<std::wstring, Duco::ObjectId>& doveIdAndTowerIds) const;
    DllExport bool ImportMissingTenorKeyAndIds(Duco::Tower& tower) const;
    DllExport size_t ImportPositionAndIds(Duco::TowerDatabase& towers, std::set<Duco::ObjectId>& ducoTowerId) const;

    DllExport size_t NumberOfTowersInCountry(const std::wstring& country) const;
    DllExport size_t NumberOfCountries() const;
    DllExport size_t NumberOfSaintAbbreviations() const;

    inline void RegisterCallBack(Duco::DoveTowerValidator* newCallback);
    inline void RegisterCancellationCallBack(Duco::ImportCancellationCallback* newCallback);

    DllExport size_t SaveSaintsNotUpdated(const std::string& filename);
    DllExport bool CompareSaint(const std::wstring& saint1, const std::wstring& saint2, bool substring) const;
    DllExport bool ProperSaint(const std::wstring& saint, std::wstring& properName) const;

protected:
    Duco::DoveFileConfiguration                    fileConfig;
    bool                                           convertToLong;
    Duco::ProgressCallback* const                  callback;
    Duco::DoveTowerValidator*                      validator;
    Duco::ImportCancellationCallback*              cancellationCheck;
    Duco::AbbreviationList*                        saintAbbreviations;
    std::multimap<std::wstring, Duco::DoveObject*> objects;
    std::map<std::wstring, std::wstring>           oldDoveIdMap;
    std::wifstream*                                inputFile;
    float                                          fileSize;
};

void
DoveDatabase::RegisterCallBack(DoveTowerValidator* newCallback)
{
    validator = newCallback;
}

void
DoveDatabase::RegisterCancellationCallBack(Duco::ImportCancellationCallback* newCallback)
{
    cancellationCheck = newCallback;
}

}

#endif //!__DOVEDATABASE_H__
