#ifndef __CSVIMPORTER_H__
#define __CSVIMPORTER_H__

#include <string>
#include <vector>
#include "DucoEngineCommon.h"
#include "FileImporter.h"

namespace Duco
{
    class RingingDatabase;
    class Peal;
    class ProgressCallback;
    class SettingsFile;

class CsvImporter : public FileImporter
{
public:
    DllExport CsvImporter(Duco::ProgressCallback& theCallback,
                            Duco::RingingDatabase& theDatabase,
                            const std::string& theInstallDir,
                            const std::string& theSettingsFileName);
    DllExport ~CsvImporter();

    DllExport static void CreateDefaultSettings(Duco::SettingsFile& settings);
    DllExport static void CreateDefaultPealBookSettings(Duco::SettingsFile& settings);
    DllExport const Duco::SettingsFile& Settings() const;

protected: // from FileImporter
    virtual void TokeniseLine(const std::wstring& fileLine, std::vector<std::wstring>& tokens, bool useComma, bool useTab) const;
    virtual void GetTotalFileSize(const std::string& fileName);
    virtual bool ProcessPeal(const std::wstring& nextLine);
    virtual bool EnforceQuotes() const;

protected: // new functions
    unsigned int CreateRingers(const std::vector<std::wstring>& tokens, Duco::Peal& thePeal) const;
    void CreateTower(const std::vector<std::wstring>& tokens, Duco::Peal& thePeal, unsigned int noOfRingers) const;
    void FindMethod(const std::vector<std::wstring>& tokens, Duco::Peal& thePeal, unsigned int noOfRingers) const;
    void FindDate(const std::vector<std::wstring>& tokens, Duco::Peal& thePeal) const;
    void FindTime(const std::vector<std::wstring>& tokens, Duco::Peal& thePeal) const;

    bool GetFieldValue(const std::vector<std::wstring>& tokens, const std::wstring& fieldName, int& fieldValue) const;
    bool GetFieldValue(const std::vector<std::wstring>& tokens, const std::wstring& fieldName, std::wstring& fieldValue) const;
    std::wstring CreateTenorField(const std::vector<std::wstring>& tokens, bool handbell) const;

private:
    Duco::SettingsFile* settings;
    bool                ignoreNextLine;
};

}

#endif //!__CSVIMPORTER_H__
