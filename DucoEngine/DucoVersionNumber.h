#ifndef __DUCOVERSIONNO_H__
#define __DUCOVERSIONNO_H__

#include <string>
#include "DucoEngineCommon.h"

namespace Duco
{
class DucoVersionNumber
{
public:
    DllExport DucoVersionNumber(const std::wstring& versionNo = L"");
    DllExport ~DucoVersionNumber();

    DllExport std::wstring Str() const;

    DllExport bool operator<(const Duco::DucoVersionNumber& rhs) const;
    DllExport bool operator==(const Duco::DucoVersionNumber& rhs) const;

protected:
    unsigned int majorVerNo;
    unsigned int minorVerNo;
    unsigned int buildNo;
};

}

#endif //__DUCOVERSIONNO_H__
