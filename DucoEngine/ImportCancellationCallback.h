#ifndef __IMPORTCANCELLATIONCALLBACK_H__
#define __IMPORTCANCELLATIONCALLBACK_H__

namespace Duco
{
    class ImportCancellationCallback
    {
    public:
        virtual bool ContinueImport() = 0;
    };

}

#endif //#ifndef __IMPORTCANCELLATIONCALLBACK_H__
