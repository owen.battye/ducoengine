#include "SearchFieldStringArgument.h"

#include "Association.h"
#include "Composition.h"
#include "CompositionDatabase.h"
#include "DucoEngineUtils.h"
#include "Method.h"
#include "MethodDatabase.h"
#include "MethodSeries.h"
#include "MethodSeriesDatabase.h"
#include "Peal.h"
#include "Picture.h"
#include "RingingDatabase.h"
#include "Ring.h"
#include "RingerDatabase.h"
#include "Ringer.h"
#include "TowerDatabase.h"
#include "Tower.h"

using namespace Duco;
using namespace std;

TSearchFieldStringArgument::TSearchFieldStringArgument(Duco::TSearchFieldId fieldId, const std::wstring& newFieldValue, bool newSubStr, bool newExcluding)
: TSearchArgument(fieldId), fieldValue(newFieldValue), subStr(newSubStr), excluding(newExcluding)
{

}

Duco::TFieldType
TSearchFieldStringArgument::FieldType() const
{
    return TFieldType::EStringField;
}

bool 
TSearchFieldStringArgument::Match(const Duco::Ringer& ringer, const Duco::RingingDatabase& database) const
{
    switch (fieldId)
    {
    case EForename:
        return ringer.ForenameContains(fieldValue, subStr, true);
    case ESurname:
        return ringer.SurnameContains(fieldValue, subStr, true);
    case ENotes:
        return DucoEngineUtils::MatchString(ringer.Notes(), fieldValue, subStr);
    case EPealBaseReference:
        return DucoEngineUtils::MatchString(ringer.PealbaseReference(), fieldValue, subStr);
    default:
        return false;
    }
}

bool
TSearchFieldStringArgument::Match(const Duco::Tower& tower, const Duco::RingingDatabase& database) const
{
    switch (fieldId)
    {
    case ETowerName:
        return DucoEngineUtils::MatchString(tower.Name(), fieldValue, subStr);
    case ECity:
        return DucoEngineUtils::MatchString(tower.City(), fieldValue, subStr);
    case ECountyField:
        return DucoEngineUtils::MatchString(tower.County(), fieldValue, subStr);
    case ERingName:
        return CheckRingNames(tower, database);
    case ETenorWeight:
        return TenorWeight(tower, database);
    case ETenorKey:
        return DucoEngineUtils::MatchString(tower.TowerKey(), fieldValue, subStr);
    case EDoveReference:
        return DucoEngineUtils::MatchString(tower.DoveRef(), fieldValue, subStr);
    case ETowerbaseId:
        return DucoEngineUtils::MatchString(tower.TowerbaseId(), fieldValue, subStr);
    case ENotes:
        return DucoEngineUtils::MatchString(tower.Notes(), fieldValue, subStr);
    case ETowerAka:
        return DucoEngineUtils::MatchString(tower.AlsoKnownAs(), fieldValue, subStr);

    default:
        return false;
    }
}

bool
TSearchFieldStringArgument::Match(const Duco::Peal& peal, const Duco::RingingDatabase& database) const
{
    switch (fieldId)
    {
    case Duco::EAssociationName:
        return DucoEngineUtils::MatchString(peal.AssociationName(database), fieldValue, subStr);

    case Duco::EBlankRWReference:
    {
        if (fieldValue.length() == 0)
        {
            if (excluding)
                return peal.RingingWorldReference().length() > 0;

            return peal.RingingWorldReference().length() == 0;
        }
        bool matchingReference = DucoEngineUtils::MatchingRingingWorldReference(peal.RingingWorldReference(), fieldValue);
        return excluding ? !matchingReference : matchingReference;
    }

    case Duco::EBellBoardReference:
    {
        if (fieldValue.length() == 0)
        {
            if (excluding)
                return peal.BellBoardId().length() > 0;

            return peal.BellBoardId().length() == 0;
        }   
        bool containsString = DucoEngineUtils::MatchString(peal.BellBoardId(), fieldValue, subStr);
        return excluding ? !containsString : containsString;
    }

    case Duco::TSearchFieldId::EFullMethodName:
        return CheckMethodFullName(peal.MethodId(), database);

    case Duco::EMethodSeriesStr:
        return CheckMethodSeriesName(peal, database);

    case Duco::EMethodMultiStr:
        return CheckMultiMethods(peal);

    case ERingerName:
        return peal.CheckRingerNames(database, excluding, fieldValue, subStr);

    case ETowerName:
        return CheckTowerName(peal, database);

    case EConductorName:
        return CheckConductorName(peal, database);

    case EComposer:
        return CheckComposerName(peal, database);

    case EStrapper:
        return peal.CheckStrapperName(database, fieldValue, subStr);

    case EFootnotes:
        return DucoEngineUtils::MatchString(peal.Footnotes(), fieldValue, subStr);

    case Duco::ENotes:
        return DucoEngineUtils::MatchString(peal.Notes(), fieldValue, subStr);

    case Duco::ECompositionStr:
        return CheckCompositionName(peal.Id(), database);

    case ETenorWeight:
        return TenorWeight(peal.TowerId(), peal.RingId(), database);
    case ETenorKey:
        return TenorKey(peal.TowerId(), peal.RingId(),  database);

    case EInconsistantFees:
        break;

    default:
        break;
    }
    return false;
}

bool
TSearchFieldStringArgument::Match(const Duco::Method& method, const Duco::RingingDatabase& database) const
{
    switch (fieldId)
    {
    case Duco::TSearchFieldId::EFullMethodName:
        return DucoEngineUtils::MatchString(method.FullName(database.MethodsDatabase()), fieldValue, subStr);
    case Duco::TSearchFieldId::EMethodName:
        return DucoEngineUtils::MatchString(method.Name(), fieldValue, subStr);
    case Duco::TSearchFieldId::EMethodType:
        return DucoEngineUtils::MatchString(method.Type(), fieldValue, subStr);
    case Duco::TSearchFieldId::EMethodPN:
        return DucoEngineUtils::MatchString(method.PlaceNotation(), fieldValue, subStr);
    default:
        return false;
    }
}

bool
TSearchFieldStringArgument::Match(const Duco::MethodSeries& methodSeries, const Duco::RingingDatabase& database) const
{
    switch (fieldId)
    {
    case Duco::EMethodSeriesStr:
        return CheckMethodSeriesName(methodSeries, database);
    case Duco::TSearchFieldId::EMethodName:
        return CompareSeriesMethodNames(methodSeries, database);

    default:
        return false;
    }
}

bool
TSearchFieldStringArgument::Match(const Duco::Composition& object, const Duco::RingingDatabase& database) const
{
    switch (fieldId)
    {
    case ENotes:
        return DucoEngineUtils::MatchString(object.Notes(), fieldValue, subStr);

    case EComposition_Name:
        return DucoEngineUtils::MatchString(object.Name(), fieldValue, subStr);

    case TSearchFieldId::EFullMethodName:
        return CheckMethodFullName(object.MethodId(), database);

    case EComposer:
        return CheckRingerName(object.ComposerId(), database);

    default:
        return false;
    }
}

bool
TSearchFieldStringArgument::Match(const Duco::Association& object, const Duco::RingingDatabase& database) const
{
    switch (fieldId)
    {
    case EAssociationName:
        return DucoEngineUtils::MatchString(object.Name(), fieldValue, subStr);

    default:
        return false;
    }
}

bool
TSearchFieldStringArgument::Match(const Duco::Picture& object, const Duco::RingingDatabase& database) const
{
    switch (fieldId)
    {
    case ENotes:
        return DucoEngineUtils::MatchString(object.Notes(), fieldValue, subStr);

    default:
        return false;
    }
}

bool
TSearchFieldStringArgument::CheckMethodFullName(const ObjectId& methodId, const Duco::RingingDatabase& database) const
{
    const Method* const theMethod = database.MethodsDatabase().FindMethod(methodId);
    if (theMethod != NULL)
    {
        return DucoEngineUtils::MatchString(theMethod->FullName(database.MethodsDatabase()), fieldValue, subStr);
    }

    return false;
}

bool
TSearchFieldStringArgument::CheckMethodSeriesName(const Duco::Peal& peal, const Duco::RingingDatabase& database) const
{
    bool pealFound (false);
    const MethodSeries* const theMethod = database.MethodSeriesDatabase().FindMethodSeries(peal.SeriesId());
    if (theMethod != NULL)
    {
        pealFound = DucoEngineUtils::MatchString(theMethod->Name(), fieldValue, subStr);
    }
    else
        pealFound = false;

    return pealFound;
}

bool
TSearchFieldStringArgument::CheckMethodSeriesName(const Duco::MethodSeries& theSeries, const Duco::RingingDatabase& database) const
{
    return DucoEngineUtils::MatchString(theSeries.Name(), fieldValue, subStr);
}

bool
TSearchFieldStringArgument::CheckMultiMethods(const Duco::Peal& peal) const
{
    return DucoEngineUtils::MatchString(peal.MultiMethods(), fieldValue, subStr);
}

bool
TSearchFieldStringArgument::CheckTowerName(const Duco::Peal& peal, const Duco::RingingDatabase& database) const
{
    bool towerFound = false;
    const Tower* const theTower = database.TowersDatabase().FindTower(peal.TowerId());
    if (theTower != NULL)
    {
        towerFound = DucoEngineUtils::MatchString(theTower->FullName(), fieldValue, subStr);
    }

    return towerFound;
}

bool
TSearchFieldStringArgument::CheckRingerName(const ObjectId& ringerId, const Duco::RingingDatabase& database) const
{
    bool includePeal (excluding);
    const Ringer* const theRinger = database.RingersDatabase().FindRinger(ringerId);
    if (theRinger != NULL)
    {
        if (excluding)
            includePeal = !theRinger->NameContainsSubstring(database.Settings(), fieldValue, subStr);
        else
            includePeal = theRinger->NameContainsSubstring(database.Settings(), fieldValue, subStr);
    }   
    return includePeal;
}

bool
TSearchFieldStringArgument::CheckConductorName(const Duco::Peal& peal, const Duco::RingingDatabase& database) const
{
    bool conductorFound (false);
    const std::set<ObjectId>& conductors = peal.Conductors();
    std::set<ObjectId>::const_iterator it = conductors.begin();
    while (it != conductors.end() && !conductorFound)
    {
        const Ringer* const theRinger = database.RingersDatabase().FindRinger(*it);
        if (theRinger != NULL)
        {
            conductorFound = theRinger->NameContainsSubstring(database.Settings(), fieldValue, subStr);
        }
        ++it;
    }

    return conductorFound;
}

bool 
TSearchFieldStringArgument::CheckComposerName(const Duco::Peal& peal, const Duco::RingingDatabase& database) const
{
    return DucoEngineUtils::MatchString(peal.Composer(), fieldValue, subStr);
}

bool 
TSearchFieldStringArgument::CheckRingNames(const Duco::Tower& tower, const Duco::RingingDatabase& database) const
{
    bool matchingRing = false;
    for (unsigned int i = 0; i < tower.NoOfRings() && !matchingRing; ++i)
    {
        const Ring* const theRing = tower.FindRingByIndex(i);
        if (theRing != NULL)
        {
            matchingRing = DucoEngineUtils::MatchString(theRing->Name(), fieldValue, subStr);           
        }
    }
    return matchingRing;
}

bool 
TSearchFieldStringArgument::TenorWeight(const Duco::Tower& tower, const Duco::RingingDatabase& database) const
{
    bool matchingRing = false;
    for (unsigned int i = 0; i < tower.NoOfRings() && !matchingRing; ++i)
    {
        const Ring* const theRing = tower.FindRingByIndex(i);
        if (theRing != NULL)
        {
            matchingRing = DucoEngineUtils::MatchString(theRing->TenorWeight(), fieldValue, subStr);
        }
    }
    return matchingRing;
}

bool
TSearchFieldStringArgument::TenorWeight(const Duco::ObjectId& towerId, const Duco::ObjectId& ringId, const Duco::RingingDatabase& database) const
{
    const Duco::Tower* const theTower = database.TowersDatabase().FindTower(towerId);
    if (theTower != NULL)
    {
        const Duco::Ring* const theRing = theTower->FindRing(ringId);
        if (theRing != NULL)
        {
            return DucoEngineUtils::MatchString(theRing->TenorWeight(), fieldValue, subStr);
        }
    }

    return false;
}

bool
TSearchFieldStringArgument::TenorKey(const Duco::ObjectId& towerId, const Duco::ObjectId& ringId, const Duco::RingingDatabase& database) const
{
    const Duco::Tower* const theTower = database.TowersDatabase().FindTower(towerId);
    if (theTower != NULL)
    {
        const Duco::Ring* const theRing = theTower->FindRing(ringId);
        if (theRing != NULL)
        {
            return DucoEngineUtils::MatchString(theRing->TenorKey(), fieldValue, subStr);
        }
    }
    return false;
}

bool
TSearchFieldStringArgument::CompareSeriesMethodNames(const Duco::MethodSeries& methodSeries, const Duco::RingingDatabase& database) const
{
    bool returnVal (false);
    std::set<ObjectId>::const_iterator it = methodSeries.Methods().begin();
    while (it != methodSeries.Methods().end() && !returnVal)
    {
        const Method* nextMethod = database.MethodsDatabase().FindMethod(*it);
        returnVal = Match(*nextMethod, database);
        ++it;
    }
    return returnVal;
}

bool
TSearchFieldStringArgument::CheckCompositionName(const ObjectId& compositionId, const Duco::RingingDatabase& database) const
{
    const Composition* const theComposition = database.CompositionsDatabase().FindComposition(compositionId);
    bool returnVal (false);
    if (theComposition != NULL)
    {
        std::wstring composerStr;
        std::wstring titleStr;
        theComposition->FullName(database, titleStr, composerStr);
        returnVal = DucoEngineUtils::MatchString(titleStr, fieldValue, subStr) || DucoEngineUtils::MatchString(composerStr, fieldValue, subStr);
    }
    return returnVal;
}
