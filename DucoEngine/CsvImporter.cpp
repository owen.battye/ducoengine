#include "CsvImporter.h"

#include "AssociationDatabase.h"
#include "CsvImportFields.h"
#include "DucoEngineUtils.h"
#include "RingingDatabase.h"
#include "SettingsFile.h"
#include "MethodDatabase.h"
#include "PealDatabase.h"
#include "RingerDatabase.h"
#include "TowerDatabase.h"
#include "Peal.h"
#include "Tower.h"
#include <cstdlib>
#include <iostream>

using namespace std;
using namespace Duco;

CsvImporter::CsvImporter(Duco::ProgressCallback& theCallback,
            Duco::RingingDatabase& theDatabase,
            const std::string& theInstallDir,
            const std::string& theSettingsFileName)
:   FileImporter(theCallback, theDatabase)
{
    settings = new Duco::SettingsFile(theInstallDir, theSettingsFileName);
    if (settings->NoOfSettings() <= 0)
    {
        CreateDefaultSettings(*settings);
    }
    if (!settings->Setting(KIgnoreFirstLineFieldNo, ignoreNextLine))
        ignoreNextLine = false;
    if (!settings->Setting(KDecodeMultiByteFieldNo, setLocale))
        setLocale = true;

}

CsvImporter::~CsvImporter()
{
    delete settings;
}

void
CsvImporter::GetTotalFileSize(const std::string& fileName)
{
    totalFileSize = GetFileSize(fileName);
}

void
CsvImporter::TokeniseLine(const std::wstring& fileLine, std::vector<std::wstring>& tokens, bool useComma, bool useTab) const
{
    FileImporter::TokeniseLine(fileLine, tokens, useComma, useTab);
    while (tokens.size() < settings->ExpectedNoOfFields())
    {
        tokens.push_back(L"");
    }
}

bool
CsvImporter::ProcessPeal(const std::wstring& nextLine)
{
    if (ignoreNextLine)
    {
        ignoreNextLine = false;
        return false;
    }

    std::vector<std::wstring> tokens;
    TokeniseLine(nextLine, tokens, true, false);

    Peal newPeal;

    std::wstring handBellField;
    if (GetFieldValue(tokens, KHandbellFieldNo, handBellField))
    {
        std::wstring handBellIndicator;
        if (settings->Setting(KHandbellStringFieldNo, handBellIndicator))
        {
            if (DucoEngineUtils::CompareString(handBellIndicator, handBellField))
            {
                newPeal.SetHandbell(true);
            }
        }
    }

    unsigned int noOfRingers (CreateRingers(tokens, newPeal));
    CreateTower(tokens, newPeal, noOfRingers);
    FindDate(tokens, newPeal);
    FindTime(tokens, newPeal);
    FindMethod(tokens, newPeal, noOfRingers);

    int noOfChanges (0);
    if (GetFieldValue(tokens, KChangesFieldNo, noOfChanges))
    {
        newPeal.SetChanges(noOfChanges);
        if (noOfChanges < 5000)
            database.Settings().SetPealDatabase(false);
    }
    std::wstring stringValue;
    if (GetFieldValue(tokens, KAssociationFieldNo, stringValue))
    {
        Duco::ObjectId associationId = database.AssociationsDatabase().SuggestAssociationOrCreate(stringValue);
        newPeal.SetAssociationId(associationId);
    }

    if (GetFieldValue(tokens, KFootnotesFieldNo, stringValue))
    {
        newPeal.SetFootnotes(stringValue);
    }
    if (GetFieldValue(tokens, KComposerFieldNo, stringValue))
    {
        newPeal.SetComposer(stringValue);
    }
    if (GetFieldValue(tokens, KSplicedMethodsFieldNo, stringValue))
    {
        newPeal.SetMultiMethods(stringValue);
    }
    if (GetFieldValue(tokens, KRingingWorldFieldNo, stringValue))
    {
        newPeal.SetRingingWorldReference(stringValue);
    }
    if (GetFieldValue(tokens, KBellBoardFieldNo, stringValue))
    {
        newPeal.SetBellBoardId(stringValue);
    }
    return database.PealsDatabase().AddObject(newPeal).ValidId();
}

void
CsvImporter::CreateTower(const std::vector<std::wstring>& tokens, Duco::Peal& thePeal, unsigned int noOfRingers) const
{
    std::wstring towerName;
    std::wstring towerTown;
    std::wstring towerCounty;
    std::wstring towerTenorKey;
    if (GetFieldValue(tokens, KTenorKeyFieldNo, towerTenorKey))
    {
        towerTenorKey = DucoEngineUtils::Trim(towerTenorKey);
    }

    if (GetFieldValue(tokens, KTowerNameFieldNo, towerName) &&
        GetFieldValue(tokens, KTowerTownFieldNo, towerTown) &&
        GetFieldValue(tokens, KTowerCountyFieldNo, towerCounty))
    {
        if (thePeal.Handbell())
        {
            noOfRingers *= 2;
        }
        std::wstring tenorWeight = CreateTenorField(tokens, thePeal.Handbell());
        ObjectId towerId = database.TowersDatabase().SuggestTower(towerName, towerTown, towerCounty, noOfRingers, tenorWeight, towerTenorKey, true);
        if (towerId.ValidId())
        {
            thePeal.SetTowerId(towerId);
            const Tower* const theTower = database.TowersDatabase().FindTower(towerId);
            ObjectId ringId;
            if (theTower->SuggestRing(noOfRingers, tenorWeight, towerTenorKey, ringId, true))
            {
                thePeal.SetRingId(ringId);
            }
            else
            {
                // need to create new ring
                Tower updatedTower (*theTower);
                ringId = updatedTower.AddRing(noOfRingers, tenorWeight, towerTenorKey);
                if (ringId.ValidId() && database.TowersDatabase().UpdateObject(updatedTower))
                {
                    thePeal.SetRingId(ringId);
                }
            }
        }
        else
        {
            Tower newTower(KNoId, towerName, towerTown, towerCounty, noOfRingers);
            Duco::ObjectId ringId = newTower.AddDefaultRing(Tower::DefaultRingName(noOfRingers), tenorWeight, towerTenorKey);

            towerId = database.TowersDatabase().AddObject(newTower);
            thePeal.SetTowerId(towerId);
            thePeal.SetRingId(ringId);
        }
    }
}

unsigned int
CsvImporter::CreateRingers(const std::vector<std::wstring>& tokens, Duco::Peal& thePeal) const
{
    bool alternateBells (false);

    int handbellType(0);
    if (thePeal.Handbell() && settings->Setting(KHandbellTypeFieldNo, handbellType))
    {
        alternateBells = handbellType == 1;
    }

    int firstRingerFieldId (0);
    int lastRingerFieldId (0);
    if (!settings->Setting(KFirstRingerFieldNo, firstRingerFieldId) || !settings->Setting(KLastRingerFieldNo, lastRingerFieldId))
        return 0;

    bool removeBellNoFromRingers (false);
    settings->Setting(KRemoveBellNumberFromRingersFieldNo, removeBellNoFromRingers);

    bool conductorSet(false);
    unsigned int numberOfRingers (0);
    unsigned int foundRingerFields (0);
    for (int i(firstRingerFieldId); i <= lastRingerFieldId; ++i)
    {
        const std::wstring& ringerStrTemp = tokens[i];
        std::wstring ringerStr = ringerStrTemp;
        if (removeBellNoFromRingers)
        {
            size_t endOfNumber = ringerStrTemp.find_first_of(' ');
            if (endOfNumber != string::npos)
            {
                ringerStr = ringerStrTemp.substr(endOfNumber);
                ringerStr = DucoEngineUtils::Trim(ringerStr);
            }
        }
        if (ringerStr.length() > 0 && (!alternateBells || (foundRingerFields % 2 == 0)))
        {
            ObjectId ringerId = database.RingersDatabase().FindIdenticalRinger(ringerStr);
            if (!ringerId.ValidId())
            {
                ringerId = database.RingersDatabase().AddRinger(ringerStr);
            }
            if (ringerId.ValidId())
            {
                ++numberOfRingers;
                thePeal.SetRingerId(ringerId, numberOfRingers, false);
            }
        }
        ++foundRingerFields;
    }

    // Conductor
    int conductorFieldId (0);
    if (settings->Setting(KConductorFieldNo, conductorFieldId) && conductorFieldId > -1)
    {
        int conductorFieldType(0);
        settings->Setting(KConductorTypeFieldNo, conductorFieldType);
        switch (conductorFieldType)
        {
        default:
        case 0: // byname
            {
            ObjectId ringerId = database.RingersDatabase().FindIdenticalRinger(tokens[conductorFieldId]);
            thePeal.SetConductorId(ringerId);
            conductorSet = true;
            }
            break;
        case 1: // by bellno
            {
            int bellNo (0);
            if (GetFieldValue(tokens, KConductorFieldNo, bellNo))
            {
                thePeal.SetConductorIdFromBell(bellNo, true, false);
                conductorSet = true;
            }
            }
            break;
        }
    }
    if (!conductorSet)
    {
        thePeal.SetConductedType(ESilentAndNonConducted);
    }

    return numberOfRingers;
}

bool
CsvImporter::GetFieldValue(const std::vector<std::wstring>& tokens, const std::wstring& fieldName, std::wstring& fieldValue) const
{
    fieldValue.clear();
    int fieldNo(0);
    if (settings->Setting(fieldName, fieldNo) && fieldNo != -1 && int(tokens.size()) > fieldNo)
    {
        fieldValue = tokens[fieldNo];
        return fieldValue.length() > 0;
    }
    return false;
}

bool
CsvImporter::GetFieldValue(const std::vector<std::wstring>& tokens, const std::wstring& fieldName, int& fieldValue) const
{
    fieldValue = 0;
    std::wstring tempValue;
    if (!GetFieldValue(tokens, fieldName, tempValue) || tempValue.length() <= 0 || isdigit (tempValue[0]) == 0)
        return false;

    fieldValue = _wtoi(tempValue.c_str());
    return true;
}

std::wstring
CsvImporter::CreateTenorField(const std::vector<std::wstring>& tokens, bool handbell) const
{
    std::wstring tenorWeight;
    int tenorWeight1 (0);
    if (GetFieldValue(tokens, KTenorWeightFieldNo1, tenorWeight1))
    {
        std::wstring tenorWeightTmp;
        GetFieldValue(tokens, KTenorWeightFieldNo1, tenorWeight);
        if (GetFieldValue(tokens, KTenorWeightFieldNo2, tenorWeight1))
        {
            tenorWeight += L"-";
            GetFieldValue(tokens, KTenorWeightFieldNo2, tenorWeightTmp);
            tenorWeight += tenorWeightTmp;
            if (GetFieldValue(tokens, KTenorWeightFieldNo3, tenorWeight1))
            {
                tenorWeight += L"-";
                GetFieldValue(tokens, KTenorWeightFieldNo3, tenorWeightTmp);
                tenorWeight += tenorWeightTmp;
            }
            return tenorWeight;
        }
        else if (GetFieldValue(tokens, KTenorWeightFieldNo2, tenorWeightTmp))
        {
            bool moreInfo(false);
            if (tenorWeightTmp.length() > 0)
            {
                tenorWeight += L" ";
                tenorWeight += tenorWeightTmp;
                moreInfo = true;
            }
            if (GetFieldValue(tokens, KTenorWeightFieldNo3, tenorWeightTmp) && tenorWeightTmp.length() > 0)
            {
                tenorWeight += L" ";
                tenorWeight += tenorWeightTmp;
                moreInfo = true;
            }
            if (!moreInfo && !handbell)
            {
                tenorWeight += L" cwt";
            }
            return tenorWeight;
        }
        else
        {
            if (!handbell)
                tenorWeight += L" cwt";
            return tenorWeight;
        }
    }

    GetFieldValue(tokens, KTenorWeightFieldNo1, tenorWeight);
    std::wstring tenorWeightTmp;
    GetFieldValue(tokens, KTenorWeightFieldNo2, tenorWeightTmp);
    if (tenorWeight.length() > 0 && tenorWeightTmp.length() > 0)
    {
        tenorWeight += L" ";
        tenorWeight += tenorWeightTmp;
    }
    GetFieldValue(tokens, KTenorWeightFieldNo3, tenorWeightTmp);
    if (tenorWeight.length() > 0 && tenorWeightTmp.length() > 0)
    {
        tenorWeight += L" ";
        tenorWeight += tenorWeightTmp;
    }
    return tenorWeight;
}

void
CsvImporter::FindDate(const std::vector<std::wstring>& tokens, Duco::Peal& thePeal) const
{
    bool useSingleDateField (false);
    if (settings->Setting(KDateSingleFieldNo, useSingleDateField) && useSingleDateField)
    {
        std::wstring date;
        if (GetFieldValue(tokens, KDayFieldNo, date))
        {
            //26/02/1974
            Duco::PerformanceDate newDate (date, false);
            thePeal.SetDate(newDate);
        }
    }
    else
    {
        int day;
        int month;
        int year;
        if (GetFieldValue(tokens, KDayFieldNo, day) && GetFieldValue(tokens, KMonthFieldNo, month) && GetFieldValue(tokens, KYearFieldNo, year))
        {
            PerformanceDate theDate(year, month, day);
            thePeal.SetDate(theDate);
        }
    }
}

void
CsvImporter::FindTime(const std::vector<std::wstring>& tokens, Duco::Peal& thePeal) const
{
    bool useSingleTimeField (false);
    if (settings->Setting(KTimeSingleFieldNo, useSingleTimeField) && useSingleTimeField)
    {
        std::wstring time(L"");
        GetFieldValue(tokens, KHoursFieldNo, time);
        PerformanceTime pealTime(time);
        thePeal.SetTime(pealTime);
    }
    else
    {
        int hours(0);
        int minutes(0);
        GetFieldValue(tokens, KHoursFieldNo, hours);
        GetFieldValue(tokens, KMinutesFieldNo, minutes);

        minutes += hours * 60;
        PerformanceTime theDate(minutes);
        thePeal.SetTime(theDate);
    }
}

void
CsvImporter::FindMethod(const std::vector<std::wstring>& tokens, Duco::Peal& thePeal, unsigned int noOfRingers) const
{
    bool useSingleMethodField (false);
    if (settings->Setting(KMethodSingleFieldNo, useSingleMethodField) && useSingleMethodField)
    {
        std::wstring method;
        GetFieldValue(tokens, KMethodFieldNo, method);
        unsigned int order (-1);
        thePeal.SetMethodId(database.MethodsDatabase().FindOrCreateMethod(method, order));
    }
    else
    {
        std::wstring method;
        std::wstring methodtype;
        std::wstring stage;

        GetFieldValue(tokens, KMethodFieldNo, method);
        GetFieldValue(tokens, KMethodTypeFieldNo, methodtype);
        GetFieldValue(tokens, KStageFieldNo, stage);
        unsigned int order (-1);
        thePeal.SetMethodId(database.MethodsDatabase().FindOrCreateMethod(method, methodtype, stage, order));
    }
}

bool
CsvImporter::EnforceQuotes() const
{
    bool enforceQuotes = false;
    settings->Setting(KEnforceQuotesFieldNo, enforceQuotes);
    return enforceQuotes;
}

void
CsvImporter::CreateDefaultSettings(Duco::SettingsFile& settings)
{
    settings.ClearAllSettings();
    settings.Set(KEnforceQuotesFieldNo, true);
    settings.Set(KConductorTypeFieldNo, 0);
    settings.Set(KHandbellTypeFieldNo, 1);
    settings.Set(KTowerTownFieldNo, 1);
    settings.Set(KTowerCountyFieldNo, 2);
    settings.Set(KTowerNameFieldNo, 3);
    settings.Set(KTenorWeightFieldNo1, 4);
    settings.Set(KTenorWeightFieldNo2, 5);
    settings.Set(KTenorWeightFieldNo3, 6);
    settings.Set(KTenorKeyFieldNo, 7);
    settings.Set(KHandbellFieldNo, 7);
    //settings.Set(KDateSingleFieldNo, false);
    settings.Set(KDayFieldNo, 8);
    settings.Set(KMonthFieldNo, 9);
    settings.Set(KYearFieldNo, 10);
    settings.Set(KAssociationFieldNo, 11);
    settings.Set(KChangesFieldNo, 13);
    //settings.Set(KMethodSingleFieldNo, false);
    settings.Set(KMethodFieldNo, 14);
    settings.Set(KMethodTypeFieldNo, 15);
    settings.Set(KStageFieldNo, 16);
    //settings.Set(KTimeSingleFieldNo, false);
    settings.Set(KHoursFieldNo, 17);
    settings.Set(KMinutesFieldNo, 18);
    //settings.Set(KRemoveBellNumberFromRingersFieldNo, false);
    settings.Set(KFirstRingerFieldNo, 19);
    settings.Set(KLastRingerFieldNo, 30);
    settings.Set(KConductorFieldNo, 31);
    settings.Set(KHandbellStringFieldNo, KHandbellStringDefaultValue);
    settings.Set(KFootnotesFieldNo, 32);
    settings.Set(KComposerFieldNo, -1);
    settings.Set(KSplicedMethodsFieldNo, -1);
    //settings.Set(KIgnoreFirstLineFieldNo, false);
    settings.Set(KBellBoardFieldNo, -1);
    settings.Set(KRingingWorldFieldNo, -1);
    settings.Set(KDecodeMultiByteFieldNo, false);
    settings.ClearSettingsChanged();
}

void
CsvImporter::CreateDefaultPealBookSettings(Duco::SettingsFile& settings)
{
    settings.ClearAllSettings();
    settings.Set(KEnforceQuotesFieldNo, false);
    //settings.Set(KConductorTypeFieldNo, false);
    settings.Set(KHandbellTypeFieldNo, true);
    settings.Set(KTowerTownFieldNo, 3);
    settings.Set(KTowerCountyFieldNo, 4);
    settings.Set(KTowerNameFieldNo, 5);
    settings.Set(KTenorWeightFieldNo1, 7);
    //settings.Set(KTenorWeightFieldNo2, -1);
    //settings.Set(KTenorWeightFieldNo3, -1);
    //settings.Set(KTenorKeyFieldNo, 7);
    settings.Set(KHandbellFieldNo, 14);
    settings.Set(KDateSingleFieldNo, true);
    settings.Set(KDayFieldNo, 6);
    //settings.Set(KMonthFieldNo, -1);
    //settings.Set(KYearFieldNo, -1);
    settings.Set(KAssociationFieldNo, 2);
    settings.Set(KChangesFieldNo, 9);
    settings.Set(KMethodSingleFieldNo, true);
    settings.Set(KMethodFieldNo, 10);
    //settings.Set(KMethodTypeFieldNo, 15);
    //settings.Set(KStageFieldNo, 16);
    settings.Set(KTimeSingleFieldNo, true);
    settings.Set(KHoursFieldNo, 9);
    //settings.Set(KMinutesFieldNo, -1);
    settings.Set(KRemoveBellNumberFromRingersFieldNo, true);
    settings.Set(KFirstRingerFieldNo, 16);
    settings.Set(KLastRingerFieldNo, 33);
    //settings.Set(KConductorFieldNo, -1);
    settings.Set(KHandbellStringFieldNo, KHandbellStringDefaultValueForPealBook);
    settings.Set(KFootnotesFieldNo, 15);
    settings.Set(KComposerFieldNo, 11);
    //settings.Set(KSplicedMethodsFieldNo, -1);
    settings.Set(KIgnoreFirstLineFieldNo, true);
    settings.Set(KBellBoardFieldNo, -1);
    settings.Set(KRingingWorldFieldNo, -1);
    settings.Set(KDecodeMultiByteFieldNo, false);
    settings.ClearSettingsChanged();
}

const Duco::SettingsFile&
CsvImporter::Settings() const
{
    return *settings;
}
