#include "SearchValidationArgument.h"

using namespace Duco;

TSearchValidationArgument::TSearchValidationArgument(Duco::TSearchFieldId newFieldId)
: fieldId(newFieldId)
{

}

TSearchValidationArgument::~TSearchValidationArgument()
{

}

void
TSearchValidationArgument::Reset()
{

}

Duco::TSearchFieldId
TSearchValidationArgument::FieldId() const
{
    return fieldId;
}

bool
TSearchValidationArgument::Match(const Duco::MethodSeries& object, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const
{
    return false;
}

bool
TSearchValidationArgument::Match(const Duco::Method& object, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const
{
    return false;
}

bool
TSearchValidationArgument::Match(const Duco::Tower& object, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const
{
    return false;
}

bool
TSearchValidationArgument::Match(const Duco::Composition& object, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const
{
    return false;
}

bool
TSearchValidationArgument::Match(const Duco::Association& object, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const
{
    return false;
}

bool
TSearchValidationArgument::Match(const Duco::Picture& object, const std::set<Duco::ObjectId>& compareIds, const Duco::RingingDatabase& database) const
{
    return false;
}
