#ifndef __CALLING_H__
#define __CALLING_H__

#include "DucoEngineCommon.h"
#include "LeadType.h"
#include <string>
#include "ObjectId.h"

namespace Duco
{
    class Change;
    class ChangeCollection;
    class Composition;
    class DatabaseReader;
    class DatabaseWriter;
    class LeadEndCollection;
    class LeadOrder;
    class PlaceNotation;

class Calling
{
    friend class Composition;
    friend class Course;
public:
    DllExport Calling(const Calling& other);
    DllExport Calling(DatabaseReader& reader, unsigned int databaseVersion);
    DllExport virtual ~Calling();

    // Accessors
    inline TLeadType LeadType() const;
    inline const Duco::ObjectId& MethodId() const;
    inline unsigned int Position() const;
    wchar_t Position(const Duco::PlaceNotation& notation) const;
    bool IsHome(const Duco::LeadOrder& leadOrder) const;
    inline unsigned int Count() const;
    DllExport std::wstring Str(bool alreadyContainsString) const;
    //DllExport bool MatchPosition(char position) const;
    DllExport bool MatchPosition(const Calling& other) const;

    // Settors
    inline void operator++();

    // returns the number of changes advanced
    bool AdvanceToCall(Duco::Change& endChange, const Duco::PlaceNotation& notation, const Duco::Calling* const previousCalling, size_t snapStartPos, size_t& noOfChanges, bool generateCourseOnly) const;
    void AdvanceToCall(Duco::ChangeCollection& changes, Duco::LeadEndCollection& leadEnds, const PlaceNotation& notation, size_t snapStartPos, bool stopAtFalse, bool stopAtRounds) const;
    bool MatchPosition(wchar_t positionToMatch, const PlaceNotation& notation) const;

    DatabaseReader& Internalise(DatabaseReader& reader, unsigned int databaseVersion);
    DatabaseWriter& Externalise(DatabaseWriter& writer);

    DllExport bool operator==(const Duco::Calling& rhs) const;
    DllExport bool operator!=(const Duco::Calling& rhs) const;

protected:
    DllExport Calling(unsigned int position, Duco::TLeadType type = EBobLead, const Duco::ObjectId& changeMethodId = KNoId);

private:
    TLeadType       leadType; // Plain, Single or Bob.
    unsigned int    position; // all positions are by the tenor.
    unsigned int    count;
    Duco::ObjectId  methodId; // If method changed, else KNoId.
    Duco::ObjectId  callingId;
};

TLeadType
Calling::LeadType() const
{
    return leadType;
}

const Duco::ObjectId&
Calling::MethodId() const
{
    return methodId;
}

unsigned int
Calling::Position() const
{
    return position;
}

unsigned int
Calling::Count() const
{
    return count;
}

void
Calling::operator++()
{
    ++count;
}

}

#endif //!__CALLING_H__

