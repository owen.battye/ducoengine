#include "SearchArgument.h"

using namespace Duco;

TSearchArgument::TSearchArgument(Duco::TSearchFieldId newFieldId)
: fieldId(newFieldId)
{

}

TSearchArgument::~TSearchArgument()
{

}

const Duco::TSearchFieldId
TSearchArgument::FieldId() const
{
    return fieldId;
}

bool
TSearchArgument::Match(const Duco::MethodSeries& object, const Duco::RingingDatabase& database) const
{
    return false;
}

bool
TSearchArgument::Match(const Duco::Method& object, const Duco::RingingDatabase& database) const
{
    return false;
}

bool
TSearchArgument::Match(const Duco::Ringer& object, const Duco::RingingDatabase& database) const
{
    return false;
}

bool
TSearchArgument::Match(const Duco::Tower& object, const Duco::RingingDatabase& database) const
{
    return false;
}

bool
TSearchArgument::Match(const Duco::Composition& object, const Duco::RingingDatabase& database) const
{
    return false;
}

bool
TSearchArgument::Match(const Duco::Association& object, const Duco::RingingDatabase& database) const
{
    return false;
}

bool
TSearchArgument::Match(const Duco::Picture& object, const Duco::RingingDatabase& database) const
{
    return false;
}
