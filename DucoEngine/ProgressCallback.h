#ifndef __PROGRESSCALLBACK_H__
#define __PROGRESSCALLBACK_H__

namespace Duco
{
class ProgressCallback
{
public:
    virtual void Initialised() = 0;
    virtual void Step(int progressPercent) = 0;
    virtual void Complete(bool error) = 0;
};

} // end namespace Duco

#endif //!__PROGRESSCALLBACK_H__
