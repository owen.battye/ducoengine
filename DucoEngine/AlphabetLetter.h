#ifndef __ALPHABETLETTER_H__
#define __ALPHABETLETTER_H__

#include "DucoEngineCommon.h"
#include <vector>
#include <cstddef>

namespace Duco
{
class AlphabetObject;
class PerformanceDate;
class Peal;
class ObjectId;

class AlphabetLetter
{
public:
    DllExport AlphabetLetter(wchar_t newLetter);
    DllExport ~AlphabetLetter();

    bool AddPeal(wchar_t letter, const ObjectId& pealId, const ObjectId& methodId, const Duco::PerformanceDate& pealDate, bool uniqueMethods);
    inline size_t Count() const;
    bool RungDate(unsigned int circleNumber, Duco::PerformanceDate& completionDate, Duco::ObjectId& pealId) const;

    void Reset();
    DllExport const AlphabetObject& operator[](unsigned int position) const;

protected:
    const wchar_t letter;
    std::vector<AlphabetObject*> pealInfo;
};

size_t
AlphabetLetter::Count() const
{
    return pealInfo.size();
}

}

#endif //!__ALPHABETLETTER_H__
