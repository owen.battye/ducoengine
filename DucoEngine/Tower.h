#ifndef __TOWER_H__
#define __TOWER_H__

#include <string>
#include <map>
#include <vector>
#include <xercesc\dom\DOMDocument.hpp>

#include "PerformanceDate.h"
#include "DucoEngineCommon.h"
#include "DucoTypes.h"
#include "RingingObject.h"

namespace Duco
{
class DatabaseWriter;
class DatabaseReader;
class DoveObject;
class DoveDatabase;
class RingingDatabase;
class Ring;

class Tower : public RingingObject
{
public:
    DllExport Tower();
    DllExport Tower(DatabaseReader& reader, unsigned int databaseVersion);
    DllExport Tower(const Tower& other);
    DllExport Tower(const DoveObject& other);
    DllExport Tower(const Duco::ObjectId& newId, const Tower& other);
    DllExport Tower(const Duco::ObjectId& newId, const std::wstring& newName, const std::wstring& newCity, const std::wstring& newCounty, unsigned int newBells);
    DllExport ~Tower();

    // From RingingObject
    DllExport TObjectType ObjectType() const;
    DllExport virtual bool Duplicate(const Duco::RingingObject& other, const Duco::RingingDatabase& database) const;
    DllExport std::wstring FullNameForSort(bool unusedSetting) const;
    DllExport virtual std::wstring ErrorString(const Duco::DatabaseSettings& /*settings*/, bool showWarnings) const;

    DllExport Duco::ObjectId NextFreeRingId() const;
    DllExport bool AddRing(const Duco::Ring& ring, bool fromImport = false);
    DllExport Duco::ObjectId AddRing(unsigned int noOfBells, const std::wstring& tenorWeight, const std::wstring& tenorKey);
    DllExport bool AddRings(const Duco::Tower& otherTower);
    DllExport bool DeleteRing(const Duco::ObjectId& ringId);
    DllExport bool DeleteRings(const std::vector<Duco::ObjectId>& ringIds);
    DllExport void CheckForDefaultRing();
    DllExport Duco::ObjectId AddDefaultRing(const std::wstring& newRingName = L"", const std::wstring& tenorWeightStr = L"", const std::wstring& tenorKey = L"");
    DllExport bool SuggestRing(unsigned int noOfBells, const std::wstring& tenorWeight, const std::wstring& tenorKey, Duco::ObjectId& ringId, unsigned int& matchCount) const;
    /* Setting fromImport to false allows any matching ring, true means its got to have at least some criteria that match. */
    DllExport bool SuggestRing(unsigned int noOfBells, const std::wstring& tenorWeight, const std::wstring& tenorKey, Duco::ObjectId& ringId, bool fromImport) const;
    DllExport bool FindSimilarRing(const Ring& otherRing, Duco::ObjectId& ringId) const;
    DllExport bool GetRingNameWithNoOfBells(unsigned int noOfBells, std::wstring& ringName) const;
    DllExport bool FirstInvalidRingId(Duco::ObjectId& ringId) const;
    DllExport static std::wstring DefaultRingName(unsigned int noOfBellsInRing);
    DllExport virtual bool RebuildRecommended() const;
    bool RemoveDuplicateRings(std::map<Duco::ObjectId, Duco::ObjectId>& duplicateRingReplacements);
    DllExport bool RingTranslations(const Duco::Tower& otherTower, std::map<Duco::ObjectId, Duco::ObjectId>& otherRingIds) const;

    // Accessors
    DllExport const std::wstring& Name() const;
    DllExport const std::wstring& City() const;
    DllExport const std::wstring& County() const;
    DllExport unsigned int Bells() const;
    DllExport unsigned int BellsForDove() const;
    DllExport size_t NoOfRings() const;
    DllExport size_t NoOfRings(unsigned int noOfBells) const;
    DllExport unsigned int NoOfBellsInRing(const Duco::ObjectId& ringId) const;
    DllExport const Duco::Ring* const FindRing(const Duco::ObjectId& ringId) const;
    DllExport const Duco::Ring* const FindRingByIndex(size_t index) const;
    DllExport size_t RingIndex(const Duco::ObjectId& ringId) const;
    DllExport Duco::ObjectId FindSimilarRing(const Duco::Ring& otherRing) const;
    DllExport void GetRingIds(std::vector<Duco::ObjectId>& ringIds) const;
    DllExport std::wstring FullName() const;
    DllExport const std::wstring& HeaviestTenor() const;
    DllExport std::wstring TenorWeight(const Duco::ObjectId& ringId) const;
    DllExport std::wstring TenorDescription(const Duco::ObjectId& ringId) const;
    DllExport bool RingOnBackBells(const Duco::Ring& ring) const;
    DllExport bool RingOnBackBells(const Duco::ObjectId& ringId) const;
    inline bool Removed() const;
    inline bool AntiClockwise() const;
    inline bool Handbell() const;
    DllExport void RenamedBells(std::map<unsigned int, TRenameBellType>& renames) const;
    DllExport Duco::TRenameBellType RenamedBell(unsigned int bellNumber) const;
    DllExport void AllBellNames(std::vector<std::wstring>& bellNames, const Duco::ObjectId& theRingId) const;
    DllExport std::wstring BellName(size_t bellNumber);
    DllExport const std::wstring& Notes() const;
    DllExport const std::wstring& DoveRef() const;
    DllExport std::wstring DoveUrl(const Duco::DatabaseSettings& settings) const;
    DllExport bool DoveRefSet() const;
    DllExport const std::wstring& Latitude() const;
    DllExport const std::wstring& Longitude() const;
    double LatitudeDegrees() const;
    double LongitudeDegrees() const;
    DllExport bool InEurope() const;
    DllExport std::wstring TowerKey() const;
    bool ClearTenorKeysIfInvalidChars();
    DllExport const std::wstring& AlsoKnownAs() const;
    DllExport const Duco::PerformanceDate& FirstRungDate() const;
    DllExport bool FirstRungDateSet(const Duco::RingingDatabase& database, bool& byPeal) const;
    DllExport bool FirstRungDateSet() const;
    DllExport std::wstring TowerbaseId() const;
    DllExport bool ValidTowerbaseId() const;
    DllExport const Duco::ObjectId& LinkedTowerId() const;
    DllExport const Duco::ObjectId& PictureId() const;

    DllExport bool PositionValid() const;
    DllExport bool HasDifferentPosition(const Duco::Tower& other) const;
    DllExport bool TowerbaseIdMatchorNull(const Duco::Tower& other) const;
    DllExport double DistanceFrom(const Duco::Tower& other) const;
    DllExport bool ContainsSpecialBells() const;

    DllExport unsigned int MinimumNoOfBells() const;

    // settors
    DllExport void SetName(const std::wstring& newName);
    DllExport void SetFullName(const std::wstring& newName);
    DllExport void SetCity(const std::wstring& newCity);
    DllExport bool UppercaseCity();
    DllExport void SetCounty(const std::wstring& newCounty);
    DllExport bool SetNoOfBells(unsigned int newNoOfBells);
    DllExport bool ReduceNoOfBellsInNewTower(unsigned int newNoOfBells);
    DllExport bool UpdateRing(const Duco::ObjectId& ringId, const Duco::Ring& ring);
    inline void SetRemoved(bool newRemoved);
    inline void SetAntiClockwise(bool newAntiClockwise);
    inline void SetHandbell(bool newHandbell);
    DllExport void SetRenamedBells(const std::map<unsigned int, TRenameBellType>& renames);
    DllExport void SetNotes(const std::wstring& newNotes);
    DllExport void SetDoveRef(const std::wstring& newRef);
    DllExport void SetLatitude(const std::wstring& newPos);
    DllExport void SetLongitude(const std::wstring& newPos);
    DllExport void SetAlsoKnownAs(const std::wstring& newAKA);
    DllExport void SetFirstRungDate(const Duco::PerformanceDate& newFirstRungDate);
    DllExport void SetTowerbaseId(const std::wstring& newReference);
    DllExport void SetLinkedTowerId(const Duco::ObjectId& newLinkedTowerId);
    DllExport void SetPictureId(const Duco::ObjectId& newPictureId);

    DllExport void SetAllRingsMaxBellCount();
    DllExport bool Valid(const RingingDatabase& ringingDb, bool warningFatal, bool clearBeforeStart) const;
    void SetError(TTowerErrorCodes code);

    DllExport bool RenumberRings(std::map<Duco::ObjectId, Duco::ObjectId>& oldRingIdsToNewRingIds);

    DllExport virtual bool operator==(const Duco::RingingObject& other) const;
    DllExport virtual bool operator!=(const Duco::RingingObject& other) const;
    DllExport virtual Tower& operator=(const Duco::Tower& other);

    DllExport void SetDoveAndLocation(const Duco::DoveObject& other);
    DllExport bool UpdateDoveLocationAndOtherIds(const Duco::DoveObject& other);
    DllExport bool UpdateTenorFromDoveAndOtherIds(const Duco::DoveObject& other);
    bool CapitaliseField(bool county, bool dedication);

    DllExport bool Match(const Duco::DoveObject& object) const;
    DllExport unsigned int MatchProbability(const Duco::DoveObject& object, const Duco::DoveDatabase& doveDb) const;
    DllExport unsigned int MatchProbability(const std::wstring& towerNameToMatch, const std::wstring& townNameToMatch, const std::wstring& countyNameToMatch, unsigned int requiredNoOfBells, const std::wstring& tenorWeight, const std::wstring& tenorKey, bool fromImport) const;

    DllExport std::wostream& Print(std::wostream& stream, const RingingDatabase& database) const;
    DllExport std::wostream& PrintTenorDescription(std::wostream& stream, const ObjectId& ringId) const;
    DatabaseWriter& Externalise(DatabaseWriter& writer) const;
    DatabaseReader& Internalise(DatabaseReader& reader, unsigned int databaseVersion);
    std::wofstream& ExportToRtf(std::wofstream& outputFile, const RingingDatabase& database) const;
    void ExportToCsv(std::wofstream& file) const;
    void ExportToXmlInPeal(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& towerElement, const Duco::ObjectId& ringId, bool includeIds) const;
    virtual void ExportToXml(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& parent, bool exportDucoObjects) const;

protected:
    friend void RingingObject::SetId(const Duco::ObjectId& newId);
    unsigned int TenorWeightMatchProbability(const std::wstring& tenorRequired, const std::wstring& tenorInARing) const;
    unsigned int TenorKeyMatchProbability(const std::wstring& tenorRequired, const std::wstring& tenorInARing) const;

    void DeleteAllRings();
    void CopyAllRings(const std::map<Duco::ObjectId, Duco::Ring*>& otherRings);
    bool AddRing(Duco::Ring* ring, bool fromImport);
    /*void UpdateTenorToDove(const std::wstring& newTenor, int noOfbells);*/

    bool DoRenumberRings(std::map<Duco::ObjectId, Duco::ObjectId> oldRingsByNoOfBells);
    bool SwapRings(const Duco::ObjectId& leftId, const Duco::ObjectId& rightId);

    bool AllRingsUniqueBells() const;
    unsigned int MaxNoOfBellsInRing() const;

private:
    std::wstring                        name;
    std::wstring                        city;
    std::wstring                        county;
    std::wstring                        notes;
    std::wstring                        doveReference;
    std::wstring                        latitude;
    std::wstring                        longitude;
    std::wstring                        aka;

    unsigned int                        noOfBells;
    bool                                handbell;
    Duco::PerformanceDate               firstRungDate;

    // list of all rings in the tower, where the key is the id of the ring
    std::map<Duco::ObjectId, Duco::Ring*> rings;
    bool                                removed;
    bool                                antiClockwise;

    std::map<unsigned int, TRenameBellType> renamedBells;

    std::wstring                        towerbaseId;
    Duco::ObjectId                      linkedTowerId;
    Duco::ObjectId                      pictureId;
};

bool
Tower::Removed() const
{
    return removed;
}

void
Tower::SetRemoved(bool newRemoved)
{
    removed = newRemoved;
}

bool
Tower::AntiClockwise() const
{
    return antiClockwise;
}

void
Tower::SetAntiClockwise(bool newAntiClockwise)
{
    antiClockwise = newAntiClockwise;
}

bool
Tower::Handbell() const
{
    return handbell;
}

void
Tower::SetHandbell(bool newHandbell)
{
    handbell = newHandbell;
}

} // end namespace Duco

#endif //!__TOWER_H__
