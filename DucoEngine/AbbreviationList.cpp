#include "AbbreviationList.h"

#include "AbbreviationObject.h"
#include <fstream>
#include <algorithm>

using namespace std;
using namespace Duco;

namespace Duco
{
    bool sortAbbreviationObjects(AbbreviationObject* i, AbbreviationObject* j) { return ((*i) < (*j)); }
}

AbbreviationList::AbbreviationList(const std::string& fileName)
{
    wifstream* inputFile = new wifstream(fileName.c_str());
    if (inputFile != NULL && inputFile->is_open())
    {
        while (!inputFile->eof())
        {
            std::wstring nextLine;
            getline(*inputFile, nextLine);
            if (nextLine.length() > 0 && nextLine[0] != '#')
            {
                AbbreviationObject* newObject = new AbbreviationObject(nextLine);
                if (newObject->Valid())
                {
                    objects.push_back(newObject);
                }
                else
                {
                    delete newObject;
                }
            }
        }
    }
    std::sort(objects.begin(), objects.end(), sortAbbreviationObjects);
    delete inputFile;
    inputFile = NULL;
}

AbbreviationList::~AbbreviationList()
{
    for (auto& it : objects )
    {
        delete it;
    }
    objects.clear();
}

bool
AbbreviationList::ProperName(const std::wstring& nameToCheck, std::wstring& properName) const
{
    bool matchFound (false);
    properName = nameToCheck;

    if (nameToCheck.length() != 0)
    {
        for (auto const& it : objects)
        {
            if (it->UpdateName(properName))
            {
                matchFound = true;
            }
        }
    }

    if (!matchFound)
    {
        notUpdatedAbbreviations.insert(nameToCheck);
    }

    return matchFound;
}

size_t
AbbreviationList::NoOfObjects() const
{
    return objects.size();
}

size_t
AbbreviationList::SaveAbbreviationsNotUpdated(const std::string& fileName)
{
    wofstream* inputFile = new wofstream(fileName.c_str());
    for (auto const& it : notUpdatedAbbreviations)
    {
        (*inputFile) << '#' << it;
        (*inputFile) << "\n";
    }
    inputFile->close();
    delete inputFile;

    return notUpdatedAbbreviations.size();
}
