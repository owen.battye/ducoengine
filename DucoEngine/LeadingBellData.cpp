#include "LeadingBellData.h"

using namespace Duco;

LeadingBellData::LeadingBellData(const Duco::ObjectId& theTowerId, size_t theBellNumber, size_t thePealCount)
    : towerId(theTowerId), bellNumber(theBellNumber), pealCount(thePealCount)
{

}

LeadingBellData::~LeadingBellData()
{
}

const Duco::ObjectId&
LeadingBellData::TowerId() const
{
    return towerId;
}

size_t
LeadingBellData::BellNumber() const
{
    return bellNumber;
}

size_t
LeadingBellData::PealCount() const
{
    return pealCount;
}

bool
LeadingBellData::operator<(const Duco::LeadingBellData& rhs) const
{
    if (rhs.pealCount != pealCount)
    {
        return rhs.pealCount < pealCount;
    }
    else if (rhs.bellNumber != bellNumber)
    {
        return rhs.bellNumber < bellNumber;
    }
    return rhs.towerId < towerId;
}
