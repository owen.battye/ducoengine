#ifndef __PEAL_H__
#define __PEAL_H__

#include <map>
#include <set>
#include <string>

#include "DucoTypes.h"
#include "DucoEngineCommon.h"
#include "RingingObject.h"
#include "PerformanceTime.h"
#include "PerformanceDate.h"
#include "PealRingerData.h"
#include <xercesc\dom\DOMDocument.hpp>
#include "PealLengthInfo.h"

namespace Duco
{
class AssociationDatabase;
class AlphabetData;
class DatabaseReader;
class DatabaseWriter;
class DatabaseSettings;
class Ring;
class RingerDatabase;
class RingingDatabase;
class RingerCirclingData;
class MethodDatabase;
class TowerDatabase;
class CompositionDatabase;
class MethodSeriesDatabase;

class Peal : public RingingObject
{
    friend class PealDatabase;

public:
    DllExport Peal(const Duco::ObjectId& newId, const Duco::ObjectId& newAssociationId, const Duco::ObjectId& newTowerId,
        const ObjectId& newRingId, const ObjectId& newMethodId, const std::wstring& newComposer, const std::wstring& newFootnotes,
        const Duco::PerformanceDate& newDate, const Duco::PerformanceTime& newTime, unsigned int newChanges, bool newHandBell,
        const std::map<unsigned int, Duco::ObjectId>& newRingerIds, TConductorType newConductorType, const std::set<Duco::ObjectId>& conductorIds,
        unsigned int dayOrder);
    DllExport Peal();
    DllExport Peal(const Duco::Peal& other);
    DllExport Peal(const Duco::ObjectId& newId, const Duco::Peal& other);
    DllExport Peal(Duco::DatabaseReader& reader, unsigned int databaseVersion);
    DllExport virtual ~Peal();
    DllExport virtual TObjectType ObjectType() const;
    DllExport virtual bool Duplicate(const Duco::RingingObject& other, const Duco::RingingDatabase& database) const;

    DllExport bool operator==(const Duco::RingingObject& other) const;
    DllExport bool operator!=(const Duco::RingingObject& other) const;
    DllExport Peal& operator=(const Duco::Peal& other);

    DllExport bool operator<(const Duco::Peal& other) const;
    DllExport bool operator>(const Duco::Peal& other) const;

    // Accessors
    inline const Duco::ObjectId& AssociationId() const;
    inline const std::wstring& OldAssociation() const;
    inline bool ContainsFootnotes() const;
    inline const std::wstring& Footnotes() const;
    inline bool ContainsComposer() const;
    inline const std::wstring& Composer() const;
    inline bool ContainsMultiMethods() const;
    inline const std::wstring& MultiMethods() const;
    inline const Duco::ObjectId& PictureId() const;
    inline const Duco::ObjectId& TowerId() const;
    inline const Duco::ObjectId& RingId() const;
    inline const Duco::ObjectId& MethodId() const;
    inline const Duco::ObjectId& CompositionId() const;
    inline const Duco::ObjectId& SeriesId() const;
    inline TConductorType ConductedType() const;
    inline const std::set<ObjectId>& Conductors() const;
    DllExport Duco::ObjectId FirstConductor() const;
    inline unsigned int NoOfChanges() const;
    DllExport unsigned int NoOfBellsInMethod(const Duco::MethodDatabase& methodDatabase, bool& dualOrderMethod) const;
    DllExport unsigned int NoOfBellsInTower(const Duco::TowerDatabase& towerDatabase) const;
    DllExport virtual unsigned int NoOfRingers(bool includeStrappers = true) const;
    DllExport virtual unsigned int NoOfBellsRung(const Duco::RingingDatabase& database) const;
    DllExport virtual unsigned int NoOfBellsRung(const Duco::TowerDatabase& towersDb, const Duco::MethodDatabase& methodsDb) const;
    inline const Duco::PerformanceDate& Date() const;
    inline const Duco::PerformanceTime& Time() const;
    inline bool Handbell() const;
    inline bool WrongStage() const;
    bool PossibleHandbellPeal() const;
    DllExport void RingerIds(std::set<ObjectId>& allIds) const;
    DllExport bool RingerId(unsigned int bellNo, bool strapper, Duco::ObjectId& ringerId) const;
    void AddPealCount(std::map<ObjectId, Duco::PealLengthInfo>& sortedRingerIds) const;
    DllExport bool StrapperOnBell(unsigned int bellNo) const;
    DllExport bool ShowStrapperOption(const Duco::RingingDatabase& ringingDb, unsigned int bellNo) const;
    DllExport bool AnyStrappers() const;
    DllExport const std::wstring& BellBoardId() const;
    DllExport std::wstring BellBoardLink(const Duco::DatabaseSettings& settings) const;
    DllExport std::wstring WebsiteLink(const Duco::DatabaseSettings& settings) const;
    DllExport std::wstring RingingWorldIssue() const;
    DllExport std::wstring RingingWorldPage() const;
    DllExport const std::wstring& RingingWorldReference() const;
    DllExport std::wstring RingingWorldLink(const Duco::DatabaseSettings& settings) const;
    DllExport std::wstring FindReference() const;
    DllExport const std::wstring& Notes() const;
    inline unsigned int DayOrder() const;
    inline bool Withdrawn() const;
    inline bool DoubleHanded() const;
    inline bool Notable() const;
    inline bool SimulatedSound() const;
    DllExport void ShortTitle(std::wstring& title) const;
    DllExport std::wstring ShortTitleWithDate() const;
    DllExport unsigned int PealFee(unsigned int bellNo, bool strapper) const;
    DllExport unsigned int TotalPealFeePaid() const;
    DllExport unsigned int AvgPealFeePaidPerPerson() const;
    DllExport bool CheckPaidFeeAgainstMedian(unsigned int minPealFeeInPencePerPerson) const;
    DllExport unsigned int MissingPealFee(unsigned int avgPealFeePaidPerPersonThisYear) const;
    DllExport bool CheckForDoubleHandedRingers() const;
    DllExport const std::wstring& EnteredBy() const;

    DllExport Duco::ObjectId GetFeePayerId() const;
    DllExport Duco::ObjectId SuggestFeePayerId(const std::set<ObjectId>& commonFeePayers) const;
    
    DllExport void Title(std::wstring& title, const RingingDatabase& database) const;
    DllExport void Title(std::wstring& title, const TowerDatabase& towerDatabase, const MethodDatabase& methodDatabase) const;
    DllExport void SummaryTitle(std::wstring& title, const RingingDatabase& database) const;
    DllExport std::wstring OnThisDayTitle(const RingingDatabase& database) const;
    DllExport std::wstring Details(const RingingDatabase& database) const;
    DllExport bool AlphabetLetter(const MethodDatabase& methodsDatabase,
                        const std::wstring& methodType, unsigned int requiredNoOfBells,
                        AlphabetData& alphabets, bool uniqueMethods) const;

    DllExport std::wstring ErrorString(const Duco::DatabaseSettings& settings, bool showWarnings) const;

    // Convenience Accessors
    DllExport std::wstring AssociationName(const Duco::AssociationDatabase& associationDatabase) const;
    DllExport std::wstring AssociationName(const Duco::RingingDatabase& ringingDatabase) const;
    DllExport std::wstring TowerName(const Duco::TowerDatabase& towerDatabase) const;
    DllExport std::wstring TowerName(const Duco::RingingDatabase& ringingDatabase) const;
    DllExport std::wstring MethodName(const Duco::MethodDatabase& methodDatabase) const;
    DllExport std::wstring MethodName(const Duco::RingingDatabase& ringingDatabase) const;
    DllExport std::wstring RingerName(unsigned int bellNo, bool strapper, const Duco::RingerDatabase& ringerDatabase, const Duco::DatabaseSettings& settings) const;
    DllExport std::wstring RingerName(unsigned int bellNo, bool strapper, const Duco::RingingDatabase& ringingDatabase) const;
    DllExport virtual std::wstring ConductorName(const RingingDatabase& database) const;
    DllExport virtual std::wstring ConductorName(const RingerDatabase& database, const Duco::DatabaseSettings& settings) const;

    // settors
    DllExport virtual void Clear();
    inline void SetPictureId(const Duco::ObjectId& newId);
    DllExport virtual void SetTowerId(const Duco::ObjectId& newTowerId);
    DllExport virtual void SetRingId(const Duco::ObjectId& newRingId);
    inline void SetMethodId(const Duco::ObjectId& newMethodId);
    inline void SetCompositionId(const Duco::ObjectId& newCompositionId);
    inline void SetSeriesId(const Duco::ObjectId& newSeriesId);
    inline void SetDate(const Duco::PerformanceDate& newDate);
    inline void SetTime(const Duco::PerformanceTime& newTime);
    inline void SetHandbell(bool newHandbell);
    inline void SetWrongStage(bool newWrongStage);
    void ConvertToHandbellPeal();
    inline void SetChanges(unsigned int newChanges);
    inline void SetFootnotes(const std::wstring& newFootnotes);
    DllExport void AddMultiMethods(unsigned int noOfMethods, const std::wstring& methods);
    inline void SetMultiMethods(const std::wstring& methods);
    inline void SetComposer(const std::wstring& newComposer);
    DllExport virtual void SetAssociationId(const Duco::ObjectId& newAssociationId);
    void SetAssociationIdAndClearOldAssociation(const Duco::ObjectId& newAssociationId);
    DllExport virtual void SetRingerId(const ObjectId& newRingerId, unsigned int newBellNumber, bool asStrapper);
    DllExport void RemoveRinger(unsigned int bellNumber, bool asStrapper);
    DllExport bool ReplaceRingerId(const ObjectId& oldRingerId, const ObjectId& newRingerId);
    DllExport bool ReplaceRingers(const std::map<ObjectId, ObjectId>& newRingerIds);
    DllExport void SetConductedType(TConductorType type);
    DllExport void SetConductorId(const ObjectId& newRingerId);
    inline void ClearConductors();
    DllExport void SetConductorIdFromBell(unsigned int newBellId, bool checked, bool strapper);
    DllExport void SetBellBoardId(const std::wstring& newBellBoardId);
    DllExport void SetRingingWorldReference(const std::wstring& newRWRef);
    DllExport void SetNotes(const std::wstring& newNotes);
    inline void SetDayOrder(unsigned int order);
    DllExport void AddStrapper(unsigned int bellNo, bool remove);
    inline void SetWithdrawn(bool newWithdrawn);
    inline void SetDoubleHanded(bool newDoubleHanded);
    inline void SetNotable(bool newNotable);
    inline void SetSimulatedSound(bool newSimualtedSound);
    DllExport bool SetPealFee(unsigned int bellNo, bool strapper, unsigned int pealFeeInPence);
    bool ReplaceTowerId(const ObjectId& oldTowerId, const ObjectId& newTowerId, const std::map<Duco::ObjectId, Duco::ObjectId>& otherRingIds);
    void SetError(TPealErrorCodes code);
    DllExport void SetEnteredBy(const std::wstring& newName);

    // Queries
    DllExport bool ValidRingingWorldReferenceForUpload() const;
    DllExport bool ValidRingingWorldLink() const;
    DllExport bool ValidWebsiteLink() const;
    DllExport bool ValidBellBoardId() const;
    bool ContainsDeceasedRinger(const RingerDatabase& ringersDb) const;

    DllExport bool Valid(const Duco::RingingDatabase& database, bool warningFatal, bool clearBeforeStart) const;
    DllExport bool ValidPeal(const Duco::RingerDatabase& ringersDatabase, const Duco::MethodDatabase& methodsDatabase, const Duco::TowerDatabase& towersDatabase,
                             const Duco::CompositionDatabase& compostionsDb, const Duco::MethodSeriesDatabase& methodSeriesDb,
                             const AssociationDatabase& associationDb,
                             const Duco::DatabaseSettings& settings, bool warningFatal, bool clearBeforeStart) const;
    DllExport bool ValidPealConductors(bool warningFatal, bool clearBeforeStart) const;
    DllExport bool ValidPealRingers(const Duco::RingingDatabase& database, bool warningFatal, bool clearBeforeStart) const;
    DllExport bool ValidPealRingers(const Duco::RingerDatabase& ringersDatabase, const Duco::MethodDatabase& methodDatabase, const Duco::TowerDatabase& towerDatabase, const Duco::DatabaseSettings& settings, bool warningFatal, bool clearBeforeStart) const;
    DllExport bool ValidPealDetails(const Duco::RingingDatabase& database, bool warningFatal, bool clearBeforeStart) const;
    DllExport bool ValidPealDetails(const Duco::MethodDatabase& database, const TowerDatabase& towerDb, const Duco::CompositionDatabase& compDB, const Duco::MethodSeriesDatabase& medthodSeriesDB, const AssociationDatabase& associationDb, const Duco::DatabaseSettings& settings, bool warningFatal, bool clearBeforeStart) const;
    DllExport bool ContainsRinger(const ObjectId& ringerId, bool& strapper) const;
    DllExport bool ContainsAnyRinger(const std::set<ObjectId>& ringerIds) const;
    DllExport virtual bool ContainsConductor(const ObjectId& ringerId) const;
    DllExport virtual bool ConductorOnBell(unsigned int bellNo, bool strapper) const;
    DllExport bool BellRung(const ObjectId& ringerId, std::set<unsigned int>& bellsRung, bool& strapper) const;
    DllExport bool HandBellsRung(const ObjectId& ringerId, std::wstring& bellNumbers) const;
    DllExport float ChangesPerMinute() const;
    DllExport bool ChangesAndTimeValid() const;
    DllExport bool ContainsNonHumanRinger(const RingerDatabase& database) const;

    DllExport bool FindRingerBellByName(const Duco::RingingDatabase& database, const std::wstring& fieldValue, bool subStr, unsigned int& bellNo) const;
    bool FindRingerBellByName(const Duco::RingerDatabase& database, const Duco::DatabaseSettings& settings, const std::wstring& fieldValue, bool subStr, unsigned int& bellNo) const;
    DllExport bool CheckRingerNames(const Duco::RingingDatabase& database, bool excluding, const std::wstring& fieldValue, bool subStr) const;
    bool CheckRingerNames(const Duco::RingerDatabase& ringersDb, const Duco::DatabaseSettings& settings, bool excluding, const std::wstring& fieldValue, bool subStr) const;
    bool CheckStrapperName(const Duco::RingingDatabase& database, const std::wstring& fieldValue, bool subStr) const;

    DllExport bool DuplicateRingers(std::map<Duco::ObjectId, unsigned int>& duplicateRingers) const;
    DllExport bool ContainsDefaultRinger(const Duco::DatabaseSettings& settings, bool& asStrapper) const;
    void AddRingersToCirclingData(std::map<Duco::ObjectId, Duco::RingerCirclingData*>& data, const Duco::Ring& theRing) const;

    // Input / Output
    DllExport std::wostream& Print(std::wostream& stream, const RingingDatabase& database) const;
    DllExport DatabaseWriter& Externalise(DatabaseWriter& writer) const;
    void ExternaliseToBellboardXml(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& document, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& rootElement, const Duco::RingerDatabase& ringersDatabase, const Duco::MethodDatabase& methodsDatabase, const Duco::TowerDatabase& towersDatabase, const Duco::AssociationDatabase& associationDatabase) const;
    std::wofstream& ExportToRtf(std::wofstream& outputFile, const RingingDatabase& database) const;
    void ExportToCsv(std::wofstream& file, const Duco::RingerDatabase& ringersDatabase, const Duco::MethodDatabase& methodsDatabase, const Duco::TowerDatabase& towersDatabase, const Duco::AssociationDatabase& associationDatabase) const;
    DllExport void ExportToXml(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& performances, bool includeIds, const Duco::RingerDatabase& ringersDatabase, const Duco::MethodDatabase& methodsDatabase, const Duco::TowerDatabase& towersDatabase, const Duco::AssociationDatabase& associationDatabase) const;
    DllExport virtual void ExportToXml(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument& outputFile, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement& parent, bool exportDucoObjects) const;

protected:
    DllExport DatabaseReader& Internalise(DatabaseReader& reader, unsigned int databaseVersion);
    bool RemoveConductor(const ObjectId& conductorId);
    void WriteBooleanToCsv(std::wofstream& file, bool state) const;
    void WriteStringWithReplaceToCsv(std::wofstream& file, const std::wstring& originalString, const std::wstring& strToFind = L"\n", const std::wstring& strToReplaceWith = L"") const;
    bool CompareRingerIds(const Duco::Peal& other, const Duco::RingerDatabase& allRingers, const Duco::DatabaseSettings& settings) const;

protected:
    Duco::ObjectId          associationId;
    std::wstring            oldAssociationAsString;
    Duco::ObjectId          pictureId;
    Duco::ObjectId          towerId;
    Duco::ObjectId          ringId;
    Duco::ObjectId          methodId;
    std::wstring            multiMethods;
    unsigned int            noOfMultiMethods;
    Duco::ObjectId          compositionId;
    Duco::ObjectId          seriesId;
    std::wstring            composer;
    std::wstring            footnotes;
    Duco::PerformanceDate   performanceDate;
    Duco::PerformanceTime   performanceTime;
    bool                    handBell;
    bool                    wrongStage;
    unsigned int            noOfChanges;
    std::map<unsigned int, PealRingerData> ringerIds;
    std::map<unsigned int, PealRingerData> strapperIds;
    TConductorType          conductedType;
    std::set<Duco::ObjectId> conductorIds;
    std::wstring            bellBoardId;
    std::wstring            ringingWorldReference;
    std::wstring            notes;
    unsigned int            dayOrder;
    bool                    withdrawn;
    bool                    doubleHanded;
    bool                    notable;
    bool                    containsSimulatedSound;
    std::wstring            enteredBy;
};

const Duco::ObjectId& 
Peal::AssociationId() const
{
    return associationId;
}

const std::wstring&
Peal::OldAssociation() const
{
    return oldAssociationAsString;
}

const std::wstring&
Peal::Footnotes() const
{
    return footnotes;
}

const ObjectId&
Peal::PictureId() const
{
    return pictureId;
}

const ObjectId&
Peal::TowerId() const
{
    return towerId;
}

unsigned int
Peal::NoOfChanges() const
{
    return noOfChanges;
}

const ObjectId&
Peal::RingId() const
{
    return ringId;
}

const ObjectId&
Peal::MethodId() const
{
    return methodId;
}

TConductorType
Peal::ConductedType() const
{
    return conductedType;
}

const std::wstring& 
Peal::Composer() const
{
    return composer;
}

const std::wstring&
Peal::MultiMethods() const
{
    return multiMethods;
}

const Duco::PerformanceDate&
Peal::Date() const
{
    return performanceDate;
}

const Duco::PerformanceTime&
Peal::Time() const
{
    return performanceTime;
}

bool
Peal::Handbell() const
{
    return handBell;
}

bool
Peal::WrongStage() const
{
    return wrongStage;
}

void
Peal::SetPictureId(const ObjectId& newId)
{
    pictureId = newId;
}

void
Peal::SetMethodId(const Duco::ObjectId& newMethodId)
{
    methodId = newMethodId;
}

void
Peal::SetDate(const Duco::PerformanceDate& newDate)
{
    performanceDate = newDate;
}

void
Peal::SetTime(const Duco::PerformanceTime& newTime)
{
    performanceTime = newTime;
}

void
Peal::SetHandbell(bool newHandbell)
{
    handBell = newHandbell;
}

void
Peal::SetWrongStage(bool newWrongStage)
{
    wrongStage = newWrongStage;
}

void
Peal::SetChanges(unsigned int newChanges)
{
    noOfChanges = newChanges;
}

void
Peal::SetFootnotes(const std::wstring& newFootnotes)
{
    footnotes = newFootnotes;
}

void
Peal::SetMultiMethods(const std::wstring& methods)
{
    multiMethods = methods;
}

void
Peal::SetComposer(const std::wstring& newComposer)
{
    composer = newComposer;
}

void
Peal::SetDayOrder(unsigned int order)
{
    dayOrder = order;
}

unsigned int
Peal::DayOrder() const
{
    return dayOrder;
}

const std::set<ObjectId>&
Peal::Conductors() const
{
    return conductorIds;
}

void 
Peal::ClearConductors()
{
    conductorIds.clear();
}

bool 
Peal::ContainsFootnotes() const
{
    return footnotes.length() > 0;
}

bool 
Peal::ContainsComposer() const
{
    return composer.length() > 0;
}

bool 
Peal::ContainsMultiMethods() const
{
    return multiMethods.length() > 4;
}

void
Peal::SetWithdrawn(bool newWithdrawn)
{
    withdrawn = newWithdrawn;
}

bool
Peal::Withdrawn() const
{ 
    return withdrawn;
}

void
Peal::SetDoubleHanded(bool newDoubleHanded)
{
    doubleHanded = newDoubleHanded;
}

bool
Peal::DoubleHanded() const
{ 
    return doubleHanded;
}

void
Peal::SetNotable(bool newNotable)
{
    notable = newNotable;
}

bool
Peal::Notable() const
{
    return notable;
}

const ObjectId&
Peal::CompositionId() const
{
    return compositionId;
}

const ObjectId&
Peal::SeriesId() const
{
    return seriesId;
}

void
Peal::SetCompositionId(const ObjectId& newCompositionId)
{
    compositionId = newCompositionId;
}

void
Peal::SetSeriesId(const ObjectId& newSeriesId)
{
    seriesId = newSeriesId;
}

void
Peal::SetSimulatedSound(bool newSimualtedSound)
{
    containsSimulatedSound = newSimualtedSound;
}

bool
Peal::SimulatedSound() const
{
    return containsSimulatedSound;
}


} // end namespace Duco

#endif // __PEAL_H__


