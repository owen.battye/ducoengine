#include "PealBaseTsvImporter.h"

#include <fstream>
#include "AssociationDatabase.h"
#include "RingingDatabase.h"
#include "TowerDatabase.h"
#include "PealDatabase.h"
#include "MethodDatabase.h"
#include "RingerDatabase.h"
#include "DucoEngineUtils.h"
#include "Peal.h"
#include "Tower.h"
#include "Method.h"
#include "Ringer.h"

#include <cstdlib>

using namespace std;
using namespace Duco;

namespace Duco
{
const std::string KBandsSuffix("bands");
const std::string KPealsSuffix("peals");
const std::string KTowersSuffix("towers");
const std::string KFileTypeSuffix(".tsv");
}

PealBaseTsvImporter::PealBaseTsvImporter(Duco::ProgressCallback& theCallback,
            Duco::RingingDatabase& theDatabase)
    : FileImporter(theCallback, theDatabase), pealsOnDate(0), processingStage(ENotStarted)
{
}

void
PealBaseTsvImporter::GetTotalFileSize(const std::string& fileName)
{
    if (FindFileNames(fileName))
    {
        bandsFilesize = GetFileSize(bandsFileName);
        towersFilesize = GetFileSize(towersFileName);
        pealsFilesize = GetFileSize(pealsFileName);
        totalFileSize = bandsFilesize + towersFilesize + pealsFilesize;

    }
}

PealBaseTsvImporter::~PealBaseTsvImporter()
{
    std::map<unsigned int, TowerInfo*>::iterator it = towers.begin();
    while (it != towers.end())
    {
        delete it->second;
        ++it;
    }
    std::multimap<unsigned int, RingerInfo*>::iterator it2 = ringers.begin();
    while (it2 != ringers.end())
    {
        delete it2->second;
        ++it2;
    }
}

bool
PealBaseTsvImporter::FindFileNames(const std::string& fileName)
{
    size_t extIndex = fileName.rfind(KFileTypeSuffix);
    if (extIndex < 0)
        return false;

    std::string fileNameWithoutExt = fileName.substr(0, extIndex);

    size_t typeIndex = fileNameWithoutExt.rfind(KBandsSuffix);
    if (typeIndex != string::npos)
    {
        bandsFileName = fileName;
    }
    else
    {
        typeIndex = fileNameWithoutExt.rfind(KPealsSuffix);
        if (typeIndex != string::npos)
        {
            pealsFileName = fileName;
        }
        else
        {
            typeIndex = fileNameWithoutExt.rfind(KTowersSuffix);
            if (typeIndex != string::npos)
            {
                towersFileName = fileName;
            }
            else
            {
                return false;
            }
        }
    }

    if (typeIndex <= 0)
        return false;

    std::string fileNameWithoutSuffix = fileName.substr(0, typeIndex);
    if (fileNameWithoutSuffix.length() <= 0)
        return false;

    if (bandsFileName.length() <= 0)
    {
        bandsFileName += fileNameWithoutSuffix;
        bandsFileName += KBandsSuffix;
        bandsFileName += KFileTypeSuffix;
    }
    if (pealsFileName.length() <= 0)
    {
        pealsFileName += fileNameWithoutSuffix;
        pealsFileName += KPealsSuffix;
        pealsFileName += KFileTypeSuffix;
    }
    if (towersFileName.length() <= 0)
    {
        towersFileName += fileNameWithoutSuffix;
        towersFileName += KTowersSuffix;
        towersFileName += KFileTypeSuffix;
    }

    return bandsFileName.length() > 0 && pealsFileName.length() > 0 && towersFileName.length() > 0;
}

void
PealBaseTsvImporter::ReadFiles(const std::string& fileName)
{
    processingStage = EBands;
    ReadFile(bandsFileName);
    processedFileSize = bandsFilesize;
    processingStage = ETowers;
    ReadFile(towersFileName);
    processedFileSize = bandsFilesize + towersFilesize;
    processingStage = EPeals;
    ReadFile(pealsFileName);
}

bool 
PealBaseTsvImporter::ProcessTower(const std::wstring& nextLine)
{
    std::vector<std::wstring> tokens;
    TokeniseLine(nextLine, tokens, true, true);

    std::vector<std::wstring>::const_iterator it = tokens.begin();
    if (it != tokens.end())
    {
        TowerInfo* newTower = new TowerInfo();
        std::wstring towerIdStr = (*it);
        if (towerIdStr.size() > 1 && towerIdStr[0] == '-')
        {
            newTower->handbell = true;
            towerIdStr = towerIdStr.substr(1);
        }
        else
            newTower->handbell = false;

        unsigned int towerId = _wtoi(towerIdStr.c_str());

        ++it;
        if (it != tokens.end())
        {
            if (newTower->handbell)
                newTower->town = DucoEngineUtils::Trim(*it);
            else
                newTower->town = DucoEngineUtils::Trim(*it);
            ++it;
        }
        if (it != tokens.end())
        {
            if (newTower->handbell)
                newTower->name = DucoEngineUtils::Trim(*it);
            else
                newTower->county = DucoEngineUtils::Trim(*it);
            ++it;
        }
        if (it != tokens.end())
        {
            if (newTower->handbell)
                newTower->county = DucoEngineUtils::Trim(*it);
            else
                newTower->name = DucoEngineUtils::Trim(*it);
            ++it;
        }
        if (newTower->name.length() <= 0 && newTower->county.length() > 0)
        {
            newTower->name = newTower->county;
            newTower->county.clear();
        }
        std::pair<unsigned int, TowerInfo*> newObject(towerId, newTower);
        if (towers.insert(newObject).second)
        {
            return true;
        }
        else
        {
            delete newTower;
        }
    }
    return false;
}

bool
PealBaseTsvImporter::ProcessRinger(const std::wstring& nextLine)
{
    std::vector<std::wstring> tokens;
    TokeniseLine(nextLine, tokens, false, true);

    std::vector<std::wstring>::const_iterator it = tokens.begin();
    if (it != tokens.end())
    {
        RingerInfo* newRinger = new RingerInfo();
        newRinger->conductor = false;
        newRinger->handbell = false;
        newRinger->bell = -1;
        unsigned int ringerId = _wtoi(it->c_str());

        ++it;
        if (it != tokens.end())
        {
            if (it->find('-') != string::npos)
                newRinger->handbell = true;

            newRinger->bell = _wtoi(it->c_str());
            ++it;
        }
        if (it != tokens.end())
        {
            newRinger->name = *it;
            ++it;
        }
        if (it != tokens.end())
        {
            if (it->length() > 0 && (*it)[0] == 'C')
                newRinger->conductor = true;
            ++it;
        }
        std::pair<unsigned int, RingerInfo*> newObject(ringerId, newRinger);
        ringers.insert(newObject);
    }
    return true;
}

bool
PealBaseTsvImporter::ProcessPeal(const std::wstring& nextLine)
{
    switch (processingStage)
    {
    case EBands:
        return ProcessRinger(nextLine);
    case ETowers:
        return ProcessTower(nextLine);
    default:
        return false;
    case EPeals:
        break;
    }

    std::vector<std::wstring> tokens;
    TokeniseLine(nextLine, tokens, false, true);

    Peal newPeal;
    newPeal.SetHandbell(tokens[1][0] == 'H');

    unsigned int bandId = _wtoi(tokens[0].c_str());
    unsigned int towerId = _wtoi(tokens[6].c_str());
    if (newPeal.Handbell() && tokens[6].length() > 1)
    {
        towerId = _wtoi(tokens[6].substr(1).c_str());
    }
    std::wstring tenorWeight = tokens[5];

    Duco::ObjectId associationId = database.AssociationsDatabase().SuggestAssociationOrCreate(tokens[2]);
    newPeal.SetAssociationId(associationId);
    newPeal.SetDate(PerformanceDate(tokens[3]));

    newPeal.SetTime(PerformanceTime(tokens[4]));
    newPeal.SetChanges(_wtoi(tokens[7].c_str()));
    unsigned int order (-1);
    newPeal.SetMethodId(database.MethodsDatabase().FindOrCreateMethod(tokens[8], order));
    newPeal.SetMultiMethods(tokens[9]);
    newPeal.SetComposer(tokens[10]);

    if (tokens.size() >= 12)
    {
        newPeal.SetFootnotes(tokens[11]);
    }

    if (lastPealDate == newPeal.Date())
    {
        newPeal.SetDayOrder(++pealsOnDate);
    }
    else
    {
        lastPealDate = newPeal.Date();
        pealsOnDate = 0;
    }

    // Find ringers
    pair<multimap<unsigned int, RingerInfo*>::const_iterator,multimap<unsigned int, RingerInfo*>::const_iterator> pealRingers = ringers.equal_range(bandId);
    multimap<unsigned int, RingerInfo*>::const_iterator ringersIt = pealRingers.first;
    unsigned int bellNo(0);
    unsigned int noOfConductors (0);
    newPeal.SetConductedType(Duco::ESingleConductor);
    while (ringersIt != pealRingers.second)
    {
        ObjectId ringerId = database.RingersDatabase().FindIdenticalRinger(ringersIt->second->name);
        if (!ringerId.ValidId())
        {
            ringerId = database.RingersDatabase().AddRinger(ringersIt->second->name);
        }
        newPeal.SetRingerId(ringerId, ++bellNo, false);
        if (ringersIt->second->conductor)
        {
            ++noOfConductors;
            if (noOfConductors > 1)
            {
                newPeal.SetConductedType(Duco::EJointlyConducted);
            }
            newPeal.SetConductorId(ringerId);
        }
        ++ringersIt;
    }
    if (noOfConductors == 0)
    {
        newPeal.SetConductedType(Duco::ESilentAndNonConducted);
    }
    if (newPeal.Handbell())
        bellNo *= 2;

    //Find tower
    std::map<unsigned int, TowerInfo*>::const_iterator towerIt = towers.find(towerId);
    if (towerIt != towers.end())
    {
        if (!towerIt->second->towerId.ValidId())
        {
            Tower newTower(KNoId, towerIt->second->name, towerIt->second->town, towerIt->second->county, bellNo);
            newPeal.SetRingId(newTower.AddDefaultRing(L"", tokens[5]));
            towerIt->second->towerId = database.TowersDatabase().AddObject(newTower);
        }
        newPeal.SetTowerId(towerIt->second->towerId);
        Tower updatedTower (*database.TowersDatabase().FindTower(towerIt->second->towerId));
        ObjectId ringId;
        if (updatedTower.SuggestRing(bellNo, tenorWeight, L"", ringId, true))
        {
            newPeal.SetRingId(ringId);
        }
        else
        {
            if (updatedTower.Bells() < bellNo)
            {
                updatedTower.SetNoOfBells(bellNo);
                newPeal.SetRingId(updatedTower.AddDefaultRing(L"", tokens[5]));
            }
            else
            {
                newPeal.SetRingId(updatedTower.AddRing(bellNo, tokens[5], L""));
            }
            database.TowersDatabase().UpdateObject(updatedTower);
        }
    }

    database.PealsDatabase().AddObject(newPeal);
    return newPeal.Valid(database, false, true);
}

bool
PealBaseTsvImporter::EnforceQuotes() const
{
    return false;
}
