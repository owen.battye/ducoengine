#pragma once
#ifndef __REFERENCEITEM_H__
#define __REFERENCEITEM_H__

#include "ObjectId.h"

namespace Duco
{
    class Peal;

    class ReferenceItem
    {
    public:
        DllExport ReferenceItem(const Duco::Peal& newPeal);

        bool AddPerformance(const Duco::Peal& newPeal);
        DllExport bool Valid() const;

        inline size_t NoOfPeals() const { return noOfPeals; }
        DllExport float PercentWithBellboardReference() const;
        DllExport float PercentWithRingingWorldReference() const;

    protected:
        const Duco::ObjectId towerId;
        size_t noOfPeals;
        size_t noOfPealsWithBellboardId;
        size_t noOfPealsWithRingingWorldPage;
    };

}

#endif //!__REFERENCEITEM_H__
