#include "MissingRinger.h"

using namespace Duco;

MissingRinger::MissingRinger()
:    bellNo (0), strapper (false), conductor (false)
{
}

bool
MissingRinger::operator<(const Duco::MissingRinger& lhs) const
{
    if (bellNo != lhs.bellNo)
        return bellNo < lhs.bellNo;

    return strapper < lhs.strapper;
}

namespace Duco
{
    bool
        operator==(const Duco::MissingRinger& lhs, const Duco::MissingRinger& rhs)
    {
        return lhs.operator==(rhs);
    }
}

bool
MissingRinger::operator==(const Duco::MissingRinger& lhs) const
{
    if (bellNo != lhs.bellNo)
        return false;

    if (strapper != lhs.strapper)
        return false;

    return true;
}

std::wstring
MissingRinger::Name(bool lastNameFirst) const
{
    if (lastNameFirst)
        return surname + L", " + forename;

    return forename + L" " + surname;
}
