#ifndef __DATABASESEARCH_H__
#define __DATABASESEARCH_H__

#include <set>
#include <list>
#include <deque>
#include "ObjectType.h"
#include "ObjectId.h"
#include "DucoEngineCommon.h"

namespace Duco
{
    class ProgressCallback;
    class RingingDatabase;
    class TSearchArgument;
    class TSearchValidationArgument;
    class TUpdateArgument;
    class TSearchDuplicateObject;
    class TSearchFieldPealFeesArgument;

class DatabaseSearch
{
public:
    DllExport DatabaseSearch(Duco::RingingDatabase& theDatabase);
    DllExport ~DatabaseSearch();
    DllExport void Clear();

    DllExport size_t AddArgument(Duco::TSearchArgument* newArgument);
    DllExport size_t AddValidationArgument(Duco::TSearchValidationArgument* newArgument);
    DllExport void AddUpdateArgument(Duco::TUpdateArgument* newArgument);

    inline size_t NoOfArguments() const;
    inline bool UpdateArgument() const;

    DllExport void Search(Duco::ProgressCallback& callback, std::set<Duco::ObjectId>& foundIds, Duco::TObjectType objectType) const;
    DllExport bool Update(Duco::ProgressCallback& callback, const std::set<Duco::ObjectId>& objectIds, Duco::TObjectType objectTypeToSearch, Duco::TObjectType objectIdTypesSupplied) const;
    DllExport std::list<Duco::ObjectId> Sort(const std::set<Duco::ObjectId>& objectIds, Duco::TObjectType objectTypeToSearch, const Duco::ObjectId& distanceFrom) const;

protected:
    Duco::TSearchFieldPealFeesArgument*             containsInconsistantFeesCheck;
    bool                                            includeWarnings;

    std::deque<Duco::TSearchArgument*>              searchArguments;
    std::deque<Duco::TSearchValidationArgument*>    validationArguments;
    Duco::TUpdateArgument*                          updateArgument;
    Duco::RingingDatabase&                          database;
};

size_t DatabaseSearch::NoOfArguments() const
{
    return searchArguments.size() + validationArguments.size();
}

bool
DatabaseSearch::UpdateArgument() const
{
    return updateArgument != NULL;
}

} // namespace Duco

#endif //!__DATABASESEARCH_H__
