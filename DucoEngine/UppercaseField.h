#ifndef __CAPITALISEFIELD_H__
#define __CAPITALISEFIELD_H__

#include "UpdateArgument.h"

namespace Duco
{
class TUppercaseField : public TUpdateArgument
{
public:
    DllExport TUppercaseField(Duco::TUpdateFieldId newFieldId);
    DllExport virtual ~TUppercaseField();

    virtual bool Update(Duco::Method& object) const;
    virtual bool Update(Duco::Peal& object) const;
    virtual bool Update(Duco::Tower& object) const;

    virtual Duco::TFieldType FieldType() const;
 
protected:

};
 
}
#endif //__CAPITALISEFIELD_H__

