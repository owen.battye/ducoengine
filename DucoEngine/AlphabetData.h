#ifndef __ALPHABETDATA_H__
#define __ALPHABETDATA_H__

#include "DucoEngineCommon.h"
#include <map>
#include <set>
#include <string>

namespace Duco
{
class AlphabetLetter;
class PerformanceDate;
class ObjectId;

class AlphabetData
{
public:
    DllExport AlphabetData();
    DllExport ~AlphabetData();

    bool AddPeal(const std::wstring& methodName, const Duco::ObjectId& pealId, const Duco::ObjectId& methodId, const Duco::PerformanceDate& pealDate, bool uniqueMethods);
    DllExport unsigned int NumberOfCompletedAlphabets() const;
    DllExport bool Completed(unsigned int circleNumber, Duco::PerformanceDate& completionDate, std::set<Duco::ObjectId>& pealIds) const;
    DllExport void Reset();

    DllExport const AlphabetLetter& operator[](wchar_t letter) const;

protected:
    std::map<wchar_t, Duco::AlphabetLetter*> letters;
};

}

#endif //!__ALPHABETDATA_H__
