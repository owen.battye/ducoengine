#ifndef __YEARID_H__
#define __YEARID_H__

#include <set>
#include "ObjectId.h"

namespace Duco
{

class YearId
{
public:
    YearId(unsigned int newYearId, const ObjectId& newId);

    bool operator<(const YearId& rhs) const;
    void Add(const ObjectId& newId);
    size_t Count() const;

protected:
    unsigned int year;
    std::set<ObjectId> ids;
};

bool operator<(const YearId& lhs, const YearId& rhs);

}

#endif //!__YEARID_H__
