#include "CirclingData.h"

using namespace Duco;

CirclingData::CirclingData(const CirclingData& other)
    : pealCount(other.pealCount), firstPealDate(other.firstPealDate)
{

}

CirclingData::CirclingData(size_t newCount, const Duco::PerformanceDate& newPealDate)
    :pealCount(newCount), firstPealDate(newPealDate)
{

}

CirclingData::CirclingData(size_t newCount)
    : pealCount(newCount)
{
    firstPealDate.ResetToEarliest();
}

CirclingData::CirclingData()
    : pealCount(0)
{
    firstPealDate.ResetToEarliest();
}

size_t
CirclingData::PealCount() const
{
    return pealCount;
}
Duco::PerformanceDate
CirclingData::FirstPealDate() const
{
    return firstPealDate;
}

void
CirclingData::Increment(Duco::PerformanceDate newPealDate)
{
    ++pealCount;
    if (newPealDate < firstPealDate || !firstPealDate.Valid())
    {
        firstPealDate = newPealDate;
    }
}
