#ifndef __CIRCLINGDATA_H__
#define __CIRCLINGDATA_H__

#include "PerformanceDate.h"
#include "DucoEngineCommon.h"

namespace Duco
{
    class CirclingData
    {
    public:
        DllExport CirclingData();
        CirclingData(size_t newCount, const Duco::PerformanceDate& newPealDate);
        CirclingData(size_t newCount);
        CirclingData(const CirclingData& other);

        DllExport size_t PealCount() const;
        DllExport Duco::PerformanceDate FirstPealDate() const;

        DllExport void Increment(Duco::PerformanceDate newPealDate);

    private:
        size_t pealCount;
        Duco::PerformanceDate firstPealDate;
    };
}

#endif //! __CIRCLINGDATA_H__