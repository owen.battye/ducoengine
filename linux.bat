@echo off
::set COMPOSE_CONVERT_WINDOWS_PATHS=1

::docker-compose up 
set TAG=buildimage

docker build -t %TAG% .
docker run -it --volume %CD%:/project --workdir /project %TAG% /bin/bash