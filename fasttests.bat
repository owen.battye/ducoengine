@echo on
SETLOCAL

::set VS2017FILE=C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\Common7\Tools\VsDevCmd.bat
::set VS2019FILE="C:\\Program Files (x86)\\Microsoft Visual Studio\\2019\\BuildTools\\Common7\Tools\\VsDevCmd.bat"

if ["%DevEnvDir%"] == [""] (
    IF EXIST "C:\\Program Files\\Microsoft Visual Studio\\2022\\Enterprise\\Common7\\Tools\\VsDevCmd.bat" (
        call "C:\\Program Files\\Microsoft Visual Studio\\2022\\Enterprise\\Common7\\Tools\\VsDevCmd.bat"
    ) ELSE (
        IF EXIST "C:\\Program Files (x86)\\Microsoft Visual Studio\\2019\\Professional\\Common7\\Tools\\VsDevCmd.bat" (
            call "C:\\Program Files (x86)\\Microsoft Visual Studio\\2019\\Professional\\Common7\\Tools\\VsDevCmd.bat"
        ) ELSE (
            echo Cannot find Visual studio setup script.
            exit /b 1
        )
    )
)

call msbuild DucoEngine.sln /property:Configuration=Debug /property:Platform=x64 || exit /b 2

OpenCppCoverage.exe --cover_children --sources %CD%  --modules DucoEngine --continue_after_cpp_exception --excluded_modules DucoEngineTest --excluded_modules xerces --excluded_modules vstest.console.exe --excluded_modules rudeconfig --export_type html:CoverageReport -- vstest.console.exe  /TestCaseFilter:"Priority!=2" /Logger:trx %CD%\\Debug\\x64\\DucoEngineTest.dll
set TESTSPASSED=%ERRORLEVEL%

start %CD%\\CoverageReport\\index.html
