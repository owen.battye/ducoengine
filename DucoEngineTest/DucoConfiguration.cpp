#include "CppUnitTest.h"
#include <DucoConfiguration.h>
#include <ctime>
#include "ToString.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(DucoConfiguration_UnitTests)
    {
    public:
        TEST_METHOD(DucoConfiguration_DefaultConstructor)
        {
            DucoConfiguration config;
            Assert::IsFalse(config.ImportNewMethod());
            Assert::AreEqual("allmeths.xml", config.XmlMethodCollectionFile().c_str());
            Assert::AreEqual("dove.txt", config.DoveDataFile().c_str());
            Assert::IsTrue(config.CheckForNextVersion());
            Assert::IsTrue(config.IncludeWarningsInValidation());
            Assert::IsFalse(config.ExportDucoIdsWithXml());
            Assert::IsFalse(config.DisableFeaturesForPerformance());
            Assert::IsFalse(config.DisableFeaturesForPerformance(201));
            Assert::AreEqual((unsigned long long)37058560, config.DatabaseMaximumSize());
            Assert::AreEqual((unsigned long long)1048576, config.MaximumImageSize());
            Assert::AreEqual((double)450, config.ImageMaxDimension());
            Assert::AreEqual((double)72, config.ImageMaxResolution());
            Assert::AreEqual((int)24, config.ImageColourDepth());
            Assert::AreEqual(L"pk.eyJ1IjoiYmVsbHJpbmdlciIsImEiOiJjbDY0MXFreTMwdXM0M2txazR6emZ1NWFkIn0.zMVsJCK0GZGPcjb-kH0YvA", config.MapBoxApi().c_str());
            Assert::AreEqual(1.0, config.ChromeZoomLevel());

            config.ClearUISettings();
            Assert::IsTrue(config.IncludeWarningsInValidation());
            Assert::IsFalse(config.ImportNewMethod());
            Assert::IsTrue(config.CheckForNextVersion());
            Assert::IsFalse(config.DisableFeaturesForPerformance());
        }

        TEST_METHOD(DucoConfiguration_ChangeSettingSaveAndReadBack)
        {
            const std::string fileName = "test.settings";
            {
                DucoConfiguration config;
                config.SetImportNewMethod(false);
                config.SetXmlMethodCollectionFile("c:\\hghghg\\test.xml");
                config.SetDoveDataFile("g:\\ghghgh\\test.txt");
                config.DisableNextVersion();
                config.SetIncludeWarningsInValidation(false);
                config.SetExportDucoIdsWithXml(true);
                config.SetDisableFeaturesForPerformance(true);

                Assert::IsFalse(config.ImportNewMethod());
                Assert::AreEqual("c:\\hghghg\\test.xml", config.XmlMethodCollectionFile().c_str());
                Assert::AreEqual("g:\\ghghgh\\test.txt", config.DoveDataFile().c_str());
                Assert::IsFalse(config.CheckForNextVersion());
                Assert::IsFalse(config.IncludeWarningsInValidation());
                Assert::IsTrue(config.ExportDucoIdsWithXml());
                Assert::IsTrue(config.DisableFeaturesForPerformance());
                Assert::IsTrue(config.DisableFeaturesForPerformance(201));
                config.Save(fileName);
            }

            DucoConfiguration newConfig;
            newConfig.Load("", fileName);
            Assert::IsFalse(newConfig.ImportNewMethod());
            Assert::AreEqual("c:\\hghghg\\test.xml", newConfig.XmlMethodCollectionFile().c_str());
            Assert::AreEqual("g:\\ghghgh\\test.txt", newConfig.DoveDataFile().c_str());
            Assert::IsFalse(newConfig.CheckForNextVersion());
            Assert::IsFalse(newConfig.IncludeWarningsInValidation());
            Assert::IsTrue(newConfig.ExportDucoIdsWithXml());
            Assert::IsTrue(newConfig.DisableFeaturesForPerformance());
            Assert::IsTrue(newConfig.DisableFeaturesForPerformance(201));
        }

        TEST_METHOD(DucoConfiguration_ResetSettings)
        {
            DucoConfiguration config;
            config.SetMapBoxApi("GGGGGGG");
            config.SetChromeZoomLevel(3.1);

            Assert::AreEqual(L"GGGGGGG", config.MapBoxApi().c_str());
            Assert::AreEqual(3.1, config.ChromeZoomLevel());

            config.ClearUISettings();

            Assert::AreEqual(L"pk.eyJ1IjoiYmVsbHJpbmdlciIsImEiOiJjbDY0MXFreTMwdXM0M2txazR6emZ1NWFkIn0.zMVsJCK0GZGPcjb-kH0YvA", config.MapBoxApi().c_str());
            Assert::AreEqual(1.0, config.ChromeZoomLevel());
        }

        TEST_METHOD(DucoConfiguration_ReadExistingSettings)
        {
            const std::string fileName = "ducosettings.ini";
            DucoConfiguration newConfig;
            newConfig.Load("", fileName);
            Assert::IsFalse(newConfig.IncludeWarningsInValidation());
            Assert::AreEqual("C:\\repositories\\duco\\ducoengine\\DucoEngineTest\\TestData\\allmeths.xml", newConfig.XmlMethodCollectionFile().c_str());
            Assert::AreEqual("C:\\repositories\\duco\\ducoengine\\DucoEngineTest\\TestData\\dove.csv", newConfig.DoveDataFile().c_str());
            Assert::IsTrue(newConfig.CheckForNextVersion());
            Assert::IsFalse(newConfig.ExportDucoIdsWithXml());
            Assert::IsFalse(newConfig.DisableFeaturesForPerformance());
        }
    };
}
