#include "CppUnitTest.h"
#include <Composition.h>
#include <CompositionDatabase.h>
#include <Method.h>
#include <MethodDatabase.h>
#include <Ringer.h>
#include <RingerDatabase.h>
#include <ProgressCallback.h>
#include <RingingDatabase.h>
#include <Change.h>
#include "ToString.h"
#include <ImportExportProgressCallback.h>
#include <LeadEndCollection.h>
#include <DatabaseSettings.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(Composition_UnitTests), ProgressCallback, ImportExportProgressCallback
    {
    public:
        // From ProgressCallback
        virtual void Initialised()
        {
        }
        void Step(int progressPercent)
        {
        }
        void Complete(bool error)
        {
            Assert::IsFalse(error);
        }
        // From ImportExportProgressCallback
        void InitialisingImportExport(bool internalising, unsigned int versionNumber, size_t totalNumberOfObjects)
        {
        }
        void ObjectProcessed(bool internalising, Duco::TObjectType objectType, Duco::ObjectId objectId, int totalPercentage, size_t numberInThisSubDatabase)
        {
        }
        void ImportExportComplete(bool internalising)
        {
        }
        void ImportExportFailed(bool internalising, int errorCode)
        {
            Assert::AreEqual(0, errorCode);
        }
        TEST_METHOD(Composition_DefaultBehaviour)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);
            Method plainBobMinor(1, 6, L"Plain", L"Bob", L"&x16x16x16-12", false);
            plainBobMinor.SetBobPlaceNotation(L"14");
            plainBobMinor.SetSinglePlaceNotation(L"1234");
            Duco::ObjectId methodId = database.MethodsDatabase().AddObject(plainBobMinor);

            Composition compositionOne(methodId, database.MethodsDatabase());
            compositionOne.SetName(L"Test comp");
            compositionOne.SetRepeats(5);
            compositionOne.SetNotes(L"Percy penny eats bread for breakfast");
            compositionOne.SetComposerId(15);
            Assert::IsTrue(compositionOne.AddCall('H', Duco::TLeadType::EBobLead));
            Duco::ObjectId compositionId = database.CompositionsDatabase().AddObject(compositionOne);
            Assert::IsTrue(compositionOne == (*database.CompositionsDatabase().FindComposition(compositionId)));

            Assert::AreEqual(methodId, compositionOne.MethodId());
            Assert::AreEqual(L"Test comp", compositionOne.Name().c_str());
            Assert::AreEqual((unsigned int)5, compositionOne.Repeats());
            Assert::AreEqual(L"Percy penny eats bread for breakfast", compositionOne.Notes().c_str());
            Assert::AreEqual(Duco::ObjectId(15), compositionOne.ComposerId());
            Assert::IsFalse(compositionOne.ProveInProgress());

            std::string databaseName = "Test_Compositions.duc";
            database.Externalise(databaseName.c_str(), this);

            RingingDatabase newDatabase;
            unsigned int databaseVersioNumber = 0;
            newDatabase.Internalise(databaseName.c_str(), this, databaseVersioNumber);

            Assert::AreEqual((size_t)1, newDatabase.NumberOfMethods());
            Assert::AreEqual((size_t)1, newDatabase.NumberOfCompositions());
            Assert::IsTrue(newDatabase.CompositionsDatabase().FindComposition(1)->operator==(compositionOne));
        }

        TEST_METHOD(Composition_CopyOperator)
        {
            Method plainBobMinor(1, 6, L"Plain", L"Bob", L"&x16x16x16-12", false);
            Method cambridgeMinor(2, 6, L"Cambridge", L"Surprise", L"&X36X14X12X36X14X56-12", false);
            Composition compositionOne(plainBobMinor);
            compositionOne.SetName(L"PlainBob");
            Composition compositionTwo(cambridgeMinor);
            compositionTwo.SetName(L"Cambridge");

            Assert::IsFalse(compositionOne == compositionTwo);
            Assert::IsTrue(compositionOne != compositionTwo);

            compositionOne = compositionTwo;
            Assert::IsTrue(compositionOne == compositionTwo);
            Assert::IsFalse(compositionOne != compositionTwo);

            compositionTwo.SetNotes(L"Test");
            Assert::IsFalse(compositionOne == compositionTwo);
            Assert::IsTrue(compositionOne != compositionTwo);
        }

        TEST_METHOD(Composition_CheckReadyToProcessWithoutCallingNotations)
        {
            Method plainBobMinor(1, 6, L"Plain", L"Bob", L"&x16x16x16-12", false);
            Duco::DatabaseSettings settings;

            Composition compositionOne(plainBobMinor);
            Assert::IsTrue(compositionOne.Ready());
            Assert::IsFalse(compositionOne.PrepareToProve(plainBobMinor, settings));
        }

        TEST_METHOD(Composition_CheckReadyToProcessWithoutCallingNotations_EvenWhenDisabled)
        {
            Method plainBobMinor(1, 6, L"Plain", L"Bob", L"&x16x16x16-12", false);
            Duco::DatabaseSettings settings;
            settings.SetValidationCheck(EMethodWarning_MissingBobPlaceNotation, TObjectType::EMethod, true);
            settings.SetValidationCheck(EMethodWarning_MissingSinglePlaceNotation, TObjectType::EMethod, true);

            Composition compositionOne(plainBobMinor);
            Assert::IsTrue(compositionOne.Ready());
            Assert::IsFalse(compositionOne.PrepareToProve(plainBobMinor, settings));
        }

        TEST_METHOD(Composition_ValidWithoutCallings_NotWhenDisabled)
        {
            Method plainBobMinor(1, 6, L"Plain", L"Bob", L"&x16x16x16-12", false);
            Duco::DatabaseSettings settings;
            settings.SetValidationCheck(EMethodWarning_MissingBobPlaceNotation, TObjectType::EMethod, true);
            settings.SetValidationCheck(EMethodWarning_MissingSinglePlaceNotation, TObjectType::EMethod, true);

            Assert::IsTrue(plainBobMinor.ValidNotation(false, true, settings, true));
        }

        TEST_METHOD(Composition_PlainBob_PlainCourse)
        {
            Method plainBobMinor(1, 6, L"Plain", L"Bob", L"&x16x16x16-12", false);
            plainBobMinor.SetBobPlaceNotation(L"14");
            plainBobMinor.SetSinglePlaceNotation(L"1234");

            Composition compositionOne(plainBobMinor);

            Assert::IsTrue(compositionOne.AddCall('H', Duco::TLeadType::EPlainLead));
            Assert::AreEqual((size_t)60, compositionOne.NoOfChanges());
            Assert::IsTrue(compositionOne.CurrentEndChange().IsRounds());
            Assert::AreEqual(L"123456", compositionOne.EndChange(true, true).c_str());
            Assert::IsFalse(compositionOne.Proven());
        }

        TEST_METHOD(Composition_PlainBob_ThreeHomes)
        {
            Method plainBobMinor(1, 6, L"Plain", L"Bob", L"&x16x16x16-12", false);
            plainBobMinor.SetBobPlaceNotation(L"14");
            plainBobMinor.SetSinglePlaceNotation(L"1234");

            Composition compositionOne(plainBobMinor);

            Assert::IsTrue(compositionOne.AddCall('H', Duco::TLeadType::EBobLead));
            Assert::IsTrue(compositionOne.AddCall('H', Duco::TLeadType::EBobLead));
            Assert::IsTrue(compositionOne.AddCall('H', Duco::TLeadType::EBobLead));
            Assert::AreEqual((size_t)180, compositionOne.NoOfChanges());
            Assert::IsTrue(compositionOne.CurrentEndChange().IsRounds());
            Assert::IsFalse(compositionOne.Proven());
        }

        TEST_METHOD(Composition_PlainBob_ThreeHomes_SingleAdd)
        {
            Method plainBobMinor(1, 6, L"Plain", L"Bob", L"&x16x16x16-12", false);
            plainBobMinor.SetBobPlaceNotation(L"14");

            Composition compositionOne(plainBobMinor);
            Assert::IsTrue(compositionOne.AddCalls('H', L"3"));
            Assert::IsTrue(compositionOne.CurrentEndChange().IsRounds());
            Assert::IsFalse(compositionOne.Proven());
            Assert::AreEqual((size_t)180, compositionOne.NoOfChanges());
            // This test has been seen to fail and generate about 2220 changes!
        }

        TEST_METHOD(Composition_PlainBob_WHWH)
        {
            Method plainBobMinor (1, 6, L"Plain", L"Bob", L"&x16x16x16-12", false);
            plainBobMinor.SetBobPlaceNotation(L"14");
            Duco::Change roundsOnSix(6);

            Composition compositionOne(plainBobMinor);
            Assert::IsTrue(roundsOnSix.IsRounds());
            Assert::IsTrue(compositionOne.AddCall('W', TLeadType::EBobLead));
            Assert::IsTrue(compositionOne.AddCall('H', TLeadType::EBobLead));
            Assert::IsTrue(compositionOne.AddCall('W', TLeadType::EBobLead));
            Assert::IsTrue(compositionOne.AddCall('H', TLeadType::EBobLead));

            Assert::IsTrue(roundsOnSix.IsRounds());
            Assert::AreEqual(L"123456", roundsOnSix.Str(true, true).c_str());
            Assert::AreEqual(roundsOnSix, compositionOne.CurrentEndChange());
            Assert::IsTrue(compositionOne.Prove(*this, ""));
            Assert::AreEqual((size_t)10, compositionOne.LeadEnds().NoOfLeads());
        }

        TEST_METHOD(Composition_CambridgeMinor_BobCourse)
        {
            Method cambridgeMinor(1, 6, L"Cambridge", L"Surprise", L"&X36X14X12X36X14X56-12", false);
            cambridgeMinor.SetBobPlaceNotation(L"14");
            Duco::Change roundsOnSix(6);

            Composition compositionOne(cambridgeMinor);
            Assert::IsTrue(roundsOnSix.IsRounds());
            Assert::IsTrue(compositionOne.AddCall('4', TLeadType::EBobLead));
            Assert::IsTrue(compositionOne.CurrentEndChange() == Duco::Change(L"135642"));
            Assert::IsTrue(compositionOne.AddCall('W', TLeadType::EBobLead));
            Assert::IsTrue(compositionOne.CurrentEndChange() == Duco::Change(L"154263"));
            Assert::IsTrue(compositionOne.AddCall('3', TLeadType::EBobLead));
            Assert::IsTrue(compositionOne.CurrentEndChange() == Duco::Change(L"146325"));
            Assert::IsTrue(compositionOne.AddCall('2', TLeadType::EBobLead));
            Assert::IsTrue(compositionOne.CurrentEndChange() == Duco::Change(L"162534"));
            Assert::IsTrue(compositionOne.AddCall('H', TLeadType::EBobLead));
            Assert::IsTrue(compositionOne.CurrentEndChange() == roundsOnSix);

            Assert::IsTrue(roundsOnSix.IsRounds());
            Assert::AreEqual(L"123456", roundsOnSix.Str(true, true).c_str());
            Assert::AreEqual((size_t)120, compositionOne.NoOfChanges());
            Assert::AreEqual(roundsOnSix, compositionOne.CurrentEndChange());

            Assert::IsTrue(compositionOne.Prove(*this, ""));
            Assert::AreEqual((size_t)5, compositionOne.LeadEnds().NoOfLeads());
            Assert::AreEqual((size_t)120, compositionOne.NoOfChanges());
        }

        TEST_METHOD(Composition_CambridgeMinor_BobCourse_FromDatabaseRead)
        {
            std::string databaseName = "Test_Single_Composition.duc";
            Method cambridgeMinor(1, 6, L"Cambridge", L"Surprise", L"&X36X14X12X36X14X56-12", false);
            cambridgeMinor.SetBobPlaceNotation(L"14");
            cambridgeMinor.SetSinglePlaceNotation(L"1234");

            RingingDatabase database;
            database.ClearDatabase(false, true);
            Duco::ObjectId methodId = database.MethodsDatabase().AddObject(cambridgeMinor);
            Duco::Change roundsOnSix(6);

            Composition compositionOne(methodId, database.MethodsDatabase());
            Assert::IsTrue(roundsOnSix.IsRounds());
            Assert::IsTrue(compositionOne.Ready());
            Assert::IsTrue(compositionOne.AddCall('4', TLeadType::EBobLead));
            Assert::IsTrue(compositionOne.CurrentEndChange() == Duco::Change(L"135642"));
            Assert::IsTrue(compositionOne.AddCall('W', TLeadType::EBobLead));
            Assert::IsTrue(compositionOne.CurrentEndChange() == Duco::Change(L"154263"));
            Assert::IsTrue(compositionOne.AddCall('3', TLeadType::EBobLead));
            Assert::IsTrue(compositionOne.CurrentEndChange() == Duco::Change(L"146325"));
            Assert::IsTrue(compositionOne.AddCall('2', TLeadType::EBobLead));
            Assert::IsTrue(compositionOne.CurrentEndChange() == Duco::Change(L"162534"));
            Assert::IsTrue(compositionOne.AddCall('H', TLeadType::EBobLead));
            Assert::IsTrue(compositionOne.CurrentEndChange() == roundsOnSix);
            size_t numberOfExpectedChanges = 120;
            Assert::AreEqual(numberOfExpectedChanges, compositionOne.NoOfChanges());

            size_t numberOfExpectedLeadEnds = 5;
            Assert::IsTrue(compositionOne.Prove(*this, ""));
            Assert::AreEqual(numberOfExpectedLeadEnds, compositionOne.LeadEnds().NoOfLeads());
            Assert::AreEqual(numberOfExpectedChanges, compositionOne.NoOfChanges());
            Assert::AreEqual(roundsOnSix, compositionOne.CurrentEndChange());
            Duco::ObjectId compositionId = database.CompositionsDatabase().AddObject(compositionOne);

            database.Externalise(databaseName.c_str(), this);

            RingingDatabase newDatabase;
            unsigned int databaseVersioNumber = 0;
            newDatabase.Internalise(databaseName.c_str(), this, databaseVersioNumber);

            Duco::Composition readComposition = *newDatabase.CompositionsDatabase().FindComposition(compositionId);
            Assert::AreEqual(compositionOne, readComposition);

            Assert::IsFalse(readComposition.Ready());
            Assert::IsTrue(readComposition.PrepareToProve(newDatabase.MethodsDatabase(), database.Settings()));
            Assert::IsTrue(readComposition.Ready());
            Assert::IsTrue(readComposition.Prove(*this, ""));
            Assert::AreEqual(numberOfExpectedLeadEnds, readComposition.LeadEnds().NoOfLeads());
            Assert::AreEqual(numberOfExpectedChanges, compositionOne.NoOfChanges());
            Assert::AreEqual(roundsOnSix, readComposition.CurrentEndChange());
        }

        TEST_METHOD(Composition_CambridgeMinor_MakeBobTwice)
        {
            Method cambridgeMinor(1, 6, L"Cambridge", L"Surprise", L"&X36X14X12X36X14X56-12", false);
            cambridgeMinor.SetBobPlaceNotation(L"14");
            Duco::Change roundsOnSix(6);

            Composition compositionOne(cambridgeMinor);
            Assert::IsTrue(roundsOnSix.IsRounds());
            Assert::IsTrue(compositionOne.AddCall('4', TLeadType::EBobLead));
            Assert::IsTrue(compositionOne.CurrentEndChange() == Duco::Change(L"135642"));
            Assert::IsTrue(compositionOne.AddCall('4', TLeadType::EBobLead));
            compositionOne.MoveToHome();
            Assert::AreEqual(roundsOnSix, compositionOne.CurrentEndChange());
            Assert::AreEqual((size_t)192, compositionOne.NoOfChanges());

            Assert::IsTrue(compositionOne.Prove(*this, ""));
            Assert::AreEqual((size_t)8, compositionOne.LeadEnds().NoOfLeads());
            Assert::AreEqual((size_t)192, compositionOne.NoOfChanges());
        }

        TEST_METHOD(Composition_PlainBobMajor_NeverComesRound)
        {
            Method plainBobMajor(1, 8, L"Plain", L"Bob", L"&X18X18X18X18-12", false);
            plainBobMajor.SetBobPlaceNotation(L"14");
            Duco::Change roundsOnEight(8);

            Composition compositionOne(plainBobMajor);
            Assert::IsTrue(roundsOnEight.IsRounds());
            Assert::IsTrue(compositionOne.AddCall('W', TLeadType::EBobLead));
            Assert::AreEqual(Duco::Change(L"12357486"), compositionOne.CurrentEndChange());
            Assert::AreEqual((size_t)16, compositionOne.NoOfChanges());
            compositionOne.MoveToHome();
            Assert::AreEqual((size_t)112, compositionOne.NoOfChanges());
            Assert::AreEqual(Duco::Change(L"15243678"), compositionOne.CurrentEndChange());
            Assert::IsFalse(compositionOne.Prove(*this, ""));
            Assert::AreEqual((size_t)8, compositionOne.LeadEnds().NoOfLeads());
            Assert::AreEqual((size_t)128, compositionOne.NoOfChanges());
            Assert::AreEqual(Duco::Change(L"12357486"), compositionOne.CurrentEndChange());
        }

        TEST_METHOD(Composition_PlainBobMajor_TwoSinglesHome)
        {
            Method plainBobMajor(1, 8, L"Plain", L"Bob", L"&X18X18X18X18-12", false);
            plainBobMajor.SetSinglePlaceNotation(L"1234");
            plainBobMajor.SetBobPlaceNotation(L"14");
            Duco::Change roundsOnEight(8);

            Composition compositionOne(plainBobMajor);
            Assert::IsTrue(roundsOnEight.IsRounds());
            Assert::IsTrue(compositionOne.AddCall('H', TLeadType::ESingleLead));
            Assert::AreEqual(Duco::Change(L"12435678"), compositionOne.CurrentEndChange());
            Assert::AreEqual((size_t)112, compositionOne.NoOfChanges());
            Assert::IsTrue(compositionOne.AddCall('H', TLeadType::ESingleLead));
            Assert::AreEqual((size_t)224, compositionOne.NoOfChanges());
            Assert::AreEqual(roundsOnEight, compositionOne.CurrentEndChange());

            Assert::IsTrue(compositionOne.Prove(*this, ""));
            Assert::AreEqual((size_t)14, compositionOne.LeadEnds().NoOfLeads());
            Assert::AreEqual((size_t)224, compositionOne.NoOfChanges());
        }

        TEST_METHOD(Composition_PlainBobMajor_DiaryTouch)
        {
            Method plainBobMajor(1, 8, L"Plain", L"Bob", L"&X18X18X18X18-12", false);
            plainBobMajor.SetBobPlaceNotation(L"14");
            Duco::Change roundsOnEight(8);

            Composition compositionOne(plainBobMajor);
            Assert::IsTrue(roundsOnEight.IsRounds());
            Assert::IsTrue(compositionOne.AddCall('W', TLeadType::EBobLead));
            Assert::IsTrue(compositionOne.AddCall('B', TLeadType::EBobLead));
            Assert::IsTrue(compositionOne.AddCall('B', TLeadType::EBobLead));
            Assert::IsTrue(compositionOne.AddCall('B', TLeadType::EBobLead));
            Assert::IsTrue(compositionOne.AddCall('M', TLeadType::EBobLead));
            Assert::IsTrue(compositionOne.AddCall('H', TLeadType::EBobLead));
            Assert::AreEqual(roundsOnEight, compositionOne.CurrentEndChange());
            Assert::AreEqual((size_t)160, compositionOne.NoOfChanges());

            Assert::IsTrue(compositionOne.Prove(*this, ""));
            Assert::AreEqual((size_t)10, compositionOne.LeadEnds().NoOfLeads());
            Assert::AreEqual((size_t)160, compositionOne.NoOfChanges());
        }

        TEST_METHOD(Composition_CambridgeMajor_MakeBobTwice)
        {
            Method cambridgeMajor(1, 8, L"Cambridge", L"Surprise", L"&X38X14X1258X36X14X58X16X78-12", false);
            cambridgeMajor.SetBobPlaceNotation(L"14");
            cambridgeMajor.SetSinglePlaceNotation(L"1234");
            Duco::Change roundsOnEight(8);

            Composition compositionOne(cambridgeMajor);
            Assert::IsTrue(roundsOnEight.IsRounds());
            Assert::IsTrue(compositionOne.AddCall('5', TLeadType::ESingleLead));
            Assert::AreEqual(Duco::Change(L"15378264"), compositionOne.CurrentEndChange());
            Assert::IsTrue(compositionOne.AddCall('2', TLeadType::ESingleLead));
            Assert::AreEqual(Duco::Change(L"18364527"), compositionOne.CurrentEndChange());
            Assert::IsTrue(compositionOne.AddCall('7', TLeadType::ESingleLead)); // or wrong!
            Assert::AreEqual(Duco::Change(L"17256483"), compositionOne.CurrentEndChange());
            Assert::IsTrue(compositionOne.AddCall('4', TLeadType::ESingleLead));
            Assert::AreEqual(Duco::Change(L"16283745"), compositionOne.CurrentEndChange());
            Assert::IsTrue(compositionOne.AddCall('H', TLeadType::EBobLead));
            Assert::IsTrue(compositionOne.CurrentEndChange() == roundsOnEight);
            Assert::AreEqual((size_t)192, compositionOne.NoOfChanges());

            Assert::IsTrue(compositionOne.Prove(*this, ""));
            Assert::AreEqual((size_t)6, compositionOne.LeadEnds().NoOfLeads());
            Assert::AreEqual((size_t)192, compositionOne.NoOfChanges());
        }

        TEST_METHOD(Composition_PlainBobMajor_2SinglesHome)
        {
            Method plainBobRoyal(1, 8, L"Plain", L"Bob", L"&X18X18X18X18-12", false);
            plainBobRoyal.SetSinglePlaceNotation(L"1234");
            Duco::Change roundsOnEight(8);

            Composition compositionOne(plainBobRoyal);
            Assert::IsTrue(roundsOnEight.IsRounds());
            Assert::IsTrue(compositionOne.AddCalls('H', L"2s"));
            Assert::AreEqual(roundsOnEight, compositionOne.CurrentEndChange());
            Assert::AreEqual((size_t)224, compositionOne.NoOfChanges());

            Assert::IsTrue(compositionOne.Prove(*this, ""));
            Assert::AreEqual((size_t)14, compositionOne.LeadEnds().NoOfLeads());
            Assert::AreEqual((size_t)224, compositionOne.NoOfChanges());
            Assert::AreEqual(roundsOnEight, compositionOne.CurrentEndChange());
        }

        TEST_METHOD(Composition_PlainBobMajor_QuarterPeal)
        {
            Method plainBobRoyal(1, 8, L"Plain", L"Bob", L"&X18X18X18X18-12", false);
            plainBobRoyal.SetBobPlaceNotation(L"14");
            Duco::Change roundsOnEight(8);

            Composition compositionOne(plainBobRoyal);
            Assert::IsTrue(roundsOnEight.IsRounds());
            Assert::IsTrue(compositionOne.AddCall('W', EBobLead));
            Assert::AreEqual((size_t)16, compositionOne.NoOfChanges());
            Assert::IsTrue(compositionOne.AddCall('w', EBobLead));
            Assert::AreEqual((size_t)128, compositionOne.NoOfChanges());
            Assert::IsTrue(compositionOne.AddCall('B', EBobLead));
            Assert::IsTrue(compositionOne.AddCall('B', EBobLead));
            Assert::IsTrue(compositionOne.AddCall('4', EBobLead));
            Assert::IsTrue(compositionOne.AddCall('H', EBobLead));
            Assert::IsTrue(compositionOne.AddCall('H', EBobLead));
            Assert::IsTrue(compositionOne.AddCall('H', EBobLead));
            Assert::AreEqual(Duco::Change(L"13572468"), compositionOne.CurrentEndChange());
            Assert::IsTrue(compositionOne.AddCall('I', EBobLead));
            Assert::IsTrue(compositionOne.AddCall('M', EBobLead));
            Assert::IsTrue(compositionOne.AddCall('H', EBobLead));
            Assert::IsTrue(compositionOne.AddCall('H', EBobLead));
            Assert::IsTrue(compositionOne.AddCall('H', EBobLead));
            Assert::AreEqual(Duco::Change(L"3246578"), compositionOne.CurrentEndChange());
            Assert::IsTrue(compositionOne.AddCall('b', EBobLead));
            Assert::IsTrue(compositionOne.AddCall('b', EBobLead));
            Assert::IsTrue(compositionOne.AddCall('m', EBobLead));
            Assert::IsTrue(compositionOne.AddCall('w', EBobLead));
            Assert::IsTrue(compositionOne.AddCall('h', EBobLead));
            Assert::IsTrue(compositionOne.AddCall('h', EBobLead));
            Assert::AreEqual(roundsOnEight, compositionOne.CurrentEndChange());
            Assert::AreEqual((size_t)1280, compositionOne.NoOfChanges());

            Assert::IsTrue(compositionOne.Prove(*this, ""));
            Assert::AreEqual((size_t)1280, compositionOne.NoOfChanges());
            Assert::AreEqual(roundsOnEight, compositionOne.CurrentEndChange());
        }

        TEST_METHOD(Composition_PlainBobMajor_QuarterPeal_ByStrings)
        {
            Duco::DatabaseSettings settings;
            Method plainBobMajor(1, 8, L"Plain", L"Bob", L"&X18X18X18X18-12", false);
            plainBobMajor.SetBobPlaceNotation(L"14");
            plainBobMajor.SetSinglePlaceNotation(L"1234");
            Duco::Change roundsOnEight(8);

            Composition compositionOne(plainBobMajor);
            Assert::IsTrue(roundsOnEight.IsRounds());
            Assert::IsTrue(compositionOne.Ready());
            Assert::IsTrue(compositionOne.PrepareToProve(plainBobMajor, settings));
            Assert::IsTrue(compositionOne.AddCalls('W', L"2"));
            Assert::IsTrue(compositionOne.AddCalls('B', L"2"));
            Assert::IsTrue(compositionOne.AddCalls('4', L"-"));
            Assert::IsTrue(compositionOne.AddCalls('H', L"3"));
            Assert::AreEqual(Duco::Change(L"13572468"), compositionOne.CurrentEndChange());
            Assert::IsTrue(compositionOne.AddCalls('I', L"-"));
            Assert::IsTrue(compositionOne.AddCalls('M', L"-"));
            Assert::IsTrue(compositionOne.AddCalls('H', L"3"));
            Assert::AreEqual(Duco::Change(L"13246578"), compositionOne.CurrentEndChange());
            Assert::IsTrue(compositionOne.AddCalls('b', L"2"));
            Assert::IsTrue(compositionOne.AddCalls('m', L"-"));
            Assert::IsTrue(compositionOne.AddCalls('W', L"-"));
            Assert::IsTrue(compositionOne.AddCalls('h', L"2"));
            Assert::AreEqual(roundsOnEight, compositionOne.CurrentEndChange());
            Assert::AreEqual((size_t)1280, compositionOne.NoOfChanges());

            Assert::IsTrue(compositionOne.Prove(*this, ""));
            Assert::AreEqual((size_t)80, compositionOne.LeadEnds().NoOfLeads());
            Assert::AreEqual((size_t)1280, compositionOne.NoOfChanges());
            Assert::AreEqual(roundsOnEight, compositionOne.CurrentEndChange());
        }

        TEST_METHOD(Composition_CambridgeRoyal_RWPipe_TestForPaulMason)
        {
            Duco::DatabaseSettings settings;
            Method cambridgeRoyal(1, 10, L"Cambridge", L"Surprise", L"&x30x14x1250x36x1470x58x16x70x18x90-12", false);
            cambridgeRoyal.SetBobPlaceNotation(L"14");
            cambridgeRoyal.SetSinglePlaceNotation(L"1234");

            Duco::Change roundsOnTen(10);
            Composition compositionOne(cambridgeRoyal);
            Assert::IsTrue(roundsOnTen.IsRounds());
            Assert::IsTrue(compositionOne.Ready());
            Assert::IsTrue(compositionOne.PrepareToProve(cambridgeRoyal, settings));
            Assert::IsTrue(compositionOne.AddCall('H', EBobLead));
            Assert::IsTrue(compositionOne.CurrentEndChange() == Duco::Change(L"42356", 10));
            Assert::IsTrue(compositionOne.AddCalls('W', L"ss"));
            Assert::IsTrue(compositionOne.AddCall('H', EBobLead));
            Assert::AreEqual(Duco::Change(L"34256", 10), compositionOne.CurrentEndChange());
            Assert::IsTrue(compositionOne.AddCall('W', ESingleLead));
            Assert::IsTrue(compositionOne.AddCall('M', ESingleLead));
            Assert::IsTrue(compositionOne.AddCall('W', ESingleLead));
            Assert::IsTrue(compositionOne.AddCalls('H', L"3"));
            Assert::AreEqual(Duco::Change(L"34265", 10), compositionOne.CurrentEndChange());
            Assert::IsTrue(compositionOne.AddCall('W', ESingleLead));
            Assert::IsTrue(compositionOne.AddCall('H', ESingleLead));
            Assert::AreEqual(Duco::Change(L"62435", 10), compositionOne.CurrentEndChange());
            Assert::IsTrue(compositionOne.AddCalls('M', L"-"));
            Assert::IsTrue(compositionOne.AddCalls('W', L"3"));
            Assert::IsTrue(compositionOne.AddCalls('M', L"2"));
            Assert::IsTrue(compositionOne.AddCall('H', ESingleLead));
            Assert::AreEqual(Duco::Change(L"64235", 10), compositionOne.CurrentEndChange());
            Assert::IsTrue(compositionOne.AddCalls('M', L"s"));
            Assert::IsTrue(compositionOne.AddCalls('W', L"s"));
            Assert::IsTrue(compositionOne.AddCalls('H', L"-"));
            Assert::IsTrue(compositionOne.CurrentEndChange() == Duco::Change(L"23456", 10));

            CompositionTable table = compositionOne.CreateCompositionTable();
            Assert::AreEqual((size_t)8, table.NoOfCourses());

            Assert::AreEqual(Change(L"42356", 10), table.Course(0).CourseEnd());
            Assert::AreEqual(Change(L"34256", 10), table.Course(1).CourseEnd());
            Assert::AreEqual(Change(L"54236", 10), table.Course(2).CourseEnd());
            Assert::AreEqual(Change(L"34265", 10), table.Course(3).CourseEnd());
            Assert::AreEqual(Change(L"62435", 10), table.Course(4).CourseEnd());
            Assert::AreEqual(Change(L"42536", 10), table.Course(5).CourseEnd());
            Assert::AreEqual(Change(L"64235", 10), table.Course(6).CourseEnd());
            Assert::AreEqual(Change(L"23456", 10), table.Course(7).CourseEnd());
            // This test has been seen to fail at check for 34265
        }

        TEST_METHOD(Composition_SetComposerByName)
        {
            Method cambridgeRoyal(1, 10, L"Cambridge", L"Surprise", L"&x30x14x1250x36x1470x58x16x70x18x90-12", false);
            cambridgeRoyal.SetBobPlaceNotation(L"14");
            cambridgeRoyal.SetSinglePlaceNotation(L"1234");

            RingingDatabase database;
            database.ClearDatabase(false, true);

            Ringer newRinger(1, L"Someone", L"New");
            database.RingersDatabase().AddObject(newRinger);

            Duco::Change roundsOnTen(10);
            Composition compositionOne(cambridgeRoyal);

            compositionOne.SetComposerId(1);
            Assert::IsTrue(compositionOne.ComposerId().ValidId());
            Assert::AreEqual(L"Composed by Someone New", compositionOne.Composer(database.RingersDatabase()).c_str());
            compositionOne.SetName(L"No. 1");
            Assert::AreEqual(L"Composed by Someone New (No. 1)", compositionOne.Composer(database.RingersDatabase()).c_str());

            compositionOne.SetComposerName(L"Owen Battye");
            Assert::IsFalse(compositionOne.ComposerId().ValidId());
            Assert::AreEqual(L"Composed by Owen Battye (No. 1)", compositionOne.Composer(database.RingersDatabase()).c_str());
        }
        TEST_METHOD(Composition_CambridgeRoyal_FirstOnPage)
        {
            Method cambridgeRoyal(1, 10, L"Cambridge", L"Surprise", L"&x30x14x1250x36x1470x58x16x70x18x90-12", false);
            cambridgeRoyal.SetBobPlaceNotation(L"14");
            cambridgeRoyal.SetSinglePlaceNotation(L"1234");

            Duco::Change roundsOnTen(10);
            Composition compositionOne(cambridgeRoyal);
            compositionOne.SetNotes(L"https://www.ringing.org/peals/royal/single-surprise/cambridge/");
            compositionOne.SetComposerName(L"Richard J Angrave");
            compositionOne.SetName(L"No. 1");

            Assert::IsTrue(compositionOne.AddCall('H', EBobLead));
            Assert::AreEqual(Duco::Change(L"42356", 10), compositionOne.CurrentEndChange());
            Assert::IsTrue(compositionOne.AddCall('W', EBobLead));
            compositionOne.MoveToHome();
            Assert::AreEqual(Duco::Change(L"54326", 10), compositionOne.CurrentEndChange());
            Assert::IsTrue(compositionOne.AddCall('I', EBobLead));
            Assert::IsTrue(compositionOne.AddCall('5', EBobLead));
            compositionOne.MoveToHome();
            Assert::AreEqual(Duco::Change(L"42563", 10), compositionOne.CurrentEndChange());
            Assert::IsTrue(compositionOne.AddCall('W', EBobLead));
            Assert::IsTrue(compositionOne.AddCall('H', EBobLead));
            Assert::AreEqual(Duco::Change(L"56423", 10), compositionOne.CurrentEndChange());
            Assert::IsTrue(compositionOne.AddCall('M', ESingleLead));
            Assert::IsTrue(compositionOne.AddCall('W', EBobLead));
            compositionOne.MoveToHome();
            Assert::AreEqual(Duco::Change(L"23465", 10), compositionOne.CurrentEndChange());
            Assert::IsTrue(compositionOne.AddCall('M', EBobLead));
            Assert::IsTrue(compositionOne.AddCall('W', ESingleLead));
            Assert::IsTrue(compositionOne.AddCalls('H', L"2"));
            Assert::AreEqual(Duco::Change(L"35642", 10), compositionOne.CurrentEndChange());
            Assert::IsTrue(compositionOne.AddCall('W', EBobLead));
            compositionOne.MoveToHome();
            Assert::AreEqual(Duco::Change(L"43652", 10), compositionOne.CurrentEndChange());
            Assert::IsTrue(compositionOne.AddCall('I', EBobLead));
            Assert::IsTrue(compositionOne.AddCall('5', EBobLead));
            compositionOne.MoveToHome();
            Assert::AreEqual(Duco::Change(L"35426", 10), compositionOne.CurrentEndChange());
            Assert::IsTrue(compositionOne.AddCall('M', EBobLead));
            Assert::IsTrue(compositionOne.AddCall('H', EBobLead));
            compositionOne.MoveToHome(); // Not required, but here for testing
            Assert::AreEqual(Duco::Change(L"64523", 10), compositionOne.CurrentEndChange());
            Assert::IsTrue(compositionOne.AddCall('M', EBobLead));
            Assert::IsTrue(compositionOne.AddCalls('W', L"2"));
            Assert::IsTrue(compositionOne.AddCalls('H', L"2"));
            Assert::AreEqual(roundsOnTen, compositionOne.CurrentEndChange());

            Assert::IsTrue(compositionOne.Prove(*this, ""));
            Assert::AreEqual((size_t)127, compositionOne.LeadEnds().NoOfLeads());
            Assert::AreEqual((size_t)5080, compositionOne.NoOfChanges());
            Assert::AreEqual(roundsOnTen, compositionOne.CurrentEndChange());
            // THis test has been seen to fail at check for 35642
        }

        BEGIN_TEST_METHOD_ATTRIBUTE(Composition_BristolSRoyal)
            TEST_PRIORITY(2)
        END_TEST_METHOD_ATTRIBUTE()
        TEST_METHOD(Composition_BristolSRoyal)
        {
            Method bristolRoyal(1, 10, L"Bristol", L"Surprise", L"&x50x14.50x50.36.14x70.58.16x16.70x16x10-10", false);
            bristolRoyal.SetBobPlaceNotation(L"18");
            bristolRoyal.SetSinglePlaceNotation(L"1890");

            Duco::Change roundsOnTen(10);
            Composition compositionOne(bristolRoyal);
            compositionOne.SetNotes(L"https://www.ringing.org/peals/royal/single-surprise/bristol/");
            Assert::IsTrue(roundsOnTen.IsRounds());

            Assert::IsTrue(compositionOne.AddCalls('O', L"s--s--"));
            Assert::IsTrue(compositionOne.AddCalls('I', L"s"));
            compositionOne.MoveToHome();
            Assert::AreEqual(Duco::Change(L"53426", 10), compositionOne.CurrentEndChange());
            Assert::IsTrue(compositionOne.AddCall('V', ESingleLead));
            Assert::IsTrue(compositionOne.AddCall('I', ESingleLead));
            compositionOne.MoveToHome();
            Assert::AreEqual(Duco::Change(L"23465", 10), compositionOne.CurrentEndChange());
            compositionOne.SetRepeats(1);

            Assert::IsTrue(compositionOne.Prove(*this, ""));
            Assert::AreEqual((size_t)126, compositionOne.LeadEnds().NoOfLeads());
            Assert::AreEqual((size_t)5040, compositionOne.NoOfChanges());
            Assert::AreEqual(roundsOnTen, compositionOne.CurrentEndChange());
        }
    };
}
