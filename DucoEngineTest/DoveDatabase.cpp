﻿#include "CppUnitTest.h"
#include <Tower.h>
#include <Ring.h>
#include <RingingDatabase.h>
#include <TowerDatabase.h>
#include <DoveDatabase.h>
#include <DoveObject.h>
#include "ToString.h"
#include <algorithm>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(DoveDatabase_UnitTests)
    {
        Duco::DoveDatabase* doveDatabase;
        RingingDatabase* database;

    public:
        DoveDatabase_UnitTests()
        {
            database = new RingingDatabase();
            database->ClearDatabase(false, true);
            doveDatabase = new DoveDatabase("dove.csv", NULL, "", true);
            doveDatabase->Import();
        }

        ~DoveDatabase_UnitTests()
        {
            delete doveDatabase;
            delete database;
        }

        TEST_METHOD(Dove_DefaultBehaviour)
        {
            Assert::IsTrue(doveDatabase->ConfigValid());
            Assert::AreEqual((size_t)7143, doveDatabase->NoOfTowers());
            Assert::AreEqual((size_t)12, doveDatabase->NumberOfSaintAbbreviations());
        }

        TEST_METHOD(Dove_FindByTower_BristolStJames)
        {
            Tower bristolStJames(1, L"Priory Church of St James", L"Bristol", L"Avon", 10);
            bristolStJames.AddRing(10, L"27¼", L"D");
            Duco::DoveObject foundTower;
            Duco::ObjectId existingTower;

            Assert::IsTrue(doveDatabase->SuggestTower(database->TowersDatabase(), bristolStJames, foundTower, existingTower));
            Assert::IsFalse(existingTower.ValidId());
            Assert::AreEqual(L"14342", foundTower.DoveId().c_str());
            Assert::AreEqual(51.45867, foundTower.Latitude(), 0.00001);
            Assert::AreEqual(-2.59279, foundTower.Longitude(), 0.00001);
        }

        TEST_METHOD(Dove_FindByFields_MoultonNorthants)
        {
            const Duco::DoveObject* foundTower = doveDatabase->SuggestTower(12, L"St Peter", L"Moulton", L"Northamptonshire", L"11", L"");

            Assert::IsNotNull(foundTower);
            Assert::AreEqual(L"13208", foundTower->DoveId().c_str());
            Assert::AreEqual(52.29034, foundTower->Latitude(), 0.00001);
            Assert::AreEqual(-0.85277, foundTower->Longitude(), 0.00001);
        }

        TEST_METHOD(Dove_FindByTower_MoultonNorthants_LessBells)
        {
            Tower towerToFind(1, L"St Peter", L"Moulton", L"Northants", 10);
            towerToFind.AddRing(10, L"11-1-14", L"A♭");
            Duco::DoveObject foundTower;
            Duco::ObjectId existingTower;
            Assert::IsTrue(doveDatabase->SuggestTower(database->TowersDatabase(), towerToFind, foundTower, existingTower));
            Assert::IsFalse(existingTower.ValidId());
            Assert::AreEqual(L"13208", foundTower.DoveId().c_str());
            Assert::AreEqual(52.29034, foundTower.Latitude(), 0.00001);
            Assert::AreEqual(-0.85277, foundTower.Longitude(), 0.00001);
            Assert::IsTrue(Tower(foundTower).InEurope());
            Assert::IsFalse(foundTower.AntiClockwise());
        }

        TEST_METHOD(Dove_FindByFields_Harpole)
        {
            const Duco::DoveObject* foundTower = doveDatabase->SuggestTower(6, L"All Saints", L"Harpole", L"Northamptonshire", L"12", L"");
            Assert::IsNotNull(foundTower);
            Assert::AreEqual(L"10385", foundTower->DoveId().c_str());
            Assert::AreEqual(L"HARPOLE", foundTower->OldDoveId().c_str());
            Assert::AreEqual(52.24254, foundTower->Latitude(), 0.00001);
            Assert::AreEqual(-0.99011, foundTower->Longitude(), 0.00001);
            Assert::AreEqual(L"2296", foundTower->TowerbaseId().c_str());
        }

        TEST_METHOD(Dove_FindByTower_MoultonNorthants_LessBellsApproxTenor)
        {
            Tower towerToFind(1, L"St Peter", L"Moulton", L"Northamptonshire", 10);
            towerToFind.AddRing(10, L"11", L"Ab");
            Duco::DoveObject foundTower;
            Duco::ObjectId existingTower;
            Assert::IsTrue(doveDatabase->SuggestTower(database->TowersDatabase(), towerToFind, foundTower, existingTower));
            Assert::IsFalse(existingTower.ValidId());
            Assert::AreEqual(L"13208", foundTower.DoveId().c_str());
            Assert::AreEqual(52.29034, foundTower.Latitude(), 0.00001);
            Assert::AreEqual(-0.85277, foundTower.Longitude(), 0.00001);
            Assert::IsTrue(Tower(foundTower).InEurope());
            Assert::IsFalse(foundTower.AntiClockwise());
        }

        TEST_METHOD(Dove_FindByFields_Middleton)
        {
            const Duco::DoveObject* foundTower = doveDatabase->SuggestTower(8, L"St Leonard", L"MIDDLETON", L"GREATER MANCHESTER", L"11", L"");
            Assert::IsNotNull(foundTower);
            Assert::AreEqual(L"16137", foundTower->DoveId().c_str());
            Assert::AreEqual(53.55322, foundTower->Latitude(), 0.00001);
            Assert::AreEqual(-2.19480, foundTower->Longitude(), 0.00001);
        }

        TEST_METHOD(Dove_FindByTower_Mossley1)
        {
            Tower towerToFind(1, L"St Barnabas, Mossley", L"LIVERPOOL", L"Merseyside", 8);
            towerToFind.AddRing(8, L"11", L"G");
            Duco::DoveObject foundTower;
            Duco::ObjectId existingTower;
            Assert::IsTrue(doveDatabase->SuggestTower(database->TowersDatabase(), towerToFind, foundTower, existingTower));
            Assert::IsFalse(existingTower.ValidId());
            Assert::AreEqual(L"11800", foundTower.DoveId().c_str());
            Assert::IsTrue(Tower(foundTower).InEurope());
        }

        TEST_METHOD(Dove_FindByTower_Mossley2)
        {
            Tower towerToFind(1, L"St Barnabas, Penny Lane, Mossley Hill", L"LIVERPOOL", L"Merseyside", 8);
            towerToFind.AddRing(8, L"11", L"G");
            Duco::DoveObject foundTower;
            Duco::ObjectId existingTower;
            Assert::IsTrue(doveDatabase->SuggestTower(database->TowersDatabase(), towerToFind, foundTower, existingTower));
            Assert::IsFalse(existingTower.ValidId());
            Assert::AreEqual(L"11800", foundTower.DoveId().c_str());
        }

        TEST_METHOD(Dove_FindByTower_LiverpoolCathedral)
        {
            Tower towerToFind(1, L"Cathedral Church of Christ", L"LIVERPOOL", L"Merseyside", 12);
            towerToFind.AddRing(12, L"82-0-11", L"Ab");
            Duco::DoveObject foundTower;
            Duco::ObjectId existingTower;
            Assert::IsTrue(doveDatabase->SuggestTower(database->TowersDatabase(), towerToFind, foundTower, existingTower));
            Assert::IsFalse(existingTower.ValidId());
            Assert::AreEqual(L"13402", foundTower.DoveId().c_str());
            Assert::AreEqual(L"3049", foundTower.TowerbaseId().c_str());
            Assert::IsTrue(Tower(foundTower).InEurope());
        }

        TEST_METHOD(Dove_FindByFields_Chilcompton)
        {
            const Duco::DoveObject* foundTower = doveDatabase->SuggestTower(10, L"St John", L"Chilcompton", L"Somerset", L"19", L"");
            Assert::IsNotNull(foundTower);
            Assert::AreEqual(L"12701", foundTower->DoveId().c_str());
            Assert::IsFalse(foundTower->AntiClockwise());
        }

        TEST_METHOD(Dove_FindByDoveId_FirstTowerInDoveFields)
        {
            Duco::DoveObject theTower;
            Assert::IsTrue(doveDatabase->FindTower(L"AB KETTLEB", 0, theTower));

            Assert::AreEqual(L"12574", theTower.DoveId().c_str());
            Assert::AreEqual(L"Leicestershire", theTower.County().c_str());
            Assert::AreEqual(L"England", theTower.Country().c_str());
            Assert::IsTrue(theTower.UKorIreland());
            Assert::AreEqual(L"Ab Kettleby", theTower.City().c_str());
            Assert::AreEqual(L"", theTower.Name().c_str());
            Assert::AreEqual(L"St James", theTower.Dedication().c_str());
            Assert::AreEqual((unsigned int)6, theTower.Bells());
            Assert::AreEqual(L"8-2-22", theTower.Tenor().c_str());
            Assert::IsFalse(theTower.TenorApprox());
            Assert::AreEqual(L"G", theTower.TenorKey().c_str());
            Assert::IsFalse(theTower.Unringable());
            Assert::AreEqual(L"", theTower.AltName().c_str());
            Assert::AreEqual(52.79858, theTower.Latitude(), 0.00001);
            Assert::AreEqual(-0.92747, theTower.Longitude(), 0.00001);
            Assert::IsTrue(theTower.GroundFloor());
            Assert::AreEqual(L"http://www.abkettlebybells.co.uk/", theTower.WebPage().c_str());
        }

        TEST_METHOD(Dove_FindByDoveId_LewesSouthover)
        {
            Duco::DoveObject theTower;
            Assert::IsTrue(doveDatabase->FindTower(L"LEWES  SOU", 0, theTower));

            Assert::AreEqual(L"15805", theTower.DoveId().c_str());
            Assert::AreEqual(L"St John the Baptist", theTower.Dedication().c_str());
            Assert::AreEqual(L"Southover", theTower.Name().c_str());
            Assert::AreEqual(L"Lewes", theTower.City().c_str());
            Assert::AreEqual(L"East Sussex", theTower.County().c_str());
            Assert::AreEqual(L"England", theTower.Country().c_str());
            Assert::IsTrue(theTower.UKorIreland());
            Assert::AreEqual((unsigned int)10, theTower.Bells());
            Assert::AreEqual(L"17-1-20", theTower.Tenor().c_str());
            Assert::IsFalse(theTower.TenorApprox());
            Assert::AreEqual(L"E", theTower.TenorKey().c_str());
            Assert::IsFalse(theTower.Unringable());
            Assert::AreEqual(L"Southover", theTower.AltName().c_str());
            Assert::AreEqual(50.86909, theTower.Latitude(), 0.00001);
            Assert::AreEqual(0.00597, theTower.Longitude(), 0.00001);
            Assert::IsFalse(theTower.GroundFloor());
            Assert::AreEqual(L"", theTower.WebPage().c_str());
        }

        TEST_METHOD(Dove_FindDoreFromYACR_UsesAltNames)
        {
            Duco::Tower normalTower(KNoId, L"Christ Church", L"DORE", L"South Yorkshire", 8);
            normalTower.AddRing(8, L"0", L"G");

            Duco::DoveObject foundTower;
            Duco::ObjectId existingTower;
            Assert::IsTrue(doveDatabase->SuggestTower(database->TowersDatabase(), normalTower, foundTower, existingTower));
            Assert::IsFalse(existingTower.ValidId());
            Assert::AreEqual(L"15539", foundTower.DoveId().c_str());
        }

        TEST_METHOD(Dove_FindPortseaStMary)
        {
            const Duco::DoveObject* foundTower = doveDatabase->SuggestTower(8, L"St Mary's", L"Portsea", L"Hampshire", L"17cwt", L"");
            Assert::IsNotNull(foundTower);
            Assert::AreEqual(L"16279", foundTower->DoveId().c_str());
        }

        TEST_METHOD(Dove_FindAdelaideCathedralSouthAustralia)
        {
            const Duco::DoveObject* foundTower = doveDatabase->SuggestTower(8, L"Cath Ch of S Peter", L"Adelaide", L"Australia", L"41-1-0", L"");
            Assert::IsNotNull(foundTower);
            Assert::AreEqual(L"16198", foundTower->DoveId().c_str());

            Duco::Tower importedTower(*foundTower);
            Assert::AreEqual((unsigned int)8, importedTower.Bells());
            Assert::AreEqual(L"South Australia, Australia", importedTower.County().c_str());
            Assert::AreEqual(L"Cathedral church of St Peter", importedTower.Name().c_str());
            Assert::IsFalse(importedTower.InEurope());
            Assert::AreEqual(L"5916", importedTower.TowerbaseId().c_str());

        }

        TEST_METHOD(Dove_FindAdelaideRCCathedralSouthAustralia)
        {
            const Duco::DoveObject* foundTower = doveDatabase->SuggestTower(12, L"RC Cath Ch of S Francis Xavier", L"Adelaide", L"SA", L"28-2-1", L"Db");
            Assert::IsNotNull(foundTower);
            Assert::AreEqual(L"15785", foundTower->DoveId().c_str());

            Duco::Tower importedTower(*foundTower);
            Assert::AreEqual((unsigned int)12, importedTower.BellsForDove());
            Assert::AreEqual((unsigned int)13, importedTower.Bells());
            Assert::AreEqual(L"South Australia, Australia", importedTower.County().c_str());
            Assert::AreEqual(L"Cathedral church of St Francis Xavier", importedTower.Name().c_str());
            Assert::IsFalse(importedTower.InEurope());
        }

        TEST_METHOD(Dove_ImportPosition_Warburton_FromOldDoveId)
        {
            std::wstring tenorWeight = L"25-2-6";
            Tower newTower(KNoId, L"St Werburgh", L"Warburton", L"Greater Manchester", 8);
            Duco::ObjectId correctRingId = newTower.AddRing(8, tenorWeight, L"E♭");
            newTower.SetDoveRef(L"WARBURTON");
            Assert::AreEqual(L"WARBURTON", newTower.DoveRef().c_str());
            Assert::AreEqual(L"", newTower.Latitude().c_str());
            Assert::AreEqual(L"", newTower.Longitude().c_str());

            Duco::ObjectId towerId = database->TowersDatabase().AddObject(newTower);
            Assert::AreEqual(Duco::ObjectId(1), towerId);
            std::set<Duco::ObjectId> ducoTowerIds;
            ducoTowerIds.insert(towerId);
            Assert::AreEqual((size_t)1, doveDatabase->ImportPositionAndIds(database->TowersDatabase(), ducoTowerIds));

            const Duco::Tower* updatedTower = database->TowersDatabase().FindTower(towerId);
            Assert::AreEqual(L"53.39842", updatedTower->Latitude().c_str());
            Assert::AreEqual(L"-2.44531", updatedTower->Longitude().c_str());
            Assert::AreEqual(L"14523", updatedTower->DoveRef().c_str());
        }

        TEST_METHOD(Dove_ImportPosition_Warburton_FromNewDoveId)
        {
            std::wstring tenorWeight = L"25-2-6";
            Tower newTower(KNoId, L"St Werburgh", L"Warburton", L"Greater Manchester", 8);
            Duco::ObjectId correctRingId = newTower.AddRing(8, tenorWeight, L"E♭");
            newTower.SetDoveRef(L"14523");
            Assert::AreEqual(L"14523", newTower.DoveRef().c_str());
            Assert::AreEqual(L"", newTower.Latitude().c_str());
            Assert::AreEqual(L"", newTower.Longitude().c_str());

            Duco::ObjectId towerId = database->TowersDatabase().AddObject(newTower);
            Assert::AreEqual(Duco::ObjectId(1), towerId);
            std::set<Duco::ObjectId> ducoTowerIds;
            ducoTowerIds.insert(towerId);
            Assert::AreEqual((size_t)1, doveDatabase->ImportPositionAndIds(database->TowersDatabase(), ducoTowerIds));

            const Duco::Tower* updatedTower = database->TowersDatabase().FindTower(towerId);
            Assert::AreEqual(L"53.39842", updatedTower->Latitude().c_str());
            Assert::AreEqual(L"-2.44531", updatedTower->Longitude().c_str());
            Assert::AreEqual(L"14523", updatedTower->DoveRef().c_str());
        }

        TEST_METHOD(Dove_FindDoveId_Sydney)
        {
            std::wstring tenorWeight = L"29-0-4 ";
            Tower newTower(KNoId, L"Cathedral Church of St Andrew", L"SYDNEY", L"New South Wales, Australia", 13);
            Duco::ObjectId correctRingId = newTower.AddRing(2, tenorWeight, L"C♯");
            std::map<unsigned int, TRenameBellType> renamedBells;
            std::pair<unsigned int, TRenameBellType> extraTreble (1, TRenameBellType::EExtraTreble);
            renamedBells.insert(extraTreble);
            newTower.SetRenamedBells(renamedBells);
            Assert::AreEqual(L"", newTower.Latitude().c_str());
            Assert::AreEqual(L"", newTower.Longitude().c_str());
            Assert::AreEqual(13u, newTower.Bells());
            Assert::AreEqual(12u, newTower.BellsForDove());

            Duco::ObjectId towerId = database->TowersDatabase().AddObject(newTower);
            Assert::AreEqual(Duco::ObjectId(1), towerId);

            Duco::DoveObject foundTower;
            Duco::ObjectId existingTowerWithId;
            Assert::IsTrue(doveDatabase->SuggestTower(database->TowersDatabase(), newTower, foundTower, existingTowerWithId));

            std::map<std::wstring, Duco::ObjectId> updateTowers;
            std::pair<std::wstring, Duco::ObjectId> updateKey(foundTower.DoveId(), towerId);
            updateTowers.insert(updateKey);
            doveDatabase->ImportTowerIdAndPosition(database->TowersDatabase(), updateTowers);
            
            const Duco::Tower* updatedTower = database->TowersDatabase().FindTower(towerId);
            Assert::AreEqual(L"-33.87395", updatedTower->Latitude().c_str());
            Assert::AreEqual(L"151.20602", updatedTower->Longitude().c_str());
            Assert::AreEqual(L"16969", updatedTower->DoveRef().c_str());
            Assert::AreEqual(L"5942", updatedTower->TowerbaseId().c_str());
        }

        TEST_METHOD(Dove_ImportTowerIdAndPosition_Warburton)
        {
            std::wstring tenorWeight = L"25-2-6";
            Tower newTower(KNoId, L"St Werburgh", L"Warburton", L"Greater Manchester", 8);
            Duco::ObjectId correctRingId = newTower.AddRing(8, tenorWeight, L"E♭");
            Duco::ObjectId towerId = database->TowersDatabase().AddObject(newTower);
            Assert::AreEqual(Duco::ObjectId(1), towerId);
            database->ResetDataChanged();

            std::map<std::wstring, Duco::ObjectId> doveIdAndTowerIds;
            std::pair<std::wstring, Duco::ObjectId> updateTower(L"WARBURTON", towerId);
            doveIdAndTowerIds.insert(updateTower);
            Assert::AreEqual((size_t)1, doveDatabase->ImportTowerIdAndPosition(database->TowersDatabase(), doveIdAndTowerIds));

            Assert::IsTrue(database->DataChanged());

            const Duco::Tower* updatedTower = database->TowersDatabase().FindTower(towerId);
            Assert::AreEqual(L"14523", updatedTower->DoveRef().c_str());
            Assert::AreEqual(L"53.39842", updatedTower->Latitude().c_str());
            Assert::AreEqual(L"-2.44531", updatedTower->Longitude().c_str());
        }

        TEST_METHOD(Dove_AbKettleby_ImportTowerByOldTowerbaseId)
        {
            std::wstring tenorWeight = L"8–2–22";
            Tower newTower(KNoId, L"St Werburgh", L"Ab Kettleby", L"Leicestershire", 6);
            Duco::ObjectId correctRingId = newTower.AddRing(8, tenorWeight, L"G");
            Duco::ObjectId towerId = database->TowersDatabase().AddObject(newTower);
            Assert::AreEqual(Duco::ObjectId(1), towerId);

            std::map<std::wstring, Duco::ObjectId> doveIdAndTowerIds;
            std::pair<std::wstring, Duco::ObjectId> updateTower(L"AB KETTLEB", towerId);
            doveIdAndTowerIds.insert(updateTower);
            Assert::AreEqual((size_t)1, doveDatabase->ImportTowerIdAndPosition(database->TowersDatabase(), doveIdAndTowerIds));

            const Duco::Tower* updatedTower = database->TowersDatabase().FindTower(towerId);
            Assert::AreEqual(L"12574", updatedTower->DoveRef().c_str());
            Assert::AreEqual(L"52.79858", updatedTower->Latitude().c_str());
            Assert::AreEqual(L"-0.92747", updatedTower->Longitude().c_str());
            Assert::AreEqual(L"6918", updatedTower->TowerbaseId().c_str());
        }

        TEST_METHOD(Dove_AbKettleby_ImportTowerByNewTowerbaseId)
        {
            std::wstring tenorWeight = L"8–2–22";
            Tower newTower(KNoId, L"St Werburgh", L"Ab Kettleby", L"Leicestershire", 6);
            Duco::ObjectId correctRingId = newTower.AddRing(8, tenorWeight, L"G");
            Duco::ObjectId towerId = database->TowersDatabase().AddObject(newTower);
            Assert::AreEqual(Duco::ObjectId(1), towerId);

            std::map<std::wstring, Duco::ObjectId> doveIdAndTowerIds;
            std::pair<std::wstring, Duco::ObjectId> updateTower(L"12574", towerId);
            doveIdAndTowerIds.insert(updateTower);
            Assert::AreEqual((size_t)1, doveDatabase->ImportTowerIdAndPosition(database->TowersDatabase(), doveIdAndTowerIds));

            const Duco::Tower* updatedTower = database->TowersDatabase().FindTower(towerId);
            Assert::AreEqual(L"12574", updatedTower->DoveRef().c_str());
            Assert::AreEqual(L"52.79858", updatedTower->Latitude().c_str());
            Assert::AreEqual(L"-0.92747", updatedTower->Longitude().c_str());
            Assert::AreEqual(L"6918", updatedTower->TowerbaseId().c_str());
        }


        TEST_METHOD(Dove_Halifax_ImportTenorKeys)
        {
            std::wstring tenorWeight = L"28–0–14";
            Tower newTower(KNoId, L"Minster Ch of S John Bapt", L"Halifax", L"West Yorkshire", 14);
            newTower.SetDoveRef(L"HALIFAX");

            std::map<unsigned int, Duco::TRenameBellType> renamedBells;
            std::pair<unsigned int, Duco::TRenameBellType> extraTreble(1, Duco::TRenameBellType::EExtraTreble);
            renamedBells.emplace(extraTreble);
            std::pair<unsigned int, Duco::TRenameBellType> flat6th(8, Duco::TRenameBellType::EFlat);
            renamedBells.emplace(flat6th);
            newTower.SetRenamedBells(renamedBells);

            Duco::ObjectId backTwelveRingId = newTower.AddRing(12, tenorWeight, L"");
            Duco::ObjectId backEightRingId = newTower.AddRing(8, L"1–2–3", L"A");
            Duco::ObjectId backSixRingId = newTower.AddRing(6, L"", L"");

            Duco::ObjectId lightTenRingId = newTower.NextFreeRingId();
            std::set<unsigned int> lightTenBells;
            lightTenBells.insert(1);
            lightTenBells.insert(2);
            lightTenBells.insert(3);
            lightTenBells.insert(4);
            lightTenBells.insert(5);
            lightTenBells.insert(6);
            lightTenBells.insert(8);
            lightTenBells.insert(9);
            lightTenBells.insert(10);
            lightTenBells.insert(11);
            Ring lightTen(lightTenRingId, 10, L"Light ten", lightTenBells, L"10–2–12", L"");
            Assert::IsTrue(newTower.AddRing(lightTen));
            Assert::AreEqual(L"28-0-14", newTower.TenorDescription(backTwelveRingId).c_str());
            Assert::AreEqual(L"1-2-3 in A", newTower.TenorDescription(backEightRingId).c_str());
            Assert::AreEqual(L"", newTower.TenorDescription(backSixRingId).c_str());
            Assert::AreEqual(L"10-2-12", newTower.TenorDescription(lightTenRingId).c_str());

            Assert::IsTrue(doveDatabase->ImportMissingTenorKeyAndIds(newTower));

            Assert::AreEqual(L"D", newTower.FindRing(backTwelveRingId)->TenorKey().c_str());
            Assert::AreEqual(L"A", newTower.FindRing(backEightRingId)->TenorKey().c_str());
            Assert::AreEqual(L"", newTower.FindRing(backSixRingId)->TenorKey().c_str());
            Assert::AreEqual(L"", newTower.FindRing(lightTenRingId)->TenorKey().c_str());
            Assert::AreEqual(L"28-0-14 in D", newTower.TenorDescription(backTwelveRingId).c_str());
            Assert::AreEqual(L"1-2-3 in A", newTower.TenorDescription(backEightRingId).c_str());
            Assert::AreEqual(L"", newTower.TenorDescription(backSixRingId).c_str());
            Assert::AreEqual(L"10-2-12", newTower.TenorDescription(lightTenRingId).c_str());
            Assert::AreEqual(L"11592", newTower.DoveRef().c_str());

            Assert::AreEqual(L"0", newTower.BellName(1).c_str());
            Assert::AreEqual(L"1", newTower.BellName(2).c_str());
            Assert::AreEqual(L"2", newTower.BellName(3).c_str());
            Assert::AreEqual(L"3", newTower.BellName(4).c_str());
            Assert::AreEqual(L"4", newTower.BellName(5).c_str());
            Assert::AreEqual(L"5", newTower.BellName(6).c_str());
            Assert::AreEqual(L"6", newTower.BellName(7).c_str());
            Assert::AreEqual(L"6♭", newTower.BellName(8).c_str());
            Assert::AreEqual(L"7", newTower.BellName(9).c_str());
            Assert::AreEqual(L"8", newTower.BellName(10).c_str());
            Assert::AreEqual(L"9", newTower.BellName(11).c_str());
            Assert::AreEqual(L"10", newTower.BellName(12).c_str());
            Assert::AreEqual(L"11", newTower.BellName(13).c_str());
            Assert::AreEqual(L"12", newTower.BellName(14).c_str());
        }

        TEST_METHOD(Dove_Halifax_ImportTenorKeysInDatabase)
        {
            std::wstring tenorWeight = L"28–0–14";
            Tower newTower(KNoId, L"Minster Ch of S John Bapt", L"Halifax", L"West Yorkshire", 14);
            newTower.SetDoveRef(L"HALIFAX");

            std::map<unsigned int, Duco::TRenameBellType> renamedBells;
            std::pair<unsigned int, Duco::TRenameBellType> extraTreble(1, Duco::TRenameBellType::EExtraTreble);
            std::pair<unsigned int, Duco::TRenameBellType> flat6th(8, Duco::TRenameBellType::EFlat);
            newTower.SetRenamedBells(renamedBells);

            Duco::ObjectId backTwelveRingId = newTower.AddRing(12, tenorWeight, L"");
            Duco::ObjectId backEightRingId = newTower.AddRing(8, L"1–2–3", L"A");
            Duco::ObjectId backSixRingId = newTower.AddRing(6, L"", L"");

            Duco::ObjectId lightTenRingId = newTower.NextFreeRingId();
            std::set<unsigned int> lightTenBells;
            lightTenBells.insert(1);
            lightTenBells.insert(2);
            lightTenBells.insert(3);
            lightTenBells.insert(4);
            lightTenBells.insert(5);
            lightTenBells.insert(6);
            lightTenBells.insert(8);
            lightTenBells.insert(9);
            lightTenBells.insert(10);
            lightTenBells.insert(11);
            Ring lightTen(lightTenRingId, 10, L"Light ten", lightTenBells, L"10–2–12", L"");
            Assert::IsTrue(newTower.AddRing(lightTen));

            Assert::AreEqual(L"28-0-14", newTower.TenorDescription(backTwelveRingId).c_str());
            Assert::AreEqual(L"1-2-3 in A", newTower.TenorDescription(backEightRingId).c_str());
            Assert::AreEqual(L"", newTower.TenorDescription(backSixRingId).c_str());
            Assert::AreEqual(L"10-2-12", newTower.TenorDescription(lightTenRingId).c_str());

            Duco::ObjectId towerId = database->TowersDatabase().AddObject(newTower);
            Assert::AreEqual(Duco::ObjectId(1), towerId);
            Assert::IsTrue(database->DataChanged());
            database->ResetDataChanged();


            Assert::IsTrue(database->TowersDatabase().ImportMissingTenorKeys(*doveDatabase));
            Assert::IsTrue(database->DataChanged());

            const Duco::Tower* updatedTower = database->TowersDatabase().FindTower(towerId);
            Assert::AreEqual(L"11592", updatedTower->DoveRef().c_str());
            Assert::AreEqual(L"D", updatedTower->FindRing(backTwelveRingId)->TenorKey().c_str());
            Assert::AreEqual(L"A", updatedTower->FindRing(backEightRingId)->TenorKey().c_str());
            Assert::AreEqual(L"", updatedTower->FindRing(backSixRingId)->TenorKey().c_str());
            Assert::AreEqual(L"", updatedTower->FindRing(lightTenRingId)->TenorKey().c_str());
            Assert::AreEqual(L"28-0-14 in D", updatedTower->TenorDescription(backTwelveRingId).c_str());
            Assert::AreEqual(L"1-2-3 in A", updatedTower->TenorDescription(backEightRingId).c_str());
            Assert::AreEqual(L"", updatedTower->TenorDescription(backSixRingId).c_str());
            Assert::AreEqual(L"10-2-12", updatedTower->TenorDescription(lightTenRingId).c_str());
        }

        TEST_METHOD(Dove_Import_AbKettleby_ByOldDoveId)
        {
            TowerDatabase database;

            Duco::DoveObject doveTower;
            Assert::IsTrue(doveDatabase->FindTower(L"AB KETTLEB", 0, doveTower));
            Duco::ObjectId towerId = database.AddTower(doveTower);

            const Duco::Tower* updatedTower = database.FindTower(towerId);
            Assert::IsNotNull(updatedTower);
            Assert::AreEqual(L"12574", updatedTower->DoveRef().c_str());
            Assert::AreEqual(L"52.79858", updatedTower->Latitude().c_str());
            Assert::AreEqual(L"-0.92747", updatedTower->Longitude().c_str());
            Assert::AreEqual(L"6918", updatedTower->TowerbaseId().c_str());
            Assert::AreEqual(L"Ground floor ringing chamber\r\nhttp://www.abkettlebybells.co.uk/", updatedTower->Notes().c_str());
        }

        TEST_METHOD(Dove_Import_AbKettleby_ByNewDoveId)
        {
            TowerDatabase database;

            Duco::DoveObject doveTower;
            Assert::IsTrue(doveDatabase->FindTower(L"12574", 0, doveTower));
            Duco::ObjectId towerId = database.AddTower(doveTower);

            const Duco::Tower* updatedTower = database.FindTower(towerId);
            Assert::IsNotNull(updatedTower);
            Assert::AreEqual(L"12574", updatedTower->DoveRef().c_str());
            Assert::AreEqual(L"52.79858", updatedTower->Latitude().c_str());
            Assert::AreEqual(L"-0.92747", updatedTower->Longitude().c_str());
            Assert::AreEqual(L"6918", updatedTower->TowerbaseId().c_str());
            Assert::AreEqual(L"Ground floor ringing chamber\r\nhttp://www.abkettlebybells.co.uk/", updatedTower->Notes().c_str());
        }

        TEST_METHOD(Dove_Import_Rugby_8Bells)
        {
            Duco::DoveObject doveTower;
            Assert::IsTrue(doveDatabase->FindTower(L"14482", 8, doveTower));

            Assert::AreEqual(L"14482", doveTower.DoveId().c_str());
            Assert::AreEqual(L"RUGBY NE", doveTower.OldDoveId().c_str());
            Assert::AreEqual(L"4483", doveTower.RingId().c_str());
            Assert::AreEqual(L"24¾", doveTower.Tenor().c_str());
            Assert::AreEqual(L"D", doveTower.TenorKey().c_str());
        }

        TEST_METHOD(Dove_Import_Rugby_6Bells_Finds8BellTower)
        {
            Duco::DoveObject doveTower;
            Assert::IsTrue(doveDatabase->FindTower(L"14482", 6, doveTower));

            Assert::AreEqual(L"14482", doveTower.DoveId().c_str());
            Assert::AreEqual(L"RUGBY NE", doveTower.OldDoveId().c_str());
            Assert::AreEqual(L"4483", doveTower.RingId().c_str());
            Assert::AreEqual(L"24¾", doveTower.Tenor().c_str());
            Assert::AreEqual(L"D", doveTower.TenorKey().c_str());
        }

        TEST_METHOD(Dove_Import_Rugby_5Bells)
        {
            Duco::DoveObject doveTower;
            Assert::IsTrue(doveDatabase->FindTower(L"14482", 5, doveTower));

            Assert::AreEqual(L"14482", doveTower.DoveId().c_str());
            Assert::AreEqual(L"RUGBY W", doveTower.OldDoveId().c_str());
            Assert::AreEqual(L"6823", doveTower.RingId().c_str());
            Assert::AreEqual(L"9-2-3", doveTower.Tenor().c_str());
            Assert::AreEqual(L"A", doveTower.TenorKey().c_str());
        }

        TEST_METHOD(Dove_Import_Sleaford_TenorInEFlat)
        {
            Duco::DoveObject doveTower;
            Assert::IsTrue(doveDatabase->FindTower(L"10240", 5, doveTower));

            Assert::AreEqual(L"10240", doveTower.DoveId().c_str());
            Assert::AreEqual(L"19-1-0", doveTower.Tenor().c_str());
            Assert::AreEqual(L"E♭", doveTower.TenorKey().c_str());

            Tower theTower(doveTower);
            Assert::AreEqual(L"E♭", theTower.TowerKey().c_str());
        }

        TEST_METHOD(Dove_WorcesterCathedralBells)
        {
            Duco::DoveObject doveTower;
            Assert::IsTrue(doveDatabase->FindTower(L"11299", 12, doveTower));

            Tower theTower(doveTower);
            Assert::AreEqual(L"B", theTower.TowerKey().c_str());
            Assert::AreEqual(L"Worcester", theTower.City().c_str());
            Assert::AreEqual(L"Cathedral church of Christ & Blessed Virgin Mary", theTower.Name().c_str());

            std::vector<std::wstring> expectedBellNames;
            expectedBellNames.push_back(L"1");
            expectedBellNames.push_back(L"2♯");
            expectedBellNames.push_back(L"2");
            expectedBellNames.push_back(L"3");
            expectedBellNames.push_back(L"4");
            expectedBellNames.push_back(L"5♯");
            expectedBellNames.push_back(L"5");
            expectedBellNames.push_back(L"6");
            expectedBellNames.push_back(L"6♭");
            expectedBellNames.push_back(L"7");
            expectedBellNames.push_back(L"8");
            expectedBellNames.push_back(L"9♯");
            expectedBellNames.push_back(L"9");
            expectedBellNames.push_back(L"10");
            expectedBellNames.push_back(L"11");
            expectedBellNames.push_back(L"12");


            std::vector<std::wstring> bellNames;
            theTower.AllBellNames(bellNames, KNoId);

            Assert::AreEqual(12u, theTower.BellsForDove());
            Assert::AreEqual(16u, theTower.Bells());
            Assert::AreEqual((size_t)16, bellNames.size());
            Assert::AreEqual(expectedBellNames, bellNames);

            Assert::AreEqual(L"1", theTower.BellName(1).c_str());
            Assert::AreEqual(L"2♯", theTower.BellName(2).c_str());
            Assert::AreEqual(L"2", theTower.BellName(3).c_str());
            Assert::AreEqual(L"3", theTower.BellName(4).c_str());
            Assert::AreEqual(L"4", theTower.BellName(5).c_str());
            Assert::AreEqual(L"5♯", theTower.BellName(6).c_str());
            Assert::AreEqual(L"5", theTower.BellName(7).c_str());
            Assert::AreEqual(L"6", theTower.BellName(8).c_str());
            Assert::AreEqual(L"6♭", theTower.BellName(9).c_str());
            Assert::AreEqual(L"7", theTower.BellName(10).c_str());
            Assert::AreEqual(L"8", theTower.BellName(11).c_str());
            Assert::AreEqual(L"9♯", theTower.BellName(12).c_str());
            Assert::AreEqual(L"9", theTower.BellName(13).c_str());
            Assert::AreEqual(L"10", theTower.BellName(14).c_str());
            Assert::AreEqual(L"11", theTower.BellName(15).c_str());
            Assert::AreEqual(L"12", theTower.BellName(16).c_str());
        }

        TEST_METHOD(Dove_EscrickBells)
        {
            Duco::DoveObject doveTower;
            Assert::IsTrue(doveDatabase->FindTower(L"11686", 12, doveTower));

            Tower theTower(doveTower);
            Assert::AreEqual(L"C", theTower.TowerKey().c_str());
            Assert::AreEqual(L"Escrick", theTower.City().c_str());
            Assert::AreEqual(L"St Helen", theTower.Name().c_str());

            std::vector<std::wstring> expectedBellNames;
            expectedBellNames.push_back(L"0");
            expectedBellNames.push_back(L"1");
            expectedBellNames.push_back(L"2");
            expectedBellNames.push_back(L"3");
            expectedBellNames.push_back(L"4");
            expectedBellNames.push_back(L"5");
            expectedBellNames.push_back(L"6");
            expectedBellNames.push_back(L"6♭");
            expectedBellNames.push_back(L"7");
            expectedBellNames.push_back(L"8");
            expectedBellNames.push_back(L"9");
            expectedBellNames.push_back(L"10");
            expectedBellNames.push_back(L"11");
            expectedBellNames.push_back(L"12");


            std::vector<std::wstring> bellNames;
            theTower.AllBellNames(bellNames, KNoId);

            Assert::AreEqual(12u, theTower.BellsForDove());
            Assert::AreEqual(14u, theTower.Bells());
            Assert::AreEqual((size_t)14, bellNames.size());
            Assert::AreEqual(expectedBellNames, bellNames);
        }

        TEST_METHOD(Dove_ImportDordrecht)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            DoveObject doveTower;
            Assert::IsTrue(doveDatabase->FindTower(L"14275", 8, doveTower));

            Tower newTower(doveTower);
            Duco::ObjectId towerId = database.TowersDatabase().AddObject(newTower);
            Assert::AreEqual(Duco::ObjectId(1), towerId);

            Assert::AreEqual(L"7645", newTower.TowerbaseId().c_str());
            Assert::AreEqual(L"14275", newTower.DoveRef().c_str());
            Assert::AreEqual(L"'t Klockhuys", newTower.Name().c_str());
            Assert::AreEqual(L"Dordrecht", newTower.City().c_str());
            Assert::AreEqual(L"Netherlands", newTower.County().c_str());
        }

        TEST_METHOD(Dove_ImportBirminghamCathedralAndFindByTowerbaseId)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            DoveObject doveTower;
            Assert::IsTrue(doveDatabase->FindTower(L"15513", 12, doveTower));

            Tower newTower(doveTower);
            Duco::ObjectId towerId = database.TowersDatabase().AddObject(newTower);
            Assert::AreEqual(Duco::ObjectId(1), towerId);

            Assert::AreEqual(L"492", newTower.TowerbaseId().c_str());
            Assert::AreEqual(towerId, database.TowersDatabase().FindTowerByTowerbaseId(L"492"));
            Assert::AreEqual(towerId, database.TowersDatabase().FindTowerByTowerbaseId(L"0492"));
            Assert::IsFalse(database.TowersDatabase().FindTowerByTowerbaseId(L"555").ValidId());
        }

        TEST_METHOD(Dove_ImportBirminghamCathedralAndFindByTowerbaseIdWithExtraZeros)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            DoveObject doveTower;
            Assert::IsTrue(doveDatabase->FindTower(L"15513", 12, doveTower));

            Tower newTower(doveTower);
            newTower.SetTowerbaseId(L"0492");
            Duco::ObjectId towerId = database.TowersDatabase().AddObject(newTower);
            Assert::AreEqual(Duco::ObjectId(1), towerId);

            Assert::AreEqual(L"492", newTower.TowerbaseId().c_str());
            Assert::AreEqual(towerId, database.TowersDatabase().FindTowerByTowerbaseId(L"492"));
            Assert::AreEqual(towerId, database.TowersDatabase().FindTowerByTowerbaseId(L"0492"));
            Assert::IsFalse(database.TowersDatabase().FindTowerByTowerbaseId(L"555").ValidId());
        }

        TEST_METHOD(Dove_AntiClockwiseTower)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            DoveObject doveTower;
            Assert::IsTrue(doveDatabase->FindTower(L"13660", 12, doveTower));

            Tower newTower(doveTower);
            Duco::ObjectId towerId = database.TowersDatabase().AddObject(newTower);
            Assert::AreEqual(Duco::ObjectId(1), towerId);

            Assert::AreEqual(L"St Margaret", newTower.Name().c_str());
            Assert::AreEqual(L"Abbotsley", newTower.City().c_str());
            Assert::IsTrue(newTower.AntiClockwise());
        }

        TEST_METHOD(Dove_NumberOfTowersInCountry_Netherlands)
        {
            Assert::AreEqual((size_t)2, doveDatabase->NumberOfTowersInCountry(L"Netherlands"));
        }

        TEST_METHOD(Dove_NumberOfTowersInCountry_Kenya)
        {
            Assert::AreEqual((size_t)1, doveDatabase->NumberOfTowersInCountry(L"Kenya"));
        }

        TEST_METHOD(Dove_NumberOfTowersInCountry_Australia)
        {
            Assert::AreEqual((size_t)68, doveDatabase->NumberOfTowersInCountry(L"Australia"));
        }

        TEST_METHOD(Dove_NumberOfCountries)
        {
            Assert::AreEqual((size_t)21, doveDatabase->NumberOfCountries());
        }

        TEST_METHOD(Dove_SaveSaintsNotUpdated)
        {
            Assert::AreEqual((size_t)637, doveDatabase->SaveSaintsNotUpdated("unmodifiedSaints.txt"));
        }
    };
}

