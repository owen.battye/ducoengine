#include "CppUnitTest.h"
#include "ToString.h"
#include <Method.h>
#include <MethodDatabase.h>
#include <RingingDatabase.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(MethodDatabase_UnitTests)
    {
    public:
        TEST_METHOD(MethodDatabase_AddMethodByFullname)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Duco::ObjectId plainBobMinor = database.MethodsDatabase().AddMethod(L"Plain bob minor");
            Assert::IsTrue(plainBobMinor.ValidId());
            Assert::AreEqual(L"Plain", database.MethodsDatabase().FindMethod(plainBobMinor, false)->Name().c_str());
            Assert::AreEqual(L"bob", database.MethodsDatabase().FindMethod(plainBobMinor, false)->Type().c_str());
            Assert::AreEqual((unsigned int)6, database.MethodsDatabase().FindMethod(plainBobMinor, false)->Order());
            Assert::AreEqual(L"Minor", database.MethodsDatabase().FindMethod(plainBobMinor, false)->OrderName(database).c_str());
        }

        TEST_METHOD(MethodDatabase_AddMethodWithoutNameOrType)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Duco::ObjectId minor = database.MethodsDatabase().AddMethod(L"Minor");
            Assert::IsTrue(minor.ValidId());
            Assert::AreEqual(L"", database.MethodsDatabase().FindMethod(minor, false)->Name().c_str());
            Assert::AreEqual(L"", database.MethodsDatabase().FindMethod(minor, false)->Type().c_str());
            Assert::AreEqual((unsigned int)6, database.MethodsDatabase().FindMethod(minor, false)->Order());
            Assert::AreEqual(L"Minor", database.MethodsDatabase().FindMethod(minor, false)->OrderName(database).c_str());
        }

        TEST_METHOD(MethodDatabase_MethodNameMatchesOrderInName)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Assert::IsFalse(database.MethodsDatabase().MethodNameMatchesOrderInName(L"Plain Bob Minor", 8));
            Assert::IsTrue(database.MethodsDatabase().MethodNameMatchesOrderInName(L"Stedman Triples", 8));
            Assert::IsTrue(database.MethodsDatabase().MethodNameMatchesOrderInName(L"Bristol Surprise Major", 8));
            Assert::IsFalse(database.MethodsDatabase().MethodNameMatchesOrderInName(L"Stedman Caters", 8));
            Assert::IsFalse(database.MethodsDatabase().MethodNameMatchesOrderInName(L"Stedman Caters", 8));
            Assert::IsFalse(database.MethodsDatabase().MethodNameMatchesOrderInName(L"Bristol Surprise maximus", 8));
        }

        TEST_METHOD(MethodDatabase_MethodNameMatchesOrderInName_EndsWithNumberOfMethods)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            std::wstring processedMethodName = L"";
            unsigned int orderNumber = 0;
            bool dualOrder = true;

            Assert::IsTrue(database.MethodsDatabase().RemoveOrderNameFromMethodName(L"Spliced Surprise Minor (8m)", processedMethodName, orderNumber, dualOrder));
            Assert::IsFalse(dualOrder);
            Assert::AreEqual((unsigned int)6, orderNumber);
            Assert::AreEqual(L"(8m) Spliced Surprise", processedMethodName.c_str());
        }

        TEST_METHOD(MethodDatabase_SuggestMethodsWithoutName)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Duco::ObjectId blankMinor = database.MethodsDatabase().AddMethod(L"", L"", 6, false);
            Duco::ObjectId blankSurpriseMajor = database.MethodsDatabase().AddMethod(L"", L"Surprise", 10, false);
            Duco::ObjectId blankTrebleBobMaximus = database.MethodsDatabase().AddMethod(L"", L"Treble Bob", 12, false);

            Assert::AreEqual(blankMinor, database.MethodsDatabase().SuggestMethod(L"Minor"));
            Assert::AreEqual(blankSurpriseMajor, database.MethodsDatabase().SuggestMethod(L"Surprise Royal"));
        }

        TEST_METHOD(MethodDatabase_SuggestBlankMethod)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Duco::ObjectId plainBobMinor = database.MethodsDatabase().AddMethod(L"Plain", L"Bob", 6, false);
            Duco::ObjectId blankMinor = database.MethodsDatabase().AddMethod(L"", L"", 6, false);

            Assert::AreEqual(blankMinor, database.MethodsDatabase().SuggestMethod(L"Minor"));
        }

        TEST_METHOD(MethodDatabase_SuggestMethod_SplicedWithNumberOfMethods_MethodsCountBefore)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Duco::ObjectId splicedSurpriseMajor = database.MethodsDatabase().AddMethod(L"Spliced", L"Surprise", 8, false);
            Duco::ObjectId fourSplicedMethodsSurpriseMajor = database.MethodsDatabase().AddMethod(L"(4m) Spliced", L"Surprise", 8, false);
            Duco::ObjectId sevenMethodsSurpriseMajor = database.MethodsDatabase().AddMethod(L"(7m)", L"Surprise", 8, false);

            Assert::AreEqual(sevenMethodsSurpriseMajor, database.MethodsDatabase().SuggestMethod(L"(7m) Surprise Major"));
            Assert::AreEqual(sevenMethodsSurpriseMajor, database.MethodsDatabase().SuggestMethod(L"Surprise Major (7m)"));
            Assert::AreEqual(sevenMethodsSurpriseMajor, database.MethodsDatabase().SuggestMethod(L"(7m) Surprise Major"));
            Assert::AreEqual(sevenMethodsSurpriseMajor, database.MethodsDatabase().SuggestMethod(L"Surprise Major (7m)"));

            Assert::AreEqual(fourSplicedMethodsSurpriseMajor, database.MethodsDatabase().SuggestMethod(L"(4m) Spliced Surprise Major"));
            Assert::AreEqual(fourSplicedMethodsSurpriseMajor, database.MethodsDatabase().SuggestMethod(L"Spliced Surprise Major (4m)"));
            Assert::AreEqual(fourSplicedMethodsSurpriseMajor, database.MethodsDatabase().SuggestMethod(L"(4m) Spliced Surprise Major"));
            Assert::AreEqual(fourSplicedMethodsSurpriseMajor, database.MethodsDatabase().SuggestMethod(L"Spliced Surprise Major (4m)"));

            Assert::AreEqual(fourSplicedMethodsSurpriseMajor, database.MethodsDatabase().SuggestMethod(L"Spliced (4m) Surprise Major"));
        }

        TEST_METHOD(MethodDatabase_SuggestMethod_LondonNo3SurpriseRoyalInBrackets)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Duco::ObjectId londonSurpriseRoyal = database.MethodsDatabase().AddMethod(L"London (No.3)", L"Surprise", 10, false);

            Assert::AreEqual(londonSurpriseRoyal, database.MethodsDatabase().SuggestMethod(L"London (No.3) Surprise Royal"));
            Assert::AreEqual(londonSurpriseRoyal, database.MethodsDatabase().SuggestMethod(L"London Surprise Royal"));
            Assert::AreEqual(londonSurpriseRoyal, database.MethodsDatabase().SuggestMethod(L"London No.3 Surprise Royal"));
        }

        TEST_METHOD(MethodDatabase_SuggestMethod_LondonNo3SurpriseRoyal)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Duco::ObjectId londonSurpriseRoyal = database.MethodsDatabase().AddMethod(L"London No.3", L"Surprise", 10, false);

            Assert::AreEqual(londonSurpriseRoyal, database.MethodsDatabase().SuggestMethod(L"London No.3 Surprise Royal"));
            Assert::AreEqual(londonSurpriseRoyal, database.MethodsDatabase().SuggestMethod(L"London (No.3) Surprise Royal"));
            Assert::AreEqual(londonSurpriseRoyal, database.MethodsDatabase().SuggestMethod(L"London Surprise Royal"));
        }

        TEST_METHOD(MethodDatabase_SuggestMethod_KentTrebleBobMajor)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Duco::ObjectId kent = database.MethodsDatabase().AddMethod(L"Kent", L"Treble Bob", 8, false);

            Assert::AreEqual(kent, database.MethodsDatabase().SuggestMethod(L"Kent Treble Bob Major", 4));
        }

        TEST_METHOD(MethodDatabase_SuggestMethodsWithSurpriseSurpriseMajorAndSuprise)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Duco::ObjectId blankMajor = database.MethodsDatabase().AddMethod(L"", L"", 8, false);
            Duco::ObjectId blankSurpriseMajor = database.MethodsDatabase().AddMethod(L"", L"Surprise", 8, false);
            Duco::ObjectId surpriseSurpriseMajor = database.MethodsDatabase().AddMethod(L"Surprise", L"Surprise", 8, false);
            Duco::ObjectId bristolSurpriseMajor = database.MethodsDatabase().AddMethod(L"Bristol", L"Surprise", 8, false);

            Assert::AreEqual(bristolSurpriseMajor, database.MethodsDatabase().SuggestMethod(L"Bristol Surprise Major"));
        }


        TEST_METHOD(MethodDatabase_SuggestMethod_NoMatch)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Duco::ObjectId blankMajor = database.MethodsDatabase().AddMethod(L"", L"", 8, false);
            Duco::ObjectId blankSurpriseMajor = database.MethodsDatabase().AddMethod(L"", L"Surprise", 8, false);
            Duco::ObjectId surpriseSurpriseMajor = database.MethodsDatabase().AddMethod(L"Surprise", L"Surprise", 8, false);
            Duco::ObjectId bristolSurpriseMajor = database.MethodsDatabase().AddMethod(L"Bristol", L"Surprise", 8, false);

            Assert::AreEqual(Duco::ObjectId(), database.MethodsDatabase().SuggestMethod(L"Owen Surprise Major"));
        }

        TEST_METHOD(MethodDatabase_SuggestMethodsWithSurpriseSurpriseMajor)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Duco::ObjectId blankMajor = database.MethodsDatabase().AddMethod(L"", L"", 8, false);
            Duco::ObjectId blankSurpriseMajor = database.MethodsDatabase().AddMethod(L"", L"Surprise", 8, false);
            Duco::ObjectId bristolSurpriseMajor = database.MethodsDatabase().AddMethod(L"Bristol", L"Surprise", 8, false);

            Assert::AreEqual(blankSurpriseMajor, database.MethodsDatabase().SuggestMethod(L"Surprise Major"));
        }

        TEST_METHOD(MethodDatabase_ReplaceShortMethodTypes_Surprise)
        {
            MethodDatabase database;
            database.CreateDefaultBellNames();

            Duco::ObjectId londonSurpriseRoyalId = database.AddMethod(L"London No.3", L"S", 10, false);
            Duco::ObjectId kentRoyalId = database.AddMethod(L"Kent", L"TB", 8, false);

            database.ReplaceMethodTypes(NULL, "");

            const Method* const london = database.FindMethod(londonSurpriseRoyalId);
            Assert::AreEqual(L"London No.3 Surprise Royal", london->FullName(database).c_str());

            const Method* const kent = database.FindMethod(kentRoyalId);
            Assert::AreEqual(L"Kent Treble Bob Major", kent->FullName(database).c_str());
        }

        TEST_METHOD(MethodDatabase_ReplaceShortMethodTypes_TrebleDelight)
        {
            MethodDatabase database;
            database.CreateDefaultBellNames();

            Duco::ObjectId owenMinorId = database.AddMethod(L"Owen", L"TD", 6, false);

            database.ReplaceMethodTypes(NULL, "");

            const Method* const kent = database.FindMethod(owenMinorId);
            Assert::AreEqual(L"Owen Treble Dodging Minor", kent->FullName(database).c_str());
        }

        // Before / after / with M no M.
        // London no3.
    };
}
