#include "CppUnitTest.h"
#include <PerformanceDate.h>
#include <DucoEngineUtils.h>
#include <ctime>
#include "ToString.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(PerformanceDate_UnitTests)
    {
    public:
        TEST_METHOD(PerformanceDate_DefaultConstructor)
        {
            PerformanceDate currentDate;
            Assert::IsTrue(currentDate.Valid());

            time_t now = time(0);
            tm* ltm = localtime(&now);

            Assert::AreEqual((unsigned int)1900 + ltm->tm_year, currentDate.Year());
            Assert::AreEqual((unsigned int)1 + ltm->tm_mon, currentDate.Month());
            Assert::AreEqual((unsigned int)ltm->tm_mday, currentDate.Day());
        }

        TEST_METHOD(PerformanceDate_StringConstructor_Jan)
        {
            PerformanceDate currentDate (L"20-Jan-2020");

            Assert::AreEqual(2020u, currentDate.Year());
            Assert::AreEqual(1u, currentDate.Month());
            Assert::AreEqual(20u, currentDate.Day());
        }

        TEST_METHOD(PerformanceDate_StringConstructor_Feb)
        {
            PerformanceDate currentDate(L"21-Feb-2020");

            Assert::AreEqual(2020u, currentDate.Year());
            Assert::AreEqual(2u, currentDate.Month());
            Assert::AreEqual(21u, currentDate.Day());
        }
        TEST_METHOD(PerformanceDate_StringConstructor_May)
        {
            PerformanceDate currentDate(L"15-May-2020");

            Assert::AreEqual(2020u, currentDate.Year());
            Assert::AreEqual(5u, currentDate.Month());
            Assert::AreEqual(15u, currentDate.Day());
        }
        TEST_METHOD(PerformanceDate_StringConstructor_Mar)
        {
            PerformanceDate currentDate(L"03-Mar-2020");

            Assert::AreEqual(2020u, currentDate.Year());
            Assert::AreEqual(3u, currentDate.Month());
            Assert::AreEqual(3u, currentDate.Day());
        }

        TEST_METHOD(PerformanceDate_StringConstructor_Apr)
        {
            PerformanceDate currentDate(L"3-Apr-2020");

            Assert::AreEqual(2020u, currentDate.Year());
            Assert::AreEqual(4u, currentDate.Month());
            Assert::AreEqual(3u, currentDate.Day());
        }
        TEST_METHOD(PerformanceDate_StringConstructor_Jun)
        {
            PerformanceDate currentDate(L"3-Jun-2020");

            Assert::AreEqual(2020u, currentDate.Year());
            Assert::AreEqual(6u, currentDate.Month());
            Assert::AreEqual(3u, currentDate.Day());
        }
        TEST_METHOD(PerformanceDate_StringConstructor_Jul_Spaces)
        {
            PerformanceDate currentDate(L"3 Jul 2020", true);

            Assert::AreEqual(2020u, currentDate.Year());
            Assert::AreEqual(7u, currentDate.Month());
            Assert::AreEqual(3u, currentDate.Day());
        }

        TEST_METHOD(PerformanceDate_StringConstructor_Aug)
        {
            PerformanceDate currentDate(L"10-Aug-2020");

            Assert::AreEqual(2020u, currentDate.Year());
            Assert::AreEqual(8u, currentDate.Month());
            Assert::AreEqual(10u, currentDate.Day());
        }
        TEST_METHOD(PerformanceDate_StringConstructor_Sep)
        {
            PerformanceDate currentDate(L"11-Sep-1901");

            Assert::AreEqual(1901u, currentDate.Year());
            Assert::AreEqual(9u, currentDate.Month());
            Assert::AreEqual(11u, currentDate.Day());
        }

        TEST_METHOD(PerformanceDate_LeapYear)
        {
            PerformanceDate currentDate (2020, 02, 29); // 29th Feb 2020

            Assert::IsTrue(currentDate.Valid());
            Assert::AreEqual((unsigned int)2020, currentDate.Year());
            Assert::AreEqual((unsigned int)2, currentDate.Month());
            Assert::AreEqual((unsigned int)29, currentDate.Day());
            Assert::AreEqual(L"29/2/2020", currentDate.Str().c_str());
            Assert::AreEqual(L"29-FEB-2020", currentDate.DateWithLongerMonth().c_str());
            Assert::AreEqual(L"Sat", currentDate.WeekDay().c_str());
            Assert::AreEqual(L"Saturday", currentDate.WeekDay(true).c_str());
            Assert::AreEqual(L"Saturday 29 February 2020", currentDate.FullDate().c_str());
            Assert::IsTrue(currentDate.Valid());
        }

        TEST_METHOD(PerformanceDate_LeapYear_DayBefore)
        {
            PerformanceDate currentDate(2020, 02, 28); // 28th Feb 2020

            Assert::IsTrue(currentDate.Valid());
            Assert::AreEqual((unsigned int)2020, currentDate.Year());
            Assert::AreEqual((unsigned int)2, currentDate.Month());
            Assert::AreEqual((unsigned int)28, currentDate.Day());
            Assert::AreEqual(L"28/2/2020", currentDate.Str().c_str());
            Assert::AreEqual(L"28-FEB-2020", currentDate.DateWithLongerMonth().c_str());
            Assert::AreEqual(L"Fri", currentDate.WeekDay().c_str());
            Assert::AreEqual(L"Friday", currentDate.WeekDay(true).c_str());
            Assert::IsTrue(currentDate.Valid());
        }

        TEST_METHOD(PerformanceDate_FirstEverPealDate)
        {
            PerformanceDate currentDate(1715, 5, 2); // 2nd May 1715

            Assert::AreEqual((unsigned int)1715, currentDate.Year());
            Assert::AreEqual((unsigned int)5, currentDate.Month());
            Assert::AreEqual((unsigned int)2, currentDate.Day());
            Assert::AreEqual(L"2/5/1715", currentDate.Str().c_str());
            Assert::AreEqual(L" 2-MAY-1715", currentDate.DateWithLongerMonth().c_str());
            Assert::AreEqual(L"Thu", currentDate.WeekDay().c_str());
            Assert::AreEqual(L"Thursday", currentDate.WeekDay(true).c_str());
            Assert::AreEqual(L"Thursday 2 May 1715", currentDate.FullDate().c_str());
            Assert::IsTrue(currentDate.Valid());
        }

        TEST_METHOD(PerformanceDate_DateCreatedCorrectlyAndValid)
        {
            PerformanceDate currentDate(2019, 10, 21); // 21th Oct 2019

            Assert::IsTrue(currentDate.Valid());
            Assert::AreEqual((unsigned int)2019, currentDate.Year());
            Assert::AreEqual((unsigned int)10, currentDate.Month());
            Assert::AreEqual((unsigned int)21, currentDate.Day());
            Assert::AreEqual(L"21/10/2019", currentDate.Str().c_str());
            Assert::AreEqual(L"21-OCT-2019", currentDate.DateWithLongerMonth().c_str());
            Assert::AreEqual(L"Mon", currentDate.WeekDay().c_str());
            Assert::AreEqual(L"Monday", currentDate.WeekDay(true).c_str());
            Assert::AreEqual(L"Monday 21 October 2019", currentDate.FullDate().c_str());
        }

        TEST_METHOD(PerformanceDate_EqualsOperator)
        {
            PerformanceDate currentDate;
            PerformanceDate aDate(2019, 10, 20); // 20th Oct 2019

            currentDate = aDate;
            Assert::AreEqual(L"20-OCT-2019", currentDate.DateWithLongerMonth().c_str());
            Assert::AreEqual(L"Sunday 20 October 2019", currentDate.FullDate().c_str());
            Assert::AreEqual(aDate, currentDate);
        }

        TEST_METHOD(PerformanceDate_LongestPealEverRung)
        {
            PerformanceDate currentDate(1963, 7, 27); // 27 Jul 1963
            Assert::AreEqual((unsigned int)1963, currentDate.Year());
            Assert::AreEqual((unsigned int)7, currentDate.Month());
            Assert::AreEqual((unsigned int)27, currentDate.Day());
            Assert::AreEqual(L"27/7/1963", currentDate.Str().c_str());
            Assert::AreEqual(L"27-JUL-1963", currentDate.DateWithLongerMonth().c_str());
            Assert::AreEqual(L"Saturday 27 July 1963", currentDate.FullDate().c_str());
            Assert::AreEqual(L"Sat", currentDate.WeekDay().c_str());
            Assert::AreEqual(L"Saturday", currentDate.WeekDay(true).c_str());

            Assert::IsTrue(currentDate.Valid());
        }

        TEST_METHOD(PerformanceDate_SubtractMonths)
        {
            PerformanceDate currentDate(2020, 10, 27);
            PerformanceDate subtractedDate(currentDate);
            subtractedDate.RemoveMonths(5);

            PerformanceDate expectedDate(2020, 5, 27);
            Assert::AreEqual(expectedDate, subtractedDate);
        }

        TEST_METHOD(PerformanceDate_Subtract600Months)
        {
            PerformanceDate currentDate(2022, 11, 17);
            PerformanceDate subtractedDate(currentDate);
            subtractedDate.RemoveMonths(600);

            PerformanceDate expectedDate(1972, 11, 17);
            Assert::AreEqual(expectedDate, subtractedDate);
        }

        TEST_METHOD(PerformanceDate_SubtractMonthsToFebruary)
        {
            PerformanceDate currentDate(2020, 7, 31);
            PerformanceDate subtractedDate(currentDate);
            subtractedDate.RemoveMonths(5);

            PerformanceDate expectedDate(2020, 2, 29);
            Assert::AreEqual(expectedDate, subtractedDate);
        }

        TEST_METHOD(PerformanceDate_SubtractMonthsOverYear)
        {
            PerformanceDate currentDate(2020, 7, 31);
            PerformanceDate subtractedDate(currentDate);
            subtractedDate.RemoveMonths(10);

            PerformanceDate expectedDate(2019, 9, 30);
            Assert::AreEqual(expectedDate, subtractedDate);
        }

        TEST_METHOD(PerformanceDate_InvalidDates)
        {
            PerformanceDate validDate(1975, 10, 20);
            Assert::IsTrue(validDate.Valid());

            PerformanceDate dateOne(0, 7, 31);
            Assert::IsFalse(dateOne.Valid());

            PerformanceDate dateTwo(31, 2, 1999);
            Assert::IsFalse(dateTwo.Valid());

            PerformanceDate dateThree(31, 0, 1999);
            Assert::IsFalse(dateThree.Valid());

            PerformanceDate dateFour(31, 13, 1999);
            Assert::IsFalse(dateFour.Valid());
        }

        TEST_METHOD(PerformanceDate_NumberOfDaysUntil)
        {
            PerformanceDate dateOne(1975, 10, 10);
            PerformanceDate dateTwo(1975, 10, 20);
            PerformanceDate dateThree(1975, 11, 20);
            PerformanceDate dateFour(1976, 2, 20);
            PerformanceDate dateFive(1976, 3, 20);

            Assert::AreEqual((unsigned int)10, dateOne.NumberOfDaysUntil(dateTwo));
            Assert::AreEqual((unsigned int)10, dateTwo.NumberOfDaysUntil(dateOne));
            Assert::AreEqual((unsigned int)31, dateTwo.NumberOfDaysUntil(dateThree));
            Assert::AreEqual((unsigned int)123, dateTwo.NumberOfDaysUntil(dateFour));
            Assert::AreEqual((unsigned int)152, dateTwo.NumberOfDaysUntil(dateFive));
            Assert::AreEqual((unsigned int)29, dateFour.NumberOfDaysUntil(dateFive));

            PerformanceDate dateSix(2020, 12, 30);
            PerformanceDate dateSeven(2021, 1, 1);
            Assert::AreEqual((unsigned int)2, dateSix.NumberOfDaysUntil(dateSeven));
        }

        TEST_METHOD(PerformanceDate_DaysInMonths)
        {
            Assert::AreEqual({ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 }, PerformanceDate::DaysInMonths(1900));
            Assert::AreEqual({ 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 }, PerformanceDate::DaysInMonths(2000));
            Assert::AreEqual({ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 }, PerformanceDate::DaysInMonths(2001));
            Assert::AreEqual({ 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 }, PerformanceDate::DaysInMonths(2020));
            Assert::AreEqual({ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 }, PerformanceDate::DaysInMonths(2021));
            Assert::AreEqual({ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 }, PerformanceDate::DaysInMonths(2100));
        }

        TEST_METHOD(PerformanceDate_AddDays)
        {
            PerformanceDate dateOne(1976, 2, 20);
            PerformanceDate dateTwo(1976, 3, 1);
            Assert::AreEqual(dateTwo, dateOne.AddDays(10));

            PerformanceDate dateThree(2020, 12, 20);
            PerformanceDate dateFour(2021, 1, 5);
            Assert::AreEqual(dateFour, dateThree.AddDays(16));

            PerformanceDate dateFive(3999, 12, 31);
            PerformanceDate dateSix(4000, 01, 01);
            Assert::AreEqual(dateSix, dateFive.AddDays(1));
            Assert::IsTrue(dateFive.Valid());
            Assert::IsFalse(dateSix.Valid());
            Assert::IsFalse(dateFive.AddDays(1).Valid());

            PerformanceDate dateSeven(1975, 2, 20);
            PerformanceDate dateEight(1975, 3, 1);
            Assert::AreEqual(dateEight, dateSeven.AddDays(9));

            PerformanceDate dateNine(2000, 1, 2);
            Assert::AreEqual(L"2/1/2000", dateNine.Str().c_str());
            dateNine = dateNine.AddDays(31);
            Assert::AreEqual(L"2/2/2000", dateNine.Str().c_str());
            dateNine = dateNine.AddDays(29);
            Assert::AreEqual(L"2/3/2000", dateNine.Str().c_str());
            dateNine = dateNine.AddDays(31);
            Assert::AreEqual(L"2/4/2000", dateNine.Str().c_str());
            dateNine = dateNine.AddDays(30);
            Assert::AreEqual(L"2/5/2000", dateNine.Str().c_str());
            dateNine = dateNine.AddDays(31);
            Assert::AreEqual(L"2/6/2000", dateNine.Str().c_str());
            dateNine = dateNine.AddDays(30);
            Assert::AreEqual(L"2/7/2000", dateNine.Str().c_str());
            dateNine = dateNine.AddDays(31);
            Assert::AreEqual(L"2/8/2000", dateNine.Str().c_str());
            dateNine = dateNine.AddDays(31);
            Assert::AreEqual(L"2/9/2000", dateNine.Str().c_str());
            dateNine = dateNine.AddDays(30);
            Assert::AreEqual(L"2/10/2000", dateNine.Str().c_str());
            dateNine = dateNine.AddDays(31);
            Assert::AreEqual(L"2/11/2000", dateNine.Str().c_str());
            dateNine = dateNine.AddDays(30);
            Assert::AreEqual(L"2/12/2000", dateNine.Str().c_str());
            dateNine = dateNine.AddDays(31);
            Assert::AreEqual(L"2/1/2001", dateNine.Str().c_str());

            PerformanceDate dateTen(2000, 1, 1);
            dateTen = dateTen.AddDays(366);
            Assert::AreEqual(L"1/1/2001", dateTen.Str().c_str());
            dateTen = dateTen.AddDays(365);
            Assert::AreEqual(L"1/1/2002", dateTen.Str().c_str());
            dateTen = dateTen.AddDays(365);
            Assert::AreEqual(L"1/1/2003", dateTen.Str().c_str());
            dateTen = dateTen.AddDays(365);
            Assert::AreEqual(L"1/1/2004", dateTen.Str().c_str());
            dateTen = dateTen.AddDays(366);
            Assert::AreEqual(L"1/1/2005", dateTen.Str().c_str());
            dateTen = dateTen.AddDays(365);
            Assert::AreEqual(L"1/1/2006", dateTen.Str().c_str());
            dateTen = dateTen.AddDays(365);
            Assert::AreEqual(L"1/1/2007", dateTen.Str().c_str());

            PerformanceDate dateEleven(2000, 1, 15);
            dateEleven = dateEleven.AddDays(366);
            Assert::AreEqual(L"15/1/2001", dateEleven.Str().c_str());
            dateEleven = dateEleven.AddDays(365);
            Assert::AreEqual(L"15/1/2002", dateEleven.Str().c_str());
            dateEleven = dateEleven.AddDays(365);
            Assert::AreEqual(L"15/1/2003", dateEleven.Str().c_str());
            dateEleven = dateEleven.AddDays(365);
            Assert::AreEqual(L"15/1/2004", dateEleven.Str().c_str());
            dateEleven = dateEleven.AddDays(366);
            Assert::AreEqual(L"15/1/2005", dateEleven.Str().c_str());
            dateEleven = dateEleven.AddDays(365);
            Assert::AreEqual(L"15/1/2006", dateEleven.Str().c_str());
            dateEleven = dateEleven.AddDays(365);
            Assert::AreEqual(L"15/1/2007", dateEleven.Str().c_str());

            PerformanceDate dateTwelve(2000, 1, 1);
            Assert::AreEqual(L"2/1/2001", dateTwelve.AddDays(367).Str().c_str());

            PerformanceDate dateThirteen(2001, 1, 1);
            Assert::AreEqual(L"1/1/2003", dateThirteen.AddDays(730).Str().c_str());

            PerformanceDate dateFourteen(2001, 1, 1);
            Assert::AreEqual(L"1/1/2002", dateFourteen.AddDays(365).Str().c_str());

            PerformanceDate dateFifteen(KFirstJanuary2000);
            Assert::AreEqual(L"1/1/2002", dateFifteen.AddDays(731).Str().c_str());

            PerformanceDate dateSixteen(KFirstJanuary2000);
            Assert::AreEqual(L"1/1/2007", dateSixteen.AddDays(2557).Str().c_str());

            PerformanceDate dateSeventeen(KFirstJanuary2000);
            Assert::AreEqual(L"1/1/2004", dateSeventeen.AddDays(1461).Str().c_str());
        }

        TEST_METHOD(PerformanceDate_AddDays_FromMovelyPealDB_WinRkv1IMport)
        {
            PerformanceDate dateOne(KSecondJanuary2000);
            Assert::AreEqual(L"2/1/2000", dateOne.Str().c_str());
            PerformanceDate dateOneExpected(2004, 12, 11);
            dateOne = dateOne.AddDays(1805);
            Assert::AreEqual(dateOneExpected, dateOne);
            Assert::AreEqual(dateOneExpected.Str().c_str(), dateOne.Str().c_str());

            PerformanceDate dateTwo(KSecondJanuary2000);
            PerformanceDate dateTwoExpected(2005, 12, 27);
            Assert::AreEqual(dateTwoExpected, dateTwo.AddDays(2186));

            PerformanceDate dateThree(KSecondJanuary2000);
            PerformanceDate dateThreeExpected(2006, 12, 28);
            Assert::AreEqual(dateThreeExpected, dateThree.AddDays(2552));

            PerformanceDate dateFour(KFirstJanuary1900);
            PerformanceDate dateFourExpected(1992, 12, 31);
            Assert::AreEqual(dateFourExpected, dateFour.AddDays(33967));
        }

        TEST_METHOD(PerformanceDate_StreamTests_PrintingJan)
        {
            std::wostringstream streamOne;
            PerformanceDate dateOne(1975, 1, 2);
            dateOne.PrintDate(streamOne);
            Assert::AreEqual(L"2 Jan 1975", streamOne.str().c_str());
        }

        TEST_METHOD(PerformanceDate_StreamTests_PrintingFeb)
        {
            std::wostringstream streamOne;
            PerformanceDate dateOne(1976, 2, 20);
            dateOne.PrintDate(streamOne);
            Assert::AreEqual(L"20 Feb 1976", streamOne.str().c_str());
        }
        TEST_METHOD(PerformanceDate_StreamTests_PrintingMarch)
        {
            std::wostringstream streamOne;
            PerformanceDate dateOne(1977, 3, 19);
            dateOne.PrintDate(streamOne);
            Assert::AreEqual(L"19 Mar 1977", streamOne.str().c_str());
        }
        TEST_METHOD(PerformanceDate_StreamTests_PrintingApril)
        {
            std::wostringstream streamOne;
            PerformanceDate dateOne(1978, 4, 20);
            dateOne.PrintDate(streamOne, true);
            Assert::AreEqual(L"20 April 1978", streamOne.str().c_str());
        }
        TEST_METHOD(PerformanceDate_StreamTests_PrintingMay)
        {
            std::wostringstream streamOne;
            PerformanceDate dateOne(1979, 5, 20);
            dateOne.PrintDate(streamOne);
            Assert::AreEqual(L"20 May 1979", streamOne.str().c_str());
        }
        TEST_METHOD(PerformanceDate_StreamTests_PrintingJune)
        {
            std::wostringstream streamOne;
            PerformanceDate dateOne(1980, 6, 20);
            dateOne.PrintDate(streamOne, true);
            Assert::AreEqual(L"20 June 1980", streamOne.str().c_str());
        }
        TEST_METHOD(PerformanceDate_StreamTests_PrintingJuly)
        {
            std::wostringstream streamOne;
            PerformanceDate dateOne(1981, 7, 20);
            dateOne.PrintDate(streamOne);
            Assert::AreEqual(L"20 Jul 1981", streamOne.str().c_str());
        }
        TEST_METHOD(PerformanceDate_StreamTests_PrintingAug)
        {
            std::wostringstream streamOne;
            PerformanceDate dateOne(1982, 8, 20);
            dateOne.PrintDate(streamOne);
            Assert::AreEqual(L"20 Aug 1982", streamOne.str().c_str());
        }
        TEST_METHOD(PerformanceDate_StreamTests_PrintingSept)
        {
            std::wostringstream streamOne;
            PerformanceDate dateOne(1983, 9, 20);
            dateOne.PrintDate(streamOne);
            Assert::AreEqual(L"20 Sep 1983", streamOne.str().c_str());
        }
        TEST_METHOD(PerformanceDate_StreamTests_PrintingOct)
        {
            std::wostringstream streamOne;
            PerformanceDate dateOne(1984, 10, 20);
            dateOne.PrintDate(streamOne);
            Assert::AreEqual(L"20 Oct 1984", streamOne.str().c_str());
        }
        TEST_METHOD(PerformanceDate_StreamTests_PrintingNov)
        {
            std::wostringstream streamOne;
            PerformanceDate dateOne(1985, 11, 20);
            dateOne.PrintDate(streamOne, true);
            Assert::AreEqual(L"20 November 1985", streamOne.str().c_str());
        }
        TEST_METHOD(PerformanceDate_StreamTests_PrintingDec)
        {
            std::wostringstream streamOne;
            PerformanceDate dateOne(1986, 12, 20);
            dateOne.PrintDate(streamOne, true);
            Assert::AreEqual(L"20 December 1986", streamOne.str().c_str());
        }

        TEST_METHOD(PerformanceDate_LongestAdd)
        {
            PerformanceDate earliestDate;
            earliestDate.ResetToEarliest();
            PerformanceDate newDate = earliestDate.AddDays(820698);
            Assert::IsTrue(newDate.Valid());
            Assert::AreEqual((unsigned int)3999, newDate.Year());
            Assert::AreEqual((unsigned int)12, newDate.Month());
            Assert::AreEqual((unsigned int)31, newDate.Day());
            Assert::IsTrue(newDate.IsYearValid());
        }
        TEST_METHOD(PerformanceDate_LongestAddPlusOne)
        {
            PerformanceDate earliestDate;
            earliestDate.ResetToEarliest();
            PerformanceDate newDate = earliestDate.AddDays(820699);
            Assert::IsFalse(newDate.Valid());
            Assert::AreEqual((unsigned int)4000, newDate.Year());
            Assert::AreEqual((unsigned int)1, newDate.Month());
            Assert::AreEqual((unsigned int)1, newDate.Day());
            Assert::IsFalse(newDate.IsYearValid());
        }

        TEST_METHOD(PerformanceDate_VeryLongAdd_DontCrash)
        {
            PerformanceDate today(2020, 4, 16);
            PerformanceDate newDate = today.AddDays(594967295);
            Assert::IsFalse(newDate.IsYearValid());
            Assert::IsFalse(newDate.Valid());
            Assert::AreEqual((unsigned int)4000, newDate.Year());
            Assert::AreEqual((unsigned int)1, newDate.Month());
            Assert::AreEqual((unsigned int)1, newDate.Day());
        }

        TEST_METHOD(PerformanceDate_AssociationDateFormat)
        {
            PerformanceDate today(2020, 1, 31);
            Assert::AreEqual(L"Friday 31 January 2020", today.DateForAssociationReports().c_str());
        }
        TEST_METHOD(PerformanceDate_AssociationDateFormat2)
        {
            PerformanceDate today(2020, 2, 19);
            Assert::AreEqual(L"Wednesday 19 February 2020", today.DateForAssociationReports().c_str());
        }
        TEST_METHOD(PerformanceDate_AssociationDateFormat3)
        {
            PerformanceDate today(2022, 1, 27);
            Assert::AreEqual(L"Thursday 27 January 2022", today.DateForAssociationReports().c_str());
        }
        TEST_METHOD(PerformanceDate_AssociationDateFormat4)
        {
            PerformanceDate today(2020, 1, 26);
            Assert::AreEqual(L"Sunday 26 January 2020", today.DateForAssociationReports().c_str());
        }

        TEST_METHOD(PerformanceDate_CompareSameDate)
        {
            PerformanceDate dateOne(2020, 1, 26);
            PerformanceDate dateTwo(2020, 1, 26);
            Assert::IsTrue(dateOne == dateTwo);
            Assert::IsFalse(dateOne != dateTwo);
        }

        TEST_METHOD(PerformanceDate_CompareDifferentDate)
        {
            PerformanceDate dateOne(2020, 1, 26);
            PerformanceDate dateTwo(2020, 1, 27);
            Assert::IsTrue(dateOne != dateTwo);
            Assert::IsFalse(dateOne == dateTwo);
        }

        TEST_METHOD(PerformanceDate_GreaterThan)
        {
            PerformanceDate dateOne(2020, 1, 26);
            PerformanceDate dateTwo(2020, 1, 27);
            Assert::IsFalse(dateOne >= dateTwo);
            Assert::IsTrue(dateOne <= dateTwo);
            Assert::IsFalse(dateOne > dateTwo);
            Assert::IsTrue(dateOne < dateTwo);
        }

        TEST_METHOD(PerformanceDate_DatesBetweenBeforeZeroEpoc)
        {
            PerformanceDate dateOne(1930, 7, 24);
            PerformanceDate dateTwo(1933, 10, 25);
            
            Assert::AreEqual(1189u, dateOne.NumberOfDaysUntil(dateTwo));
            Assert::AreEqual(1189u, dateTwo.NumberOfDaysUntil(dateOne));
        }

        TEST_METHOD(PerformanceDate_DayMatches)
        {
            PerformanceDate dateOne(1930, 7, 24);
            PerformanceDate dateTwo(1933, 7, 24);

            Assert::IsTrue(dateOne.DaysMatch(dateTwo));
        }

        TEST_METHOD(PerformanceDate_DayDoesntMatches)
        {
            PerformanceDate dateOne(1930, 7, 24);
            PerformanceDate dateTwo(1933, 8, 24);

            Assert::IsFalse(dateOne.DaysMatch(dateTwo));
        }

        TEST_METHOD(PerformanceDate_Clear)
        {
            PerformanceDate dateOne(1930, 7, 24);
            PerformanceDate dateTwo;
            dateOne.Clear();

            Assert::AreEqual(dateOne, dateTwo);
        }
    };
}
