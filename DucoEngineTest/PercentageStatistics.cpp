#include "CppUnitTest.h"
#include <Method.h>
#include <MethodDatabase.h>
#include <Peal.h>
#include <PealDatabase.h>
#include <RingerDatabase.h>
#include <RingingDatabase.h>
#include <PealLengthInfo.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(PercentageStatistics_UnitTests)
    {
    public:
        TEST_METHOD(PealLengthInfo_Lengths)
        {
            Duco::ObjectId towerId(1);

            RingingDatabase database;
            Peal firstPeal;
            firstPeal.SetRingerId(1, 1, false);
            firstPeal.SetRingerId(2, 2, false);
            firstPeal.SetRingerId(3, 3, false);
            firstPeal.SetRingerId(4, 4, false);
            firstPeal.SetRingerId(5, 5, false);
            firstPeal.SetRingerId(6, 6, false);
            firstPeal.SetChanges(5000);
            firstPeal.SetTowerId(towerId);
            PerformanceTime pealTimeOne(180);
            firstPeal.SetTime(pealTimeOne);
            database.PealsDatabase().AddObject(firstPeal);

            Peal secondPeal;
            secondPeal.SetRingerId(7, 1, false);
            secondPeal.SetRingerId(8, 2, false);
            secondPeal.SetRingerId(9, 3, false);
            secondPeal.SetRingerId(4, 4, false);
            secondPeal.SetRingerId(5, 5, false);
            secondPeal.SetRingerId(6, 6, false);
            secondPeal.SetChanges(5100);
            secondPeal.SetTowerId(towerId);
            PerformanceTime pealTimeTwo(120);
            secondPeal.SetTime(pealTimeTwo);
            database.PealsDatabase().AddObject(secondPeal);

            Peal thirdPeal;
            thirdPeal.SetRingerId(7, 1, false);
            thirdPeal.SetRingerId(8, 2, false);
            thirdPeal.SetRingerId(9, 3, false);
            thirdPeal.SetRingerId(4, 4, false);
            thirdPeal.SetRingerId(5, 5, false);
            thirdPeal.SetRingerId(6, 6, false);
            thirdPeal.SetChanges(5100);
            thirdPeal.SetTowerId(2);
            thirdPeal.SetTime(pealTimeTwo);
            database.PealsDatabase().AddObject(thirdPeal);

            std::set<Duco::ObjectId> ringerIds;
            ringerIds.insert(1);
            ringerIds.insert(2);
            ringerIds.insert(3);
            ringerIds.insert(4);
            ringerIds.insert(5);
            ringerIds.insert(6);
            Duco::ObjectId lastpealId;

            Duco::PercentageStatistics stats = database.PealsDatabase().GetRingersPercentages(towerId, ringerIds, lastpealId);

            Assert::AreEqual((size_t)2, stats.NoOfPeals());
            Assert::AreEqual((size_t)10100, stats.NoOfChanges());
            std::multiset<Duco::RingerCount> counts = stats.ChangeCounts();
            std::multiset<Duco::RingerCount>::const_iterator it = counts.begin();

            Assert::AreEqual((size_t)10100, it->NumberOfChanges());
            Assert::AreEqual((size_t)2, it->NumberOfPeals());
            Assert::AreEqual((double)1, it->PercentageOfChanges());
            Assert::AreEqual((double)1, it->PercentageOfPeals());
            ++it;
            Assert::AreEqual((size_t)10100, it->NumberOfChanges());
            Assert::AreEqual((size_t)2, it->NumberOfPeals());
            Assert::AreEqual((double)1, it->PercentageOfChanges());
            Assert::AreEqual((double)1, it->PercentageOfPeals());
            ++it;
            Assert::AreEqual((size_t)10100, it->NumberOfChanges());
            Assert::AreEqual((size_t)2, it->NumberOfPeals());
            Assert::AreEqual((double)1, it->PercentageOfChanges());
            Assert::AreEqual((double)1, it->PercentageOfPeals());
            ++it;
            Assert::AreEqual((size_t)5000, it->NumberOfChanges());
            Assert::AreEqual((size_t)1, it->NumberOfPeals());
            Assert::AreEqual((double)0.49, it->PercentageOfChanges(), 0.01);
            Assert::AreEqual((double)0.5, it->PercentageOfPeals(), 0.1);
            ++it;
            Assert::AreEqual((size_t)5000, it->NumberOfChanges());
            Assert::AreEqual((size_t)1, it->NumberOfPeals());
            Assert::AreEqual((double)0.49, it->PercentageOfChanges(), 0.01);
            Assert::AreEqual((double)0.5, it->PercentageOfPeals(), 0.1);
            ++it;
            Assert::AreEqual((size_t)5000, it->NumberOfChanges());
            Assert::AreEqual((size_t)1, it->NumberOfPeals());
            Assert::AreEqual((double)0.49, it->PercentageOfChanges(), 0.01);
            Assert::AreEqual((double)0.5, it->PercentageOfPeals(), 0.1);
            ++it;
            Assert::IsTrue(it == counts.end());
        }
    };
}
