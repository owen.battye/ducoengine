#include "CppUnitTest.h"
#include <Method.h>
#include <MethodDatabase.h>
#include <MethodNotationDatabaseSingleImporter.h>
#include "ToString.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(MethodImporting_UnitTests)
    {
    private:
        Duco::MethodNotationDatabaseSingleImporter* methodReader;
        Duco::MethodDatabase database;
    public:
        MethodImporting_UnitTests()
        {
            database.ClearObjects(true);
            std::string methodDatbaseFilename = "allmeths.xml";
            methodReader = new MethodNotationDatabaseSingleImporter (database, methodDatbaseFilename, NULL);
            methodReader->ReadMethods();
        }

        ~MethodImporting_UnitTests()
        {
            delete methodReader;
        }
        BEGIN_TEST_METHOD_ATTRIBUTE(MethodImporting_BasicRead_KnownDatabaseSize)
            TEST_PRIORITY(2)
        END_TEST_METHOD_ATTRIBUTE()
        TEST_METHOD(MethodImporting_BasicRead_KnownDatabaseSize)
        {
            Assert::AreEqual((unsigned int)21922, methodReader->MethodCount());
        }

        TEST_METHOD(MethodImporting_BasicRead_FindKnownAllianceMajorMethod)
        {
            Assert::AreEqual((size_t)0, database.NumberOfObjects());
            Duco::ObjectId foundMethodId = methodReader->FindMethod(L"Templetown Alliance Major");
            Assert::IsTrue(foundMethodId.ValidId());
            Assert::AreEqual((size_t)1, database.NumberOfObjects());
            const Duco::Method* const foundMethod = database.FindMethod(foundMethodId, false);
            Assert::AreEqual(L"&x14x38.14x14.58.14x14.58-12", foundMethod->PlaceNotation().c_str());
        }

        BEGIN_TEST_METHOD_ATTRIBUTE(MethodImporting_BasicRead_FindKnownAllianceMajorMethod_TypeAbbreviated)
            TEST_PRIORITY(2)
        END_TEST_METHOD_ATTRIBUTE()
        TEST_METHOD(MethodImporting_BasicRead_FindKnownAllianceMajorMethod_TypeAbbreviated)
        {
            Assert::AreEqual((size_t)0, database.NumberOfObjects());
            Duco::ObjectId foundMethodId = methodReader->FindMethod(L"Templetown A Major");
            Assert::IsTrue(foundMethodId.ValidId());
            Assert::AreEqual((size_t)1, database.NumberOfObjects());
            const Duco::Method* const foundMethod = database.FindMethod(foundMethodId, false);
            Assert::AreEqual(L"&x14x38.14x14.58.14x14.58-12", foundMethod->PlaceNotation().c_str());
        }

        BEGIN_TEST_METHOD_ATTRIBUTE(MethodImporting_BasicRead_FindKnownSurpriseRoyalMethod)
            TEST_PRIORITY(2)
        END_TEST_METHOD_ATTRIBUTE()
        TEST_METHOD(MethodImporting_BasicRead_FindKnownSurpriseRoyalMethod)
        {
            Assert::AreEqual((size_t)0, database.NumberOfObjects());
            Duco::ObjectId foundMethodId = methodReader->FindMethod(L"Bristol Surprise Royal");
            Assert::IsTrue(foundMethodId.ValidId());
            Assert::AreEqual((size_t)1, database.NumberOfObjects());
            const Duco::Method* const foundMethod = database.FindMethod(foundMethodId, false);
            Assert::AreEqual(L"&x50x14.50x50.36.14x70.58.16x16.70x16x10-10", foundMethod->PlaceNotation().c_str());
        }
        BEGIN_TEST_METHOD_ATTRIBUTE(MethodImporting_BasicRead_StedmanCinques)
            TEST_PRIORITY(2)
        END_TEST_METHOD_ATTRIBUTE()
        TEST_METHOD(MethodImporting_BasicRead_StedmanCinques)
        {
            Assert::AreEqual((size_t)0, database.NumberOfObjects());
            Duco::ObjectId foundMethodId = methodReader->FindMethod(L"Stedman Cinques");
            Assert::IsTrue(foundMethodId.ValidId());
            Assert::AreEqual((size_t)1, database.NumberOfObjects());
            const Duco::Method* const foundMethod = database.FindMethod(foundMethodId, false);
            Assert::AreEqual(L"3.1.E.3.1.3.1.3.E.1.3.1", foundMethod->PlaceNotation().c_str());
        }
    };
}
