#include "CppUnitTest.h"
#include <Gender.h>
#include "ToString.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(Gender_UnitTests)
    {
    public:
        TEST_METHOD(Gender_ToString_NotSet)
        {
            Duco::Gender newObject;
            Assert::AreEqual(L"", newObject.Str().c_str());
            Assert::IsFalse(newObject.Defined());
            Assert::IsFalse(newObject.Female());
            Assert::IsFalse(newObject.Male());
        }
        TEST_METHOD(Gender_ToString_Male)
        {
            Duco::Gender newObject;
            newObject.SetMale();
            Assert::AreEqual(L"Male", newObject.Str().c_str());
            Assert::IsTrue(newObject.Defined());
            Assert::IsFalse(newObject.Female());
            Assert::IsTrue(newObject.Male());
        }
        TEST_METHOD(Gender_ToString_Female)
        {
            Duco::Gender newObject;
            newObject.SetFemale();
            Assert::AreEqual(L"Female", newObject.Str().c_str());
            Assert::IsTrue(newObject.Defined());
            Assert::IsFalse(newObject.Male());
            Assert::IsTrue(newObject.Female());
        }

        TEST_METHOD(Gender_DuplicationAndComparison)
        {
            Duco::Gender newObject;
            newObject.SetFemale();

            Duco::Gender copiedObject(newObject);
            Assert::AreEqual(newObject, copiedObject);

            newObject.SetMale();
            Assert::AreNotEqual(newObject, copiedObject);

            newObject = copiedObject;
            Assert::AreEqual(newObject, copiedObject);
        }
    };
}
