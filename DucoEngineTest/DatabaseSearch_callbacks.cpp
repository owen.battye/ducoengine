//#include <algorithm>
//#include <ctime>
#include "CppUnitTest.h"

#include <DatabaseSearch.h>
#include <ImportExportProgressCallback.h>
#include <ProgressCallback.h>
#include <Peal.h>
#include <PealDatabase.h>
#include <RingingDatabase.h>
#include <Ringer.h>
#include <RingerDatabase.h>
#include <SearchFieldStringArgument.h>
#include <SearchValidObject.h>
#include <StatisticFilters.h>

#include "ToString.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(DatabaseSearchCallbacks_UnitTests), ProgressCallback
    {
    public:
        void Initialised()
        {
            previousProgressPercentage = -1;
        }

        void Step(int progressPercent)
        {
            Assert::IsTrue(progressPercent > previousProgressPercentage);
            previousProgressPercentage = progressPercent;
        }

        void Complete(bool error)
        {
            Assert::IsFalse(error);
            Assert::AreEqual(100, previousProgressPercentage);
        }

        TEST_METHOD(DatabaseSearchCallbacks_Ringers)
        {
            Duco::RingingDatabase database;
            DatabaseSearch search(database);

            Ringer newRingerOne(KNoId, L"R Owen", L"Battye");
            database.RingersDatabase().AddObject(newRingerOne);
            Ringer newRingerTwo(KNoId, L"R O", L"Battye");
            database.RingersDatabase().AddObject(newRingerTwo);
            Ringer newRingerThree(KNoId, L"John", L"Smith");
            database.RingersDatabase().AddObject(newRingerThree);
            Ringer newRingerFour(KNoId, L"Clive", L"Smith");
            database.RingersDatabase().AddObject(newRingerFour);

            std::set<Duco::ObjectId> foundIds;
            Duco::TSearchFieldStringArgument* newStr = new Duco::TSearchFieldStringArgument(TSearchFieldId::ESurname, L"Battye");
            search.AddArgument(newStr);
            search.Search(*this, foundIds, TObjectType::ERinger);
            Assert::AreEqual((size_t)2, foundIds.size());
        }
        TEST_METHOD(DatabaseSearchCallbacks_Peals)
        {
            Duco::RingingDatabase database;
            DatabaseSearch search(database);

            Peal one;
            database.PealsDatabase().AddObject(one);
            Peal two;
            database.PealsDatabase().AddObject(two);
            Peal three;
            database.PealsDatabase().AddObject(three);
            Peal four;
            database.PealsDatabase().AddObject(four);

            std::set<Duco::ObjectId> foundIds;
            Duco::TSearchFieldStringArgument* newStr = new Duco::TSearchFieldStringArgument(TSearchFieldId::ERingerName, L"Battye");
            search.AddArgument(newStr);
            search.Search(*this, foundIds, TObjectType::EPeal);
            Assert::AreEqual((size_t)0, foundIds.size());
        }

        TEST_METHOD(DatabaseSearchCallbacks_ValidatePeals)
        {
            Duco::RingingDatabase database;
            DatabaseSearch search(database);

            Peal one;
            database.PealsDatabase().AddObject(one);
            Peal two;
            database.PealsDatabase().AddObject(two);
            Peal three;
            database.PealsDatabase().AddObject(three);
            Peal four;
            database.PealsDatabase().AddObject(four);

            std::set<Duco::ObjectId> foundIds;
            Duco::TSearchValidObject* newValidation = new Duco::TSearchValidObject(true);
            search.AddValidationArgument(newValidation);
            search.Search(*this, foundIds, TObjectType::EPeal);
            Assert::AreEqual((size_t)4, foundIds.size());
        }

        TEST_METHOD(DatabaseSearchCallbacks_RingersSearchAndValidatePeals)
        {
            Duco::RingingDatabase database;
            DatabaseSearch search(database);

            Ringer newRingerOne(KNoId, L"R Owen", L"Battye");
            database.RingersDatabase().AddObject(newRingerOne);
            Ringer newRingerTwo(KNoId, L"R O", L"Battye");
            database.RingersDatabase().AddObject(newRingerTwo);
            Ringer newRingerThree(KNoId, L"John", L"Smith");
            database.RingersDatabase().AddObject(newRingerThree);
            Ringer newRingerFour(KNoId, L"Clive", L"Smith");
            database.RingersDatabase().AddObject(newRingerFour);

            std::set<Duco::ObjectId> foundIds;
            Duco::TSearchFieldStringArgument* newStr = new Duco::TSearchFieldStringArgument(TSearchFieldId::ESurname, L"Battye");
            search.AddArgument(newStr);
            Duco::TSearchValidObject* newValidation = new Duco::TSearchValidObject(true);
            search.AddValidationArgument(newValidation);
            search.Search(*this, foundIds, TObjectType::ERinger);
            Assert::AreEqual((size_t)2, foundIds.size());
        }

    private:
        int previousProgressPercentage;
    };
}
