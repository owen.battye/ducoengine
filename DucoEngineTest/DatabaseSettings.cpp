#include "CppUnitTest.h"
#include <RingingDatabase.h>
#include <ImportExportProgressCallback.h>
#include <DatabaseSettings.h>
#include <Tower.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(DatabaseSettings_UnitTests), ImportExportProgressCallback
    {
    public:
        //From ImportExportProgressCallback
        void InitialisingImportExport(bool internalising, unsigned int versionNumber, size_t totalNumberOfObjects)
        {

        }
        void ObjectProcessed(bool internalising, Duco::TObjectType objectType, Duco::ObjectId objectId, int totalPercentage, size_t numberInThisSubDatabase)
        {

        }
        void ImportExportComplete(bool internalising)
        {

        }
        void ImportExportFailed(bool internalising, int errorCode)
        {
            Assert::IsFalse(true);
        }

        TEST_METHOD(DatabaseSettings_Version33_MapBoxEnabled)
        {
            const std::string sampleDatabasename = "lundy_DBVer_33.duc";
            RingingDatabase database;

            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);

            Assert::IsFalse(database.Settings().SettingsChanged());
            Assert::IsTrue(database.Settings().UseMapBox());
            Assert::AreEqual(33U, database.DatabaseVersion());
            Assert::AreEqual(database.DatabaseVersion(), databaseVersionNumber);
            Assert::AreEqual(L"https://bb.ringingworld.co.uk", database.Settings().BellBoardURL().c_str());
            Assert::AreEqual(L"https://bb.ringingworld.co.uk", database.Settings().PreferedDownloadWebsiteUrl().c_str());

            Assert::AreEqual((size_t)1, database.NumberOfPictures());
        }

        TEST_METHOD(DatabaseSettings_Version38_MethodGridSettings)
        {
            const std::string sampleDatabasename = "Version38_Empty_GoogleMaps.duc";
            RingingDatabase database;

            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)38, databaseVersionNumber);

            Assert::IsFalse(database.Settings().ShowTreble());
            Assert::IsFalse(database.Settings().ShowGridLines());
            Assert::IsFalse(database.Settings().ShowFullGrid());
            Assert::IsFalse(database.Settings().ShowBobsAndSingles());
            Assert::IsTrue(DatabaseSettings::TLineType::ENormalLine == database.Settings().LineType());

            database.Settings().SetLineType(DatabaseSettings::TLineType::ELongLine);
            Assert::IsTrue(DatabaseSettings::TLineType::ELongLine == database.Settings().LineType());
            database.Settings().SetShowGridLines(true);
            database.Settings().SetShowBobsAndSingles(true);

            Assert::IsTrue(database.Settings().ShowGridLines());
            Assert::IsTrue(DatabaseSettings::TLineType::ENormalLine == database.Settings().LineType());
            Assert::IsTrue(database.Settings().ShowBobsAndSingles());
        }

        TEST_METHOD(DatabaseSettings_Version38_DownloadWebsite)
        {
            const std::string sampleDatabasename = "Version38_Empty_GoogleMaps.duc";
            RingingDatabase database;

            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)38, databaseVersionNumber);

            Assert::AreEqual(L"https://bb.ringingworld.co.uk", database.Settings().BellBoardURL().c_str());
            Assert::IsTrue(database.Settings().UseMapBox());

            database.Settings().ClearSettings(true);
            Assert::AreEqual(L"https://bb.ringingworld.co.uk", database.Settings().BellBoardURL().c_str());
            Assert::IsTrue(database.Settings().UseMapBox());
        }

        TEST_METHOD(DatabaseSettings_CheckDefaultSettingsForValidationCkecks_Version38)
        {
            const std::string sampleDatabasename = "Version38_Empty_GoogleMaps.duc";
            RingingDatabase database;

            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)38, databaseVersionNumber);

            Assert::AreEqual(L"https://bb.ringingworld.co.uk", database.Settings().BellBoardURL().c_str());
            Assert::IsFalse(database.Settings().ValidationCheckDisabled(EPealWarning_AssociationMissing, TObjectType::EPeal));
            Assert::IsFalse(database.Settings().ValidationCheckDisabled(EPealWarning_AssociationBlank, TObjectType::EPeal));
            Assert::IsFalse(database.Settings().ValidationCheckDisabled(EPealWarning_SeriesMissing, TObjectType::EPeal));
            Assert::IsFalse(database.Settings().ValidationCheckDisabled(EMethodWarning_MissingBobPlaceNotation, TObjectType::EMethod));
            Assert::IsFalse(database.Settings().ValidationCheckDisabled(EMethodWarning_MissingSinglePlaceNotation, TObjectType::EMethod));

            database.Settings().ClearSettings(true);
            Assert::AreEqual(L"https://bb.ringingworld.co.uk", database.Settings().BellBoardURL().c_str());
            Assert::IsFalse(database.Settings().ValidationCheckDisabled(EPealWarning_AssociationMissing, TObjectType::EPeal));
            Assert::IsFalse(database.Settings().ValidationCheckDisabled(EPealWarning_AssociationBlank, TObjectType::EPeal));
            Assert::IsFalse(database.Settings().ValidationCheckDisabled(EPealWarning_SeriesMissing, TObjectType::EPeal));
            Assert::IsFalse(database.Settings().ValidationCheckDisabled(EMethodWarning_MissingBobPlaceNotation, TObjectType::EMethod));
            Assert::IsFalse(database.Settings().ValidationCheckDisabled(EMethodWarning_MissingSinglePlaceNotation, TObjectType::EMethod));
        }

        TEST_METHOD(DatabaseSettings_CheckDefaultSettingsForValidationChecks_LatestDBVersion)
        {
            RingingDatabase database;

            Assert::IsFalse(database.Settings().ValidationCheckDisabled(EPealWarning_AssociationMissing, TObjectType::EPeal));
            Assert::IsFalse(database.Settings().ValidationCheckDisabled(EPealWarning_AssociationBlank, TObjectType::EPeal));
            Assert::IsFalse(database.Settings().ValidationCheckDisabled(EPealWarning_SeriesMissing, TObjectType::EPeal));
            Assert::IsFalse(database.Settings().ValidationCheckDisabled(EMethodWarning_MissingBobPlaceNotation, TObjectType::EMethod));
            Assert::IsFalse(database.Settings().ValidationCheckDisabled(EMethodWarning_MissingSinglePlaceNotation, TObjectType::EMethod));
        }

        TEST_METHOD(DatabaseSettings_CheckDefaultSettingsForValidationChecks_WriteAndReadDatabase)
        {
            const std::string sampleDatabasename = "validationcheckstest.duc";
            {
                RingingDatabase database;
                unsigned int databaseVersionNumber = 0;
                database.Externalise(sampleDatabasename.c_str(), this);
            }
            {
                RingingDatabase database;
                unsigned int databaseVersionNumber = 0;
                database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
                Assert::AreEqual((unsigned int)52, databaseVersionNumber);

                Assert::IsFalse(database.Settings().ValidationCheckDisabled(EPealWarning_AssociationMissing, TObjectType::EPeal));
                Assert::IsFalse(database.Settings().ValidationCheckDisabled(EPealWarning_AssociationBlank, TObjectType::EPeal));
                Assert::IsFalse(database.Settings().ValidationCheckDisabled(EPealWarning_SeriesMissing, TObjectType::EPeal));
                Assert::IsFalse(database.Settings().ValidationCheckDisabled(EMethodWarning_MissingBobPlaceNotation, TObjectType::EMethod));
                Assert::IsFalse(database.Settings().ValidationCheckDisabled(EMethodWarning_MissingSinglePlaceNotation, TObjectType::EMethod));

                Assert::IsFalse(database.Settings().ValidationCheckDisabled(EMethod_OrderNameMissing, TObjectType::EMethod));
                Assert::IsFalse(database.Settings().ValidationCheckDisabled(ETower_TowerNameInvalid, TObjectType::ETower));
            }
        }
    };
}
