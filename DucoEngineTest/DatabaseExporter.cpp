#include "CppUnitTest.h"
#include <Peal.h>
#include <PealDatabase.h>
#include <RingingDatabase.h>
#include "ToString.h"
#include <ImportExportProgressCallback.h>
#include <RenumberProgressCallback.h>
#include <DatabaseExporter.h>
#include <StatisticFilters.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(DatabaseExporter_UnitTests), ImportExportProgressCallback, RenumberProgressCallback
    {
    public:
        //From ImportExportProgressCallback
        void InitialisingImportExport(bool internalising, unsigned int versionNumber, size_t totalNumberOfObjects)
        {

        }
        void ObjectProcessed(bool internalising, Duco::TObjectType objectType, Duco::ObjectId objectId, int totalPercentage, size_t numberInThisSubDatabase)
        {

        }
        void ImportExportComplete(bool internalising)
        {

        }
        void ImportExportFailed(bool internalising, int errorCode)
        {
            Assert::IsFalse(true);
        }
        // from RenumberProgressCallback
        void RenumberInitialised(RenumberProgressCallback::TRenumberStage newStage)
        {

        }
        void RenumberStep(size_t objectId, size_t total)
        {

        }
        void RenumberComplete()
        {

        }
        BEGIN_TEST_METHOD_ATTRIBUTE(DatabaseExporter_ExportAndCheck)
            TEST_PRIORITY(2)
        END_TEST_METHOD_ATTRIBUTE()
        TEST_METHOD(DatabaseExporter_ExportAndCheck)
        {
            const std::string inputFilename = "dordrecht_forexporttest.duc";
            const std::string outputFilename = "dordrecht_forexporttest_output.duc";

            {
                RingingDatabase database;
                database.ClearDatabase(false, true);

                unsigned int databaseVersion = 0;
                database.Internalise(inputFilename.c_str(), this, databaseVersion);

                size_t pealsWithErrors = database.NumberOfObjectsWithErrors(false, TObjectType::EPeal);
                Assert::AreEqual((size_t)3, pealsWithErrors);

                std::set<Duco::ObjectId> pealsToExport;

                const Duco::Peal* const pealOne = database.PealsDatabase().FindPeal(database.PealsDatabase().FindPeal(PerformanceDate(2018, 9, 16)));
                CheckPealOne(*pealOne, database);
                pealsToExport.insert(pealOne->Id());

                const Duco::Peal* const pealTwo = database.PealsDatabase().FindPeal(database.PealsDatabase().FindPeal(PerformanceDate(2015, 7, 4)));
                CheckPealTwo(*pealTwo, database);
                pealsToExport.insert(pealTwo->Id());

                DatabaseExporter exporter(database);
                Assert::IsTrue(exporter.AddAllObjectsForPeals(pealsToExport));
                Assert::AreEqual((size_t)2, exporter.NumberOfPealsToImport());
                Assert::AreEqual((size_t)20, exporter.TotalNumberOfObjectsToImport());

                Assert::IsTrue(exporter.Export(outputFilename.c_str(), this));
            }
            {
                RingingDatabase rereadDatabase;
                Duco::StatisticFilters filters(rereadDatabase);

                unsigned int databaseVersion = 0;
                rereadDatabase.Internalise(outputFilename.c_str(), this, databaseVersion);

                size_t pealsWithErrors = rereadDatabase.NumberOfObjectsWithErrors(false, TObjectType::EPeal);
                Assert::AreEqual((size_t)0, pealsWithErrors);

                const Duco::Peal* const pealOneFromExport = rereadDatabase.PealsDatabase().FindPeal(rereadDatabase.PealsDatabase().FindPeal(PerformanceDate(2018, 9, 16)));
                CheckPealOne(*pealOneFromExport, rereadDatabase);
                const Duco::Peal* const pealTwoFromExport = rereadDatabase.PealsDatabase().FindPeal(rereadDatabase.PealsDatabase().FindPeal(PerformanceDate(2015, 7, 4)));
                CheckPealTwo(*pealTwoFromExport, rereadDatabase);

                rereadDatabase.RebuildDatabase(this);
                Assert::AreEqual((size_t)2, rereadDatabase.NumberOfAssociations());
                Assert::AreEqual((size_t)1, rereadDatabase.NumberOfTowers());
                Assert::AreEqual((size_t)2, rereadDatabase.NumberOfMethods());
                Assert::AreEqual((size_t)0, rereadDatabase.NumberOfMethodSeries());
                Assert::AreEqual((size_t)2, rereadDatabase.PerformanceInfo(filters).TotalPeals());
                Assert::AreEqual((size_t)0, rereadDatabase.NumberOfPictures());
                Assert::AreEqual((size_t)13, rereadDatabase.NumberOfRingers());
                Assert::AreEqual((size_t)0, rereadDatabase.NumberOfObjectsWithErrors(false, TObjectType::EPeal));
                pealsWithErrors = rereadDatabase.NumberOfObjectsWithErrors(false, TObjectType::EPeal);
                Assert::AreEqual((size_t)0, pealsWithErrors);
                const Duco::Peal* const pealOneFromExportAfterRebuild = rereadDatabase.PealsDatabase().FindPeal(rereadDatabase.PealsDatabase().FindPeal(PerformanceDate(2018, 9, 16)));
                CheckPealOne(*pealOneFromExportAfterRebuild, rereadDatabase);
                const Duco::Peal* const pealTwoFromExportAfterRebuild = rereadDatabase.PealsDatabase().FindPeal(rereadDatabase.PealsDatabase().FindPeal(PerformanceDate(2015, 7, 4)));
                CheckPealTwo(*pealTwoFromExportAfterRebuild, rereadDatabase);
            }
        }

        void
        CheckPealOne(const Duco::Peal& thePeal, const Duco::RingingDatabase& database)
        {
            Assert::AreEqual(L"DORDRECHT SOCIETY OF CHANGE RINGERS", thePeal.AssociationName(database).c_str());
            Assert::AreEqual(L"DORDRECHT, South Holland. ('t Klockhuys)", thePeal.TowerName(database).c_str());
            Assert::AreEqual(L"16-SEP-2018", thePeal.Date().DateWithLongerMonth().c_str());
            Assert::AreEqual(PerformanceTime(155), thePeal.Time());
            Assert::AreEqual((unsigned int)5088, thePeal.NoOfChanges());
            Assert::AreEqual(L"Uxbridge Surprise Major", thePeal.MethodName(database).c_str());
            Assert::AreEqual(L"Donald F Morrison (no 2729)", thePeal.Composer().c_str());
            Assert::AreEqual(L"", thePeal.MultiMethods().c_str());
            Assert::AreEqual(L"", thePeal.Notes().c_str());
            Assert::AreEqual(L"", thePeal.Footnotes().c_str());

            Assert::AreEqual(L"Furniss, Beverley J", thePeal.RingerName(1, false, database).c_str());
            Assert::AreEqual(L"Thurman, John M", thePeal.RingerName(2, false, database).c_str());
            Assert::AreEqual(L"Bright, Nicola D", thePeal.RingerName(3, false, database).c_str());
            Assert::AreEqual(L"Marchbank, Trevor W", thePeal.RingerName(4, false, database).c_str());
            Assert::AreEqual(L"Marchbank, Carol A", thePeal.RingerName(5, false, database).c_str());
            Assert::AreEqual(L"Furniss, Peter L", thePeal.RingerName(6, false, database).c_str());
            Assert::AreEqual(L"Bright, Martin J", thePeal.RingerName(7, false, database).c_str());
            Assert::AreEqual(L"Smith, Clive G", thePeal.RingerName(8, false, database).c_str());
            Assert::AreEqual(L"", thePeal.RingerName(9, false, database).c_str());
            Assert::AreEqual(L"", thePeal.RingerName(5, true, database).c_str());

            Assert::AreEqual(L"Bright, Martin J", thePeal.ConductorName(database).c_str());
        }

        void
        CheckPealTwo(const Duco::Peal& thePeal, const Duco::RingingDatabase& database)
        {
            Assert::AreEqual(L"CENTRAL EUROPEAN ASSOCIATION", thePeal.AssociationName(database).c_str());
            Assert::AreEqual(L"DORDRECHT, South Holland. ('t Klockhuys)", thePeal.TowerName(database).c_str());
            Assert::AreEqual(L" 4-JUL-2015", thePeal.Date().DateWithLongerMonth().c_str());
            Assert::AreEqual(PerformanceTime(150), thePeal.Time());
            Assert::AreEqual((unsigned int)5088, thePeal.NoOfChanges());
            Assert::AreEqual(L"Cornwall Surprise Major", thePeal.MethodName(database).c_str());
            Assert::AreEqual(L"Donald F Morrison (no 1170)", thePeal.Composer().c_str());
            Assert::AreEqual(L"", thePeal.MultiMethods().c_str());
            Assert::AreEqual(L"", thePeal.Notes().c_str());

            Assert::AreEqual(L"Gardner, Geoffrey R", thePeal.RingerName(1, false, database).c_str());
            Assert::AreEqual(L"Bright, Nicola D", thePeal.RingerName(2, false, database).c_str());
            Assert::AreEqual(L"Smith, Anthony H", thePeal.RingerName(3, false, database).c_str());
            Assert::AreEqual(L"de Kok, Paul M", thePeal.RingerName(4, false, database).c_str());
            Assert::AreEqual(L"de Kok, Harm Jan A", thePeal.RingerName(5, false, database).c_str());
            Assert::AreEqual(L"Smith, Clive G", thePeal.RingerName(6, false, database).c_str());
            Assert::AreEqual(L"Apter, Susan L", thePeal.RingerName(7, false, database).c_str());
            Assert::AreEqual(L"Thurman, John M", thePeal.RingerName(8, false, database).c_str());
            Assert::AreEqual(L"", thePeal.RingerName(9, false, database).c_str());
            Assert::AreEqual(L"", thePeal.RingerName(5, true, database).c_str());

            Assert::AreEqual(L"With best wishes to Dominee M Den Dekker for his retirement, after having served the congregation of the Grote Kerk in Dordrecht for almost 22 years.", thePeal.Footnotes().c_str());

            Assert::AreEqual(L"Thurman, John M", thePeal.ConductorName(database).c_str());
        }

    };
}
