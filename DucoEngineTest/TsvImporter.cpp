#include "CppUnitTest.h"
#include <PealbaseTsvImporter.h>

#include <DatabaseSettings.h>
#include <MethodDatabase.h>
#include <Method.h>
#include <Peal.h>
#include <PealDatabase.h>
#include <ProgressCallback.h>
#include <RenumberProgressCallback.h>
#include <Ringer.h>
#include <RingerDatabase.h>
#include <RingingDatabase.h>
#include <Tower.h>
#include <TowerDatabase.h>
#include "ToString.h"
#include <StatisticFilters.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(PealBaseTsvImporter_UnitTests), Duco::ProgressCallback, Duco::RenumberProgressCallback
    {
        Duco::RingingDatabase* database;
        Duco::PealBaseTsvImporter* importer;
        bool expectedError;
        bool completed;
        bool initialised;
        int previousPercent;
        int cancelWhenAbove;

    public:
        PealBaseTsvImporter_UnitTests()
        {
            expectedError = false;
            completed = false;
            initialised = false;
            cancelWhenAbove = 101;
            database = new Duco::RingingDatabase();
            importer = new Duco::PealBaseTsvImporter(*this, *database);
        }

        ~PealBaseTsvImporter_UnitTests()
        {
            delete importer;
            delete database;
        }

        void Initialised()
        {
            initialised = true;
            previousPercent = 0;
        }

        void Step(int progressPercent)
        {
            Assert::IsTrue(initialised);
            Assert::IsTrue(progressPercent < 100);
            Assert::IsTrue(previousPercent <= progressPercent);
            previousPercent = progressPercent;
            if (progressPercent > cancelWhenAbove)
            {
                importer->Cancel();
            }
        }

        void Complete(bool error)
        {
            Assert::AreEqual(expectedError, error);
            Assert::IsTrue(initialised);
            completed = true;
        }

        void RenumberInitialised(RenumberProgressCallback::TRenumberStage newStage)
        {

        }
        void RenumberStep(size_t objectId, size_t total)
        {

        }
        void RenumberComplete()
        {

        }

        void FindCheckRinger(const Duco::Peal& thePeal, unsigned int bellNo, bool strapper, const std::wstring& expectedName) const
        {
            Duco::ObjectId ringerId;
            Assert::IsTrue(database->RingersDatabase().FindRinger(thePeal.RingerId(bellNo, strapper, ringerId)));
            const Ringer* const pealRinger = database->RingersDatabase().FindRinger(ringerId);
            Assert::AreEqual(expectedName, pealRinger->FullName(database->Settings().LastNameFirst()));
        }

        void CheckPeals() const
        {
            {
                PerformanceDate peal18Date(1965, 4, 13);
                ObjectId peal18Id = database->PealsDatabase().FindPeal(peal18Date);
                const Peal* const peal18 = database->PealsDatabase().FindPeal(peal18Id);
                Assert::IsNotNull(peal18);
                Assert::AreEqual(L"DURHAM & NEWCASTLE DIOCESAN ASSOCIATION", peal18->AssociationName(*database).c_str());
                Assert::IsTrue(peal18->Handbell());
                const Tower* const peal18Tower11 = database->TowersDatabase().FindTower(peal18->TowerId());
                Assert::AreEqual(L"22 Castleton Grove", peal18Tower11->Name().c_str());
                Assert::AreEqual(L"Newcastle Upon Tyne", peal18Tower11->City().c_str());
                Assert::AreEqual(L"", peal18Tower11->County().c_str());
                Assert::AreEqual(PerformanceTime(123), peal18->Time());
                Assert::AreEqual((unsigned int)5040, peal18->NoOfChanges());
                const Method* const peal18Method = database->MethodsDatabase().FindMethod(peal18->MethodId());
                Assert::AreEqual(L"Plain Bob Minor", peal18Method->FullName(database->MethodsDatabase()).c_str());
                Assert::AreEqual(L"", peal18->Composer().c_str());

                FindCheckRinger(*peal18, 1, false, L"Askew, Roger H E");
                FindCheckRinger(*peal18, 2, false, L"Yeates, Rodney A");
                FindCheckRinger(*peal18, 3, false, L"Craddock, Andrew G");
                Assert::AreEqual(L"First peal: 1-2.", peal18->Footnotes().c_str());
                Assert::AreEqual(L"Craddock, Andrew G", peal18->ConductorName(*database).c_str());
            }
            {
                PerformanceDate peal34Date(1965, 9, 10);
                ObjectId peal34Id = database->PealsDatabase().FindPeal(peal34Date);
                const Peal* const peal34 = database->PealsDatabase().FindPeal(peal34Id);
                Assert::IsNotNull(peal34);
                Assert::AreEqual(L"ELY DIOCESAN ASSOCIATION", peal34->AssociationName(*database).c_str());
                Assert::IsFalse(peal34->Handbell());
                const Tower* const peal34Tower20 = database->TowersDatabase().FindTower(peal34->TowerId());
                Assert::AreEqual(L"St Mary", peal34Tower20->Name().c_str());
                Assert::AreEqual(L"Higham Ferrers", peal34Tower20->City().c_str());
                Assert::AreEqual(L"Northamptonshire", peal34Tower20->County().c_str());
                Assert::AreEqual(PerformanceTime(181), peal34->Time());
                Assert::AreEqual((unsigned int)5056, peal34->NoOfChanges());
                const Method* const peal34Method = database->MethodsDatabase().FindMethod(peal34->MethodId());
                Assert::AreEqual(L"Plain Bob Major", peal34Method->FullName(database->MethodsDatabase()).c_str());
                Assert::AreEqual(L"John R Pritchard", peal34->Composer().c_str());

                FindCheckRinger(*peal34, 1, false, L"Elmes, Mary E");
                FindCheckRinger(*peal34, 2, false, L"Woollven, Peter");
                FindCheckRinger(*peal34, 3, false, L"Bonham, George E");
                FindCheckRinger(*peal34, 4, false, L"Craddock, Andrew G");
                FindCheckRinger(*peal34, 5, false, L"Elmes, Graham W");
                FindCheckRinger(*peal34, 6, false, L"Sansom, Anthony D");
                FindCheckRinger(*peal34, 7, false, L"Dove, C Barrie");
                FindCheckRinger(*peal34, 8, false, L"Mehew, Philip");
                Assert::AreEqual(L"", peal34->Footnotes().c_str());
                Assert::AreEqual(L"Mehew, Philip", peal34->ConductorName(*database).c_str());
            }
            {
                PerformanceDate peal37Date(1965, 12, 04);
                ObjectId peal37Id = database->PealsDatabase().FindPeal(peal37Date);
                const Peal* const peal37 = database->PealsDatabase().FindPeal(peal37Id);
                Assert::IsNotNull(peal37);
                Assert::AreEqual(L"DURHAM & NEWCASTLE DIOCESAN ASSOCIATION", peal37->AssociationName(*database).c_str());
                Assert::IsFalse(peal37->Handbell());
                const Tower* const peal37Tower = database->TowersDatabase().FindTower(peal37->TowerId());
                Assert::AreEqual(L"S John The Evangelist", peal37Tower->Name().c_str());
                Assert::AreEqual(L"Darlington", peal37Tower->City().c_str());
                Assert::AreEqual(L"Durham", peal37Tower->County().c_str());
                Assert::AreEqual(PerformanceTime(160), peal37->Time());
                Assert::AreEqual((unsigned int)5056, peal37->NoOfChanges());
                const Method* const peal37Method = database->MethodsDatabase().FindMethod(peal37->MethodId());
                Assert::AreEqual(L"Yorkshire Surprise Major", peal37Method->FullName(database->MethodsDatabase()).c_str());
                Assert::AreEqual(L"Not Given", peal37->Composer().c_str());

                FindCheckRinger(*peal37, 1, false, L"Warwick, Janet");
                FindCheckRinger(*peal37, 2, false, L"Davidson, William");
                FindCheckRinger(*peal37, 3, false, L"Cooke, Janet");
                FindCheckRinger(*peal37, 4, false, L"Hodgson, Fred");
                FindCheckRinger(*peal37, 5, false, L"Cooke, Thomas");
                FindCheckRinger(*peal37, 6, false, L"Craddock, Andrew G");
                FindCheckRinger(*peal37, 7, false, L"Crowther, James");
                FindCheckRinger(*peal37, 8, false, L"Maughan, Michael");
                Assert::AreEqual(L"First peal in the method: 3. 100th peal of Surprise: 8. Rung as a birthday compliment to Stephen Thompson, tower captain at St Mary's Whickham, County Durham.", peal37->Footnotes().c_str());
                Assert::AreEqual(L"Maughan, Michael", peal37->ConductorName(*database).c_str());
            }
        }

        TEST_METHOD(PealBaseTsvImporter_Import_NonExistingFile)
        {
            expectedError = true;
            Assert::IsFalse(importer->Import("wibble.tsv"));
            Assert::IsTrue(completed);
            Assert::AreEqual(0, previousPercent);
        }

        TEST_METHOD(PealBaseTsvImporter_Import_Cancel)
        {
            cancelWhenAbove = 50;
            Assert::IsFalse(importer->Import("Andrew G Craddock peals.tsv"));
            Duco::StatisticFilters filters(*database);
            Assert::IsFalse(completed);
            Assert::IsTrue(previousPercent < 100);
            Assert::IsTrue(previousPercent > cancelWhenAbove);
            Assert::IsTrue(database->PerformanceInfo(filters).TotalPeals() < 290);
        }

        BEGIN_TEST_METHOD_ATTRIBUTE(PealBaseTsvImporter_ImportAndrewCraddock)
            TEST_PRIORITY(2)
        END_TEST_METHOD_ATTRIBUTE()
        TEST_METHOD(PealBaseTsvImporter_ImportAndrewCraddock)
        {
            Duco::StatisticFilters filters(*database);
            Assert::IsTrue(importer->Import("Andrew G Craddock peals.tsv"));
            Assert::IsTrue(completed);
            Assert::IsTrue(previousPercent >= 99);
            Assert::AreEqual((size_t)292, database->PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)12, database->NumberOfAssociations());
            PealLengthInfo withdrawnCount;

            filters.SetIncludeHandBell(false);
            Assert::AreEqual((size_t)147, database->PealsDatabase().PerformanceInfo(filters).TotalPeals());
            filters.SetIncludeHandBell(true);
            filters.SetIncludeTowerBell(false);
            Assert::AreEqual((size_t)145, database->PealsDatabase().PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)81, database->NumberOfMethods()); // Counted in file manually
            Assert::AreEqual((size_t)360, database->NumberOfRingers());
            Assert::AreEqual((size_t)90, database->NumberOfTowers());

            CheckPeals();

            Assert::IsTrue(database->RebuildDatabase(this));

            CheckPeals();
        }
    };
}
