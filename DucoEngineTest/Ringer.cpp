#include "CppUnitTest.h"
#include <RingingDatabase.h>
#include <Ringer.h>
#include <RingerDatabase.h>
#include <stdexcept>
#include <StatisticFilters.h>
#include "ToString.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(Ringer_UnitTests)
    {
    public:
        TEST_METHOD(Ringer_DatabaseReader_MissingFile)
        {
            RingingDatabase database;
            unsigned int databaseVersion = 0;
            auto func = [&] {
                database.Internalise("DoesntExist.duc", NULL, databaseVersion);
            };
            Assert::ExpectException<std::invalid_argument>(func, L"File doesn't exist");
        }

        TEST_METHOD(Ringer_DefaultConstructor)
        {
            RingingDatabase database;

            Ringer theRinger;
            Assert::AreEqual(Duco::ObjectId(), theRinger.Id());
            Assert::AreEqual(L"", theRinger.Forename().c_str());
            Assert::AreEqual(L"", theRinger.Surname().c_str());
            Assert::IsFalse(theRinger.Male());
            Assert::IsFalse(theRinger.Female());
            Assert::IsFalse(theRinger.NonHuman());
            Assert::IsFalse(theRinger.Deceased());
            Duco::PerformanceDate dateOfDeath;
            Assert::IsFalse(theRinger.DateOfDeath(dateOfDeath));
            Assert::IsFalse(dateOfDeath.Valid());
            Assert::IsFalse(theRinger.Valid(database, true, true));
        }

        TEST_METHOD(Ringer_NonHuman)
        {
            RingingDatabase database;

            Ringer theRinger;
            theRinger.SetNonHuman(true);
            Assert::AreEqual(Duco::ObjectId(), theRinger.Id());
            Assert::AreEqual(L"", theRinger.Forename().c_str());
            Assert::AreEqual(L"", theRinger.Surname().c_str());
            Assert::IsFalse(theRinger.Male());
            Assert::IsFalse(theRinger.Female());
            Assert::IsTrue(theRinger.NonHuman());
            Assert::IsFalse(theRinger.Valid(database, true, true));
        }

        TEST_METHOD(Ringer_DatabaseReader_SingleRinger)
        {
            RingingDatabase database;
            Duco::StatisticFilters filters(database);
            unsigned int databaseVersion = 0;
            database.Internalise("Single_ringer.duc", NULL, databaseVersion);

            // Check counts of all objects
            Assert::AreEqual((size_t)0, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)1, database.NumberOfRingers());
            Assert::AreEqual((size_t)0, database.NumberOfTowers());
            size_t noOfLinkedTowers = 0;
            Assert::AreEqual((size_t)0, database.NumberOfActiveTowers(noOfLinkedTowers));
            Assert::AreEqual((size_t)0, noOfLinkedTowers);
            Assert::AreEqual((size_t)0, database.NumberOfRemovedTowers());
            Assert::AreEqual((size_t)0, database.NumberOfMethods());
            Assert::AreEqual((size_t)0, database.NumberOfMethodSeries());
            Assert::AreEqual((size_t)0, database.NumberOfCompositions());
            Assert::AreEqual((size_t)0, database.NumberOfAssociations());

            Assert::AreEqual(L"Battye, Robert Owen", database.RingersDatabase().RingerFullName(1, database.Settings()).c_str());
            Assert::AreEqual(L"", database.RingersDatabase().RingerFullName(0, database.Settings()).c_str());

            // Check single ringer is correct
            const Ringer* const theRinger = database.RingersDatabase().FindRinger(1);
            Assert::IsTrue(theRinger->Male());
            Assert::IsFalse(theRinger->Female());
            Assert::AreEqual(L"Robert Owen", theRinger->Forename().c_str());
            Assert::AreEqual(L"Battye", theRinger->Surname().c_str());
            Assert::AreEqual(L"Robert Owen Battye", theRinger->OriginalName(false).c_str());
            Assert::AreEqual(L"Battye, Robert Owen", theRinger->OriginalName(true).c_str());
            Assert::AreEqual(L"Robert Owen Battye", theRinger->FullName(false).c_str());
            Assert::AreEqual(L"Battye, Robert Owen", theRinger->FullName(true).c_str());
            Assert::AreEqual((size_t)0, theRinger->NoOfNameChanges());
        }

        TEST_METHOD(Ringer_CheckAkas)
        {
            Ringer theRinger(1, L"Fred", L"Bloggs");
            Assert::AreEqual(L"Fred", theRinger.Forename().c_str());
            Assert::AreEqual(L"Bloggs", theRinger.Surname().c_str());
            Assert::AreEqual(L"Fred Bloggs", theRinger.FullName(false).c_str());
            Assert::IsFalse(theRinger.Male());
            Assert::IsFalse(theRinger.Female());
            Assert::AreEqual((size_t)0, theRinger.NoOfNameChanges());

            theRinger.SetMale();
            Assert::IsTrue(theRinger.Male());
            Assert::IsFalse(theRinger.Female());
            Assert::IsFalse(theRinger.NonHuman());

            PerformanceDate dateChanged(2010, 05, 01);
            theRinger.AddNameChange(L"Frida", L"Smith", dateChanged);
            theRinger.SetFemale();
            Assert::AreEqual(L"Frida Smith", theRinger.FullName(false).c_str());
            Assert::IsFalse(theRinger.Male());
            Assert::IsTrue(theRinger.Female());
            Assert::AreEqual((size_t)1, theRinger.NoOfNameChanges());

            PerformanceDate dateToTest(2010, 04, 30);
            Assert::AreEqual(L"Bloggs, Fred", theRinger.FullName(dateToTest, true).c_str());

            Ringer theNewestRinger(2, L"James", L"Brown");
            PerformanceDate newestDateChanged(2015, 01, 01);
            theRinger.AddNameChange(theNewestRinger, newestDateChanged);
            Assert::AreEqual((size_t)2, theRinger.NoOfNameChanges());
            Assert::AreEqual(L"Bloggs, Fred", theRinger.FullName(dateToTest, true).c_str());
            Assert::AreEqual(L"Brown, James", theRinger.FullName(newestDateChanged, true).c_str());
            Assert::IsFalse( theRinger.NonHuman());
        }

        TEST_METHOD(Ringer_CloneConstructor)
        {
            Ringer theRinger(13, L"Fred", L"Bloggs");
            theRinger.AddNameChange(L"Frida", L"Smith", PerformanceDate(1965, 8, 11));
            theRinger.SetNonHuman(true);
            theRinger.SetDeceased(true);

            Ringer theRingerClone(theRinger);
            Assert::AreEqual(L"Fred", theRingerClone.Forename().c_str());
            Assert::AreEqual(L"Bloggs", theRingerClone.Surname().c_str());
            Assert::AreEqual(L"Frida Smith", theRingerClone.FullName(false).c_str());
            Assert::AreEqual(Duco::ObjectId(13), theRingerClone.Id());

            Assert::AreEqual(L"Fred Bloggs", theRingerClone.FullName(PerformanceDate(1965, 8, 10), false).c_str());
            Assert::IsFalse(theRinger != theRingerClone);
            Assert::IsTrue(theRinger == theRingerClone);
            Assert::AreEqual((size_t)1, theRingerClone.NoOfNameChanges());
            Assert::AreEqual(L"Fred", theRingerClone.Forename().c_str());
            Assert::AreEqual(L"Bloggs", theRingerClone.Surname().c_str());
            Assert::IsTrue(theRingerClone.NonHuman());
            Assert::IsTrue(theRingerClone.Deceased());
        }

        TEST_METHOD(Ringer_CloneConstructorWithId)
        {
            Ringer theRinger(13, L"Fred", L"Bloggs");
            theRinger.AddNameChange(L"Frida", L"Smith", PerformanceDate(1965, 8, 11));
            theRinger.SetNonHuman(true);
            theRinger.SetDeceased(true);

            Ringer theRingerClone(2, theRinger);
            Assert::AreEqual(L"Fred", theRingerClone.Forename().c_str());
            Assert::AreEqual(L"Bloggs", theRingerClone.Surname().c_str());
            Assert::AreEqual(L"Frida Smith", theRingerClone.FullName(false).c_str());
            Assert::AreEqual(Duco::ObjectId(2), theRingerClone.Id());

            Assert::AreEqual(L"Fred Bloggs", theRingerClone.FullName(PerformanceDate(1965, 8, 10), false).c_str());
            Assert::IsTrue(theRinger != theRingerClone);
            Assert::IsFalse(theRinger == theRingerClone);
            Assert::AreEqual((size_t)1, theRingerClone.NoOfNameChanges());
            Assert::AreEqual(L"Fred", theRingerClone.Forename().c_str());
            Assert::AreEqual(L"Bloggs", theRingerClone.Surname().c_str());
            Assert::IsTrue(theRingerClone.NonHuman());
            Assert::IsTrue(theRingerClone.Deceased());
        }

        TEST_METHOD(Ringer_ChangeForename)
        {
            Ringer theRinger(13, L"Jennie", L"Paul");
            theRinger.SetForename(0, L"Claire");
            Assert::AreEqual(L"Claire Paul", theRinger.FullName(false).c_str());
        }

        TEST_METHOD(Ringer_ChangeSurname)
        {
            Ringer theRinger(13, L"Jennie", L"Paul");
            theRinger.SetSurname(0, L"Higson");
            Assert::AreEqual(L"Jennie Higson", theRinger.FullName(false).c_str());
        }

        TEST_METHOD(Ringer_InsertNameChange)
        {
            Ringer theRinger(13, L"One", L"One");
            theRinger.AddNameChange(L"Two", L"Two", PerformanceDate(2010, 01, 01));
            Assert::AreEqual((size_t)1, theRinger.NoOfNameChanges());

            theRinger.InsertNameChange(L"Four", L"Four", PerformanceDate(2012, 01, 01));
            Assert::AreEqual((size_t)2, theRinger.NoOfNameChanges());

            theRinger.InsertNameChange(L"Three", L"Three", PerformanceDate(2011, 01, 01));
            Assert::AreEqual((size_t)3, theRinger.NoOfNameChanges());

            Assert::AreEqual(L"Four Four", theRinger.FullName(false).c_str());

            theRinger.DeleteNameChange(2);
            Assert::AreEqual(L"Four Four", theRinger.FullName(false).c_str());
        }

        TEST_METHOD(Ringer_SetInvalidDateOfDeath)
        {
            Ringer theRinger(13, L"One", L"One");
            Assert::IsFalse(theRinger.Deceased());

            Duco::PerformanceDate dateOfDeath(2010, 01, 21);
            theRinger.SetDateOfDeath(dateOfDeath);

            Duco::PerformanceDate retrievedDate;
            Assert::IsTrue(theRinger.Deceased());

            Assert::IsTrue(theRinger.DateOfDeath(retrievedDate));
            Assert::AreEqual(dateOfDeath, retrievedDate);
        }

        TEST_METHOD(Ringer_DeceasedWithoutDate)
        {
            Ringer theRinger(13, L"One", L"One");
            theRinger.SetDeceased(true);

            Duco::PerformanceDate retrievedDate;
            Assert::IsTrue(theRinger.Deceased());

            Assert::IsFalse(theRinger.DateOfDeath(retrievedDate));
            Assert::IsFalse(retrievedDate.Valid());
        }

    };
}

