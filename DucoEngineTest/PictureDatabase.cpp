#include "CppUnitTest.h"
#include <Composition.h>
#include <CompositionDatabase.h>
#include <DucoConfiguration.h>
#include <Method.h>
#include <MethodDatabase.h>
#include <Peal.h>
#include <PealDatabase.h>
#include <Picture.h>
#include <PictureDatabase.h>
#include <Tower.h>
#include <TowerDatabase.h>
#include <RingingDatabase.h>
#include "ToString.h"
#include "RenumberProgressCallback.h"
#include "ImportExportProgressCallback.h"
#include <fstream>
#include <DatabaseSearch.h>
#include <SearchFieldBoolArgument.h>
#include "ProgressCallback.h"
#include <StatisticFilters.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(PictureDatabase_UnitTests), Duco::RenumberProgressCallback, Duco::ImportExportProgressCallback, Duco::ProgressCallback
    {
    public:
        // from RenumberProgressCallback
        void RenumberInitialised(RenumberProgressCallback::TRenumberStage newStage)
        {
        }

        void RenumberStep(size_t objectId, size_t total)
        {

        }
        void RenumberComplete()
        {

        }

        //from ImportExportProgressCallback
        void InitialisingImportExport(bool internalising, unsigned int versionNumber, size_t totalNumberOfObjects)
        {
        }
        void ObjectProcessed(bool internalising, Duco::TObjectType objectType, Duco::ObjectId objectId, int totalPercentage, size_t numberInThisSubDatabase)
        {
        }
        void ImportExportComplete(bool internalising)
        {
        }
        void ImportExportFailed(bool internalising, int errorCode)
        {
            Assert::IsTrue(false);
        }

        // FromProgressCallback
        void Initialised()
        {

        }
        void Step(int progressPercent)
        {

        }
        void Complete(bool error)
        {
            Assert::IsFalse(error);
        }

        TEST_METHOD(PictureDatabase_Insert)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Picture firstPicture;
            firstPicture.ReplacePicture("bellboard.jpg");
            Assert::AreEqual(".jpeg", firstPicture.GetFileExtension().c_str());
            Assert::IsTrue(database.PicturesDatabase().AddObject(firstPicture).ValidId());
            Assert::AreEqual((size_t)1, database.NumberOfPictures());

            Picture secondPicture ("bothered2.png");
            Assert::AreEqual(".png", secondPicture.GetFileExtension().c_str());
            Assert::IsTrue(database.PicturesDatabase().AddObject(secondPicture).ValidId());

            Assert::AreEqual(TObjectType::EPicture, secondPicture.ObjectType());
            Assert::IsFalse(firstPicture.Duplicate(secondPicture, database));
            Assert::IsTrue(firstPicture == firstPicture);
            Assert::IsFalse(firstPicture == secondPicture);
            Assert::AreEqual((size_t)2, database.NumberOfPictures());
            Assert::AreEqual((unsigned long long)173275, database.PicturesDatabase().TotalSizeOfAllPictures());

            Assert::IsFalse(database.RebuildDatabase(this));
         }

        TEST_METHOD(PictureDatabase_Insert_Reindex)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);
            Assert::AreEqual((unsigned long long)0, database.PicturesDatabase().TotalSizeOfAllPictures());

            Picture firstPicture;
            firstPicture.ReplacePicture("bellboard.jpg");
            Assert::AreEqual(".jpeg", firstPicture.GetFileExtension().c_str());
            Assert::IsTrue(database.PicturesDatabase().AddObject(firstPicture).ValidId());
            Assert::AreEqual((size_t)1, database.NumberOfPictures());

            Picture secondPicture(3, firstPicture);
            secondPicture.ReplacePicture("bothered2.png");
            Assert::AreEqual(".png", secondPicture.GetFileExtension().c_str());
            Assert::IsTrue(database.PicturesDatabase().AddObject(secondPicture).ValidId());

            Assert::AreEqual((size_t)2, database.NumberOfPictures());
            Assert::IsTrue(database.PicturesDatabase().FindPicture(1, false) != NULL);
            Assert::IsTrue(database.PicturesDatabase().FindPicture(2, false) == NULL);
            Assert::IsTrue(database.PicturesDatabase().FindPicture(3, false) != NULL);

            Assert::IsTrue(database.RebuildDatabase(this));

            Assert::AreEqual((size_t)2, database.NumberOfPictures());
            Assert::IsTrue(database.PicturesDatabase().FindPicture(1, false) != NULL);
            Assert::IsTrue(database.PicturesDatabase().FindPicture(2, false) != NULL);
            Assert::IsTrue(database.PicturesDatabase().FindPicture(3, false) == NULL);
            Assert::AreEqual((unsigned long long)173275, database.PicturesDatabase().TotalSizeOfAllPictures());
         }

        TEST_METHOD(PictureDatabase_Insert_Reindex_RenumberInPeals)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Picture firstPicture;
            firstPicture.ReplacePicture("bellboard.jpg");
            Assert::AreEqual(".jpeg", firstPicture.GetFileExtension().c_str());
            Assert::IsTrue(database.PicturesDatabase().AddObject(firstPicture).ValidId());
            Assert::AreEqual((size_t)1, database.NumberOfPictures());

            Picture secondPicture(3, firstPicture);
            secondPicture.ReplacePicture("bothered2.png");
            Assert::AreEqual(".png", secondPicture.GetFileExtension().c_str());
            Assert::IsTrue(database.PicturesDatabase().AddObject(secondPicture).ValidId());

            Assert::AreEqual((size_t)2, database.NumberOfPictures());
            Assert::IsTrue(database.PicturesDatabase().FindPicture(1, false) != NULL);
            Assert::IsTrue(database.PicturesDatabase().FindPicture(2, false) == NULL);
            Assert::IsTrue(database.PicturesDatabase().FindPicture(3, false) != NULL);

            Peal pealOne;
            pealOne.SetPictureId(1);
            database.PealsDatabase().AddObject(pealOne);
            Peal pealTwo;
            pealTwo.SetPictureId(3);
            database.PealsDatabase().AddObject(pealTwo);
            Assert::AreEqual(ObjectId(1), database.PealsDatabase().FindPeal(1)->PictureId());
            Assert::AreEqual(ObjectId(3), database.PealsDatabase().FindPeal(2)->PictureId());

            Tower towerOne;
            towerOne.SetPictureId(3);
            database.TowersDatabase().AddObject(towerOne);
            Assert::AreEqual(ObjectId(3), database.TowersDatabase().FindTower(1)->PictureId());

            Assert::IsTrue(database.RebuildDatabase(this));

            Assert::AreEqual((size_t)2, database.NumberOfPictures());
            Assert::IsTrue(database.PicturesDatabase().FindPicture(1, false) != NULL);
            Assert::IsTrue(database.PicturesDatabase().FindPicture(2, false) != NULL);
            Assert::IsTrue(database.PicturesDatabase().FindPicture(3, false) == NULL);
            Assert::AreEqual(ObjectId(1), database.PealsDatabase().FindPeal(1)->PictureId());
            Assert::AreEqual(ObjectId(2), database.PealsDatabase().FindPeal(2)->PictureId());
            Assert::AreEqual(ObjectId(2), database.TowersDatabase().FindTower(1)->PictureId());
        }

        TEST_METHOD(PictureDatabase_Rebuild_PictureUsedMoreThanOnceInPeals)
        {
            RingingDatabase database;
            Duco::StatisticFilters filters(database);
            database.ClearDatabase(false, true);

            Picture firstPicture;
            firstPicture.ReplacePicture("bellboard.jpg");
            Assert::AreEqual(".jpeg", firstPicture.GetFileExtension().c_str());
            Assert::IsTrue(database.PicturesDatabase().AddObject(firstPicture).ValidId());
            Assert::AreEqual((size_t)1, database.NumberOfPictures());

            Picture secondPicture(2, firstPicture);
            secondPicture.ReplacePicture("bothered2.png");
            Assert::AreEqual(".png", secondPicture.GetFileExtension().c_str());
            Assert::IsTrue(database.PicturesDatabase().AddObject(secondPicture).ValidId());

            Picture fourthPicture(4, firstPicture);
            fourthPicture.ReplacePicture("Newtons_cradle_animation.gif");
            Assert::AreEqual(".gif", fourthPicture.GetFileExtension().c_str());
            Assert::IsTrue(database.PicturesDatabase().AddObject(fourthPicture).ValidId());

            Assert::IsTrue(database.PicturesDatabase().FindPicture(1, false) != NULL);
            Assert::IsTrue(database.PicturesDatabase().FindPicture(2, false) != NULL);
            Assert::IsTrue(database.PicturesDatabase().FindPicture(3, false) == NULL);
            Assert::IsTrue(database.PicturesDatabase().FindPicture(4, false) != NULL);

            Peal pealOne;
            pealOne.SetPictureId(1);
            database.PealsDatabase().AddObject(pealOne);
            Peal pealTwo;
            pealTwo.SetPictureId(2);
            database.PealsDatabase().AddObject(pealTwo);
            Peal pealThree;
            pealThree.SetPictureId(1);
            database.PealsDatabase().AddObject(pealThree);
            Peal pealFour;
            pealFour.SetPictureId(4);
            database.PealsDatabase().AddObject(pealFour);

            Assert::AreEqual(ObjectId(1), database.PealsDatabase().FindPeal(1)->PictureId());
            Assert::AreEqual(ObjectId(2), database.PealsDatabase().FindPeal(2)->PictureId());
            Assert::AreEqual(ObjectId(1), database.PealsDatabase().FindPeal(3)->PictureId());
            Assert::AreEqual(ObjectId(4), database.PealsDatabase().FindPeal(4)->PictureId());

            Assert::AreEqual((size_t)4, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)3, database.NumberOfPictures());
            Assert::IsTrue(database.RebuildDatabase(this));

            Assert::AreEqual((size_t)4, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)3, database.NumberOfPictures());
            Assert::IsTrue(database.PicturesDatabase().FindPicture(1, false) != NULL);
            Assert::IsTrue(database.PicturesDatabase().FindPicture(2, false) != NULL);
            Assert::IsTrue(database.PicturesDatabase().FindPicture(3, false) != NULL);
            Assert::IsTrue(database.PicturesDatabase().FindPicture(4, false) == NULL);
            Assert::AreEqual(ObjectId(1), database.PealsDatabase().FindPeal(1)->PictureId());
            Assert::AreEqual(ObjectId(2), database.PealsDatabase().FindPeal(2)->PictureId());
            Assert::AreEqual(ObjectId(1), database.PealsDatabase().FindPeal(3)->PictureId());
            Assert::AreEqual(ObjectId(3), database.PealsDatabase().FindPeal(4)->PictureId());
        }

        TEST_METHOD(PictureDatabase_SaveAndRead)
        {
            RingingDatabase database;
            Duco::StatisticFilters filters(database);
            database.ClearDatabase(false, true);

            Picture firstPicture;
            firstPicture.ReplacePicture("bellboard.jpg");
            Assert::AreEqual(".jpeg", firstPicture.GetFileExtension().c_str());
            Assert::IsTrue(database.PicturesDatabase().AddObject(firstPicture).ValidId());
            Assert::AreEqual((size_t)1, database.NumberOfPictures());
            size_t picOneOriginalSize = firstPicture.GetPictureBuffer().size();

            Picture fourthPicture(4, firstPicture);
            fourthPicture.ReplacePicture("Newtons_cradle_animation.gif");
            Assert::AreEqual(".gif", fourthPicture.GetFileExtension().c_str());
            Assert::IsTrue(database.PicturesDatabase().AddObject(fourthPicture).ValidId());
            size_t picFourOriginalSize = fourthPicture.GetPictureBuffer().size();

            Assert::IsTrue(database.PicturesDatabase().FindPicture(1, false) != NULL);
            Assert::IsTrue(database.PicturesDatabase().FindPicture(2, false) == NULL);
            Assert::IsTrue(database.PicturesDatabase().FindPicture(3, false) == NULL);
            Assert::IsTrue(database.PicturesDatabase().FindPicture(4, false) != NULL);

            Peal pealOne;
            pealOne.SetPictureId(1);
            database.PealsDatabase().AddObject(pealOne);
            Peal pealTwo;
            pealTwo.SetPictureId(4);
            database.PealsDatabase().AddObject(pealTwo);

            Assert::AreEqual(ObjectId(1), database.PealsDatabase().FindPeal(1)->PictureId());
            Assert::AreEqual(ObjectId(4), database.PealsDatabase().FindPeal(2)->PictureId());
            Assert::AreEqual((size_t)2, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)2, database.NumberOfPictures());

            std::string databaseName = "picture_test.duc";
            Assert::IsTrue(database.Externalise(databaseName.c_str(), this));

            unsigned int dbVersion = 0;
            RingingDatabase readDatabase;
            readDatabase.Internalise(databaseName.c_str(), this, dbVersion);
            Assert::IsTrue(dbVersion != 0);
            Assert::AreEqual((size_t)2, readDatabase.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)2, readDatabase.NumberOfPictures());
            Assert::AreEqual(ObjectId(1), readDatabase.PealsDatabase().FindPeal(1)->PictureId());
            Assert::AreEqual(ObjectId(4), readDatabase.PealsDatabase().FindPeal(2)->PictureId());

            Assert::AreEqual(picOneOriginalSize, readDatabase.PicturesDatabase().FindPicture(1)->GetPictureBuffer().size());
            Assert::AreEqual(picFourOriginalSize, readDatabase.PicturesDatabase().FindPicture(4)->GetPictureBuffer().size());
        }


        TEST_METHOD(PictureDatabase_Compare)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Picture firstPicture;
            firstPicture.ReplacePicture("bellboard.jpg");
            Assert::AreEqual(".jpeg", firstPicture.GetFileExtension().c_str());

            Picture secondPicture(3, firstPicture);
            secondPicture.ReplacePicture("Newtons_cradle_animation.gif");

            Picture thirdPicture(2, firstPicture);
            Assert::AreEqual(".jpeg", firstPicture.GetFileExtension().c_str());
            Picture fourthPicture;
            fourthPicture = firstPicture;

            Assert::AreNotEqual(firstPicture, secondPicture);
            Assert::AreNotEqual(firstPicture, thirdPicture);
            Assert::AreEqual(firstPicture, fourthPicture);
        }

        TEST_METHOD(PictureDatabase_Export_And_Match)
        {
            Duco::ObjectId pictureId;
            const char* tempFilename = "temp.duc";
            {
                RingingDatabase database;
                database.ClearDatabase(false, true);

                Picture firstPicture;
                firstPicture.ReplacePicture("bellboard.jpg");
                pictureId = database.PicturesDatabase().AddObject(firstPicture);

                Assert::IsTrue(firstPicture.ContainsValidPicture());
                Assert::AreEqual((size_t)105680, firstPicture.PictureSize());
                database.Externalise(tempFilename, this);
            }
            {
                RingingDatabase reReadDatabase;
                unsigned int databaseVersion = 0;
                reReadDatabase.Internalise(tempFilename, this, databaseVersion);
                Assert::AreEqual(Duco::DucoConfiguration::LatestDatabaseVersion(), databaseVersion);

                const Picture* const firstPicture = reReadDatabase.PicturesDatabase().FindPicture(pictureId);
                // Exported incase you want to view it seperatly.
                std::string pictureFilename = "bellboard_copy";
                firstPicture->SavePicture(pictureFilename);

                Assert::AreEqual("bellboard_copy.jpeg", pictureFilename.c_str());
                Assert::AreEqual((size_t)105680, firstPicture->PictureSize());
                Assert::IsTrue(firstPicture->ContainsValidPicture());

                Picture newPicture;
                newPicture.ReplacePicture("bellboard.jpg");

                Assert::AreEqual(newPicture.GetPictureBuffer(), firstPicture->GetPictureBuffer());
            }
        }

        TEST_METHOD(PictureDatabase_ImportPicture)
        {
            const std::string sampleDatabasename = "lundy_DBVer_33.duc";
            RingingDatabase database;
            unsigned int dbVersion = 0;
            database.Internalise(sampleDatabasename.c_str(), this, dbVersion);
            Assert::AreEqual(33u, dbVersion);
            Assert::AreEqual((size_t)1, database.NumberOfPictures());

            const Picture* const firstPicture = database.PicturesDatabase().FindPicture(1);
            Assert::IsTrue(firstPicture->ContainsValidPicture());
            Assert::AreEqual(".png", firstPicture->GetFileExtension().c_str());
            const std::string buffer = firstPicture->GetPictureBuffer();
            Assert::AreEqual('�', buffer[0]);
            Assert::AreEqual('�', buffer[1]);
            Assert::AreEqual('�', buffer[2]);
            Assert::AreEqual('�', buffer[3]);
        }

        TEST_METHOD(PictureDatabase_SinglePictureDatabase_DBVer_38)
        {
            const std::string sampleDatabasename = "single_picture_DBVer_38.duc";
            RingingDatabase database;
            Duco::StatisticFilters filters(database);
            unsigned int dbVersion = 0;
            database.Internalise(sampleDatabasename.c_str(), this, dbVersion);

            Assert::AreEqual(38u, dbVersion);
            Assert::AreEqual((size_t)0, database.NumberOfAssociations());
            Assert::AreEqual((size_t)0, database.NumberOfCompositions());
            Assert::AreEqual((size_t)0, database.NumberOfMethods());
            Assert::AreEqual((size_t)0, database.NumberOfMethodSeries());
            Assert::AreEqual((size_t)0, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)1, database.NumberOfPictures());
            Assert::AreEqual((size_t)0, database.NumberOfTowers());

            const Picture* const firstPicture = database.PicturesDatabase().FindPicture(1);
            Assert::IsTrue(firstPicture->ContainsValidPicture()); // this didnt work properly till a fix in DB version 41
            Assert::AreEqual(".tmp", firstPicture->GetFileExtension().c_str()); // this didnt work till a fix in DB version 41
            Assert::AreEqual((size_t)34622, firstPicture->GetPictureBuffer().size());
        }

        TEST_METHOD(PictureDatabase_SearchForPealsWithPictures)
        {
            RingingDatabase database;

            Picture firstPicture;
            firstPicture.ReplacePicture("bellboard.jpg");
            Assert::AreEqual(".jpeg", firstPicture.GetFileExtension().c_str());
            Duco::ObjectId pictureOneId = database.PicturesDatabase().AddObject(firstPicture);

            Picture secondPicture;
            secondPicture.ReplacePicture("bothered2.png");
            Assert::AreEqual(".png", secondPicture.GetFileExtension().c_str());
            Duco::ObjectId pictureTwoId = database.PicturesDatabase().AddObject(secondPicture);

            Assert::AreEqual((size_t)2, database.NumberOfPictures());

            Peal pealOne;
            pealOne.SetPictureId(pictureOneId);
            Duco::ObjectId pealOneId = database.PealsDatabase().AddObject(pealOne);
            Peal pealTwo;
            Duco::ObjectId pealTwoId = database.PealsDatabase().AddObject(pealTwo);
            Peal pealThree;
            pealThree.SetPictureId(pictureTwoId);
            Duco::ObjectId pealThreeId = database.PealsDatabase().AddObject(pealThree);

            DatabaseSearch search(database);

            search.AddArgument(new TSearchFieldBoolArgument (EContainsPicture));

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::EPeal);
            Assert::AreEqual((size_t)2, objectIds.size());
            Assert::AreEqual(pealOneId, *objectIds.begin());
            Assert::AreEqual(pealThreeId, *objectIds.rbegin());

            search.Search(*this, objectIds, TObjectType::ETower);
            Assert::AreEqual((size_t)0, objectIds.size());
        }

        TEST_METHOD(PictureDatabase_SearchForTowersWithPictures)
        {
            RingingDatabase database;

            Picture firstPicture;
            firstPicture.ReplacePicture("bellboard.jpg");
            Assert::AreEqual(".jpeg", firstPicture.GetFileExtension().c_str());
            Duco::ObjectId pictureOneId = database.PicturesDatabase().AddObject(firstPicture);

            Picture secondPicture;
            secondPicture.ReplacePicture("bothered2.png");
            Assert::AreEqual(".png", secondPicture.GetFileExtension().c_str());
            Duco::ObjectId pictureTwoId = database.PicturesDatabase().AddObject(secondPicture);

            Assert::AreEqual((size_t)2, database.NumberOfPictures());

            Tower towerOne;
            towerOne.SetPictureId(pictureOneId);
            Duco::ObjectId towerOneId = database.TowersDatabase().AddObject(towerOne);
            Tower towerTwo;
            Duco::ObjectId towerTwoId = database.TowersDatabase().AddObject(towerTwo);
            Tower towerThree;
            towerThree.SetPictureId(pictureTwoId);
            Duco::ObjectId towerThreeId = database.TowersDatabase().AddObject(towerThree);

            DatabaseSearch search(database);

            search.AddArgument(new TSearchFieldBoolArgument(EContainsPicture));

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::EPeal);
            Assert::AreEqual((size_t)0, objectIds.size());

            search.Search(*this, objectIds, TObjectType::ETower);
            Assert::AreEqual((size_t)2, objectIds.size());
            Assert::AreEqual(towerOneId, *objectIds.begin());
            Assert::AreEqual(towerThreeId, *objectIds.rbegin());
        }
    };
}
