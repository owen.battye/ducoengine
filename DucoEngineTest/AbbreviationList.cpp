#include "CppUnitTest.h"
#include <AbbreviationList.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(AbbreviationList_UnitTests)
    {
        AbbreviationList* database;

    public:
        AbbreviationList_UnitTests()
        {
            database = new AbbreviationList("DoveSaints.txt");
        }

        ~AbbreviationList_UnitTests()
        {
            delete database;
        }

        TEST_METHOD(AbbreviationList_ProperSaint_Cathedral_Church_of_St_Peter)
        {
            Assert::AreEqual((size_t)12, database->NoOfObjects());
            std::wstring updatedSaint;
            Assert::IsTrue(database->ProperName(L"Cath Ch of S Peter", updatedSaint));
            Assert::AreEqual(L"Cathedral church of St Peter", updatedSaint.c_str());
        }

        TEST_METHOD(AbbreviationList_ProperSaint_Assumption_of_BVM)
        {
            std::wstring updatedSaint;
            Assert::IsTrue(database->ProperName(L"Assumption of BVM", updatedSaint));
            Assert::AreEqual(L"Assumption of Blessed Virgin Mary", updatedSaint.c_str());
        }

        TEST_METHOD(AbbreviationList_ProperSaint_S_Andrew_Gt)
        {
            std::wstring updatedSaint;
            Assert::IsTrue(database->ProperName(L"S Andrew Gt", updatedSaint));
            Assert::AreEqual(L"St Andrew the Great", updatedSaint.c_str());
        }

        TEST_METHOD(AbbreviationList_ProperSaint_Old_Steeple)
        {
            std::wstring updatedSaint;
            Assert::IsTrue(database->ProperName(L"Old Steeple (S Mary's Tower)", updatedSaint));
            Assert::AreEqual(L"Old Steeple (St Mary's Tower)", updatedSaint.c_str());
        }
        TEST_METHOD(AbbreviationList_ProperSaint_Cath_Metropolitical_Ch_of_Christ)
        {
            std::wstring updatedSaint;
            Assert::IsTrue(database->ProperName(L"Cath & Metropolitical Ch of Christ", updatedSaint));
            Assert::AreEqual(L"Cathedral & Metropolitical Church of Christ", updatedSaint.c_str());
        }

        TEST_METHOD(AbbreviationList_ProperSaint_Our_Lady_Help_of_Christians)
        {
            std::wstring updatedSaint;
            Assert::IsFalse(database->ProperName(L"Our Lady Help of Christians", updatedSaint));
            Assert::AreEqual(L"Our Lady Help of Christians", updatedSaint.c_str());
        }

        TEST_METHOD(AbbreviationList_ProperSaint_All_Hallows)
        {
            std::wstring updatedSaint;
            Assert::IsFalse(database->ProperName(L"All Hallows", updatedSaint));
            Assert::AreEqual(L"All Hallows", updatedSaint.c_str());
        }

        TEST_METHOD(AbbreviationList_ProperSaint_St_Mary)
        {
            std::wstring updatedSaint;
            Assert::IsFalse(database->ProperName(L"St Mary", updatedSaint));
            Assert::AreEqual(L"St Mary", updatedSaint.c_str());
        }
    };
}
