﻿#include "CppUnitTest.h"
#include <Ring.h>
#include "ToString.h"
#include <DatabaseSettings.h>
#include <RingingDatabase.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(Ring_UnitTests)
    {
    public:
        TEST_METHOD(Ring_Construction)
        {
            std::set<unsigned int> newBells;
            newBells.insert(1);
            newBells.insert(2);
            newBells.insert(3);
            newBells.insert(4);
            newBells.insert(5);
            newBells.insert(6);
            newBells.insert(7);
            newBells.insert(8);
            newBells.insert(9);
            newBells.insert(10);

            Ring theRing(0, 10, L"Ten bell peal", newBells, L"11-1-14", L"");

            Assert::AreEqual(L"Ten bell peal", theRing.Name().c_str());
            Assert::AreEqual(L"11-1-14", theRing.TenorWeight().c_str());
            Assert::AreEqual((unsigned int)10, theRing.NoOfBells());
            Assert::AreEqual(TObjectType::ERing, theRing.ObjectType());
        }

        TEST_METHOD(Ring_CopyConstruction)
        {
            std::set<unsigned int> newBells;
            newBells.insert(1);
            newBells.insert(2);
            newBells.insert(3);
            newBells.insert(4);
            newBells.insert(5);
            newBells.insert(6);
            newBells.insert(7);
            newBells.insert(8);
            newBells.insert(9);
            newBells.insert(10);

            Ring theRing(0, 10, L"Ten bell peal", newBells, L"11-1-14", L"E♭");
            Ring newRing(theRing);

            Assert::AreEqual(L"Ten bell peal", newRing.Name().c_str());
            Assert::AreEqual(L"11-1-14", newRing.TenorWeight().c_str());
            Assert::AreEqual(L"E♭", newRing.TenorKey().c_str());
            Assert::AreEqual((unsigned int)10, newRing.NoOfBells());
            Assert::AreEqual(TObjectType::ERing, newRing.ObjectType());
            Assert::IsTrue(theRing == newRing);
        }


        TEST_METHOD(Ring_ContainsBells)
        {
            std::set<unsigned int> newBells;
            newBells.insert(5);
            newBells.insert(6);
            newBells.insert(7);
            newBells.insert(8);
            newBells.insert(9);
            newBells.insert(10);

            Ring theRing(0, 6, L"Six bell peal", newBells, L"11-1-14", L"");

            Assert::IsTrue(theRing.ContainsBell(5));
        }

        TEST_METHOD(Ring_DoesNotContainBells)
        {
            std::set<unsigned int> newBells;
            newBells.insert(5);
            newBells.insert(6);
            newBells.insert(7);
            newBells.insert(8);
            newBells.insert(9);
            newBells.insert(10);

            Ring theRing(0, 6, L"Six bell peal", newBells, L"11-1-14", L"");

            Assert::IsFalse(theRing.ContainsBell(1));
        }

        TEST_METHOD(Ring_RealBellNumber)
        {
            std::set<unsigned int> newBells;
            newBells.insert(5);
            newBells.insert(6);
            newBells.insert(7);
            newBells.insert(8);
            newBells.insert(9);
            newBells.insert(10);

            Ring theRing(0, 6, L"Six bell peal", newBells, L"11-1-14", L"");

            unsigned int realBellNumber = 6;
            Assert::IsTrue(theRing.RealBellNumber(realBellNumber));
            Assert::AreEqual((unsigned int)10, realBellNumber);
        }

        TEST_METHOD(Ring_RealBellNumber_BellNotInRing)
        {
            std::set<unsigned int> newBells;
            newBells.insert(5);
            newBells.insert(6);
            newBells.insert(7);
            newBells.insert(8);
            newBells.insert(9);
            newBells.insert(10);

            Ring theRing(0, 6, L"Six bell peal", newBells, L"11-1-14", L"");

            unsigned int realBellNumber = 8;
            Assert::IsFalse(theRing.RealBellNumber(realBellNumber));
        }

        TEST_METHOD(Ring_BellNumberInRing)
        {
            std::set<unsigned int> newBells;
            newBells.insert(5);
            newBells.insert(6);
            newBells.insert(7);
            newBells.insert(8);
            newBells.insert(9);
            newBells.insert(10);

            Ring theRing(0, 6, L"Six bell peal", newBells, L"11-1-14", L"");

            Assert::IsFalse(theRing.BellNumberInRing(4));
            Assert::IsTrue(theRing.BellNumberInRing(6));
            Assert::IsTrue(theRing.BellNumberInRing(7));
            Assert::IsFalse(theRing.BellNumberInRing(11));
        }

        TEST_METHOD(Ring_BellsMatch)
        {
            std::set<unsigned int> newBells;
            newBells.insert(5);
            newBells.insert(6);
            newBells.insert(7);
            newBells.insert(8);
            newBells.insert(9);
            newBells.insert(10);

            Ring theRing(0, 6, L"Six bell peal", newBells, L"11-1-14", L"");

            unsigned int realBellNumber = 6;
            Assert::IsTrue(theRing.BellsMatch(theRing));
        }

        TEST_METHOD(Ring_Similar)
        {
            RingingDatabase database;
            std::set<unsigned int> newBells;
            newBells.insert(5);
            newBells.insert(6);
            newBells.insert(7);
            newBells.insert(8);
            newBells.insert(9);
            newBells.insert(10);

            Ring theRing(0, 6, L"Six bell peal", newBells, L"11-1-14", L"E♭");
            Ring otherRing(0, 6, L"Six bell peal", newBells, L"11", L"E♭");

            Assert::IsTrue(theRing.Duplicate(otherRing, database));
        }

        TEST_METHOD(Ring_Similar_NoDetails_ButBellCount)
        {
            RingingDatabase database;
            std::set<unsigned int> newBells;
            newBells.insert(5);
            newBells.insert(6);
            newBells.insert(7);
            newBells.insert(8);
            newBells.insert(9);
            newBells.insert(10);

            Ring theRing(0, 6, L"Six bell peal", newBells, L"", L"");
            Ring otherRing(0, 6, L"Six bell peal", newBells, L"", L"");

            Assert::IsTrue(theRing.Duplicate(otherRing, database));
        }

        TEST_METHOD(Ring_BellsDontMatch)
        {
            std::set<unsigned int> newBells;
            newBells.insert(5);
            newBells.insert(6);
            newBells.insert(7);
            newBells.insert(8);
            newBells.insert(9);
            newBells.insert(10);
            Ring theRing(0, 6, L"Six bell peal", newBells, L"11-1-14", L"");

            std::set<unsigned int> otherBells;
            newBells.insert(4);
            newBells.insert(5);
            newBells.insert(7);
            newBells.insert(8);
            newBells.insert(9);
            newBells.insert(10);
            Ring otherRing(0, 6, L"Six bell peal", otherBells, L"11-1-14", L"");

            Assert::IsFalse(theRing.BellsMatch(otherRing));
        }

        TEST_METHOD(Ring_NoTenorWeight)
        {
            std::set<unsigned int> newBells;
            newBells.insert(5);
            newBells.insert(6);
            newBells.insert(7);
            newBells.insert(8);
            newBells.insert(9);
            newBells.insert(10);

            Ring theRing(0, 6, L"Six bell peal", newBells, L"", L"");
            std::map<unsigned int, Duco::TRenameBellType> renamedBells;

            Assert::IsFalse(theRing.Valid(renamedBells, 10, 10, true, true));
            Assert::IsFalse(theRing.ErrorCode().test(ERing_InvalidRingNoOfBells));
            Assert::IsTrue(theRing.ErrorCode().test(ERing_InvalidRingTenorWeight));
            Assert::IsTrue(theRing.ErrorCode().test(ERing_MissingRingTenorKey));
            Assert::IsFalse(theRing.ErrorCode().test(ERing_InvalidRingName));
            Assert::IsFalse(theRing.ErrorCode().test(ERing_PossibleInvalidRingBells));
            DatabaseSettings settings;

            Assert::AreEqual(L"", theRing.ErrorString(settings, true).c_str());
        }

        TEST_METHOD(Ring_InvalidNumberOfBells)
        {
            std::set<unsigned int> newBells;
            newBells.insert(5);
            newBells.insert(6);
            newBells.insert(8);
            newBells.insert(9);
            newBells.insert(10);

            Ring theRing(0, 6, L"Six bell peal", newBells, L"Tenor", L"A");
            std::map<unsigned int, Duco::TRenameBellType> renamedBells;

            Assert::IsFalse(theRing.Valid(renamedBells, 10, 10, true, true));
            Assert::IsTrue(theRing.ErrorCode().test(ERing_InvalidRingNoOfBells));
            Assert::IsFalse(theRing.ErrorCode().test(ERing_InvalidRingTenorWeight));
            Assert::IsFalse(theRing.ErrorCode().test(ERing_MissingRingTenorKey));
            Assert::IsFalse(theRing.ErrorCode().test(ERing_InvalidRingName));
            Assert::IsFalse(theRing.ErrorCode().test(ERing_PossibleInvalidRingBells));
        }

        TEST_METHOD(Ring_InvalidBell)
        {
            std::set<unsigned int> newBells;
            newBells.insert(6);
            newBells.insert(7);
            newBells.insert(8);
            newBells.insert(9);
            newBells.insert(10);
            newBells.insert(11);

            Ring theRing(0, 6, L"Six bell peal", newBells, L"Tenor", L"");
            std::map<unsigned int, Duco::TRenameBellType> renamedBells;

            Assert::IsFalse(theRing.Valid(renamedBells, 10, 10, true, true));
            Assert::IsTrue(theRing.ErrorCode().test(ERing_InvalidRingNoOfBells));
            Assert::IsFalse(theRing.ErrorCode().test(ERing_InvalidRingTenorWeight));
            Assert::IsTrue(theRing.ErrorCode().test(ERing_MissingRingTenorKey));
            Assert::IsFalse(theRing.ErrorCode().test(ERing_InvalidRingName));
            Assert::IsFalse(theRing.ErrorCode().test(ERing_PossibleInvalidRingBells));
        }

        TEST_METHOD(Ring_InvalidBell_TooLarge)
        {
            std::set<unsigned int> newBells;
            newBells.insert(-1);
            newBells.insert(7);
            newBells.insert(8);
            newBells.insert(9);
            newBells.insert(10);
            newBells.insert(11);

            Ring theRing(0, 6, L"Six bell peal", newBells, L"Tenor", L"A");
            std::map<unsigned int, Duco::TRenameBellType> renamedBells;

            Assert::IsFalse(theRing.Valid(renamedBells, 10, 10, true, true));
            Assert::IsTrue(theRing.ErrorCode().test(ERing_InvalidRingNoOfBells));
            Assert::IsFalse(theRing.ErrorCode().test(ERing_InvalidRingTenorWeight));
            Assert::IsFalse(theRing.ErrorCode().test(ERing_MissingRingTenorKey));
            Assert::IsFalse(theRing.ErrorCode().test(ERing_InvalidRingName));
            Assert::IsFalse(theRing.ErrorCode().test(ERing_PossibleInvalidRingBells));
        }

        TEST_METHOD(Ring_NoRingName)
        {
            std::set<unsigned int> newBells;
            newBells.insert(5);
            newBells.insert(6);
            newBells.insert(7);
            newBells.insert(8);
            newBells.insert(9);
            newBells.insert(10);

            Ring theRing(0, 6, L"", newBells, L"Tenor", L"A");
            std::map<unsigned int, Duco::TRenameBellType> renamedBells;

            Assert::IsFalse(theRing.Valid(renamedBells, 10, 10, true, true));
            Assert::IsFalse(theRing.ErrorCode().test(ERing_InvalidRingNoOfBells));
            Assert::IsFalse(theRing.ErrorCode().test(ERing_InvalidRingTenorWeight));
            Assert::IsFalse(theRing.ErrorCode().test(ERing_MissingRingTenorKey));
            Assert::IsTrue(theRing.ErrorCode().test(ERing_InvalidRingName));
            Assert::IsFalse(theRing.ErrorCode().test(ERing_PossibleInvalidRingBells));
        }

        TEST_METHOD(Ring_AddBell)
        {
            std::set<unsigned int> newBells;
            newBells.insert(5);
            //newBells.insert(6);
            newBells.insert(7);
            newBells.insert(8);
            newBells.insert(9);
            newBells.insert(10);

            Ring theRing(0, 6, L"", newBells, L"Tenor", L"B");
            std::map<unsigned int, Duco::TRenameBellType> renamedBells;

            Assert::IsFalse(theRing.Valid(renamedBells, 10, 10, false, true));
            Assert::IsTrue(theRing.ErrorCode().test(ERing_InvalidRingNoOfBells));

            theRing.ChangeBell(6, true);
            Assert::IsTrue(theRing.Valid(renamedBells, 10, 10, false, true));
            Assert::IsFalse(theRing.ErrorCode().test(ERing_InvalidRingNoOfBells));
        }

        TEST_METHOD(Ring_RemoveBell)
        {
            std::set<unsigned int> newBells;
            newBells.insert(5);
            newBells.insert(6);
            newBells.insert(7);
            newBells.insert(8);
            newBells.insert(9);
            newBells.insert(10);

            Ring theRing(0, 6, L"", newBells, L"Tenor", L"A");
            std::map<unsigned int, Duco::TRenameBellType> renamedBells;

            Assert::IsTrue(theRing.Valid(renamedBells, 10, 10, false, true));
            Assert::IsFalse(theRing.ErrorCode().test(ERing_InvalidRingNoOfBells));

            theRing.ChangeBell(6, false);
            Assert::IsFalse(theRing.Valid(renamedBells, 10, 10, false, true));
            Assert::IsTrue(theRing.ErrorCode().test(ERing_InvalidRingNoOfBells));
        }

        TEST_METHOD(Ring_UpdateTenorInRing)
        {
            std::set<unsigned int> newBells;
            newBells.insert(1);
            newBells.insert(2);
            newBells.insert(3);
            newBells.insert(4);
            newBells.insert(5);
            newBells.insert(6);

            Ring theRing(0, 6, L"Test", newBells, L"11", L"D");
            std::map<unsigned int, Duco::TRenameBellType> renamedBells;

            Assert::IsTrue(theRing.Valid(renamedBells, 6, 6, true, true));
            Assert::IsFalse(theRing.ErrorCode().any());
            Assert::AreEqual(L"11", theRing.TenorWeight().c_str());

            theRing.UpdateTenorWeightToDove(L"11-1-14", 6);
            Assert::IsTrue(theRing.Valid(renamedBells, 6, 6, true, true));
            Assert::IsFalse(theRing.ErrorCode().any());
            Assert::AreEqual(L"11-1-14", theRing.TenorWeight().c_str());
        }

        TEST_METHOD(Ring_UpdateTenorNotInRing)
        {
            std::set<unsigned int> newBells;
            newBells.insert(1);
            newBells.insert(2);
            newBells.insert(3);
            newBells.insert(4);
            newBells.insert(5);
            newBells.insert(6);

            Ring theRing(0, 6, L"Test", newBells, L"11", L"A");
            std::map<unsigned int, Duco::TRenameBellType> renamedBells;

            Assert::IsTrue(theRing.Valid(renamedBells, 10, 10, true, true));
            Assert::IsFalse(theRing.ErrorCode().any());
            Assert::AreEqual(L"11", theRing.TenorWeight().c_str());

            theRing.UpdateTenorWeightToDove(L"11-1-14", 8);
            Assert::IsTrue(theRing.Valid(renamedBells, 10, 10, true, true));
            Assert::IsFalse(theRing.ErrorCode().any());
            Assert::AreEqual(L"11", theRing.TenorWeight().c_str());
            Assert::AreEqual(L"11 in A", theRing.TenorDescription().c_str());
        }

        TEST_METHOD(Ring_TenorDescription_NoKey)
        {
            std::set<unsigned int> newBells;
            Ring theRing(0, 6, L"Test", newBells, L"11", L"");

            Assert::AreEqual(L"11", theRing.TenorDescription().c_str());
        }

        TEST_METHOD(Ring_TenorDescription_FlatKey)
        {
            std::set<unsigned int> newBells;
            Ring theRing(0, 6, L"Test", newBells, L"11", L"D♭");

            Assert::AreEqual(L"11 in D♭", theRing.TenorDescription().c_str());
        }

        TEST_METHOD(Ring_ContainsFlatAndSameBellsNormal)
        {
            std::map<unsigned int, TRenameBellType> renames;
            std::pair<unsigned int, TRenameBellType> renamedBell1(1, TRenameBellType::EExtraTreble);
            renames.insert(renamedBell1);
            std::pair<unsigned int, TRenameBellType> renamedBell2(8, TRenameBellType::EFlat);
            renames.insert(renamedBell2);

            std::set<unsigned int> newBells;
            newBells.insert(1);
            newBells.insert(2);
            newBells.insert(3);
            newBells.insert(4);
            newBells.insert(5);
            newBells.insert(7);
            newBells.insert(8);
            newBells.insert(10);

            Ring theRing(KNoId, 8, L"", newBells, L"", L"");

            Assert::IsFalse(theRing.Valid(renames, 12, 14, true, true));
            Assert::IsTrue(theRing.ErrorCode().test(ERing_PossibleInvalidRingBells));
        }

        TEST_METHOD(Ring_ContainsSharpAndSameBellsNormal)
        {
            std::map<unsigned int, TRenameBellType> renames;
            std::pair<unsigned int, TRenameBellType> renamedBell1(1, TRenameBellType::EExtraTreble);
            renames.insert(renamedBell1);
            std::pair<unsigned int, TRenameBellType> renamedBell2(8, TRenameBellType::ESharp);
            renames.insert(renamedBell2);

            std::set<unsigned int> newBells;
            newBells.insert(1);
            newBells.insert(2);
            newBells.insert(3);
            newBells.insert(4);
            newBells.insert(5);
            newBells.insert(8);
            newBells.insert(9);
            newBells.insert(11);

            Ring theRing(KNoId, 8, L"", newBells, L"", L"");

            Assert::IsFalse(theRing.Valid(renames, 12, 14, true, true));
            Assert::IsTrue(theRing.ErrorCode().test(ERing_PossibleInvalidRingBells));

        }
    };
}
