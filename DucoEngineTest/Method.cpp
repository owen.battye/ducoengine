#include "CppUnitTest.h"
#include <Method.h>
#include <MethodDatabase.h>
#include "ToString.h"
#include <DatabaseSettings.h>
#include <RingingDatabase.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(Method_UnitTests)
    {
        Duco::MethodDatabase database;
        Duco::DatabaseSettings settings;

    public:
        Method_UnitTests()
        {
            database.ClearObjects(true);
        }

        TEST_METHOD(Method_DefaultBehaviour)
        {
            Method theMethod(1, 10, L"Bristol", L"Surprise", L"&X50X14.50X50.36.14X70.58.16X16.70X16X10-10", false);
            Assert::AreEqual(TObjectType::EMethod, theMethod.ObjectType());

            Assert::AreEqual(L"Bristol S R", theMethod.ShortName(database).c_str());
            Assert::AreEqual(L"Bristol Surprise Royal", theMethod.FullName(database).c_str());
            Assert::IsFalse(theMethod.ValidNotation(true, false, settings, true));

            Assert::IsTrue(theMethod.Valid(database, settings, false, true));
            Assert::IsFalse(theMethod.Valid(database, settings, true, true));

            theMethod.SetBobPlaceNotation(L"14");
            Assert::IsTrue(theMethod.ContainsBobNotation());
            Assert::IsFalse(theMethod.ContainsSingleNotation());
            Assert::IsTrue(theMethod.ContainsBobOrSingleNotation());
            Assert::IsFalse(theMethod.ContainsBobAndSingleNotation());
            theMethod.SetSinglePlaceNotation(L"1234");
            Assert::IsTrue(theMethod.ContainsSingleNotation());
            Assert::IsTrue(theMethod.ContainsBobOrSingleNotation());
            Assert::IsTrue(theMethod.ContainsBobAndSingleNotation());
            Assert::IsTrue(theMethod.Valid(database, settings, true, true));
        }


        TEST_METHOD(Method_DefaultBehaviour_WarningsDisabled)
        {
            Method theMethod(1, 10, L"Bristol", L"Surprise", L"&X50X14.50X50.36.14X70.58.16X16.70X16X10-10", false);
            Assert::AreEqual(TObjectType::EMethod, theMethod.ObjectType());

            Assert::AreEqual(L"Bristol S R", theMethod.ShortName(database).c_str());
            Assert::AreEqual(L"Bristol Surprise Royal", theMethod.FullName(database).c_str());
            Assert::IsFalse(theMethod.ValidNotation(true, false, settings, true));
            settings.SetValidationCheck(EMethodWarning_MissingBobPlaceNotation, TObjectType::EMethod, true);
            settings.SetValidationCheck(EMethodWarning_MissingSinglePlaceNotation, TObjectType::EMethod, true);

            Assert::IsTrue(theMethod.Valid(database, settings, false, true));
            Assert::IsTrue(theMethod.Valid(database, settings, true, true));

            settings.ClearSettings(false);
        }

        TEST_METHOD(Method_DualOrderFullName)
        {
            Method theMethod(1, 10, L"Mixed", L"Surprise", L"", true);
            Assert::AreEqual(TObjectType::EMethod, theMethod.ObjectType());

            Assert::AreEqual(L"Mixed Surprise Caters and Royal", theMethod.FullName(database).c_str());
        }

        TEST_METHOD(Method_FindBristolRoyal)
        {
            database.AddMethod(L"Bristol Surprise Major");
            Duco::ObjectId bristolRoyal = database.AddMethod(L"Bristol Surprise Royal");
            database.AddMethod(L"Bristol Surprise Maximus");
            Assert::AreEqual((size_t)3, database.NumberOfObjects());

            Duco::ObjectId foundMethod = database.SuggestMethod(L"Bristol Surprise Royal");
            Assert::AreEqual(bristolRoyal, foundMethod);
        }

        TEST_METHOD(Method_Grandsire)
        {
            database.AddMethod(L"Grandsire Triples");
            database.AddMethod(L"Grandsire Caters");
            Duco::ObjectId grandsireCinques = database.AddMethod(L"Grandsire Cinques");
            Assert::AreEqual((size_t)3, database.NumberOfObjects());

            Duco::ObjectId foundMethod = database.SuggestMethod(L"Grandsire Cinques");
            Assert::AreEqual(grandsireCinques, foundMethod);
            Assert::IsFalse(database.SuggestMethod(L"Grandsire").ValidId());
        }

        TEST_METHOD(Method_SplicedMinor)
        {
            Duco::ObjectId surpriseMinor = database.AddMethod(L"", L"Surprise", 6, false);
            Assert::AreEqual((size_t)1, database.NumberOfObjects());

            Duco::ObjectId foundMethod = database.SuggestMethod(L"Surprise Minor");
            Assert::AreEqual(surpriseMinor, foundMethod);
        }

        TEST_METHOD(Method_RemoveOrderNameFromMethod_Major)
        {
            std::wstring originalMethodName = L"Bristol Surprise Major";
            std::wstring finishedMethodName;
            unsigned int orderNumber;
            bool dualOrder = false;

            Assert::IsTrue(database.RemoveOrderNameFromMethodName(originalMethodName, finishedMethodName, orderNumber, dualOrder));
            Assert::IsFalse(dualOrder);
            Assert::AreEqual((unsigned int)8, orderNumber);
            Assert::AreEqual(L"Bristol Surprise", finishedMethodName.c_str());
        }

        TEST_METHOD(Method_RemoveOrderNameFromMethod_Cinques)
        {
            std::wstring originalMethodName = L"Stedman Cinques";
            std::wstring finishedMethodName;
            unsigned int orderNumber;
            bool dualOrder = false;

            Assert::IsTrue(database.RemoveOrderNameFromMethodName(originalMethodName, finishedMethodName, orderNumber, dualOrder));
            Assert::IsFalse(dualOrder);
            Assert::AreEqual((unsigned int)11, orderNumber);
            Assert::AreEqual(L"Stedman", finishedMethodName.c_str());
        }

        TEST_METHOD(Method_RemoveOrderNameFromMethod_DualOrder)
        {
            std::wstring originalMethodName = L"Mixed Cinques and Maximus";
            std::wstring finishedMethodName;
            unsigned int orderNumber;
            bool dualOrder = false;

            Assert::IsTrue(database.RemoveOrderNameFromMethodName(originalMethodName, finishedMethodName, orderNumber, dualOrder));
            Assert::IsTrue(dualOrder);
            Assert::AreEqual((unsigned int)12, orderNumber);
            Assert::AreEqual(L"Mixed", finishedMethodName.c_str());
        }

        TEST_METHOD(Method_RemoveOrderNameFromMethod_MissingOrder)
        {
            std::wstring originalMethodName = L"Dave";
            std::wstring finishedMethodName;
            unsigned int orderNumber = 0;
            bool dualOrder = true;

            Assert::IsFalse(database.RemoveOrderNameFromMethodName(originalMethodName, finishedMethodName, orderNumber, dualOrder));
            Assert::IsFalse(dualOrder);
            Assert::AreEqual((unsigned int)0, orderNumber);
            Assert::AreEqual(L"", finishedMethodName.c_str());
        }

        TEST_METHOD(Method_OrderNames_CheckRanges)
        {
            Assert::AreEqual((unsigned int)20, database.HighestValidOrderNumber());
            Assert::AreEqual((unsigned int)3, database.LowestValidOrderNumber());
            Assert::AreEqual((size_t)18, database.NoOfOrderNames());

            unsigned int order = 0;
            bool dualOrder = true;
            std::wstring major = L"Major";
            Assert::IsTrue(database.FindOrderNumber(major, order, dualOrder));
            Assert::IsFalse(dualOrder);
            Assert::AreEqual((unsigned int)8, order);

            std::wstring triples = L"Triples";
            Assert::IsTrue(database.FindOrderNumber(triples, order, dualOrder));
            Assert::IsFalse(dualOrder);
            Assert::AreEqual((unsigned int)7, order);

            std::wstring cinques = L"cinques";
            Assert::IsTrue(database.FindOrderNumber(cinques, order, dualOrder));
            Assert::IsFalse(dualOrder);
            Assert::AreEqual((unsigned int)11, order);

            std::wstring dualOrderMajor = L"Triples and Major";
            Assert::IsTrue(database.FindOrderNumber(dualOrderMajor, order, dualOrder));
            Assert::IsTrue(dualOrder);
            Assert::AreEqual((unsigned int)8, order);
        }

        TEST_METHOD(Method_OrderNames_DualOrder)
        {
            Assert::IsTrue(database.DualOrder(L"Triples and Major"));
            Assert::IsTrue(database.DualOrder(L"Doubles and minor"));
            Assert::IsFalse(database.DualOrder(L"Major"));
            Assert::IsFalse(database.DualOrder(L"Cinques"));
            Assert::IsFalse(database.DualOrder(L"Maximus"));
        }

        TEST_METHOD(Method_OrderNames_MethodIsSpliced)
        {
            Duco::ObjectId methodId = database.AddMethod(L"Grandsire Triples");
            Assert::IsFalse(database.MethodIsSpliced(methodId));

            Duco::ObjectId methodId1 = database.AddMethod(L"Spliced Major");
            Assert::IsTrue(database.MethodIsSpliced(methodId1));

            Duco::ObjectId methodId2 = database.AddMethod(L"10 Methods Surprise Triples");
            Assert::IsTrue(database.MethodIsSpliced(methodId2));

            Duco::ObjectId methodId3 = database.AddMethod(L"Multiple Triples");
            Assert::IsTrue(database.MethodIsSpliced(methodId3));
        }

        TEST_METHOD(Method_PlaceNotationErrorStrings)
        {
            Duco::ObjectId methodIdOne = database.AddMethod(L"Plain", L"Bob", 6, false);

            Method theMethod = *database.FindMethod(methodIdOne);
            Assert::IsFalse(theMethod.Spliced());
            Assert::IsFalse(theMethod.Valid(database, settings, true, true));
            Assert::AreEqual(L"Place notation missing", theMethod.ErrorString(settings, true).c_str());
            theMethod.SetPlaceNotation(L"&x6x6x6-12");
            Assert::IsTrue(theMethod.Valid(database, settings, false, true));
            Assert::AreEqual(L"", theMethod.ErrorString(settings, false).c_str());
            Assert::IsFalse(theMethod.Valid(database, settings, true, true));
            Assert::AreEqual(L"Missing bob place notation, Missing single place notation", theMethod.ErrorString(settings, true).c_str());
            theMethod.SetBobPlaceNotation(L"14");
            theMethod.SetSinglePlaceNotation(L"12");
            Assert::IsTrue(theMethod.Valid(database, settings, true, true));
            Assert::AreEqual(L"", theMethod.ErrorString(settings, true).c_str());
        }

        TEST_METHOD(Method_PlaceNotationErrorStringsForSpliced)
        {
            Duco::ObjectId methodIdOne = database.AddMethod(L"", L"Surprise", 6, false);

            Method theMethod = *database.FindMethod(methodIdOne);
            Assert::IsTrue(theMethod.Spliced());
            Assert::IsTrue(theMethod.Valid(database, settings, false, true));
            Assert::AreEqual(L"", theMethod.ErrorString(settings, false).c_str());
            Assert::IsFalse(theMethod.Valid(database, settings, true, true));
            Assert::AreEqual(L"Method name missing", theMethod.ErrorString(settings, true).c_str());
        }

        TEST_METHOD(Method_SetCallings)
        {
            Method theMethod(1, 10, L"Bristol", L"Surprise", L"&X50X14.50X50.36.14X70.58.16X16.70X16X10-10", false);
            Assert::AreEqual(TObjectType::EMethod, theMethod.ObjectType());

            Assert::IsFalse(theMethod.SetBobPlaceNotation(L"18"));
            Assert::IsFalse(theMethod.SetSinglePlaceNotation(L"1890"));

            Assert::IsFalse(theMethod.SetBobPlaceNotation(L"14", 1));
            Assert::IsFalse(theMethod.SetSinglePlaceNotation(L"1234", 1));

            std::wstring actualNotation;
            Assert::IsTrue(theMethod.BobPlaceNotation(actualNotation, 0));
            Assert::AreEqual(L"18", actualNotation.c_str());
            Assert::IsTrue(theMethod.SinglePlaceNotation(actualNotation, 0));
            Assert::AreEqual(L"1890", actualNotation.c_str());

            Assert::IsTrue(theMethod.BobPlaceNotation(actualNotation, 1));
            Assert::AreEqual(L"14", actualNotation.c_str());
            Assert::IsTrue(theMethod.SinglePlaceNotation(actualNotation, 1));
            Assert::AreEqual(L"1234", actualNotation.c_str());
        }

        TEST_METHOD(Method_SetCallings_WrongOrder)
        {
            Method theMethod(1, 10, L"Bristol", L"Surprise", L"&X50X14.50X50.36.14X70.58.16X16.70X16X10-10", false);
            Assert::AreEqual(TObjectType::EMethod, theMethod.ObjectType());

            Assert::IsFalse(theMethod.SetBobPlaceNotation(L"14", 1));
            Assert::IsFalse(theMethod.SetSinglePlaceNotation(L"1234", 1));

            Assert::IsFalse(theMethod.SetBobPlaceNotation(L"18"));
            Assert::IsFalse(theMethod.SetSinglePlaceNotation(L"1890"));

            std::wstring actualNotation;
            Assert::IsTrue(theMethod.BobPlaceNotation(actualNotation, 0));
            Assert::AreEqual(L"18", actualNotation.c_str());
            Assert::IsTrue(theMethod.SinglePlaceNotation(actualNotation, 0));
            Assert::AreEqual(L"1890", actualNotation.c_str());

            Assert::IsTrue(theMethod.BobPlaceNotation(actualNotation, 1));
            Assert::AreEqual(L"14", actualNotation.c_str());
            Assert::IsTrue(theMethod.SinglePlaceNotation(actualNotation, 1));
            Assert::AreEqual(L"1234", actualNotation.c_str());
        }

        TEST_METHOD(Method_SetCallings_NonSet)
        {
            Method theMethod(1, 10, L"Bristol", L"Surprise", L"&X50X14.50X50.36.14X70.58.16X16.70X16X10-10", false);
            Assert::AreEqual(TObjectType::EMethod, theMethod.ObjectType());

            std::wstring actualNotation;
            Assert::IsFalse(theMethod.BobPlaceNotation(actualNotation, 0));
            Assert::AreEqual(L"", actualNotation.c_str());
            Assert::IsFalse(theMethod.SinglePlaceNotation(actualNotation, 0));
            Assert::AreEqual(L"", actualNotation.c_str());

            Assert::IsFalse(theMethod.BobPlaceNotation(actualNotation, 1));
            Assert::AreEqual(L"", actualNotation.c_str());
            Assert::IsFalse(theMethod.SinglePlaceNotation(actualNotation, 1));
            Assert::AreEqual(L"", actualNotation.c_str());
        }

        TEST_METHOD(Method_SetCallings_Updated)
        {
            Method theMethod(1, 10, L"Bristol", L"Surprise", L"&X50X14.50X50.36.14X70.58.16X16.70X16X10-10", false);
            Assert::AreEqual(TObjectType::EMethod, theMethod.ObjectType());

            Assert::IsFalse(theMethod.SetBobPlaceNotation(L"18"));
            Assert::IsFalse(theMethod.SetSinglePlaceNotation(L"1890"));

            std::wstring actualNotation;
            Assert::IsTrue(theMethod.BobPlaceNotation(actualNotation, 0));
            Assert::AreEqual(L"18", actualNotation.c_str());
            Assert::IsTrue(theMethod.SinglePlaceNotation(actualNotation, 0));
            Assert::AreEqual(L"1890", actualNotation.c_str());

            Assert::IsFalse(theMethod.SetBobPlaceNotation(L"14"));
            Assert::IsFalse(theMethod.SetSinglePlaceNotation(L"1234"));


            Assert::IsTrue(theMethod.BobPlaceNotation(actualNotation, 0));
            Assert::AreEqual(L"14", actualNotation.c_str());
            Assert::IsTrue(theMethod.SinglePlaceNotation(actualNotation, 0));
            Assert::AreEqual(L"1234", actualNotation.c_str());
        }

        TEST_METHOD(Method_Duplicate_PlainBobMajor)
        {
            RingingDatabase database;
            Method theMethod(1, 8, L"Plain", L"Bob", L"&x18x18x18x18-12", false);
            Method theMethod2(theMethod);

            Assert::IsTrue(theMethod.Duplicate(theMethod2, database));
        }

        TEST_METHOD(Method_Duplicate_BristolRoyal)
        {
            RingingDatabase database;
            Method theMethod(1, 10, L"Bristol", L"Surprise", L"&X50X14.50X50.36.14X70.58.16X16.70X16X10-10", false);
            Method theMethod2(1, 10, L"Bristol", L"Surprise", L"&X50X14.50X50.36.14X70.58.16X16.70X16X10-10", false);

            Assert::IsTrue(theMethod.Duplicate(theMethod2, database));
        }
        TEST_METHOD(Method_Duplicate_BristolRoyalAndMajor)
        {
            RingingDatabase database;
            Method theMethod(1, 10, L"Bristol", L"Surprise", L"&X50X14.50X50.36.14X70.58.16X16.70X16X10-10", false);
            Method theMethod2(1, 8, L"Bristol", L"Surprise", L"&x58x14.58x58.36.14x14.58x14x18-18", false);

            Assert::IsFalse(theMethod.Duplicate(theMethod2, database));
        }
        TEST_METHOD(Method_Duplicate_BristolRoyalAndYorkshire)
        {
            RingingDatabase database;
            Method theMethod(1, 10, L"Bristol", L"Surprise", L"&X50X14.50X50.36.14X70.58.16X16.70X16X10-10", false);
            Method theMethod2(1, 10, L"Yorkshire", L"Surprise", L"&x30x14x50x16x1270x38x14x50x16x90-12", false);

            Assert::IsFalse(theMethod.Duplicate(theMethod2, database));
        }
        TEST_METHOD(Method_Duplicate_BristolSurpriseAndLittleSurprise)
        {
            RingingDatabase database;
            Method theMethod(1, 10, L"Bristol", L"Surprise", L"", false);
            Method theMethod2(1, 10, L"Bristol", L"Little Surprise", L"", false);

            Assert::IsFalse(theMethod.Duplicate(theMethod2, database));
        }
    };
}
