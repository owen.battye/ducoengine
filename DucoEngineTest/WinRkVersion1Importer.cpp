#include "CppUnitTest.h"

#include <WinRkVersion1Importer.h>

#include <DatabaseSettings.h>
#include <MethodDatabase.h>
#include <Method.h>
#include <Peal.h>
#include <PealDatabase.h>
#include <ProgressCallback.h>
#include <RenumberProgressCallback.h>
#include <ImportExportProgressCallback.h>
#include <Ringer.h>
#include <RingerDatabase.h>
#include <RingingDatabase.h>
#include <TowerDatabase.h>
#include <Tower.h>
#include "ToString.h"
#include <StatisticFilters.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(WinRkVersion1Importer_UnitTests), Duco::ProgressCallback, Duco::RenumberProgressCallback, Duco::ImportExportProgressCallback
    {
        Duco::RingingDatabase* database;
        Duco::WinRkVersion1Importer* importer;
        bool expectedError;
        bool completed;
        bool initialised;
        int previousPercent;
        int cancelWhenAbove;

    public:
        WinRkVersion1Importer_UnitTests()
        {
            expectedError = false;
            completed = false;
            initialised = false;
            cancelWhenAbove = 101;
            previousPercent = 0;
            database = new Duco::RingingDatabase();
            importer = new Duco::WinRkVersion1Importer(*this, *database);
        }

        ~WinRkVersion1Importer_UnitTests()
        {
            delete importer;
            delete database;
        }


        // from RenumberProgressCallback
        void Initialised()
        {
            initialised = true;
            previousPercent = 0;
        }

        void Step(int progressPercent)
        {
            Assert::IsTrue(initialised);
            Assert::IsTrue(previousPercent <= progressPercent);
            previousPercent = progressPercent;
            if (progressPercent > cancelWhenAbove)
            {
                importer->Cancel();
            }
        }

        void Complete(bool error)
        {
            Assert::AreEqual(expectedError, error);
            Assert::IsTrue(initialised);
            completed = true;
        }


        // From RenumberProgressCallback
        void RenumberInitialised(RenumberProgressCallback::TRenumberStage newStage)
        {

        }
        void RenumberStep(size_t objectId, size_t total)
        {

        }
        void RenumberComplete()
        {

        }

        //from ImportExportProgressCallback
        void InitialisingImportExport(bool internalising, unsigned int versionNumber, size_t totalNumberOfObjects)
        {
        }
        void ObjectProcessed(bool internalising, Duco::TObjectType objectType, Duco::ObjectId objectId, int totalPercentage, size_t numberInThisSubDatabase)
        {
        }
        void ImportExportComplete(bool internalising)
        {
        }
        void ImportExportFailed(bool internalising, int errorCode)
        {
        }


        void FindCheckRinger(const Duco::Peal& thePeal, unsigned int bellNo, bool strapper, const std::wstring& expectedName) const
        {
            Duco::ObjectId ringerId;
            Assert::IsTrue(database->RingersDatabase().FindRinger(thePeal.RingerId(bellNo, strapper, ringerId)));
            const Ringer* const pealRinger = database->RingersDatabase().FindRinger(ringerId);
            Assert::AreEqual(expectedName.c_str(), pealRinger->FullName(database->Settings().LastNameFirst()).c_str());
        }

        void CheckPeals() const
        {
            PerformanceDate peal18Date(2005, 12, 27);
            ObjectId peal141Id = database->PealsDatabase().FindPeal(peal18Date);
            const Peal* const peal141 = database->PealsDatabase().FindPeal(peal141Id);
            Assert::IsNotNull(peal141);
            Assert::AreEqual(L"Suffolk Guild", peal141->AssociationName(*database).c_str());
            Assert::IsFalse(peal141->Handbell());
            const Tower* const peal141Tower72 = database->TowersDatabase().FindTower(peal141->TowerId());
            Assert::AreEqual(L"Minenho-le-Tower", peal141Tower72->Name().c_str());
            Assert::AreEqual(L"Newmarket", peal141Tower72->City().c_str());
            Assert::AreEqual(L"Suffolk", peal141Tower72->County().c_str());
            Assert::AreEqual(PerformanceTime(128), peal141->Time());
            Assert::AreEqual((unsigned int)5088, peal141->NoOfChanges());
            const Method* const peal141Method = database->MethodsDatabase().FindMethod(peal141->MethodId());
            Assert::AreEqual(L"Yorkshire Surprise Major", peal141Method->FullName(database->MethodsDatabase()).c_str());
            Assert::AreEqual(L"A Craven", peal141->Composer().c_str());
            database->Settings().SetLastNameFirst(false);

            FindCheckRinger(*peal141, 1, false, L"Michael J Edwards");
            FindCheckRinger(*peal141, 2, false, L"Peter J Waterfield");
            FindCheckRinger(*peal141, 3, false, L"Mary E Dunbavin");
            FindCheckRinger(*peal141, 4, false, L"Christopher M Movley");
            FindCheckRinger(*peal141, 5, false, L"Richard A Knight");
            FindCheckRinger(*peal141, 6, false, L"Phillip J Wilding");
            FindCheckRinger(*peal141, 7, false, L"David J Salter");
            FindCheckRinger(*peal141, 8, false, L"Ian M Holland");
            Assert::AreEqual(L"", peal141->Footnotes().c_str());
            Assert::AreEqual(L"Ian M Holland", peal141->ConductorName(*database).c_str());

            PerformanceDate peal1Date(1992, 12, 31);
            ObjectId peal1Id = database->PealsDatabase().FindPeal(peal1Date);
            const Peal* const peal1 = database->PealsDatabase().FindPeal(peal1Id);
            Assert::IsNotNull(peal1);
            Assert::AreEqual(L"Guildford Diocesan Guild", peal1->AssociationName(*database).c_str());
            Assert::IsFalse(peal1->Handbell());
            const Tower* const peal1Tower1 = database->TowersDatabase().FindTower(peal1->TowerId());
            Assert::AreEqual(L"St Mary V", peal1Tower1->Name().c_str());
            Assert::AreEqual(L"Shalford", peal1Tower1->City().c_str());
            Assert::AreEqual(L"Surrey", peal1Tower1->County().c_str());
            Assert::AreEqual(PerformanceTime(167), peal1->Time());
            Assert::AreEqual((unsigned int)5104, peal1->NoOfChanges());
            const Method* const peal1Method = database->MethodsDatabase().FindMethod(peal1->MethodId());
            Assert::AreEqual(L"Iodine Surprise Major", peal1Method->FullName(database->MethodsDatabase()).c_str());
            Assert::AreEqual(L"T G Pett", peal1->Composer().c_str());

            FindCheckRinger(*peal1, 1, false, L"Christopher M Movley");
            FindCheckRinger(*peal1, 2, false, L"Anne M Anthony");
            FindCheckRinger(*peal1, 3, false, L"Beryl R Norris");
            FindCheckRinger(*peal1, 4, false, L"Graham L Nayler");
            FindCheckRinger(*peal1, 5, false, L"Glenn J Poyntz");
            FindCheckRinger(*peal1, 6, false, L"Robin J Walker");
            FindCheckRinger(*peal1, 7, false, L"W John Couperthwaite");
            FindCheckRinger(*peal1, 8, false, L"Mike Pidd");
            Assert::AreEqual(L"", peal1->Footnotes().c_str());
            Assert::AreEqual(L"Mike Pidd", peal1->ConductorName(*database).c_str());
        }

        TEST_METHOD(WinRkVersion1ImporterImporter_Import_NonExistingFile)
        {
            expectedError = true;
            Assert::IsFalse(importer->Import("wibble.plf"));

            Assert::IsTrue(completed);
            Assert::AreEqual(0, previousPercent);
        }

        BEGIN_TEST_METHOD_ATTRIBUTE(WinRkVersion1ImporterImporter_Import_Cancel)
            TEST_PRIORITY(2)
        END_TEST_METHOD_ATTRIBUTE()
        TEST_METHOD(WinRkVersion1ImporterImporter_Import_Cancel)
        {
            cancelWhenAbove = 27;
            Assert::IsFalse(importer->Import("Christopher Movley peals.plf"));
            Assert::IsFalse(completed);
            Assert::IsTrue(previousPercent < 100);
            Assert::IsTrue(previousPercent > cancelWhenAbove);
        }

        BEGIN_TEST_METHOD_ATTRIBUTE(WinRkVersion1ImporterImporter_ImportChristopherMovley)
            TEST_PRIORITY(2)
        END_TEST_METHOD_ATTRIBUTE()
        TEST_METHOD(WinRkVersion1ImporterImporter_ImportChristopherMovley)
        {
            Duco::StatisticFilters filters(*database);
            Assert::IsTrue(importer->Import("Christopher Movley peals.plf"));
            Assert::IsTrue(completed);
            Assert::IsTrue(previousPercent >= 28);
            Assert::AreEqual((size_t)142, database->PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)17, database->NumberOfAssociations());
            Assert::AreEqual((size_t)47, database->NumberOfMethods()); // Counted by hand to be 46
            Assert::AreEqual((size_t)69, database->NumberOfTowers());
            Assert::AreEqual((size_t)415, database->NumberOfRingers());

            filters.SetIncludeHandBell(true);
            filters.SetIncludeTowerBell(false);
            Assert::AreEqual((size_t)2, database->PealsDatabase().PerformanceInfo(filters).TotalPeals()); // handbell peal count
            filters.SetIncludeHandBell(false);
            filters.SetIncludeTowerBell(true);
            Assert::AreEqual((size_t)140, database->PealsDatabase().PerformanceInfo(filters).TotalPeals());

            CheckPeals();

            Assert::IsTrue(database->RebuildDatabase(this));

            CheckPeals();

            //database->Externalise("Christopher Movley peals.duc", this);
        }
    };
}
