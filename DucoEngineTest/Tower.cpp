﻿#include "CppUnitTest.h"
#include <DatabaseSettings.h>
#include <Tower.h>
#include <TowerDatabase.h>
#include <Ring.h>
#include <Peal.h>
#include <PealDatabase.h>
#include <RingingDatabase.h>
#include "ToString.h"
#include <StatisticFilters.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(Tower_UnitTests)
    {
    public:
        TEST_METHOD(DatabaseReader_SingleTower)
        {
            RingingDatabase database;
            Duco::StatisticFilters filters(database);
            unsigned int databaseVersion = 0;
            database.Internalise("Single_tower.duc", NULL, databaseVersion);

            // Check counts of all objects
            Assert::AreEqual((size_t)0, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)0, database.NumberOfRingers());
            Assert::AreEqual((size_t)1, database.NumberOfTowers());
            size_t noOfLinkedTowers = 0;
            Assert::AreEqual((size_t)1, database.NumberOfActiveTowers(noOfLinkedTowers));
            Assert::AreEqual((size_t)0, noOfLinkedTowers);
            Assert::AreEqual((size_t)0, database.NumberOfRemovedTowers());
            Assert::AreEqual((size_t)0, database.NumberOfMethods());
            Assert::AreEqual((size_t)0, database.NumberOfMethodSeries());
            Assert::AreEqual((size_t)0, database.NumberOfCompositions());
            Assert::AreEqual((size_t)0, database.NumberOfAssociations());

            //Check number of bells and ringers
            const Tower* const theTower = database.TowersDatabase().FindTower(1);

            Assert::AreEqual(L"St Peter", theTower->Name().c_str());
            Assert::AreEqual(L"Burnley", theTower->City().c_str());
            Assert::AreEqual(L"Lancashire", theTower->County().c_str());
            Assert::AreEqual((unsigned int)10, theTower->Bells());
            Assert::AreEqual((size_t)4, theTower->NoOfRings());
            Assert::AreEqual(L"BURNLEY", theTower->DoveRef().c_str());
            Assert::AreEqual(L"863", theTower->TowerbaseId().c_str());
            Assert::AreEqual((size_t)4, theTower->NextFreeRingId().Id());
            Assert::AreEqual(L"18-0-8", theTower->HeaviestTenor().c_str());
            Assert::IsFalse(theTower->Removed());
        }

        TEST_METHOD(DatabaseReader_SingleTower_CheckRings)
        {
            RingingDatabase database;
            unsigned int databaseVersion = 0;
            database.Internalise("Single_tower.duc", NULL, databaseVersion);
            const Tower* const theTower = database.TowersDatabase().FindTower(1);

            Assert::AreEqual((unsigned int)10, theTower->NoOfBellsInRing(0));
            Assert::AreEqual((unsigned int)8, theTower->NoOfBellsInRing(1));
            Assert::AreEqual((unsigned int)6, theTower->NoOfBellsInRing(2));
            Assert::AreEqual((unsigned int)6, theTower->NoOfBellsInRing(3));

            ObjectId ringId(1);
            Assert::IsFalse(theTower->FirstInvalidRingId(ringId));
            Assert::AreEqual((size_t)KNoId, ringId.Id());

            Assert::IsTrue(theTower->SuggestRing(10, L"18-0-8", L"", ringId, false));
            Assert::AreEqual(Duco::ObjectId(0), ringId);
            Assert::IsTrue(theTower->SuggestRing(8, L"18-0-8", L"", ringId, false));
            Assert::AreEqual(Duco::ObjectId(1), ringId);
            Assert::IsTrue(theTower->SuggestRing(6, L"6-1-22", L"", ringId, false));
            Assert::AreEqual(Duco::ObjectId(2), ringId);
            Assert::IsTrue(theTower->SuggestRing(6, L"18-0-8", L"", ringId, false));
            Assert::AreEqual(Duco::ObjectId(3), ringId);

            std::wstring ringName;
            Assert::IsFalse(theTower->GetRingNameWithNoOfBells(12, ringName));
            Assert::AreEqual(L"", ringName.c_str());
            Assert::IsTrue(theTower->GetRingNameWithNoOfBells(10, ringName));
            Assert::AreEqual(L"10 bell peal", ringName.c_str());
            Assert::IsTrue(theTower->GetRingNameWithNoOfBells(8, ringName));
            Assert::AreEqual(L"Back eight", ringName.c_str());
            Assert::IsTrue(theTower->GetRingNameWithNoOfBells(6, ringName));
            Assert::AreEqual(L"Front six", ringName.c_str());
            Assert::IsFalse(theTower->GetRingNameWithNoOfBells(4, ringName));
            Assert::AreEqual(L"", ringName.c_str());
        }

        TEST_METHOD(Tower_SuggestRing_NoTenorKeyOrWeight_10Bells)
        {
            Tower newTower(KNoId, L"St Peter", L"Burnley", L"Lancashire", 10);
            Duco::ObjectId ringId = newTower.AddRing(10, L"18-0-8", L"D");

            Duco::ObjectId newRingId;
            Assert::IsTrue(newTower.SuggestRing(10, L"", L"", newRingId, false));
            Assert::AreEqual(ringId, newRingId);
        }

        TEST_METHOD(Tower_SuggestRing_NoTenorKeyOrWeight_6Bells)
        {
            Tower newTower(KNoId, L"St Peter", L"Burnley", L"Lancashire", 10);
            Duco::ObjectId ringId = newTower.AddRing(6, L"18-0-8", L"D");
            newTower.AddRing(10, L"18-0-8", L"D");

            Duco::ObjectId newRingId;
            Assert::IsTrue(newTower.SuggestRing(6, L"", L"", newRingId, false));
            Assert::AreEqual(ringId, newRingId);
        }

        TEST_METHOD(Tower_SuggestRing_ByTenorWeight)
        {
            std::wstring tenorWeight = L"25-2-6";
            std::wstring tenorKey = L"E♭";
            Tower newTower(KNoId, L"St Werburgh", L"Warburton", L"Greater Manchester", 8);
            Duco::ObjectId correctRingId = newTower.AddRing(8, tenorWeight, tenorKey);
            newTower.AddRing(6, L"14-1-1", L"C");

            Duco::ObjectId ringId;
            Assert::IsTrue(newTower.SuggestRing(8, tenorWeight, tenorKey, ringId, false));
            Assert::AreEqual(correctRingId, ringId);
            Assert::IsFalse(newTower.SuggestRing(10, tenorWeight, tenorKey, ringId, false));
            Assert::AreEqual((size_t)KNoId, ringId.Id());
            Assert::IsTrue(newTower.SuggestRing(8, L"25", tenorKey, ringId, false));
            Assert::AreEqual(correctRingId, ringId);
            Assert::IsTrue(newTower.SuggestRing(8, L"26", tenorKey, ringId, false));
            Assert::AreEqual(correctRingId, ringId);
        }
        TEST_METHOD(Ring_TenorKeyWithFlatCharacter)
        {
            std::set<unsigned int> newBells;
            std::wstring tenorKey = L"Gb";

            Ring newRing(KNoId, 6, L"Back 6", newBells, L"25–2–6", tenorKey);

            Assert::AreEqual(std::wstring(L"G♭"), newRing.TenorKey());
        }

        TEST_METHOD(Ring_LongTenorKey)
        {
            std::wstring tenorKey = L"ABCDEFG";
            std::set<unsigned int> newBells;
            Ring newRing(KNoId, 6, L"Back 6", newBells, L"25–2–6", tenorKey);
            Assert::AreEqual(std::wstring(L"A"), newRing.TenorKey());
        }

        TEST_METHOD(Ring_SharpSymbol)
        {
            std::set<unsigned int> newBells;
            std::wstring tenorKey = L"a#";
            Ring newRing(KNoId, 6, L"Back 6", newBells, L"25–2–6", tenorKey);

            Assert::AreEqual(std::wstring(L"A♯"), newRing.TenorKey());
        }

        TEST_METHOD(Ring_InvalidSymbol)
        {
            std::wstring tenorKey = L"TEST";
            std::set<unsigned int> newBells;
            Ring newRing(KNoId, 6, L"Back 6", newBells, L"25–2–6", tenorKey);

            Assert::AreEqual(std::wstring(L""), newRing.TenorKey());
        }

        TEST_METHOD(Tower_SuggestTower_SingleTower)
        {
            Tower newTower(KNoId, L"Cath Ch of S John", L"Brisbane", L"Qld", 12);
            newTower.AddRing(12, L"16-1-17", L"E");

            TowerDatabase database;
            database.AddObject(newTower);

            Assert::AreEqual((size_t)1, database.NumberOfObjects());
            Assert::IsTrue(database.SuggestTower(L"Cath", L"Brisbane", L"Qld", 12, L"16-1-17", L"").ValidId());
            Assert::IsTrue(database.SuggestTower(L"Cath", L"Brisbane", L"Qld", 12, L"16", L"").ValidId());
        }
        TEST_METHOD(Tower_SuggestTower_TwoTowers)
        {
            Tower tower1(KNoId, L"Cath Ch of S John", L"Brisbane", L"Qld", 12);
            tower1.AddRing(12, L"16-1-17", L"Ab");
            Tower tower2(KNoId, L"Cath Ch of S John", L"Brisbane", L"Qld", 6);
            tower2.AddRing(6, L"16-1-17", L"Ab");

            TowerDatabase database;
            Duco::ObjectId tower1Id = database.AddObject(tower1);
            Duco::ObjectId tower2Id = database.AddObject(tower2);

            Assert::AreEqual((size_t)2, database.NumberOfObjects());
            Assert::AreEqual(tower1Id, database.SuggestTower(L"Cath", L"Brisbane", L"Qld", 12, L"16-1-17", L"A"));
            Assert::AreEqual(tower2Id, database.SuggestTower(L"Cath", L"Brisbane", L"Qld", 6, L"16-1-17", L""));
        }

        TEST_METHOD(Tower_SuggestRingAtWorsley)
        {
            Tower worsley(KNoId, L"St Mark", L"WORSLEY", L"Greater Manchester", 10);
            ObjectId allTen = worsley.AddRing(10, L"24cwt", L"D");
            ObjectId backEight = worsley.AddRing(8, L"24-0-7", L"D");

            ObjectId ringId;
            Assert::IsTrue(worsley.SuggestRing(10, L"24-0-7", L"D", ringId, false));
            Assert::AreEqual(allTen, ringId);
        }

        TEST_METHOD(Tower_FirstRungDateNotSet)
        {
            Tower worsley(KNoId, L"St Mark", L"WORSLEY", L"Greater Manchester", 10);
            Assert::IsFalse(worsley.FirstRungDateSet());
        }

        TEST_METHOD(Tower_FirstRungDateSetWithoutPeal)
        {
            RingingDatabase database;

            Tower worsley(KNoId, L"St Mark", L"WORSLEY", L"Greater Manchester", 10);
            PerformanceDate now;
            worsley.SetFirstRungDate(now);

            Assert::IsTrue(worsley.FirstRungDateSet());
            bool byPeal = false;
            Assert::IsTrue(worsley.FirstRungDateSet(database, byPeal));
            Assert::IsFalse(byPeal);
            Assert::AreEqual(now, worsley.FirstRungDate());
        }

        TEST_METHOD(Tower_FirstRungDateByPeal)
        {
            RingingDatabase database;

            Tower worsley(KNoId, L"St Mark", L"WORSLEY", L"Greater Manchester", 10);
            Duco::ObjectId towerId = database.TowersDatabase().AddObject(worsley);
            PerformanceDate pealDate(2010, 1, 1);
            Assert::IsFalse(worsley.FirstRungDateSet());

            Peal thePeal;
            thePeal.SetTowerId(towerId);
            thePeal.SetDate(pealDate);
            Duco::ObjectId pealId = database.PealsDatabase().AddObject(thePeal);

            const Duco::Tower* const worsleyFromDatabase = database.TowersDatabase().FindTower(towerId);

            Assert::IsFalse(worsleyFromDatabase->FirstRungDateSet());
            bool byPeal = false;
            Assert::IsTrue(worsleyFromDatabase->FirstRungDateSet(database, byPeal));
            Assert::IsTrue(byPeal);
        }

        TEST_METHOD(Tower_FirstRungDateByEarlierPeal)
        {
            RingingDatabase database;

            Tower worsley(KNoId, L"St Mark", L"WORSLEY", L"Greater Manchester", 10);
            worsley.SetFirstRungDate(PerformanceDate());
            Duco::ObjectId towerId = database.TowersDatabase().AddObject(worsley);
            PerformanceDate pealDate(2010, 1, 1);
            Assert::IsTrue(worsley.FirstRungDateSet());

            Peal thePeal;
            thePeal.SetTowerId(towerId);
            thePeal.SetDate(pealDate);
            Duco::ObjectId pealId = database.PealsDatabase().AddObject(thePeal);

            const Duco::Tower* const worsleyFromDatabase = database.TowersDatabase().FindTower(towerId);

            Assert::IsTrue(worsleyFromDatabase->FirstRungDateSet());
            bool byPeal = false;
            Assert::IsTrue(worsleyFromDatabase->FirstRungDateSet(database, byPeal));
            Assert::IsTrue(byPeal);
        }

        TEST_METHOD(Tower_FirstRungDateWithLatestEarlierPeal)
        {
            RingingDatabase database;

            PerformanceDate rungDate(2010, 1, 1);
            Tower worsley(KNoId, L"St Mark", L"WORSLEY", L"Greater Manchester", 10);
            worsley.SetFirstRungDate(rungDate);
            Duco::ObjectId towerId = database.TowersDatabase().AddObject(worsley);
            Assert::IsTrue(worsley.FirstRungDateSet());

            Peal thePeal;
            thePeal.SetTowerId(towerId);
            thePeal.SetDate(PerformanceDate());
            Duco::ObjectId pealId = database.PealsDatabase().AddObject(thePeal);

            const Duco::Tower* const worsleyFromDatabase = database.TowersDatabase().FindTower(towerId);

            Assert::IsTrue(worsleyFromDatabase->FirstRungDateSet());
            bool byPeal = false;
            Assert::IsTrue(worsleyFromDatabase->FirstRungDateSet(database, byPeal));
            Assert::IsFalse(byPeal);
        }

        TEST_METHOD(Tower_FindUnusedTowersEmptyDatabase)
        {
            std::set<Duco::ObjectId> unusedTowers;
            RingingDatabase database;

            database.TowersDatabase().RemoveUsedIds(unusedTowers, database.Settings());
            Assert::AreEqual((size_t)0, unusedTowers.size());
        }

        TEST_METHOD(Tower_FindUnusedTowersNoPeals)
        {
            RingingDatabase database;

            PerformanceDate rungDate(2010, 1, 1);
            Tower worsley(KNoId, L"St Mark", L"WORSLEY", L"Greater Manchester", 10);
            Duco::ObjectId worsleyId = database.TowersDatabase().AddObject(worsley);
            std::set<Duco::ObjectId> unusedTowers = { worsleyId };

            database.TowersDatabase().RemoveUsedIds(unusedTowers, database.Settings());

            Assert::AreEqual((size_t)1, unusedTowers.size());
            Assert::AreEqual(worsleyId, *unusedTowers.begin());
        }

        TEST_METHOD(Tower_FindUnusedTowersNoPeals_RungDateSet_DontKeep)
        {
            RingingDatabase database;
            database.Settings().SetKeepTowersWithRungDate(false);

            PerformanceDate rungDate(2010, 1, 1);
            Tower worsley(KNoId, L"St Mark", L"WORSLEY", L"Greater Manchester", 10);
            Duco::ObjectId worsleyId = database.TowersDatabase().AddObject(worsley);
            std::set<Duco::ObjectId> unusedTowers = { worsleyId };

            database.TowersDatabase().RemoveUsedIds(unusedTowers, database.Settings());

            Assert::AreEqual((size_t)1, unusedTowers.size());
            Assert::AreEqual(worsleyId, *unusedTowers.begin());
        }

        TEST_METHOD(Tower_FindUnusedTowersNoPeals_RungDateSet_Keep)
        {
            RingingDatabase database;
            database.Settings().SetKeepTowersWithRungDate(true);

            PerformanceDate rungDate(2010, 1, 1);
            Tower worsley(KNoId, L"St Mark", L"WORSLEY", L"Greater Manchester", 10);
            worsley.SetFirstRungDate(rungDate);
            Duco::ObjectId worsleyId = database.TowersDatabase().AddObject(worsley);
            std::set<Duco::ObjectId> unusedTowers = { worsleyId };

            database.TowersDatabase().RemoveUsedIds(unusedTowers, database.Settings());

            Assert::AreEqual((size_t)0, unusedTowers.size());
        }


        TEST_METHOD(Tower_FindUnusedTowersNoPeals_LinkedTowers)
        {
            RingingDatabase database;

            PerformanceDate rungDate(2010, 1, 1);
            Tower oldNorthampton(KNoId, L"All Saints (oldBells)", L"NORTHAMPTON", L"Greater Manchester", 8);
            Duco::ObjectId oldNorthamptonId = database.TowersDatabase().AddObject(oldNorthampton);
            Tower newNorthampton(KNoId, L"All Saints (newBells)", L"NORTHAMPTON", L"Greater Manchester", 10);
            newNorthampton.SetLinkedTowerId(oldNorthamptonId);
            Duco::ObjectId newNorthamptonId = database.TowersDatabase().AddObject(newNorthampton);
            std::set<Duco::ObjectId> unusedTowers = { oldNorthamptonId, newNorthamptonId };

            database.TowersDatabase().RemoveUsedIds(unusedTowers, database.Settings());

            Assert::AreEqual((size_t)0, unusedTowers.size());
        }

        TEST_METHOD(Tower_FindUnusedTowers)
        {
            std::set<Duco::ObjectId> unusedList = { };
            std::multimap<Duco::ObjectId, Duco::ObjectId> unusedMap = { };
            RingingDatabase database;

            Tower worsley(KNoId, L"St Mark", L"WORSLEY", L"Greater Manchester", 10);
            Duco::ObjectId worsleyId = database.TowersDatabase().AddObject(worsley);
            Tower walkden(KNoId, L"St Paul", L"WALKDEN", L"Greater Manchester", 10);
            Duco::ObjectId walkdenId = database.TowersDatabase().AddObject(walkden);
            std::set<Duco::ObjectId> unusedTowers = { worsleyId, walkdenId };

            Peal thePeal;
            thePeal.SetTowerId(worsleyId);
            Duco::ObjectId pealId = database.PealsDatabase().AddObject(thePeal);
            database.PealsDatabase().RemoveUsedIds(unusedList, unusedList, unusedTowers, unusedMap, unusedList, unusedList, unusedList);

            Assert::AreEqual((size_t)1, unusedTowers.size());
            Assert::AreEqual(walkdenId, *unusedTowers.begin());
        }

        TEST_METHOD(Tower_RingOnBackBells)
        {
            Tower worsley(KNoId, L"St Mark", L"WORSLEY", L"Greater Manchester", 10);
            ObjectId back10Id = worsley.AddRing(10, L"3-3-3", L"");
            ObjectId back8Id = worsley.AddRing(8, L"3-3-3", L"");
            ObjectId back6Id = worsley.AddRing(6, L"3-3-3", L"");
            Ring back6 = *worsley.FindRing(back6Id);
            Assert::IsTrue(worsley.UpdateRing(back6.Id(), back6));
            ObjectId front6Id = worsley.AddRing(6, L"1-2-3", L"");
            Ring front6 = *worsley.FindRing(front6Id);
            front6.ChangeBell(1, true);
            front6.ChangeBell(2, true);
            front6.ChangeBell(3, true);
            front6.ChangeBell(4, true);
            front6.ChangeBell(5, true);
            front6.ChangeBell(6, true);
            front6.ChangeBell(7, false);
            front6.ChangeBell(8, false);
            front6.ChangeBell(9, false);
            front6.ChangeBell(10, false);
            Assert::IsTrue(worsley.UpdateRing(front6.Id(), front6));

            Assert::IsFalse(worsley.RingOnBackBells(front6));
            Assert::IsTrue(worsley.RingOnBackBells(back6Id));
            Assert::IsTrue(worsley.RingOnBackBells(back10Id));
        }

        TEST_METHOD(Ring_FindHeaviestTenorKey)
        {
            Tower newTower(KNoId, L"", L"", L"", 8);

            std::wstring tenorKey = L"G♭";
            {
                std::set<unsigned int> newBells;
                newBells.insert(3);
                newBells.insert(4);
                newBells.insert(5);
                newBells.insert(6);
                newBells.insert(7);
                newBells.insert(8);
                Ring newRing(KNoId, 6, L"Back 6", newBells, L"25–2–6", tenorKey);
                newTower.AddRing(newRing);
            }
            {
                std::set<unsigned int> otherBells;
                otherBells.insert(1);
                otherBells.insert(2);
                otherBells.insert(3);
                otherBells.insert(4);
                otherBells.insert(5);
                otherBells.insert(6);
                Ring newRing(KNoId, 6, L"Front 6", otherBells, L"25–2–6", L"A");
                newTower.AddRing(newRing);
            }
            Assert::AreEqual(std::wstring(tenorKey), newTower.TowerKey());
        }

        TEST_METHOD(Tower_InvalidCharactersInTenorWeight)
        {
            RingingDatabase database;
            Tower newTower(KNoId, L"Minster Ch of S Margaret", L"King's Lynn", L"Norfolk", 12);
            newTower.SetTowerbaseId(L"1001");
            ObjectId back10Id = newTower.AddRing(10, L"13–2–1", L"A");
            ObjectId back9Id = newTower.AddRing(9, L"13¼", L"A");
            ObjectId back8Id = newTower.AddRing(8, L"13½ ", L"A");
            ObjectId back7Id = newTower.AddRing(7, L" 13¾", L"A");
            ObjectId back6Id = newTower.AddRing(6, L"13  ", L"A");
            ObjectId back4Id = newTower.AddRing(4, L"13lbs", L"A");
            ObjectId back2Id = newTower.AddRing(2, L"13cwt", L"A");

            Assert::IsTrue(newTower.Valid(database, true, true));
            Assert::AreEqual((size_t)7, newTower.NoOfRings());
            Assert::AreEqual(L"13-2-1", newTower.FindRing(back10Id)->TenorWeight().c_str());
            Assert::AreEqual(L"13¼", newTower.FindRing(back9Id)->TenorWeight().c_str());
            Assert::AreEqual(L"13½", newTower.FindRing(back8Id)->TenorWeight().c_str());
            Assert::AreEqual(L"13¾", newTower.FindRing(back7Id)->TenorWeight().c_str());
            Assert::AreEqual(L"13", newTower.FindRing(back6Id)->TenorWeight().c_str());
            Assert::AreEqual(L"13lbs", newTower.FindRing(back4Id)->TenorWeight().c_str());
            Assert::AreEqual(L"13cwt", newTower.FindRing(back2Id)->TenorWeight().c_str());
        }

        TEST_METHOD(Tower_TwoDuplicateTowers)
        {
            RingingDatabase ringingDb;
            Tower one;
            one.SetDoveRef(L"TEST");
            Tower two(one);

            Assert::IsTrue(one.Duplicate(two, ringingDb));

            one.SetRemoved(true);
            Assert::IsFalse(one.Duplicate(two, ringingDb));
        }

        TEST_METHOD(Tower_TwoDuplicateTowers_NoNameOrDoveRef)
        {
            RingingDatabase ringingDb;
            Tower one (KNoId, L"St Thomas", L"Heptonstall", L"West Yorkshire", 8);
            Tower two (one);

            Assert::IsTrue(one.Duplicate(two, ringingDb));

            one.SetRemoved(true);
            Assert::IsFalse(one.Duplicate(two, ringingDb));
        }

        TEST_METHOD(Tower_TenorDescription)
        {
            Tower testTower(KNoId, L"Quex", L"Birchington", L"Kent", 12);
            Duco::ObjectId ringId = testTower.AddRing(12, L"14¾ ", L"F");

            Assert::AreEqual(L"14¾ in F",testTower.TenorDescription(ringId).c_str());
        }

        TEST_METHOD(Tower_BellNames_NoRenamedBells)
        {
            Tower theTower(KNoId, L"", L"Halifax", L"West Yorkshire", 14);
            Duco::ObjectId ringId = theTower.AddRing(6, L"1-2-3", L"B");

            std::vector<std::wstring> bells;
            theTower.AllBellNames(bells, ringId);

            Assert::AreEqual((size_t)6, bells.size());
            Assert::AreEqual(L"1", bells[0].c_str());
            Assert::AreEqual(L"2", bells[1].c_str());
            Assert::AreEqual(L"3", bells[2].c_str());
            Assert::AreEqual(L"4", bells[3].c_str());
            Assert::AreEqual(L"5", bells[4].c_str());
            Assert::AreEqual(L"6", bells[5].c_str());
        }

        TEST_METHOD(Tower_BellNames_ExtraTreble)
        {
            Tower theTower(KNoId, L"", L"Halifax", L"West Yorkshire", 14);

            std::set<unsigned int> bells;
            bells.insert(1);
            bells.insert(2);
            bells.insert(3);
            bells.insert(4);
            bells.insert(5);
            bells.insert(6);
            Ring frontSix(1, 6, L"Front six", bells, L"6–2–16", L"D");

            Duco::ObjectId ringId = theTower.AddRing(frontSix);

            std::map<unsigned int, TRenameBellType> renames;
            std::pair<unsigned int, TRenameBellType> renamedBell1(1, TRenameBellType::EExtraTreble);
            renames.insert(renamedBell1);
            std::pair<unsigned int, TRenameBellType> renamedBell2(8, TRenameBellType::EFlat);
            renames.insert(renamedBell2);
            theTower.SetRenamedBells(renames);

            std::vector<std::wstring> bellNames;
            theTower.AllBellNames(bellNames, ringId);

            Assert::AreEqual((size_t)6, bellNames.size());
            Assert::AreEqual(L"1", bellNames[0].c_str());
            Assert::AreEqual(L"2", bellNames[1].c_str());
            Assert::AreEqual(L"3", bellNames[2].c_str());
            Assert::AreEqual(L"4", bellNames[3].c_str());
            Assert::AreEqual(L"5", bellNames[4].c_str());
            Assert::AreEqual(L"6", bellNames[5].c_str());
        }

        TEST_METHOD(Tower_BellNames_Flat6th)
        {
            Tower theTower(KNoId, L"", L"Halifax", L"West Yorkshire", 14);

            std::set<unsigned int> bells;
            bells.insert(1);
            bells.insert(2);
            bells.insert(3);
            bells.insert(4);
            bells.insert(5);
            bells.insert(6);
            bells.insert(8);
            bells.insert(9);
            bells.insert(10);
            bells.insert(11);
            Ring lightTen(1, 10, L"Front six", bells, L"10-2-12", L"G");

            Duco::ObjectId ringId = theTower.AddRing(lightTen);

            std::map<unsigned int, TRenameBellType> renames;
            std::pair<unsigned int, TRenameBellType> renamedBell1(1, TRenameBellType::EExtraTreble);
            renames.insert(renamedBell1);
            std::pair<unsigned int, TRenameBellType> renamedBell2(8, TRenameBellType::EFlat);
            renames.insert(renamedBell2);
            theTower.SetRenamedBells(renames);

            std::vector<std::wstring> bellNames;
            theTower.AllBellNames(bellNames, ringId);

            Assert::AreEqual((size_t)10, bellNames.size());
            Assert::AreEqual(L"1", bellNames[0].c_str());
            Assert::AreEqual(L"2", bellNames[1].c_str());
            Assert::AreEqual(L"3", bellNames[2].c_str());
            Assert::AreEqual(L"4", bellNames[3].c_str());
            Assert::AreEqual(L"5", bellNames[4].c_str());
            Assert::AreEqual(L"6", bellNames[5].c_str());
            Assert::AreEqual(L"7", bellNames[6].c_str());
            Assert::AreEqual(L"8", bellNames[7].c_str());
            Assert::AreEqual(L"9", bellNames[8].c_str());
            Assert::AreEqual(L"10", bellNames[9].c_str());
        }

        TEST_METHOD(Tower_BellNames_Allbells_Flat6AndExtraTreble)
        {
            Tower theTower(KNoId, L"", L"Halifax", L"West Yorkshire", 14);

            std::map<unsigned int, TRenameBellType> renames;
            std::pair<unsigned int, TRenameBellType> renamedBell1(1, TRenameBellType::EExtraTreble);
            renames.insert(renamedBell1);
            std::pair<unsigned int, TRenameBellType> renamedBell2(8, TRenameBellType::EFlat);
            renames.insert(renamedBell2);
            theTower.SetRenamedBells(renames);

            std::vector<std::wstring> bellNames;
            theTower.AllBellNames(bellNames, KNoId);

            Assert::AreEqual((size_t)14, bellNames.size());
            Assert::AreEqual(L"0", bellNames[0].c_str());
            Assert::AreEqual(L"1", bellNames[1].c_str());
            Assert::AreEqual(L"2", bellNames[2].c_str());
            Assert::AreEqual(L"3", bellNames[3].c_str());
            Assert::AreEqual(L"4", bellNames[4].c_str());
            Assert::AreEqual(L"5", bellNames[5].c_str());
            Assert::AreEqual(L"6", bellNames[6].c_str());
            Assert::AreEqual(L"6♭", bellNames[7].c_str());
            Assert::AreEqual(L"7", bellNames[8].c_str());
            Assert::AreEqual(L"8", bellNames[9].c_str());
            Assert::AreEqual(L"9", bellNames[10].c_str());
            Assert::AreEqual(L"10", bellNames[11].c_str());
            Assert::AreEqual(L"11", bellNames[12].c_str());
            Assert::AreEqual(L"12", bellNames[13].c_str());
        }

        TEST_METHOD(Tower_BellNames_Allbells_Sharp2nd)
        {
            Tower theTower(KNoId, L"Town hall", L"Manchester", L"Greater Manchester", 13);

            std::map<unsigned int, TRenameBellType> renames;
            std::pair<unsigned int, TRenameBellType> renamedBell1(2, TRenameBellType::ESharp);
            renames.insert(renamedBell1);
            theTower.SetRenamedBells(renames);

            std::vector<std::wstring> bellNames;
            theTower.AllBellNames(bellNames, KNoId);

            Assert::AreEqual((size_t)13, bellNames.size());
            Assert::AreEqual(L"1", bellNames[0].c_str());
            Assert::AreEqual(L"2♯", bellNames[1].c_str());
            Assert::AreEqual(L"2", bellNames[2].c_str());
            Assert::AreEqual(L"3", bellNames[3].c_str());
            Assert::AreEqual(L"4", bellNames[4].c_str());
            Assert::AreEqual(L"5", bellNames[5].c_str());
            Assert::AreEqual(L"6", bellNames[6].c_str());
            Assert::AreEqual(L"7", bellNames[7].c_str());
            Assert::AreEqual(L"8", bellNames[8].c_str());
            Assert::AreEqual(L"9", bellNames[9].c_str());
            Assert::AreEqual(L"10", bellNames[10].c_str());
            Assert::AreEqual(L"11", bellNames[11].c_str());
            Assert::AreEqual(L"12", bellNames[12].c_str());
        }

        TEST_METHOD(Tower_Halifax_CheckbellNames)
        {
            std::wstring tenorWeight = L"28–0–14";
            Tower newTower(KNoId, L"Minster Ch of S John Bapt", L"Halifax", L"West Yorkshire", 14);
            newTower.SetDoveRef(L"HALIFAX");

            std::vector<std::wstring> bellNames;
            newTower.AllBellNames(bellNames, KNoId);
            Assert::AreEqual((size_t)14, bellNames.size());
            Assert::IsTrue(std::find(bellNames.begin(), bellNames.end(), L"1") != bellNames.end());
            Assert::IsTrue(std::find(bellNames.begin(), bellNames.end(), L"12") != bellNames.end());
            Assert::IsTrue(std::find(bellNames.begin(), bellNames.end(), L"14") != bellNames.end());
            Assert::IsFalse(std::find(bellNames.begin(), bellNames.end(), L"6♭") != bellNames.end());

            std::map<unsigned int, Duco::TRenameBellType> renamedBells;
            std::pair<unsigned int, Duco::TRenameBellType> extraTreble(1, Duco::TRenameBellType::EExtraTreble);
            renamedBells.insert(extraTreble);
            std::pair<unsigned int, Duco::TRenameBellType> flat6th(8, Duco::TRenameBellType::EFlat);
            renamedBells.insert(flat6th);
            newTower.SetRenamedBells(renamedBells);

            newTower.AllBellNames(bellNames, KNoId);
            Assert::AreEqual((size_t)14, bellNames.size());
            Assert::IsTrue(std::find(bellNames.begin(), bellNames.end(), L"1") != bellNames.end());
            Assert::IsTrue(std::find(bellNames.begin(), bellNames.end(), L"12") != bellNames.end());
            Assert::IsFalse(std::find(bellNames.begin(), bellNames.end(), L"14") != bellNames.end());
            Assert::IsTrue(std::find(bellNames.begin(), bellNames.end(), L"6♭") != bellNames.end());
        }

        TEST_METHOD(Tower_Halifax_ValidateTower_SameKeyDifferentBell)
        {
            RingingDatabase database;

            std::wstring tenorWeight = L"28–0–14";
            Tower newTower(KNoId, L"Minster Ch of S John Bapt", L"Halifax", L"West Yorkshire", 14);
            newTower.SetTowerbaseId(L"1001");

            Duco::ObjectId backTwelveRingId = newTower.AddRing(12, tenorWeight, L"D");
            Duco::ObjectId backEightRingId = newTower.AddRing(8, L"1–2–3", L"A");
            Duco::ObjectId backSixRingId = newTower.AddRing(6, L"", L"D");

            Duco::ObjectId lightTenRingId = newTower.NextFreeRingId();
            std::set<unsigned int> lightTenBells;
            lightTenBells.insert(1);
            lightTenBells.insert(2);
            lightTenBells.insert(3);
            lightTenBells.insert(4);
            lightTenBells.insert(5);
            lightTenBells.insert(6);
            lightTenBells.insert(8);
            lightTenBells.insert(9);
            lightTenBells.insert(10);
            lightTenBells.insert(11);
            Ring lightTen(lightTenRingId, 10, L"Light ten", lightTenBells, L"10–2–12", L"D");
            Assert::IsTrue(newTower.AddRing(lightTen));
            Assert::AreEqual(6u, newTower.MinimumNoOfBells());

            database.TowersDatabase().AddObject(newTower);

            Assert::IsFalse(newTower.Valid(database, true, true));
            Assert::IsTrue(newTower.ErrorCode().test(ERingWarning_PossibleInvalidTenorKey));
            Assert::AreEqual(L"Invalid ring tenor weight, Check ring tenor keys - one is the same as the tower tenor but a different bell?", newTower.ErrorString(database.Settings(), true).c_str());
        }

        TEST_METHOD(Tower_Halifax_ValidateTower_SameBellDifferentKeys)
        {
            RingingDatabase database;

            std::wstring tenorWeight = L"28–0–14";
            Tower newTower(KNoId, L"Minster Ch of S John Bapt", L"Halifax", L"West Yorkshire", 14);
            newTower.SetTowerbaseId(L"1001");

            Duco::ObjectId backTwelveRingId = newTower.AddRing(12, tenorWeight, L"D");
            Duco::ObjectId backEightRingId = newTower.AddRing(8, L"1–2–3", L"A");

            database.TowersDatabase().AddObject(newTower);

            Assert::IsFalse(newTower.Valid(database, true, true));
            Assert::IsTrue(newTower.ErrorCode().test(ERingWarning_PossibleInvalidTenorKey));
            Assert::AreEqual(L"Check ring tenor keys - one is the same as the tower tenor but a different bell?", newTower.ErrorString(database.Settings(), true).c_str());
        }

        TEST_METHOD(Tower_Halifax_OldSytleDoveRef)
        {
            RingingDatabase database;

            std::wstring tenorWeight = L"28–0–14";
            Tower newTower(KNoId, L"Minster Ch of S John Bapt", L"Halifax", L"West Yorkshire", 14);
            newTower.SetDoveRef(L"HALIFAX");

            database.TowersDatabase().AddObject(newTower);

            Assert::IsFalse(newTower.Valid(database, true, true));
            Assert::IsTrue(newTower.ErrorCode().test(ETowerWarning_DoveContainsOldChars));
            Assert::IsTrue(newTower.ErrorString(database.Settings(), true).find(L"Dove ref should now only be using the new numerical ids") != std::wstring::npos);
        }

        TEST_METHOD(Tower_Halifax_NewSytleDoveRef)
        {
            RingingDatabase database;

            std::wstring tenorWeight = L"28–0–14";
            Tower newTower(KNoId, L"Minster Ch of S John Bapt", L"Halifax", L"West Yorkshire", 14);
            newTower.SetDoveRef(L"11592");
            newTower.SetTowerbaseId(L"1001");

            database.TowersDatabase().AddObject(newTower);

            Assert::IsFalse(newTower.Valid(database, true, true));
            Assert::IsFalse(newTower.ErrorCode().test(ETowerWarning_DoveContainsOldChars));
            Assert::AreEqual(L"No rings specified", newTower.ErrorString(database.Settings(), true).c_str());
        }

        TEST_METHOD(Tower_DifferentPosition_NonSet)
        {
            Tower tower1(KNoId, L"A", L"A", L"A", 8);
            tower1.SetLatitude(L"1.1");
            tower1.SetLongitude(L"2.2");
            Tower tower2(KNoId, L"A", L"A", L"A", 8);

            Assert::IsFalse(tower1.HasDifferentPosition(tower2));
        }

        TEST_METHOD(Tower_DifferentPosition_SamePosition)
        {
            Tower tower1(KNoId, L"A", L"A", L"A", 8);
            tower1.SetLatitude(L"1.1");
            tower1.SetLongitude(L"2.2");
            Tower tower2(KNoId, L"A", L"A", L"A", 8);
            tower2.SetLatitude(L"1.1");
            tower2.SetLongitude(L"2.2");

            Assert::IsFalse(tower1.HasDifferentPosition(tower2));
        }

        TEST_METHOD(Tower_DifferentPosition_EquivalentPositions)
        {
            Tower tower1(KNoId, L"A", L"A", L"A", 8);
            tower1.SetLatitude(L"1.10");
            tower1.SetLongitude(L"2.2");
            Tower tower2(KNoId, L"A", L"A", L"A", 8);
            tower2.SetLatitude(L"1.1");
            tower2.SetLongitude(L"2.20");

            Assert::IsFalse(tower1.HasDifferentPosition(tower2));
        }

        TEST_METHOD(Tower_DifferentPosition_DifferentPosition)
        {
            Tower tower1(KNoId, L"A", L"A", L"A", 8);
            tower1.SetLatitude(L"1.2");
            tower1.SetLongitude(L"2.3");
            Tower tower2(KNoId, L"A", L"A", L"A", 8);
            tower2.SetLatitude(L"1.1");
            tower2.SetLongitude(L"2.2");

            Assert::IsTrue(tower1.HasDifferentPosition(tower2));
        }

        TEST_METHOD(Tower_DoveReferenceLink_OldFormat)
        {
            RingingDatabase database;
            database.Settings().ResetWebsiteLinks();

            Tower theTower;
            theTower.SetDoveRef(L"ANSTEY LCS");
            Assert::AreEqual(L"https://dove.cccbr.org.uk/detail.php?DoveID=ANSTEY LCS", theTower.DoveUrl(database.Settings()).c_str());
        }

        TEST_METHOD(Tower_DoveReferenceLink_NewFormat)
        {
            RingingDatabase database;
            database.Settings().ResetWebsiteLinks();

            Tower theTower;
            theTower.SetDoveRef(L"16503");

            Assert::AreEqual(L"https://dove.cccbr.org.uk/tower/16503", theTower.DoveUrl(database.Settings()).c_str());
        }

        TEST_METHOD(Tower_WorcesterCathedralBells)
        {
            Tower theTower;
            theTower.SetNoOfBells(16);

            std::map<unsigned int, TRenameBellType> renamedbells;
            std::pair<unsigned int, TRenameBellType> sharpSecond(2, TRenameBellType::ESharp);
            renamedbells.insert(sharpSecond);
            std::pair<unsigned int, TRenameBellType> sharpFifth(6, TRenameBellType::ESharp);
            renamedbells.insert(sharpFifth);
            std::pair<unsigned int, TRenameBellType> flatSixth(9, TRenameBellType::EFlat);
            renamedbells.insert(flatSixth);
            std::pair<unsigned int, TRenameBellType> sharpNinth(12, TRenameBellType::ESharp);
            renamedbells.insert(sharpNinth);
            theTower.SetRenamedBells(renamedbells);

            std::vector<std::wstring> expectedBellNames;
            expectedBellNames.push_back(L"1");
            expectedBellNames.push_back(L"2♯");
            expectedBellNames.push_back(L"2");
            expectedBellNames.push_back(L"3");
            expectedBellNames.push_back(L"4");
            expectedBellNames.push_back(L"5♯");
            expectedBellNames.push_back(L"5");
            expectedBellNames.push_back(L"6");
            expectedBellNames.push_back(L"6♭");
            expectedBellNames.push_back(L"7");
            expectedBellNames.push_back(L"8");
            expectedBellNames.push_back(L"9♯");
            expectedBellNames.push_back(L"9");
            expectedBellNames.push_back(L"10");
            expectedBellNames.push_back(L"11");
            expectedBellNames.push_back(L"12");


            std::vector<std::wstring> bellNames;
            theTower.AllBellNames(bellNames, KNoId);

            Assert::AreEqual(expectedBellNames, bellNames);
        }

        TEST_METHOD(Tower_HandBellTower_NoWarningsOnMissingTowerbaseId)
        {
            RingingDatabase database;

            Tower theTower;
            theTower.SetName(L"Handbells");
            theTower.SetCity(L"Handbells");
            theTower.SetCounty(L"Handbells");
            theTower.SetNoOfBells(8);
            theTower.SetHandbell(true);
            theTower.AddDefaultRing(L"Eight bells", L"12", L"C");

            Assert::IsTrue(theTower.Valid(database, true, true));
            Assert::AreEqual(L"", theTower.ErrorString(database.Settings(), true).c_str());
        }

        TEST_METHOD(Tower_CopyOperator)
        {
            RingingDatabase database;

            Tower theTower;
            theTower.SetHandbell(true);
            theTower.SetRemoved(true);
            theTower.SetNoOfBells(10);

            Tower otherTower;
            Assert::IsFalse(otherTower.Handbell());
            Assert::IsFalse(otherTower.Removed());
            Assert::AreEqual(12u, otherTower.Bells());

            otherTower = theTower;
            Assert::IsTrue(otherTower.Handbell());
            Assert::IsTrue(otherTower.Removed());
            Assert::AreEqual(10u, otherTower.Bells());
        }

        TEST_METHOD(Tower_SwanBells_Valid)
        {
            RingingDatabase database;

            Tower theTower (KNoId, L"The Swan tower", L"PERTH", L"Western Australia", 18);
            theTower.SetTowerbaseId(L"6138");

            std::map<unsigned int, TRenameBellType> renames;
            std::pair<unsigned int, TRenameBellType> renamedBell1(4, TRenameBellType::EFlat);
            renames.insert(renamedBell1);
            std::pair<unsigned int, TRenameBellType> renamedBell2(12, TRenameBellType::EFlat);
            renames.insert(renamedBell2);
            theTower.SetRenamedBells(renames);

            std::vector<std::wstring> expectedBellNames;
            expectedBellNames.push_back(L"1");
            expectedBellNames.push_back(L"2");
            expectedBellNames.push_back(L"3");
            expectedBellNames.push_back(L"3♭");
            expectedBellNames.push_back(L"4");
            expectedBellNames.push_back(L"5");
            expectedBellNames.push_back(L"6");
            expectedBellNames.push_back(L"7");
            expectedBellNames.push_back(L"8");
            expectedBellNames.push_back(L"9");
            expectedBellNames.push_back(L"10");
            expectedBellNames.push_back(L"10♭");
            expectedBellNames.push_back(L"11");
            expectedBellNames.push_back(L"12");
            expectedBellNames.push_back(L"13");
            expectedBellNames.push_back(L"14");
            expectedBellNames.push_back(L"15");
            expectedBellNames.push_back(L"16");

            std::vector<std::wstring> bellNames;
            theTower.AllBellNames(bellNames, KNoId);
            Assert::AreEqual(expectedBellNames, bellNames);

            std::set<unsigned int> backSixteenBells = {1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 13, 14, 15, 16, 17, 18};
            Ring sixteenBells(0, 16, L"Sixteen bell peal", backSixteenBells, L"29-0-14", L"C#");
            theTower.AddRing(sixteenBells);

            std::set<unsigned int> backTwelveBells = { 6, 7, 8, 9, 10, 11, 13, 14, 15, 16, 17, 18 };
            Ring twelveBells(0, 12, L"Twelve bell peal", backTwelveBells, L"29-0-14", L"C#");
            theTower.AddRing(twelveBells);


            //Assert::IsTrue(theTower.Valid(database, false));
            theTower.Valid(database, true, true);
            Assert::AreEqual(L"", theTower.ErrorString(database.Settings(), true).c_str());
        }

        TEST_METHOD(Tower_Similar_Moulton)
        {
            RingingDatabase database;
            Tower theTower(KNoId, L"Ss Peter And Paul", L"Moulton", L"Northampton", 12);
            Tower theTower2(theTower);

            Assert::IsTrue(theTower.Duplicate(theTower2, database));
        }
        TEST_METHOD(Tower_Similar_Moulton_DifferentNumberOfBells_TowerbaseIdMatches)
        {
            RingingDatabase database;
            Tower theTower(KNoId, L"Ss Peter And Paul", L"Moulton", L"Northampton", 12);
            theTower.SetTowerbaseId(L"3475");
            Tower theTower2(theTower);
            theTower2.SetNoOfBells(10);

            Assert::IsTrue(theTower.Duplicate(theTower2, database));
        }
        TEST_METHOD(Tower_Similar_Moulton_DifferentNumberOfBells_DoveRefMatches)
        {
            RingingDatabase database;
            Tower theTower(KNoId, L"Ss Peter And Paul", L"Moulton", L"Northampton", 12);
            theTower.SetDoveRef(L"13208");
            Tower theTower2(theTower);
            theTower2.SetNoOfBells(10);

            Assert::IsTrue(theTower.Duplicate(theTower2, database));
        }
        TEST_METHOD(Tower_Similar_Moulton_UnrelatedTowers)
        {
            RingingDatabase database;
            Tower theTower(KNoId, L"Ss Peter And Paul", L"Moulton", L"Northampton", 12);
            Tower theTower2(KNoId, L"All Saints", L"Harpole", L"Northampton", 6);
            Assert::IsFalse(theTower.Duplicate(theTower2, database));
        }


        //Test for deleting towers
        //test for deleting towers without peals but first run date set
    };
}
