#include "CppUnitTest.h"
#include "ToString.h"
#include <Association.h>
#include <AssociationDatabase.h>
#include <RingingDatabase.h>
#include <Peal.h>
#include <PealDatabase.h>

#include <RenumberProgressCallback.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(AssociationDatabase_UnitTests), Duco::RenumberProgressCallback
    {
    public:
        // RenumberProgressCallback
        void RenumberInitialised(RenumberProgressCallback::TRenumberStage newStage)
        {
        }

        void RenumberStep(size_t objectId, size_t total)
        {

        }
        void RenumberComplete()
        {
        }

        TEST_METHOD(AssociationDatabase_MoveInEmptyDatabase)
        {
            AssociationDatabase database;
            Assert::IsFalse(database.DataChanged());

            ObjectId firstId;
            Assert::IsFalse(database.FirstObject(firstId));
            Assert::IsFalse(firstId.ValidId());

            Assert::IsFalse(database.LastObject(firstId));
            Assert::IsFalse(firstId.ValidId());

            Assert::IsFalse(database.NextObject(firstId, true));
            Assert::IsFalse(firstId.ValidId());

            Assert::IsFalse(database.NextObject(firstId, false));
            Assert::IsFalse(firstId.ValidId());

            Assert::IsFalse(database.ForwardMultipleObjects(firstId, true, 2));
            Assert::IsFalse(firstId.ValidId());

            Assert::IsFalse(database.ForwardMultipleObjects(firstId, false, 2));
            Assert::IsFalse(firstId.ValidId());
            Assert::IsFalse(database.DataChanged());
        }

        TEST_METHOD(AssociationDatabase_MoveInDatabase)
        {
            AssociationDatabase database;
            Association associationOne(KNoId,   L"Association one", L"");
            Duco::ObjectId associationOneId =   database.AddObject(associationOne);
            Association associationTwo(KNoId,   L"Association two", L"");
            Duco::ObjectId associationTwoId =   database.AddObject(associationTwo);
            Association associationThree(KNoId, L"Association three", L"");
            Duco::ObjectId associationThreeId = database.AddObject(associationThree);
            Association associationFour(KNoId,  L"Association four", L"");
            Duco::ObjectId associationFourId =  database.AddObject(associationFour);
            Association associationFive(KNoId,  L"Association five", L"");
            Duco::ObjectId associationFiveId =  database.AddObject(associationFive);
            Assert::IsTrue(database.DataChanged());

            ObjectId firstId;
            Assert::IsTrue(database.FirstObject(firstId));
            Assert::AreEqual(associationOneId, firstId);

            Assert::IsTrue(database.LastObject(firstId));
            Assert::AreEqual(associationFiveId, firstId);

            Assert::IsTrue(database.NextObject(firstId, false));
            Assert::AreEqual(associationFourId, firstId);

            Assert::IsTrue(database.NextObject(firstId, false));
            Assert::AreEqual(associationThreeId, firstId);

            Assert::IsTrue(database.NextObject(firstId, true));
            Assert::AreEqual(associationFourId, firstId);
        }

        TEST_METHOD(AssociationDatabase_MoveInDatabase_NotSettingIndex)
        {
            AssociationDatabase database;
            Association associationOne(KNoId, L"Association one", L"");
            Duco::ObjectId associationOneId = database.AddObject(associationOne);
            Association associationTwo(KNoId, L"Association two", L"");
            Duco::ObjectId associationTwoId = database.AddObject(associationTwo);
            Association associationThree(KNoId, L"Association three", L"");
            Duco::ObjectId associationThreeId = database.AddObject(associationThree);
            Association associationFour(KNoId, L"Association four", L"");
            Duco::ObjectId associationFourId = database.AddObject(associationFour);
            Association associationFive(KNoId, L"Association five", L"");
            Duco::ObjectId associationFiveId = database.AddObject(associationFive);
            Assert::IsTrue(database.DataChanged());

            ObjectId firstId;
            Assert::IsTrue(database.FirstObject(firstId, false));
            Assert::AreEqual(associationOneId, firstId);

            Assert::IsTrue(database.LastObject(firstId, false));
            Assert::AreEqual(associationFiveId, firstId);

            Assert::IsTrue(database.NextObject(firstId, false, false));
            Assert::AreEqual(associationFourId, firstId);

            Assert::IsTrue(database.NextObject(firstId, false, false));
            Assert::AreEqual(associationThreeId, firstId);

            Assert::IsTrue(database.NextObject(firstId, true, false));
            Assert::AreEqual(associationFourId, firstId);
        }

        TEST_METHOD(AssociationDatabase_MoveByMultipleObjects)
        {
            AssociationDatabase database;
            Association associationOne(KNoId, L"Association one", L"");
            Duco::ObjectId associationOneId = database.AddObject(associationOne);
            Association associationTwo(KNoId, L"Association two", L"");
            Duco::ObjectId associationTwoId = database.AddObject(associationTwo);
            Association associationThree(KNoId, L"Association three", L"");
            Duco::ObjectId associationThreeId = database.AddObject(associationThree);
            Association associationFour(KNoId, L"Association four", L"");
            Duco::ObjectId associationFourId = database.AddObject(associationFour);
            Association associationFive(KNoId, L"Association five", L"");
            Duco::ObjectId associationFiveId = database.AddObject(associationFive);

            ObjectId firstId;
            Assert::IsTrue(database.FirstObject(firstId));
            Assert::AreEqual(associationOneId, firstId);

            Assert::IsTrue(database.ForwardMultipleObjects(firstId, true, 3));
            Assert::AreEqual(associationFourId, firstId);

            Assert::IsTrue(database.ForwardMultipleObjects(firstId, false, 2));
            Assert::AreEqual(associationTwoId, firstId);

            Assert::IsTrue(database.ForwardMultipleObjects(firstId, false, 5));
            Assert::AreEqual(associationOneId, firstId);

            Assert::IsTrue(database.ForwardMultipleObjects(firstId, true, 5));
            Assert::AreEqual(associationFiveId, firstId);

            Assert::IsTrue(database.ObjectExists(associationFiveId));
            Assert::IsFalse(database.ObjectExists(25));
        }

        TEST_METHOD(AssociationDatabase_CheckLinks)
        {
            AssociationDatabase database;
            Association associationOne(KNoId, L"Association one", L"");
            Duco::ObjectId associationOneId = database.AddObject(associationOne);

            Association associationTwo(KNoId, L"Association two", L"");
            associationTwo.AddAssociationLink(associationOneId);
            Duco::ObjectId associationTwoId = database.AddObject(associationTwo);

            Association associationThree(KNoId, L"Association three", L"");
            associationThree.AddAssociationLink(associationOneId);
            associationThree.RemoveAssociationLink(associationOneId);
            associationThree.AddAssociationLink(associationTwoId);
            Duco::ObjectId associationThreeId = database.AddObject(associationThree);

            Association associationFour(KNoId, L"Association four", L"");
            Duco::ObjectId associationFourId = database.AddObject(associationFour);

            Association associationFive(KNoId, L"Association five", L"");
            associationFive.AddAssociationLink(associationTwoId);
            associationFive.AddAssociationLink(associationThreeId);
            Duco::ObjectId associationFiveId = database.AddObject(associationFive);

            std::set<Duco::ObjectId> links;
            database.FindLinksToAssociation(associationOneId, links);

            Assert::AreEqual(1, (int)links.size());
            Assert::IsTrue(links.contains(associationTwoId));
            Assert::IsFalse(links.contains(associationThreeId));
            Assert::IsFalse(links.contains(associationFourId));
            Assert::IsFalse(links.contains(associationFiveId));

            database.FindLinksToAssociation(associationFourId, links);
            Assert::AreEqual(0, (int)links.size());

            database.FindLinksToAssociation(associationThreeId, links);

            Assert::AreEqual(1, (int)links.size());
            Assert::IsTrue(links.contains(associationFiveId));
        }

        TEST_METHOD(AssociationDatabase_SortAndRenumber)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            std::set<Duco::ObjectId> links;
            {
                Association associationThree(KNoId, L"3", L"");
                Duco::ObjectId associationThreeId = database.AssociationsDatabase().AddObject(associationThree);
                Association associationOne(KNoId, L"1", L"");
                Duco::ObjectId associationOneId = database.AssociationsDatabase().AddObject(associationOne);
                Association associationTwo(KNoId, L"2", L"");
                associationTwo.AddAssociationLink(associationThreeId);
                Duco::ObjectId associationTwoId = database.AssociationsDatabase().AddObject(associationTwo);

                Peal pealOne;
                pealOne.SetAssociationId(associationTwoId);
                Duco::ObjectId pealOneId = database.PealsDatabase().AddObject(pealOne);

                database.AssociationsDatabase().FindLinksToAssociation(associationOneId, links);
                Assert::AreEqual(0, (int)links.size());

                database.AssociationsDatabase().FindLinksToAssociation(associationTwoId, links);
                Assert::AreEqual(0, (int)links.size());

                database.AssociationsDatabase().FindLinksToAssociation(associationThreeId, links);
                Assert::AreEqual(1, (int)links.size());

                database.RebuildDatabase(this);
            }

            Duco::ObjectId associationOneId = database.AssociationsDatabase().SuggestAssociation(L"1");
            Duco::ObjectId associationTwoId = database.AssociationsDatabase().SuggestAssociation(L"2");
            Duco::ObjectId associationThreeId = database.AssociationsDatabase().SuggestAssociation(L"3");

            database.AssociationsDatabase().FindLinksToAssociation(associationOneId, links);
            Assert::AreEqual(0, (int)links.size());

            database.AssociationsDatabase().FindLinksToAssociation(associationTwoId, links);
            Assert::AreEqual(0, (int)links.size());

            database.AssociationsDatabase().FindLinksToAssociation(associationThreeId, links);
            Assert::AreEqual(1, (int)links.size());
        }
    };
}
