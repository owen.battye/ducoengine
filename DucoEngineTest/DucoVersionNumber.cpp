#include "CppUnitTest.h"
#include <DucoVersionNumber.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(DucoVersionNo_UnitTests)
    {
    public:

        TEST_METHOD(DucoVersionNo_Construction_BasicTest)
        {
            DucoVersionNumber version;
            std::wstring expectedResult = L"0.0.0";
            Assert::AreEqual(expectedResult, version.Str());
        }

        TEST_METHOD(DucoVersionNo_Construction_NewNumberSchemeDecode_Start)
        {
            DucoVersionNumber version (L"2020.1.1");
            std::wstring expectedResult = L"2020.01.01";
            Assert::AreEqual(expectedResult, version.Str());
        }
        TEST_METHOD(DucoVersionNo_Construction_NewNumberSchemeDecode_End)
        {
            DucoVersionNumber version(L"2020.12.31");
            std::wstring expectedResult = L"2020.12.31";
            Assert::AreEqual(expectedResult, version.Str());
        }

        TEST_METHOD(DucoVersionNo_Construction_NewNumberSchemeDecode_TooShort)
        {
            DucoVersionNumber version(L"2020.12");
            std::wstring expectedResult = L"2020.12.01";
            Assert::AreEqual(expectedResult, version.Str());
        }
        TEST_METHOD(DucoVersionNo_Construction_NewNumberSchemeDecode_FarTooShort)
        {
            DucoVersionNumber version(L"2020");
            std::wstring expectedResult = L"2020.01.01";
            Assert::AreEqual(expectedResult, version.Str());
        }

        TEST_METHOD(DucoVersionNo_Construction_UINamingSchemeRendering)
        {
            DucoVersionNumber version(L"1.3.67");
            std::wstring expectedResult = L"1.3.67";
            Assert::AreEqual(expectedResult, version.Str());
        }

        TEST_METHOD(DucoVersionNo_Construction_UINamingSchemeRendering2)
        {
            DucoVersionNumber version(L"1.0.0");
            std::wstring expectedResult = L"1.0.0";
            Assert::AreEqual(expectedResult, version.Str());
        }

        TEST_METHOD(DucoVersionNo_Construction_EmptyInputString)
        {
            DucoVersionNumber version (L"");
            std::wstring expectedResult = L"0.0.0";
            Assert::AreEqual(expectedResult, version.Str());
        }
    
        TEST_METHOD(DucoVersionNo_3rdDigitComparision)
        {
            DucoVersionNumber versionOne (L"0.1.85");
            DucoVersionNumber versionTwo (L"0.1.86");
            Assert::IsTrue(versionOne < versionTwo );
        }

        TEST_METHOD(DucoVersionNo_2ndDigitComparision)
        {
            DucoVersionNumber versionOne(L"0.1.85");
            DucoVersionNumber versionTwo(L"0.2.86");
            Assert::IsTrue(versionOne < versionTwo);
        }

        TEST_METHOD(DucoVersionNo_1stDigitComparision)
        {
            DucoVersionNumber versionOne(L"0.1.85");
            DucoVersionNumber versionTwo(L"1.1.86");
            Assert::IsTrue(versionOne < versionTwo);
        }

        TEST_METHOD(DucoVersionNo_Identical)
        {
            DucoVersionNumber versionOne(L"0.1.85");
            DucoVersionNumber versionTwo(L"0.1.85");
            Assert::IsFalse(versionOne < versionTwo);
            Assert::IsFalse(versionTwo < versionOne);
        }

        TEST_METHOD(DucoVersionNo_Greater_ThirdDigit)
        {
            DucoVersionNumber versionOne(L"0.1.86");
            DucoVersionNumber versionTwo(L"0.1.85");
            Assert::IsFalse(versionOne < versionTwo);
        }
        
        TEST_METHOD(DucoVersionNo_Greater_FirstDigit)
        {
            DucoVersionNumber versionOne(L"1.0.0");
            DucoVersionNumber versionTwo(L"0.1.86");
            Assert::IsFalse(versionOne < versionTwo);
        }

        TEST_METHOD(DucoVersionNo_Greater_NewNumberingSchemeAndOld)
        {
            DucoVersionNumber versionOne(L"2020.05.04");
            DucoVersionNumber versionTwo(L"0.1.86");
            Assert::IsTrue(versionTwo < versionOne);
        }

        TEST_METHOD(DucoVersionNo_FirstVersionAfterRenumbering)
        {
            DucoVersionNumber versionOne(L"1.0.0");
            DucoVersionNumber versionTwo(L"0.1.86");
            Assert::IsTrue(versionTwo < versionOne);
        }


        TEST_METHOD(DucoVersionNo_EqualsOperator_Same)
        {
            DucoVersionNumber versionOne(L"1.2.3");
            DucoVersionNumber versionTwo(L"1.2.3");
            Assert::IsTrue(versionTwo == versionOne);
        }

        TEST_METHOD(DucoVersionNo_EqualsOperator_MajorDifference)
        {
            DucoVersionNumber versionOne(L"2.2.3");
            DucoVersionNumber versionTwo(L"1.2.3");
            Assert::IsFalse(versionTwo == versionOne);
        }

        TEST_METHOD(DucoVersionNo_EqualsOperator_MinorDifference)
        {
            DucoVersionNumber versionOne(L"1.2.3");
            DucoVersionNumber versionTwo(L"1.3.3");
            Assert::IsFalse(versionTwo == versionOne);
        }

        TEST_METHOD(DucoVersionNo_EqualsOperator_BuildDifference)
        {
            DucoVersionNumber versionOne(L"1.2.3");
            DucoVersionNumber versionTwo(L"1.2.4");
            Assert::IsFalse(versionTwo == versionOne);
        }
    };
}