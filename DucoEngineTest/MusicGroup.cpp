#include "CppUnitTest.h"
#include <Change.h>
#include <MusicGroup.h>
#include "ToString.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(MusicGroup_UnitTests)
    {
    public:
        TEST_METHOD(MusicGroup_Tittums)
        {
            MusicGroup music(L"15263748.Tittums");

            Assert::AreEqual(L"Tittums", music.Name(false).c_str());
            Assert::AreEqual(L"Tittums", music.Name(true).c_str());

            Change tittumsOnEight(L"15263748");
            Assert::IsTrue(music.Match(tittumsOnEight));
            Change tittumsOnTen(L"1627384950");
            Assert::IsFalse(music.Match(tittumsOnTen));
            Change random(L"8569741230");
            Assert::IsFalse(music.Match(random));
        }

        TEST_METHOD(MusicGroup_BackRollups)
        {
            MusicGroup music(L"xxxxx678..xxx45678.xxxx5678");

            Assert::AreEqual(L"0: 678", music.Name(false).c_str());
            Assert::AreEqual(L"0: xxxxx678", music.Name(true).c_str());

            Change backThree(L"54321678");
            Assert::IsTrue(music.Match(backThree));
            Change backFour(L"43215678");
            Assert::IsTrue(music.Match(backThree));

            Change random(L"85762314");
            Assert::IsFalse(music.Match(random));
            Change roundsOnTwelve(L"1234567890ET");
            Assert::IsFalse(music.Match(roundsOnTwelve));
        }
    };
}
