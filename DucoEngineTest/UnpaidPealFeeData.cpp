#include "CppUnitTest.h"
#include <Association.h>
#include <AssociationDatabase.h>
#include <UnpaidPealFeeData.h>
#include <PealDatabase.h>
#include <RingingDatabase.h>
#include <RingerDatabase.h>
#include <Peal.h>
#include "ToString.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(UnpaidPealFeeData_UnitTests)
    {
    public:

        TEST_METHOD(UnpaidPealFeeData_DefaultConstructor)
        {
            UnpaidPealFeeData data(55, 3, 150, 100);

            Assert::AreEqual(Duco::ObjectId(55),data.PealId());
            Assert::AreEqual(Duco::ObjectId(3), data.FeePayerId());
            Assert::AreEqual((unsigned int)150, data.AmountPaidInPence());
            Assert::AreEqual((unsigned int)100, data.AmountUnPaidInPence());
        }

        TEST_METHOD(UnpaidPealFeeData_OnePeal_FullyPaid)
        {
            RingingDatabase database;
            Duco::ObjectId ringerOne = database.RingersDatabase().AddRinger(L"Owen Battye");
            Assert::IsTrue(ringerOne.ValidId());

            Association yorkshire(1, L"Yorkshire Association", L"");
            Duco::ObjectId yorksAssociationId = database.AssociationsDatabase().AddObject(yorkshire);

            Peal newPeal;
            newPeal.SetAssociationId(yorksAssociationId);
            newPeal.SetRingerId(ringerOne, 1, false);
            newPeal.SetPealFee(1, false, 4000);
            database.PealsDatabase().AddObject(newPeal);
            Peal secondPeal;
            secondPeal.SetAssociationId(yorksAssociationId);
            secondPeal.SetRingerId(ringerOne, 1, false);
            secondPeal.SetPealFee(1, false, 4000);
            database.PealsDatabase().AddObject(secondPeal);

            std::set<UnpaidPealFeeData> pealIds;
            Assert::IsFalse(database.PealsDatabase().FindPealsWithUnpaidFees(1, yorksAssociationId, -1, pealIds));
        }
        TEST_METHOD(UnpaidPealFeeData_OnePeal_OneUnpaid)
        {
            RingingDatabase database;
            Duco::ObjectId ringerOne = database.RingersDatabase().AddRinger(L"Owen Battye");
            Assert::IsTrue(ringerOne.ValidId());

            Association yorkshire (1, L"Yorkshire Association", L"");
            Duco::ObjectId yorksAssociationId = database.AssociationsDatabase().AddObject(yorkshire);

            Peal newPeal;
            newPeal.SetAssociationId(yorksAssociationId);
            newPeal.SetRingerId(ringerOne, 1, false);
            newPeal.SetPealFee(1, false, 4000);
            database.PealsDatabase().AddObject(newPeal);
            Peal secondPeal;
            secondPeal.SetAssociationId(yorksAssociationId);
            secondPeal.SetRingerId(ringerOne, 1, false);
            database.PealsDatabase().AddObject(secondPeal);

            std::set<UnpaidPealFeeData> pealIds;
            Assert::IsTrue(database.PealsDatabase().FindPealsWithUnpaidFees(1, yorksAssociationId, -1, pealIds));
            Assert::AreEqual((size_t)1, pealIds.size());
        }
    };
}
