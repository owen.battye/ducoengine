﻿#include "CppUnitTest.h"
#include "ToString.h"

#include <Association.h>
#include <AssociationDatabase.h>
#include <DatabaseSettings.h>
#include <ImportExportProgressCallback.h>
#include <LeadingBellData.h>
#include <Method.h>
#include <MethodDatabase.h>
#include <Peal.h>
#include <PealDatabase.h>
#include <PealLengthInfo.h>
#include <RenumberProgressCallback.h>
#include <RingerDatabase.h>
#include <RingerCirclingData.h>
#include <StatisticFilters.h>
#include <RingingDatabase.h>
#include <Tower.h>
#include <TowerDatabase.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(PealDatabase_UnitTests), Duco::RenumberProgressCallback, Duco::ImportExportProgressCallback
    {
    public:
        //From ImportExportProgressCallback
        void InitialisingImportExport(bool internalising, unsigned int versionNumber, size_t totalNumberOfObjects)
        {

        }
        void ObjectProcessed(bool internalising, Duco::TObjectType objectType, Duco::ObjectId objectId, int totalPercentage, size_t numberInThisSubDatabase)
        {

        }
        void ImportExportComplete(bool internalising)
        {

        }
        void ImportExportFailed(bool internalising, int errorCode)
        {
            Assert::IsFalse(true);
        }
        // from RenumberProgressCallback
        void RenumberInitialised(RenumberProgressCallback::TRenumberStage newStage)
        {
        }

        void RenumberStep(size_t objectId, size_t total)
        {

        }
        void RenumberComplete()
        {

        }

        TEST_METHOD(PealDatabase_DatabaseReader_SinglePeal)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);
            PerformanceDate today(2000, 4, 16);

            Peal pealOne;
            PerformanceDate dateOne(2018, 12, 25);
            pealOne.SetDate(dateOne);

            Peal pealTwo;
            PerformanceDate dateTwo(2018, 4, 16);
            pealTwo.SetDate(dateTwo);

            Duco::ObjectId pealOneId = database.PealsDatabase().AddObject(pealOne);
            Duco::ObjectId pealTwoId = database.PealsDatabase().AddObject(pealTwo);

            std::set<Duco::ObjectId> pealIds = database.PealsDatabase().OnThisDay(today);
            Assert::AreEqual((size_t)1, pealIds.size());
            Assert::IsTrue(database.PealsDatabase().AnyOnThisDay(today));

            pealIds = database.PealsDatabase().OnThisDay(PerformanceDate(2020, 1, 1));
            Assert::AreEqual((size_t)0, pealIds.size());
            Assert::IsFalse(database.PealsDatabase().AnyOnThisDay(PerformanceDate(2020, 1, 1)));
        }

        TEST_METHOD(PealDatabase_PealOrder_SwapPealsOnDifferentDays)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);
            PerformanceDate tomorrow(2020, 4, 17);

            Peal pealOne;
            pealOne.SetChanges(5000);
            pealOne.SetDate(tomorrow);

            Peal pealTwo;
            PerformanceDate today(2020, 4, 16);
            pealTwo.SetChanges(5001);
            pealTwo.SetDate(today);

            Duco::ObjectId pealOneId = database.PealsDatabase().AddObject(pealOne);
            Duco::ObjectId pealTwoId = database.PealsDatabase().AddObject(pealTwo);

            std::list<PerformanceDate> dates;
            database.PealsDatabase().GetDatesWithMultiplePeals(dates);

            Assert::AreEqual((size_t)0, dates.size());

            Assert::AreEqual((unsigned int)5000, database.PealsDatabase().FindPeal(pealOneId, true)->NoOfChanges());
            Assert::AreEqual((unsigned int)5001, database.PealsDatabase().FindPeal(pealTwoId)->NoOfChanges());
            database.RebuildDatabase(this);
            Assert::AreEqual((unsigned int)5001, database.PealsDatabase().FindPeal(pealOneId)->NoOfChanges());
            Assert::AreEqual((unsigned int)5000, database.PealsDatabase().FindPeal(pealTwoId)->NoOfChanges());
        }

        TEST_METHOD(PealDatabase_PealOrder_SwapPealsOnSameDay)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);
            PerformanceDate today(2020, 4, 16);

            Peal pealOne;
            pealOne.SetChanges(5000);
            pealOne.SetDate(today);

            Peal pealTwo;
            pealTwo.SetChanges(5001);
            pealTwo.SetDate(today);

            Duco::ObjectId pealOneId = database.PealsDatabase().AddObject(pealOne);
            Duco::ObjectId pealTwoId = database.PealsDatabase().AddObject(pealTwo);
            Peal pealThree;
            pealThree.SetDate(PerformanceDate(2020,4,17));
            Duco::ObjectId pealThreeId = database.PealsDatabase().AddObject(pealThree);

            std::list<PerformanceDate> dates;
            database.PealsDatabase().GetDatesWithMultiplePeals(dates);

            Assert::AreEqual((size_t)1, dates.size());
            Assert::AreEqual(today, *dates.begin());

            Assert::AreEqual((unsigned int)5000, database.PealsDatabase().FindPeal(pealOneId, true)->NoOfChanges());
            Assert::AreEqual((unsigned int)5001, database.PealsDatabase().FindPeal(pealTwoId)->NoOfChanges());
            database.PealsDatabase().SwapPeals(pealOneId, pealTwoId, 0, true);
            Assert::AreEqual((unsigned int)5001, database.PealsDatabase().FindPeal(pealOneId)->NoOfChanges());
            Assert::AreEqual((unsigned int)5000, database.PealsDatabase().FindPeal(pealTwoId)->NoOfChanges());

            database.RebuildDatabase(this);
            Assert::AreEqual((unsigned int)5001, database.PealsDatabase().FindPeal(pealOneId)->NoOfChanges());
            Assert::AreEqual((unsigned int)5000, database.PealsDatabase().FindPeal(pealTwoId)->NoOfChanges());
        }

        TEST_METHOD(PealDatabase_PealOrder_SortPealsOnSameDay)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);
            PerformanceDate today(2020, 4, 16);

            Peal pealOne;
            pealOne.SetChanges(5000);
            pealOne.SetDate(today);

            Peal pealTwo;
            pealTwo.SetChanges(5001);
            pealTwo.SetDate(today);

            Duco::ObjectId pealOneId = database.PealsDatabase().AddObject(pealOne);
            Duco::ObjectId pealTwoId = database.PealsDatabase().AddObject(pealTwo);

            Assert::AreEqual((unsigned int)5000, database.PealsDatabase().FindPeal(pealOneId, true)->NoOfChanges());
            Assert::AreEqual((unsigned int)5001, database.PealsDatabase().FindPeal(pealTwoId)->NoOfChanges());

            database.PealsDatabase().FindPeal(pealOneId)->SetDayOrder(2);
            database.PealsDatabase().FindPeal(pealTwoId)->SetDayOrder(1);
            database.RebuildDatabase(this);
            Assert::AreEqual((unsigned int)5001, database.PealsDatabase().FindPeal(pealOneId)->NoOfChanges());
            Assert::AreEqual((unsigned int)5000, database.PealsDatabase().FindPeal(pealTwoId)->NoOfChanges());
        }

        TEST_METHOD(PealDatabase_PealOrder_SortPealsOnSameDayFromCreation)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);
            PerformanceDate today(2020, 4, 16);

            Peal pealOne;
            pealOne.SetChanges(5000);
            pealOne.SetDate(today);
            pealOne.SetDayOrder(2);

            Peal pealTwo;
            pealTwo.SetChanges(5001);
            pealTwo.SetDate(today);
            pealOne.SetDayOrder(1);

            Duco::ObjectId pealOneId = database.PealsDatabase().AddObject(pealOne);
            Duco::ObjectId pealTwoId = database.PealsDatabase().AddObject(pealTwo);

            Assert::AreEqual((unsigned int)5000, database.PealsDatabase().FindPeal(pealOneId, true)->NoOfChanges());
            Assert::AreEqual((unsigned int)5001, database.PealsDatabase().FindPeal(pealTwoId)->NoOfChanges());

            database.RebuildDatabase(this);
            Assert::AreEqual((unsigned int)5001, database.PealsDatabase().FindPeal(pealOneId)->NoOfChanges());
            Assert::AreEqual((unsigned int)5000, database.PealsDatabase().FindPeal(pealTwoId)->NoOfChanges());
        }

        TEST_METHOD(PealDatabase_LeadingBells)
        {
            RingingDatabase database;
            ObjectId ringerId = 1;

            Tower theTower;
            ObjectId theRingId = theTower.AddRing(8, L"11-1-14", L"");
            ObjectId theTowerId = database.TowersDatabase().AddObject(theTower);

            Peal pealOne;
            pealOne.SetTowerId(theTowerId);
            pealOne.SetRingId(theRingId);
            pealOne.SetRingerId(ringerId, 2, false);
            database.PealsDatabase().AddObject(pealOne);
            Peal pealTwo;
            pealTwo.SetTowerId(theTowerId);
            pealTwo.SetRingId(theRingId);
            pealTwo.SetRingerId(ringerId, 2, false);
            database.PealsDatabase().AddObject(pealTwo);
            Peal pealThree;
            pealThree.SetTowerId(theTowerId);
            pealThree.SetRingId(theRingId);
            pealThree.SetRingerId(ringerId, 4, false);
            pealThree.SetRingerId(2, 5, false);
            database.PealsDatabase().AddObject(pealThree);

            std::set<Duco::LeadingBellData> sortedLeadingBellPealCounts;
            database.PealsDatabase().GetLeadingBells(ringerId, sortedLeadingBellPealCounts);

            Assert::AreEqual((size_t)2, sortedLeadingBellPealCounts.size());
            Assert::AreEqual((size_t)6, sortedLeadingBellPealCounts.begin()->BellNumber());
            Assert::AreEqual((size_t)2, sortedLeadingBellPealCounts.begin()->PealCount());
            Assert::AreEqual(theTowerId, sortedLeadingBellPealCounts.begin()->TowerId());
            Assert::AreEqual((size_t)8, sortedLeadingBellPealCounts.begin().operator++()->BellNumber());
            Assert::AreEqual((size_t)1, sortedLeadingBellPealCounts.begin().operator++()->PealCount());
            Assert::AreEqual(theTowerId, sortedLeadingBellPealCounts.begin().operator++()->TowerId());
        }

        TEST_METHOD(PealDatabase_YearsPealCount)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);
            StatisticFilters filters(database);

            Peal pealOne;
            pealOne.SetDate(PerformanceDate(2010, 01, 01));
            database.PealsDatabase().AddObject(pealOne);
            Peal pealTwo;
            pealTwo.SetDate(PerformanceDate(2012, 01, 01));
            database.PealsDatabase().AddObject(pealTwo);
            Peal pealThree;
            pealThree.SetDate(PerformanceDate(2010, 01, 02));
            database.PealsDatabase().AddObject(pealThree);

            std::map<unsigned int, Duco::PealLengthInfo> pealCounts;
            database.PealsDatabase().GetAllYearsByPealCount(filters, pealCounts);

            Assert::AreEqual((size_t)3, pealCounts.size());
            Assert::AreEqual((size_t)2, pealCounts[2010].TotalPeals());
            Assert::AreEqual((size_t)0, pealCounts[2011].TotalPeals());
            Assert::AreEqual((size_t)1, pealCounts[2012].TotalPeals());
        }

        TEST_METHOD(PealDatabase_YearsPealCount_EmptyDatabase)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);
            StatisticFilters filters(database);

            std::map<unsigned int, Duco::PealLengthInfo> pealCounts;
            database.PealsDatabase().GetAllYearsByPealCount(filters, pealCounts);

            Assert::AreEqual((size_t)0, pealCounts.size());
        }

        TEST_METHOD(PealDatabase_GetTopRingersPealCountPerYear)
        {   // Default ringer is removed.
            RingingDatabase database;
            database.ClearDatabase(false, true);
            StatisticFilters filters(database);

            Duco::ObjectId ringerOne(1);
            Duco::ObjectId ringerTwo(2);
            Duco::ObjectId ringerThree(3);
            Duco::ObjectId ringerFour(4);

            Peal pealOne;
            pealOne.SetRingerId(ringerOne, 1, false);
            pealOne.SetRingerId(ringerTwo, 2, false);
            pealOne.SetRingerId(ringerFour, 4, false);
            pealOne.SetDate(PerformanceDate(2010, 01, 01));
            database.PealsDatabase().AddObject(pealOne);
            Peal pealTwo;
            pealTwo.SetDate(PerformanceDate(2012, 01, 03));
            pealTwo.SetRingerId(ringerOne, 1, false);
            pealTwo.SetRingerId(ringerThree, 3, false);
            pealTwo.SetRingerId(ringerFour, 4, false);
            database.PealsDatabase().AddObject(pealTwo);
            Peal pealThree;
            pealThree.SetDate(PerformanceDate(2010, 01, 02));
            pealThree.SetRingerId(ringerOne, 1, false);
            pealThree.SetRingerId(ringerTwo, 2, false);
            pealThree.SetRingerId(ringerFour, 4, false);
            database.PealsDatabase().AddObject(pealThree);
            Peal pealFour;
            pealFour.SetDate(PerformanceDate(2013, 01, 04));
            pealFour.SetRingerId(ringerFour, 4, false);
            database.PealsDatabase().AddObject(pealFour);
            Assert::IsFalse(database.Settings().DefaultRingerSet());

            std::map<ObjectId, std::map<unsigned int, Duco::PealLengthInfo> > sortedPealCounts;
            database.PealsDatabase().GetTopRingersPealCountPerYear(filters, false, 5, sortedPealCounts);

            Assert::IsTrue(database.Settings().DefaultRingerSet());
            Assert::AreEqual(ringerFour, database.Settings().DefaultRinger());
            Assert::AreEqual((size_t)3, sortedPealCounts.size());

            std::map<unsigned int, Duco::PealLengthInfo> ringerOneCounts = sortedPealCounts[ringerOne];
            Assert::AreEqual((size_t)3, ringerOneCounts.size());
            Assert::AreEqual((size_t)2, ringerOneCounts[2010].TotalPeals());
            Assert::AreEqual((size_t)0, ringerOneCounts[2011].TotalPeals());
            Assert::AreEqual((size_t)1, ringerOneCounts[2012].TotalPeals());

            std::map<unsigned int, Duco::PealLengthInfo> ringerTwoCounts = sortedPealCounts[ringerTwo];
            Assert::AreEqual((size_t)3, ringerTwoCounts.size());
            Assert::AreEqual((size_t)2, ringerTwoCounts[2010].TotalPeals());
            Assert::AreEqual((size_t)0, ringerTwoCounts[2011].TotalPeals());
            Assert::AreEqual((size_t)0, ringerTwoCounts[2012].TotalPeals());

            std::map<unsigned int, Duco::PealLengthInfo> ringerThreeCounts = sortedPealCounts[ringerThree];
            Assert::AreEqual((size_t)3, ringerThreeCounts.size());
            Assert::AreEqual((size_t)0, ringerThreeCounts[2010].TotalPeals());
            Assert::AreEqual((size_t)0, ringerThreeCounts[2011].TotalPeals());
            Assert::AreEqual((size_t)1, ringerThreeCounts[2012].TotalPeals());

            // Default ringer is removed
            Assert::IsFalse(sortedPealCounts.contains(ringerFour));
        }

        TEST_METHOD(PealDatabase_GetTopRingersPealCountPerYear_CombineLinkedRingers_WithNoLinks)
        {   // Default ringer is removed.
            RingingDatabase database;
            database.ClearDatabase(false, true);
            StatisticFilters filters(database);

            Duco::ObjectId ringerOne(1);
            Duco::ObjectId ringerTwo(2);
            Duco::ObjectId ringerThree(3);
            Duco::ObjectId ringerFour(4);

            Peal pealOne;
            pealOne.SetRingerId(ringerOne, 1, false);
            pealOne.SetRingerId(ringerTwo, 2, false);
            pealOne.SetRingerId(ringerFour, 4, false);
            pealOne.SetDate(PerformanceDate(2010, 01, 01));
            database.PealsDatabase().AddObject(pealOne);
            Peal pealTwo;
            pealTwo.SetDate(PerformanceDate(2012, 01, 03));
            pealTwo.SetRingerId(ringerOne, 1, false);
            pealTwo.SetRingerId(ringerThree, 3, false);
            pealTwo.SetRingerId(ringerFour, 4, false);
            database.PealsDatabase().AddObject(pealTwo);
            Peal pealThree;
            pealThree.SetDate(PerformanceDate(2010, 01, 02));
            pealThree.SetRingerId(ringerOne, 1, false);
            pealThree.SetRingerId(ringerTwo, 2, false);
            pealThree.SetRingerId(ringerFour, 4, false);
            database.PealsDatabase().AddObject(pealThree);
            Peal pealFour;
            pealFour.SetDate(PerformanceDate(2013, 01, 04));
            pealFour.SetRingerId(ringerFour, 4, false);
            database.PealsDatabase().AddObject(pealFour);
            Assert::IsFalse(database.Settings().DefaultRingerSet());

            std::map<ObjectId, std::map<unsigned int, Duco::PealLengthInfo> > sortedPealCounts;
            database.PealsDatabase().GetTopRingersPealCountPerYear(filters, true, 5, sortedPealCounts);

            Assert::IsTrue(database.Settings().DefaultRingerSet());
            Assert::AreEqual(ringerFour, database.Settings().DefaultRinger());
            Assert::AreEqual((size_t)3, sortedPealCounts.size());

            std::map<unsigned int, Duco::PealLengthInfo> ringerOneCounts = sortedPealCounts[ringerOne];
            Assert::AreEqual((size_t)3, ringerOneCounts.size());
            Assert::AreEqual((size_t)2, ringerOneCounts[2010].TotalPeals());
            Assert::AreEqual((size_t)0, ringerOneCounts[2011].TotalPeals());
            Assert::AreEqual((size_t)1, ringerOneCounts[2012].TotalPeals());

            std::map<unsigned int, Duco::PealLengthInfo> ringerTwoCounts = sortedPealCounts[ringerTwo];
            Assert::AreEqual((size_t)3, ringerTwoCounts.size());
            Assert::AreEqual((size_t)2, ringerTwoCounts[2010].TotalPeals());
            Assert::AreEqual((size_t)0, ringerTwoCounts[2011].TotalPeals());
            Assert::AreEqual((size_t)0, ringerTwoCounts[2012].TotalPeals());

            std::map<unsigned int, Duco::PealLengthInfo> ringerThreeCounts = sortedPealCounts[ringerThree];
            Assert::AreEqual((size_t)3, ringerThreeCounts.size());
            Assert::AreEqual((size_t)0, ringerThreeCounts[2010].TotalPeals());
            Assert::AreEqual((size_t)0, ringerThreeCounts[2011].TotalPeals());
            Assert::AreEqual((size_t)1, ringerThreeCounts[2012].TotalPeals());

            // Default ringer is removed
            Assert::IsFalse(sortedPealCounts.contains(ringerFour));
        }

        TEST_METHOD(PealDatabase_GetTopRingersPealCountPerYear_CombineLinkedRingers)
        {   // Default ringer is removed.
            RingingDatabase database;
            database.ClearDatabase(false, true);
            StatisticFilters filters(database);

            Duco::ObjectId ringerOne = database.RingersDatabase().AddRinger(L"One");
            Duco::ObjectId ringerTwo = database.RingersDatabase().AddRinger(L"Two");
            Duco::ObjectId ringerThree = database.RingersDatabase().AddRinger(L"Three");
            Duco::ObjectId ringerFour = database.RingersDatabase().AddRinger(L"Four");
            Duco::ObjectId defaultRinger = database.RingersDatabase().AddRinger(L"Five");

            Peal pealOne;
            pealOne.SetRingerId(ringerOne, 1, false);
            pealOne.SetRingerId(ringerTwo, 2, false);
            pealOne.SetRingerId(defaultRinger, 8, false);
            pealOne.SetDate(PerformanceDate(2010, 01, 01));
            database.PealsDatabase().AddObject(pealOne);
            Peal pealTwo;
            pealTwo.SetDate(PerformanceDate(2010, 01, 02));
            pealTwo.SetRingerId(defaultRinger, 8, false);
            database.PealsDatabase().AddObject(pealTwo);
            Peal pealThree;
            pealThree.SetDate(PerformanceDate(2010, 01, 03));
            pealThree.SetRingerId(ringerOne, 1, false);
            pealThree.SetRingerId(ringerThree, 3, false);
            pealThree.SetRingerId(defaultRinger, 8, false);
            database.PealsDatabase().AddObject(pealThree);

            Assert::IsTrue(database.RingersDatabase().LinkRingers(ringerOne, ringerFour));
            Assert::IsTrue(database.RingersDatabase().LinkRingers(ringerTwo, ringerThree));

            std::map<ObjectId, std::map<unsigned int, Duco::PealLengthInfo> > sortedPealCounts;
            database.PealsDatabase().GetTopRingersPealCountPerYear(filters, true, 5, sortedPealCounts);
            Assert::IsTrue(database.Settings().DefaultRingerSet());

            Assert::AreEqual((size_t)2, sortedPealCounts.size());

            std::map<unsigned int, Duco::PealLengthInfo> ringerOneCounts = sortedPealCounts[ringerOne];
            Assert::AreEqual((size_t)1, ringerOneCounts.size());
            Assert::AreEqual((size_t)2, ringerOneCounts[2010].TotalPeals());

            std::map<unsigned int, Duco::PealLengthInfo> ringerTwoCounts = sortedPealCounts[ringerTwo];
            Assert::AreEqual((size_t)1, ringerTwoCounts.size());
            Assert::AreEqual((size_t)2, ringerTwoCounts[2010].TotalPeals());

            Assert::IsFalse(sortedPealCounts.contains(ringerThree));
            Assert::IsFalse(sortedPealCounts.contains(ringerFour));
            Assert::IsFalse(sortedPealCounts.contains(defaultRinger));
        }

        TEST_METHOD(PealDatabase_GetTopRingersPealCountPerYear_CombineLinkedRingers_LinkedRingersNotInpeals)
        {   // Default ringer is removed.
            RingingDatabase database;
            database.ClearDatabase(false, true);
            StatisticFilters filters(database);

            Duco::ObjectId ringerOne = database.RingersDatabase().AddRinger(L"One");
            Duco::ObjectId ringerTwo = database.RingersDatabase().AddRinger(L"Two");
            Duco::ObjectId defaultRinger = database.RingersDatabase().AddRinger(L"Five");

            Peal pealOne;
            pealOne.SetRingerId(defaultRinger, 8, false);
            pealOne.SetDate(PerformanceDate(2010, 01, 01));
            database.PealsDatabase().AddObject(pealOne);
            Peal pealTwo;
            pealTwo.SetRingerId(defaultRinger, 8, false);
            database.PealsDatabase().AddObject(pealTwo);

            Assert::IsTrue(database.RingersDatabase().LinkRingers(ringerOne, ringerTwo));

            std::map<ObjectId, std::map<unsigned int, Duco::PealLengthInfo> > sortedPealCounts;
            database.PealsDatabase().GetTopRingersPealCountPerYear(filters, true, 5, sortedPealCounts);
            Assert::IsTrue(database.Settings().DefaultRingerSet());

            Assert::IsFalse(sortedPealCounts.contains(ringerOne));
            Assert::IsFalse(sortedPealCounts.contains(ringerTwo));
            Assert::IsFalse(sortedPealCounts.contains(defaultRinger));
        }

        TEST_METHOD(PealDatabase_GetTopRingersPealCountPerYear_EmptyDatabase)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);
            StatisticFilters filters(database);

            std::map<ObjectId, std::map<unsigned int, Duco::PealLengthInfo> > sortedPealCounts;
            database.PealsDatabase().GetTopRingersPealCountPerYear(filters, false, 5, sortedPealCounts);

            Assert::IsFalse(database.Settings().DefaultRingerSet());
            Assert::AreEqual((size_t)0, sortedPealCounts.size());
        }

        TEST_METHOD(PealDatabase_GetTopRingersPealCountPerYear_LinkedRingers)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);
            StatisticFilters filters(database);

            Duco::ObjectId ringerOneId = database.RingersDatabase().AddRinger(L"One One");
            Duco::ObjectId ringerTwoId = database.RingersDatabase().AddRinger(L"Two two");;
            Duco::ObjectId ringerThreeId = database.RingersDatabase().AddRinger(L"Three three");;
            Duco::ObjectId ringerFourId = database.RingersDatabase().AddRinger(L"Four four");;
            Duco::ObjectId defaultRingerId = database.RingersDatabase().AddRinger(L"Default default");;

            Peal pealOne;
            pealOne.SetRingerId(ringerOneId, 1, false);
            pealOne.SetRingerId(ringerTwoId, 2, false);
            pealOne.SetRingerId(defaultRingerId, 10, false);
            pealOne.SetDate(PerformanceDate(2010, 01, 01));
            database.PealsDatabase().AddObject(pealOne);
            Peal pealTwo;
            pealTwo.SetDate(PerformanceDate(2012, 01, 03));
            pealTwo.SetRingerId(ringerThreeId, 3, false);
            pealTwo.SetRingerId(ringerFourId, 4, false);
            pealTwo.SetRingerId(defaultRingerId, 10, false);
            database.PealsDatabase().AddObject(pealTwo);
            Peal pealThree;
            pealThree.SetDate(PerformanceDate(2010, 01, 02));
            pealThree.SetRingerId(ringerOneId, 1, false);
            pealThree.SetRingerId(ringerTwoId, 2, false);
            pealThree.SetRingerId(ringerThreeId, 3, false);
            pealThree.SetRingerId(defaultRingerId, 10, false);
            database.PealsDatabase().AddObject(pealThree);
            Peal pealFour;
            pealFour.SetDate(PerformanceDate(2013, 01, 04));
            pealFour.SetRingerId(ringerFourId, 4, false);
            pealFour.SetRingerId(defaultRingerId, 10, false);
            database.PealsDatabase().AddObject(pealFour);
            Peal pealFive;
            pealFive.SetDate(PerformanceDate(2013, 02, 04));
            pealFive.SetRingerId(ringerFourId, 4, false);
            pealFive.SetRingerId(defaultRingerId, 10, false);
            database.PealsDatabase().AddObject(pealFive);
            Assert::IsFalse(database.Settings().DefaultRingerSet());
            Assert::IsTrue(database.RingersDatabase().LinkRingers(ringerOneId, ringerFourId));

            std::map<ObjectId, std::map<unsigned int, Duco::PealLengthInfo> > sortedPealCounts;
            database.PealsDatabase().GetTopRingersPealCountPerYear(filters, true, 5, sortedPealCounts);

            Assert::AreEqual((size_t)3, sortedPealCounts.size());
            Assert::IsFalse(sortedPealCounts.contains(ringerOneId));

            // Non combined ringers
            std::map<unsigned int, Duco::PealLengthInfo> ringerTwoCounts = sortedPealCounts[ringerTwoId];
            Assert::AreEqual((size_t)4, ringerTwoCounts.size());
            Assert::AreEqual((size_t)2, ringerTwoCounts[2010].TotalPeals());
            Assert::AreEqual((size_t)0, ringerTwoCounts[2011].TotalPeals());
            Assert::AreEqual((size_t)0, ringerTwoCounts[2012].TotalPeals());

            std::map<unsigned int, Duco::PealLengthInfo> ringerThreeCounts = sortedPealCounts[ringerThreeId];
            Assert::AreEqual((size_t)4, ringerThreeCounts.size());
            Assert::AreEqual((size_t)1, ringerThreeCounts[2010].TotalPeals());
            Assert::AreEqual((size_t)0, ringerThreeCounts[2011].TotalPeals());
            Assert::AreEqual((size_t)1, ringerThreeCounts[2012].TotalPeals());

            // Combined ringers
            std::map<unsigned int, Duco::PealLengthInfo> ringerOneAndFourCounts = sortedPealCounts[ringerFourId];
            Assert::AreEqual((size_t)4, ringerOneAndFourCounts.size());
            Assert::AreEqual((size_t)2, ringerOneAndFourCounts[2010].TotalPeals());
            Assert::AreEqual((size_t)0, ringerOneAndFourCounts[2011].TotalPeals());
            Assert::AreEqual((size_t)1, ringerOneAndFourCounts[2012].TotalPeals());
            Assert::AreEqual((size_t)2, ringerOneAndFourCounts[2013].TotalPeals());

        }

        TEST_METHOD(PealDatabase_GetStagesCountByYear)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);
            StatisticFilters filters(database);

            Duco::ObjectId sixBellMethodId = database.MethodsDatabase().AddMethod(L"Plain", L"Bob", 6, false);
            //Duco::ObjectId eightBellMethodId = database.MethodsDatabase().AddMethod(L"Plain", L"Bob", 8, false);
            Duco::ObjectId elevenBellMethodId = database.MethodsDatabase().AddMethod(L"Plain", L"Bob", 11, false);
            Duco::ObjectId twelveBellMethodId = database.MethodsDatabase().AddMethod(L"Plain", L"Bob", 12, false);


            Peal pealOne;
            pealOne.SetDate(PerformanceDate(2010, 01, 01));
            pealOne.SetMethodId(elevenBellMethodId);
            database.PealsDatabase().AddObject(pealOne);
            Peal pealTwo;
            pealTwo.SetDate(PerformanceDate(2012, 01, 03));
            pealTwo.SetMethodId(twelveBellMethodId);
            database.PealsDatabase().AddObject(pealTwo);
            Peal pealThree;
            pealThree.SetDate(PerformanceDate(2010, 01, 02));
            pealThree.SetMethodId(sixBellMethodId);
            database.PealsDatabase().AddObject(pealThree);
            Peal pealFour;
            pealFour.SetDate(PerformanceDate(2013, 01, 04));
            pealFour.SetMethodId(elevenBellMethodId);
            database.PealsDatabase().AddObject(pealFour);

            std::map<unsigned int, std::map<unsigned int, Duco::PealLengthInfo> > sortedPealCounts;
            database.PealsDatabase().GetStagesCountByYear(filters, sortedPealCounts);

            Assert::AreEqual((size_t)7, sortedPealCounts.size());

            Assert::AreEqual((size_t)4, sortedPealCounts[6].size());
            Assert::AreEqual((size_t)1, sortedPealCounts[6][2010].TotalPeals());
            Assert::AreEqual((size_t)0, sortedPealCounts[6][2011].TotalPeals());
            Assert::AreEqual((size_t)0, sortedPealCounts[6][2012].TotalPeals());
            Assert::AreEqual((size_t)0, sortedPealCounts[6][2013].TotalPeals());

            Assert::AreEqual((size_t)4, sortedPealCounts[7].size());
            Assert::AreEqual((size_t)0, sortedPealCounts[7][2010].TotalPeals());
            Assert::AreEqual((size_t)0, sortedPealCounts[7][2011].TotalPeals());
            Assert::AreEqual((size_t)0, sortedPealCounts[7][2012].TotalPeals());
            Assert::AreEqual((size_t)0, sortedPealCounts[7][2013].TotalPeals());

            Assert::AreEqual((size_t)4, sortedPealCounts[8].size());
            Assert::AreEqual((size_t)0, sortedPealCounts[8][2010].TotalPeals());
            Assert::AreEqual((size_t)0, sortedPealCounts[8][2011].TotalPeals());
            Assert::AreEqual((size_t)0, sortedPealCounts[8][2012].TotalPeals());
            Assert::AreEqual((size_t)0, sortedPealCounts[8][2013].TotalPeals());

            Assert::AreEqual((size_t)4, sortedPealCounts[9].size());
            Assert::AreEqual((size_t)0, sortedPealCounts[9][2010].TotalPeals());
            Assert::AreEqual((size_t)0, sortedPealCounts[9][2011].TotalPeals());
            Assert::AreEqual((size_t)0, sortedPealCounts[9][2012].TotalPeals());
            Assert::AreEqual((size_t)0, sortedPealCounts[9][2013].TotalPeals());

            Assert::AreEqual((size_t)4, sortedPealCounts[10].size());
            Assert::AreEqual((size_t)0, sortedPealCounts[10][2010].TotalPeals());
            Assert::AreEqual((size_t)0, sortedPealCounts[10][2011].TotalPeals());
            Assert::AreEqual((size_t)0, sortedPealCounts[10][2012].TotalPeals());
            Assert::AreEqual((size_t)0, sortedPealCounts[10][2013].TotalPeals());

            Assert::AreEqual((size_t)4, sortedPealCounts[11].size());
            Assert::AreEqual((size_t)1, sortedPealCounts[11][2010].TotalPeals());
            Assert::AreEqual((size_t)0, sortedPealCounts[11][2011].TotalPeals());
            Assert::AreEqual((size_t)0, sortedPealCounts[11][2012].TotalPeals());
            Assert::AreEqual((size_t)1, sortedPealCounts[11][2013].TotalPeals());

            Assert::AreEqual((size_t)4, sortedPealCounts[12].size());
            Assert::AreEqual((size_t)0, sortedPealCounts[12][2010].TotalPeals());
            Assert::AreEqual((size_t)0, sortedPealCounts[12][2011].TotalPeals());
            Assert::AreEqual((size_t)1, sortedPealCounts[12][2012].TotalPeals());
            Assert::AreEqual((size_t)0, sortedPealCounts[12][2013].TotalPeals());
        }

        TEST_METHOD(PealDatabase_GetStagesCountByYear_EmptyDatabase)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);
            StatisticFilters filters(database);

            std::map<unsigned int, std::map<unsigned int, Duco::PealLengthInfo> > sortedPealCounts;
            database.PealsDatabase().GetStagesCountByYear(filters, sortedPealCounts);

            Assert::AreEqual((size_t)0, sortedPealCounts.size());
        }

        TEST_METHOD(PealDatabase_GetTopTowersPealCountPerYear)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);
            StatisticFilters filters(database);

            ObjectId towerOneId(1);
            ObjectId towerTwoId(2);
            ObjectId towerThreeId(3);

            Peal pealOne;
            pealOne.SetDate(PerformanceDate(2010, 01, 01));
            pealOne.SetTowerId(towerOneId);
            database.PealsDatabase().AddObject(pealOne);
            Peal pealTwo;
            pealTwo.SetDate(PerformanceDate(2012, 01, 03));
            pealTwo.SetTowerId(towerTwoId);
            database.PealsDatabase().AddObject(pealTwo);
            Peal pealThree;
            pealThree.SetDate(PerformanceDate(2011, 01, 02));
            pealThree.SetTowerId(towerThreeId);
            database.PealsDatabase().AddObject(pealThree);
            Peal pealFour;
            pealFour.SetDate(PerformanceDate(2013, 01, 04));
            pealFour.SetTowerId(towerTwoId);
            database.PealsDatabase().AddObject(pealFour);

            std::map<Duco::ObjectId, std::map<unsigned int, Duco::PealLengthInfo> > sortedPealCounts;
            database.PealsDatabase().GetTopTowersPealCountPerYear(filters, 5, sortedPealCounts);

            Assert::AreEqual((size_t)3, sortedPealCounts.size());

            Assert::AreEqual((size_t)4, sortedPealCounts[towerOneId].size());
            Assert::AreEqual((size_t)1, sortedPealCounts[towerOneId][2010].TotalPeals());
            Assert::AreEqual((size_t)0, sortedPealCounts[towerOneId][2011].TotalPeals());
            Assert::AreEqual((size_t)0, sortedPealCounts[towerOneId][2012].TotalPeals());
            Assert::AreEqual((size_t)0, sortedPealCounts[towerOneId][2013].TotalPeals());

            Assert::AreEqual((size_t)4, sortedPealCounts[towerTwoId].size());
            Assert::AreEqual((size_t)0, sortedPealCounts[towerTwoId][2010].TotalPeals());
            Assert::AreEqual((size_t)0, sortedPealCounts[towerTwoId][2011].TotalPeals());
            Assert::AreEqual((size_t)1, sortedPealCounts[towerTwoId][2012].TotalPeals());
            Assert::AreEqual((size_t)1, sortedPealCounts[towerTwoId][2013].TotalPeals());

            Assert::AreEqual((size_t)4, sortedPealCounts[towerThreeId].size());
            Assert::AreEqual((size_t)0, sortedPealCounts[towerThreeId][2010].TotalPeals());
            Assert::AreEqual((size_t)1, sortedPealCounts[towerThreeId][2011].TotalPeals());
            Assert::AreEqual((size_t)0, sortedPealCounts[towerThreeId][2012].TotalPeals());
            Assert::AreEqual((size_t)0, sortedPealCounts[towerThreeId][2013].TotalPeals());
        }

        TEST_METHOD(PealDatabase_GetTopTowersPealCountPerYear_EmptyDatabase)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);
            StatisticFilters filters(database);

            std::map<Duco::ObjectId, std::map<unsigned int, Duco::PealLengthInfo> > sortedPealCounts;
            database.PealsDatabase().GetTopTowersPealCountPerYear(filters, 5, sortedPealCounts);

            Assert::AreEqual((size_t)0, sortedPealCounts.size());
        }

        TEST_METHOD(PealDatabase_GetUniqueCounts)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);
            StatisticFilters filters(database);

            Peal pealOne;
            pealOne.SetDate(PerformanceDate(2010, 01, 01));
            pealOne.SetTowerId(1);
            pealOne.SetMethodId(1);
            pealOne.SetRingerId(1,1,false);
            database.PealsDatabase().AddObject(pealOne);
            Peal pealTwo;
            pealTwo.SetDate(PerformanceDate(2012, 01, 04));
            pealTwo.SetTowerId(5);
            pealTwo.SetMethodId(3);
            pealTwo.SetRingerId(5, 1, false);
            database.PealsDatabase().AddObject(pealTwo);
            Peal pealThree;
            pealThree.SetDate(PerformanceDate(2010, 01, 02));
            pealThree.SetTowerId(1);
            pealThree.SetMethodId(1);
            pealThree.SetRingerId(1, 1, false);
            database.PealsDatabase().AddObject(pealThree);
            Peal pealFour;
            pealFour.SetDate(PerformanceDate(2014, 01, 06));
            pealFour.SetTowerId(3);
            pealFour.SetMethodId(4);
            database.PealsDatabase().AddObject(pealFour);
            Peal pealFive;
            pealFive.SetDate(PerformanceDate(2012, 01, 05));
            pealFive.SetTowerId(3);
            pealFive.SetMethodId(2);
            pealFive.SetRingerId(3, 1, false);
            pealFive.SetRingerId(4, 2, false);
            database.PealsDatabase().AddObject(pealFive);
            Peal pealSix;
            pealSix.SetDate(PerformanceDate(2011, 01, 03));
            pealSix.SetTowerId(3);
            pealSix.SetMethodId(1);
            database.PealsDatabase().AddObject(pealSix);

            std::map<unsigned int, size_t> towerCounts;
            std::map<unsigned int, size_t> ringerCounts;
            std::map<unsigned int, size_t> methodCounts;
            database.PealsDatabase().GetUniqueCounts(filters, towerCounts, ringerCounts, methodCounts);

            Assert::AreEqual((size_t)5, towerCounts.size());
            Assert::AreEqual((size_t)1, towerCounts[2010]);
            Assert::AreEqual((size_t)1, towerCounts[2011]);
            Assert::AreEqual((size_t)2, towerCounts[2012]);
            Assert::AreEqual((size_t)0, towerCounts[2013]);
            Assert::AreEqual((size_t)1, towerCounts[2014]);

            Assert::AreEqual((size_t)5, ringerCounts.size());
            Assert::AreEqual((size_t)1, ringerCounts[2010]);
            Assert::AreEqual((size_t)0, ringerCounts[2011]);
            Assert::AreEqual((size_t)3, ringerCounts[2012]);
            Assert::AreEqual((size_t)0, ringerCounts[2013]);
            Assert::AreEqual((size_t)0, ringerCounts[2014]);

            Assert::AreEqual((size_t)5, methodCounts.size());
            Assert::AreEqual((size_t)1, methodCounts[2010]);
            Assert::AreEqual((size_t)1, methodCounts[2011]);
            Assert::AreEqual((size_t)2, methodCounts[2012]);
            Assert::AreEqual((size_t)0, methodCounts[2013]);
            Assert::AreEqual((size_t)1, methodCounts[2014]);
        }

        TEST_METHOD(PealDatabase_GetUniqueCounts_EmptyDatabase)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);
            StatisticFilters filters(database);

            std::map<unsigned int, size_t> towerCounts;
            std::map<unsigned int, size_t> ringerCounts;
            std::map<unsigned int, size_t> methodCounts;
            database.PealsDatabase().GetUniqueCounts(filters, towerCounts, ringerCounts, methodCounts);

            Assert::AreEqual((size_t)0, towerCounts.size());
            Assert::AreEqual((size_t)0, ringerCounts.size());
            Assert::AreEqual((size_t)0, methodCounts.size());
        }

        TEST_METHOD(PealDatabase_RemoveExistingBellBoardPealIds)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Peal pealOne;
            pealOne.SetBellBoardId(L"1351637");
            database.PealsDatabase().AddObject(pealOne);
            Peal pealTwo;
            pealTwo.SetBellBoardId(L"1350378");
            database.PealsDatabase().AddObject(pealTwo);

            std::list<std::wstring> originalPealIds = { L"1", L"1350378", L"1351637", L"1351117", L"2"};
            std::set<std::wstring> expectedExistingPealIds = { L"1350378", L"1351637" };
            std::list<std::wstring> expectedRemovedPealIds = { L"1", L"1351117", L"2" };
            std::set<std::wstring> pealIds = database.PealsDatabase().RemoveExistingBellBoardPealIds(originalPealIds);

            Assert::AreEqual(expectedExistingPealIds, pealIds);
            Assert::AreEqual(expectedRemovedPealIds, originalPealIds);
        }

        TEST_METHOD(PealDatabase_RemoveExistingBellBoardPealIds_EmptyDatabase)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            std::list<std::wstring> originalPealIds = { L"1", L"2", L"3", L"1351117" };
            std::list<std::wstring> originalPealIdsCopy (originalPealIds.begin(), originalPealIds.end());
            std::set<std::wstring> expectedPealIds = { };
            std::set<std::wstring> pealIds = database.PealsDatabase().RemoveExistingBellBoardPealIds(originalPealIds);

            Assert::AreEqual(expectedPealIds, pealIds);
            Assert::AreEqual(originalPealIds, originalPealIdsCopy);
        }

        TEST_METHOD(PealDatabase_FirstAndLastPealDate)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);
            Tower theTower;
            Duco::ObjectId towerId = database.TowersDatabase().AddObject(theTower);

            PerformanceDate firstPealDate(1950, 1, 1);
            PerformanceDate secondPealDate(1950, 1, 2);
            PerformanceDate thirdPealDate(1951, 1, 3);
            PerformanceDate fourthPealDate(1950, 1, 4);

            Peal pealTwo;
            pealTwo.SetTowerId(towerId);
            pealTwo.SetDate(secondPealDate);
            database.PealsDatabase().AddObject(pealTwo);
            Peal pealthree;
            pealthree.SetTowerId(towerId);
            pealthree.SetDate(thirdPealDate);
            Duco::ObjectId lastPealId = database.PealsDatabase().AddObject(pealthree);
            Peal pealFour;
            pealFour.SetTowerId(towerId);
            pealFour.SetDate(fourthPealDate);
            database.PealsDatabase().AddObject(pealFour);
            Peal pealOne;
            pealOne.SetTowerId(towerId);
            pealOne.SetDate(firstPealDate);
            Duco::ObjectId firstPealId = database.PealsDatabase().AddObject(pealOne);
            Duco::StatisticFilters filters(database);

            Assert::AreEqual((size_t)4, database.PerformanceInfo(filters).TotalPeals());

            filters.SetTower(true, towerId);

            PealLengthInfo info = database.PealsDatabase().PerformanceInfo(filters);
            Assert::AreEqual(firstPealDate, info.DateOfFirstPeal());
            Assert::AreEqual(thirdPealDate, info.DateOfLastPeal());
            Assert::AreEqual(firstPealId, info.FirstPealId());
            Assert::AreEqual(lastPealId, info.LastPealId());
        }

        TEST_METHOD(PealDatabase_FirstPealWithoutBellboardId)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);
            Duco::ObjectId towerId(1);

            PerformanceDate firstPealDate(1950, 1, 1);
            PerformanceDate secondPealDate(1950, 1, 2);
            PerformanceDate thirdPealDate(1950, 1, 3);
            PerformanceDate fourthPealDate(1950, 1, 4);

            Peal pealTwo;
            pealTwo.SetTowerId(towerId);
            pealTwo.SetDate(secondPealDate);
            Duco::ObjectId pealId = database.PealsDatabase().AddObject(pealTwo);
            Peal pealthree;
            pealthree.SetTowerId(towerId);
            pealthree.SetDate(thirdPealDate);
            database.PealsDatabase().AddObject(pealthree);
            Peal pealFour;
            pealFour.SetBellBoardId(L"4");
            pealFour.SetTowerId(towerId);
            pealFour.SetDate(fourthPealDate);
            database.PealsDatabase().AddObject(pealFour);
            Peal pealOne;
            pealOne.SetBellBoardId(L"1");
            pealOne.SetTowerId(towerId);
            pealOne.SetDate(firstPealDate);
            database.PealsDatabase().AddObject(pealOne);
            Duco::StatisticFilters filters(database);

            Assert::AreEqual((size_t)4, database.PerformanceInfo(filters).TotalPeals());
            PerformanceDate pealDate;
            Assert::AreEqual(pealId, database.PealsDatabase().FindFirstPealWithoutBellboardId(pealDate));
            Assert::AreEqual(secondPealDate, pealDate);
        }

        TEST_METHOD(PealDatabase_FindLastPealBeforeDate)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);
            Duco::ObjectId towerId(1);

            PerformanceDate firstPealDate(1950, 1, 1);
            PerformanceDate secondPealDate(1950, 1, 2);
            PerformanceDate thirdPealDate(1950, 1, 3);
            PerformanceDate fourthPealDate(1950, 1, 4);

            Peal pealTwo;
            pealTwo.SetBellBoardId(L"2");
            pealTwo.SetTowerId(towerId);
            pealTwo.SetDate(secondPealDate);
            Duco::ObjectId pealTwoId = database.PealsDatabase().AddObject(pealTwo);
            Peal pealthree;
            pealthree.SetTowerId(towerId);
            pealthree.SetDate(thirdPealDate);
            Duco::ObjectId pealThreeId = database.PealsDatabase().AddObject(pealthree);
            Peal pealFour;
            pealFour.SetTowerId(towerId);
            pealFour.SetDate(fourthPealDate);
            Duco::ObjectId pealFourId = database.PealsDatabase().AddObject(pealFour);
            Peal pealOne;
            pealOne.SetBellBoardId(L"1");
            pealOne.SetTowerId(towerId);
            pealOne.SetDate(firstPealDate);
            Duco::ObjectId pealOneId = database.PealsDatabase().AddObject(pealOne);
            Duco::StatisticFilters filters(database);

            Assert::AreEqual((size_t)4, database.PerformanceInfo(filters).TotalPeals());

            filters.SetEndDate(true, thirdPealDate);
            Duco::PealLengthInfo info = database.PealsDatabase().PerformanceInfo(filters);
            Assert::AreEqual(pealThreeId, info.LastPealId());
        }

        TEST_METHOD(PealDatabase_PealsInYear)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);
            Duco::ObjectId towerId(1);

            unsigned int pealYearToFind = 1950;

            PerformanceDate firstPealDate(1949-1, 12, 31);
            PerformanceDate secondPealDate(pealYearToFind, 1, 1);
            PerformanceDate thirdPealDate(pealYearToFind, 5, 7);
            PerformanceDate fourthPealDate(pealYearToFind, 12, 31);
            PerformanceDate fifthPealDate(pealYearToFind+1, 1, 1);

            Peal pealOne;
            pealOne.SetTowerId(towerId);
            pealOne.SetDate(firstPealDate);
            Duco::ObjectId pealOneId = database.PealsDatabase().AddObject(pealOne);
            Peal pealTwo;
            pealTwo.SetTowerId(towerId);
            pealTwo.SetDate(secondPealDate);
            Duco::ObjectId pealTwoId = database.PealsDatabase().AddObject(pealTwo);
            Peal pealThree;
            pealThree.SetTowerId(towerId);
            pealThree.SetDate(thirdPealDate);
            Duco::ObjectId pealThreeId = database.PealsDatabase().AddObject(pealThree);
            Peal pealFour;
            pealFour.SetTowerId(towerId);
            pealFour.SetDate(fourthPealDate);
            Duco::ObjectId pealFourId = database.PealsDatabase().AddObject(pealFour);
            Peal pealFive;
            pealFive.SetTowerId(towerId);
            pealFive.SetDate(fifthPealDate);
            Duco::ObjectId pealFiveId = database.PealsDatabase().AddObject(pealFive);

            Duco::StatisticFilters filters(database);
            Assert::AreEqual((size_t)5, database.PerformanceInfo(filters).TotalPeals());

            std::set<ObjectId> pealIds;
            database.PealsDatabase().GetPealsForYear(pealYearToFind, pealIds);
            Assert::AreEqual((size_t)3, pealIds.size());
            Assert::IsFalse(std::find(pealIds.begin(), pealIds.end(), pealOneId) != pealIds.end());
            Assert::IsFalse(std::find(pealIds.begin(), pealIds.end(), pealFiveId) != pealIds.end());
            Assert::IsTrue(pealIds.find(pealTwoId) != pealIds.end());
            Assert::IsTrue(pealIds.find(pealThreeId) != pealIds.end());
            Assert::IsTrue(pealIds.find(pealFourId) != pealIds.end());
        }

        TEST_METHOD(PealDatabase_YearsInRange)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);
            Duco::ObjectId towerId(1);

            Peal pealOne;
            PerformanceDate firstPealDate(1998, 12, 31);
            pealOne.SetDate(firstPealDate);
            Duco::ObjectId pealOneId = database.PealsDatabase().AddObject(pealOne);
            Peal pealTwo;
            PerformanceDate secondPealDate(1999, 11, 31);
            pealTwo.SetDate(secondPealDate);
            Duco::ObjectId pealTwoId = database.PealsDatabase().AddObject(pealTwo);
            Peal pealThree;
            PerformanceDate thirdPealDate(1999, 12, 31);
            pealThree.SetDate(thirdPealDate);
            Duco::ObjectId pealThreeId = database.PealsDatabase().AddObject(pealThree);
            Peal pealFour;
            PerformanceDate fourthPealDate(2001, 12, 31);
            pealFour.SetDate(fourthPealDate);
            Duco::ObjectId pealFourId = database.PealsDatabase().AddObject(pealFour);

            std::set<unsigned int> years;
            Duco::StatisticFilters filters(database);
            database.PealsDatabase().GetYearRange(filters, years);

            Assert::AreEqual((size_t)3, years.size());
            Assert::IsTrue(years.find(1998) != years.end());
            Assert::IsTrue(years.find(1999) != years.end());
            Assert::IsTrue(years.find(2001) != years.end());
        }

        TEST_METHOD(PealDatabase_GetRingersTowerCircling_SinglePeal)
        {
            RingingDatabase database;
            unsigned int databaseVersion = 0;
            database.Internalise("Single_peal.duc", NULL, databaseVersion);
            std::map<Duco::ObjectId, std::map<unsigned int, Duco::PealLengthInfo>> circlingData;
            std::set<Duco::ObjectId> ringerIds = {1,2,3,4,5,6,7,8,9,10};

            StatisticFilters filters(database);
            filters.SetTower(true, 1);
            database.PealsDatabase().GetRingersTowerCircling(filters, 1, ringerIds, circlingData);

            Assert::AreEqual(circlingData.size(), (size_t)10);
            Assert::AreEqual(circlingData[1].size(), (size_t)10);
            Assert::AreEqual(circlingData[2].size(), (size_t)10);
            Assert::AreEqual(circlingData[3].size(), (size_t)10);
            Assert::AreEqual(circlingData[4].size(), (size_t)10);
            Assert::AreEqual(circlingData[5].size(), (size_t)10);
            Assert::AreEqual(circlingData[6].size(), (size_t)10);
            Assert::AreEqual(circlingData[7].size(), (size_t)10);
            Assert::AreEqual(circlingData[8].size(), (size_t)10);
            Assert::AreEqual(circlingData[9].size(), (size_t)10);
            Assert::AreEqual(circlingData[10].size(), (size_t)10);

            Assert::AreEqual(circlingData[1][4].TotalPeals(), (size_t)1);
            Assert::AreEqual(circlingData[2][1].TotalPeals(), (size_t)1);
            Assert::AreEqual(circlingData[3][2].TotalPeals(), (size_t)1);
            Assert::AreEqual(circlingData[4][3].TotalPeals(), (size_t)1);
            Assert::AreEqual(circlingData[5][5].TotalPeals(), (size_t)1);
            Assert::AreEqual(circlingData[6][6].TotalPeals(), (size_t)1);
            Assert::AreEqual(circlingData[7][7].TotalPeals(), (size_t)1);
            Assert::AreEqual(circlingData[8][8].TotalPeals(), (size_t)1);
            Assert::AreEqual(circlingData[9][8].TotalPeals(), (size_t)0);
            Assert::AreEqual(circlingData[9][9].TotalPeals(), (size_t)1);
            Assert::AreEqual(circlingData[9][10].TotalPeals(), (size_t)0);
            Assert::IsFalse(circlingData[10].find(9) == circlingData[10].end());
            Assert::AreEqual(circlingData[10][8].TotalPeals(), (size_t)0);
            Assert::AreEqual(circlingData[10][9].TotalPeals(), (size_t)0);
            Assert::AreEqual(circlingData[10][10].TotalPeals(), (size_t)1);
        }

        TEST_METHOD(PealDatabase_DateRange_WithJennie)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)36, databaseVersionNumber);

            StatisticFilters filters(database);
            filters.SetRinger(true, 233);

            PealLengthInfo info = database.PealsDatabase().PerformanceInfo(filters);
            Assert::AreEqual(Duco::PerformanceDate(1993, 3, 27), info.DateOfFirstPeal());
            Assert::AreEqual(Duco::PerformanceDate(1999, 9, 4), info.DateOfLastPeal());
        }

        TEST_METHOD(PealDatabase_FirstTowerCircling_SinglePealOnAllBells)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)36, databaseVersionNumber);

            PerformanceDate unsetDate;
            unsetDate.ResetToEarliest();

            std::map<unsigned int, Duco::CirclingData> sortedBellPealCounts;
            PerformanceDate firstCircledDate;
            size_t daysToFirstCircle;
            StatisticFilters filters(database);
            filters.SetTower(true, 203); // St Benedicts Northampton
            Duco::PealLengthInfo noOfPeals;

            database.PealsDatabase().GetTowerCircling(filters, sortedBellPealCounts, noOfPeals, firstCircledDate, daysToFirstCircle);
            Assert::AreEqual((size_t)6, noOfPeals.TotalPeals());
            Assert::AreEqual(PerformanceDate(1998, 1, 17), firstCircledDate);
            Assert::AreEqual((size_t)6, sortedBellPealCounts.size());
            Assert::AreEqual((size_t)1, sortedBellPealCounts[1].PealCount());
            Assert::AreEqual((size_t)1, sortedBellPealCounts[2].PealCount());
            Assert::AreEqual((size_t)1, sortedBellPealCounts[3].PealCount());
            Assert::AreEqual((size_t)1, sortedBellPealCounts[4].PealCount());
            Assert::AreEqual((size_t)1, sortedBellPealCounts[5].PealCount());
            Assert::AreEqual((size_t)1, sortedBellPealCounts[6].PealCount());

            filters.SetTower(true, 68); // Daventry
            database.PealsDatabase().GetTowerCircling(filters, sortedBellPealCounts, noOfPeals, firstCircledDate, daysToFirstCircle);
            Assert::AreEqual((size_t)17, noOfPeals.TotalPeals());
            Assert::AreEqual(unsetDate, firstCircledDate);
            Assert::AreEqual((size_t)9, sortedBellPealCounts.size()); // Just the third missing!
            Assert::AreEqual((size_t)1, sortedBellPealCounts[1].PealCount());
            Assert::AreEqual((size_t)1, sortedBellPealCounts[2].PealCount());
            Assert::AreEqual((size_t)0, sortedBellPealCounts[3].PealCount());
            Assert::AreEqual((size_t)1, sortedBellPealCounts[4].PealCount());
            Assert::AreEqual((size_t)2, sortedBellPealCounts[5].PealCount());
            Assert::AreEqual((size_t)3, sortedBellPealCounts[6].PealCount());
            Assert::AreEqual((size_t)1, sortedBellPealCounts[7].PealCount());
            Assert::AreEqual((size_t)5, sortedBellPealCounts[8].PealCount());
            Assert::AreEqual((size_t)2, sortedBellPealCounts[9].PealCount());
            Assert::AreEqual((size_t)1, sortedBellPealCounts[10].PealCount());

            filters.Clear();
            Duco::PealLengthInfo info = database.PealsDatabase().PerformanceInfo(filters);
            Assert::AreEqual(PerformanceDate(1990, 12, 8), info.DateOfFirstPeal());
            Assert::AreEqual(PerformanceDate(2020, 2, 8), info.DateOfLastPeal());
        }

        TEST_METHOD(PealDatabase_FirstTowerCircling_MulitplePeals)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)36, databaseVersionNumber);

            std::map<unsigned int, Duco::CirclingData> sortedBellPealCounts;
            PerformanceDate firstCircledDate;
            StatisticFilters filters(database);
            filters.SetTower(true, 292); //Walkden

            Duco::PealLengthInfo noOfPeals;
            size_t daysToFirstCircle;

            database.PealsDatabase().GetTowerCircling(filters, sortedBellPealCounts, noOfPeals, firstCircledDate, daysToFirstCircle);
            Assert::AreEqual((size_t)48, noOfPeals.TotalPeals());
            Assert::AreEqual(PerformanceDate(2010, 5, 12), firstCircledDate);
            Assert::AreEqual((size_t)8, sortedBellPealCounts.size());
            Assert::AreEqual((size_t)11, sortedBellPealCounts[1].PealCount());
            Assert::AreEqual((size_t)4, sortedBellPealCounts[2].PealCount());
            Assert::AreEqual((size_t)1, sortedBellPealCounts[3].PealCount());
            Assert::AreEqual((size_t)7, sortedBellPealCounts[4].PealCount());
            Assert::AreEqual((size_t)8, sortedBellPealCounts[5].PealCount());
            Assert::AreEqual((size_t)6, sortedBellPealCounts[6].PealCount());
            Assert::AreEqual((size_t)10, sortedBellPealCounts[7].PealCount());
            Assert::AreEqual((size_t)1, sortedBellPealCounts[8].PealCount());

        }

        TEST_METHOD(PealDatabase_GetLeadingRingersTowerCircling_SinglePeal)
        {
            RingingDatabase database;
            unsigned int databaseVersion = 0;
            database.Internalise("Single_peal.duc", NULL, databaseVersion);

            std::multimap<PerformanceDate, RingerCirclingData*> topCircledRingers;
            database.PealsDatabase().GetLeadingRingersTowerCircling(1, topCircledRingers, 8, false);

            Assert::AreEqual((size_t)0, topCircledRingers.size());

        }

        TEST_METHOD(PealDatabase_GetLeadingRingersTowerCircling_Lundy)
        {
            RingingDatabase database;
            unsigned int databaseVersion = 0;
            database.Internalise("lundy_DBVer_33.duc", NULL, databaseVersion);

            std::multimap<PerformanceDate, RingerCirclingData*> topCircledRingers;
            database.PealsDatabase().GetLeadingRingersTowerCircling(1, topCircledRingers, 5, false);

            Assert::AreEqual((size_t)5, topCircledRingers.size());
            Duco::PerformanceDate dateCircled;
            size_t numberOfCircles(0);

            PerformanceDate peterRandallCircledTime(2007,9,4);
            RingerCirclingData* firstRinger = topCircledRingers.find(peterRandallCircledTime)->second;
            Assert::IsNotNull(firstRinger);
            Assert::IsTrue(firstRinger->Circled(10));
            Assert::IsFalse(firstRinger->Circled(8));
            Assert::IsTrue(firstRinger->FirstCircleDate(dateCircled, 10));
            Assert::AreEqual(PerformanceDate(2007, 9, 4), dateCircled);
            Assert::IsTrue(firstRinger->LastCircleDate(dateCircled, numberOfCircles, 10));
            Assert::AreEqual(PerformanceDate(2013, 9, 13), dateCircled);
            Assert::AreEqual((size_t)3, numberOfCircles);
            Assert::AreEqual(Duco::ObjectId(402), firstRinger->RingerId());
        }

        TEST_METHOD(PealDatabase_RingersInTower_Lundy_All)
        {
            RingingDatabase database;
            unsigned int databaseVersion = 0;
            database.Internalise("lundy_DBVer_33.duc", NULL, databaseVersion);

            std::set<Duco::ObjectId> leadingRingers;
            database.PealsDatabase().GetRingersInTower(1, leadingRingers);

            Assert::AreEqual((size_t)593, leadingRingers.size());
            Assert::IsTrue(leadingRingers.find(75) != leadingRingers.end());
        }

        TEST_METHOD(PealDatabase_RingersInTower_Owen_All)
        {
            RingingDatabase database;
            unsigned int databaseVersion = 0;
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            database.Internalise(sampleDatabasename.c_str(), NULL, databaseVersion);

            std::set<Duco::ObjectId> leadingRingers;
            database.PealsDatabase().GetRingersInTower(164, leadingRingers);

            Assert::AreEqual((size_t)66, leadingRingers.size());
            Assert::IsTrue(leadingRingers.find(19) != leadingRingers.end());
            Assert::IsTrue(leadingRingers.find(272) == leadingRingers.end());

            database.PealsDatabase().GetRingersInTower(164, leadingRingers, 5);

            Assert::AreEqual((size_t)5, leadingRingers.size());
            Assert::IsTrue(leadingRingers.find(19) != leadingRingers.end());
            Assert::IsTrue(leadingRingers.find(261) != leadingRingers.end());
            Assert::IsTrue(leadingRingers.find(283) != leadingRingers.end());
            Assert::IsTrue(leadingRingers.find(49) != leadingRingers.end());
            Assert::IsTrue(leadingRingers.find(48) != leadingRingers.end());
            Assert::IsTrue(leadingRingers.find(257) == leadingRingers.end());

            database.PealsDatabase().GetRingersInTower(164, leadingRingers, 6);

            Assert::AreEqual((size_t)7, leadingRingers.size());
            Assert::IsTrue(leadingRingers.find(19) != leadingRingers.end());
            Assert::IsTrue(leadingRingers.find(261) != leadingRingers.end());
            Assert::IsTrue(leadingRingers.find(283) != leadingRingers.end());
            Assert::IsTrue(leadingRingers.find(49) != leadingRingers.end());
            Assert::IsTrue(leadingRingers.find(48) != leadingRingers.end());
            Assert::IsTrue(leadingRingers.find(257) != leadingRingers.end());
            Assert::IsTrue(leadingRingers.find(347) != leadingRingers.end());
            Assert::IsTrue(leadingRingers.find(77) == leadingRingers.end());
        }

        TEST_METHOD(PealDatabase_GetTownNamesWithPealCount_Owen)
        {
            RingingDatabase database;
            unsigned int databaseVersion = 0;
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            database.Internalise(sampleDatabasename.c_str(), NULL, databaseVersion);

            StatisticFilters filters(database);
            std::map<std::wstring, PealLengthInfo> sortedTowerIds;
            database.PealsDatabase().GetTownNamesWithPealCount(filters, sortedTowerIds);

            Assert::AreEqual((size_t)150, sortedTowerIds.size());
            Assert::AreEqual((size_t)99, sortedTowerIds.find(L"BURNLEY")->second.TotalPeals());
            Assert::AreEqual((size_t)22, sortedTowerIds.find(L"NORTHAMPTON")->second.TotalPeals());

            filters.SetAssociation(true, 1);
            database.PealsDatabase().GetTownNamesWithPealCount(filters, sortedTowerIds);

            Assert::AreEqual((size_t)92, sortedTowerIds.size());
            Assert::AreEqual((size_t)99, sortedTowerIds.find(L"BURNLEY")->second.TotalPeals());
            Assert::IsTrue(sortedTowerIds.find(L"MOULTON") == sortedTowerIds.end());
        }

        TEST_METHOD(PealDatabase_GetCountiesWithPealCount_Owen)
        {
            RingingDatabase database;
            unsigned int databaseVersion = 0;
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            database.Internalise(sampleDatabasename.c_str(), NULL, databaseVersion);

            StatisticFilters filters(database);
            std::map<std::wstring, PealLengthInfo> sortedTowerIds;
            database.PealsDatabase().GetCountiesWithPealCount(filters, sortedTowerIds);

            Assert::AreEqual((size_t)45, sortedTowerIds.size());
            Assert::AreEqual((size_t)212, sortedTowerIds.find(L"Lancashire")->second.TotalPeals());
            Assert::AreEqual((size_t)58, sortedTowerIds.find(L"Devon")->second.TotalPeals());
            Assert::AreEqual((size_t)6, sortedTowerIds.find(L"Worcestershire")->second.TotalPeals());

            filters.SetStage(true, 8);
            database.PealsDatabase().GetCountiesWithPealCount(filters, sortedTowerIds);

            Assert::AreEqual((size_t)18, sortedTowerIds.size());
            Assert::AreEqual((size_t)84, sortedTowerIds.find(L"Lancashire")->second.TotalPeals());
            Assert::IsTrue(sortedTowerIds.find(L"Worcestershire") == sortedTowerIds.end());
        }

        TEST_METHOD(PealDatabase_GetTenorKeysWithPealCount_Owen)
        {
            RingingDatabase database;
            unsigned int databaseVersion = 0;
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            database.Internalise(sampleDatabasename.c_str(), NULL, databaseVersion);

            StatisticFilters filters(database);
            std::map<std::wstring, PealLengthInfo> sortedTowerIds;
            database.PealsDatabase().GetTenorKeysWithPealCount(filters, sortedTowerIds);

            Assert::AreEqual((size_t)11, sortedTowerIds.size());
            Assert::AreEqual((size_t)1, sortedTowerIds.find(L"A♭")->second.TotalPeals());
            Assert::AreEqual((size_t)2, sortedTowerIds.find(L"C")->second.TotalPeals());

            filters.SetAssociation(true, 1);
            database.PealsDatabase().GetTenorKeysWithPealCount(filters, sortedTowerIds);

            Assert::AreEqual((size_t)11, sortedTowerIds.size());
            Assert::AreEqual((size_t)1, sortedTowerIds.find(L"D")->second.TotalPeals());
            Assert::IsTrue(sortedTowerIds.find(L"H") == sortedTowerIds.end());
        }

        TEST_METHOD(PealDatabase_NumberOfPealsOnRing_Owen)
        {
            RingingDatabase database;
            unsigned int databaseVersion = 0;
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            database.Internalise(sampleDatabasename.c_str(), NULL, databaseVersion);

            Duco::StatisticFilters filters(database);
            filters.SetTower(true, 164);

            Assert::AreEqual((size_t)53, database.PealsDatabase().PerformanceInfo(filters).TotalPeals());
            filters.SetRing(true, 0);
            Assert::AreEqual((size_t)1, database.PealsDatabase().PerformanceInfo(filters).TotalPeals());
            filters.SetRing(true, 1);
            Assert::AreEqual((size_t)2, database.PealsDatabase().PerformanceInfo(filters).TotalPeals());
            filters.SetRing(true, 2);
            Assert::AreEqual((size_t)5, database.PealsDatabase().PerformanceInfo(filters).TotalPeals());
            filters.SetRing(true, 3);
            Assert::AreEqual((size_t)45, database.PealsDatabase().PerformanceInfo(filters).TotalPeals());
        }

        TEST_METHOD(PealDatabase_NumberOfBellsRungInTower_Owen)
        {
            RingingDatabase database;
            unsigned int databaseVersion = 0;
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            database.Internalise(sampleDatabasename.c_str(), NULL, databaseVersion);

            Assert::AreEqual(10u, database.PealsDatabase().NumberOfBellsRungInTower(164));
            Assert::AreEqual(10u, database.PealsDatabase().NumberOfBellsRungInTower(161));
        }

        TEST_METHOD(PealDatabase_GetTowersAndPealCount_ForCambridgeMajor)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)36, databaseVersionNumber);

            std::multimap<PealLengthInfo, Duco::ObjectId> sortedTowerIds;
            Duco::StatisticFilters filters(database);
            filters.SetMethod(true, 63);

            database.PealsDatabase().GetTowersAndPealCount(filters, false, sortedTowerIds);
            Assert::AreEqual((size_t)2, sortedTowerIds.size());

            /*std::map<Duco::ObjectId, PealLengthInfo>::iterator towersWithPeals = sortedTowerIds.find(ObjectId(70));
            Assert::AreEqual((size_t)1, towersWithPeals->second.TotalPeals());
            towersWithPeals = sortedTowerIds.find(ObjectId(199));
            Assert::AreEqual((size_t)1, towersWithPeals->second.TotalPeals());*/
        }

        TEST_METHOD(PealDatabase_GetTowersAndPealCount_ForNorthamptonAllSaints)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)36, databaseVersionNumber);

            std::multimap<PealLengthInfo, Duco::ObjectId> sortedTowerIds;
            Duco::StatisticFilters filters(database);
            filters.SetTower(true, 200);
            filters.SetIncludeLinkedTowers(true);

            database.PealsDatabase().GetTowersAndPealCount(filters, false, sortedTowerIds);
            Assert::AreEqual((size_t)2, sortedTowerIds.size());

            /*std::map<Duco::ObjectId, PealLengthInfo>::iterator towersWithPeals = sortedTowerIds.find(ObjectId(200));
            Assert::AreEqual((size_t)3, towersWithPeals->second.TotalPeals());

            database.PealsDatabase().GetTowersAndPealCount(sortedTowerIds, filters, true);
            Assert::AreEqual((size_t)1, sortedTowerIds.size());
            towersWithPeals = sortedTowerIds.find(ObjectId(200));
            Assert::AreEqual((size_t)5, towersWithPeals->second.TotalPeals());*/
        }

        TEST_METHOD(PealDatabase_RebuildRecommended_PealsInOfDateOrder)
        {
            RingingDatabase database;
            Assert::IsFalse(database.RebuildRecommended());

            PerformanceDate pealOneDate(1999, 1, 31);
            Peal pealOne;
            pealOne.SetDate(pealOneDate);
            database.PealsDatabase().AddObject(pealOne);
            Assert::IsFalse(database.RebuildRecommended());

            PerformanceDate pealTwoDate(2000, 1, 31);
            Peal pealTwo;
            pealTwo.SetDate(pealTwoDate);
            database.PealsDatabase().AddObject(pealTwo);

            Assert::IsFalse(database.RebuildRecommended());
        }

        TEST_METHOD(PealDatabase_RebuildRecommended_PealsOutOfDateOrder)
        {
            RingingDatabase database;
            Assert::IsFalse(database.RebuildRecommended());

            PerformanceDate pealOneDate(2000, 1, 31);
            Peal pealOne;
            pealOne.SetDate(pealOneDate);
            database.PealsDatabase().AddObject(pealOne);
            Assert::IsFalse(database.RebuildRecommended());

            PerformanceDate pealTwoDate(1999, 1, 31);
            Peal pealTwo;
            pealTwo.SetDate(pealTwoDate);
            database.PealsDatabase().AddObject(pealTwo);

            Assert::IsTrue(database.RebuildRecommended());
        }

        TEST_METHOD(PealDatabase_RebuildRecommended_PealsNeedReindexing)
        {
            RingingDatabase database;

            Peal pealOne;
            database.PealsDatabase().AddObject(pealOne);

            Peal pealTwo;
            Duco::ObjectId pealToDelete = database.PealsDatabase().AddObject(pealTwo);

            Peal pealThree;
            database.PealsDatabase().AddObject(pealThree);
            Assert::IsFalse(database.RebuildRecommended());

            database.PealsDatabase().DeleteObject(pealToDelete);

            Assert::IsTrue(database.RebuildRecommended());
        }

        TEST_METHOD(PealDatabase_RemoveDuplicateRings)
        {
            RingingDatabase database;

            Tower towerOne;
            towerOne.SetNoOfBells(8);
            Duco::ObjectId ringOne = towerOne.AddRing(8, L"1-2-3", L"G");
            Duco::ObjectId ringTwo = towerOne.AddRing(6, L"1-2-5", L"F");
            Duco::ObjectId ringThree = towerOne.AddRing(8, L"1-2-3", L"G");
            Duco::ObjectId ringFour = towerOne.AddRing(8, L"1-2-5", L"G");
            Duco::ObjectId towerId = database.TowersDatabase().AddObject(towerOne);

            Peal pealOne;
            pealOne.SetTowerId(towerId);
            pealOne.SetRingId(ringThree);
            Duco::ObjectId pealOneId = database.PealsDatabase().AddObject(pealOne);

            Peal pealTwo;
            pealTwo.SetTowerId(towerId);
            pealTwo.SetRingId(ringTwo);
            Duco::ObjectId pealTwoId = database.PealsDatabase().AddObject(pealTwo);

            Assert::IsTrue(database.PealsDatabase().RemoveDuplicateRings(NULL));

            const Tower* const updatedTower = database.TowersDatabase().FindTower(towerId);
            Assert::AreEqual((size_t)3, updatedTower->NoOfRings());

            const Peal* const updatedPealOne = database.PealsDatabase().FindPeal(pealOneId);
            Assert::AreEqual(L"1-2-3 in G", updatedTower->TenorDescription(updatedPealOne->RingId()).c_str());

            const Peal* const updatedPealTwo = database.PealsDatabase().FindPeal(pealTwoId);
            Assert::AreEqual(L"1-2-5 in F", updatedTower->TenorDescription(updatedPealTwo->RingId()).c_str());

        }

        TEST_METHOD(PealDatabase_RemoveDuplicateMethods)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Method methodOne (KNoId, 8, L"Bristol", L"Surprise", L"");
            Duco::ObjectId methodOneId = database.MethodsDatabase().AddObject(methodOne);

            Method methodThree(KNoId, 10, L"Cambridge", L"Surprise", L"");;
            Duco::ObjectId methodThreeId = database.MethodsDatabase().AddObject(methodThree);

            Method methodTwo(KNoId, 8, L"Bristol", L"Surprise", L"");
            Duco::ObjectId methodTwoId = database.MethodsDatabase().AddObject(methodTwo);

            Peal pealOne;
            pealOne.SetMethodId(methodThreeId);
            Duco::ObjectId pealOneId = database.PealsDatabase().AddObject(pealOne);

            Peal pealTwo;
            pealTwo.SetMethodId(methodTwoId);
            Duco::ObjectId pealTwoId = database.PealsDatabase().AddObject(pealTwo);

            Peal pealThree;
            pealThree.SetMethodId(methodOneId);
            Duco::ObjectId pealThreeId = database.PealsDatabase().AddObject(pealThree);

            Assert::IsTrue(database.PealsDatabase().RemoveDuplicateMethods(NULL));

            const Peal* const updatedPealOne = database.PealsDatabase().FindPeal(pealOneId);
            Assert::AreEqual(L"Cambridge Surprise Royal", updatedPealOne->MethodName(database).c_str());

            const Peal* const updatedPealTwo = database.PealsDatabase().FindPeal(pealTwoId);
            Assert::AreEqual(L"Bristol Surprise Major", updatedPealTwo->MethodName(database).c_str());

            const Peal* const updatedPealThree = database.PealsDatabase().FindPeal(pealThreeId);
            Assert::AreEqual(L"Bristol Surprise Major", updatedPealThree->MethodName(database).c_str());
        }

        TEST_METHOD(PealDatabase_TowerUsed)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);
            Tower towerOne;
            Duco::ObjectId towerIdOne = database.TowersDatabase().AddObject(towerOne);
            Tower towerTwo;
            Duco::ObjectId unusedTowerIdTwo = database.TowersDatabase().AddObject(towerTwo);
            Tower towerThree;
            Duco::ObjectId towerIdThree = database.TowersDatabase().AddObject(towerThree);

            Peal pealOne;
            database.PealsDatabase().AddObject(pealOne);
            Peal pealTwo;
            pealTwo.SetTowerId(towerIdOne);
            database.PealsDatabase().AddObject(pealTwo);
            Peal pealthree;
            pealthree.SetTowerId(towerIdThree);
            database.PealsDatabase().AddObject(pealthree);
            Peal pealFour;
            database.PealsDatabase().AddObject(pealFour);

            StatisticFilters filters(database);
            filters.SetTower(true, towerIdOne);
            Assert::IsTrue(database.PealsDatabase().AnyMatch(filters));
            filters.SetTower(true, unusedTowerIdTwo);
            Assert::IsFalse(database.PealsDatabase().AnyMatch(filters));
            filters.SetTower(true, towerIdThree);
            Assert::IsTrue(database.PealsDatabase().AnyMatch(filters));
        }

        TEST_METHOD(PealDatabase_CheckNumberOfPealsOnDays)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)36, databaseVersionNumber);

            Duco::StatisticFilters filters(database);
            Assert::AreEqual((size_t)1, database.PealsDatabase().OnThisDay(Duco::PerformanceDate(2000, 1, 1)).size());
            Assert::AreEqual((size_t)0, database.PealsDatabase().OnThisDay(Duco::PerformanceDate(2000, 2, 1)).size());
            Assert::AreEqual((size_t)4, database.PealsDatabase().OnThisDay(Duco::PerformanceDate(2000, 11, 30)).size());
        }

        TEST_METHOD(PealDatabase_ReferenceStats)
        {
            RingingDatabase database;

            Duco::StatisticFilters filters (database);
            std::map<ObjectId, Duco::ReferenceItem> pealCounts;
            database.PealsDatabase().ReferenceStats(filters, pealCounts);

            Assert::AreEqual((size_t)0, pealCounts.size());

            Peal pealOne;
            pealOne.SetTowerId(1);
            pealOne.SetRingingWorldReference(L"1111.2222");
            database.PealsDatabase().AddObject(pealOne);
            Peal pealTwo;
            pealTwo.SetTowerId(1);
            database.PealsDatabase().AddObject(pealTwo);
            Peal pealThree;
            pealThree.SetTowerId(2);
            pealThree.SetRingingWorldReference(L"1111.2222");
            pealThree.SetBellBoardId(L"23232323");
            database.PealsDatabase().AddObject(pealThree);
            Peal pealFour;
            pealFour.SetTowerId(2);
            pealFour.SetBellBoardId(L"34343434");
            database.PealsDatabase().AddObject(pealFour);
            Peal pealFive;
            pealFive.SetTowerId(2);
            database.PealsDatabase().AddObject(pealFive);


            database.PealsDatabase().ReferenceStats(filters, pealCounts);
            Assert::AreEqual((size_t)2, pealCounts.size());
            Assert::AreEqual((size_t)2, pealCounts.find(1)->second.NoOfPeals());
            Assert::AreEqual(0.0f, pealCounts.find(1)->second.PercentWithBellboardReference());
            Assert::AreEqual(50.0f, pealCounts.find(1)->second.PercentWithRingingWorldReference());
            Assert::IsTrue(pealCounts.find(1)->second.Valid());
            Assert::AreEqual((size_t)3, pealCounts.find(2)->second.NoOfPeals());
            Assert::AreEqual(66.6f, pealCounts.find(2)->second.PercentWithBellboardReference(), 0.1f);
            Assert::AreEqual(33.3f, pealCounts.find(2)->second.PercentWithRingingWorldReference(), 0.1f);
            Assert::IsTrue(pealCounts.find(2)->second.Valid());
        }

        TEST_METHOD(PealDatabase_GetMethodsPealCount_FriendsOfPercy)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)36, databaseVersionNumber);

            Duco::ObjectId associationId = database.AssociationsDatabase().SuggestAssociation(L"FRIENDS OF PERCY SOCIETY");

            std::map<Duco::ObjectId, Duco::PealLengthInfo> sortedMethodIds;
            Duco::StatisticFilters filters(database);
            filters.SetAssociation(true, associationId);

            database.PealsDatabase().GetMethodsPealCount(filters, sortedMethodIds);
            Assert::AreEqual((size_t)3, sortedMethodIds.size());
        }

        TEST_METHOD(PealDatabase_GetMethodsPealCount_MoultonInBristolMax)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)36, databaseVersionNumber);

            Duco::ObjectId towerId = database.TowersDatabase().SuggestTower(L"Moulton, Northamptonshire (SS Peter and Paul)", 12);
            Duco::ObjectId bristolMaxId = database.MethodsDatabase().SuggestMethod(L"Bristol", L"Surprise", 12);

            std::map<Duco::ObjectId, Duco::PealLengthInfo> sortedMethodIds;
            Duco::StatisticFilters filters(database);
            filters.SetTower(true, towerId);
            filters.SetMethod(true, bristolMaxId);

            database.PealsDatabase().GetMethodsPealCount(filters, sortedMethodIds);
            Assert::AreEqual((size_t)1, sortedMethodIds.size());
            Assert::AreEqual((size_t)6, sortedMethodIds[bristolMaxId].TotalPeals());
        }

        TEST_METHOD(PealDatabase_GetMethodsPealCount_MoultonOnBackEight)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)36, databaseVersionNumber);

            Duco::ObjectId towerId = database.TowersDatabase().SuggestTower(L"Moulton, Northamptonshire (SS Peter and Paul)", 12);

            std::map<Duco::ObjectId, Duco::PealLengthInfo> sortedMethodIds;
            Duco::StatisticFilters filters(database);
            filters.SetTower(true, towerId);
            filters.SetRing(true, 0);

            database.PealsDatabase().GetMethodsPealCount(filters, sortedMethodIds);
            Assert::AreEqual((size_t)2, sortedMethodIds.size());
        }

        TEST_METHOD(PealDatabase_GetAllMethodTypesByPealCount)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)36, databaseVersionNumber);

            std::map<std::wstring, Duco::PealLengthInfo> sortedPealCounts;
            Duco::StatisticFilters filters(database);

            database.PealsDatabase().GetAllMethodTypesByPealCount(filters, sortedPealCounts);
            Assert::AreEqual((size_t)7, sortedPealCounts.size());

            Assert::AreEqual((size_t)31, sortedPealCounts[L""].TotalPeals());
            Assert::AreEqual((size_t)459, sortedPealCounts[L"Surprise"].TotalPeals());
            Assert::AreEqual((size_t)0, sortedPealCounts[L"Little Surprise"].TotalPeals());
            Assert::AreEqual((size_t)4, sortedPealCounts[L"Little Delight"].TotalPeals());
            Assert::AreEqual((size_t)50, sortedPealCounts[L"Delight"].TotalPeals());
        }

        TEST_METHOD(PealDatabase_GetAllMethodNamesByPealCount)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)36, databaseVersionNumber);

            std::map<std::wstring, Duco::PealLengthInfo> sortedPealCounts;

            database.PealsDatabase().GetAllMethodNamesByPealCount(sortedPealCounts);
            Assert::AreEqual((size_t)207, sortedPealCounts.size());

            Assert::AreEqual((size_t)3, sortedPealCounts[L"Bristol Channel"].TotalPeals());
        }

        TEST_METHOD(PealDatabase_AnyPealContainingBothRingers)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)36, databaseVersionNumber);

            std::set<Duco::ObjectId> nearMatches;
            Duco::ObjectId owenId = database.RingersDatabase().SuggestRinger(L"R Owen Battye", false, nearMatches);
            Duco::ObjectId jennieId = database.RingersDatabase().SuggestRinger(L"Jennie Paul", false, nearMatches);
            Duco::ObjectId matthewId = database.RingersDatabase().SuggestRinger(L"Matthew J L Durham", false, nearMatches);

            Assert::IsTrue(database.PealsDatabase().AnyPealContainingBothRingers(owenId, jennieId, false));
            Assert::IsFalse(database.PealsDatabase().AnyPealContainingBothRingers(owenId, KNoId, true));
            Assert::IsTrue(database.PealsDatabase().AnyPealContainingBothRingers(owenId, matthewId, false));
            Assert::IsFalse(database.PealsDatabase().AnyPealContainingBothRingers(matthewId, jennieId, false));
        }

        TEST_METHOD(PealDatabase_AnyPealContainingBothLinkedRingers)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Duco::ObjectId ringer1Id = database.RingersDatabase().AddRinger(L"One", L"One");
            Duco::ObjectId ringer2Id = database.RingersDatabase().AddRinger(L"Two", L"Two");
            Duco::ObjectId ringer3Id = database.RingersDatabase().AddRinger(L"Two", L"Three");

            Peal pealOne;
            pealOne.SetChanges(5001);
            pealOne.SetRingerId(ringer1Id, 1, false);
            pealOne.SetRingerId(ringer2Id, 2, false);
            database.PealsDatabase().AddObject(pealOne);

            Assert::IsTrue(database.PealsDatabase().AnyPealContainingBothRingers(ringer1Id, ringer2Id, false));
            Assert::IsTrue(database.PealsDatabase().AnyPealContainingBothRingers(ringer1Id, ringer2Id, true));
            Assert::IsFalse(database.PealsDatabase().AnyPealContainingBothRingers(ringer1Id, ringer3Id, true));

            Assert::IsTrue(database.RingersDatabase().LinkRingers(ringer2Id, ringer3Id));
            Assert::IsFalse(database.RingersDatabase().LinkRingers(ringer2Id, ringer2Id));
            Assert::IsFalse(database.PealsDatabase().AnyPealContainingBothRingers(ringer1Id, ringer3Id, false));
            Assert::IsTrue(database.PealsDatabase().AnyPealContainingBothRingers(ringer1Id, ringer3Id, true));
        }

        TEST_METHOD(PealDatabase_NoOfPealsInTower)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)36, databaseVersionNumber);

            Duco::ObjectId towerId = database.TowersDatabase().SuggestTower(L"Moulton, Northamptonshire (SS Peter and Paul)", 12);
            Duco::StatisticFilters filters(database);
            filters.SetTower(true, towerId);

            Assert::AreEqual((size_t)55, database.PealsDatabase().PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)37, database.PealsDatabase().PerformanceInfo(filters, 100).TotalPeals());
        }

        TEST_METHOD(PealDatabase_NoOfPealsAsConductor)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            RingingDatabase database;
            Duco::StatisticFilters filters(database);
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)36, databaseVersionNumber);

            std::set<Duco::ObjectId> nearMatches;
            Duco::ObjectId owenId = database.RingersDatabase().SuggestRinger(L"R Owen Battye", false, nearMatches);
            filters.SetConductor(true, owenId);

            Assert::AreEqual((size_t)24, database.PealsDatabase().PerformanceInfo(filters).TotalPeals());

            PerformanceDate endDate(1996, 1, 1);
            filters.SetEndDate(true, endDate);
            Assert::AreEqual((size_t)15, database.PealsDatabase().PerformanceInfo(filters).TotalPeals());

            filters.SetConductor(true, owenId);
            Assert::IsTrue(database.PealsDatabase().AnyMatch(filters));
        }

        TEST_METHOD(PealDatabase_FirstPealInMethod)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            RingingDatabase database;
            Duco::StatisticFilters filters(database);
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)36, databaseVersionNumber);

            Duco::ObjectId objectId = database.MethodsDatabase().SuggestMethod(L"Bristol Surprise Major");
            filters.SetMethod(true, objectId);

            Duco::PealLengthInfo info = database.PealsDatabase().PerformanceInfo(filters);
            Assert::AreEqual(PerformanceDate(1993, 12, 30), info.DateOfFirstPeal());
        }

        TEST_METHOD(PealDatabase_PealsInMethod)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            RingingDatabase database;
            Duco::StatisticFilters filters(database);
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)36, databaseVersionNumber);

            Duco::ObjectId objectId = database.MethodsDatabase().SuggestMethod(L"Bristol Surprise Major");
            filters.SetMethod(true, objectId);

            std::set<Duco::ObjectId> pealIds;

            Duco::PealLengthInfo pealInfo = database.PealsDatabase().AllMatches(filters, pealIds);
            Assert::AreEqual(PerformanceDate(1993, 12, 30), pealInfo.DateOfFirstPeal());
            Assert::AreEqual((size_t)21, pealIds.size());
        }

        TEST_METHOD(PealDatabase_GetDateRange)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            RingingDatabase database;
            Duco::StatisticFilters filters(database);
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)36, databaseVersionNumber);

            Duco::PealLengthInfo pealInfo = database.PealsDatabase().AllMatches(filters);
            Assert::AreEqual(PerformanceDate(1990, 12, 8), pealInfo.DateOfFirstPeal());
            Assert::AreEqual(PerformanceDate(2020, 2, 8), pealInfo.DateOfLastPeal());
            Assert::AreEqual(Duco::ObjectId(1), pealInfo.FirstPealId());
            Assert::AreEqual(Duco::ObjectId(600), pealInfo.LastPealId());
        }

        TEST_METHOD(PealDatabase_GetChangesPerMinute)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            RingingDatabase database;
            Duco::StatisticFilters filters(database);
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)36, databaseVersionNumber);

            Duco::ObjectId towerId = database.TowersDatabase().SuggestTower(L"Moulton, Northampton. (St Peter And Paul)", 12);
            Assert::AreEqual(Duco::ObjectId(190), towerId);
            filters.SetTower(true, towerId);

            Duco::PealLengthInfo pealInfo = database.PealsDatabase().AllMatches(filters);
            Assert::AreEqual((size_t)55, pealInfo.TotalPeals());
            Assert::AreEqual((size_t)278828, pealInfo.TotalChanges());
            Assert::AreEqual((size_t)10232, pealInfo.TotalMinutes());
            Assert::AreEqual(2394u, pealInfo.DaysBetweenFirstAndLastPeal());
            Assert::AreEqual(27.25f, pealInfo.AveragePealSpeed(), 0.01f);
            Assert::AreEqual(2394u, pealInfo.DaysBetweenFirstAndLastPeal());
            Assert::AreEqual(L"6 years and 204 days", pealInfo.DurationBetweenFirstAndLastPealString().c_str());
        }

        TEST_METHOD(PealDatabase_GetNoOfChangesRange)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            RingingDatabase database;
            Duco::StatisticFilters filters(database);
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)36, databaseVersionNumber);

            std::map<unsigned int, Duco::PealLengthInfo> noOfChanges;
            database.PealsDatabase().GetNoOfChangesRange(filters, noOfChanges);
            Assert::AreEqual((size_t)55, noOfChanges.size());
            Assert::AreEqual((size_t)50, noOfChanges[5000].TotalPeals());
            Assert::AreEqual((size_t)250000, noOfChanges[5000].TotalChanges());
            Assert::AreEqual((size_t)3, noOfChanges[5150].TotalPeals());
            Assert::AreEqual((size_t)15450, noOfChanges[5150].TotalChanges());
        }

        TEST_METHOD(PealDatabase_GetAllMonthsByPealCount)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            RingingDatabase database;
            Duco::StatisticFilters filters(database);
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)36, databaseVersionNumber);

            Duco::ObjectId towerId = database.TowersDatabase().SuggestTower(L"Harpole, Northampton. (All Saints)", 6);
            Assert::IsTrue(towerId.ValidId());
            filters.SetTower(true, towerId);

            std::map<unsigned int, Duco::PealLengthInfo> monthCounts;
            database.PealsDatabase().GetAllMonthsByPealCount(filters, 1990, monthCounts);
            Assert::AreEqual((size_t)0, monthCounts.size());

            database.PealsDatabase().GetAllMonthsByPealCount(filters, 1993, monthCounts);
            Assert::AreEqual((size_t)1, monthCounts.size());
            Assert::AreEqual((size_t)1, monthCounts[12].TotalPeals());
        }

        TEST_METHOD(PealDatabase_GetPerformanceInfoForIdList)
        {
            const std::string sampleDatabasename = "john_m_thurman_DBVer_44.duc";
            RingingDatabase database;
            Duco::StatisticFilters filters(database);

            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)44, databaseVersionNumber);

            std::set<ObjectId> performanceIds;
            Assert::AreEqual((size_t)946, database.PealsDatabase().AllMatches(filters, performanceIds).TotalPeals());

            filters.SetIncludeWithdrawn(true);
            Assert::AreEqual((size_t)948, database.PealsDatabase().AllMatches(filters, performanceIds).TotalPeals());

            filters.SetExcludeValid(true);

            Assert::AreEqual((size_t)2, database.PealsDatabase().PerformanceInfo(filters, performanceIds).TotalPeals());
        }

        TEST_METHOD(PealDatabase_GetAssociationsPealCount)
        {
            const std::string sampleDatabasename = "manchester_other_towers.duc";
            RingingDatabase database;
            Duco::StatisticFilters filters(database);

            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)44, databaseVersionNumber);

            std::map<Duco::ObjectId, Duco::PealLengthInfo> associationCounts;
            database.PealsDatabase().GetAssociationsPealCount(filters, associationCounts, false);

            Duco::ObjectId nullAssociationId = database.AssociationsDatabase().SuggestAssociation(L"");
            Duco::ObjectId lacrId = database.AssociationsDatabase().SuggestAssociation(L"LANCASHIRE ASSOCIATION");
            Duco::ObjectId elyId = database.AssociationsDatabase().SuggestAssociation(L"ELY DISCESAN GUILD");
            Duco::ObjectId leciesterId = database.AssociationsDatabase().SuggestAssociation(L"LEICESTER DIOCESAN GUILD");

            Assert::IsTrue(nullAssociationId.ValidId());

            Assert::AreEqual((size_t)56, associationCounts.size());
            Assert::IsTrue(associationCounts.find(KNoId) == associationCounts.end());
            Assert::IsTrue(associationCounts.find(nullAssociationId) != associationCounts.end());

            Assert::AreEqual((size_t)399, associationCounts[lacrId].TotalPeals());
            Assert::AreEqual((size_t)1, associationCounts[elyId].TotalPeals());
            Assert::AreEqual((size_t)9, associationCounts[leciesterId].TotalPeals());
            Assert::AreEqual((size_t)27, associationCounts[nullAssociationId].TotalPeals());
        }

        TEST_METHOD(PealDatabase_GetAssociationsPealCount_Merged)
        {
            const std::string sampleDatabasename = "manchester_other_towers.duc";
            RingingDatabase database;
            Duco::StatisticFilters filters(database);

            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)44, databaseVersionNumber);

            Duco::ObjectId ascyId = database.AssociationsDatabase().SuggestAssociation(L"ANCIENT SOCIETY OF COLLEGE YOUTHS");
            Duco::ObjectId combineId = database.AssociationsDatabase().SuggestAssociation(L"Ancient Society of College Youths and Antient Free and Accepted Masons");
            Duco::ObjectId combine2Id = database.AssociationsDatabase().SuggestAssociation(L"ANCIENT SOCIETY OF COLLEGE YOUTHS AND THE LANCASHIRE ASSOCIATION");
            Duco::ObjectId mugsId = database.AssociationsDatabase().SuggestAssociation(L"MANCHESTER UNIVERSITY GUILD");
            Duco::ObjectId mugs_oldId = database.AssociationsDatabase().SuggestAssociation(L"MANCHESTER UNIVERSITIES' GUILD OF CHANGE RINGERS");
            Duco::ObjectId mugs_old2Id = database.AssociationsDatabase().SuggestAssociation(L"MANCHESTER UNIVERSITY SOCIETY");
            Duco::ObjectId chesterId = database.AssociationsDatabase().SuggestAssociation(L"CHESTER DIOCESAN GUILD");
            Duco::ObjectId yorkshireId = database.AssociationsDatabase().SuggestAssociation(L"YORKSHIRE ASSOCIATION");
            Duco::ObjectId chester_and_yorkshireId = database.AssociationsDatabase().SuggestAssociation(L"THE CHESTER DIOCESAN GUILD AND THE LANCASHIRE ASSOCIATION");

            std::map<Duco::ObjectId, Duco::PealLengthInfo> associationCounts;
            database.PealsDatabase().GetAssociationsPealCount(filters, associationCounts, false);

            // Pre linking
            Assert::AreEqual((size_t)43, associationCounts[ascyId].TotalPeals());
            Assert::AreEqual((size_t)1, associationCounts[combineId].TotalPeals());
            Assert::AreEqual((size_t)69, associationCounts[combine2Id].TotalPeals());

            Assert::AreEqual((size_t)167, associationCounts[mugsId].TotalPeals());
            Assert::AreEqual((size_t)11, associationCounts[mugs_oldId].TotalPeals());
            Assert::AreEqual((size_t)2, associationCounts[mugs_old2Id].TotalPeals());

            Assert::AreEqual((size_t)24, associationCounts[chesterId].TotalPeals());
            Assert::AreEqual((size_t)24, associationCounts[yorkshireId].TotalPeals());
            Assert::AreEqual((size_t)1, associationCounts[chester_and_yorkshireId].TotalPeals());

            // Link
            Association ascyLink1 = *database.AssociationsDatabase().FindAssociation(combineId);
            ascyLink1.AddAssociationLink(ascyId);
            Assert::IsTrue(database.AssociationsDatabase().UpdateObject(ascyLink1));
            Association ascyLink2 = *database.AssociationsDatabase().FindAssociation(combine2Id);
            ascyLink2.AddAssociationLink(ascyId);
            Assert::IsTrue(database.AssociationsDatabase().UpdateObject(ascyLink2));
            Association mugsLink1 = *database.AssociationsDatabase().FindAssociation(mugs_oldId);
            mugsLink1.AddAssociationLink(mugsId);
            Assert::IsTrue(database.AssociationsDatabase().UpdateObject(mugsLink1));
            Association mugsLink2 = *database.AssociationsDatabase().FindAssociation(mugs_old2Id);
            mugsLink2.AddAssociationLink(mugsId);
            Assert::IsTrue(database.AssociationsDatabase().UpdateObject(mugsLink2));
            Association chesterAndYorkshire = *database.AssociationsDatabase().FindAssociation(chester_and_yorkshireId);
            chesterAndYorkshire.AddAssociationLink(chesterId);
            chesterAndYorkshire.AddAssociationLink(yorkshireId);
            Assert::IsTrue(database.AssociationsDatabase().UpdateObject(chesterAndYorkshire));

            database.PealsDatabase().GetAssociationsPealCount(filters, associationCounts, true);

            // Post linking
            Assert::IsTrue(associationCounts.contains(ascyId));
            Assert::AreEqual((size_t)113, associationCounts[ascyId].TotalPeals());
            Assert::IsFalse(associationCounts.contains(combineId));
            Assert::IsFalse(associationCounts.contains(combine2Id));

            Assert::AreEqual((size_t)180, associationCounts[mugsId].TotalPeals());
            Assert::IsFalse(associationCounts.contains(mugs_oldId));
            Assert::IsFalse(associationCounts.contains(mugs_old2Id));

            Assert::AreEqual((size_t)25, associationCounts[chesterId].TotalPeals());
            Assert::AreEqual((size_t)25, associationCounts[yorkshireId].TotalPeals());
            Assert::IsFalse(associationCounts.contains(chester_and_yorkshireId));
        }

        TEST_METHOD(PealDatabase_GetMethodSeriesPealCount)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_38.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual(38u, databaseVersionNumber);

            StatisticFilters filters(database);
            std::map<ObjectId, Duco::PealLengthInfo> sortedMethodSeriesIds;

            database.PealsDatabase().GetMethodSeriesPealCount(filters, sortedMethodSeriesIds);

            Assert::AreEqual((size_t)64, sortedMethodSeriesIds.size());

            Assert::AreEqual((size_t)0, sortedMethodSeriesIds[6].TotalPeals());
            Assert::AreEqual((size_t)1, sortedMethodSeriesIds[10].TotalPeals());
        }

        TEST_METHOD(PealDatabase_GetFurthestPeals_Owen)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_43.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual(43u, databaseVersionNumber);

            StatisticFilters filters(database);
            std::map<ObjectId, Duco::PealLengthInfo> sortedMethodSeriesIds;

            Duco::ObjectId mostNortherlyId;
            Duco::ObjectId mostSoutherlyId;
            Duco::ObjectId mostEasterlyId;
            Duco::ObjectId mostWesterlyId;

            Assert::IsTrue(database.PealsDatabase().GetFurthestPeals(filters, mostNortherlyId, mostSoutherlyId, mostEasterlyId, mostWesterlyId));

            Assert::AreEqual(database.PealsDatabase().FindPeal(PerformanceDate(2012, 6, 8)), mostNortherlyId);
            Assert::AreEqual(database.PealsDatabase().FindPeal(PerformanceDate(2010, 1, 23)), mostEasterlyId);
            Assert::AreEqual(database.PealsDatabase().FindPeal(PerformanceDate(2008, 5, 5)), mostSoutherlyId);
            Assert::AreEqual(database.PealsDatabase().FindPeal(PerformanceDate(2005, 6, 12)), mostWesterlyId);
        }

        TEST_METHOD(PealDatabase_FindPealByDate_TwoPealsOnTheSameDay)
        {
            const std::string sampleDatabasename = "john_m_thurman_DBVer_44.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual(44u, databaseVersionNumber);

            Assert::AreEqual(Duco::ObjectId(915), database.PealsDatabase().FindPeal(PerformanceDate(2022, 6, 16)));
        }

        TEST_METHOD(PealDatabase_GetFurthestPeals_John_UKOnly)
        {
            const std::string sampleDatabasename = "john_m_thurman_DBVer_44.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual(44u, databaseVersionNumber);

            StatisticFilters filters(database);
            filters.SetEuropeOnly(true);
            std::map<ObjectId, Duco::PealLengthInfo> sortedMethodSeriesIds;

            Duco::ObjectId mostNortherlyId;
            Duco::ObjectId mostSoutherlyId;
            Duco::ObjectId mostEasterlyId;
            Duco::ObjectId mostWesterlyId;

            Assert::IsTrue(database.PealsDatabase().GetFurthestPeals(filters, mostNortherlyId, mostSoutherlyId, mostEasterlyId, mostWesterlyId));

            Assert::AreEqual(database.PealsDatabase().FindPeal(PerformanceDate(2013, 5, 25)), mostNortherlyId);
            Assert::AreEqual(database.PealsDatabase().FindPeal(PerformanceDate(2022, 6, 16)), mostEasterlyId);
            Assert::AreEqual(database.PealsDatabase().FindPeal(PerformanceDate(2017, 10, 25)), mostSoutherlyId);
            Assert::AreEqual(database.PealsDatabase().FindPeal(PerformanceDate(2017, 12, 7)), mostWesterlyId);
        }

        TEST_METHOD(PealDatabase_RingersAroundDates)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);
            PerformanceDate today;

            Peal pealOne;
            pealOne.SetDate(today);
            pealOne.SetRingerId(1, 1, false);
            pealOne.SetRingerId(2, 2, false);
            pealOne.SetRingerId(3, 3, false);
            pealOne.SetRingerId(4, 4, false);
            pealOne.SetRingerId(5, 5, false);
            pealOne.SetRingerId(6, 6, false);

            Peal pealTwo;
            PerformanceDate dateTwo(2018, 4, 16);
            pealTwo.SetDate(dateTwo);
            pealTwo.SetRingerId(7, 1, false);
            pealTwo.SetRingerId(8, 2, false);
            pealTwo.SetRingerId(9, 3, false);
            pealTwo.SetRingerId(10, 4, false);
            pealTwo.SetRingerId(11, 5, false);
            pealTwo.SetRingerId(12, 6, false);


            database.PealsDatabase().AddObject(pealOne);
            database.PealsDatabase().AddObject(pealTwo);

            Duco::StatisticFilters filters(database);
            PerformanceDate lastMonth;
            lastMonth.RemoveMonths(1);
            filters.SetStartDate(true, lastMonth);

            std::set<Duco::ObjectId> ringerIds;
            
            database.PealsDatabase().RingersAroundDates(filters, ringerIds);

            Assert::AreEqual((size_t)6, ringerIds.size());
            Assert::IsFalse(ringerIds.find(1) == ringerIds.end());
            Assert::IsFalse(ringerIds.find(6) == ringerIds.end());
            Assert::IsTrue(ringerIds.find(7) == ringerIds.end());
            Assert::IsTrue(ringerIds.find(8) == ringerIds.end());
        }

        TEST_METHOD(PealDatabase_GetCurrentRingers_NoDate)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);
            PerformanceDate lastMonth;
            lastMonth.RemoveMonths(1);

            Peal pealOne;
            pealOne.SetDate(lastMonth);
            pealOne.SetRingerId(1, 1, false);
            pealOne.SetRingerId(2, 2, false);
            pealOne.SetRingerId(3, 3, false);
            pealOne.SetRingerId(4, 4, false);
            pealOne.SetRingerId(5, 5, false);
            pealOne.SetRingerId(6, 6, false);

            Peal pealTwo;
            PerformanceDate dateTwo(2018, 4, 16);
            pealTwo.SetDate(dateTwo);
            pealTwo.SetRingerId(7, 1, false);
            pealTwo.SetRingerId(8, 2, false);
            pealTwo.SetRingerId(9, 3, false);
            pealTwo.SetRingerId(10, 4, false);
            pealTwo.SetRingerId(11, 5, false);
            pealTwo.SetRingerId(12, 6, false);

            database.PealsDatabase().AddObject(pealOne);
            database.PealsDatabase().AddObject(pealTwo);

            Duco::StatisticFilters filters(database);
            std::set<Duco::ObjectId> ringerIds;
            database.PealsDatabase().RingersAroundDates(filters, ringerIds);

            Assert::AreEqual((size_t)6, ringerIds.size());
            Assert::IsFalse(ringerIds.find(1) == ringerIds.end());
            Assert::IsFalse(ringerIds.find(6) == ringerIds.end());
            Assert::IsTrue(ringerIds.find(7) == ringerIds.end());
            Assert::IsTrue(ringerIds.find(8) == ringerIds.end());
        }

        TEST_METHOD(PealDatabase_GetAllConductorIds)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Peal pealOne;
            pealOne.SetRingerId(1, 1, false);
            pealOne.SetRingerId(2, 2, false);
            pealOne.SetRingerId(3, 3, false);
            pealOne.SetRingerId(4, 4, false);
            pealOne.SetRingerId(5, 5, false);
            pealOne.SetRingerId(6, 6, false);
            pealOne.SetConductorId(6);

            Peal pealTwo;
            pealTwo.SetRingerId(7, 1, false);
            pealTwo.SetRingerId(8, 2, false);
            pealTwo.SetRingerId(9, 3, false);
            pealTwo.SetRingerId(10, 4, false);
            pealTwo.SetRingerId(11, 5, false);
            pealTwo.SetRingerId(12, 6, false);
            pealTwo.SetConductorId(11);

            Peal pealThree;
            pealThree.SetRingerId(7, 1, false);
            pealThree.SetRingerId(8, 2, false);
            pealThree.SetRingerId(9, 3, false);
            pealThree.SetRingerId(10, 4, false);
            pealThree.SetRingerId(11, 5, false);
            pealThree.SetRingerId(12, 6, false);
            pealThree.SetConductorIdFromBell(5, true, false);

            database.PealsDatabase().AddObject(pealOne);
            database.PealsDatabase().AddObject(pealTwo);
            database.PealsDatabase().AddObject(pealThree);

            std::set<Duco::ObjectId> conductorIds;
            conductorIds.insert(2);

            database.PealsDatabase().GetAllConductorIds(conductorIds);

            Assert::AreEqual((size_t)2, conductorIds.size());
            Assert::IsFalse(conductorIds.find(6) == conductorIds.end());
            Assert::IsFalse(conductorIds.find(11) == conductorIds.end());
            Assert::IsTrue(conductorIds.find(2) == conductorIds.end());
        }

        TEST_METHOD(PealDatabase_ReplaceTower_TowerThatDoesntExist)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Assert::IsFalse(database.ReplaceTower(4, 3));
        }

        TEST_METHOD(PealDatabase_ReplaceTower_WithItself)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Assert::IsFalse(database.ReplaceTower(3, 3));
        }

        TEST_METHOD(PealDatabase_ReplaceTower_TowerThatIsntUsed)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);
            Tower towerOne;
            Duco::ObjectId towerOneId = database.TowersDatabase().AddObject(towerOne);
            Tower towerTwo;
            Duco::ObjectId towerTwoId = database.TowersDatabase().AddObject(towerTwo);

            Peal pealOne;
            pealOne.SetTowerId(towerTwoId);

            database.PealsDatabase().AddObject(pealOne);

            Assert::IsFalse(database.ReplaceTower(towerOneId, towerTwoId));
        }

        TEST_METHOD(PealDatabase_ReplaceTower)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Tower towerOne;
            towerOne.SetName(L"One");
            Duco::ObjectId towerOneId = database.TowersDatabase().AddObject(towerOne);
            Tower towerTwo;
            towerOne.SetName(L"Two");
            Duco::ObjectId towerTwoId = database.TowersDatabase().AddObject(towerTwo);
            Tower towerThree;
            towerOne.SetName(L"Three");
            Duco::ObjectId towerThreeId = database.TowersDatabase().AddObject(towerThree);


            Peal pealOne;
            pealOne.SetTowerId(towerOneId);

            Peal pealTwo;
            pealTwo.SetTowerId(towerTwoId);

            Peal pealThree;
            pealThree.SetTowerId(towerThreeId);

            database.PealsDatabase().AddObject(pealOne);
            database.PealsDatabase().AddObject(pealTwo);
            database.PealsDatabase().AddObject(pealThree);

            std::map<ObjectId, Duco::PealLengthInfo> sortedTowerIds;

            Duco::StatisticFilters filters(database);
            database.PealsDatabase().GetTowersPealCount(filters, sortedTowerIds, false);
            Assert::AreEqual((size_t)3, sortedTowerIds.size());
            Assert::AreEqual((size_t)1, sortedTowerIds.find(towerOneId)->second.TotalPeals());
            Assert::AreEqual((size_t)1, sortedTowerIds.find(towerTwoId)->second.TotalPeals());
            Assert::AreEqual((size_t)1, sortedTowerIds.find(towerThreeId)->second.TotalPeals());

            Assert::IsTrue(database.ReplaceTower(1, 3));

            database.PealsDatabase().GetTowersPealCount(filters, sortedTowerIds, false);
            Assert::AreEqual((size_t)3, sortedTowerIds.size());
            Assert::AreEqual((size_t)0, sortedTowerIds.find(towerOneId)->second.TotalPeals());
            Assert::AreEqual((size_t)1, sortedTowerIds.find(towerTwoId)->second.TotalPeals());
            Assert::AreEqual((size_t)2, sortedTowerIds.find(towerThreeId)->second.TotalPeals());
        }
    };
}
