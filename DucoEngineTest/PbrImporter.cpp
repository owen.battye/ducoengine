#include "CppUnitTest.h"
#include <ImportExportProgressCallback.h>
#include <TowerDatabase.h>
#include <Tower.h>
#include <MethodDatabase.h>
#include <Method.h>
#include <Peal.h>
#include <Ring.h>
#include <PealbasePbrImporter.h>
#include <RingingDatabase.h>
#include <PealDatabase.h>
#include <ProgressCallback.h>
#include <RenumberProgressCallback.h>
#include <Ringer.h>
#include <RingerDatabase.h>
#include <StatisticFilters.h>
#include "ToString.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(PealBasePbrImporter_UnitTests), Duco::ProgressCallback, Duco::RenumberProgressCallback, Duco::ImportExportProgressCallback
    {
        Duco::RingingDatabase* database;
        Duco::PealBasePbrImporter* importer;
        bool expectedError;
        bool completed;
        bool initialised;
        int previousPercent;
        int cancelWhenAbove;

    public:
        void RenumberInitialised(RenumberProgressCallback::TRenumberStage newStage)
        {
        }
        void RenumberStep(size_t objectId, size_t total)
        {
        }
        void RenumberComplete()
        {
        }
        //from ImportExportProgressCallback
        void InitialisingImportExport(bool internalising, unsigned int versionNumber, size_t totalNumberOfObjects)
        {
        }
        void ObjectProcessed(bool internalising, Duco::TObjectType objectType, Duco::ObjectId objectId, int totalPercentage, size_t numberInThisSubDatabase)
        {
        }
        void ImportExportComplete(bool internalising)
        {
        }
        void ImportExportFailed(bool internalising, int errorCode)
        {
        }

        PealBasePbrImporter_UnitTests()
        {
            previousPercent = 0;
            expectedError = false;
            completed = false;
            initialised = false;
            cancelWhenAbove = 101;
            database = new Duco::RingingDatabase();
            importer = new Duco::PealBasePbrImporter(*this, *database);
        }

        ~PealBasePbrImporter_UnitTests()
        {
            delete importer;
            delete database;
        }

        void Initialised()
        {
            initialised = true;
            previousPercent = 0;
        }

        void Step(int progressPercent)
        {
            Assert::IsTrue(initialised);
            Assert::IsTrue(progressPercent < 100);
            Assert::IsTrue(previousPercent <= progressPercent);
            previousPercent = progressPercent;
            if (progressPercent > cancelWhenAbove)
            {
                importer->Cancel();
            }
        }

        void Complete(bool error)
        {
            Assert::AreEqual(expectedError, error);
            Assert::IsTrue(initialised);
            completed = true;
        }

        void FindCheckRinger(const Duco::Peal& thePeal, unsigned int bellNo, bool strapper, const std::wstring& expectedName) const
        {
            Duco::ObjectId ringerId;
            Assert::IsTrue(database->RingersDatabase().FindRinger(thePeal.RingerId(bellNo, strapper, ringerId)));
            const Ringer* const pealRinger = database->RingersDatabase().FindRinger(ringerId);
            Assert::AreEqual(expectedName, pealRinger->FullName(false));
        }

        TEST_METHOD(PealBasePbrImporter_Import_NonExistingFile)
        {
            expectedError = true;
            Assert::IsFalse(importer->Import("wibble.pbr"));
            Assert::IsTrue(completed);
            Assert::AreEqual(0, previousPercent);
        }

        BEGIN_TEST_METHOD_ATTRIBUTE(PealBasePbrImporter_Import_Cancel)
            TEST_PRIORITY(2)
        END_TEST_METHOD_ATTRIBUTE()
        TEST_METHOD(PealBasePbrImporter_Import_Cancel)
        {
            cancelWhenAbove = 25;
            Assert::IsFalse(importer->Import("Andrew G Craddock.pbr"));
            Assert::IsFalse(completed);
            Assert::IsTrue(previousPercent < 100);
            Assert::IsTrue(previousPercent > cancelWhenAbove);
        }

        BEGIN_TEST_METHOD_ATTRIBUTE(PealBasePbrImporter_ImportAndrewCraddock)
            TEST_PRIORITY(2)
        END_TEST_METHOD_ATTRIBUTE()
        TEST_METHOD(PealBasePbrImporter_ImportAndrewCraddock)
        {
            Duco::StatisticFilters filters(*database);

            Assert::IsTrue(importer->Import("Andrew G Craddock.pbr"));
            Assert::IsTrue(completed);
            Assert::IsTrue(previousPercent >= 99);
            Assert::AreEqual((size_t)302, database->PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)12, database->NumberOfAssociations());
            filters.SetIncludeHandBell(false);
            Assert::AreEqual((size_t)154, database->PealsDatabase().PerformanceInfo(filters).TotalPeals());
            Assert::IsTrue(database->PealsDatabase().AnyMatch(filters));
            filters.SetIncludeHandBell(true);
            filters.SetIncludeTowerBell(false);
            Assert::AreEqual((size_t)148, database->PealsDatabase().PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)57, database->NumberOfMethods()); // counted this manually
            Assert::AreEqual((size_t)367, database->NumberOfRingers());
            Assert::IsTrue(database->PealsDatabase().AnyMatch(filters));
            //Assert::AreEqual((size_t)122, database->NumberOfTowers());

            CheckPealOn13April1965(*database);
            CheckPealOn10Sept1965(*database);

            database->RebuildDatabase(this);

            CheckPealOn10Sept1965(*database);
            CheckPealOn13April1965(*database);

            filters.SetIncludeHandBell(false);
            filters.SetIncludeTowerBell(false);
            Assert::IsFalse(database->PealsDatabase().AnyMatch(filters));

            //database->ExternaliseToXml("Andrew G Craddock.xml", this, "Andrew G Craddock.pbr", false, true);
        }

        void CheckPealOn13April1965(const RingingDatabase& database)
        {
            PerformanceDate peal18Date(1965, 4, 13);
            ObjectId peal18Id = database.PealsDatabase().FindPeal(peal18Date);
            const Peal* const peal18 = database.PealsDatabase().FindPeal(peal18Id);
            Assert::IsNotNull(peal18);
            Assert::AreEqual(L"Durham & Newcastle Diocesan Association", peal18->AssociationName(database).c_str());
            Assert::IsTrue(peal18->Handbell());
            const Tower* const peal18Tower = database.TowersDatabase().FindTower(peal18->TowerId());
            Assert::AreEqual(L"Newcastle Upon Tyne", peal18Tower->Name().c_str());
            Assert::AreEqual(L"Newcastle Upon Tyne", peal18Tower->City().c_str());
            Assert::AreEqual(L"22 Castleton Grove", peal18Tower->County().c_str());
            Assert::AreEqual(PerformanceTime(123), peal18->Time());
            Assert::AreEqual((unsigned int)5040, peal18->NoOfChanges());
            const Method* const peal18Method = database.MethodsDatabase().FindMethod(peal18->MethodId());
            Assert::AreEqual(L"Plain Bob Minor", peal18Method->FullName(database.MethodsDatabase()).c_str());
            Assert::AreEqual(L"", peal18->Composer().c_str());

            FindCheckRinger(*peal18, 1, false, L"Roger H E Askew");
            FindCheckRinger(*peal18, 2, false, L"Rodney A Yeates");
            FindCheckRinger(*peal18, 3, false, L"Andrew G Craddock");
            Assert::AreEqual(L"First peal: 1-2.", peal18->Footnotes().c_str());
            Assert::AreEqual(L"Craddock, Andrew G", peal18->ConductorName(database).c_str());
        }

        void CheckPealOn10Sept1965(const RingingDatabase& database)
        {
            PerformanceDate peal34Date(1965, 9, 10);
            ObjectId peal34Id = database.PealsDatabase().FindPeal(peal34Date);
            const Peal* const peal34 = database.PealsDatabase().FindPeal(peal34Id);
            Assert::IsNotNull(peal34);
            Assert::AreEqual(L"Ely Diocesan Association", peal34->AssociationName(database).c_str());
            Assert::IsFalse(peal34->Handbell());
            const Tower* const peal34Tower = database.TowersDatabase().FindTower(peal34->TowerId());
            Assert::AreEqual(L"S Mary the Virgin", peal34Tower->Name().c_str());
            Assert::AreEqual(L"Higham Ferrers", peal34Tower->City().c_str());
            Assert::AreEqual(L"Northamptonshire", peal34Tower->County().c_str());
            Assert::AreEqual(PerformanceTime(181), peal34->Time());
            Assert::AreEqual((unsigned int)5056, peal34->NoOfChanges());
            const Method* const peal34Method = database.MethodsDatabase().FindMethod(peal34->MethodId());
            Assert::AreEqual(L"Plain Bob Major", peal34Method->FullName(database.MethodsDatabase()).c_str());
            Assert::AreEqual(L"John R Pritchard", peal34->Composer().c_str());

            FindCheckRinger(*peal34, 1, false, L"Mary E Elmes");
            FindCheckRinger(*peal34, 2, false, L"Peter Woollven");
            FindCheckRinger(*peal34, 3, false, L"George E Bonham");
            FindCheckRinger(*peal34, 4, false, L"Andrew G Craddock");
            FindCheckRinger(*peal34, 5, false, L"Graham W Elmes");
            FindCheckRinger(*peal34, 6, false, L"Anthony D Sansom");
            FindCheckRinger(*peal34, 7, false, L"C Barrie Dove");
            FindCheckRinger(*peal34, 8, false, L"Philip Mehew");
            Assert::AreEqual(L"", peal34->Footnotes().c_str());
            Assert::AreEqual(L"Mehew, Philip", peal34->ConductorName(database).c_str());
        }

        BEGIN_TEST_METHOD_ATTRIBUTE(PealBasePbrImporter_ImportMatthewJHilling)
            TEST_PRIORITY(2)
        END_TEST_METHOD_ATTRIBUTE()
        TEST_METHOD(PealBasePbrImporter_ImportMatthewJHilling)
        {
            Duco::StatisticFilters filters(*database);
            Assert::IsTrue(importer->Import("Matthew J Hilling.pbr"));
            Assert::IsTrue(completed);
            Assert::IsTrue(previousPercent >= 99);
            Assert::AreEqual((size_t)1060, database->PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)28, database->NumberOfAssociations());

            filters.SetIncludeHandBell(false);
            Assert::AreEqual((size_t)946, database->PealsDatabase().PerformanceInfo(filters).TotalPeals());

            filters.SetIncludeHandBell(true);
            filters.SetIncludeTowerBell(false);
            Assert::AreEqual((size_t)114, database->PealsDatabase().PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)255, database->NumberOfMethods()); // counted this manually
            Assert::AreEqual((size_t)724, database->NumberOfRingers());
            Assert::AreEqual((size_t)413, database->NumberOfTowers());

            CheckPeal37(*database);
            CheckPeal155(*database);

            database->RebuildDatabase(this);

            CheckPeal37(*database);
            CheckPeal155(*database);

            //database->ExternaliseToXml("Matthew J Hilling.xml", this, "Matthew J Hilling.pbr", true, true);
            //database->Externalise("Matthew J Hilling.duc", this);
        }

        void CheckPeal37(const Duco::RingingDatabase& database)
        {
            PerformanceDate peal37Date(1993, 4, 12);
            ObjectId peal37Id = database.PealsDatabase().FindPeal(peal37Date);
            Assert::AreEqual((size_t)37, peal37Id.Id());
            const Peal* const peal37 = database.PealsDatabase().FindPeal(peal37Id);
            Assert::IsNotNull(peal37);
            Assert::AreEqual(L"Ancient Society Of College Youths", peal37->AssociationName(database).c_str());
            Assert::IsFalse(peal37->Handbell());
            Assert::AreEqual(PerformanceTime(235), peal37->Time());
            Assert::AreEqual((unsigned int)5021, peal37->NoOfChanges());
            FindCheckRinger(*peal37, 1, false, L"Jeremy R Pratt");
            FindCheckRinger(*peal37, 2, false, L"Michael P Moreton");
            FindCheckRinger(*peal37, 3, false, L"Matthew J Hilling");
            FindCheckRinger(*peal37, 12, false, L"Robert C Kippin");
            FindCheckRinger(*peal37, 12, true, L"A James Phillips");
            Assert::AreEqual(L"For the Festival of Easter.", peal37->Footnotes().c_str());
            Assert::AreEqual(L"Townsend, Peter J", peal37->ConductorName(database).c_str());

            const Tower* const peal37Tower = database.TowersDatabase().FindTower(peal37->TowerId());
            Assert::AreEqual(L"St Paul", peal37Tower->Name().c_str());
            Assert::AreEqual(L"London", peal37Tower->City().c_str());
            Assert::AreEqual(L"Middlesex", peal37Tower->County().c_str());

            const Ring* const peal37Ring = peal37Tower->FindRing(peal37->RingId());
            Assert::IsNotNull(peal37Ring);
            Assert::AreEqual((unsigned int)12, peal37Ring->NoOfBells());
            Assert::AreEqual(L"61 cwt", peal37Ring->TenorWeight().c_str());
            Assert::AreEqual(L"12 bell peal", peal37Ring->Name().c_str());

            const Method* const peal37Method = database.MethodsDatabase().FindMethod(peal37->MethodId());
            Assert::AreEqual(L"Stedman Cinques", peal37Method->FullName(database.MethodsDatabase()).c_str());
            Assert::AreEqual(L"Paul N Mounsey", peal37->Composer().c_str());
        }

        void CheckPeal155(const Duco::RingingDatabase& database)
        {
            PerformanceDate pealDate(1996, 11, 30);
            ObjectId pealId = database.PealsDatabase().FindPeal(pealDate);

            const Peal* const peal = database.PealsDatabase().FindPeal(pealId);
            Assert::IsNotNull(peal);
            Assert::AreEqual(L"Guild Of Devonshire Ringers", peal->AssociationName(database).c_str());
            Assert::IsFalse(peal->Handbell());
            Assert::AreEqual(PerformanceTime(247), peal->Time());
            Assert::AreEqual((unsigned int)5007, peal->NoOfChanges());
            FindCheckRinger(*peal, 1, false, L"Ian L C Campbell");
            FindCheckRinger(*peal, 2, false, L"Howard W Egglestone");
            FindCheckRinger(*peal, 3, false, L"Frank D Mack");
            FindCheckRinger(*peal, 12, false, L"Matthew J Hilling");
            FindCheckRinger(*peal, 12, true, L"David P Macey");
            Assert::AreEqual(L"100th peal: 1.", peal->Footnotes().c_str());
            Assert::AreEqual(L"Mears, Michael E C", peal->ConductorName(database).c_str());

            const Tower* const pealTower = database.TowersDatabase().FindTower(peal->TowerId());
            Assert::AreEqual(L"St Peter", pealTower->Name().c_str());
            Assert::AreEqual(L"Exeter", pealTower->City().c_str());
            Assert::AreEqual(L"Devon", pealTower->County().c_str());
            Assert::AreEqual((size_t)2, pealTower->NoOfRings());

            const Ring* const pealRing = pealTower->FindRing(peal->RingId());
            Assert::IsNotNull(pealRing);
            Assert::AreEqual((unsigned int)12, pealRing->NoOfBells());
            Assert::AreEqual(L"72 cwt", pealRing->TenorWeight().c_str());
            Assert::AreEqual(L"12 bell peal", pealRing->Name().c_str());

            const Method* const pealMethod = database.MethodsDatabase().FindMethod(peal->MethodId());
            Assert::AreEqual(L"Stedman Cinques", pealMethod->FullName(database.MethodsDatabase()).c_str());
            Assert::AreEqual(L"Thomas Hooley", peal->Composer().c_str());
        }

    };
}
