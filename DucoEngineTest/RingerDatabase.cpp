#include "CppUnitTest.h"
#include <Ringer.h>
#include <RingerDatabase.h>
#include <Peal.h>
#include <PealDatabase.h>
#include <RingingDatabase.h>
#include "ToString.h"
#include <ImportExportProgressCallback.h>
#include <StatisticFilters.h>
#include <DatabaseSettings.h>
#include <DatabaseSearch.h>
#include <SearchDuplicateObject.h>
#include <ProgressCallback.h>
#include <SearchFieldStringArgument.h>
#include <SearchValidObject.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(RingerDatabase_UnitTests), Duco::ImportExportProgressCallback, Duco::ProgressCallback
    {
    public:
        //From ImportExportProgressCallback
        void InitialisingImportExport(bool internalising, unsigned int versionNumber, size_t totalNumberOfObjects)
        {

        }
        void ObjectProcessed(bool internalising, Duco::TObjectType objectType, Duco::ObjectId objectId, int totalPercentage, size_t numberInThisSubDatabase)
        {

        }
        void ImportExportComplete(bool internalising)
        {

        }
        void ImportExportFailed(bool internalising, int errorCode)
        {
            Assert::IsFalse(true);
        }

        // ProgressCallback
        void Initialised()
        {
        }

        void Step(int progressPercent)
        {
        }

        void Complete(bool error)
        {
        }

        TEST_METHOD(RingerDatabase_SuggestRinger_SameSurnames)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Ringer ringerOne(KNoId, L"Owen", L"Battye");
            Duco::ObjectId ringerOneId = database.RingersDatabase().AddObject(ringerOne);
            Ringer ringerTwo(KNoId, L"Sarah", L"Battye");
            Duco::ObjectId ringerTwoId = database.RingersDatabase().AddObject(ringerTwo);

            std::set<Duco::ObjectId> nearMatches;
            Assert::AreEqual(ringerOneId, database.RingersDatabase().SuggestRinger(L"Owen Battye", false, nearMatches));
            Assert::AreEqual(ringerTwoId, database.RingersDatabase().SuggestRinger(L"Battye, Sarah", false, nearMatches));
            Assert::IsFalse(database.RingersDatabase().SuggestRinger(L"Battye", false, nearMatches).ValidId());
        }

        TEST_METHOD(RingerDatabase_SuggestRinger_WithBrckets)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Ringer ringerOne(KNoId, L"Owen", L"Battye");
            Duco::ObjectId ringerOneId = database.RingersDatabase().AddObject(ringerOne);
            Ringer ringerTwo(KNoId, L"Sarah", L"Battye");
            Duco::ObjectId ringerTwoId = database.RingersDatabase().AddObject(ringerTwo);

            std::set<Duco::ObjectId> nearMatches;
            Assert::IsFalse(database.RingersDatabase().SuggestRinger(L"Owen Battye (Moulton)", true, nearMatches).ValidId());
            Assert::AreEqual(ringerOneId, *nearMatches.begin());
        }

        TEST_METHOD(RingerDatabase_SuggestRinger_WithoutInitials)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Ringer ringerOne(KNoId, L"Alyson E", L"Blanchard");
            Duco::ObjectId ringerOneId = database.RingersDatabase().AddObject(ringerOne);

            std::set<Duco::ObjectId> nearMatches;
            Assert::IsFalse(database.RingersDatabase().SuggestRinger(L"Alyson Blanchard", true, nearMatches).ValidId());
            Assert::AreEqual(ringerOneId, *nearMatches.begin());
            Assert::AreEqual(ringerOneId, database.RingersDatabase().SuggestRinger(L"Blanchard, Alyson E", true, nearMatches));
        }

        TEST_METHOD(RingerDatabase_SuggestRinger_InitialsWithoutSpaces)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Ringer ringerOne(KNoId, L"Graham JN", L"Colborne");
            Duco::ObjectId ringerOneId = database.RingersDatabase().AddObject(ringerOne);
            Duco::ObjectId noRinger;

            std::set<Duco::ObjectId> nearMatches;
            Assert::AreEqual(ringerOneId, database.RingersDatabase().SuggestRinger(L"Graham JN Colborne", true, nearMatches));
            Assert::IsFalse(database.RingersDatabase().SuggestRinger(L"Graham J N Colborne", true, nearMatches).ValidId());
            Assert::AreEqual(ringerOneId, *nearMatches.begin());
        }

        TEST_METHOD(RingerDatabase_SuggestRinger_InitialsWithSpaces)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Ringer ringerOne(KNoId, L"Graham J N", L"Colborne");
            Duco::ObjectId ringerOneId = database.RingersDatabase().AddObject(ringerOne);

            std::set<Duco::ObjectId> nearMatches;
            Assert::IsFalse(database.RingersDatabase().SuggestRinger(L"Graham JN Colborne", true, nearMatches).ValidId());
            Assert::AreEqual(ringerOneId, *nearMatches.begin());
            Assert::AreEqual(ringerOneId, database.RingersDatabase().SuggestRinger(L"Graham J N Colborne", true, nearMatches));
        }

        TEST_METHOD(RingerDatabase_SuggestRinger_WithInitials)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Ringer ringerOne(KNoId, L"Alyson", L"Blanchard");
            Duco::ObjectId ringerOneId = database.RingersDatabase().AddObject(ringerOne);

            std::set<Duco::ObjectId> nearMatches;
            Assert::AreEqual(ringerOneId, database.RingersDatabase().SuggestRinger(L"Alyson Blanchard", true, nearMatches));
            Assert::IsFalse(database.RingersDatabase().SuggestRinger(L"Blanchard, Alyson E", true, nearMatches).ValidId());
            Assert::AreEqual(ringerOneId, *nearMatches.begin());
        }

        TEST_METHOD(RingerDatabase_SuggestRinger_SameSurnames_InitialsOnly)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);
            database.Settings().SetLastNameFirst(false);
            Duco::ObjectId noRinger;

            Ringer ringerOne(KNoId, L"R Owen", L"Battye");
            Duco::ObjectId ringerOneId = database.RingersDatabase().AddObject(ringerOne);
            Ringer ringerTwo(KNoId, L"Sarah", L"Battye");
            Duco::ObjectId ringerTwoId = database.RingersDatabase().AddObject(ringerTwo);
            Ringer ringerThree(KNoId, L"Robert", L"Smith");
            Duco::ObjectId ringerThreeId = database.RingersDatabase().AddObject(ringerTwo);

            std::set<Duco::ObjectId> nearMatches;
            Assert::IsFalse(database.RingersDatabase().SuggestRinger(L"Robert Owen Battye", true, nearMatches).ValidId());
            Assert::AreEqual(ringerOneId, *nearMatches.begin());
            Assert::IsFalse(database.RingersDatabase().SuggestRinger(L"Robert O Battye", true, nearMatches).ValidId());
            Assert::AreEqual(ringerOneId, *nearMatches.begin());
            Assert::IsFalse(database.RingersDatabase().SuggestRinger(L"R O Battye", true, nearMatches).ValidId());
            Assert::AreEqual(ringerOneId, *nearMatches.begin());

            Assert::IsFalse(database.RingersDatabase().SuggestRinger(L"Battye, Robert Owen", true, nearMatches).ValidId());
            Assert::AreEqual(ringerOneId, *nearMatches.begin());
            Assert::IsFalse(database.RingersDatabase().SuggestRinger(L"Battye, Robert O", true, nearMatches).ValidId());
            Assert::AreEqual(ringerOneId, *nearMatches.begin());
            Assert::IsFalse(database.RingersDatabase().SuggestRinger(L"Battye, R O", true, nearMatches).ValidId());
            Assert::AreEqual(ringerOneId, *nearMatches.begin());
            Assert::IsFalse(database.RingersDatabase().SuggestRinger(L"Battye, R", true, nearMatches).ValidId());
            Assert::AreEqual(ringerOneId, *nearMatches.begin());

            Assert::AreEqual(ringerOneId, database.RingersDatabase().SuggestRinger(L"Battye, R Owen", false, nearMatches));

        }

        TEST_METHOD(RingerDatabase_SuggestRinger_SameSurnames_InitialsOnly_LastNameFirst)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);
            database.Settings().SetLastNameFirst(true);
            Duco::ObjectId noRinger;

            Ringer ringerOne(KNoId, L"R Owen", L"Battye");
            Duco::ObjectId ringerOneId = database.RingersDatabase().AddObject(ringerOne);
            Ringer ringerTwo(KNoId, L"Sarah", L"Battye");
            Duco::ObjectId ringerTwoId = database.RingersDatabase().AddObject(ringerTwo);
            Ringer ringerThree(KNoId, L"Robert", L"Smith");
            Duco::ObjectId ringerThreeId = database.RingersDatabase().AddObject(ringerTwo);

            std::set<Duco::ObjectId> nearMatches;
            Assert::IsFalse(database.RingersDatabase().SuggestRinger(L"Robert Owen Battye", true, nearMatches).ValidId());
            Assert::AreEqual(ringerOneId, *nearMatches.begin());
            Assert::IsFalse(database.RingersDatabase().SuggestRinger(L"Robert O Battye", true, nearMatches).ValidId());
            Assert::AreEqual(ringerOneId, *nearMatches.begin());
            Assert::IsFalse(database.RingersDatabase().SuggestRinger(L"R O Battye", true, nearMatches).ValidId());
            Assert::AreEqual(ringerOneId, *nearMatches.begin());

            Assert::IsFalse(database.RingersDatabase().SuggestRinger(L"Battye, Robert Owen", true, nearMatches).ValidId());
            Assert::AreEqual(ringerOneId, *nearMatches.begin());
            Assert::IsFalse(database.RingersDatabase().SuggestRinger(L"Battye, Robert O", true, nearMatches).ValidId());
            Assert::AreEqual(ringerOneId, *nearMatches.begin());
            Assert::IsFalse(database.RingersDatabase().SuggestRinger(L"Battye, R O", true, nearMatches).ValidId());
            Assert::AreEqual(ringerOneId, *nearMatches.begin());
        }


        TEST_METHOD(RingerDatabase_SuggestRinger_Variety)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Ringer ringerOne(KNoId, L"John", L"Thurman");
            Duco::ObjectId ringerOneId = database.RingersDatabase().AddObject(ringerOne);
            Ringer ringerTwo(KNoId, L"Jennie", L"Higson");
            Duco::ObjectId ringerTwoId = database.RingersDatabase().AddObject(ringerTwo);

            std::set<Duco::ObjectId> nearMatches;
            Assert::AreEqual(ringerOneId, database.RingersDatabase().SuggestRinger(L"John Thurman", false, nearMatches));
            Assert::AreEqual(ringerOneId, database.RingersDatabase().SuggestRinger(L"Thurman, John", false, nearMatches));
            Assert::AreEqual(ringerTwoId, database.RingersDatabase().SuggestRinger(L"Jennie Higson", false, nearMatches));
            Assert::AreEqual(Duco::ObjectId(), database.RingersDatabase().SuggestRinger(L"Jennie Paul", false, nearMatches));
            Assert::AreEqual(Duco::ObjectId(), database.RingersDatabase().SuggestRinger(L"Battye", false, nearMatches));
            Assert::AreEqual(Duco::ObjectId(), database.RingersDatabase().SuggestRinger(L"Owen Battye", false, nearMatches));

            Ringer ringerThree(KNoId, L"Jennie", L"Smith");
            Duco::ObjectId ringerThreeId = database.RingersDatabase().AddObject(ringerThree);
            Assert::AreEqual(Duco::ObjectId(), database.RingersDatabase().SuggestRinger(L"Jennie Paul", false, nearMatches));
            Assert::AreEqual(Duco::ObjectId(), database.RingersDatabase().SuggestRinger(L"Smith", false, nearMatches));
            Assert::AreEqual(Duco::ObjectId(), database.RingersDatabase().SuggestRinger(L"Jennie", false, nearMatches));
            Assert::AreEqual(ringerTwoId, database.RingersDatabase().SuggestRinger(L"Jennie Higson", false, nearMatches));
        }

        TEST_METHOD(RingerDatabase_SuggestRinger_ChangedName)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Ringer ringerOne(KNoId, L"Jennie", L"Paul");
            ringerOne.InsertNameChange(L"Jennie", L"Higson", PerformanceDate(2010, 5, 20));
            Duco::ObjectId ringerOneId = database.RingersDatabase().AddObject(ringerOne);
            Ringer ringerTwo(KNoId, L"John", L"Thurman");
            Duco::ObjectId ringerTwoId = database.RingersDatabase().AddObject(ringerTwo);
            Ringer ringerThree(KNoId, L"Jennie", L"Smith");
            Duco::ObjectId ringerThreeId = database.RingersDatabase().AddObject(ringerThree);

            std::set<Duco::ObjectId> nearMatches;
            Assert::AreEqual(Duco::ObjectId(), database.RingersDatabase().SuggestRinger(L"owen battye", false, nearMatches));
            Assert::AreEqual(ringerOneId, database.RingersDatabase().SuggestRinger(L"Jennie Higson", false, nearMatches));
            Assert::AreEqual(ringerOneId, database.RingersDatabase().SuggestRinger(L"Paul, jennie", false, nearMatches));
            Assert::AreEqual(ringerOneId, database.RingersDatabase().SuggestRinger(L"Jennie Paul", false, nearMatches));
        }

        TEST_METHOD(RingerDatabase_SuggestRinger_SubStrings)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Ringer ringerOne(KNoId, L"Audrey", L"Ringer");
            Duco::ObjectId ringerOneId = database.RingersDatabase().AddObject(ringerOne);
            Ringer ringerTwo(KNoId, L"A", L"Ringer");
            Duco::ObjectId ringerTwoId = database.RingersDatabase().AddObject(ringerTwo);

            std::set<Duco::ObjectId> nearMatches;
            Assert::AreEqual(ringerOneId, database.RingersDatabase().SuggestRinger(ringerOne.FullName(false), false, nearMatches));
            Assert::AreEqual(ringerTwoId, database.RingersDatabase().SuggestRinger(ringerTwo.FullName(false), false, nearMatches));
        }

        TEST_METHOD(RingerDatabase_SuggestRinger_Aka)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Ringer ringerOne(KNoId, L"Stephen", L"Penny");
            ringerOne.SetAlsoKnownAs(L"Percy");
            Duco::ObjectId ringerOneId = database.RingersDatabase().AddObject(ringerOne);
            Ringer ringerTwo(KNoId, L"James", L"Penny");
            Duco::ObjectId ringerTwoId = database.RingersDatabase().AddObject(ringerTwo);

            std::set<Duco::ObjectId> nearMatches;
            Assert::AreEqual(ringerOneId, database.RingersDatabase().SuggestRinger(L"Stephen Penny", false, nearMatches));
            Assert::IsFalse(database.RingersDatabase().SuggestRinger(L"Percy Penny", false, nearMatches).ValidId());
        }

        TEST_METHOD(RingerDatabase_SetRingerGender)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Assert::AreEqual((size_t)0, database.NumberOfRingers());
            ObjectId ringerId = database.RingersDatabase().AddRinger(L"Owen", L"Battye");
            Assert::AreEqual((size_t)1, database.NumberOfRingers());
            Assert::AreEqual(ObjectId(1), ringerId);

            Assert::AreEqual(L"Battye, Owen", database.RingersDatabase().RingerFullName(ringerId, database.Settings()).c_str());
            Assert::AreEqual(L"", database.RingersDatabase().RingerFullName(25, database.Settings()).c_str());

            const Ringer* const foundRinger = database.RingersDatabase().FindRinger(ringerId);
            Assert::AreEqual(false, foundRinger->Male());
            Assert::AreEqual(false, foundRinger->Female());

            Assert::IsTrue(database.RingersDatabase().SetRingerGender(ringerId, true));

            const Ringer* const foundRingerAgain = database.RingersDatabase().FindRinger(ringerId);
            Assert::AreEqual(true, foundRingerAgain->Male());
            Assert::AreEqual(false, foundRingerAgain->Female());
        }

        TEST_METHOD(RingerDatabase_RingerGenderStats)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Assert::AreEqual((size_t)0, database.NumberOfRingers());
            ObjectId ringerIdOne = database.RingersDatabase().AddRinger(L"Owen", L"Battye");
            ObjectId ringerIdTwo = database.RingersDatabase().AddRinger(L"Isla", L"Battye");
            ObjectId ringerIdThree = database.RingersDatabase().AddRinger(L"George", L"Campling");
            ObjectId ringerIdFour = database.RingersDatabase().AddRinger(L"Simon", L"Woof");
            Assert::AreEqual((size_t)4, database.NumberOfRingers());

            Assert::IsTrue(database.RingersDatabase().SetRingerGender(ringerIdOne, true));
            Assert::IsTrue(database.RingersDatabase().SetRingerGender(ringerIdTwo, false));
            Assert::IsTrue(database.RingersDatabase().SetRingerGender(ringerIdThree, true));

            float percentMale, percentFemale, percentNotSet, percentNonHuman;
            database.RingersDatabase().RingerGenderStats(percentMale, percentFemale, percentNotSet, percentNonHuman);
            Assert::AreEqual(50.0f, percentMale);
            Assert::AreEqual(25.0f, percentFemale);
            Assert::AreEqual(25.0f, percentNotSet);
            Assert::AreEqual(0.0f, percentNonHuman);
        }

        TEST_METHOD(RingerDatabase_RingerGenderStats_WithOneNonHuman)
        {
            const std::string sampleDatabasename = "non_human_ringers.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)44, databaseVersionNumber);

            float percentMale, percentFemale, percentNotSet, percentNonHuman;
            database.RingersDatabase().RingerGenderStats(percentMale, percentFemale, percentNotSet, percentNonHuman);
            Assert::AreEqual(33.3f, percentMale, 0.1f);
            Assert::AreEqual(16.7f, percentFemale, 0.1f);
            Assert::AreEqual(33.3f, percentNotSet, 0.1f);
            Assert::AreEqual(16.7f, percentNonHuman, 0.1f);

            StatisticFilters filters(database);

            float percentMaleConductors, percentFemaleConductors, percentNotSetConductors, percentNonHumanConductors;
            database.PealsDatabase().RingerGenderStats(filters, percentMale, percentFemale, percentNotSet, percentNonHuman, percentMaleConductors, percentFemaleConductors, percentNotSetConductors, percentNonHumanConductors);

            Assert::AreEqual(33.3f, percentMale, 0.1f);
            Assert::AreEqual(16.6f, percentFemale, 0.1f);
            Assert::AreEqual(33.3f, percentNotSet, 0.1f);
            Assert::AreEqual(16.7f, percentNonHuman, 0.1f);
            Assert::AreEqual(100.0f, percentMaleConductors);
            Assert::AreEqual(0.0f, percentFemaleConductors);
            Assert::AreEqual(0.0f, percentNotSetConductors);
            Assert::AreEqual(0.0f, percentNonHumanConductors);
        }

        TEST_METHOD(RingerDatabase_ReadRingerNameWithExtendedCharacter)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_38.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)38, databaseVersionNumber);

            const Ringer* const sianAustin = database.RingersDatabase().FindRinger(13);
            Assert::IsTrue(sianAustin->Female());
            Assert::AreEqual(L"Si�n E", sianAustin->Forename().c_str());
            Assert::AreEqual(L"Austin", sianAustin->Surname().c_str());

            const Ringer* const samAustin = database.RingersDatabase().FindRinger(12);
            Assert::IsFalse(samAustin->Female());
            Assert::AreEqual(L"Samuel M", samAustin->Forename().c_str());
            Assert::AreEqual(L"Austin", samAustin->Surname().c_str());
        }

        TEST_METHOD(RingerDatabase_SuggestAllRingers_surnamepartialmatches)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_38.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)38, databaseVersionNumber);

            std::set<Duco::ObjectId> nearMatches;
            database.RingersDatabase().SuggestAllRingers(L"woo", nearMatches);
            Assert::AreEqual((size_t)6, nearMatches.size());

        }

        TEST_METHOD(RingerDatabase_SuggestAllRingers_surnameandfornamematches)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_38.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)38, databaseVersionNumber);

            std::set<Duco::ObjectId> nearMatches;
            database.RingersDatabase().SuggestAllRingers(L"paul", nearMatches);
            Assert::AreEqual((size_t)16, nearMatches.size());
        }

        TEST_METHOD(RingerDatabase_SuggestAllRingers_foreandsurname)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_38.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)38, databaseVersionNumber);

            std::set<Duco::ObjectId> nearMatches;
            database.RingersDatabase().SuggestAllRingers(L"s woof", nearMatches);
            Assert::AreEqual((size_t)1, nearMatches.size());
        }

        TEST_METHOD(RingerDatabase_SuggestRinger_ByPealCount)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            database.RingersDatabase().AddRinger(L"J (Jnr)", L"Wilde");
            database.RingersDatabase().AddRinger(L"J (Snr)", L"Wilde");
            database.RingersDatabase().AddRinger(L"James (Snr)", L"Wilde");
            database.RingersDatabase().AddRinger(L"John", L"Wilde");
            Duco::ObjectId ringerId = database.RingersDatabase().AddRinger(L"Jas", L"Wilde");
            database.RingersDatabase().AddRinger(L"Jas (Snr)", L"Wilde");
            database.RingersDatabase().AddRinger(L"Jas (Jnr)", L"Wilde");
            database.RingersDatabase().AddRinger(L"T", L"Wilde");
            database.RingersDatabase().AddRinger(L"Thomas", L"Wilde");
            database.RingersDatabase().AddRinger(L"Tom", L"Wilde");

            Duco::PerformanceDate performanceDate;

            Peal newPeal;
            newPeal.SetRingerId(ringerId, 1, false);

            database.PealsDatabase().AddObject(newPeal);

            std::set<Duco::RingerPealDates> nearMatches;

            Assert::AreEqual(Duco::ObjectId(), database.SuggestRingers(L"J Wilde", performanceDate, true, nearMatches));
            Assert::AreEqual((size_t)7, nearMatches.size());
            Assert::AreEqual(ringerId, nearMatches.begin()->ringerId);
        }

        TEST_METHOD(RingerDatabase_LinkedRingers_NoLinks)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Duco::ObjectId ringer1 = database.RingersDatabase().AddRinger(L"J (Jnr)", L"Wilde");
            Duco::ObjectId ringer2 = database.RingersDatabase().AddRinger(L"J (Snr)", L"Wilde");
            Duco::ObjectId ringer3 = database.RingersDatabase().AddRinger(L"R Owen", L"Battye");
            Duco::ObjectId ringer4 = database.RingersDatabase().AddRinger(L"Janet", L"Lucas");

            std::set<Duco::ObjectId> links;
            Assert::IsFalse(database.RingersDatabase().LinkedRingers(ringer1, links, false));
            Assert::IsFalse(database.RingersDatabase().LinkedRingers(ringer2, links, false));
            Assert::IsFalse(database.RingersDatabase().LinkedRingers(ringer3, links, false));
            Assert::IsFalse(database.RingersDatabase().LinkedRingers(ringer4, links, false));

            Assert::IsFalse(database.RingersDatabase().RingersLinked(ringer1, ringer2));
            Assert::IsFalse(database.RingersDatabase().RingersLinked(ringer1, ringer3));
            Assert::IsFalse(database.RingersDatabase().RingersLinked(ringer1, ringer4));
            Assert::IsFalse(database.RingersDatabase().RingersLinked(ringer2, ringer3));
            Assert::IsFalse(database.RingersDatabase().RingersLinked(ringer2, ringer4));
            Assert::IsFalse(database.RingersDatabase().RingersLinked(ringer3, ringer4));
        }


        TEST_METHOD(RingerDatabase_LinkedRingers_SimpleAdd)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Duco::ObjectId ringer1 = database.RingersDatabase().AddRinger(L"J (Jnr)", L"Wilde");
            Duco::ObjectId ringer2 = database.RingersDatabase().AddRinger(L"J (Snr)", L"Wilde");
            Duco::ObjectId ringer3 = database.RingersDatabase().AddRinger(L"R Owen", L"Battye");
            Duco::ObjectId ringer4 = database.RingersDatabase().AddRinger(L"Janet", L"Lucas");

            database.RingersDatabase().LinkRingers(ringer1, ringer2);

            std::set<Duco::ObjectId> links;
            Assert::IsFalse(database.RingersDatabase().LinkedRingers(ringer3, links, false));
            Assert::IsFalse(database.RingersDatabase().LinkedRingers(ringer4, links, false));

            Assert::IsTrue(database.RingersDatabase().LinkedRingers(ringer1, links, false));
            Assert::AreEqual((size_t)2, links.size());
            Assert::IsTrue(links.find(ringer2) != links.end());

            Assert::IsTrue(database.RingersDatabase().RingersLinked(ringer1, ringer2));
            Assert::IsFalse(database.RingersDatabase().RingersLinked(ringer1, ringer3));
            Assert::IsFalse(database.RingersDatabase().RingersLinked(ringer1, ringer4));
            Assert::IsFalse(database.RingersDatabase().RingersLinked(ringer2, ringer3));
            Assert::IsFalse(database.RingersDatabase().RingersLinked(ringer2, ringer4));
            Assert::IsFalse(database.RingersDatabase().RingersLinked(ringer3, ringer4));
        }

        TEST_METHOD(RingerDatabase_LinkedRingers_JoinLinks)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Duco::ObjectId ringer1 = database.RingersDatabase().AddRinger(L"J (Jnr)", L"Wilde");
            Duco::ObjectId ringer2 = database.RingersDatabase().AddRinger(L"J (Snr)", L"Wilde");
            Duco::ObjectId ringer3 = database.RingersDatabase().AddRinger(L"R Owen", L"Battye");
            Duco::ObjectId ringer4 = database.RingersDatabase().AddRinger(L"Janet", L"Lucas");

            database.RingersDatabase().LinkRingers(ringer1, ringer2);
            database.RingersDatabase().LinkRingers(ringer3, ringer4);

            std::set<Duco::ObjectId> links;
            Assert::IsTrue(database.RingersDatabase().LinkedRingers(ringer1, links, false));
            Assert::AreEqual((size_t)2, links.size());
            Assert::IsTrue(links.find(ringer2) != links.end());

            // Now join
            database.RingersDatabase().LinkRingers(ringer2, ringer4);

            Assert::IsTrue(database.RingersDatabase().LinkedRingers(ringer1, links, false));
            Assert::AreEqual((size_t)4, links.size());

            Assert::IsTrue(database.RingersDatabase().RingersLinked(ringer1, ringer2));
            Assert::IsTrue(database.RingersDatabase().RingersLinked(ringer1, ringer3));
            Assert::IsTrue(database.RingersDatabase().RingersLinked(ringer1, ringer4));
            Assert::IsTrue(database.RingersDatabase().RingersLinked(ringer2, ringer3));
            Assert::IsTrue(database.RingersDatabase().RingersLinked(ringer2, ringer4));
            Assert::IsTrue(database.RingersDatabase().RingersLinked(ringer3, ringer4));
        }

        TEST_METHOD(RingerDatabase_LinkedRingers_RemoveFromSingleLink)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Duco::ObjectId ringer1 = database.RingersDatabase().AddRinger(L"J (Jnr)", L"Wilde");
            Duco::ObjectId ringer2 = database.RingersDatabase().AddRinger(L"J (Snr)", L"Wilde");

            database.RingersDatabase().LinkRingers(ringer1, ringer2);

            std::set<Duco::ObjectId> links;

            Assert::IsTrue(database.RingersDatabase().LinkedRingers(ringer1, links, false));
            Assert::AreEqual((size_t)2, links.size());
            Assert::IsTrue(links.find(ringer2) != links.end());

            database.RingersDatabase().RemoveLinkedRinger(ringer1);

            Assert::IsFalse(database.RingersDatabase().LinkedRingers(ringer1, links, false));
        }

        TEST_METHOD(RingerDatabase_LinkRingers_LinkedAdditionalLink_Reverse)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Duco::ObjectId ringer1 = database.RingersDatabase().AddRinger(L"One", L"One");
            Duco::ObjectId ringer2 = database.RingersDatabase().AddRinger(L"Two", L"Two");
            Duco::ObjectId ringer3 = database.RingersDatabase().AddRinger(L"Three", L"Three");

            database.RingersDatabase().LinkRingers(ringer1, ringer2);
            database.RingersDatabase().LinkRingers(ringer3, ringer2);

            Assert::IsTrue(database.RingersDatabase().RingersLinked(ringer1, ringer3));
        }

        TEST_METHOD(RingerDatabase_LinkedRingers_RemoveFromMultipleLink)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Duco::ObjectId ringer1 = database.RingersDatabase().AddRinger(L"J (Jnr)", L"Wilde");
            Duco::ObjectId ringer2 = database.RingersDatabase().AddRinger(L"J (Snr)", L"Wilde");
            Duco::ObjectId ringer3 = database.RingersDatabase().AddRinger(L"J", L"Wilde");

            database.RingersDatabase().LinkRingers(ringer1, ringer2);
            database.RingersDatabase().LinkRingers(ringer1, ringer3);

            std::set<Duco::ObjectId> links;

            Assert::IsTrue(database.RingersDatabase().LinkedRingers(ringer1, links, false));
            Assert::AreEqual((size_t)3, links.size());
            Assert::IsTrue(links.find(ringer2) != links.end());
            Assert::IsTrue(links.find(ringer3) != links.end());

            Assert::IsTrue(database.RingersDatabase().RemoveLinkedRinger(ringer1));

            Assert::IsFalse(database.RingersDatabase().LinkedRingers(ringer1, links, false));
            Assert::IsTrue(database.RingersDatabase().LinkedRingers(ringer2, links, false));
            Assert::AreEqual((size_t)2, links.size());
            Assert::IsTrue(links.find(ringer3) != links.end());
        }

        TEST_METHOD(RingerDatabase_LinkedRingers_SaveAndRestore)
        {
            Duco::ObjectId ringer1;
            Duco::ObjectId ringer3;
            {
                RingingDatabase database;
                database.ClearDatabase(false, true);

                ringer1 = database.RingersDatabase().AddRinger(L"J (Jnr)", L"Wilde");
                database.RingersDatabase().AddRinger(L"J (Snr)", L"Wilde");
                ringer3 = database.RingersDatabase().AddRinger(L"J", L"Wilde");

                database.RingersDatabase().LinkRingers(ringer1, ringer3);

                database.Externalise("linked_ringers.duc", this);
            }

            RingingDatabase database2;
            unsigned int databaseVersion = 0;
            database2.Internalise("linked_ringers.duc", this, databaseVersion);

            Assert::IsTrue(databaseVersion >= 52);

            Assert::IsTrue(database2.RingersDatabase().RingersLinked(ringer1, ringer3));
        }

        TEST_METHOD(RingerDatabase_LinkedRingers_CombinePealLengthInfo)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Duco::ObjectId ringer1Id = database.RingersDatabase().AddRinger(L"J (Jnr)", L"Wilde");
            Duco::ObjectId ringer2Id = database.RingersDatabase().AddRinger(L"James", L"Wilde");
            Duco::ObjectId ringer3Id = database.RingersDatabase().AddRinger(L"J (Snr)", L"Wilde");
            database.RingersDatabase().LinkRingers(ringer1Id, ringer2Id);

            Peal pealOne;
            pealOne.SetChanges(5001);
            pealOne.SetRingerId(ringer1Id, 1, false);
            pealOne.SetRingerId(ringer3Id, 2, false);

            Peal pealTwo;
            pealTwo.SetChanges(5002);
            pealTwo.SetRingerId(ringer2Id, 1, false);
            pealTwo.SetRingerId(ringer3Id, 2, false);

            std::map<ObjectId, Duco::PealLengthInfo> ringerPealInfo;

            PealLengthInfo ringerOneInfo(pealOne);
            ringerPealInfo[ringer1Id] = ringerOneInfo;
            PealLengthInfo ringerTwoInfo(pealTwo);
            ringerPealInfo[ringer2Id] = ringerTwoInfo;
            PealLengthInfo ringerThreeInfo(pealOne);
            ringerThreeInfo += pealTwo;
            ringerPealInfo[ringer3Id] = ringerThreeInfo;

            database.RingersDatabase().CombineLinkedRingers(ringerPealInfo);

            Assert::AreEqual((size_t)2, ringerPealInfo.size());
            Assert::AreEqual((size_t)2, ringerPealInfo[ringer1Id].TotalPeals());
            Assert::AreEqual((size_t)2, ringerPealInfo[ringer3Id].TotalPeals());
        }

        TEST_METHOD(RingerDatabase_SetLinkedRingers_CheckDataChanged)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)36, databaseVersionNumber);

            Assert::IsFalse(database.DataChanged());

            database.RingersDatabase().LinkRingers(1, 2);
            Assert::IsTrue(database.DataChanged());
        }

        TEST_METHOD(RingerDatabase_SetInvalidLinkedRingers_CheckDataNotChanged)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)36, databaseVersionNumber);

            Assert::IsFalse(database.DataChanged());

            database.RingersDatabase().LinkRingers(100000000000000000, 10000000000000001);
            Assert::IsFalse(database.DataChanged());
        }

        TEST_METHOD(RingerDatabase_SetLinkedRingers_SameRinger_Ignored)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)36, databaseVersionNumber);

            Assert::IsFalse(database.DataChanged());

            database.RingersDatabase().LinkRingers(1, 1);
            Assert::IsFalse(database.DataChanged());
        }

        TEST_METHOD(RingerDatabase_LinkedRingers_Renumber)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Duco::ObjectId ringer1Id = database.RingersDatabase().AddRinger(L"B", L"B");
            Duco::ObjectId ringer2Id = database.RingersDatabase().AddRinger(L"C", L"C");
            Duco::ObjectId ringer3Id = database.RingersDatabase().AddRinger(L"A", L"A");
            database.RingersDatabase().LinkRingers(ringer1Id, ringer2Id);


            Assert::IsTrue(database.RingersDatabase().RingersLinked(ringer1Id, ringer2Id));
            Assert::IsFalse(database.RingersDatabase().RingersLinked(ringer1Id, ringer3Id));

            database.RebuildDatabase(NULL);
            std::set<Duco::ObjectId> closeMatches;
            ringer1Id = database.RingersDatabase().SuggestRinger(L"B B", false, closeMatches);
            ringer2Id = database.RingersDatabase().SuggestRinger(L"C C", false, closeMatches);
            ringer3Id = database.RingersDatabase().SuggestRinger(L"A A", false, closeMatches);

            Assert::IsTrue(database.RingersDatabase().RingersLinked(ringer1Id, ringer2Id));
            Assert::IsFalse(database.RingersDatabase().RingersLinked(ringer1Id, ringer3Id));
        }


        TEST_METHOD(RingerDatabase_CheckRemoveDuplicateLinks)
        {
            RingingDatabase database;
            Duco::ObjectId idOne = database.RingersDatabase().AddRinger(L"Owen Battye");
            Duco::ObjectId idTwo = database.RingersDatabase().AddRinger(L"O Battye");

            database.RingersDatabase().LinkRingers(idOne, idTwo);

            Assert::IsTrue(database.RingersDatabase().RingersLinked(idOne, idTwo));

            database.RingersDatabase().RemoveLinkedRinger(idOne);
            Assert::IsFalse(database.RingersDatabase().RingersLinked(idOne, idTwo));
        }

        TEST_METHOD(RingerDatabase_NameWithInitialDuplicate)
        {
            RingingDatabase database;
            Duco::ObjectId idOne = database.RingersDatabase().AddRinger(L"R O Battye");
            Duco::ObjectId idTwo = database.RingersDatabase().AddRinger(L"R Owen Battye");

            Assert::IsFalse(database.RingersDatabase().RingersLinked(idOne, idTwo));

            DatabaseSearch search(database);
            Duco::TSearchDuplicateObject* newStr = new Duco::TSearchDuplicateObject(true);
            search.AddValidationArgument(newStr);

            std::set<Duco::ObjectId> foundIds;
            search.Search(*this, foundIds, TObjectType::ERinger);
            Assert::AreEqual((size_t)2, foundIds.size());
        }


        TEST_METHOD(RingerDatabase_CheckRngersValidatedAndReportedAsDuplicates)
        {
            RingingDatabase database;
            Duco::ObjectId idOne = database.RingersDatabase().AddRinger(L"William Farey");
            Duco::ObjectId idTwo = database.RingersDatabase().AddRinger(L"William G Farey");
            Duco::ObjectId idThree = database.RingersDatabase().AddRinger(L"James Linnell");

            Assert::IsFalse(database.RingersDatabase().RingersLinked(idOne, idTwo));

            DatabaseSearch search(database);
            Duco::TSearchDuplicateObject* newStr = new Duco::TSearchDuplicateObject(true);
            search.AddValidationArgument(newStr);

            std::set<Duco::ObjectId> foundIds;
            search.Search(*this, foundIds, TObjectType::ERinger);
            Assert::AreEqual((size_t)2, foundIds.size());

            Assert::AreEqual(L"Ringer similar to another", database.RingersDatabase().FindRinger(idOne)->ErrorString(database.Settings(), false).c_str());
            Assert::AreEqual(L"Ringer similar to another", database.RingersDatabase().FindRinger(idTwo)->ErrorString(database.Settings(), false).c_str());
            Assert::AreEqual(L"", database.RingersDatabase().FindRinger(idThree)->ErrorString(database.Settings(), false).c_str());
        }

        TEST_METHOD(RingerDatabase_CheckMultipleRngersValidatedAndReportedAsDuplicates)
        {
            RingingDatabase database;
            Duco::ObjectId idOne = database.RingersDatabase().AddRinger(L"William Farey");
            Duco::ObjectId idTwo = database.RingersDatabase().AddRinger(L"William G Farey");
            Duco::ObjectId idThree = database.RingersDatabase().AddRinger(L"James Linnell");
            Duco::ObjectId idFour = database.RingersDatabase().AddRinger(L"William (Junior) Farey");
            Duco::ObjectId idFive = database.RingersDatabase().AddRinger(L"William (Senior) Farey");

            Assert::IsFalse(database.RingersDatabase().RingersLinked(idOne, idTwo));

            DatabaseSearch search(database);
            Duco::TSearchFieldStringArgument* newStr = new Duco::TSearchFieldStringArgument(TSearchFieldId::ESurname, L"Farey");
            search.AddArgument(newStr);


            Duco::TSearchDuplicateObject* newValid = new Duco::TSearchDuplicateObject(true);
            search.AddValidationArgument(newValid);

            std::set<Duco::ObjectId> foundIds;
            search.Search(*this, foundIds, TObjectType::ERinger);
            Assert::AreEqual((size_t)4, foundIds.size());

            Assert::AreEqual(L"Ringer similar to another", database.RingersDatabase().FindRinger(idOne)->ErrorString(database.Settings(), true).c_str());
            Assert::AreEqual(L"Ringer similar to another", database.RingersDatabase().FindRinger(idTwo)->ErrorString(database.Settings(), true).c_str());
            Assert::AreEqual(L"", database.RingersDatabase().FindRinger(idThree)->ErrorString(database.Settings(), true).c_str());
            Assert::AreEqual(L"Ringer similar to another", database.RingersDatabase().FindRinger(idFour)->ErrorString(database.Settings(), true).c_str());
            Assert::AreEqual(L"Ringer similar to another", database.RingersDatabase().FindRinger(idFive)->ErrorString(database.Settings(), true).c_str());
        }

        TEST_METHOD(RingerDatabase_CheckMultipleRingersValidatedAndReportedAsDuplicates_byNotvalid)
        {
            RingingDatabase database;
            Duco::ObjectId idOne = database.RingersDatabase().AddRinger(L"William Farey");
            Duco::ObjectId idTwo = database.RingersDatabase().AddRinger(L"William G Farey");
            Duco::ObjectId idThree = database.RingersDatabase().AddRinger(L"James Linnell");
            Duco::ObjectId idFour = database.RingersDatabase().AddRinger(L"William (Junior) Farey");
            Duco::ObjectId idFive = database.RingersDatabase().AddRinger(L"William (Senior) Farey");

            Assert::IsFalse(database.RingersDatabase().RingersLinked(idOne, idTwo));

            DatabaseSearch search(database);
            TSearchValidObject* newValid = new TSearchValidObject(true);
            search.AddValidationArgument(newValid);

            std::set<Duco::ObjectId> foundIds;
            search.Search(*this, foundIds, TObjectType::ERinger);
            Assert::AreEqual((size_t)5, foundIds.size());

            Assert::AreEqual(L"Gender not set, Ringer similar to another", database.RingersDatabase().FindRinger(idOne)->ErrorString(database.Settings(), true).c_str());
            Assert::AreEqual(L"Gender not set, Ringer similar to another", database.RingersDatabase().FindRinger(idTwo)->ErrorString(database.Settings(), true).c_str());
            Assert::AreEqual(L"Gender not set", database.RingersDatabase().FindRinger(idThree)->ErrorString(database.Settings(), true).c_str());
            Assert::AreEqual(L"Gender not set, Ringer similar to another", database.RingersDatabase().FindRinger(idFour)->ErrorString(database.Settings(), true).c_str());
            Assert::AreEqual(L"Gender not set, Ringer similar to another", database.RingersDatabase().FindRinger(idFive)->ErrorString(database.Settings(), true).c_str());
        }
    };
}
