#include "CppUnitTest.h"
#include <RingingDatabase.h>
#include "ToString.h"
#include <ImportExportProgressCallback.h>
#include <DatabaseRtfExporter.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(RtfExporter_UnitTests), ImportExportProgressCallback
    {
    public:
        //From ImportExportProgressCallback
        void InitialisingImportExport(bool internalising, unsigned int versionNumber, size_t totalNumberOfObjects)
        {

        }
        void ObjectProcessed(bool internalising, Duco::TObjectType objectType, Duco::ObjectId objectId, int totalPercentage, size_t numberInThisSubDatabase)
        {

        }
        void ImportExportComplete(bool internalising)
        {

        }
        void ImportExportFailed(bool internalising, int errorCode)
        {
            Assert::IsFalse(true);
        }

        // This test is to ensure this format doesnt change - as YACR use this format
        TEST_METHOD(RtfExporter_CompareToExistingFile)
        {
            const std::string inputFilename = "dordrecht_DBVer_26.duc";
            const std::string outputFilename = "dordrecht_DBVer_26_new.rtf";
            const std::string originalOutputFilename = "dordrecht_DBVer_26.rtf";

            {
                RingingDatabase database;
                database.ClearDatabase(false, true);

                unsigned int databaseVersion = 0;
                database.Internalise(inputFilename.c_str(), this, databaseVersion);

                std::set<Duco::ObjectId> pealsToExport;
                for (unsigned int count = 48; count <= 71; ++count)
                {
                    pealsToExport.insert(count);
                }

                DatabaseRtfExporter exporter(database, pealsToExport);
                exporter.Export(outputFilename.c_str(), this);
            }
            {
                Assert::IsTrue(compareFiles(outputFilename, originalOutputFilename));
            }
        }

        bool compareFiles(const std::string& p1, const std::string& p2) {
            std::wifstream f1(p1, std::wifstream::binary | std::wifstream::ate);
            std::wifstream f2(p2, std::wifstream::binary | std::wifstream::ate);

            if (f1.fail() || f2.fail()) {
                return false; //file problem
            }

            if (f1.tellg() != f2.tellg()) {
                return false; //size mismatch
            }

            //seek back to beginning and use std::equal to compare contents
            f1.seekg(0, std::wifstream::beg);
            f2.seekg(0, std::wifstream::beg);
            return std::equal(std::istreambuf_iterator<wchar_t>(f1.rdbuf()),
                std::istreambuf_iterator<wchar_t>(),
                std::istreambuf_iterator<wchar_t>(f2.rdbuf()));
        }
    };
}
