#include "CppUnitTest.h"
#include <DucoConfiguration.h>
#include <ImportExportProgressCallback.h>
#include <Method.h>
#include <MethodDatabase.h>
#include <MethodSeries.h>
#include <MethodSeriesDatabase.h>
#include <Peal.h>
#include <PealDatabase.h>
#include <ProgressCallback.h>
#include <RenumberProgressCallback.h>
#include <Ringer.h>
#include <RingerDatabase.h>
#include <RingingDatabase.h>
#include "ToString.h"
#include <StatisticFilters.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(MethodSeriesDatabase_UnitTests), Duco::ImportExportProgressCallback, Duco::RenumberProgressCallback, Duco::ProgressCallback
    {
    protected:
        RenumberProgressCallback::TRenumberStage lastStage;

    public:
        // RenumberProgressCallback
        void RenumberInitialised(RenumberProgressCallback::TRenumberStage newStage)
        {
            if (RenumberProgressCallback::TRenumberStage::ENotStarted != newStage)
            {
                Assert::IsTrue(newStage > lastStage);
            }
            lastStage = newStage;
        }

        void RenumberStep(size_t objectId, size_t total)
        {

        }
        void RenumberComplete()
        {
        }
        // From ImportExportProgressCallback
        void InitialisingImportExport(bool internalising, unsigned int versionNumber, size_t totalNumberOfObjects)
        {
        }
        void ObjectProcessed(bool internalising, Duco::TObjectType objectType, Duco::ObjectId objectId, int totalPercentage, size_t numberInThisSubDatabase)
        {
        }
        void ImportExportComplete(bool internalising)
        {
        }
        void ImportExportFailed(bool internalising, int errorCode)
        {
            Assert::AreEqual(0, errorCode);
        }
        // From ProgressCallback
        virtual void Initialised()
        {
        }
        void Step(int progressPercent)
        {
        }
        void Complete(bool error)
        {
            Assert::IsFalse(error);
        }

        BEGIN_TEST_METHOD_ATTRIBUTE(MethodSeriesDatabase_InternaliseAndCheckSeries)
            TEST_PRIORITY(2)
        END_TEST_METHOD_ATTRIBUTE()
        TEST_METHOD(MethodSeriesDatabase_InternaliseAndCheckSeries)
        {
            RingingDatabase database;
            const char* filename("jennie_paul.duc");
            unsigned int dbVer = 0;
            database.Internalise(filename, this, dbVer);
            Assert::IsFalse(dbVer == 0);

            const Peal* thePeal = database.PealsDatabase().FindPeal(26, false);
            Assert::AreEqual(ObjectId(1), thePeal->SeriesId());

            thePeal = database.PealsDatabase().FindPeal(22, false);
            Assert::AreEqual(ObjectId(16), thePeal->SeriesId());

            const MethodSeries* theSeries1 = database.MethodSeriesDatabase().FindMethodSeries(1);
            Assert::IsNotNull(theSeries1);
            const MethodSeries* theSeries16 = database.MethodSeriesDatabase().FindMethodSeries(16);
            Assert::IsNull(theSeries16);
        }

        TEST_METHOD(MethodSeriesDatabase_SortAndRenumber)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            MethodSeries seriesTwo(KNoId, L"Series 2", 4);
            Duco::ObjectId seriesTwoId = database.MethodSeriesDatabase().AddObject(seriesTwo);
            MethodSeries seriesOne (KNoId, L"Series 1", 2);
            Duco::ObjectId seriesOneId = database.MethodSeriesDatabase().AddObject(seriesOne);
            MethodSeries seriesThree(KNoId, L"Series 3", 7);
            Duco::ObjectId seriesThreeId = database.MethodSeriesDatabase().AddObject(seriesThree);

            Peal pealOne;
            pealOne.SetSeriesId(seriesTwoId);
            Duco::ObjectId pealOneId = database.PealsDatabase().AddObject(pealOne);

            Assert::AreEqual(seriesTwoId, database.PealsDatabase().FindPeal(pealOneId)->SeriesId());

            database.RebuildDatabase(this);
            Duco::ObjectId renumberedSeriesId = database.PealsDatabase().FindPeal(pealOneId)->SeriesId();

            const Duco::MethodSeries* renumberedSeries = database.MethodSeriesDatabase().FindMethodSeries(renumberedSeriesId);
            Assert::AreEqual(L"Series 2", renumberedSeries->Name().c_str());
            Assert::AreNotEqual(seriesTwoId, renumberedSeries->Id());

        }

        TEST_METHOD(MethodSeriesDatabase_DeleteUsedAndRenumber)
        {
            RingingDatabase database;
            Duco::StatisticFilters filters(database);
            database.ClearDatabase(false, true);

            MethodSeries seriesOne(KNoId, L"Series 1", 2);
            Duco::ObjectId seriesOneId = database.MethodSeriesDatabase().AddObject(seriesOne);
            MethodSeries seriesTwo(KNoId, L"Series 2", 4);
            Duco::ObjectId seriesTwoId = database.MethodSeriesDatabase().AddObject(seriesTwo);
            MethodSeries seriesThree(KNoId, L"Series 3", 7);
            Duco::ObjectId seriesThreeId = database.MethodSeriesDatabase().AddObject(seriesThree);

            Peal pealOne;
            pealOne.SetSeriesId(seriesTwoId);
            Duco::ObjectId pealOneId = database.PealsDatabase().AddObject(pealOne);

            Assert::AreEqual((size_t)1, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)3, database.NumberOfMethodSeries());

            std::set<ObjectId> unusedMethods;
            std::set<ObjectId> unusedRingers;
            std::set<ObjectId> unusedTowers;
            std::multimap<ObjectId, ObjectId> unusedRings;
            std::set<ObjectId> unusedMethodSeries;
            std::set<ObjectId> unusedCompositions;
            std::set<ObjectId> unusedAssociations;
            database.FindUnused(unusedMethods, unusedRingers, unusedTowers, unusedRings, unusedMethodSeries, unusedCompositions, unusedAssociations);
            Assert::AreEqual((size_t)2, unusedMethodSeries.size());

            database.MethodSeriesDatabase().DeleteObjects(unusedMethodSeries);

            database.RebuildDatabase(this);

            Assert::AreEqual((size_t)1, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)1, database.NumberOfMethodSeries());

            Duco::ObjectId renumberedSeriesId = database.PealsDatabase().FindPeal(pealOneId)->SeriesId();

            const Duco::MethodSeries* renumberedSeries = database.MethodSeriesDatabase().FindMethodSeries(renumberedSeriesId);
            Assert::AreEqual(L"Series 2", renumberedSeries->Name().c_str());
            Assert::AreNotEqual(seriesTwoId, renumberedSeries->Id());
        }

        TEST_METHOD(MethodSeriesDatabase_AddMethods)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            MethodSeries seriesOne(KNoId, L"Series 1", 2);
            Duco::ObjectId seriesOneId = database.MethodSeriesDatabase().AddObject(seriesOne);
            Assert::AreEqual((size_t)1, database.NumberOfMethodSeries());

            Method cambridgeMinor(KNoId, 6, L"Cambridge", L"Surprise", L"");
            Duco::ObjectId cambridgeId = database.MethodsDatabase().AddObject(cambridgeMinor);
            Method beverleyMinor(KNoId, 6, L"Beverley", L"Surprise", L"");
            Duco::ObjectId beverleyId = database.MethodsDatabase().AddObject(beverleyMinor);
            Method surfleetMinor(KNoId, 6, L"Surfleet", L"Surprise", L"");
            Duco::ObjectId surfleetId = database.MethodsDatabase().AddObject(surfleetMinor);

            std::set<Duco::ObjectId> methodIds;
            methodIds.insert(cambridgeId);
            methodIds.insert(beverleyId);
            seriesOne.AddMethods(methodIds, database);
            Assert::AreEqual((size_t)2, seriesOne.NoOfMethods());

            Duco::PerformanceDate unsetDate;
            unsetDate.ResetToEarliest();
            Duco::PerformanceDate completedDate;
            PealLengthInfo info;
            size_t unrungMethods;
            Assert::IsFalse(seriesOne.SeriesCompletedDate(database, completedDate, unrungMethods, info));
            Assert::AreEqual((size_t)2, unrungMethods);
            Assert::AreEqual(unsetDate, completedDate);
            Assert::AreEqual((size_t)0, info.TotalPeals());

            Assert::AreEqual(L"Cambridge and Beverley Surprise", seriesOne.MethodNames(database.MethodsDatabase()).c_str());
        }

        TEST_METHOD(MethodSeriesDatabase_Externalise)
        {
            const char* filename("tempdatabase.duc");
            {
                RingingDatabase database;
                database.ClearDatabase(false, true);

                MethodSeries seriesOne(KNoId, L"Series 1", 2);
                Duco::ObjectId seriesOneId = database.MethodSeriesDatabase().AddObject(seriesOne);
                MethodSeries seriesTwo(KNoId, L"Series 2", 4);
                Duco::ObjectId seriesTwoId = database.MethodSeriesDatabase().AddObject(seriesTwo);
                MethodSeries seriesThree(KNoId, L"Series 3", 7);
                Duco::ObjectId seriesThreeId = database.MethodSeriesDatabase().AddObject(seriesThree);
                Assert::AreEqual((size_t)3, database.NumberOfMethodSeries());

                database.Externalise(filename, this);
            }

            {
                RingingDatabase database;
                unsigned int dbVer = 0;
                database.Internalise(filename, this, dbVer);
                Assert::IsFalse(dbVer == 0);
                Assert::AreEqual((size_t)3, database.NumberOfMethodSeries());
            }
        }

        TEST_METHOD(MethodSeriesDatabase_GetMethodSeriesByNoOfBells)
        {
            RingingDatabase database;
            unsigned int databaseVersion;
            database.Internalise("owen_battye_DBVer_38.duc", this, databaseVersion);
            Duco::StatisticFilters filters(database);
            Assert::AreEqual((size_t)611, database.PerformanceInfo(filters).TotalPeals());

            std::vector<Duco::ObjectId> objectIds;
            database.MethodSeriesDatabase().GetMethodSeriesByNoOfBells(objectIds, 6);

            Assert::AreEqual((size_t)42, objectIds.size());
        }

        TEST_METHOD(MethodSeriesDatabase_SeriesCompletedDate_Completed)
        {
            RingingDatabase database;
            unsigned int databaseVersion;
            database.Internalise("owen_battye_DBVer_38.duc", this, databaseVersion);
            Duco::StatisticFilters filters(database);
            Assert::AreEqual((size_t)611, database.PerformanceInfo(filters).TotalPeals());

            const MethodSeries* const theSeries = database.MethodSeriesDatabase().FindMethodSeries(9);
            Assert::AreEqual(L"4 Spliced Surprise Royal (S, B, C, L)", theSeries->Name().c_str());

            Duco::PerformanceDate completedDate;
            size_t unrungMethods = 99;
            Duco::PealLengthInfo allPealsInfo;
            
            theSeries->SeriesCompletedDate(database, completedDate, unrungMethods, allPealsInfo);

            Assert::AreEqual((size_t)0, unrungMethods);
            Assert::AreEqual((size_t)128, allPealsInfo.TotalPeals());
            Assert::AreEqual(PerformanceDate(2006, 6, 11), completedDate);
        }

        TEST_METHOD(MethodSeriesDatabase_SeriesCompletedDate_Incompleted)
        {
            RingingDatabase database;
            unsigned int databaseVersion;
            database.Internalise("owen_battye_DBVer_38.duc", this, databaseVersion);
            Duco::StatisticFilters filters(database);
            Assert::AreEqual((size_t)611, database.PerformanceInfo(filters).TotalPeals());

            const MethodSeries* const theSeries = database.MethodSeriesDatabase().FindMethodSeries(8);
            Assert::AreEqual(L"Grandsire", theSeries->Name().c_str());

            Duco::PerformanceDate completedDate;
            size_t unrungMethods = 99;
            Duco::PealLengthInfo allPealsInfo;

            theSeries->SeriesCompletedDate(database, completedDate, unrungMethods, allPealsInfo);

            Assert::AreEqual((size_t)2, unrungMethods);
            Assert::IsFalse(completedDate.Valid());
            Assert::AreEqual((size_t)8, allPealsInfo.TotalPeals());
        }
    };
}
