#include "CppUnitTest.h"
#include <MilestoneData.h>
#include <Peal.h>
#include <PealDatabase.h>
#include <RingingDatabase.h>
#include <RenumberProgressCallback.h>
#include <ctime>
#include "ToString.h"
#include <StatisticFilters.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(MilestoneData_UnitTests), RenumberProgressCallback
    {
    public:
        // From RenumberProgressCallback
        void RenumberInitialised(RenumberProgressCallback::TRenumberStage newStage)
        {
        }
        void RenumberStep(size_t objectId, size_t total)
        {
        }
        void RenumberComplete()
        {
        }
        TEST_METHOD(MilestoneData_EmptyDatabase)
        {
            RingingDatabase database;
            Duco::StatisticFilters filters(database);
            std::set<MilestoneData> milestones;
            Assert::IsTrue(database.PealsDatabase().Milestones(filters, milestones));
            Assert::AreEqual((size_t)2, milestones.size());
        }

        void DatePlusDays(struct tm* date, int days)
        {
            const time_t ONE_DAY = 24 * 60 * 60;
            // Seconds since start of epoch
            time_t date_seconds = mktime(date) + (days * ONE_DAY);
            // Update caller's date
            // Use localtime because mktime converts to UTC so may change date
            *date = *localtime(&date_seconds);
        }

        PerformanceDate GeneratePeals(Duco::RingingDatabase& database, size_t numberOfPealsToGenerate, unsigned int daysBetweenPeals, int daysOffSet = 0)
        {
            time_t t = time(NULL);
            tm now = *gmtime(&t);
            
            DatePlusDays(&now, daysOffSet);

            for (unsigned int count = 1; count < numberOfPealsToGenerate; ++count)
            {
                DatePlusDays(&now, -(int)daysBetweenPeals);
            }
            PerformanceDate firstPealDate(now);

            for (unsigned int count = 0; count < numberOfPealsToGenerate; ++count)
            {
                Peal thePeal;
                PerformanceDate pealDate(now);
                thePeal.SetDate(pealDate);
                database.PealsDatabase().AddObject(thePeal);
                DatePlusDays(&now, daysBetweenPeals);
            }
            return firstPealDate;
        }

        TEST_METHOD(MilestoneData_OnePealToday)
        {
            RingingDatabase database;

            Peal thePeal;
            database.PealsDatabase().AddObject(thePeal);

            Duco::StatisticFilters filters(database);
            std::set<MilestoneData> milestones;
            Assert::IsTrue(database.PealsDatabase().Milestones(filters, milestones));
            Assert::AreEqual((size_t)3, milestones.size());
            // 1, 10, 25, 50
            std::set<MilestoneData>::const_iterator it = milestones.begin();
            while (it != milestones.end())
            {
                if (it->Milestone() == 1)
                {
                    Assert::IsFalse(it->Prediction());
                    Assert::IsTrue(it->PealId().ValidId());
                    Assert::IsTrue(it->DateAchieved().Valid());
                    Assert::AreEqual(PerformanceDate(), it->DateAchieved());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsing6MonthAverage());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsing12MonthAverage());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsingAllTimeAverage());
                    Assert::AreEqual((size_t)0, it->PealsIn6MonthWindow());
                    Assert::AreEqual((size_t)0, it->PealsIn12MonthWindow());
                    Assert::AreEqual((size_t)0, it->PealsInAllTimeWindow());
                }
                else if (it->Milestone() == 5)
                {
                    Assert::IsTrue(it->Prediction());
                    Assert::IsFalse(it->PealId().ValidId());
                    Assert::AreEqual((size_t)730, it->DaysToMilestoneUsing6MonthAverage());
                    Assert::AreEqual((size_t)1461, it->DaysToMilestoneUsing12MonthAverage());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsingAllTimeAverage());
                }
                else if (it->Milestone() == 10)
                {
                    Assert::IsTrue(it->Prediction());
                    Assert::IsTrue(it->DateAchieved().Valid());
                    Assert::IsFalse(it->PealId().ValidId());
                    Assert::AreEqual((size_t)1643, it->DaysToMilestoneUsing6MonthAverage());
                    Assert::AreEqual((size_t)3287, it->DaysToMilestoneUsing12MonthAverage());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsingAllTimeAverage());
                    Assert::AreEqual((size_t)1, it->PealsIn6MonthWindow());
                    Assert::AreEqual((size_t)1, it->PealsIn12MonthWindow());
                    Assert::AreEqual((size_t)1, it->PealsInAllTimeWindow());
                }
                else
                {
                    Assert::IsFalse(true); // wrong milestone
                }
                ++it;
            }
        }
        TEST_METHOD(MilestoneData_OnePealTwoDaysAgo)
        {
            RingingDatabase database;

            time_t t = time(NULL);
            tm now = *gmtime(&t);
            DatePlusDays(&now, -2);
            PerformanceDate pealDate(now);

            Peal thePeal;
            thePeal.SetDate(pealDate);
            database.PealsDatabase().AddObject(thePeal);

            Duco::StatisticFilters filters(database);
            std::set<MilestoneData> milestones;
            Assert::IsTrue(database.PealsDatabase().Milestones(filters, milestones));
            Assert::AreEqual((size_t)3, milestones.size());
            // 1, 10, 25, 50
            std::set<MilestoneData>::const_iterator it = milestones.begin();
            while (it != milestones.end())
            {
                if (it->Milestone() == 1)
                {
                    Assert::IsFalse(it->Prediction());
                    Assert::IsTrue(it->PealId().ValidId());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsing6MonthAverage());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsing12MonthAverage());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsingAllTimeAverage());
                    Assert::AreEqual((size_t)0, it->PealsIn6MonthWindow());
                    Assert::AreEqual((size_t)0, it->PealsIn12MonthWindow());
                    Assert::AreEqual((size_t)0, it->PealsInAllTimeWindow());
                }
                else if (it->Milestone() == 5)
                {
                    Assert::IsTrue(it->Prediction());
                    Assert::IsFalse(it->PealId().ValidId());
                    Assert::AreEqual((size_t)730, it->DaysToMilestoneUsing6MonthAverage());
                    Assert::AreEqual((size_t)1461, it->DaysToMilestoneUsing12MonthAverage());
                    Assert::AreEqual((size_t)8, it->DaysToMilestoneUsingAllTimeAverage());
                }
                else if (it->Milestone() == 10)
                {
                    Assert::IsTrue(it->Prediction());
                    Assert::IsFalse(it->PealId().ValidId());
                    Assert::AreEqual((size_t)1643, it->DaysToMilestoneUsing6MonthAverage());
                    Assert::AreEqual((size_t)3287, it->DaysToMilestoneUsing12MonthAverage());
                    Assert::AreEqual((size_t)18, it->DaysToMilestoneUsingAllTimeAverage());
                    Assert::AreEqual((size_t)1, it->PealsIn6MonthWindow());
                    Assert::AreEqual((size_t)1, it->PealsIn12MonthWindow());
                    Assert::AreEqual((size_t)1, it->PealsInAllTimeWindow());
                }
                else
                {
                    Assert::IsFalse(true); // wrong milestone
                }
                ++it;
            }
        }

        TEST_METHOD(MilestoneData_TenPeals)
        {
            RingingDatabase database;

            GeneratePeals(database, 10, 2);

            Duco::StatisticFilters filters(database);
            std::set<MilestoneData> milestones;
            Assert::IsTrue(database.PealsDatabase().Milestones(filters, milestones));
            Assert::AreEqual((size_t)4, milestones.size());
            // 1, 10, 25, 50
            std::set<MilestoneData>::const_iterator it = milestones.begin();
            while (it != milestones.end())
            {
                if (it->Milestone() == 1)
                {
                    Assert::IsFalse(it->Prediction());
                    Assert::IsTrue(it->PealId().ValidId());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsing6MonthAverage());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsing12MonthAverage());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsingAllTimeAverage());
                    Assert::AreEqual((size_t)0, it->PealsIn6MonthWindow());
                    Assert::AreEqual((size_t)0, it->PealsIn12MonthWindow());
                    Assert::AreEqual((size_t)0, it->PealsInAllTimeWindow());
                }
                else if (it->Milestone() == 10)
                {
                    Assert::IsFalse(it->Prediction());
                    Assert::IsTrue(it->PealId().ValidId());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsing6MonthAverage());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsing12MonthAverage());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsingAllTimeAverage());
                }
                else if (it->Milestone() == 25)
                {
                    Assert::IsTrue(it->Prediction());
                    Assert::IsFalse(it->PealId().ValidId());
                    Assert::AreEqual((size_t)273, it->DaysToMilestoneUsing6MonthAverage());
                    Assert::AreEqual((size_t)547, it->DaysToMilestoneUsing12MonthAverage());
                    Assert::AreEqual((size_t)26, it->DaysToMilestoneUsingAllTimeAverage());
                }
                else if (it->Milestone() == 50)
                {
                    Assert::IsTrue(it->Prediction());
                    Assert::IsFalse(it->PealId().ValidId());
                    Assert::AreEqual((size_t)730, it->DaysToMilestoneUsing6MonthAverage());
                    Assert::AreEqual((size_t)1461, it->DaysToMilestoneUsing12MonthAverage());
                    Assert::AreEqual((size_t)72, it->DaysToMilestoneUsingAllTimeAverage());
                    Assert::AreEqual((size_t)10, it->PealsIn6MonthWindow());
                    Assert::AreEqual((size_t)10, it->PealsIn12MonthWindow());
                    Assert::AreEqual((size_t)10, it->PealsInAllTimeWindow());
                }
                else
                {
                    Assert::IsFalse(true); // wrong milestone
                }
                ++it;
            }
        }
        TEST_METHOD(MilestoneData_FiftyPeals)
        {
            RingingDatabase database;

            GeneratePeals(database, 50, 1);

            Duco::StatisticFilters filters(database);
            std::set<MilestoneData> milestones;
            Assert::IsTrue(database.PealsDatabase().Milestones(filters, milestones));
            Assert::AreEqual((size_t)6, milestones.size());
            // 1, 10, 25, 50
            std::set<MilestoneData>::const_iterator it = milestones.begin();
            while (it != milestones.end())
            {
                if (it->Milestone() == 1)
                {
                    Assert::IsFalse(it->Prediction());
                    Assert::IsTrue(it->PealId().ValidId());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsing6MonthAverage());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsing12MonthAverage());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsingAllTimeAverage());
                }
                else if (it->Milestone() == 10)
                {
                    Assert::IsFalse(it->Prediction());
                    Assert::IsTrue(it->PealId().ValidId());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsing6MonthAverage());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsing12MonthAverage());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsingAllTimeAverage());
                }
                else if (it->Milestone() == 25)
                {
                    Assert::IsFalse(it->Prediction());
                    Assert::IsTrue(it->PealId().ValidId());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsing6MonthAverage());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsing12MonthAverage());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsingAllTimeAverage());
                }
                else if (it->Milestone() == 50)
                {
                    Assert::IsFalse(it->Prediction());
                    Assert::IsTrue(it->PealId().ValidId());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsing6MonthAverage());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsing12MonthAverage());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsingAllTimeAverage());
                }
                else if (it->Milestone() == 100)
                {
                    Assert::IsTrue(it->Prediction());
                    Assert::IsFalse(it->PealId().ValidId());
                    Assert::AreEqual((size_t)182, it->DaysToMilestoneUsing6MonthAverage());
                    Assert::AreEqual((size_t)365, it->DaysToMilestoneUsing12MonthAverage());
                    Assert::AreEqual((size_t)49, it->DaysToMilestoneUsingAllTimeAverage());
                }
                else if (it->Milestone() == 250)
                {
                    Assert::IsTrue(it->Prediction());
                    Assert::IsFalse(it->PealId().ValidId());
                    Assert::AreEqual((size_t)730, it->DaysToMilestoneUsing6MonthAverage());
                    Assert::AreEqual((size_t)1461, it->DaysToMilestoneUsing12MonthAverage());
                    Assert::AreEqual((size_t)196, it->DaysToMilestoneUsingAllTimeAverage());
                }
                else
                {
                    Assert::IsFalse(true); // wrong milestone
                }
                ++it;
            }
        }

        TEST_METHOD(MilestoneData_OneHundredPeals)
        {
            RingingDatabase database;

            GeneratePeals(database, 50, 1);
            PerformanceDate currentDate;
            PerformanceDate firstPealDate = GeneratePeals(database, 50, 5);
            database.RebuildDatabase(this);

            Duco::StatisticFilters filters(database);
            std::set<MilestoneData> milestones;
            Assert::IsTrue(database.PealsDatabase().Milestones(filters, milestones));
            Assert::AreEqual((size_t)8, milestones.size());
            // 1, 10, 25, 50, 100, 250, 1000.
            size_t countFound = 0;
            std::set<MilestoneData>::const_iterator it = milestones.begin();
            while (it != milestones.end())
            {
                if (it->Milestone() == 1)
                {
                    countFound++;
                    Assert::IsFalse(it->Prediction());
                    Assert::IsTrue(it->PealId().ValidId());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsing6MonthAverage());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsing12MonthAverage());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsingAllTimeAverage());
                }
                else if (it->Milestone() == 10)
                {
                    countFound++;
                    Assert::IsFalse(it->Prediction());
                    Assert::IsTrue(it->PealId().ValidId());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsing6MonthAverage());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsing12MonthAverage());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsingAllTimeAverage());
                }
                else if (it->Milestone() == 25)
                {
                    countFound++;
                    Assert::IsFalse(it->Prediction());
                    Assert::IsTrue(it->PealId().ValidId());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsing6MonthAverage());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsing12MonthAverage());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsingAllTimeAverage());
                }
                else if (it->Milestone() == 50)
                {
                    countFound++;
                    Assert::IsFalse(it->Prediction());
                    Assert::IsTrue(it->PealId().ValidId());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsing6MonthAverage());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsing12MonthAverage());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsingAllTimeAverage());
                }
                else if (it->Milestone() == 100)
                {
                    countFound++;
                    Assert::IsFalse(it->Prediction());
                    Assert::IsTrue(it->PealId().ValidId());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsing6MonthAverage());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsing12MonthAverage());
                    Assert::AreEqual((size_t)0, it->DaysToMilestoneUsingAllTimeAverage());
                }
                else if (it->Milestone() == 200)
                {
                    countFound++;
                    Assert::IsTrue(it->Prediction());
                    Assert::IsFalse(it->PealId().ValidId());
                    Assert::AreEqual((size_t)209, it->DaysToMilestoneUsing6MonthAverage());
                    Assert::AreEqual((size_t)365, it->DaysToMilestoneUsing12MonthAverage());
                    Assert::AreEqual((size_t)244, it->DaysToMilestoneUsingAllTimeAverage());
                }
                else if (it->Milestone() == 250)
                {
                    countFound++;
                    Assert::IsTrue(it->Prediction());
                    Assert::IsFalse(it->PealId().ValidId());
                    Assert::AreEqual((size_t)314, it->DaysToMilestoneUsing6MonthAverage());
                    Assert::AreEqual((size_t)547, it->DaysToMilestoneUsing12MonthAverage());
                    Assert::AreEqual((size_t)367, it->DaysToMilestoneUsingAllTimeAverage());
                }
                else if (it->Milestone() == 1000)
                {
                    countFound++;
                    Assert::IsTrue(it->Prediction());
                    Assert::IsFalse(it->PealId().ValidId());
                    Assert::AreEqual((size_t)1889, it->DaysToMilestoneUsing6MonthAverage());
                    Assert::AreEqual((size_t)3287, it->DaysToMilestoneUsing12MonthAverage());
                    Assert::AreEqual((size_t)2205, it->DaysToMilestoneUsingAllTimeAverage());

                    Assert::AreEqual(firstPealDate, it->StartDateOfAllTimeCalculation());
                    PerformanceDate predictedEndDate = currentDate.AddDays(it->DaysToMilestoneUsingAllTimeAverage());
                    Assert::AreEqual(predictedEndDate, it->DateOfMilestoneUsingAllTimeAverage());
                    predictedEndDate = currentDate.AddDays(it->DaysToMilestoneUsing6MonthAverage());
                    Assert::AreEqual(predictedEndDate, it->DateOfMilestoneUsing6MonthAverage());
                    predictedEndDate = currentDate.AddDays(it->DaysToMilestoneUsing12MonthAverage());
                    Assert::AreEqual(predictedEndDate, it->DateOfMilestoneUsing12MonthAverage());
                    Assert::AreEqual((size_t)87, it->PealsIn6MonthWindow());
                    Assert::AreEqual((size_t)100, it->PealsIn12MonthWindow());
                    Assert::AreEqual((size_t)100, it->PealsInAllTimeWindow());
                }
                else
                {
                    Assert::IsFalse(true); // wrong milestone
                }
                ++it;
            }
            Assert::AreEqual((size_t)8, countFound);
        }

        TEST_METHOD(MilestoneData_601Peals)
        {
            RingingDatabase database;

            GeneratePeals(database, 601, 2);

            Duco::StatisticFilters filters(database);
            std::set<MilestoneData> milestones;
            Assert::IsTrue(database.PealsDatabase().Milestones(filters, milestones));
            Assert::AreEqual((size_t)19, milestones.size());
            // 1, 10, 25, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 601: 700, 750, 1000
            std::set<MilestoneData>::const_iterator it = milestones.begin();
            while (it != milestones.end())
            {
                switch (it->Milestone())
                {
                case 601:
                    Assert::IsFalse(it->Prediction());
                    Assert::IsTrue(it->LastPeal());
                    break;
                case 700:
                case 750:
                case 1000:
                    Assert::IsTrue(it->Prediction());
                    Assert::IsFalse(it->LastPeal());
                    break;
                default:
                    Assert::IsFalse(it->Prediction());
                    Assert::IsFalse(it->LastPeal());
                    break;
                }
                ++it;
            }
        }

        TEST_METHOD(MilestoneData_OnePealALongTimeAgo)
        {
            RingingDatabase database;

            PerformanceDate pealDate(1800, 1, 1);
            Peal thePeal;
            thePeal.SetDate(pealDate);
            database.PealsDatabase().AddObject(thePeal);

            Duco::StatisticFilters filters(database);
            std::set<MilestoneData> milestones;
            Assert::IsTrue(database.PealsDatabase().Milestones(filters, milestones));
            Assert::AreEqual((size_t)3, milestones.size());
            // 1; 5, 10
            std::set<MilestoneData>::const_iterator it = milestones.begin();
            while (it != milestones.end())
            {
                switch (it->Milestone())
                {
                case 1:
                    Assert::AreEqual(pealDate, it->DateAchieved());
                    Assert::IsFalse(it->Prediction());
                    Assert::IsTrue(it->LastPeal());
                    break;
                case 5:
                default:
                    Assert::IsTrue(it->Prediction());
                    Assert::IsFalse(it->LastPeal());
                    Assert::IsTrue(it->DateOfMilestoneUsingAllTimeAverage().Valid());
                    Assert::IsFalse(it->DateOfMilestoneUsing6MonthAverage().Valid());
                    Assert::IsFalse(it->DateOfMilestoneUsing12MonthAverage().Valid());
                    break;
                case 10:
                    Assert::IsTrue(it->Prediction());
                    Assert::IsFalse(it->LastPeal());
                    Assert::IsFalse(it->DateOfMilestoneUsingAllTimeAverage().Valid());
                    Assert::IsFalse(it->DateOfMilestoneUsing6MonthAverage().Valid());
                    Assert::IsFalse(it->DateOfMilestoneUsing12MonthAverage().Valid());
                    break;
                }
                ++it;
            }
        }
        TEST_METHOD(MilestoneData_FewPealsALongTimeAgo)
        {
            RingingDatabase database;

            PerformanceDate pealDate(1800, 1, 1);
            Peal thePeal;
            thePeal.SetDate(pealDate);
            database.PealsDatabase().AddObject(thePeal);

            GeneratePeals(database, 50, 365);

            Duco::StatisticFilters filters(database);
            std::set<MilestoneData> milestones;
            Assert::IsTrue(database.PealsDatabase().Milestones(filters, milestones));
            Assert::AreEqual((size_t)7, milestones.size());
            // 1, 10, 25, 50; 51; 100, 250
            std::set<MilestoneData>::const_iterator it = milestones.begin();
            while (it != milestones.end())
            {
                switch (it->Milestone())
                {
                case 1:
                    Assert::AreEqual(pealDate, it->DateAchieved());
                case 10:
                case 25:
                case 50:
                    Assert::IsFalse(it->Prediction());
                    Assert::IsFalse(it->LastPeal());
                    break;

                case 51:
                    Assert::IsFalse(it->Prediction());
                    Assert::IsTrue(it->LastPeal());
                    break;

                default:
                    Assert::IsTrue(it->Prediction());
                    Assert::IsFalse(it->LastPeal());
                    Assert::IsTrue(it->DateOfMilestoneUsingAllTimeAverage().Valid());
                    Assert::IsTrue(it->DateOfMilestoneUsing6MonthAverage().Valid());
                    Assert::IsTrue(it->DateOfMilestoneUsing12MonthAverage().Valid());
                    break;
                }
                ++it;
            }
        }
    };
}
