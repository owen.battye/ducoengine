#include "CppUnitTest.h"
#include <RingingDatabase.h>
#include <Association.h>
#include <AssociationDatabase.h>
#include <Peal.h>
#include <PealDatabase.h>
#include "ToString.h"
#include <StatisticFilters.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(Association_UnitTests)
    {
    public:
        TEST_METHOD(Association_CopyConstructor)
        {
            Association theAssociation(1, L"South northants society", L"Dont do duvets");
            Association copiedAssociation(theAssociation);

            Assert::AreEqual(ObjectId(1), copiedAssociation.Id());
            Assert::AreEqual(L"South northants society", copiedAssociation.Name().c_str());
            Assert::AreEqual(L"Dont do duvets", copiedAssociation.Notes().c_str());
            Assert::AreEqual(L"", copiedAssociation.PealbaseLink().c_str());
        }

        TEST_METHOD(Association_CheckPealCounts)
        {
            RingingDatabase database;
            Association associationOne(1, L"Association1", L"");
            Association associationTwo(2, L"Association2", L"");
            Association associationThree(3, L"Association3", L"");
            database.AssociationsDatabase().AddObject(associationOne);
            database.AssociationsDatabase().AddObject(associationTwo);
            database.AssociationsDatabase().AddObject(associationThree);

            Peal pealOne;
            pealOne.SetAssociationId(associationOne.Id());
            database.PealsDatabase().AddObject(pealOne);
            Peal pealTwo;
            pealTwo.SetAssociationId(associationOne.Id());
            database.PealsDatabase().AddObject(pealTwo);
            Peal pealThree;
            pealThree.SetAssociationId(associationTwo.Id());
            database.PealsDatabase().AddObject(pealThree);

            Duco::StatisticFilters filters(database);
            Assert::AreEqual((size_t)3, database.NumberOfAssociations());
            Assert::AreEqual((size_t)3, database.PerformanceInfo(filters).TotalPeals());

            std::map<Duco::ObjectId, Duco::PealLengthInfo> sortedPealCounts;
            database.PealsDatabase().GetAssociationsPealCount(filters, sortedPealCounts, false);

            Assert::AreEqual((size_t)3, sortedPealCounts.size());
            Assert::AreEqual((size_t)2, sortedPealCounts[associationOne.Id()].TotalPeals());
            Assert::AreEqual((size_t)1, sortedPealCounts[associationTwo.Id()].TotalPeals());
            Assert::AreEqual((size_t)0, sortedPealCounts[associationThree.Id()].TotalPeals());

            filters.SetAssociation(true, associationOne.Id());
            Assert::AreEqual((size_t)2, database.PealsDatabase().PerformanceInfo(filters).TotalPeals());
            filters.SetAssociation(true, associationTwo.Id());
            Assert::AreEqual((size_t)1, database.PealsDatabase().PerformanceInfo(filters).TotalPeals());
            filters.SetAssociation(true, associationThree.Id());
            Assert::AreEqual((size_t)0, database.PealsDatabase().PerformanceInfo(filters).TotalPeals());
        }

        TEST_METHOD(Association_CheckPealCounts_WithLinks)
        {
            RingingDatabase database;
            Association associationOne(1, L"Association1", L"");
            database.AssociationsDatabase().AddObject(associationOne);
            Association associationTwo(2, L"Association2", L"");
            database.AssociationsDatabase().AddObject(associationTwo);
            Association associationThree(3, L"Association3", L"");
            database.AssociationsDatabase().AddObject(associationThree);
            Association associationFour(4, L"Association1&2", L"");
            associationFour.AddAssociationLink(associationOne.Id());
            associationFour.AddAssociationLink(associationTwo.Id());
            database.AssociationsDatabase().AddObject(associationFour);

            Peal pealOne;
            pealOne.SetChanges(5000);
            pealOne.SetAssociationId(associationOne.Id());
            database.PealsDatabase().AddObject(pealOne);
            Peal pealTwo;
            pealTwo.SetChanges(5001);
            pealTwo.SetAssociationId(associationTwo.Id());
            database.PealsDatabase().AddObject(pealTwo);
            Peal pealThree;
            pealThree.SetChanges(5002);
            pealThree.SetAssociationId(associationFour.Id());
            database.PealsDatabase().AddObject(pealThree);

            Duco::StatisticFilters filters(database);
            Assert::AreEqual((size_t)4, database.NumberOfAssociations());
            Assert::AreEqual((size_t)3, database.PerformanceInfo(filters).TotalPeals());

            std::map<Duco::ObjectId, Duco::PealLengthInfo> sortedPealCounts;
            database.PealsDatabase().GetAssociationsPealCount(filters, sortedPealCounts, true);

            Assert::AreEqual((size_t)2, sortedPealCounts[associationOne.Id()].TotalPeals());
            Assert::AreEqual((size_t)2, sortedPealCounts[associationTwo.Id()].TotalPeals());
            Assert::AreEqual((size_t)0, sortedPealCounts[associationThree.Id()].TotalPeals());
            Assert::AreEqual((size_t)3, sortedPealCounts.size());
            Assert::IsTrue(sortedPealCounts.find(associationFour.Id()) == sortedPealCounts.end());

            Assert::AreEqual((size_t)10002, sortedPealCounts[associationOne.Id()].TotalChanges());
            Assert::AreEqual((size_t)10003, sortedPealCounts[associationTwo.Id()].TotalChanges());
            Assert::AreEqual((size_t)0, sortedPealCounts[associationThree.Id()].TotalChanges());
        }

        TEST_METHOD(Association_CheckGeneralBehaviour)
        {
            Association theAssociation(1, L"South northants society", L"Dont do duvets");
            Assert::AreEqual(L"South northants society", theAssociation.Name().c_str());
            Assert::AreEqual(TObjectType::EAssociationOrSociety, theAssociation.ObjectType());

            RingingDatabase database;
            Assert::IsTrue(theAssociation.Valid(database, false, true));
            Assert::IsFalse(theAssociation.Valid(database, true, true));
            Assert::AreEqual(L"", theAssociation.ErrorString(database.Settings(), false).c_str());

            theAssociation.SetPealbaseLink(L"12345");
            Assert::IsTrue(theAssociation.Valid(database, false, true));
            Assert::IsTrue(theAssociation.Valid(database, true, true));
            Assert::AreEqual(L"", theAssociation.ErrorString(database.Settings(), true).c_str());
        }

        TEST_METHOD(Association_CheckUppercase)
        {
            Association theAssociation(1, L"South northants society", L"Dont do duvets");
            Assert::AreEqual(L"South northants society", theAssociation.Name().c_str());

            theAssociation.UppercaseAssociation();
            Assert::AreEqual(L"SOUTH NORTHANTS SOCIETY", theAssociation.Name().c_str());

        }

        TEST_METHOD(Association_CheckCopyAndCompare)
        {
            Association theAssociation(1, L"South northants society", L"Dont do duvets");
            Association copiedAssociation(theAssociation);
            Assert::IsTrue(theAssociation == copiedAssociation);
            Assert::IsFalse(theAssociation != copiedAssociation);

            copiedAssociation.SetName(L"Different");
            Assert::IsFalse(theAssociation == copiedAssociation);
            Assert::IsTrue(theAssociation != copiedAssociation);
        }

        TEST_METHOD(Association_SuggestAssociation)
        {
            AssociationDatabase database;
            Association theAssociation(1, L"The Dordrecht Society of Change ringers", L"");
            Duco::ObjectId societyId = database.AddObject(theAssociation);

            Assert::AreEqual((size_t)1, database.NumberOfObjects());
            Assert::AreEqual(societyId, database.SuggestAssociationOrCreate(L"Dordrecht Society of Change ringers"));
            Assert::AreEqual(societyId, database.SuggestAssociationOrCreate(L"DORDRECHT SOCIETY"));
            Assert::AreEqual(societyId, database.SuggestAssociationOrCreate(L"The Dordrecht Society of Change ringers"));
            Assert::AreEqual((size_t)1, database.NumberOfObjects());
        }

        TEST_METHOD(Association_SuggestAssociation_TwoSimilarAssociations)
        {
            AssociationDatabase database;
            Association lacrAssociation(KNoId, L"Lancashire Association", L"");
            Duco::ObjectId lacrId = database.AddObject(lacrAssociation);
            Assert::IsTrue(lacrId.ValidId());

            Association lacrAndASYCAssociation(KNoId, L"ANCIENT SOCIETY OF COLLEGE YOUTHS AND THE LANCASHIRE ASSOCIATION", L"");
            Duco::ObjectId lacrAndASYCId = database.AddObject(lacrAndASYCAssociation);
            Assert::IsTrue(lacrAndASYCId.ValidId());

            Assert::AreEqual((size_t)2, database.NumberOfObjects());
            Assert::AreEqual(lacrId, database.SuggestAssociationOrCreate(L"Lancashire Association"));
            Assert::AreNotEqual(lacrId, database.SuggestAssociationOrCreate(L"The Lancashire Association"));
            Assert::AreEqual((size_t)2, database.NumberOfObjects());
        }

        TEST_METHOD(Association_SuggestAssociation_CreateNew)
        {
            AssociationDatabase database;
            database.CreateAssociation(L"Association1");
            database.CreateAssociation(L"Association2");
            
            Assert::AreEqual((size_t)2, database.NumberOfObjects());
            Assert::AreEqual(Duco::ObjectId(3), database.SuggestAssociationOrCreate(L"DORDRECHT SOCIETY"));
            Assert::AreEqual((size_t)3, database.NumberOfObjects());
        }

        TEST_METHOD(Association_FindAssociation_BySubstring)
        {
            AssociationDatabase database;
            Association theAssociation(1, L"The Dordrecht Society of Change ringers", L"");
            Duco::ObjectId societyId = database.AddObject(theAssociation);

            Assert::AreEqual((size_t)1, database.NumberOfObjects());
            Assert::AreEqual(societyId, database.SuggestAssociation(L"Dordrecht Society of Change ringers"));
        }

        TEST_METHOD(Association_FindAssociation_BySubstring_OtherWayRound)
        {
            AssociationDatabase database;
            Association theAssociation(1, L"Dordrecht Society of Change ringers", L"");
            Duco::ObjectId societyId = database.AddObject(theAssociation);

            Assert::AreEqual((size_t)1, database.NumberOfObjects());
            Assert::AreEqual(societyId, database.SuggestAssociation(L"The Dordrecht Society of Change ringers"));
        }

        TEST_METHOD(Association_FindBlankAssociation)
        {
            AssociationDatabase database;
            Duco::ObjectId societyId = database.CreateAssociation(L"The Dordrecht Society of Change ringers");
            Duco::ObjectId blankSocietyId = database.CreateAssociation(L"");

            Assert::AreEqual((size_t)2, database.NumberOfObjects());
            Assert::AreEqual(societyId, database.SuggestAssociation(L"Dordrecht Society of Change ringers"));
            Assert::AreEqual(blankSocietyId, database.SuggestAssociation(L""));
        }

        TEST_METHOD(Association_FindBlankAssociation_Missing)
        {
            AssociationDatabase database;
            Duco::ObjectId societyId = database.CreateAssociation(L"The Dordrecht Society of Change ringers");
            Duco::ObjectId blankSocietyId = database.CreateAssociation(L"");

            Assert::AreEqual((size_t)2, database.NumberOfObjects());
            Assert::IsFalse(database.SuggestAssociation(L"South Northamptonshire Society").ValidId());
        }

        TEST_METHOD(Association_FindAssociationName)
        {
            std::wstring associationName = L"The Dordrecht Society of Change ringers";
            AssociationDatabase database;
            Association theAssociation(1, associationName, L"");
            Duco::ObjectId societyId = database.AddObject(theAssociation);

            Assert::AreEqual((size_t)1, database.NumberOfObjects());
            Assert::AreEqual(associationName, database.FindAssociationName(societyId));
        }

        TEST_METHOD(Association_FindAssociationName_Missing)
        {
            AssociationDatabase database;
            Association theAssociation(1, L"The Dordrecht Society of Change ringers", L"");
            Duco::ObjectId societyId = database.AddObject(theAssociation);

            Assert::AreEqual((size_t)1, database.NumberOfObjects());
            Assert::AreEqual(L"Unknown association", database.FindAssociationName(2).c_str());
        }

        TEST_METHOD(Association_UppercaseAssociations)
        {
            AssociationDatabase database;
            Duco::ObjectId societyId1 = database.CreateAssociation(L"The Dordrecht Society of Change ringers");
            Duco::ObjectId societyId2 = database.CreateAssociation(L"DORDRECHT SOCIETY");

            float stepNumber= 0;
            Assert::IsTrue(database.UppercaseAssociations(NULL));
            Assert::AreEqual((size_t)2, database.NumberOfObjects());
            Assert::AreEqual(L"THE DORDRECHT SOCIETY OF CHANGE RINGERS", database.FindAssociationName(societyId1).c_str());
            Assert::AreEqual(L"DORDRECHT SOCIETY", database.FindAssociationName(societyId2).c_str());
        }

        TEST_METHOD(Association_FindDuplicateAssociationsReplacements)
        {
            AssociationDatabase database;
            Duco::ObjectId societyId1 = database.CreateAssociation(L"The Dordrecht Society of Change ringers");
            Duco::ObjectId societyId2 = database.CreateAssociation(L"Dordrecht Society of Change ringers");
            Duco::ObjectId societyId3 = database.CreateAssociation(L"Dordrecht Society");
            Duco::ObjectId societyId4 = database.CreateAssociation(L"Dordrecht Society");

            Assert::AreEqual((size_t)4, database.NumberOfObjects());

            std::map<Duco::ObjectId, Duco::ObjectId> objectIds;
            database.FindDuplicateIds(objectIds);

            Assert::AreEqual((size_t)1, objectIds.size());
            Assert::AreEqual(societyId3, objectIds[societyId4]);
        }

        TEST_METHOD(Association_RemoveDuplicates)
        {
            RingingDatabase database;
            Duco::ObjectId societyId1 = database.AssociationsDatabase().CreateAssociation(L"Association1");
            Duco::ObjectId societyId2 = database.AssociationsDatabase().CreateAssociation(L"Association2");
            Duco::ObjectId societyId1_Copy = database.AssociationsDatabase().CreateAssociation(L"Association1");

            Peal pealOne;
            pealOne.SetAssociationId(societyId1.Id());
            Duco::ObjectId pealOneId = database.PealsDatabase().AddObject(pealOne);
            Peal pealTwo;
            pealTwo.SetAssociationId(societyId2.Id());
            Duco::ObjectId pealTwoId = database.PealsDatabase().AddObject(pealTwo);
            Peal pealThree;
            pealThree.SetAssociationId(societyId1_Copy.Id());
            Duco::ObjectId pealThreeId = database.PealsDatabase().AddObject(pealThree);

            Duco::StatisticFilters filters(database);

            Assert::AreEqual((size_t)3, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)3, database.NumberOfAssociations());

            Assert::IsTrue(database.RemoveDuplicateAssociations());

            Assert::AreEqual(societyId1, database.PealsDatabase().FindPeal(pealOneId)->AssociationId());
            Assert::AreEqual(societyId2, database.PealsDatabase().FindPeal(pealTwoId)->AssociationId());
            Assert::AreEqual(societyId1, database.PealsDatabase().FindPeal(pealThreeId)->AssociationId());
            Assert::AreEqual((size_t)2, database.NumberOfAssociations());
            Assert::IsNotNull(database.AssociationsDatabase().FindAssociation(societyId1));
            Assert::IsNotNull(database.AssociationsDatabase().FindAssociation(societyId2));
            Assert::IsNull(database.AssociationsDatabase().FindAssociation(societyId1_Copy));
        }
    };
}
