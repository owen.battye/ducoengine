#include "CppUnitTest.h"
#include <Change.h>
#include "ToString.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(Change_UnitTests)
    {
    public:
        TEST_METHOD(Change_CheckGenerateRoundsOn6)
        {
            Change roundsOnSix(6);
            Assert::AreEqual(L"123456", roundsOnSix.Str(true, true).c_str());
            Assert::IsTrue(roundsOnSix.IsRounds());
            Assert::IsTrue(roundsOnSix.IsHome());
            Assert::IsFalse(roundsOnSix.IsBefore());
            Assert::AreEqual((unsigned int)6, roundsOnSix.CallingPosition());
            unsigned int treblePosition = 0;
            Assert::IsTrue(roundsOnSix.TreblePosition(treblePosition));
            Assert::AreEqual((unsigned int)1, treblePosition);
        }

        TEST_METHOD(Change_CheckGenerateRoundsOn12)
        {
            Change roundsOnTwelve(12);
            Assert::AreEqual(L"1234567890ET", roundsOnTwelve.Str(true, true).c_str());
            Assert::AreEqual(L"23456", roundsOnTwelve.Str(false, false).c_str());
            Assert::AreEqual(L"234567890ET", roundsOnTwelve.Str(true, false).c_str());
            Assert::AreEqual(L"123456", roundsOnTwelve.Str(false, true).c_str());
            Assert::IsTrue(roundsOnTwelve.IsRounds());
            Assert::IsTrue(roundsOnTwelve.IsHome());
            Assert::IsFalse(roundsOnTwelve.IsBefore());
            Assert::AreEqual((unsigned int)12, roundsOnTwelve.CallingPosition());
            unsigned int treblePosition = 0;
            Assert::IsTrue(roundsOnTwelve.TreblePosition(treblePosition));
            Assert::AreEqual((unsigned int)1, treblePosition);
        }

        TEST_METHOD(Change_CheckGenerateRoundsOn16)
        {
            Change roundsOnSixteen(16);
            Assert::AreEqual(L"1234567890ETABCD", roundsOnSixteen.Str(true, true).c_str());
            Assert::IsTrue(roundsOnSixteen.IsRounds());
            Assert::IsTrue(roundsOnSixteen.IsHome());
            Assert::IsFalse(roundsOnSixteen.IsBefore());
            Assert::AreEqual((unsigned int)16, roundsOnSixteen.CallingPosition());
            unsigned int treblePosition = 0;
            Assert::IsTrue(roundsOnSixteen.TreblePosition(treblePosition));
            Assert::AreEqual((unsigned int)1, treblePosition);
        }

        TEST_METHOD(Change_CheckGenerateRandomChangeOn8)
        {
            Change change(L"43218756");
            Assert::AreEqual(L"43218756", change.Str().c_str());
            Assert::IsFalse(change.IsRounds());
            Assert::IsFalse(change.IsHome());
            Assert::IsFalse(change.IsBefore());
            Assert::AreEqual((unsigned int)5, change.CallingPosition());
            unsigned int treblePosition = 0;
            Assert::IsTrue(change.TreblePosition(treblePosition));
            Assert::AreEqual((unsigned int)4, treblePosition);

        }

        TEST_METHOD(Change_CheckBefore)
        {
            Change change(L"18756423");
            Assert::AreEqual(L"8756423", change.Str().c_str());
            Assert::IsFalse(change.IsRounds());
            Assert::IsFalse(change.IsHome());
            Assert::IsTrue(change.IsBefore());
            Assert::AreEqual((unsigned int)2, change.CallingPosition());
            unsigned int treblePosition = 0;
            Assert::IsTrue(change.TreblePosition(treblePosition));
            Assert::AreEqual((unsigned int)1, treblePosition);

        }

        TEST_METHOD(Change_CheckBeforeAlt)
        {
            Change change(L"17856423");
            Assert::AreEqual(L"7856423", change.Str().c_str());
            Assert::IsFalse(change.IsRounds());
            Assert::IsFalse(change.IsHome());
            Assert::IsTrue(change.IsBefore());
            Assert::AreEqual((unsigned int)3, change.CallingPosition());
            unsigned int treblePosition = 0;
            Assert::IsTrue(change.TreblePosition(treblePosition));
            Assert::AreEqual((unsigned int)1, treblePosition);

            unsigned int invalidPosition = 0;
            Assert::IsFalse(change.Position(9, invalidPosition));
            Assert::AreEqual((unsigned int)-1, invalidPosition);
        }

        TEST_METHOD(Change_Operators)
        {
            Change changeOne(L"12345678");
            Change changeTwo(L"12345687");
            Change changeThree(L"1234568790");
            Assert::IsTrue(changeOne.operator==(changeOne));
            Assert::IsFalse(changeOne.operator==(changeTwo));
            Assert::IsFalse(changeOne.operator==(changeThree));
            Assert::IsFalse(changeOne.operator!=(changeOne));
            Assert::IsTrue(changeOne.operator!=(changeTwo));
            Assert::IsTrue(changeOne.operator!=(changeThree));

            std::vector<unsigned int> affectedBells;
            Assert::IsFalse(changeOne.Differences(changeOne, affectedBells));
            Assert::AreEqual((size_t)0, affectedBells.size());
            Assert::IsTrue(changeOne.Differences(changeTwo, affectedBells));
            Assert::AreEqual((size_t)2, affectedBells.size());
            std::vector<unsigned int> expectedAffectedBells = { 7,8 };
            Assert::IsTrue(expectedAffectedBells == affectedBells);
        }

        TEST_METHOD(Change_InvalidChange)
        {
            auto func = [&] {
                Change changeOne(L"11345678");
            };
            Assert::ExpectException<std::invalid_argument>(func, L"Invalid change");
        }

        TEST_METHOD(Change_PartialConstructors)
        {
            Assert::AreEqual(Change(L"23456"), Change(L"123456"));
            Assert::AreEqual(Change(L"53246"), Change(L"153246"));
            Assert::AreEqual(Change(L"53246", 8), Change(L"15324678"));
        }

        TEST_METHOD(Change_EqualOperator)
        {
            Assert::IsFalse(Change(L"12345678") == Change(L"23456"));
            Assert::IsTrue(Change(L"12345678") == Change(L"23456", 8));
            Assert::IsTrue(Change(L"12345678") == Change(L"123456", 8));
            Assert::IsTrue(Change(L"1234567890ET") == Change(L"123456", 12));
        }
    };
}
