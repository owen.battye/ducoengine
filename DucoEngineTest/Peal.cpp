#include "CppUnitTest.h"
#include <Method.h>
#include <MethodDatabase.h>
#include <Peal.h>
#include <PealDatabase.h>
#include <Ringer.h>
#include <RingerDatabase.h>
#include <TowerDatabase.h>
#include <TowerDatabase.h>
#include <RingingDatabase.h>
#include <Tower.h>
#include <DatabaseSettings.h>
#include "ToString.h"
#include <StatisticFilters.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(Peal_UnitTests)
    {
    public:
        TEST_METHOD(Peal_DatabaseReader_SinglePeal)
        {
            RingingDatabase database;
            Duco::StatisticFilters filters(database);
            unsigned int databaseVersion = 0;
            database.Internalise("Single_peal.duc", NULL, databaseVersion);

            // Check counts of all objects
            Assert::AreEqual((size_t)1, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)10, database.NumberOfRingers());
            Assert::AreEqual((size_t)2, database.NumberOfTowers());
            size_t noOfLinkedTowers = 0;
            Assert::AreEqual((size_t)1, database.NumberOfActiveTowers(noOfLinkedTowers));
            Assert::AreEqual((size_t)1, noOfLinkedTowers);
            Assert::AreEqual((size_t)1, database.NumberOfRemovedTowers());
            Assert::AreEqual((size_t)1, database.NumberOfMethods());
            Assert::AreEqual((size_t)0, database.NumberOfMethodSeries());
            Assert::AreEqual((size_t)0, database.NumberOfCompositions());
            Assert::AreEqual((size_t)1, database.NumberOfAssociations());

            //Check number of bells and ringers
            const Peal* const thePeal = database.PealsDatabase().FindPeal(1);

            Assert::AreEqual((unsigned int)10, thePeal->NoOfBellsInTower(database.TowersDatabase()));
            bool dualOrder = false;
            Assert::AreEqual((unsigned int)9, thePeal->NoOfBellsInMethod(database.MethodsDatabase(), dualOrder));
            Assert::IsFalse(dualOrder);
            Assert::AreEqual((unsigned int)10, thePeal->NoOfRingers());
            Assert::AreEqual((unsigned int)10, thePeal->NoOfRingers(true));
            Assert::AreEqual((unsigned int)10, thePeal->NoOfBellsRung(database));

        }

        TEST_METHOD(Peal_NoRingers_NoOfBellsCorrect)
        {
            RingingDatabase database;
            database.MethodsDatabase().AddMethod(L"Bristol", L"Surprise", 10, false);
            Peal thePeal;
            thePeal.SetMethodId(1);

            Assert::AreEqual((unsigned int)0, thePeal.NoOfBellsInTower(database.TowersDatabase()));
            bool dualOrder = false;
            Assert::AreEqual((unsigned int)10, thePeal.NoOfBellsInMethod(database.MethodsDatabase(), dualOrder));
            Assert::IsFalse(dualOrder);
            Assert::AreEqual((unsigned int)0, thePeal.NoOfRingers());
            Assert::AreEqual((unsigned int)0, thePeal.NoOfRingers(true));
            Assert::AreEqual((unsigned int)10, thePeal.NoOfBellsRung(database));
        }

        TEST_METHOD(Peal_AllRingers_WithMethod_NoOfBellsCorrect)
        {
            RingingDatabase database;
            database.RingersDatabase().AddRinger(L"One One");
            database.RingersDatabase().AddRinger(L"Two One");
            database.RingersDatabase().AddRinger(L"Three One");
            database.RingersDatabase().AddRinger(L"Four One");
            database.RingersDatabase().AddRinger(L"Five One");
            database.RingersDatabase().AddRinger(L"Six One");
            database.RingersDatabase().AddRinger(L"Seven One");
            database.RingersDatabase().AddRinger(L"Eight One");
            database.RingersDatabase().AddRinger(L"Nine One");
            database.RingersDatabase().AddRinger(L"Ten One");

            database.MethodsDatabase().AddMethod(L"Bristol", L"Surprise", 10, false);
            Peal thePeal;
            thePeal.SetMethodId(1);
            thePeal.SetRingerId(1, 1, false);
            thePeal.SetRingerId(2, 2, false);
            thePeal.SetRingerId(3, 3, false);
            thePeal.SetRingerId(4, 4, false);
            thePeal.SetRingerId(5, 5, false);
            thePeal.SetRingerId(6, 6, false);
            thePeal.SetRingerId(7, 7, false);
            thePeal.SetRingerId(8, 8, false);
            thePeal.SetRingerId(9, 9, false);
            thePeal.SetRingerId(10, 10, false);

            Assert::AreEqual((unsigned int)0, thePeal.NoOfBellsInTower(database.TowersDatabase()));
            bool dualOrder = false;
            Assert::AreEqual((unsigned int)10, thePeal.NoOfBellsInMethod(database.MethodsDatabase(), dualOrder));
            Assert::IsFalse(dualOrder);
            Assert::AreEqual((unsigned int)10, thePeal.NoOfRingers());
            Assert::AreEqual((unsigned int)10, thePeal.NoOfRingers(true));
            Assert::AreEqual((unsigned int)10, thePeal.NoOfBellsRung(database));
        }

        TEST_METHOD(Peal_SomeRingers_WithMethod_NoOfBellsCorrect)
        {
            RingingDatabase database;
            database.RingersDatabase().AddRinger(L"One One");
            database.RingersDatabase().AddRinger(L"Two One");
            database.RingersDatabase().AddRinger(L"Three One");
            database.RingersDatabase().AddRinger(L"Four One");
            database.RingersDatabase().AddRinger(L"Five One");
            database.RingersDatabase().AddRinger(L"Six One");
            database.RingersDatabase().AddRinger(L"Seven One");
            database.RingersDatabase().AddRinger(L"Eight One");
            database.RingersDatabase().AddRinger(L"Nine One");
            database.RingersDatabase().AddRinger(L"Ten One");

            database.MethodsDatabase().AddMethod(L"Bristol", L"Surprise", 10, false);
            Peal thePeal;
            thePeal.SetMethodId(1);
            thePeal.SetRingerId(1, 1, false);
            thePeal.SetRingerId(2, 2, false);
            thePeal.SetRingerId(3, 3, false);
            thePeal.SetRingerId(4, 4, false);
            thePeal.SetRingerId(5, 5, false);
            thePeal.SetRingerId(6, 6, false);
            thePeal.SetRingerId(8, 8, false);
            thePeal.SetRingerId(9, 9, false);
            thePeal.SetRingerId(10, 10, false);

            Assert::AreEqual((unsigned int)0, thePeal.NoOfBellsInTower(database.TowersDatabase()));
            bool dualOrder = false;
            Assert::AreEqual((unsigned int)10, thePeal.NoOfBellsInMethod(database.MethodsDatabase(), dualOrder));
            Assert::IsFalse(dualOrder);
            Assert::AreEqual((unsigned int)9, thePeal.NoOfRingers());
            Assert::AreEqual((unsigned int)9, thePeal.NoOfRingers(true));
            Assert::AreEqual((unsigned int)10, thePeal.NoOfBellsRung(database));
        }

        TEST_METHOD(Peal_HandBellPeal_NoOfBellsRungCorrect)
        {
            RingingDatabase database;
            database.RingersDatabase().AddRinger(L"One One");
            database.RingersDatabase().AddRinger(L"Two One");
            database.RingersDatabase().AddRinger(L"Three One");
            database.RingersDatabase().AddRinger(L"Four One");
            database.RingersDatabase().AddRinger(L"Five One");

            database.MethodsDatabase().AddMethod(L"Bristol", L"Surprise", 10, false);
            Peal thePeal;
            thePeal.SetMethodId(1);
            thePeal.SetHandbell(true);
            thePeal.SetRingerId(1, 1, false);
            thePeal.SetRingerId(2, 2, false);
            thePeal.SetRingerId(3, 3, false);
            thePeal.SetRingerId(4, 4, false);
            thePeal.SetRingerId(5, 5, false);

            Assert::AreEqual((unsigned int)0, thePeal.NoOfBellsInTower(database.TowersDatabase()));
            bool dualOrder = false;
            Assert::AreEqual((unsigned int)10, thePeal.NoOfBellsInMethod(database.MethodsDatabase(), dualOrder));
            Assert::IsFalse(dualOrder);
            Assert::AreEqual((unsigned int)5, thePeal.NoOfRingers());
            Assert::AreEqual((unsigned int)5, thePeal.NoOfRingers(true));
            Assert::AreEqual((unsigned int)10, thePeal.NoOfBellsRung(database));
        }

        TEST_METHOD(Peal_ValidWebSiteLinks)
        {
            DatabaseSettings settings;
            std::wstring bellboardId = L"1335142";

            Peal thePeal;
            Assert::IsFalse(thePeal.ValidWebsiteLink());
            Assert::IsFalse(thePeal.ValidBellBoardId());
            Assert::AreEqual(L"", thePeal.WebsiteLink(settings).c_str());

            thePeal.SetBellBoardId(bellboardId);
            settings.SetPreferredOnlineWebsite(DatabaseSettings::TPreferredViewWebsite::EViewOnBellBoard);
            Assert::IsTrue(thePeal.ValidWebsiteLink());
            Assert::IsTrue(thePeal.ValidBellBoardId());
            Assert::IsTrue(thePeal.WebsiteLink(settings).find(bellboardId) != std::wstring::npos);

            thePeal.SetBellBoardId(L"");
            settings.SetPreferredOnlineWebsite(DatabaseSettings::TPreferredViewWebsite::EViewOnBellBoard);
            Assert::IsFalse(thePeal.ValidWebsiteLink());
            Assert::IsFalse(thePeal.ValidBellBoardId());

            thePeal.SetBellBoardId(bellboardId);
            Assert::IsTrue(thePeal.WebsiteLink(settings).find(bellboardId) != std::wstring::npos);
        }

        TEST_METHOD(Peal_SetRingingWorldReference)
        {
            Peal thePeal;
            std::wstring rwRef = L"5667.1185";

            Assert::AreEqual(L"", thePeal.RingingWorldReference().c_str());
            thePeal.SetRingingWorldReference(rwRef);
            Assert::AreEqual(rwRef, thePeal.RingingWorldReference());
        }

        TEST_METHOD(Peal_BellsOnTowerAndMethodDifferent_NoRingers)
        {
            RingingDatabase database;
            Duco::ObjectId methodId = database.MethodsDatabase().AddMethod(L"Bristol", L"Surprise", 10, false);

            Tower newTower(KNoId, L"St Werburgh", L"Warburton", L"Greater Manchester", 8);
            Duco::ObjectId correctRingId = newTower.AddRing(8, L"", L"");
            Duco::ObjectId towerId = database.TowersDatabase().AddObject(newTower);
            Peal thePeal;
            thePeal.SetMethodId(methodId);
            thePeal.SetTowerId(towerId);
            thePeal.SetRingId(correctRingId);
            
            Assert::AreEqual((unsigned int)8, thePeal.NoOfBellsInTower(database.TowersDatabase()));
            bool dualOrder = false;
            Assert::AreEqual((unsigned int)10, thePeal.NoOfBellsInMethod(database.MethodsDatabase(), dualOrder));
            Assert::IsFalse(dualOrder);
            Assert::AreEqual((unsigned int)8, thePeal.NoOfBellsRung(database));

            Assert::IsFalse(thePeal.Valid(database, false, true));
        }

        TEST_METHOD(Peal_NoOfBellsRung_OneRingerDoubleHanding)
        {
            RingingDatabase database;
            Duco::ObjectId methodId = database.MethodsDatabase().AddMethod(L"Bristol", L"Surprise", 6, false);
            Tower newTower(KNoId, L"St Werburgh", L"Warburton", L"Greater Manchester", 6);
            Duco::ObjectId correctRingId = newTower.AddRing(6, L"", L"");
            Duco::ObjectId towerId = database.TowersDatabase().AddObject(newTower);
            Peal thePeal;
            thePeal.SetMethodId(methodId);
            thePeal.SetTowerId(towerId);
            thePeal.SetRingId(correctRingId);
            thePeal.SetDoubleHanded(true);
            thePeal.SetRingerId(1, 1, false);
            thePeal.SetRingerId(2, 2, false);
            thePeal.SetRingerId(3, 3, false);
            thePeal.SetRingerId(4, 4, false);
            thePeal.SetRingerId(5, 5, false);
            thePeal.SetRingerId(6, 5, false);

            Assert::AreEqual((unsigned int)6, thePeal.NoOfBellsInTower(database.TowersDatabase()));
            bool dualOrder = false;
            Assert::AreEqual((unsigned int)6, thePeal.NoOfBellsInMethod(database.MethodsDatabase(), dualOrder));
            Assert::IsFalse(dualOrder);
            Assert::AreEqual((unsigned int)5, thePeal.NoOfRingers(true));

            Assert::AreEqual((unsigned int)6, thePeal.NoOfBellsRung(database));
            Assert::IsTrue(thePeal.ValidPealRingers(database, false, true));
            Assert::AreEqual(L"", thePeal.ErrorString(database.Settings(), false).c_str());
        }

        TEST_METHOD(Peal_BellsOnTowerMethodAndRingersDifferent)
        {
            RingingDatabase database;
            Duco::ObjectId methodId = database.MethodsDatabase().AddMethod(L"Bristol", L"Surprise", 10, false);

            Tower newTower(KNoId, L"St Werburgh", L"Warburton", L"Greater Manchester", 8);
            Duco::ObjectId correctRingId = newTower.AddRing(8, L"", L"");
            Duco::ObjectId towerId = database.TowersDatabase().AddObject(newTower);
            Peal thePeal;
/*            thePeal.SetAssociationId(1);
            thePeal.SetChanges(5000);*/
            thePeal.SetMethodId(methodId);
            thePeal.SetTowerId(towerId);
            thePeal.SetRingId(correctRingId);
            thePeal.SetRingerId(1, 1, false);
            thePeal.SetRingerId(2, 2, false);
            thePeal.SetRingerId(3, 3, false);
            thePeal.SetRingerId(4, 4, false);
            thePeal.SetRingerId(5, 5, false);
            thePeal.SetRingerId(6, 6, false);
            thePeal.SetRingerId(7, 7, false);
            thePeal.SetRingerId(8, 8, false);
            thePeal.SetRingerId(9, 9, false);
            thePeal.SetRingerId(10, 10, false);
            thePeal.SetRingerId(11, 11, false);
            thePeal.SetRingerId(12, 12, false);

            Assert::AreEqual((unsigned int)8, thePeal.NoOfBellsInTower(database.TowersDatabase()));
            bool dualOrder = false;
            Assert::AreEqual((unsigned int)10, thePeal.NoOfBellsInMethod(database.MethodsDatabase(), dualOrder));
            Assert::IsFalse(dualOrder);
            Assert::AreEqual((unsigned int)12, thePeal.NoOfRingers(true));

            Assert::AreEqual((unsigned int)8, thePeal.NoOfBellsRung(database));
            Assert::IsFalse(thePeal.ValidPealRingers(database, false, true));

            Assert::AreEqual(L"Number of ringers doesn't match the number of bells", thePeal.ErrorString(database.Settings(), false).c_str());
        }

        TEST_METHOD(Peal_Invalid_TwoRingersMissing)
        {
            RingingDatabase database;
            Duco::ObjectId methodId = database.MethodsDatabase().AddMethod(L"Bristol", L"Surprise", 10, false);

            Tower newTower(KNoId, L"St Werburgh", L"Warburton", L"Greater Manchester", 10);
            Duco::ObjectId correctRingId = newTower.AddRing(10, L"", L"");
            Duco::ObjectId towerId = database.TowersDatabase().AddObject(newTower);
            Peal thePeal;
            thePeal.SetAssociationId(1);
            thePeal.SetChanges(5000);
            thePeal.SetMethodId(methodId);
            thePeal.SetTowerId(towerId);
            thePeal.SetRingId(correctRingId);
            thePeal.SetRingerId(1, 1, false);
            thePeal.SetRingerId(2, 2, false);
            thePeal.SetRingerId(3, 3, false);
            //thePeal.SetRingerId(4, 4, false);
            thePeal.SetRingerId(5, 5, false);
            thePeal.SetRingerId(6, 6, false);
            thePeal.SetRingerId(7, 7, false);
            thePeal.SetRingerId(8, 8, false);
            //thePeal.SetRingerId(9, 9, false);
            thePeal.SetRingerId(10, 10, false);

            Assert::AreEqual((unsigned int)10, thePeal.NoOfBellsInTower(database.TowersDatabase()));
            bool dualOrder = false;
            Assert::AreEqual((unsigned int)10, thePeal.NoOfBellsInMethod(database.MethodsDatabase(), dualOrder));
            Assert::IsFalse(dualOrder);
            Assert::AreEqual((unsigned int)8, thePeal.NoOfRingers(true));

            Assert::AreEqual((unsigned int)10, thePeal.NoOfBellsRung(database));

            Assert::IsFalse(thePeal.ValidPealRingers(database, false, true));
            Assert::AreEqual(L"Ringers incomplete", thePeal.ErrorString(database.Settings(), false).c_str());
        }

        TEST_METHOD(Peal_Invalid_TwoExtraRingers)
        {
            RingingDatabase database;
            Duco::ObjectId methodId = database.MethodsDatabase().AddMethod(L"Bristol", L"Surprise", 10, false);

            Tower newTower(KNoId, L"St Werburgh", L"Warburton", L"Greater Manchester", 10);
            Duco::ObjectId correctRingId = newTower.AddRing(10, L"", L"");
            Duco::ObjectId towerId = database.TowersDatabase().AddObject(newTower);
            Peal thePeal;
            thePeal.SetAssociationId(1);
            thePeal.SetChanges(5000);
            thePeal.SetMethodId(methodId);
            thePeal.SetTowerId(towerId);
            thePeal.SetRingId(correctRingId);
            thePeal.SetRingerId(1, 1, false);
            thePeal.SetRingerId(2, 2, false);
            thePeal.SetRingerId(3, 3, false);
            thePeal.SetRingerId(4, 4, false);
            thePeal.SetRingerId(5, 5, false);
            thePeal.SetRingerId(6, 6, false);
            thePeal.SetRingerId(7, 7, false);
            thePeal.SetRingerId(8, 8, false);
            thePeal.SetRingerId(9, 9, false);
            thePeal.SetRingerId(10, 10, false);
            thePeal.SetRingerId(11, 11, false);
            thePeal.SetRingerId(12, 12, false);

            Assert::AreEqual((unsigned int)10, thePeal.NoOfBellsInTower(database.TowersDatabase()));
            bool dualOrder = false;
            Assert::AreEqual((unsigned int)10, thePeal.NoOfBellsInMethod(database.MethodsDatabase(), dualOrder));
            Assert::IsFalse(dualOrder);
            Assert::AreEqual((unsigned int)12, thePeal.NoOfRingers(true));

            Assert::AreEqual((unsigned int)10, thePeal.NoOfBellsRung(database));

            Assert::IsFalse(thePeal.ValidPealRingers(database, false, true));
            Assert::AreEqual(L"Ringers incomplete", thePeal.ErrorString(database.Settings(), false).c_str());
        }

        TEST_METHOD(Peal_BellsRung_NoDetailsAtAll)
        {
            RingingDatabase database;
            Peal thePeal;
            Assert::AreEqual((unsigned int)0, thePeal.NoOfBellsInTower(database.TowersDatabase()));
            bool dualOrder = false;
            Assert::AreEqual((unsigned int)0, thePeal.NoOfBellsInMethod(database.MethodsDatabase(), dualOrder));
            Assert::IsFalse(dualOrder);
            Assert::AreEqual((unsigned int)0, thePeal.NoOfRingers(true));
            Assert::AreEqual((unsigned int)0, thePeal.NoOfBellsRung(database));
        }

        TEST_METHOD(Peal_ConductorName)
        {
            RingingDatabase database;
            std::wstring jenniePaulName = L"Paul, Jennie";
            std::wstring jennieHigsonName = L"Higson, Jennie";
            Duco::ObjectId jennieId = 1;
            Ringer jennieHigson(jennieId, L"Jennie", L"Paul");
            Assert::AreEqual(jennieId, database.RingersDatabase().AddObject(jennieHigson));
            database.RingersDatabase().AddRinger(L"Two One");
            database.RingersDatabase().AddRinger(L"Three One");
            database.RingersDatabase().AddRinger(L"Four One");
            database.RingersDatabase().AddRinger(L"Five One");
            database.RingersDatabase().AddRinger(L"Six One");
            Peal thePeal;
            thePeal.SetDate(PerformanceDate());
            thePeal.SetRingerId(jennieId, 1, false);
            thePeal.SetRingerId(2, 2, false);
            thePeal.SetRingerId(3, 3, false);
            thePeal.SetRingerId(4, 4, false);
            thePeal.SetRingerId(5, 5, false);
            thePeal.SetRingerId(6, 6, false);

            Assert::AreEqual(L"", thePeal.ConductorName(database).c_str());
            Assert::AreEqual(jenniePaulName, thePeal.RingerName(1, false, database));

            thePeal.SetConductorIdFromBell(1, true, false);
            Assert::AreEqual(jenniePaulName, thePeal.ConductorName(database));

            PerformanceDate lastMonth;
            lastMonth.RemoveMonths(1);

            jennieHigson.AddNameChange(L"Jennie", L"Higson", lastMonth);
            Assert::IsTrue(database.RingersDatabase().UpdateObject(jennieHigson));
            Assert::AreEqual(jennieHigsonName, thePeal.ConductorName(database));

            PerformanceDate twoMonthsAgo;
            twoMonthsAgo.RemoveMonths(2);

            thePeal.SetDate(twoMonthsAgo);
            Assert::AreEqual(jenniePaulName, thePeal.ConductorName(database));
        }

        TEST_METHOD(Peal_RingerName_NameChangeBeforePeal)
        {
            RingingDatabase database;
            std::wstring jenniePaulName = L"Paul, Jennie";
            std::wstring jennieHigsonName = L"Higson, Jennie";
            Duco::ObjectId jennieId = 1;
            Ringer jennieHigson(jennieId, L"Jennie", L"Paul");
            Assert::AreEqual(jennieId, database.RingersDatabase().AddObject(jennieHigson));
            database.RingersDatabase().AddRinger(L"Two One");
            database.RingersDatabase().AddRinger(L"Three One");
            database.RingersDatabase().AddRinger(L"Four One");
            database.RingersDatabase().AddRinger(L"Five One");
            database.RingersDatabase().AddRinger(L"Six One");
            Peal thePeal;
            thePeal.SetDate(PerformanceDate());
            thePeal.SetRingerId(jennieId, 1, false);
            thePeal.SetRingerId(2, 2, false);
            thePeal.SetRingerId(3, 3, false);
            thePeal.SetRingerId(4, 4, false);
            thePeal.SetRingerId(5, 5, false);
            thePeal.SetRingerId(6, 6, false);

            Assert::AreEqual(jenniePaulName, thePeal.RingerName(1, false, database));

            PerformanceDate lastMonth;
            lastMonth.RemoveMonths(1);

            jennieHigson.AddNameChange(L"Jennie", L"Higson", lastMonth);
            Assert::IsTrue(database.RingersDatabase().UpdateObject(jennieHigson));

            Assert::AreEqual(jennieHigsonName, thePeal.RingerName(1, false, database));
        }

        TEST_METHOD(Peal_RingerName_NameChangeAfterPeal)
        {
            RingingDatabase database;
            std::wstring jenniePaulName = L"Paul, Jennie";
            std::wstring jennieHigsonName = L"Higson, Jennie";
            Duco::ObjectId jennieId = 1;
            Ringer jennieHigson(jennieId, L"Jennie", L"Paul");
            Assert::AreEqual(jennieId, database.RingersDatabase().AddObject(jennieHigson));
            database.RingersDatabase().AddRinger(L"Two One");
            database.RingersDatabase().AddRinger(L"Three One");
            database.RingersDatabase().AddRinger(L"Four One");
            database.RingersDatabase().AddRinger(L"Five One");
            database.RingersDatabase().AddRinger(L"Six One");

            PerformanceDate pealDate (2010, 1 , 1);
            PerformanceDate monthAfter(2010, 2, 1);

            Peal thePeal;
            thePeal.SetDate(pealDate);
            thePeal.SetRingerId(jennieId, 1, false);
            thePeal.SetRingerId(2, 2, false);
            thePeal.SetRingerId(3, 3, false);
            thePeal.SetRingerId(4, 4, false);
            thePeal.SetRingerId(5, 5, false);
            thePeal.SetRingerId(6, 6, false);

            Assert::AreEqual(jenniePaulName, thePeal.RingerName(1, false, database));

            jennieHigson.AddNameChange(L"Jennie", L"Higson", monthAfter);
            Assert::IsTrue(database.RingersDatabase().UpdateObject(jennieHigson));

            Assert::AreEqual(jenniePaulName, thePeal.RingerName(1, false, database));
            Assert::IsFalse(thePeal.ContainsNonHumanRinger(database.RingersDatabase()));
        }

        TEST_METHOD(Peal_NonHuman)
        {
            RingingDatabase database;
            Duco::ObjectId robotId = 1;
            Ringer robot(robotId, L"Robot", L"Robot");
            robot.SetNonHuman(true);
            Assert::AreEqual(robotId, database.RingersDatabase().AddObject(robot));
            database.RingersDatabase().AddRinger(L"Two One");
            database.RingersDatabase().AddRinger(L"Three One");
            database.RingersDatabase().AddRinger(L"Four One");
            database.RingersDatabase().AddRinger(L"Five One");
            database.RingersDatabase().AddRinger(L"Six One");

            Peal thePeal;
            thePeal.SetRingerId(robotId, 1, false);
            thePeal.SetRingerId(2, 2, false);
            thePeal.SetRingerId(3, 3, false);
            thePeal.SetRingerId(4, 4, false);
            thePeal.SetRingerId(5, 5, false);
            thePeal.SetRingerId(6, 6, false);

            Assert::IsTrue(thePeal.ContainsNonHumanRinger(database.RingersDatabase()));
        }

        TEST_METHOD(Peal_RingingWorldLink_DoesntExist)
        {
            Peal thePeal;
            Assert::IsFalse(thePeal.ValidRingingWorldLink());
            Assert::AreEqual(L"", thePeal.RingingWorldIssue().c_str());
            Assert::AreEqual(L"", thePeal.RingingWorldPage().c_str());
        }

        TEST_METHOD(Peal_RingingWorldLink_WithInvalidchars)
        {
            Peal thePeal;
            thePeal.SetRingingWorldReference(L"19A65");
            Assert::IsFalse(thePeal.ValidRingingWorldLink());
            Assert::IsFalse(thePeal.ValidRingingWorldReferenceForUpload());
        }

        TEST_METHOD(Peal_RingingWorldLink_MissingPage)
        {
            Peal thePeal;
            thePeal.SetRingingWorldReference(L"1965");
            Assert::IsFalse(thePeal.ValidRingingWorldLink());
            Assert::AreEqual(L"1965", thePeal.RingingWorldIssue().c_str());
            Assert::AreEqual(L"", thePeal.RingingWorldPage().c_str());
            Assert::IsFalse(thePeal.ValidRingingWorldReferenceForUpload());
        }

        TEST_METHOD(Peal_RingingWorldLink_ValidLink)
        { // https://bb.ringingworld.co.uk/view.php?id=1582007
            RingingDatabase database;
            Peal thePeal;
            thePeal.SetDate(PerformanceDate(2023, 1, 15));
            thePeal.SetRingingWorldReference(L"5832.102");
            Assert::AreEqual(L"5832", thePeal.RingingWorldIssue().c_str());
            Assert::AreEqual(L"102", thePeal.RingingWorldPage().c_str());
            Assert::IsTrue(thePeal.ValidRingingWorldReferenceForUpload());
            Assert::IsTrue(thePeal.ValidRingingWorldLink());
            Assert::AreEqual(L"https://bb.ringingworld.co.uk/issues/2023/102", thePeal.RingingWorldLink(database.Settings()).c_str());
        }

        TEST_METHOD(Peal_RingingWorldLink_ValidLinkFromPreviousYear)
        {
            RingingDatabase database;
            Peal thePeal;
            thePeal.SetDate(PerformanceDate(2013, 12, 28));
            thePeal.SetRingingWorldReference(L"5360.0058");
            Assert::AreEqual(L"5360", thePeal.RingingWorldIssue().c_str());
            Assert::AreEqual(L"58", thePeal.RingingWorldPage().c_str());
            Assert::IsTrue(thePeal.ValidRingingWorldLink());
            Assert::IsTrue(thePeal.ValidRingingWorldReferenceForUpload());
            Assert::AreEqual(L"https://bb.ringingworld.co.uk/issues/2013/58", thePeal.RingingWorldLink(database.Settings()).c_str());
        }

        TEST_METHOD(Peal_RingingWorldLink_RemoveLeadingIssueZeros)
        {
            RingingDatabase database;
            Peal thePeal;
            thePeal.SetDate(PerformanceDate(2013, 12, 28));
            thePeal.SetRingingWorldReference(L"0360.0058");
            Assert::AreEqual(L"360", thePeal.RingingWorldIssue().c_str());
            Assert::AreEqual(L"58", thePeal.RingingWorldPage().c_str());
            Assert::IsTrue(thePeal.ValidRingingWorldLink());
            Assert::IsTrue(thePeal.ValidRingingWorldReferenceForUpload());
            Assert::AreEqual(L"https://bb.ringingworld.co.uk/issues/2013/58", thePeal.RingingWorldLink(database.Settings()).c_str());
        }

        TEST_METHOD(Peal_RingingWorldLink_NoRWIssueNumber)
        {
            RingingDatabase database;
            Peal thePeal;
            thePeal.SetDate(PerformanceDate(2013, 12, 28));
            thePeal.SetRingingWorldReference(L".0058");
            Assert::IsTrue(thePeal.ValidRingingWorldLink());
            Assert::IsFalse(thePeal.ValidRingingWorldReferenceForUpload());
            Assert::AreEqual(L"", thePeal.RingingWorldIssue().c_str());
            Assert::AreEqual(L"58", thePeal.RingingWorldPage().c_str());
            Assert::AreEqual(L"https://bb.ringingworld.co.uk/issues/2013/58", thePeal.RingingWorldLink(database.Settings()).c_str());
        }

        TEST_METHOD(Peal_HandbellPealInTower)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);
            Tower tower(KNoId, L"Tower", L"Tower", L"Tower", 8);
            tower.AddDefaultRing();
            Duco::ObjectId towerId = database.TowersDatabase().AddObject(tower);
            Duco::ObjectId methodId = database.MethodsDatabase().AddMethod(L"Cambridge Surprise Major");

            Peal thePeal;
            thePeal.SetChanges(5000);
            thePeal.SetTowerId(towerId);
            thePeal.SetRingId(0);
            thePeal.SetMethodId(methodId);
            thePeal.SetTime(PerformanceTime(180));
            thePeal.SetConductedType(TConductorType::ESilentAndNonConducted);
            thePeal.SetHandbell(true);

            Assert::IsFalse(thePeal.Valid(database, false, true));
            Assert::AreEqual(L"Ringers incomplete, Handbell setting in Tower and Peal do not match", thePeal.ErrorString(database.Settings(), false).c_str());
        }

        TEST_METHOD(Peal_TowerPealInhouse)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);
            Tower tower(KNoId, L"Tower", L"Tower", L"Tower", 8);
            tower.SetHandbell(true);
            tower.AddDefaultRing();
            Duco::ObjectId towerId = database.TowersDatabase().AddObject(tower);
            Duco::ObjectId methodId = database.MethodsDatabase().AddMethod(L"Cambridge Surprise Major");

            Peal thePeal;
            thePeal.SetChanges(5000);
            thePeal.SetRingId(0);
            thePeal.SetMethodId(methodId);
            thePeal.SetTime(PerformanceTime(180));
            thePeal.SetConductedType(TConductorType::ESilentAndNonConducted);
            thePeal.SetTowerId(towerId);

            Assert::IsFalse(thePeal.Valid(database, false, true));
            Assert::AreEqual(L"Ringers incomplete, Handbell setting in Tower and Peal do not match", thePeal.ErrorString(database.Settings(), false).c_str());
        }

        TEST_METHOD(Peal_MultiConductorWithOnlyOneConductor)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);
            Tower tower(KNoId, L"Tower", L"Tower", L"Tower", 8);
            tower.AddDefaultRing();
            Duco::ObjectId towerId = database.TowersDatabase().AddObject(tower);
            Duco::ObjectId methodId = database.MethodsDatabase().AddMethod(L"Cambridge Surprise Major");

            Peal thePeal;
            thePeal.SetChanges(5000);
            thePeal.SetRingId(0);
            thePeal.SetMethodId(methodId);
            thePeal.SetTime(PerformanceTime(180));
            thePeal.SetConductedType(TConductorType::EJointlyConducted);
            thePeal.SetTowerId(towerId);
            thePeal.SetConductorId(1);

            Assert::IsFalse(thePeal.Valid(database, false, true));
            Assert::AreEqual(L"Missing conductors, Ringers incomplete", thePeal.ErrorString(database.Settings(), false).c_str());
        }

        TEST_METHOD(Peal_FindBellNewsReference)
        {
            Peal thePeal;
            thePeal.SetNotes(L"Owen was here\nBell News Vol 25 page 52\nSo was Jesus");

            Assert::AreEqual(L"Bell News Vol 25 page 52", thePeal.FindReference().c_str());
        }


        TEST_METHOD(Peal_FindBellNewsReferenceInCapitals)
        {
            Peal thePeal;
            thePeal.SetNotes(L"Owen was here\nBELL NEWS: Vol 25 page 51\nSo was Jesus");

            Assert::AreEqual(L"BELL NEWS: Vol 25 page 51", thePeal.FindReference().c_str());
        }

        TEST_METHOD(Peal_FindBellNewsReferenceSingleLine)
        {
            Peal thePeal;
            thePeal.SetNotes(L"bell news Vol 25 page 3");

            Assert::AreEqual(L"bell news Vol 25 page 3", thePeal.FindReference().c_str());
        }

        TEST_METHOD(Peal_FindChurchBellsReferenceSingleLine)
        {
            Peal thePeal;
            thePeal.SetNotes(L"Church bells Vol 1 page 375");

            Assert::AreEqual(L"Church bells Vol 1 page 375", thePeal.FindReference().c_str());
        }

        TEST_METHOD(Peal_NoOfBellsRung_Inconsistant_SoUseRing)
        {
            RingingDatabase database;

            Method theMethod(KNoId, 8, L"Cambridge", L"Surprise", L"");
            Duco::ObjectId methodId = database.MethodsDatabase().AddObject(theMethod);

            Tower theTower (KNoId, L"Name", L"City", L"County", 8);
            Duco::ObjectId eightBellRingId = theTower.AddDefaultRing();
            Duco::ObjectId sixBellRingId = theTower.AddRing(6, L"", L"");
            Duco::ObjectId towerId = database.TowersDatabase().AddObject(theTower);

            Peal thePeal;
            thePeal.SetTowerId(towerId);
            thePeal.SetRingId(eightBellRingId);

            Assert::AreEqual(8u, thePeal.NoOfBellsRung(database));

            thePeal.SetRingId(sixBellRingId);
            Assert::AreEqual(6u, thePeal.NoOfBellsRung(database));

            thePeal.SetMethodId(methodId);
            Assert::AreEqual(6u, thePeal.NoOfBellsRung(database));

            thePeal.SetRingerId(1, 1, false);
            Assert::AreEqual(6u, thePeal.NoOfBellsRung(database));
        }
        
        TEST_METHOD(Peal_ValidatePealRingers)
        {
            RingingDatabase database;

            Method theMethod(KNoId, 6, L"Cambridge", L"Surprise", L"");
            Duco::ObjectId methodId = database.MethodsDatabase().AddObject(theMethod);

            Tower theTower(KNoId, L"Name", L"City", L"County", 6);
            Duco::ObjectId sixBellRingId = theTower.AddDefaultRing();
            Duco::ObjectId towerId = database.TowersDatabase().AddObject(theTower);

            Peal thePeal;
            thePeal.SetMethodId(methodId);
            thePeal.SetTowerId(towerId);
            thePeal.SetRingId(sixBellRingId);
            thePeal.SetDoubleHanded(false);
            thePeal.SetRingerId(1, 1, false);
            thePeal.SetRingerId(2, 2, false);
            thePeal.SetRingerId(3, 3, false);
            thePeal.SetRingerId(4, 4, false);
            thePeal.SetRingerId(5, 5, false);
            thePeal.SetRingerId(6, 6, false);

            Assert::IsTrue(thePeal.ValidPealRingers(database, true, true));
        }

        TEST_METHOD(Peal_ValidatePealRingersWithDoubleHandedRinger)
        {
            RingingDatabase database;

            Method theMethod(KNoId, 6, L"Cambridge", L"Surprise", L"");
            Duco::ObjectId methodId = database.MethodsDatabase().AddObject(theMethod);

            Tower theTower(KNoId, L"Name", L"City", L"County", 6);
            Duco::ObjectId sixBellRingId = theTower.AddDefaultRing();
            Duco::ObjectId towerId = database.TowersDatabase().AddObject(theTower);

            Peal thePeal;
            thePeal.SetMethodId(methodId);
            thePeal.SetTowerId(towerId);
            thePeal.SetRingId(sixBellRingId);
            thePeal.SetDoubleHanded(true);
            thePeal.SetRingerId(1, 1, false);
            thePeal.SetRingerId(2, 2, false);
            thePeal.SetRingerId(3, 3, false);
            thePeal.SetRingerId(4, 4, false);
            thePeal.SetRingerId(5, 5, false);
            thePeal.SetRingerId(5, 6, false); // same ringer as the 5th.

            Assert::IsTrue(thePeal.ValidPealRingers(database, true, true));
        }

        TEST_METHOD(Peal_ValidatePealRingersOnHandbells)
        {
            RingingDatabase database;

            Method theMethod(KNoId, 6, L"Cambridge", L"Surprise", L"");
            Duco::ObjectId methodId = database.MethodsDatabase().AddObject(theMethod);

            Tower theTower(KNoId, L"Name", L"City", L"County", 6);
            theTower.SetHandbell(true);
            Duco::ObjectId sixBellRingId = theTower.AddDefaultRing();
            Duco::ObjectId towerId = database.TowersDatabase().AddObject(theTower);

            Peal thePeal;
            thePeal.SetMethodId(methodId);
            thePeal.SetTowerId(towerId);
            thePeal.SetRingId(sixBellRingId);
            thePeal.SetHandbell(true);
            thePeal.SetRingerId(1, 1, false);
            thePeal.SetRingerId(2, 2, false);
            thePeal.SetRingerId(3, 3, false);

            Assert::IsTrue(thePeal.ValidPealRingers(database, true, true));
        }

        TEST_METHOD(Peal_ValidateQuarter_TowerMissing)
        {
            RingingDatabase database;

            Method theMethod(KNoId, 6, L"Cambridge", L"Surprise", L"");
            Duco::ObjectId methodId = database.MethodsDatabase().AddObject(theMethod);

            Peal thePeal;
            thePeal.SetMethodId(methodId);
            thePeal.SetHandbell(true);
            thePeal.SetRingerId(1, 1, false);
            thePeal.SetRingerId(2, 2, false);
            thePeal.SetRingerId(3, 3, false);

            Assert::IsFalse(thePeal.ValidPealDetails(database, true, true));
            Assert::AreEqual(L"Invalid tower, Invalid ring, Not enough changes - 1250 or more required, Time invalid", thePeal.ErrorString(database.Settings(), false).c_str());
        }

        TEST_METHOD(Peal_ValidatePeal_TowerMissing)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Method theMethod(KNoId, 6, L"Cambridge", L"Surprise", L"");
            Duco::ObjectId methodId = database.MethodsDatabase().AddObject(theMethod);

            Peal thePeal;
            thePeal.SetMethodId(methodId);
            thePeal.SetHandbell(true);
            thePeal.SetRingerId(1, 1, false);
            thePeal.SetRingerId(2, 2, false);
            thePeal.SetRingerId(3, 3, false);

            Assert::IsFalse(thePeal.ValidPealDetails(database, true, true));
            Assert::AreEqual(L"Invalid tower, Invalid ring, Not enough changes - 5000 or more required, Time invalid", thePeal.ErrorString(database.Settings(), false).c_str());
        }

        TEST_METHOD(Peal_Duplicate_Identical)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Method theMethod(KNoId, 6, L"Cambridge", L"Surprise", L"");
            Duco::ObjectId methodId = database.MethodsDatabase().AddObject(theMethod);

            Tower theTower(KNoId, L"Name", L"City", L"County", 6);
            Duco::ObjectId towerId = database.TowersDatabase().AddObject(theTower);

            Peal thePeal;
            thePeal.SetDate(PerformanceDate(2013, 12, 28));
            thePeal.SetTime(PerformanceTime(181));
            thePeal.SetChanges(5184);
            thePeal.SetMethodId(methodId);
            thePeal.SetTowerId(methodId);
            thePeal.SetHandbell(true);
            thePeal.SetRingerId(1, 1, false);
            thePeal.SetRingerId(2, 2, false);
            thePeal.SetRingerId(3, 3, false);

            Peal thePeal2(thePeal);

            Assert::IsTrue(thePeal.Duplicate(thePeal2, database));
            Assert::IsFalse(thePeal.Duplicate(theMethod, database));
            Assert::IsFalse(thePeal.Duplicate(theTower, database));
            Assert::IsFalse(theTower.Duplicate(theMethod, database));
        }

        TEST_METHOD(Peal_Duplicate_Identical_Apart_from_ringerOrder)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Method theMethod(KNoId, 6, L"Cambridge", L"Surprise", L"");
            Duco::ObjectId methodId = database.MethodsDatabase().AddObject(theMethod);

            Peal thePeal;
            thePeal.SetDate(PerformanceDate(2013, 12, 28));
            thePeal.SetTime(PerformanceTime(181));
            thePeal.SetChanges(5184);
            thePeal.SetMethodId(methodId);
            thePeal.SetHandbell(true);
            thePeal.SetRingerId(1, 1, false);
            thePeal.SetRingerId(2, 2, false);
            thePeal.SetRingerId(3, 3, false);
            thePeal.SetRingerId(4, 4, false);
            thePeal.SetRingerId(5, 5, false);
            thePeal.SetRingerId(6, 6, false);

            Peal thePeal2(thePeal);
            thePeal.SetRingerId(1, 6, false);
            thePeal.SetRingerId(2, 5, false);
            thePeal.SetRingerId(3, 4, false);
            thePeal.SetRingerId(4, 3, false);
            thePeal.SetRingerId(5, 2, false);
            thePeal.SetRingerId(6, 1, false);

            Assert::IsFalse(thePeal.Duplicate(thePeal2, database));
        }

        TEST_METHOD(Peal_Duplicate_Identical_Apart_from_some_missing_ringers)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Method theMethod(KNoId, 6, L"Cambridge", L"Surprise", L"");
            Duco::ObjectId methodId = database.MethodsDatabase().AddObject(theMethod);

            Peal thePeal;
            thePeal.SetDate(PerformanceDate(2013, 12, 28));
            thePeal.SetTime(PerformanceTime(181));
            thePeal.SetChanges(5184);
            thePeal.SetMethodId(methodId);
            thePeal.SetHandbell(true);
            Peal thePeal2(thePeal);

            thePeal.SetRingerId(1, 1, false);
            thePeal.SetRingerId(2, 2, false);
            thePeal.SetRingerId(3, 3, false);
            thePeal.SetRingerId(4, 4, false);
            //thePeal.SetRingerId(5, 5, false);
            thePeal.SetRingerId(6, 6, false);

            thePeal.SetRingerId(1, 6, false);
            thePeal.SetRingerId(2, 5, false);
            thePeal.SetRingerId(3, 4, false);
            thePeal.SetRingerId(4, 3, false);
            thePeal.SetRingerId(5, 2, false);
            thePeal.SetRingerId(6, 1, false);

            Assert::IsFalse(thePeal.Duplicate(thePeal2, database));
        }

        TEST_METHOD(Peal_Duplicate_Identical_Apart_from_one_minute)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Method theMethod(KNoId, 6, L"Cambridge", L"Surprise", L"");
            Duco::ObjectId methodId = database.MethodsDatabase().AddObject(theMethod);

            Tower theTower(KNoId, L"Name", L"City", L"County", 6);
            Duco::ObjectId towerId = database.TowersDatabase().AddObject(theTower);

            Peal thePeal;
            thePeal.SetDate(PerformanceDate(2013, 12, 28));
            thePeal.SetTime(PerformanceTime(180));
            thePeal.SetChanges(5184);
            thePeal.SetMethodId(methodId);
            thePeal.SetTowerId(methodId);
            thePeal.SetHandbell(true);
            thePeal.SetRingerId(1, 1, false);
            thePeal.SetRingerId(2, 2, false);
            thePeal.SetRingerId(3, 3, false);

            Peal thePeal2(thePeal);

            Assert::IsTrue(thePeal.Duplicate(thePeal2, database));
            Assert::IsFalse(thePeal.Duplicate(theMethod, database));
            Assert::IsFalse(thePeal.Duplicate(theTower, database));
            Assert::IsFalse(theTower.Duplicate(theMethod, database));
        }

        TEST_METHOD(Peal_Duplicate_Identical_Apart_from_duplicate_tower)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Method theMethod(KNoId, 6, L"Cambridge", L"Surprise", L"");
            Duco::ObjectId methodId = database.MethodsDatabase().AddObject(theMethod);

            Tower theTower(KNoId, L"Name", L"City", L"County", 6);
            theTower.SetTowerbaseId(L"1234");
            Duco::ObjectId towerId = database.TowersDatabase().AddObject(theTower);
            Tower theTower2(KNoId, L"Name2", L"City2", L"County2", 6);
            theTower2.SetTowerbaseId(theTower.TowerbaseId());
            Duco::ObjectId towerId2 = database.TowersDatabase().AddObject(theTower2);

            Peal thePeal;
            thePeal.SetDate(PerformanceDate(2013, 12, 28));
            thePeal.SetTime(PerformanceTime(180));
            thePeal.SetChanges(5184);
            thePeal.SetMethodId(methodId);
            thePeal.SetTowerId(methodId);
            thePeal.SetHandbell(true);
            thePeal.SetRingerId(1, 1, false);
            thePeal.SetRingerId(2, 2, false);
            thePeal.SetRingerId(3, 3, false);

            Peal thePeal2(thePeal);
            thePeal2.SetTowerId(towerId2);

            Assert::IsTrue(thePeal.Duplicate(thePeal2, database));
        }

        TEST_METHOD(Peal_ValidPealConductors_NoConductors_ExpectedSingleConductor)
        {
            DatabaseSettings settings;

            Peal thePeal;
            Assert::IsFalse(thePeal.ValidPealConductors(false, true));
            Assert::AreEqual(L"Missing conductors", thePeal.ErrorString(settings, false).c_str());

            thePeal.ClearErrors();
            thePeal.SetConductorIdFromBell(1, true, false);
            Assert::IsFalse(thePeal.ValidPealConductors(false, true));
            Assert::AreEqual(L"Missing conductors", thePeal.ErrorString(settings, false).c_str());

            thePeal.ClearErrors();
            thePeal.SetConductorId(Duco::ObjectId(10));
            Assert::IsFalse(thePeal.ValidPealConductors(false, true));
            Assert::AreEqual(L"Conductor isn't ringing", thePeal.ErrorString(settings, false).c_str());
        }

        TEST_METHOD(Peal_Clear)
        {
            Peal thePeal;
            thePeal.SetDate(PerformanceDate(2013, 12, 28));
            thePeal.SetRingerId(1, 1, false);
            thePeal.SetConductorId(1018);
            thePeal.SetMethodId(1019);
            thePeal.SetTowerId(1020);

            thePeal.Clear();

            std::set<ObjectId> allIds;

            Assert::AreEqual(PerformanceDate(), thePeal.Date());
            Assert::AreEqual((size_t)0, thePeal.Conductors().size());
            thePeal.RingerIds(allIds);
            Assert::AreEqual((size_t)0, allIds.size());
            Assert::AreEqual(Duco::ObjectId(), thePeal.MethodId());
            Assert::AreEqual(Duco::ObjectId(), thePeal.TowerId());
        }


        TEST_METHOD(Peal_ContainsLinkedRingers_Invalid)
        {
            RingingDatabase database;
            Duco::ObjectId ringerOneId = database.RingersDatabase().AddRinger(L"Ringer 1");
            Duco::ObjectId ringerTwoId = database.RingersDatabase().AddRinger(L"Ringer 2");
            database.RingersDatabase().LinkRingers(ringerOneId, ringerTwoId);
            Duco::ObjectId ringerThreeId = database.RingersDatabase().AddRinger(L"Ringer 3");

            Peal thePeal;
            thePeal.SetRingerId(ringerOneId, 1, false);
            thePeal.SetRingerId(ringerTwoId, 2, false);
            thePeal.SetRingerId(ringerThreeId, 3, false);

            Assert::IsFalse(thePeal.ValidPealRingers(database, false, true));
            Assert::AreEqual(L"Invalid tower, Invalid method, Linked ringers in the same peal", thePeal.ErrorString(database.Settings(), false).c_str());
        }
    };
}
