#include "CppUnitTest.h"
#include <ChangeCollection.h>
#include "ToString.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(ChangeCollection_UnitTests)
    {
    public:
        TEST_METHOD(ChangeCollection_Initialisation)
        {
            Change roundsOnSix(6);
            ChangeCollection changes(roundsOnSix);

            Assert::AreEqual(roundsOnSix, changes.StartingChange());
            Assert::AreEqual(roundsOnSix, changes.EndChange());
            Assert::AreEqual((size_t)0, changes.NoOfChanges());
            Assert::IsFalse(changes.EndsWithRounds());
            Assert::IsTrue(changes.True());
            size_t changeNo = 0;
            Assert::IsFalse(changes.FalseChangeNumber(changeNo));
        }

        TEST_METHOD(ChangeCollection_PlainHuntOnSix)
        {
            Change roundsOnSix(6);
            ChangeCollection changes (roundsOnSix);

            Assert::AreEqual(roundsOnSix, changes.StartingChange());
            Assert::IsTrue(changes.Add(Duco::Change(L"214365")));
            Assert::IsTrue(changes.Add(Duco::Change(L"241635")));
            Assert::IsTrue(changes.Add(Duco::Change(L"426153")));
            Assert::IsTrue(changes.Add(Duco::Change(L"462513")));
            Assert::IsTrue(changes.Add(Duco::Change(L"645231")));
            Assert::IsTrue(changes.Add(Duco::Change(L"654321")));
            Assert::IsTrue(changes.Add(Duco::Change(L"563412")));
            Assert::IsTrue(changes.Add(Duco::Change(L"536142")));
            Assert::IsTrue(changes.Add(Duco::Change(L"351624")));
            Assert::IsTrue(changes.Add(Duco::Change(L"315264")));
            Assert::IsTrue(changes.Add(Duco::Change(L"132546")));
            Assert::IsTrue(changes.Add(Duco::Change(L"123456")));

            Assert::AreEqual((size_t)12, changes.NoOfChanges());
            Assert::IsTrue(changes.EndsWithRounds());
            Assert::AreEqual(roundsOnSix, changes.EndChange());
            Assert::IsTrue(changes.True());
            size_t changeNo = 0;
            Assert::IsFalse(changes.FalseChangeNumber(changeNo));
            Change falseChange (6);
            Assert::IsFalse(changes.FalseChange(falseChange));
            Assert::AreEqual(L"", changes.FalseDetails().c_str());
        }

        TEST_METHOD(ChangeCollection_PlainHuntOnSix_PlusOne_False)
        {
            Change roundsOnSix(6);
            ChangeCollection changes(roundsOnSix);

            Assert::AreEqual(roundsOnSix, changes.StartingChange());
            Assert::IsTrue(changes.Add(Duco::Change(L"214365")));
            Assert::IsTrue(changes.Add(Duco::Change(L"241635")));
            Assert::IsTrue(changes.Add(Duco::Change(L"426153")));
            Assert::IsTrue(changes.Add(Duco::Change(L"462513")));
            Assert::IsTrue(changes.Add(Duco::Change(L"645231")));
            Assert::IsTrue(changes.Add(Duco::Change(L"654321")));
            Assert::IsTrue(changes.Add(Duco::Change(L"563412")));
            Assert::IsTrue(changes.Add(Duco::Change(L"536142")));
            Assert::IsTrue(changes.Add(Duco::Change(L"351624")));
            Assert::IsTrue(changes.Add(Duco::Change(L"315264")));
            Assert::IsTrue(changes.Add(Duco::Change(L"132546")));
            Assert::IsTrue(changes.Add(Duco::Change(L"123456")));
            Assert::IsFalse(changes.Add(Duco::Change(L"214365")));

            Assert::AreEqual((size_t)13, changes.NoOfChanges());
            Assert::IsFalse(changes.EndsWithRounds());
            Assert::IsFalse(changes.True());
            size_t changeNo = 0;
            Assert::IsTrue(changes.FalseChangeNumber(changeNo));
            Assert::AreEqual((size_t)13, changeNo);
            Change falseChange (6);
            Assert::IsTrue(changes.FalseChange(falseChange));
            Assert::AreEqual(Change(L"214365"), falseChange);
            Assert::AreEqual(L"False at change number 13: 214365", changes.FalseDetails().c_str());
            Assert::AreEqual(Change(L"214365"), changes.EndChange());
        }

        TEST_METHOD(ChangeCollection_PlainHuntOnTen_CopyCompare)
        {
            Change roundsOnTen(10);
            ChangeCollection changes(roundsOnTen);

            Assert::AreEqual(roundsOnTen, changes.StartingChange());
            Assert::IsTrue(changes.Add(Duco::Change(L"123456790")));
            Assert::IsTrue(changes.Add(Duco::Change(L"2143658709")));
            Assert::IsTrue(changes.Add(Duco::Change(L"2416385079")));
            Assert::IsTrue(changes.Add(Duco::Change(L"4261830597")));
            Assert::IsTrue(changes.Add(Duco::Change(L"4628103957")));
            Assert::IsTrue(changes.Add(Duco::Change(L"6482019375")));
            Assert::IsTrue(changes.Add(Duco::Change(L"6840291735")));
            Assert::IsTrue(changes.Add(Duco::Change(L"8604927153")));
            Assert::IsTrue(changes.Add(Duco::Change(L"8069472513")));
            Assert::IsTrue(changes.Add(Duco::Change(L"0896745231")));
            Assert::IsTrue(changes.Add(Duco::Change(L"0987654321")));
            Assert::IsTrue(changes.Add(Duco::Change(L"9078563412")));
            Assert::IsTrue(changes.Add(Duco::Change(L"9705836142")));
            Assert::IsTrue(changes.Add(Duco::Change(L"7950381624")));
            Assert::IsTrue(changes.Add(Duco::Change(L"7593018264")));
            Assert::IsTrue(changes.Add(Duco::Change(L"5739102846")));
            Assert::IsTrue(changes.Add(Duco::Change(L"5371920486")));
            Assert::IsTrue(changes.Add(Duco::Change(L"3517294068")));
            Assert::IsTrue(changes.Add(Duco::Change(L"3152749608")));
            Assert::IsTrue(changes.Add(Duco::Change(L"1325476980")));
            Assert::IsTrue(changes.Add(Duco::Change(L"1234567890")));

            ChangeCollection changesCopy(changes);
            Assert::IsTrue(changes == changesCopy);

            Assert::IsTrue(changes.Add(Duco::Change(L"1234509876")));
            Assert::IsFalse(changes == changesCopy);
        }

        TEST_METHOD(ChangeCollection_CompareIdenticalCounts)
        {
            Change roundsOnTen(10);
            ChangeCollection changes(roundsOnTen);
            Assert::AreEqual(roundsOnTen, changes.StartingChange());
            Assert::IsTrue(changes.Add(Duco::Change(L"1234567890")));
            Assert::IsTrue(changes.Add(Duco::Change(L"2143658709")));
            Assert::IsTrue(changes.Add(Duco::Change(L"2416385079")));

            ChangeCollection changes2(roundsOnTen);
            Assert::IsTrue(changes2.Add(Duco::Change(L"1234567890")));
            Assert::IsTrue(changes2.Add(Duco::Change(L"2143658709")));
            Assert::IsTrue(changes2.Add(Duco::Change(L"2416385097")));

            Assert::IsFalse(changes == changes2);
        }

    };
}
