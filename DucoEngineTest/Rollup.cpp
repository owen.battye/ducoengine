#include "CppUnitTest.h"
#include <Change.h>
#include <Rollup.h>
#include "ToString.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(Rollup_UnitTests)
    {
    public:
        TEST_METHOD(Rollup_LittleBellRollups)
        {
            Rollup rollup(L"!8.1234.Four Bell little bell rollups");

            Assert::AreEqual((unsigned int)8, rollup.Order());
            Assert::AreEqual(L"0: Four Bell little bell rollups (1234)", rollup.Print().c_str());

            Change roundsOnEight(L"12345678");
            Assert::IsTrue(rollup.Match(roundsOnEight));
            Change roundsOnTen(L"1234567890");
            Assert::IsFalse(rollup.Match(roundsOnTen));
            Change randomEight(L"83261754");
            Assert::IsFalse(rollup.Match(randomEight));
            Change threeBellRollup(L"12387654");
            Assert::IsFalse(rollup.Match(threeBellRollup));
            Change fourBellRollup(L"12348765");
            Assert::IsTrue(rollup.Match(fourBellRollup));
            Change fiveBellRollup(L"12345876");
            Assert::IsTrue(rollup.Match(fiveBellRollup));

            Assert::AreEqual(L"3: Four Bell little bell rollups (1234)", rollup.Print().c_str());
        }
        TEST_METHOD(Rollup_BigBellRollup)
        {
            Rollup rollup(L"!8.678.Three Bell big bell rollups");

            Assert::AreEqual((unsigned int)8, rollup.Order());
            Assert::AreEqual(L"0: Three Bell big bell rollups (678)", rollup.Print().c_str());

            Change roundsOnEight(L"12345678");
            Assert::IsTrue(rollup.Match(roundsOnEight));
            Change roundsOnTen(L"1234567890");
            Assert::IsFalse(rollup.Match(roundsOnTen));
            Change randomEight(L"83261754");
            Assert::IsFalse(rollup.Match(randomEight));
            Change twoBellRollup(L"78123456");
            Assert::IsFalse(rollup.Match(twoBellRollup));
            Change threeBellRollup(L"67812345");
            Assert::IsTrue(rollup.Match(threeBellRollup));
            Change fourBellRollup(L"56781234");
            Assert::IsTrue(rollup.Match(fourBellRollup));
            Change fiveBellRollup(L"45678123");
            Assert::IsTrue(rollup.Match(fiveBellRollup));

            Assert::AreEqual(L"4: Three Bell big bell rollups (678)", rollup.Print().c_str());
        }
    };
}
