﻿#include <CppUnitTest.h>
#include <DatabaseReader.h>
#include <DatabaseWriter.h>
#include <PerformanceDate.h>
#include "ToString.h"
#include <DucoConfiguration.h>
#include <RingingDatabase.h>
#include <ImportExportProgressCallback.h>
#include <codecvt>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    int value1 = 348;
    bool value2 = false;
    std::wstring value3 = L"value3";
    std::wstring value4 = L"value4♯a";
    //std::wstring value4 = L"value4";
    PerformanceDate value5(2021, 5, 7);
    std::wstring value6 = L"siân";
    time_t value7 = time(NULL);
    std::string value8 = "ÿØÿà";
    Duco::ObjectId value9(274);

    TEST_CLASS(DatabaseReaderAndWriter_UnitTests), Duco::ImportExportProgressCallback
    {
    public:
        // From ImportExportProgressCallback
        void InitialisingImportExport(bool internalising, unsigned int versionNumber, size_t totalNumberOfObjects)
        {
        }
        void ObjectProcessed(bool internalising, Duco::TObjectType objectType, Duco::ObjectId objectId, int totalPercentage, size_t numberInThisSubDatabase)
        {
        }
        void ImportExportComplete(bool internalising)
        {
        }
        void ImportExportFailed(bool internalising, int errorCode)
        {
            Assert::AreEqual(0, errorCode);
        }

        void CheckReader(Duco::DatabaseReader& reader)
        {
            Assert::AreEqual(Duco::DucoConfiguration::LatestDatabaseVersion(), reader.ReadUInt());
            Assert::AreEqual(value1, reader.ReadInt());
            Assert::AreEqual(value2, reader.ReadBool());
            std::wstring reReadValue3;
            std::wstring reReadValue4;
            PerformanceDate reReadValue5;
            std::wstring reReadValue6;
            time_t reReadValue7;
            std::string* reReadValue8 = NULL;
            Duco::ObjectId reReadValue9;

            reader.ReadString(reReadValue3);
            Assert::AreEqual(value3, reReadValue3);
            reader.ReadString(reReadValue4);
            Assert::AreEqual(value4, reReadValue4);
            reader.ReadDate(reReadValue5);
            Assert::AreEqual(value5, reReadValue5);
            reader.ReadString(reReadValue6);
            Assert::AreEqual(value6, reReadValue6);
            reReadValue7 = reader.ReadTime();
            Assert::AreEqual(value7, reReadValue7);
            reader.ReadPictureBuffer(reReadValue8);
            Assert::AreEqual(value8, *reReadValue8);
            reader.ReadId(reReadValue9);
            Assert::AreEqual(value9, reReadValue9);
            Assert::AreEqual(value1, reader.ReadInt());
        }

        TEST_METHOD(DatabaseReaderAndWriter_StringChecks)
        {
            const char* databasefileName = "test.tmp";
            {
                DatabaseWriter writer(databasefileName);
                writer.WriteInt(Duco::DucoConfiguration::LatestDatabaseVersion());
                writer.WriteInt(value1);
                writer.WriteBool(value2);
                writer.WriteString(value3);
                writer.WriteString(value4);
                writer.WriteDate(value5);
                writer.WriteString(value6);
                writer.WriteTime(value7);
                writer.WritePictureBuffer(value8);
                writer.WriteId(value9);
                writer.WriteInt(value1);
            }
            {
                DatabaseReader reader(databasefileName);
                CheckReader(reader);
            }
        }

        TEST_METHOD(DatabaseReaderAndWriter_HACKAWAY)
        {
            //std::wstring test = L"siân";
            std::wstring test = L"value4♯a";
            //std::wofstream output("test.utf8.txt", std::ios::out | std::ios::trunc | std::ios::binary);
            std::wofstream output;
            output.open("test.utf8.txt", std::ios::out | std::ios::trunc | std::ios::binary);
            //output.exceptions(std::wifstream::failbit);
            std::locale loc(std::locale("C"), new std::codecvt_utf8<wchar_t>());
            output.imbue(loc);
            output << test.length() << std::ends;
            output << test.c_str();
            output.close();

            //std::setlocale(LC_ALL, "en_US.utf8");
            std::wofstream output2("test.utf16.txt");
            std::locale loc2(std::locale("C"), new std::codecvt_utf8<wchar_t>());
            output2.imbue(loc2);
            output2 << test.length() << std::ends;
            output2 << test.c_str();
            output2.close();

            std::wstring readString;
            size_t readLength;
            //std::wifstream input("test.utf8.txt", std:: ios::out | std::ios::trunc | std::ios::binary);
            //std::wifstream input("test.utf8.txt", std::ios::in | std::ios::binary);
            std::wifstream input("test.utf8.txt", std::ios::in);
            input.imbue(loc);
            input >> readLength;
            input.ignore();
            input >> readString;
            input.close();

            std::wstring readString2;
            size_t readLength2;
            std::wifstream input2("test.utf16.txt");
            input2.imbue(loc2);
            input2 >> readLength2;
            input2.ignore();
            input2 >> readString2;
            input2.close();

            Assert::AreEqual(readLength, readLength2);
            Assert::AreEqual(readString, readString2);
            Assert::AreEqual(readLength, test.length());
            Assert::AreEqual(test, readString);
        }


        TEST_METHOD(DatabaseReaderAndWriter_StringCheckOldFile)
        {
            const std::string databaseName("composition_DBVer36.duc");
            DatabaseReader reader(databaseName.c_str());
            Assert::AreEqual(36, reader.ReadInt());
        }

        TEST_METHOD(DatabaseReaderAndWriter_CreateNewFileAndReread)
        {
            const std::string databaseName("NewDatabase.duc");
            {
                Duco::RingingDatabase database;
                database.Externalise(databaseName.c_str(), this);
            }

            unsigned int newDatabaseVersion = 0;
            {
                Duco::RingingDatabase rereaddatabase;
                rereaddatabase.Internalise(databaseName.c_str(), this, newDatabaseVersion);
            }

            Assert::AreEqual(Duco::DucoConfiguration::LatestDatabaseVersion(), newDatabaseVersion);

        }
    };
}

