#include "CppUnitTest.h"
#include <MethodSeries.h>
#include <MethodDatabase.h>
#include "ToString.h"
//#include <DatabaseSettings.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(MethodSeries_UnitTests)
    {
        Duco::MethodDatabase database;
        Duco::ObjectId bristolRoyalId;

    public:
        MethodSeries_UnitTests()
        {
            database.ClearObjects(true);
            bristolRoyalId = database.AddMethod(L"Bristol", L"Surprise", 10, false);
        }

        TEST_METHOD(MethodSeries_DefaultBehaviour)
        {
            MethodSeries theSeries;
            Assert::AreEqual(TObjectType::EMethodSeries, theSeries.ObjectType());
            Assert::IsFalse(theSeries.NoOfBellsSet());
            Assert::AreEqual(-1U, theSeries.NoOfBells());
            Assert::AreEqual((size_t)0, theSeries.NoOfMethods());

            Assert::AreEqual(ObjectId(1), bristolRoyalId);
        }

        TEST_METHOD(MethodSeries_DefaultConstructor)
        {
            const wchar_t* methodName = L"Standard Eight major";
            MethodSeries theSeries(1, methodName, 8);
            Assert::AreEqual(TObjectType::EMethodSeries, theSeries.ObjectType());
            Assert::AreEqual(-1U, theSeries.NoOfBells());
            Assert::IsFalse(theSeries.NoOfBellsSet());
            Assert::AreEqual(methodName, theSeries.Name().c_str());

            Assert::IsTrue(theSeries.AddMethod(bristolRoyalId, database));
            Assert::IsTrue(theSeries.NoOfBellsSet());

            Assert::AreEqual(10U, theSeries.NoOfBells());

            MethodSeries copiedSeries(theSeries);
            Assert::AreEqual(TObjectType::EMethodSeries, copiedSeries.ObjectType());
            Assert::AreEqual(10U, copiedSeries.NoOfBells());
            Assert::IsTrue(copiedSeries.NoOfBellsSet());
            Assert::AreEqual(methodName, copiedSeries.Name().c_str());
            Assert::IsTrue(theSeries.ContainsMethod(bristolRoyalId));
        }

        TEST_METHOD(MethodSeries_CheckDifferent)
        {
            MethodSeries seriesOne(KNoId, L"Series 1", 6);
            MethodSeries seriesTwo(KNoId, L"Series 2", 8);

            Assert::IsFalse(seriesOne == seriesTwo);
            Assert::IsTrue(seriesOne != seriesTwo);

        }
    };
}
