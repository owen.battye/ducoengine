﻿#include "CppUnitTest.h"
#include <Tower.h>
#include <TowerDatabase.h>
#include <PealDatabase.h>
#include <RingingDatabase.h>
#include "ToString.h"
#include <RenumberProgressCallback.h>
#include <ImportExportProgressCallback.h>
#include <StatisticFilters.h>
#include <DucoEngineUtils.h>
#include <Peal.h>
#include "ProgressCallback.h"
#include <chrono>
#include "DucoEngineLog.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;
using namespace std::chrono;

namespace DucoEngineTest
{
    TEST_CLASS(TowerDatabase_UnitTests), Duco::ImportExportProgressCallback, Duco::RenumberProgressCallback, Duco::ProgressCallback
    {
    protected:
        RenumberProgressCallback::TRenumberStage lastStage;

    public:
        // RenumberProgressCallback
        void RenumberInitialised(RenumberProgressCallback::TRenumberStage newStage)
        {
            if (RenumberProgressCallback::TRenumberStage::ENotStarted != newStage)
            {
                Assert::IsTrue(newStage > lastStage);
            }
            lastStage = newStage;
        }

        void RenumberStep(size_t objectId, size_t total)
        {

        }
        void RenumberComplete()
        {

        }
        // From ImportExportProgressCallback
        void InitialisingImportExport(bool internalising, unsigned int versionNumber, size_t totalNumberOfObjects)
        {
        }
        void ObjectProcessed(bool internalising, Duco::TObjectType objectType, Duco::ObjectId objectId, int totalPercentage, size_t numberInThisSubDatabase)
        {
        }
        void ImportExportComplete(bool internalising)
        {
        }
        void ImportExportFailed(bool internalising, int errorCode)
        {
            Assert::AreEqual(0, errorCode);
        }

        // from ProgressCallback
        void Initialised()
        {

        }
        void Step(int progressPercent)
        {

        }
        void Complete(bool error)
        {

        }
        TEST_METHOD(TowerDatabase_Create_ExternaliseInternalise)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Tower moulton(KNoId, L"St Peter and Paul", L"Moulton", L"Northampton", 12);
            moulton.AddDefaultRing(L"12 bell peal", L"11-1-14", L"Ab");
            moulton.SetRemoved(true);
            Duco::ObjectId moultonId = database.TowersDatabase().AddObject(moulton);
            Assert::IsTrue(moultonId.ValidId());

            Tower moulton2(KNoId, L"St Peter and Paul", L"Barryville", L"Northampton", 12);
            moulton.AddDefaultRing(L"12 bell peal", L"11-1-14", L"Ab");
            moulton2.SetHandbell(true);
            Duco::ObjectId moultonId2 = database.TowersDatabase().AddObject(moulton2);
            Assert::IsTrue(moultonId2.ValidId());

            Assert::AreEqual((size_t)2, database.NumberOfTowers());

            const std::string tempFilename = "towertest.duc";
            database.Externalise(tempFilename.c_str(), this);

            RingingDatabase newDatabase;
            unsigned int databaseVersion = 0;
            newDatabase.Internalise(tempFilename.c_str(), this, databaseVersion);
            Assert::IsTrue(databaseVersion > 10);
            Assert::AreEqual((size_t)2, newDatabase.NumberOfTowers());

            const Tower* const anyTower = newDatabase.TowersDatabase().FindTower(moultonId);
            Assert::AreEqual(L"Moulton, Northampton. (St Peter and Paul)", anyTower->FullName().c_str());
            Assert::IsFalse(anyTower->Handbell());
            Assert::IsTrue(anyTower->Removed());

            const Tower* const anyOtherTower = newDatabase.TowersDatabase().FindTower(moultonId2);
            Assert::AreEqual(L"Barryville, Northampton. (St Peter and Paul)", anyOtherTower->FullName().c_str());
            Assert::IsTrue(anyOtherTower->Handbell());
            Assert::IsFalse(anyOtherTower->Removed());

        }
        TEST_METHOD(TowerDatabase_FindAllSaintsNorthampton)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Tower allSaintsNorthampton(KNoId, L"All Saints", L"Northampton", L"Northamptonshire", 10);
            allSaintsNorthampton.AddDefaultRing(L"10 bell peal", L"17-0-4", L"E");
            Duco::ObjectId allSaintsNorthamptonId = database.TowersDatabase().AddObject(allSaintsNorthampton);
            Assert::IsTrue(allSaintsNorthamptonId.ValidId());
            Assert::AreEqual((size_t)1, database.NumberOfTowers());

            Tower allSaintsNorthamptonOldBells(KNoId, L"All Saints (Old bells)", L"Northampton", L"Northamptonshire", 8);
            allSaintsNorthamptonOldBells.AddDefaultRing(L"Old bells", L"21", L"Ab");
            allSaintsNorthamptonOldBells.SetRemoved(true);
            Duco::ObjectId allSaintsNorthamptonOldBellsId = database.TowersDatabase().AddObject(allSaintsNorthamptonOldBells);
            Assert::IsTrue(allSaintsNorthamptonOldBellsId.ValidId());
            Assert::AreEqual((size_t)2, database.NumberOfTowers());

            Assert::AreEqual(allSaintsNorthamptonId, database.TowersDatabase().SuggestTower(L"All Saints", L"Northampton", L"", 10, L"17-0-4", L""));
            Assert::AreEqual(allSaintsNorthamptonId, database.TowersDatabase().SuggestTower(L"All Saints", L"Northampton", L"", 8, L"", L"E"));
            Assert::AreEqual(allSaintsNorthamptonOldBellsId, database.TowersDatabase().SuggestTower(L"All Saints (Old Bells)", L"Northampton", L"", 8, L"", L"Ab"));

            Assert::AreEqual(allSaintsNorthamptonId, database.TowersDatabase().SuggestTower(L"All Saints", L"Northampton", L"Northamptonshire", 8, L"17-0-4", L""));
            Assert::AreEqual(allSaintsNorthamptonId, database.TowersDatabase().SuggestTower(L"All Saints", L"Northampton", L"Northamptonshire", 8, L"", L"E"));
            Assert::AreEqual(allSaintsNorthamptonOldBellsId, database.TowersDatabase().SuggestTower(L"All Saints", L"Northampton", L"Northamptonshire", 8, L"", L"A♭"));
            Assert::AreEqual(allSaintsNorthamptonOldBellsId, database.TowersDatabase().SuggestTower(L"All Saints", L"Northampton", L"Northamptonshire", 8, L"21", L"A♭"));
            Assert::AreEqual(allSaintsNorthamptonOldBellsId, database.TowersDatabase().SuggestTower(L"All Saints", L"Northampton", L"", 8, L"", L""));
        }

        TEST_METHOD(TowerDatabase_FindAllSaintsNorthampton_SuggestTowerFullname)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Tower allSaintsNorthampton(KNoId, L"All Saints", L"Northampton", L"Northamptonshire", 10);
            allSaintsNorthampton.AddDefaultRing(L"10 bell peal", L"17-0-4", L"E");
            Duco::ObjectId allSaintsNorthamptonId = database.TowersDatabase().AddObject(allSaintsNorthampton);
            Assert::IsTrue(allSaintsNorthamptonId.ValidId());

            Assert::AreEqual((size_t)1, database.NumberOfTowers());

            Duco::ObjectId suggestedTowerId = database.TowersDatabase().SuggestTower(L"Northampton, Northamptonshire. (All Saints)", 10);
            Assert::AreEqual(allSaintsNorthamptonId, suggestedTowerId);

            Tower stGilesNorthampton(KNoId, L"St Giles", L"Northampton", L"Northamptonshire", 10);
            stGilesNorthampton.AddDefaultRing(L"10 bell peal", L"22-3-0");
            Duco::ObjectId stGilesNorthamptonId = database.TowersDatabase().AddObject(stGilesNorthampton);
            Assert::IsTrue(stGilesNorthamptonId.ValidId());

            suggestedTowerId = database.TowersDatabase().SuggestTower(L"Northampton, Northamptonshire. (St Giles)", 10);
            Assert::AreEqual(stGilesNorthamptonId, suggestedTowerId);
        }

        TEST_METHOD(TowerDatabase_FindDoreFromYACR)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Tower dore(KNoId, L"Christ Church", L"Dore", L"", 8);
            dore.AddRing(6, L"", L"G");
            dore.AddRing(8, L"0", L"G");
            Duco::ObjectId doreId = database.TowersDatabase().AddObject(dore);
            Assert::IsTrue(doreId.ValidId());

            Assert::AreEqual((size_t)1, database.NumberOfTowers());

            Duco::ObjectId suggestedTowerId = database.TowersDatabase().SuggestTower(L"Christ Church", L"Dore", L"South Yorkshire", 8, L"11-0-24", L"G");
            Assert::AreEqual(doreId, suggestedTowerId);
        }

        TEST_METHOD(TowerDatabase_SuggestTower)
        {
            RingingDatabase database;

            Tower saintPaulsLondon(KNoId, L"St Paul", L"London", L"Middlesex", 12);
            saintPaulsLondon.AddRing(12, L"28 cwt", L"");
            Duco::ObjectId saintPaulsId = database.TowersDatabase().AddObject(saintPaulsLondon);

            Assert::IsFalse(database.TowersDatabase().SuggestTower(L"St Sepulchre without Newgate", L"London", L"Middlesex", 10, L"61 cwt", L"").ValidId());

            Tower saintSepulchreLondon(KNoId, L"St Sepulchre without Newgate", L"London", L"Middlesex", 10);
            saintSepulchreLondon.AddRing(10, L"61 cwt", L"");
            Duco::ObjectId saintSepsId = database.TowersDatabase().AddObject(saintSepulchreLondon);

            Assert::AreNotEqual(saintPaulsId, saintSepsId);
            Assert::AreEqual(saintSepsId, database.TowersDatabase().SuggestTower(L"St Sepulchre without Newgate", L"London", L"Middlesex", 10, L"61 cwt", L""));
        }

        TEST_METHOD(TowerDatabase_AndrewGCraddock_FirstTwoPeals)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Tower jesmond(KNoId, L"St George's Parish Hall", L"Jesmond", L"Newcastle Upon Tyne", 6);
            jesmond.AddRing(6, L"11", L"");
            Duco::ObjectId jesmondId = database.TowersDatabase().AddObject(jesmond);
            Assert::IsTrue(jesmondId.ValidId());
            Tower northampton(KNoId, L"All Saints", L"Northampton", L"Northamptonshire", 8);
            northampton.AddRing(8, L"21", L"");
            Duco::ObjectId northamptonId = database.TowersDatabase().AddObject(northampton);
            Assert::IsTrue(northamptonId.ValidId());

            Assert::AreEqual((size_t)2, database.NumberOfTowers());

            Assert::AreEqual(jesmondId, database.TowersDatabase().SuggestTower(L"Tower of St George's Church", L"Jesmond", L"Newcastle Upon Tyne", 6, L"11", L""));
        }

        TEST_METHOD(TowerDatabase_RemovedTowers)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)36, databaseVersionNumber);

            Assert::AreEqual((size_t)1, database.TowersDatabase().NumberOfRemovedTowers());
            size_t noOfLinkedTowers;
            Assert::AreEqual((size_t)318, database.NumberOfActiveTowers(noOfLinkedTowers));
            Assert::AreEqual((size_t)1, noOfLinkedTowers);

            const Tower* const anyTower = database.TowersDatabase().FindTower(25);
            Assert::AreEqual(L"BOLTON, Lancashire. (St Mary the Virgin)", anyTower->FullName().c_str());
            Assert::IsFalse(anyTower->Handbell());
            Assert::IsFalse(anyTower->Removed());
        }

        TEST_METHOD(TowerDatabase_GetAllPlaceOptions)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)36, databaseVersionNumber);

            std::set<std::wstring> counties;
            std::set<std::wstring> cities;
            std::set<std::wstring> names;

            database.TowersDatabase().GetAllPlaceOptions(counties, cities, names);
            Assert::AreEqual((size_t)71, counties.size());
            Assert::AreEqual((size_t)276, cities.size());
            Assert::AreEqual((size_t)153, names.size());
        }

        TEST_METHOD(TowerDatabase_CreateLinkedTowers_newTower)
        {
            std::wstring errorString;
            RingingDatabase database;

            Tower newTower(KNoId, L"Newer", L"Somewhere", L"County", 6);
            Duco::ObjectId newTowerId = database.TowersDatabase().AddObject(newTower);
            Assert::IsTrue(newTowerId.ValidId());
            Assert::AreEqual((size_t)1, database.NumberOfTowers());

            Tower oldTower(KNoId, L"Older", L"Somewhere", L"County", 6);
            Assert::IsTrue(database.TowersDatabase().CheckLinkedTowerId(oldTower.Id(), newTowerId, errorString));
            Assert::AreEqual((size_t)0, errorString.length());

            std::list<std::set<Duco::ObjectId> > allLinkedTowerIds;
            database.TowersDatabase().GetAllLinkedIds(allLinkedTowerIds);
            Assert::AreEqual((size_t)0, allLinkedTowerIds.size());
        }

        TEST_METHOD(TowerDatabase_CreateLinkedTowers_ExistingTowers)
        {
            std::wstring errorString;
            RingingDatabase database;

            Tower newTower(KNoId, L"Newer", L"Somewhere", L"County", 6);
            Duco::ObjectId newTowerId = database.TowersDatabase().AddObject(newTower);
            Assert::IsTrue(newTowerId.ValidId());
            Assert::AreEqual((size_t)1, database.NumberOfTowers());

            Tower oldTower(KNoId, L"Older", L"Somewhere", L"County", 6);
            Duco::ObjectId oldTowerId = database.TowersDatabase().AddObject(oldTower);
            Assert::IsTrue(oldTowerId.ValidId());
            Assert::AreEqual((size_t)2, database.NumberOfTowers());

            Assert::IsTrue(database.TowersDatabase().CheckLinkedTowerId(oldTowerId, newTowerId, errorString));
            Assert::AreEqual((size_t)0, errorString.length());

            std::list<std::set<Duco::ObjectId> > allLinkedTowerIds;
            database.TowersDatabase().GetAllLinkedIds(allLinkedTowerIds);
            Assert::AreEqual((size_t)0, allLinkedTowerIds.size()); // for performance - all non linked ideas are removed.
            // towers arent actually linked in the database


        }

        TEST_METHOD(TowerDatabase_CreateLinkedTowers_CircularReference)
        {
            std::wstring errorString;
            RingingDatabase database;

            Tower newTower(KNoId, L"Newer", L"Somewhere", L"County", 6);
            Duco::ObjectId newTowerId = database.TowersDatabase().AddObject(newTower);
            Assert::IsTrue(newTowerId.ValidId());
            Assert::AreEqual((size_t)1, database.NumberOfTowers());

            Tower oldTower(KNoId, L"Older", L"Somewhere", L"County", 6);
            Duco::ObjectId oldTowerId = database.TowersDatabase().AddObject(oldTower);
            Assert::IsTrue(oldTowerId.ValidId());
            Assert::AreEqual((size_t)2, database.NumberOfTowers());

            newTower = *database.TowersDatabase().FindTower(newTowerId);
            newTower.SetLinkedTowerId(oldTowerId);
            Assert::IsTrue(database.TowersDatabase().UpdateObject(newTower));

            Assert::IsFalse(database.TowersDatabase().CheckLinkedTowerId(oldTowerId, newTowerId, errorString));
            Assert::AreEqual(L"That would create a circular tower reference", errorString.c_str());

            std::list<std::set<Duco::ObjectId> > allLinkedTowerIds;
            database.TowersDatabase().GetAllLinkedIds(allLinkedTowerIds);
            Assert::AreEqual((size_t)1, allLinkedTowerIds.size());

            std::set<Duco::ObjectId> linkedIds;
            database.TowersDatabase().GetLinkedIds(newTowerId, linkedIds);
            Assert::AreEqual((size_t)2, linkedIds.size());
        }

        TEST_METHOD(TowerDatabase_CreateLinkedTowers_ThreeTowers)
        {
            RingingDatabase database;

            Tower newTower(KNoId, L"Newer", L"Somewhere", L"County", 6);
            Duco::ObjectId newTowerId = database.TowersDatabase().AddObject(newTower);
            Assert::IsTrue(newTowerId.ValidId());
            Assert::AreEqual((size_t)1, database.NumberOfTowers());

            Tower oldTower(KNoId, L"Older", L"Somewhere", L"County", 6);
            oldTower.SetLinkedTowerId(newTowerId);
            Duco::ObjectId oldTowerId = database.TowersDatabase().AddObject(oldTower);
            Assert::IsTrue(oldTowerId.ValidId());
            Assert::AreEqual((size_t)2, database.NumberOfTowers());

            Tower oldestTower(KNoId, L"Oldest", L"Somewhere", L"County", 6);
            oldestTower.SetLinkedTowerId(newTowerId);
            Duco::ObjectId oldestTowerId = database.TowersDatabase().AddObject(oldestTower);
            Assert::IsTrue(oldestTowerId.ValidId());
            Assert::AreEqual((size_t)3, database.NumberOfTowers());

            std::list<std::set<Duco::ObjectId> > allLinkedTowerIds;
            database.TowersDatabase().GetAllLinkedIds(allLinkedTowerIds);
            Assert::AreEqual((size_t)1, allLinkedTowerIds.size());
            Assert::AreEqual((size_t)3, allLinkedTowerIds.begin()->size());

            std::set<Duco::ObjectId> linkedIds;
            database.TowersDatabase().GetLinkedIds(newTowerId, linkedIds);
            Assert::AreEqual((size_t)3, linkedIds.size());
        }

        TEST_METHOD(TowerDatabase_GetMasterTower_NoTowers)
        {
            RingingDatabase database;

            std::set<Duco::ObjectId> linkedIds;

            Assert::AreEqual((size_t)0, linkedIds.size());
            Duco::ObjectId masterTowerId = database.TowersDatabase().GetMasterTowerId(linkedIds);
            Assert::AreEqual(Duco::ObjectId(KNoId), masterTowerId);
        }

        TEST_METHOD(TowerDatabase_GetMasterTower_TwoTowersFirstRemoved_NoneWithPosition)
        {
            RingingDatabase database;

            Tower firstTower(KNoId, L"A", L"Somewhere", L"County", 6);
            firstTower.SetRemoved(true);
            Duco::ObjectId firstTowerId = database.TowersDatabase().AddObject(firstTower);
            Tower secondTower(KNoId, L"B", L"Somewhere", L"County", 6);
            secondTower.SetLinkedTowerId(firstTowerId);
            Duco::ObjectId secondTowerId = database.TowersDatabase().AddObject(secondTower);

            std::set<Duco::ObjectId> linkedIds;

            database.TowersDatabase().GetLinkedIds(firstTowerId, linkedIds);
            Assert::AreEqual((size_t)2, linkedIds.size());
            Duco::ObjectId masterTowerId = database.TowersDatabase().GetMasterTowerId(linkedIds);
            Assert::AreEqual(secondTowerId, masterTowerId);


            database.TowersDatabase().GetLinkedIds(secondTowerId, linkedIds);
            Assert::AreEqual((size_t)2, linkedIds.size());
            masterTowerId = database.TowersDatabase().GetMasterTowerId(linkedIds);
            Assert::AreEqual(secondTowerId, masterTowerId);
        }

        TEST_METHOD(TowerDatabase_GetMasterTower_TwoTowersSecondRemoved_NoneWithPosition)
        {
            RingingDatabase database;

            Tower firstTower(KNoId, L"A", L"Somewhere", L"County", 6);
            Duco::ObjectId firstTowerId = database.TowersDatabase().AddObject(firstTower);
            Tower secondTower(KNoId, L"B", L"Somewhere", L"County", 6);
            secondTower.SetRemoved(true);
            secondTower.SetLinkedTowerId(firstTowerId);
            Duco::ObjectId secondTowerId = database.TowersDatabase().AddObject(secondTower);

            std::set<Duco::ObjectId> linkedIds;

            database.TowersDatabase().GetLinkedIds(firstTowerId, linkedIds);
            Assert::AreEqual((size_t)2, linkedIds.size());
            Duco::ObjectId masterTowerId = database.TowersDatabase().GetMasterTowerId(linkedIds);
            Assert::AreEqual(firstTowerId, masterTowerId);


            database.TowersDatabase().GetLinkedIds(secondTowerId, linkedIds);
            Assert::AreEqual((size_t)2, linkedIds.size());
            masterTowerId = database.TowersDatabase().GetMasterTowerId(linkedIds);
            Assert::AreEqual(firstTowerId, masterTowerId);
        }


        TEST_METHOD(TowerDatabase_GetMasterTower_VeryLargeDatabase)
        {
            RingingDatabase database;

            for (size_t i = 0; i < 3000; ++i)
            {
                Tower firstTower(KNoId, DucoEngineUtils::ToString(i), L"Somewhere", L"County", 6);
                database.TowersDatabase().AddObject(firstTower);
            }
            Duco::ObjectId firstTowerId(1);
            Tower lastTower(KNoId, L"Last", L"Somewhere", L"County", 6);
            lastTower.SetLinkedTowerId(firstTowerId);
            Duco::ObjectId firstLinkedTowerId = database.TowersDatabase().AddObject(lastTower);

            std::set<Duco::ObjectId> linkedIds;

            database.TowersDatabase().GetLinkedIds(firstTowerId, linkedIds);
            Assert::AreEqual((size_t)2, linkedIds.size());
            Duco::ObjectId masterTowerId = database.TowersDatabase().GetMasterTowerId(linkedIds);
            Assert::AreEqual(firstTowerId, masterTowerId);
        }

        TEST_METHOD(TowerDatabase_GetMasterTower_VeryLargeDatabase_NoLinkedTowers)
        {
            RingingDatabase database;

            for (size_t i = 0; i < 3000; ++i)
            {
                Tower firstTower(KNoId, DucoEngineUtils::ToString(i), L"Somewhere", L"County", 6);
                database.TowersDatabase().AddObject(firstTower);
            }
            Duco::ObjectId firstTowerId(1);

            std::set<Duco::ObjectId> linkedIds;

            database.TowersDatabase().GetLinkedIds(firstTowerId, linkedIds);
            Assert::AreEqual((size_t)1, linkedIds.size());
            Duco::ObjectId masterTowerId = database.TowersDatabase().GetMasterTowerId(linkedIds);
            Assert::AreEqual(firstTowerId, masterTowerId);
        }

        TEST_METHOD(TowerDatabase_GetAllLinkTowerIds_VeryLargeDatabase)
        {
            RingingDatabase database;

            for (size_t i = 0; i < 3000; ++i)
            {
                Tower firstTower(KNoId, DucoEngineUtils::ToString(i), L"Somewhere", L"County", 6);
                database.TowersDatabase().AddObject(firstTower);
            }
            Duco::ObjectId firstTowerId(1);
            Tower lastTower(KNoId, L"Last", L"Somewhere", L"County", 6);
            lastTower.SetLinkedTowerId(firstTowerId);
            Duco::ObjectId firstLinkedTowerId = database.TowersDatabase().AddObject(lastTower);

            std::list<std::set<Duco::ObjectId> > linkedTowerIds;
            database.TowersDatabase().GetAllLinkedIds(linkedTowerIds);
            Assert::AreEqual((size_t)1, linkedTowerIds.size());

            Assert::IsFalse(DucoEngineUtils::FindId(linkedTowerIds, firstLinkedTowerId) == linkedTowerIds.end());

        }

        TEST_METHOD(TowerDatabase_CombineLinkedTowers_WithBlankOrIdenticalLocations)
        {
            std::wstring latitude = L"123.4567890";
            std::wstring longitude = L"098.7654321";

            TowerDatabase database;

            Tower firstTower(KNoId, L"A", L"A", L"County", 6);
            firstTower.SetLatitude(latitude);
            firstTower.SetLongitude(longitude);
            Duco::ObjectId firstTowerId = database.AddObject(firstTower);

            Tower secondTower(KNoId, L"A", L"A", L"County", 6);
            secondTower.SetLinkedTowerId(firstTowerId);
            secondTower.SetLatitude(L"111.1111111");
            secondTower.SetLongitude(L"222.2222222");
            Duco::ObjectId secondTowerId = database.AddObject(secondTower);

            Tower thirdTower(KNoId, L"A", L"A", L"County", 6);
            thirdTower.SetLinkedTowerId(firstTowerId);
            Duco::ObjectId thirdTowerId = database.AddObject(thirdTower);

            Tower fourthTower(KNoId, L"A", L"A", L"County", 6);
            fourthTower.SetLinkedTowerId(firstTowerId);
            fourthTower.SetLatitude(latitude);
            fourthTower.SetLongitude(longitude);
            Duco::ObjectId fourthTowerId = database.AddObject(fourthTower);

            Assert::AreEqual((size_t)4, database.NumberOfObjects());

            std::set<Duco::ObjectId> linkedTowerIds;
            database.GetLinkedIds(firstTowerId, linkedTowerIds);

            Assert::AreEqual((size_t)4, linkedTowerIds.size());
            std::map<unsigned int, Duco::ObjectId> ringerIds;
            std::set<Duco::ObjectId> conductorIds;

            std::map<Duco::ObjectId, Duco::PealLengthInfo> towerIds;
            std::pair<Duco::ObjectId, Duco::PealLengthInfo> firstTowerInfo(firstTowerId, Duco::PealLengthInfo(Peal(KNoId, KNoId, firstTowerId, KNoId, KNoId, L"", L"", PerformanceDate(1, 1, 1), PerformanceTime(1), 5001, false, ringerIds, TConductorType::ESingleConductor, conductorIds, 0)));
            towerIds.insert(firstTowerInfo);
            std::pair<Duco::ObjectId, Duco::PealLengthInfo> secondTowerInfo(secondTowerId, Duco::PealLengthInfo(Peal(KNoId, KNoId, firstTowerId, KNoId, KNoId, L"", L"", PerformanceDate(1, 1, 1), PerformanceTime(2), 5002, false, ringerIds, TConductorType::ESingleConductor, conductorIds, 0)));
            towerIds.insert(secondTowerInfo);
            std::pair<Duco::ObjectId, Duco::PealLengthInfo> thirdTowerInfo(thirdTowerId, Duco::PealLengthInfo(Peal(KNoId, KNoId, firstTowerId, KNoId, KNoId, L"", L"", PerformanceDate(1, 1, 1), PerformanceTime(3), 5003, false, ringerIds, TConductorType::ESingleConductor, conductorIds, 0)));
            towerIds.insert(thirdTowerInfo);
            std::pair<Duco::ObjectId, Duco::PealLengthInfo> fourthTowerInfo(fourthTowerId, Duco::PealLengthInfo(Peal(KNoId, KNoId, firstTowerId, KNoId, KNoId, L"", L"", PerformanceDate(1, 1, 1), PerformanceTime(4), 5004, false, ringerIds, TConductorType::ESingleConductor, conductorIds, 0)));
            towerIds.insert(fourthTowerInfo);

            Assert::AreEqual((size_t)4, towerIds.size());

            database.MergeTowersWithSameLocation(towerIds);

            // After merge
            Assert::AreEqual((size_t)2, towerIds.size());
            Assert::IsTrue(towerIds.find(firstTowerId) != towerIds.end());
            Assert::IsTrue(towerIds.find(secondTowerId) != towerIds.end());
            Assert::IsFalse(towerIds.find(thirdTowerId) != towerIds.end());
            Assert::IsFalse(towerIds.find(fourthTowerId) != towerIds.end());
        }

        TEST_METHOD(TowerDatabase_UppercaseTowerName)
        {
            TowerDatabase database;

            Tower firstTower(KNoId, L"Aaaaa", L"Moulton", L"County", 6);
            Duco::ObjectId firstTowerId = database.AddObject(firstTower);

            Tower secondTower(KNoId, L"St Aaaaa", L"Harpole", L"Another county", 6);
            Duco::ObjectId secondTowerId = database.AddObject(secondTower);

            database.UppercaseCities(NULL);

            const Tower* secondUpdatedTower = database.FindTower(secondTowerId);
            Assert::AreEqual(L"HARPOLE, Another county. (St Aaaaa)", secondUpdatedTower->FullName().c_str());

            const Tower* firstUpdatedTower = database.FindTower(firstTowerId);
            Assert::AreEqual(L"MOULTON, County. (Aaaaa)", firstUpdatedTower->FullName().c_str());
        }

        TEST_METHOD(TowerDatabase_FindBurnley_inLACR2022Database)
        {
            RingingDatabase database;
            const std::string databaseFilename = "LACR peals 2022.duc";
            unsigned int databaseVersion = 0;
            database.Internalise(databaseFilename.c_str(), this, databaseVersion);

            Duco::ObjectId towerId = database.TowersDatabase().SuggestTower(L"St peter", L"burnley", L"Lancashire", 10, L"18-0-8", L"E");
            const Tower* const theTower = database.TowersDatabase().FindTower(towerId);
            Assert::AreEqual(L"BURNLEY", theTower->City().c_str());
        }

        TEST_METHOD(TowerDatabase_FindNorbury)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Tower newTower(KNoId, L"St Thomas", L"Norbury", L"Greater Manchester", 8);
            newTower.AddRing(6, L"14-1-22", L"F♯");
            newTower.AddRing(8, L"14-1-22", L"F♯");
            Duco::ObjectId newTowerId = database.TowersDatabase().AddObject(newTower);

            Tower oldTower(KNoId, L"St Thomas", L"Norbury", L"Greater Manchester", 8);
            oldTower.AddRing(6, L"7-2-2", L"");
            oldTower.AddRing(8, L"7-2-2", L"");
            oldTower.SetRemoved(true);
            oldTower.SetLinkedTowerId(newTowerId);
            Duco::ObjectId oldTowerId = database.TowersDatabase().AddObject(oldTower);

            Duco::ObjectId towerId = database.TowersDatabase().SuggestTower(L"St Thomas", L"Norbury", L"Greater Manchester", 8, L"14–1–22", L"F♯");
            Assert::AreEqual(newTowerId, towerId);

            towerId = database.TowersDatabase().SuggestTower(L"St Thomas", L"Norbury", L"Greater Manchester", 8, L"7-2-2", L"");
            Assert::AreEqual(oldTowerId, towerId);
        }

        TEST_METHOD(TowerDatabase_FindNorburyByTowerbaseId)
        {
            std::wstring towerbaseId = L"11240";
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Tower newTower(KNoId, L"St Thomas", L"Norbury", L"Greater Manchester", 8);
            newTower.AddRing(6, L"14-1-22", L"F♯");
            newTower.AddRing(8, L"14-1-22", L"F♯");
            newTower.SetTowerbaseId(towerbaseId);
            Duco::ObjectId newTowerId = database.TowersDatabase().AddObject(newTower);

            Tower oldTower(KNoId, L"St Thomas", L"Norbury", L"Greater Manchester", 8);
            oldTower.AddRing(6, L"7-2-2", L"");
            oldTower.AddRing(8, L"7-2-2", L"");
            oldTower.SetRemoved(true);
            oldTower.SetTowerbaseId(towerbaseId);
            oldTower.SetLinkedTowerId(newTowerId);
            Duco::ObjectId oldTowerId = database.TowersDatabase().AddObject(oldTower);

            Duco::ObjectId towerId = database.TowersDatabase().FindTowerByTowerbaseId(towerbaseId, 8, L"14–1–22", L"F♯");
            Assert::AreEqual(newTowerId, towerId);

            towerId = database.TowersDatabase().FindTowerByTowerbaseId(towerbaseId, 8, L"7-2-2", L"");
            Assert::AreEqual(oldTowerId, towerId);
        }

        TEST_METHOD(TowerDatabase_MapCentre_IOW)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            const std::string tempFilename = "all_isle_of_wight.duc";
            unsigned int dbVer;
            database.Internalise(tempFilename.c_str(), this, dbVer);

            Assert::AreEqual(44u, dbVer);
            size_t noOfLinkedTowers;
            Assert::AreEqual((size_t)14, database.NumberOfActiveTowers(noOfLinkedTowers));
            Assert::AreEqual((size_t)0, noOfLinkedTowers);

            std::map<Duco::ObjectId, Duco::PealLengthInfo> filteredTowerIds;
            std::set<Duco::ObjectId> allTowerIds;
            std::multimap<Duco::ObjectId, Duco::ObjectId> allRingIds;
            database.TowersDatabase().GetAllObjectIds(allTowerIds, allRingIds);
            std::set<Duco::ObjectId>::const_iterator it = allTowerIds.begin();
            while (it != allTowerIds.end())
            {
                std::pair<Duco::ObjectId, Duco::PealLengthInfo> newPair(*it, Duco::PealLengthInfo());
                filteredTowerIds.insert(newPair);
                ++it;
            }

            double latitudeMiddle;
            double longitudeMiddle;
            int zoomLevel;
            Assert::IsTrue(database.MapBoxZoomAndMiddle(filteredTowerIds, latitudeMiddle, longitudeMiddle, zoomLevel));
            Assert::AreEqual(11, zoomLevel);
            Assert::AreEqual(-1.326670, longitudeMiddle, 0.001);
            Assert::AreEqual(50.657891, latitudeMiddle, 0.001);
        }

        TEST_METHOD(TowerDatabase_JohnThurman_ConvertTowerbaseIdOnImport)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            const std::string tempFilename = "john_m_thurman_DBVer_44.duc";
            unsigned int dbVer;
            database.Internalise(tempFilename.c_str(), this, dbVer);

            Assert::AreEqual(44u, dbVer);

            const Tower* const birmingham = database.TowersDatabase().FindTower(33);
            Assert::AreEqual(L"492", birmingham->TowerbaseId().c_str());

            Assert::AreEqual(birmingham->Id(), database.TowersDatabase().FindTowerByTowerbaseId(L"492"));
        }

        TEST_METHOD(TowerDatabase_RemoveDuplicateRing)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Tower newTower(KNoId, L"All Saints", L"Stradbroke", L"Suffolk", 10);
            newTower.AddDefaultRing();
            newTower.AddRing(8, L"", L"");
            newTower.AddRing(8, L"", L"");
            Assert::AreEqual((size_t)3, newTower.NoOfRings());

            Duco::ObjectId newTowerId = database.TowersDatabase().AddObject(newTower);

            database.PealsDatabase().RemoveDuplicateRings(this);


            const Tower* const theTower = database.TowersDatabase().FindTower(newTowerId);
            Assert::AreEqual((size_t)2, theTower->NoOfRings());
        }

#ifndef _DEBUG
        TEST_METHOD(TowerDatabase_IndexPerformanceTest)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            for (int i = 0; i < 5000; ++i)
            {
                std::wstring name = DucoEngineUtils::ToString(i);
                Tower newTower(KNoId, name, name, name, 10);
                newTower.AddDefaultRing();
                database.TowersDatabase().AddObject(newTower);
            }
            Assert::AreEqual((size_t)5000, database.TowersDatabase().NumberOfObjects());

            time_point<high_resolution_clock> start = high_resolution_clock::now();
            for (const size_t id : {2, 4, 6, 8, 500, 750, 999, 2500})
            {
                ObjectId towerToFind(id);
                database.TowersDatabase().FindTower(towerToFind, true);
                database.TowersDatabase().FindTower(towerToFind, true);
            }
            auto stop = high_resolution_clock::now();
            auto duration = duration_cast<microseconds>(stop - start);

            Assert::IsTrue(duration.count() < 1000, L"Less than one second");
            Assert::IsTrue(duration.count() < 800, L"Less than 800 ms");
            Assert::IsTrue(duration.count() < 700, L"Less than 700 ms");
            Assert::IsTrue(duration.count() < 600, L"Less than 600 ms");
            Assert::IsTrue(duration.count() < 450, L"Less than 450 ms");
            Assert::IsTrue(duration.count() < 350, L"Less than 350 ms");
            Assert::IsTrue(duration.count() < 300, L"Less than 300 ms");
            Assert::IsTrue(duration.count() < 250, L"Less than 250 ms");
            Assert::IsTrue(duration.count() < 200, L"Less than 200 ms");
            Assert::IsTrue(duration.count() < 150, L"Less than 150 ms");
            Assert::IsTrue(duration.count() < 100, L"Less than 100 ms");
            Assert::IsTrue(duration.count() < 50, L"Less than 50 ms");
        }
#endif
        std::wstring bowdonTowerbaseId = L"616";
        std::wstring bowdonDoveId = L"14100";
        void CreateTowersForTests(RingingDatabase& database, ObjectId& oldBellsId, ObjectId& newBellsId)
        {
            database.ClearDatabase(false, true);

            Tower oldBells(KNoId, L"St Mary the Virgin (Old Bells)", L"Bowdon", L"Greater Manchester", 8);
            oldBells.AddRing(8, L"14-1-26", L"");
            oldBells.SetRemoved(true);
            oldBells.SetTowerbaseId(bowdonTowerbaseId);
            oldBells.SetDoveRef(bowdonDoveId);
            oldBellsId = database.TowersDatabase().AddObject(oldBells);

            Tower newBells(KNoId, L"St Mary the Virgin", L"Bowdon", L"Greater Manchester", 8);
            newBells.AddRing(8, L"18-0-6", L"E");
            newBells.SetTowerbaseId(bowdonTowerbaseId);
            newBells.SetDoveRef(bowdonDoveId);
            newBellsId = database.TowersDatabase().AddObject(newBells);

        }
        TEST_METHOD(TowerDatabase_FindTowerByDoveId_FindOldRemovedBells)
        {
            Duco::ObjectId oldBellsId;
            Duco::ObjectId newBellsId;
            RingingDatabase database;
            CreateTowersForTests(database, oldBellsId, newBellsId);
            Duco::ObjectId foundTowerId = database.TowersDatabase().FindTowerByDoveId(bowdonDoveId, 8, L"14-1-26", L"");
            Assert::AreEqual(oldBellsId, foundTowerId);
        }
        TEST_METHOD(TowerDatabase_FindTowerByDoveId_FindNewBells)
        {
            Duco::ObjectId oldBellsId;
            Duco::ObjectId newBellsId;
            RingingDatabase database;
            CreateTowersForTests(database, oldBellsId, newBellsId);
            Duco::ObjectId foundTowerId = database.TowersDatabase().FindTowerByDoveId(bowdonDoveId, 8, L"18-0-6", L"E");
            Assert::AreEqual(newBellsId, foundTowerId);
        }
        TEST_METHOD(TowerDatabase_FindTowerByDoveId_FindNewBells_NoDetails)
        {
            Duco::ObjectId oldBellsId;
            Duco::ObjectId newBellsId;
            RingingDatabase database;
            CreateTowersForTests(database, oldBellsId, newBellsId);
            Duco::ObjectId foundTowerId = database.TowersDatabase().FindTowerByDoveId(bowdonDoveId, 8, L"", L"");
            Assert::AreEqual(newBellsId, foundTowerId);
        }

        TEST_METHOD(TowerDatabase_FindTowerByTowerbaseId_FindOldRemovedBells)
        {
            Duco::ObjectId oldBellsId;
            Duco::ObjectId newBellsId;
            RingingDatabase database;
            CreateTowersForTests(database, oldBellsId, newBellsId);
            Duco::ObjectId foundTowerId = database.TowersDatabase().FindTowerByTowerbaseId(bowdonTowerbaseId, 8, L"14-1-26", L"");
            Assert::AreEqual(oldBellsId, foundTowerId);
        }
        TEST_METHOD(TowerDatabase_FindTowerByTowerbaseId_FindNewBells)
        {
            Duco::ObjectId oldBellsId;
            Duco::ObjectId newBellsId;
            RingingDatabase database;
            CreateTowersForTests(database, oldBellsId, newBellsId);
            Duco::ObjectId foundTowerId = database.TowersDatabase().FindTowerByTowerbaseId(bowdonTowerbaseId, 8, L"18-0-6", L"E");
            Assert::AreEqual(newBellsId, foundTowerId);
        }
        TEST_METHOD(TowerDatabase_FindTowerByTowerbaseId_FindNewBells_NoDetails)
        {
            Duco::ObjectId oldBellsId;
            Duco::ObjectId newBellsId;
            RingingDatabase database;
            CreateTowersForTests(database, oldBellsId, newBellsId);
            Duco::ObjectId foundTowerId = database.TowersDatabase().FindTowerByTowerbaseId(bowdonTowerbaseId, 8, L"", L"");
            Assert::AreEqual(newBellsId, foundTowerId);
        }
    };
}
