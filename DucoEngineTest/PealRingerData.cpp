#include "CppUnitTest.h"
#include <PealRingerData.h>
#include "ToString.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(PealRingerData_UnitTests)
    {
    public:

        TEST_METHOD(PealRingerData_DefaultConstructor)
        {
            PealRingerData data;

            Assert::AreEqual((unsigned int)0, data.PealFee());
            Assert::AreEqual(Duco::ObjectId(), data.RingerId());
            Assert::IsFalse(data.FullyPaid());
        }

        TEST_METHOD(PealRingerData_Constructor)
        {
            PealRingerData data (10, 150, true);

            Assert::AreEqual((unsigned int)150, data.PealFee());
            Assert::AreEqual(Duco::ObjectId(10), data.RingerId());
            Assert::IsTrue(data.FullyPaid());
        }

        TEST_METHOD(PealRingerData_CopyConstructor)
        {
            PealRingerData dataOriginal(20, 250, true);
            PealRingerData data(dataOriginal);

            Assert::AreEqual((unsigned int)250, data.PealFee());
            Assert::AreEqual(Duco::ObjectId(20), data.RingerId());
            Assert::IsTrue(data.FullyPaid());
        }

        TEST_METHOD(PealRingerData_DefaultConstructorAndSettors)
        {
            PealRingerData data;
            data.SetPealFee(150);
            data.SetRingerId(15);
            data.SetFullyPaid(true);

            Assert::AreEqual((unsigned int)150, data.PealFee());
            Assert::AreEqual(Duco::ObjectId(15), data.RingerId());
            Assert::IsTrue(data.FullyPaid());
        }

        TEST_METHOD(PealRingerData_CopyOperator)
        {
            PealRingerData dataOriginal(20, 250, true);
            PealRingerData data = dataOriginal;

            Assert::AreEqual((unsigned int)250, data.PealFee());
            Assert::AreEqual(Duco::ObjectId(20), data.RingerId());
            Assert::IsTrue(data.FullyPaid());
            Assert::IsTrue(data == dataOriginal);
        }
    };
}
