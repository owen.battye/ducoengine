#include "CppUnitTest.h"
#include <AlphabetData.h>
#include <AlphabetLetter.h>
#include <AlphabetObject.h>
#include <Method.h>
#include <MethodDatabase.h>
#include <Peal.h>
#include <PealDatabase.h>
#include <RingingDatabase.h>
#include "ToString.h"
#include <StatisticFilters.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(Alphabets_UnitTests)
    {
    public:
        TEST_METHOD(Alphabets_None)
        {
            PerformanceDate earliestDate;
            earliestDate.ResetToEarliest();

            RingingDatabase database;
            Duco::StatisticFilters filters(database);
            Duco::AlphabetData alphabets;
            Assert::AreEqual((unsigned int)0, database.PealsDatabase().GetAlphabeticCompletions(filters, false, false, alphabets));
            Assert::AreEqual((unsigned int)0, alphabets.NumberOfCompletedAlphabets());

            Duco::PerformanceDate circledDate;
            std::set<ObjectId> pealIds;
            Assert::IsFalse(alphabets.Completed(0, circledDate, pealIds));
            Assert::AreEqual(earliestDate, circledDate);
            Assert::AreEqual((size_t)0, pealIds.size());

            Assert::IsFalse(alphabets.Completed(1, circledDate, pealIds));
            Assert::AreEqual(earliestDate, circledDate);
            Assert::AreEqual((size_t)0, pealIds.size());
        }

        TEST_METHOD(Alphabets_SimpleSinglePealTest)
        {
            PerformanceDate earliestDate;
            earliestDate.ResetToEarliest();

            RingingDatabase database;
            Duco::ObjectId aRoyalMethod = database.MethodsDatabase().AddMethod(L"Aasgsg", L"Surprise", 10, false);

            Peal newPeal;
            newPeal.SetMethodId(aRoyalMethod);
            database.PealsDatabase().AddObject(newPeal);

            Duco::StatisticFilters filters(database);
            Duco::AlphabetData alphabets;
            Assert::AreEqual((unsigned int)0, database.PealsDatabase().GetAlphabeticCompletions(filters, false, false, alphabets));
            Assert::AreEqual((unsigned int)0, alphabets.NumberOfCompletedAlphabets());
            Assert::AreEqual((size_t)1, alphabets['a'].Count());
            Assert::AreEqual((size_t)0, alphabets['b'].Count());
            Assert::AreEqual((size_t)0, alphabets['c'].Count());
            Assert::AreEqual((size_t)0, alphabets['d'].Count());
            Assert::AreEqual((size_t)0, alphabets['e'].Count());
            Assert::AreEqual((size_t)0, alphabets['f'].Count());
            Assert::AreEqual((size_t)0, alphabets['g'].Count());
            Assert::AreEqual((size_t)0, alphabets['h'].Count());
            Assert::AreEqual((size_t)0, alphabets['i'].Count());
            Assert::AreEqual((size_t)0, alphabets['j'].Count());
            Assert::AreEqual((size_t)0, alphabets['k'].Count());
            Assert::AreEqual((size_t)0, alphabets['l'].Count());
            Assert::AreEqual((size_t)0, alphabets['m'].Count());
            Assert::AreEqual((size_t)0, alphabets['n'].Count());
            Assert::AreEqual((size_t)0, alphabets['o'].Count());
            Assert::AreEqual((size_t)0, alphabets['p'].Count());
            Assert::AreEqual((size_t)0, alphabets['q'].Count());
            Assert::AreEqual((size_t)0, alphabets['r'].Count());
            Assert::AreEqual((size_t)0, alphabets['s'].Count());
            Assert::AreEqual((size_t)0, alphabets['t'].Count());
            Assert::AreEqual((size_t)0, alphabets['u'].Count());
            Assert::AreEqual((size_t)0, alphabets['v'].Count());
            Assert::AreEqual((size_t)0, alphabets['w'].Count());
            Assert::AreEqual((size_t)0, alphabets['x'].Count());
            Assert::AreEqual((size_t)0, alphabets['y'].Count());
            Assert::AreEqual((size_t)0, alphabets['z'].Count());

            Duco::PerformanceDate circledDate;
            std::set<ObjectId> pealIds;
            Assert::IsFalse(alphabets.Completed(0, circledDate, pealIds));
            Assert::AreEqual(earliestDate, circledDate);
            Assert::AreEqual((size_t)1, pealIds.size());

            Assert::IsFalse(alphabets.Completed(1, circledDate, pealIds));
            Assert::AreEqual(earliestDate, circledDate);
            Assert::AreEqual((size_t)0, pealIds.size());
        }

        TEST_METHOD(Alphabets_MultipleLetters_UniqueMethods)
        {
            RingingDatabase database;
            Duco::ObjectId firstRoyalMethod = database.MethodsDatabase().AddMethod(L"Aasgsg", L"Surprise", 10, false);
            Duco::ObjectId secondRoyalMethod = database.MethodsDatabase().AddMethod(L"Asfksjgj", L"Alliance", 10, false);

            Peal firstPeal;
            firstPeal.SetDate(PerformanceDate(2001, 1, 1));
            firstPeal.SetMethodId(firstRoyalMethod);
            Duco::ObjectId firstPealId = database.PealsDatabase().AddObject(firstPeal);

            Peal secondPeal;
            secondPeal.SetDate(PerformanceDate(2001, 2, 1));
            secondPeal.SetMethodId(firstRoyalMethod);
            database.PealsDatabase().AddObject(secondPeal);

            Peal thirdPeal;
            thirdPeal.SetDate(PerformanceDate(2001, 3, 1));
            thirdPeal.SetMethodId(secondRoyalMethod);
            Duco::ObjectId thirdPealId = database.PealsDatabase().AddObject(thirdPeal);

            Peal fourthPeal;
            fourthPeal.SetDate(PerformanceDate(2001, 4, 1));
            fourthPeal.SetMethodId(firstRoyalMethod);
            database.PealsDatabase().AddObject(fourthPeal);

            Duco::StatisticFilters filters(database);
            Duco::AlphabetData alphabets;
            Assert::AreEqual((unsigned int)0, database.PealsDatabase().GetAlphabeticCompletions(filters, false, false, alphabets));
            Assert::AreEqual((unsigned int)0, alphabets.NumberOfCompletedAlphabets());
            Assert::AreEqual((size_t)4, alphabets['a'].Count());
            Assert::AreEqual((size_t)0, alphabets['b'].Count());
            Assert::AreEqual((size_t)0, alphabets['c'].Count());
            Assert::AreEqual((size_t)0, alphabets['d'].Count());
            Assert::AreEqual((size_t)0, alphabets['e'].Count());
            Assert::AreEqual((size_t)0, alphabets['f'].Count());
            Assert::AreEqual((size_t)0, alphabets['g'].Count());
            Assert::AreEqual((size_t)0, alphabets['h'].Count());
            Assert::AreEqual((size_t)0, alphabets['i'].Count());
            Assert::AreEqual((size_t)0, alphabets['j'].Count());
            Assert::AreEqual((size_t)0, alphabets['k'].Count());
            Assert::AreEqual((size_t)0, alphabets['l'].Count());
            Assert::AreEqual((size_t)0, alphabets['m'].Count());
            Assert::AreEqual((size_t)0, alphabets['n'].Count());
            Assert::AreEqual((size_t)0, alphabets['o'].Count());
            Assert::AreEqual((size_t)0, alphabets['p'].Count());
            Assert::AreEqual((size_t)0, alphabets['q'].Count());
            Assert::AreEqual((size_t)0, alphabets['r'].Count());
            Assert::AreEqual((size_t)0, alphabets['s'].Count());
            Assert::AreEqual((size_t)0, alphabets['t'].Count());
            Assert::AreEqual((size_t)0, alphabets['u'].Count());
            Assert::AreEqual((size_t)0, alphabets['v'].Count());
            Assert::AreEqual((size_t)0, alphabets['w'].Count());
            Assert::AreEqual((size_t)0, alphabets['x'].Count());
            Assert::AreEqual((size_t)0, alphabets['y'].Count());
            Assert::AreEqual((size_t)0, alphabets['z'].Count());
            Assert::AreEqual(firstRoyalMethod, alphabets['a'][0].MethodId());
            Assert::AreEqual(firstPealId, alphabets['a'][0].PealId());
            Assert::AreEqual(firstRoyalMethod, alphabets['a'][1].MethodId());
            Assert::AreEqual(secondRoyalMethod, alphabets['a'][2].MethodId());
            Assert::AreEqual(thirdPealId, alphabets['a'][2].PealId());
            Assert::AreEqual(firstRoyalMethod, alphabets['a'][3].MethodId());

            Assert::AreEqual((unsigned int)0, database.PealsDatabase().GetAlphabeticCompletions(filters, true, false, alphabets));
            Assert::AreEqual((unsigned int)0, alphabets.NumberOfCompletedAlphabets());
            Assert::AreEqual((size_t)2, alphabets['a'].Count());
            Assert::AreEqual(firstRoyalMethod, alphabets['a'][0].MethodId());
            Assert::AreEqual(secondRoyalMethod, alphabets['a'][1].MethodId());
            Assert::AreEqual(firstPealId, alphabets['a'][0].PealId());
            Assert::AreEqual(thirdPealId, alphabets['a'][1].PealId());
        }

        TEST_METHOD(Alphabets_InsideOnly)
        {
            {
                RingingDatabase database;
                Duco::ObjectId firstMethod = database.MethodsDatabase().AddMethod(L"Aasgsg", L"Surprise", 10, false);
                Method stedmanCaters(2, 9, L"Stedman", L"", L"3.1.9.3.1.3.1.3.9.1.3.1", false);
                Duco::ObjectId secondMethod = database.MethodsDatabase().AddObject(stedmanCaters);
                Duco::ObjectId thirdMethod = database.MethodsDatabase().AddMethod(L"fffff", L"Surprise", 8, false);

                Duco::ObjectId ringerId = 10;
                Peal firstPeal;
                firstPeal.SetRingerId(ringerId, 2, false);
                firstPeal.SetDate(PerformanceDate(2001, 1, 1));
                firstPeal.SetMethodId(firstMethod);
                Duco::ObjectId firstPealId = database.PealsDatabase().AddObject(firstPeal);

                Peal secondPeal;
                secondPeal.SetRingerId(ringerId, 1, false);
                secondPeal.SetDate(PerformanceDate(2001, 2, 1));
                secondPeal.SetMethodId(secondMethod);
                Duco::ObjectId secondPealId = database.PealsDatabase().AddObject(secondPeal);

                Peal thirdPeal;
                thirdPeal.SetRingerId(ringerId, 1, false);
                thirdPeal.SetDate(PerformanceDate(2000, 1, 1));
                thirdPeal.SetMethodId(thirdMethod);
                Duco::ObjectId thirdPealId = database.PealsDatabase().AddObject(thirdPeal);

                Duco::StatisticFilters filters(database);
                filters.SetRinger(true, ringerId);
                Duco::AlphabetData alphabets;
                Assert::AreEqual((unsigned int)0, database.PealsDatabase().GetAlphabeticCompletions(filters, false, false, alphabets));
                Assert::AreEqual((unsigned int)0, alphabets.NumberOfCompletedAlphabets());
                Assert::AreEqual((size_t)1, alphabets['a'].Count());
                Assert::AreEqual((size_t)0, alphabets['b'].Count());
                Assert::AreEqual((size_t)0, alphabets['c'].Count());
                Assert::AreEqual((size_t)0, alphabets['d'].Count());
                Assert::AreEqual((size_t)0, alphabets['e'].Count());
                Assert::AreEqual((size_t)1, alphabets['f'].Count());
                Assert::AreEqual((size_t)0, alphabets['g'].Count());
                Assert::AreEqual((size_t)0, alphabets['h'].Count());
                Assert::AreEqual((size_t)0, alphabets['i'].Count());
                Assert::AreEqual((size_t)0, alphabets['j'].Count());
                Assert::AreEqual((size_t)0, alphabets['k'].Count());
                Assert::AreEqual((size_t)0, alphabets['l'].Count());
                Assert::AreEqual((size_t)0, alphabets['m'].Count());
                Assert::AreEqual((size_t)0, alphabets['n'].Count());
                Assert::AreEqual((size_t)0, alphabets['o'].Count());
                Assert::AreEqual((size_t)0, alphabets['p'].Count());
                Assert::AreEqual((size_t)0, alphabets['q'].Count());
                Assert::AreEqual((size_t)0, alphabets['r'].Count());
                Assert::AreEqual((size_t)1, alphabets['s'].Count());
                Assert::AreEqual((size_t)0, alphabets['t'].Count());
                Assert::AreEqual((size_t)0, alphabets['u'].Count());
                Assert::AreEqual((size_t)0, alphabets['v'].Count());
                Assert::AreEqual((size_t)0, alphabets['w'].Count());
                Assert::AreEqual((size_t)0, alphabets['x'].Count());
                Assert::AreEqual((size_t)0, alphabets['y'].Count());
                Assert::AreEqual((size_t)0, alphabets['z'].Count());
                Assert::AreEqual(firstMethod, alphabets['a'][0].MethodId());
                Assert::AreEqual(firstPealId, alphabets['a'][0].PealId());
                Assert::AreEqual(secondMethod, alphabets['s'][0].MethodId());
                Assert::AreEqual(secondPealId, alphabets['s'][0].PealId());

                Assert::AreEqual((unsigned int)0, database.PealsDatabase().GetAlphabeticCompletions(filters, true, true, alphabets));
                Assert::AreEqual((unsigned int)0, alphabets.NumberOfCompletedAlphabets());
                Assert::AreEqual((size_t)1, alphabets['a'].Count());
                Assert::AreEqual((size_t)0, alphabets['f'].Count());
                Assert::AreEqual((size_t)1, alphabets['s'].Count());
                Assert::AreEqual(firstMethod, alphabets['a'][0].MethodId());
            }
        }

        TEST_METHOD(Alphabets_SortPeals)
        {
            RingingDatabase database;
            Duco::ObjectId firstRoyalMethod = database.MethodsDatabase().AddMethod(L"Aasgsg", L"Surprise", 10, false);

            Peal firstPeal;
            firstPeal.SetDate(PerformanceDate(2001, 1, 1));
            firstPeal.SetMethodId(firstRoyalMethod);
            Duco::ObjectId firstPealId = database.PealsDatabase().AddObject(firstPeal);

            Peal secondPeal;
            secondPeal.SetDate(PerformanceDate(2002, 2, 1));
            secondPeal.SetMethodId(firstRoyalMethod);
            Duco::ObjectId secondPealId = database.PealsDatabase().AddObject(secondPeal);

            Peal thirdPeal;
            thirdPeal.SetDate(PerformanceDate(2001, 3, 1));
            thirdPeal.SetMethodId(firstRoyalMethod);
            Duco::ObjectId thirdPealId = database.PealsDatabase().AddObject(thirdPeal);

            Duco::StatisticFilters filters(database);
            Duco::AlphabetData alphabets;
            Assert::AreEqual((unsigned int)0, database.PealsDatabase().GetAlphabeticCompletions(filters, false, false, alphabets));
            Assert::AreEqual((unsigned int)0, alphabets.NumberOfCompletedAlphabets());
            Assert::AreEqual((size_t)3, alphabets['a'].Count());
            Assert::AreEqual((size_t)0, alphabets['b'].Count());
            Assert::AreEqual((size_t)0, alphabets['c'].Count());
            Assert::AreEqual((size_t)0, alphabets['d'].Count());
            Assert::AreEqual((size_t)0, alphabets['e'].Count());
            Assert::AreEqual((size_t)0, alphabets['f'].Count());
            Assert::AreEqual((size_t)0, alphabets['g'].Count());
            Assert::AreEqual((size_t)0, alphabets['h'].Count());
            Assert::AreEqual((size_t)0, alphabets['i'].Count());
            Assert::AreEqual((size_t)0, alphabets['j'].Count());
            Assert::AreEqual((size_t)0, alphabets['k'].Count());
            Assert::AreEqual((size_t)0, alphabets['l'].Count());
            Assert::AreEqual((size_t)0, alphabets['m'].Count());
            Assert::AreEqual((size_t)0, alphabets['n'].Count());
            Assert::AreEqual((size_t)0, alphabets['o'].Count());
            Assert::AreEqual((size_t)0, alphabets['p'].Count());
            Assert::AreEqual((size_t)0, alphabets['q'].Count());
            Assert::AreEqual((size_t)0, alphabets['r'].Count());
            Assert::AreEqual((size_t)0, alphabets['s'].Count());
            Assert::AreEqual((size_t)0, alphabets['t'].Count());
            Assert::AreEqual((size_t)0, alphabets['u'].Count());
            Assert::AreEqual((size_t)0, alphabets['v'].Count());
            Assert::AreEqual((size_t)0, alphabets['w'].Count());
            Assert::AreEqual((size_t)0, alphabets['x'].Count());
            Assert::AreEqual((size_t)0, alphabets['y'].Count());
            Assert::AreEqual((size_t)0, alphabets['z'].Count());
            Assert::AreEqual(firstRoyalMethod, alphabets['a'][0].MethodId());
            Assert::AreEqual(firstRoyalMethod, alphabets['a'][1].MethodId());
            Assert::AreEqual(firstRoyalMethod, alphabets['a'][2].MethodId());
            Assert::AreEqual(firstPealId, alphabets['a'][0].PealId());
            Assert::AreEqual(thirdPealId, alphabets['a'][1].PealId());
            Assert::AreEqual(secondPealId, alphabets['a'][2].PealId());
        }
        TEST_METHOD(Alphabets_CompletedAlphabet)
        {
            RingingDatabase database;
            Duco::ObjectId aRoyalMethod = database.MethodsDatabase().AddMethod(L"Aa", L"Surprise", 10, false);
            Duco::ObjectId bRoyalMethod = database.MethodsDatabase().AddMethod(L"Bb", L"Surprise", 10, false);
            Duco::ObjectId cRoyalMethod = database.MethodsDatabase().AddMethod(L"Cc", L"Surprise", 10, false);
            Duco::ObjectId dRoyalMethod = database.MethodsDatabase().AddMethod(L"Dd", L"Surprise", 10, false);
            Duco::ObjectId eRoyalMethod = database.MethodsDatabase().AddMethod(L"Ee", L"Surprise", 10, false);
            Duco::ObjectId fRoyalMethod = database.MethodsDatabase().AddMethod(L"Ff", L"Surprise", 10, false);
            Duco::ObjectId gRoyalMethod = database.MethodsDatabase().AddMethod(L"Gg", L"Surprise", 10, false);
            Duco::ObjectId hRoyalMethod = database.MethodsDatabase().AddMethod(L"Hh", L"Surprise", 10, false);
            Duco::ObjectId iRoyalMethod = database.MethodsDatabase().AddMethod(L"iI", L"Surprise", 10, false);
            Duco::ObjectId jRoyalMethod = database.MethodsDatabase().AddMethod(L"Jj", L"Surprise", 10, false);
            Duco::ObjectId kRoyalMethod = database.MethodsDatabase().AddMethod(L"Kk", L"Surprise", 10, false);
            Duco::ObjectId lRoyalMethod = database.MethodsDatabase().AddMethod(L"lL", L"Surprise", 10, false);
            Duco::ObjectId mRoyalMethod = database.MethodsDatabase().AddMethod(L"Mm", L"Surprise", 10, false);
            Duco::ObjectId nRoyalMethod = database.MethodsDatabase().AddMethod(L"Nn", L"Surprise", 10, false);
            Duco::ObjectId oRoyalMethod = database.MethodsDatabase().AddMethod(L"Oo", L"Surprise", 10, false);
            Duco::ObjectId pRoyalMethod = database.MethodsDatabase().AddMethod(L"Pp", L"Surprise", 10, false);
            Duco::ObjectId qRoyalMethod = database.MethodsDatabase().AddMethod(L"Qq", L"Surprise", 10, false);
            Duco::ObjectId rRoyalMethod = database.MethodsDatabase().AddMethod(L"Rr", L"Surprise", 10, false);
            Duco::ObjectId sRoyalMethod = database.MethodsDatabase().AddMethod(L"Ss", L"Surprise", 10, false);
            Duco::ObjectId tRoyalMethod = database.MethodsDatabase().AddMethod(L"Tt", L"Surprise", 10, false);
            Duco::ObjectId uRoyalMethod = database.MethodsDatabase().AddMethod(L"uU", L"Surprise", 10, false);
            Duco::ObjectId vRoyalMethod = database.MethodsDatabase().AddMethod(L"vV", L"Surprise", 10, false);
            Duco::ObjectId wRoyalMethod = database.MethodsDatabase().AddMethod(L"Ww", L"Surprise", 10, false);
            Duco::ObjectId xRoyalMethod = database.MethodsDatabase().AddMethod(L"Xx", L"Surprise", 10, false);
            Duco::ObjectId yRoyalMethod = database.MethodsDatabase().AddMethod(L"Yy", L"Surprise", 10, false);
            Duco::ObjectId zRoyalMethod = database.MethodsDatabase().AddMethod(L"Zz", L"Surprise", 10, false);

            Duco::PerformanceDate lastpealDate(2005, 24, 9);
            Peal thePeal;
            thePeal.SetDate(PerformanceDate(2001, 1, 1));
            thePeal.SetMethodId(aRoyalMethod);
            Duco::ObjectId aPealId = database.PealsDatabase().AddObject(thePeal);
            thePeal.SetDate(PerformanceDate(2001, 1, 2));
            thePeal.SetMethodId(bRoyalMethod);
            Duco::ObjectId bPealId = database.PealsDatabase().AddObject(thePeal);
            thePeal.SetDate(PerformanceDate(2001, 1, 2));
            thePeal.SetMethodId(cRoyalMethod);
            Duco::ObjectId cPealId = database.PealsDatabase().AddObject(thePeal);
            thePeal.SetDate(PerformanceDate(2001, 1, 2));
            thePeal.SetMethodId(dRoyalMethod);
            Duco::ObjectId dPealId = database.PealsDatabase().AddObject(thePeal);
            thePeal.SetDate(PerformanceDate(2001, 1, 2));
            thePeal.SetMethodId(eRoyalMethod);
            Duco::ObjectId ePealId = database.PealsDatabase().AddObject(thePeal);
            thePeal.SetDate(PerformanceDate(2001, 1, 2));
            thePeal.SetMethodId(fRoyalMethod);
            Duco::ObjectId fPealId = database.PealsDatabase().AddObject(thePeal);
            thePeal.SetDate(PerformanceDate(2001, 1, 2));
            thePeal.SetMethodId(gRoyalMethod);
            Duco::ObjectId gPealId = database.PealsDatabase().AddObject(thePeal);
            thePeal.SetDate(PerformanceDate(2001, 1, 2));
            thePeal.SetMethodId(hRoyalMethod);
            Duco::ObjectId hPealId = database.PealsDatabase().AddObject(thePeal);
            thePeal.SetDate(PerformanceDate(2001, 1, 2));
            thePeal.SetMethodId(iRoyalMethod);
            Duco::ObjectId iPealId = database.PealsDatabase().AddObject(thePeal);
            thePeal.SetDate(PerformanceDate(2001, 1, 2));
            thePeal.SetMethodId(jRoyalMethod);
            Duco::ObjectId jPealId = database.PealsDatabase().AddObject(thePeal);
            thePeal.SetDate(PerformanceDate(2001, 1, 2));
            thePeal.SetMethodId(kRoyalMethod);
            Duco::ObjectId kPealId = database.PealsDatabase().AddObject(thePeal);
            thePeal.SetDate(PerformanceDate(2001, 1, 2));
            thePeal.SetMethodId(lRoyalMethod);
            Duco::ObjectId lPealId = database.PealsDatabase().AddObject(thePeal);
            thePeal.SetDate(PerformanceDate(2001, 1, 2));
            thePeal.SetMethodId(mRoyalMethod);
            Duco::ObjectId mPealId = database.PealsDatabase().AddObject(thePeal);
            thePeal.SetDate(PerformanceDate(2001, 1, 2));
            thePeal.SetMethodId(nRoyalMethod);
            Duco::ObjectId nPealId = database.PealsDatabase().AddObject(thePeal);
            thePeal.SetDate(lastpealDate);
            thePeal.SetMethodId(oRoyalMethod);
            Duco::ObjectId oPealId = database.PealsDatabase().AddObject(thePeal);
            thePeal.SetDate(PerformanceDate(2001, 1, 2));
            thePeal.SetMethodId(pRoyalMethod);
            Duco::ObjectId pPealId = database.PealsDatabase().AddObject(thePeal);
            thePeal.SetDate(PerformanceDate(2001, 1, 2));
            thePeal.SetMethodId(qRoyalMethod);
            Duco::ObjectId qPealId = database.PealsDatabase().AddObject(thePeal);
            thePeal.SetDate(PerformanceDate(2001, 1, 2));
            thePeal.SetMethodId(rRoyalMethod);
            Duco::ObjectId rPealId = database.PealsDatabase().AddObject(thePeal);
            thePeal.SetDate(PerformanceDate(2001, 1, 2));
            thePeal.SetMethodId(sRoyalMethod);
            Duco::ObjectId sPealId = database.PealsDatabase().AddObject(thePeal);
            thePeal.SetDate(PerformanceDate(2001, 1, 2));
            thePeal.SetMethodId(tRoyalMethod);
            Duco::ObjectId tPealId = database.PealsDatabase().AddObject(thePeal);
            thePeal.SetDate(PerformanceDate(2001, 1, 2));
            thePeal.SetMethodId(uRoyalMethod);
            Duco::ObjectId uPealId = database.PealsDatabase().AddObject(thePeal);
            thePeal.SetDate(PerformanceDate(2001, 1, 2));
            thePeal.SetMethodId(vRoyalMethod);
            Duco::ObjectId vPealId = database.PealsDatabase().AddObject(thePeal);
            thePeal.SetDate(PerformanceDate(2001, 1, 2));
            thePeal.SetMethodId(wRoyalMethod);
            Duco::ObjectId wPealId = database.PealsDatabase().AddObject(thePeal);
            thePeal.SetDate(PerformanceDate(2001, 1, 2));
            thePeal.SetMethodId(xRoyalMethod);
            Duco::ObjectId xPealId = database.PealsDatabase().AddObject(thePeal);
            thePeal.SetDate(PerformanceDate(2001, 1, 2));
            thePeal.SetMethodId(yRoyalMethod);
            Duco::ObjectId yPealId = database.PealsDatabase().AddObject(thePeal);
            thePeal.SetDate(PerformanceDate(2001, 1, 2));
            thePeal.SetMethodId(zRoyalMethod);
            Duco::ObjectId zPealId = database.PealsDatabase().AddObject(thePeal);

            Duco::StatisticFilters filters(database);
            Duco::AlphabetData alphabets;
            Assert::AreEqual((unsigned int)1, database.PealsDatabase().GetAlphabeticCompletions(filters, false, false, alphabets));
            Assert::AreEqual((unsigned int)1, alphabets.NumberOfCompletedAlphabets());
            Assert::AreEqual((size_t)1, alphabets['a'].Count());
            Assert::AreEqual((size_t)1, alphabets['b'].Count());
            Assert::AreEqual((size_t)1, alphabets['c'].Count());
            Assert::AreEqual((size_t)1, alphabets['d'].Count());
            Assert::AreEqual((size_t)1, alphabets['e'].Count());
            Assert::AreEqual((size_t)1, alphabets['f'].Count());
            Assert::AreEqual((size_t)1, alphabets['g'].Count());
            Assert::AreEqual((size_t)1, alphabets['h'].Count());
            Assert::AreEqual((size_t)1, alphabets['i'].Count());
            Assert::AreEqual((size_t)1, alphabets['j'].Count());
            Assert::AreEqual((size_t)1, alphabets['k'].Count());
            Assert::AreEqual((size_t)1, alphabets['l'].Count());
            Assert::AreEqual((size_t)1, alphabets['m'].Count());
            Assert::AreEqual((size_t)1, alphabets['n'].Count());
            Assert::AreEqual((size_t)1, alphabets['o'].Count());
            Assert::AreEqual((size_t)1, alphabets['p'].Count());
            Assert::AreEqual((size_t)1, alphabets['q'].Count());
            Assert::AreEqual((size_t)1, alphabets['r'].Count());
            Assert::AreEqual((size_t)1, alphabets['s'].Count());
            Assert::AreEqual((size_t)1, alphabets['t'].Count());
            Assert::AreEqual((size_t)1, alphabets['u'].Count());
            Assert::AreEqual((size_t)1, alphabets['v'].Count());
            Assert::AreEqual((size_t)1, alphabets['w'].Count());
            Assert::AreEqual((size_t)1, alphabets['x'].Count());
            Assert::AreEqual((size_t)1, alphabets['y'].Count());
            Assert::AreEqual((size_t)1, alphabets['z'].Count());
            Assert::AreEqual(aRoyalMethod, alphabets['a'][0].MethodId());
            Assert::AreEqual(bRoyalMethod, alphabets['b'][0].MethodId());
            Assert::AreEqual(cRoyalMethod, alphabets['c'][0].MethodId());
            Assert::AreEqual(dRoyalMethod, alphabets['d'][0].MethodId());
            Assert::AreEqual(eRoyalMethod, alphabets['e'][0].MethodId());
            Assert::AreEqual(fRoyalMethod, alphabets['f'][0].MethodId());
            Assert::AreEqual(gRoyalMethod, alphabets['g'][0].MethodId());
            Assert::AreEqual(hRoyalMethod, alphabets['h'][0].MethodId());
            Assert::AreEqual(iRoyalMethod, alphabets['i'][0].MethodId());
            Assert::AreEqual(jRoyalMethod, alphabets['j'][0].MethodId());
            Assert::AreEqual(kRoyalMethod, alphabets['k'][0].MethodId());
            Assert::AreEqual(lRoyalMethod, alphabets['l'][0].MethodId());
            Assert::AreEqual(mRoyalMethod, alphabets['m'][0].MethodId());
            Assert::AreEqual(nRoyalMethod, alphabets['n'][0].MethodId());
            Assert::AreEqual(oRoyalMethod, alphabets['o'][0].MethodId());
            Assert::AreEqual(pRoyalMethod, alphabets['p'][0].MethodId());
            Assert::AreEqual(qRoyalMethod, alphabets['q'][0].MethodId());
            Assert::AreEqual(rRoyalMethod, alphabets['r'][0].MethodId());
            Assert::AreEqual(sRoyalMethod, alphabets['s'][0].MethodId());
            Assert::AreEqual(tRoyalMethod, alphabets['t'][0].MethodId());
            Assert::AreEqual(uRoyalMethod, alphabets['u'][0].MethodId());
            Assert::AreEqual(vRoyalMethod, alphabets['v'][0].MethodId());
            Assert::AreEqual(wRoyalMethod, alphabets['w'][0].MethodId());
            Assert::AreEqual(xRoyalMethod, alphabets['x'][0].MethodId());
            Assert::AreEqual(yRoyalMethod, alphabets['y'][0].MethodId());
            Assert::AreEqual(zRoyalMethod, alphabets['z'][0].MethodId());
            Assert::AreEqual(aPealId, alphabets['a'][0].PealId());
            Assert::AreEqual(bPealId, alphabets['b'][0].PealId());
            Assert::AreEqual(cPealId, alphabets['c'][0].PealId());
            Assert::AreEqual(dPealId, alphabets['d'][0].PealId());
            Assert::AreEqual(ePealId, alphabets['e'][0].PealId());
            Assert::AreEqual(fPealId, alphabets['f'][0].PealId());
            Assert::AreEqual(gPealId, alphabets['g'][0].PealId());
            Assert::AreEqual(hPealId, alphabets['h'][0].PealId());
            Assert::AreEqual(iPealId, alphabets['i'][0].PealId());
            Assert::AreEqual(jPealId, alphabets['j'][0].PealId());
            Assert::AreEqual(kPealId, alphabets['k'][0].PealId());
            Assert::AreEqual(lPealId, alphabets['l'][0].PealId());
            Assert::AreEqual(mPealId, alphabets['m'][0].PealId());
            Assert::AreEqual(nPealId, alphabets['n'][0].PealId());
            Assert::AreEqual(oPealId, alphabets['o'][0].PealId());
            Assert::AreEqual(pPealId, alphabets['p'][0].PealId());
            Assert::AreEqual(qPealId, alphabets['q'][0].PealId());
            Assert::AreEqual(rPealId, alphabets['r'][0].PealId());
            Assert::AreEqual(sPealId, alphabets['s'][0].PealId());
            Assert::AreEqual(tPealId, alphabets['t'][0].PealId());
            Assert::AreEqual(uPealId, alphabets['u'][0].PealId());
            Assert::AreEqual(vPealId, alphabets['v'][0].PealId());
            Assert::AreEqual(wPealId, alphabets['w'][0].PealId());
            Assert::AreEqual(xPealId, alphabets['x'][0].PealId());
            Assert::AreEqual(yPealId, alphabets['y'][0].PealId());
            Assert::AreEqual(zPealId, alphabets['z'][0].PealId());

            Duco::PerformanceDate circledDate;
            std::set<ObjectId> pealIds;
            Assert::IsTrue(alphabets.Completed(0, circledDate, pealIds));
            Assert::AreEqual(lastpealDate, circledDate);
            Assert::AreEqual((size_t)26, pealIds.size());
        }
    };
}
