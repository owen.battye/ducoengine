﻿#include "CppUnitTest.h"
#include <CsvImporter.h>
#include <RingingDatabase.h>
#include <Association.h>
#include <AssociationDatabase.h>
#include <Peal.h>
#include <PealDatabase.h>
#include <TowerDatabase.h>
#include <Tower.h>
#include <MethodDatabase.h>
#include <Method.h>
#include <ProgressCallback.h>
#include <Ring.h>
#include <RingerDatabase.h>
#include <Ringer.h>
#include <SettingsFile.h>
#include <CsvImportFields.h>
#include <StatisticFilters.h>
#include "ToString.h"
#include <DucoEngineUtils.h>
#include <DucoEngineLog.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

#define KDefaultCsvImportSettings "csvimportsettings.config"
#define KCsvWithQuotesImportSettings "csvimportsettings_withquotes.config"

namespace DucoEngineTest
{
    TEST_CLASS(CsvImporter_UnitTests), Duco::ProgressCallback
    {
        Duco::RingingDatabase* database;
        Duco::CsvImporter* importer;
        bool expectedError;

    public:
        CsvImporter_UnitTests()
        {
            importer = NULL;
            database = new Duco::RingingDatabase();
            expectedError = false;
        }

        ~CsvImporter_UnitTests()
        {
            delete database;
        }

        void Initialised()
        {
        }

        void Step(int progressPercent)
        {
        }

        void Complete(bool error)
        {
            Assert::AreEqual(expectedError, error);
        }

        TEST_METHOD(CsvImporter_Import_NonExistingFile)
        {
            expectedError = true;
            Duco::CsvImporter importer(*this, *database, ".", KDefaultCsvImportSettings);
            Assert::IsFalse(importer.Import("wibble.csv"));
        }

        TEST_METHOD(CsvImporter_CreateDefaultSettingsAndCheck)
        {
            expectedError = false;
            Duco::CsvImporter importer(*this, *database, ".", KDefaultCsvImportSettings);

            Duco::SettingsFile settings("", KDefaultCsvImportSettings);

            bool boolSetting;
            Assert::IsTrue(settings.Setting(KEnforceQuotesFieldNo, boolSetting));
            Assert::IsFalse(boolSetting);
            importer.CreateDefaultSettings(settings);
            Assert::IsTrue(settings.Setting(KEnforceQuotesFieldNo, boolSetting));
            Assert::IsTrue(boolSetting);
            Assert::IsTrue(settings.Setting(KDecodeMultiByteFieldNo, boolSetting));
            Assert::IsFalse(boolSetting);

            std::string tempSettingsFilename = "tempSettings.config";
            settings.SetFilename(".", tempSettingsFilename);
            Assert::IsTrue(settings.Save());

            Duco::SettingsFile rereadSettings("", tempSettingsFilename);
            Assert::IsTrue(rereadSettings.Setting(KEnforceQuotesFieldNo, boolSetting));
            Assert::IsTrue(boolSetting);
            Assert::IsTrue(rereadSettings.Setting(KDecodeMultiByteFieldNo, boolSetting));
            Assert::IsFalse(boolSetting);
        }

        TEST_METHOD(CsvImporter_CreatePealBookSettingsAndCheck)
        {
            expectedError = false;
            Duco::CsvImporter importer(*this, *database, ".", KDefaultCsvImportSettings);

            Duco::SettingsFile settings("", KDefaultCsvImportSettings);

            bool boolSetting;
            int intSetting;
            std::wstring stringSettings;
            Assert::IsTrue(settings.Setting(KEnforceQuotesFieldNo, boolSetting));
            Assert::IsFalse(boolSetting);
            Assert::IsTrue(settings.Setting(KTowerTownFieldNo, intSetting));
            Assert::AreEqual(1, intSetting);
            Assert::IsTrue(settings.Setting(KHandbellStringFieldNo, stringSettings));
            Assert::AreEqual(L"HB", stringSettings.c_str());
            Assert::IsFalse(settings.Setting(KRemoveBellNumberFromRingersFieldNo, boolSetting));
            importer.CreateDefaultPealBookSettings(settings);
            Assert::IsTrue(settings.Setting(KEnforceQuotesFieldNo, boolSetting));
            Assert::IsFalse(boolSetting);
            Assert::IsTrue(settings.Setting(KTowerTownFieldNo, intSetting));
            Assert::AreEqual(3, intSetting);
            Assert::IsTrue(settings.Setting(KHandbellStringFieldNo, stringSettings));
            Assert::AreEqual(L"H", stringSettings.c_str());
            Assert::IsTrue(settings.Setting(KRemoveBellNumberFromRingersFieldNo, boolSetting));
            Assert::IsTrue(boolSetting);

            std::string tempSettingsFilename = "tempSettings.config";
            settings.SetFilename(".", tempSettingsFilename);
            Assert::IsTrue(settings.Save());

            Duco::SettingsFile rereadSettings("", tempSettingsFilename);
            Assert::IsTrue(rereadSettings.Setting(KEnforceQuotesFieldNo, boolSetting));
            Assert::IsFalse(boolSetting);
            Assert::IsTrue(rereadSettings.Setting(KTowerTownFieldNo, intSetting));
            Assert::AreEqual(3, intSetting);
            Assert::IsTrue(rereadSettings.Setting(KHandbellStringFieldNo, stringSettings));
            Assert::AreEqual(L"H", stringSettings.c_str());
            Assert::IsTrue(rereadSettings.Setting(KRemoveBellNumberFromRingersFieldNo, boolSetting));
            Assert::IsTrue(boolSetting);

        }

        TEST_METHOD(CsvImporter_Import_MikeChesterCSV)
        {
            Duco::StatisticFilters filters(*database);
            Duco::CsvImporter importer (*this, *database, ".", KDefaultCsvImportSettings);

            Assert::IsTrue(importer.Import("Mike chester sample with footnotes.csv"));
            Assert::AreEqual((size_t)5, database->PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)3, database->NumberOfTowers()); // 3 counted in the file.
            Assert::AreEqual((size_t)3, database->NumberOfMethods());
            Assert::AreEqual((size_t)1, database->NumberOfAssociations());

            std::set<Duco::ObjectId> nearMatches;
            Assert::IsTrue(database->RingersDatabase().SuggestRinger(L"Ringer 1111", false, nearMatches).ValidId());
            Assert::IsTrue(database->RingersDatabase().SuggestRinger(L"Ringer 2222", false, nearMatches).ValidId());
            Assert::IsTrue(database->RingersDatabase().SuggestRinger(L"Ringer 3333", false, nearMatches).ValidId());
            Assert::IsTrue(database->RingersDatabase().SuggestRinger(L"Ringer 4444", false, nearMatches).ValidId());
            Assert::IsTrue(database->RingersDatabase().SuggestRinger(L"Ringer 5555", false, nearMatches).ValidId());
            Assert::IsTrue(database->RingersDatabase().SuggestRinger(L"Ringer 6666", false, nearMatches).ValidId());
            Assert::IsTrue(database->RingersDatabase().SuggestRinger(L"Ringer 7777", false, nearMatches).ValidId());
            Assert::IsTrue(database->RingersDatabase().SuggestRinger(L"Ringer 8888", false, nearMatches).ValidId());
            Assert::IsTrue(database->RingersDatabase().SuggestRinger(L"Ringer 9999", false, nearMatches).ValidId());
            Assert::IsTrue(database->RingersDatabase().SuggestRinger(L"Ringer 1000", false, nearMatches).ValidId());
            Assert::IsTrue(database->RingersDatabase().SuggestRinger(L"Ringer 11", false, nearMatches).ValidId());
            Assert::IsTrue(database->RingersDatabase().SuggestRinger(L"Ringer 12", false, nearMatches).ValidId());
            Assert::IsTrue(database->RingersDatabase().SuggestRinger(L"321 Ringer", false, nearMatches).ValidId());

            std::map<Duco::ObjectId, Duco::PealLengthInfo> pealCounts;
            database->PealsDatabase().GetAssociationsPealCount(filters, pealCounts, false);

            Assert::AreEqual((size_t)1, pealCounts.size());
            Assert::AreEqual((size_t)5, pealCounts.begin()->second.TotalPeals());

            database->PealsDatabase().GetTowersPealCount(filters, pealCounts);
            Assert::AreEqual((size_t)3, pealCounts.size());
            Assert::AreEqual((size_t)1, pealCounts.begin()->second.TotalPeals());
            Assert::AreEqual((size_t)3, pealCounts.begin().operator++()->second.TotalPeals());

            Peal firstPeal = *database->PealsDatabase().FindPeal(1);
            unsigned int bellNo = 0;
            Assert::IsTrue(firstPeal.FindRingerBellByName(*database, L"1111, Ringer", true, bellNo));
            Assert::AreEqual((unsigned int)1, bellNo);

            Assert::AreEqual(L"9999, Ringer", firstPeal.RingerName(3, false, *database).c_str());

            Duco::ObjectId ringerId;
            Assert::IsTrue(firstPeal.RingerId(2, false, ringerId));
            Assert::AreEqual(L"2222, Ringer", database->RingersDatabase().RingerFullName(ringerId, database->Settings()).c_str());
            Assert::IsTrue(firstPeal.RingerId(11, false, ringerId));
            Assert::AreEqual(L"Ringer, 11", database->RingersDatabase().RingerFullName(ringerId, database->Settings()).c_str());

            Peal thirdPeal = *database->PealsDatabase().FindPeal(3);
            Assert::IsTrue(thirdPeal.Handbell());
            Assert::AreEqual(L"22/12/1978", thirdPeal.Date().Str().c_str());

            database->PealsDatabase().GetRingersPealCount(filters, false, pealCounts);
            Assert::AreEqual((size_t)5, pealCounts.find(1)->second.TotalPeals());

            Assert::AreEqual(L"", database->PealsDatabase().FindPeal(3)->Footnotes().c_str());
            Assert::AreEqual(L"this is a foot note", database->PealsDatabase().FindPeal(4)->Footnotes().c_str());
            Assert::AreEqual(L"This is also a foot note", database->PealsDatabase().FindPeal(5)->Footnotes().c_str());

            Assert::AreEqual(L"501 methods", database->PealsDatabase().FindPeal(1)->MultiMethods().c_str());
            Assert::AreEqual(L"Owen Battye (No1)", database->PealsDatabase().FindPeal(1)->Composer().c_str());

            Assert::AreEqual((unsigned int)5010, database->PealsDatabase().FindPeal(1)->NoOfChanges());
            Assert::AreEqual((unsigned int)5011, database->PealsDatabase().FindPeal(2)->NoOfChanges());
            Assert::AreEqual((unsigned int)5011, database->PealsDatabase().FindPeal(3)->NoOfChanges());
            Assert::AreEqual((unsigned int)5012, database->PealsDatabase().FindPeal(4)->NoOfChanges());
            Assert::AreEqual((unsigned int)5011, database->PealsDatabase().FindPeal(5)->NoOfChanges());

            PerformanceDate pealDate(1975, 10, 20);
            const Duco::Peal* const foundPeal = database->PealsDatabase().FindPeal(database->PealsDatabase().FindPeal(pealDate), false);
            Assert::AreEqual(L"1111, Ringer", foundPeal->RingerName(1, false, *database).c_str());
            Assert::AreEqual(L"2222, Ringer", foundPeal->RingerName(2, false, *database).c_str());
            Assert::AreEqual(L"9999, Ringer", foundPeal->RingerName(3, false, *database).c_str());
            Assert::AreEqual(L"4444, Ringer", foundPeal->RingerName(4, false, *database).c_str());
            Assert::AreEqual(L"5555, Ringer", foundPeal->RingerName(5, false, *database).c_str());
            Assert::AreEqual(L"6666, Ringer", foundPeal->RingerName(6, false, *database).c_str());
            Assert::AreEqual(L"7777, Ringer", foundPeal->RingerName(7, false, *database).c_str());
            Assert::AreEqual(L"8888, Ringer", foundPeal->RingerName(8, false, *database).c_str());
            Assert::AreEqual(L"3333, Ringer", foundPeal->RingerName(9, false, *database).c_str());
            Assert::AreEqual(L"1000, Ringer", foundPeal->RingerName(10, false, *database).c_str());
            Assert::AreEqual(L"Ringer, 11", foundPeal->RingerName(11, false, *database).c_str());
            Assert::AreEqual(L"12, Ringer", foundPeal->RingerName(12, false, *database).c_str());

            Assert::AreEqual(L"5867-0324", database->PealsDatabase().FindPeal(1)->RingingWorldReference().c_str());
            Assert::AreEqual(L"4321-1234", database->PealsDatabase().FindPeal(2)->RingingWorldReference().c_str());
            Assert::AreEqual(L"", database->PealsDatabase().FindPeal(3)->RingingWorldReference().c_str());
            Assert::AreEqual(L"5867-3478", database->PealsDatabase().FindPeal(4)->RingingWorldReference().c_str());
            Assert::AreEqual(L"", database->PealsDatabase().FindPeal(5)->RingingWorldReference().c_str());

            Assert::AreEqual(L"1706420", database->PealsDatabase().FindPeal(1)->BellBoardId().c_str());
            Assert::AreEqual(L"738523", database->PealsDatabase().FindPeal(2)->BellBoardId().c_str());
            Assert::AreEqual(L"", database->PealsDatabase().FindPeal(3)->BellBoardId().c_str());
            Assert::AreEqual(L"1706420", database->PealsDatabase().FindPeal(4)->BellBoardId().c_str());
            Assert::AreEqual(L"", database->PealsDatabase().FindPeal(5)->BellBoardId().c_str());
        }

        TEST_METHOD(CsvImporter_Import_MikeChesterCSV_with_symbols)
        {
            //Duco::StatisticFilters filters(*database);
            Duco::CsvImporter importer(*this, *database, ".", KDefaultCsvImportSettings);
            Duco::StatisticFilters filters(*database);

            Assert::IsTrue(importer.Import("Mike chester sample with footnotes and symbols.csv"));
            Assert::AreEqual((size_t)5, database->PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)3, database->NumberOfTowers()); // 3 counted in the file.
            Assert::AreEqual((size_t)3, database->NumberOfMethods());
            Assert::AreEqual((size_t)1, database->NumberOfAssociations());

            const Duco::Tower* const firstTower = database->TowersDatabase().FindTower(1);
            Assert::AreEqual(L"A♭", firstTower->FindRing(0)->TenorKey().c_str());
            const Duco::Tower* const secondTower = database->TowersDatabase().FindTower(2);
            Assert::AreEqual(L"B♯", secondTower->FindRing(0)->TenorKey().c_str());
            const Duco::Tower* const thirdTower = database->TowersDatabase().FindTower(3);
            Assert::AreEqual(L"HB", thirdTower->FindRing(0)->TenorKey().c_str());
        }

        TEST_METHOD(CsvImporter_Import_MikeChesterCSV_WithQuotes)
        {
            Duco::StatisticFilters filters(*database);

            Duco::CsvImporter importer(*this, *database, ".", KCsvWithQuotesImportSettings);

            Assert::IsTrue(importer.Import("Mike chester sample with footnotes and quotes.csv"));
            Assert::AreEqual((size_t)5, database->PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)14, database->NumberOfRingers());
            Assert::AreEqual((size_t)3, database->NumberOfTowers());
            Assert::AreEqual((size_t)3, database->NumberOfMethods());
            Assert::AreEqual((size_t)1, database->NumberOfAssociations());

            std::set<Duco::ObjectId> nearMatches;
            Assert::AreNotEqual(Duco::ObjectId(), database->RingersDatabase().SuggestRinger(L"Ringer 1111", false, nearMatches));
            Assert::AreNotEqual(Duco::ObjectId(), database->RingersDatabase().SuggestRinger(L"Ringer 2222", false, nearMatches));
            Assert::AreNotEqual(Duco::ObjectId(), database->RingersDatabase().SuggestRinger(L"3333, Ringer", false, nearMatches));
            Assert::AreNotEqual(Duco::ObjectId(), database->RingersDatabase().SuggestRinger(L"Ringer 4444", false, nearMatches));
            Assert::AreNotEqual(Duco::ObjectId(), database->RingersDatabase().SuggestRinger(L"Ringer 5555", false, nearMatches));
            Assert::AreNotEqual(Duco::ObjectId(), database->RingersDatabase().SuggestRinger(L"Ringer 6666", false, nearMatches));
            Assert::AreNotEqual(Duco::ObjectId(), database->RingersDatabase().SuggestRinger(L"Ringer 7777", false, nearMatches));
            Assert::AreNotEqual(Duco::ObjectId(), database->RingersDatabase().SuggestRinger(L"Ringer 8888", false, nearMatches));
            Assert::AreNotEqual(Duco::ObjectId(), database->RingersDatabase().SuggestRinger(L"Ringer 9999", false, nearMatches));
            Assert::AreNotEqual(Duco::ObjectId(), database->RingersDatabase().SuggestRinger(L"Ringer 1000", false, nearMatches));
            Assert::AreNotEqual(Duco::ObjectId(), database->RingersDatabase().SuggestRinger(L"11 Ringer", false, nearMatches));
            Assert::AreNotEqual(Duco::ObjectId(), database->RingersDatabase().SuggestRinger(L"Ringer 12", false, nearMatches));
            Assert::AreNotEqual(Duco::ObjectId(), database->RingersDatabase().SuggestRinger(L"321 Ringer", false, nearMatches));

            std::map<Duco::ObjectId, Duco::PealLengthInfo> pealCounts;
            database->PealsDatabase().GetAssociationsPealCount(filters, pealCounts, false);

            Assert::AreEqual((size_t)1, pealCounts.size());
            Assert::AreEqual((size_t)5, pealCounts.begin()->second.TotalPeals());

            database->PealsDatabase().GetTowersPealCount(filters, pealCounts);
            Assert::AreEqual((size_t)3, pealCounts.size());
            Assert::AreEqual((size_t)1, pealCounts.begin()->second.TotalPeals());
            Assert::AreEqual((size_t)3, pealCounts.begin().operator++()->second.TotalPeals());

            Peal firstPeal = *database->PealsDatabase().FindPeal(1);
            unsigned int bellNo = 0;
            Assert::IsTrue(firstPeal.FindRingerBellByName(*database, L"1, Ringer", true, bellNo));
            Assert::AreEqual((unsigned int)1, bellNo);

            Assert::AreEqual(L"Ringer, 3333", firstPeal.RingerName(3, false, *database).c_str());

            Duco::ObjectId ringerId;
            Assert::IsTrue(firstPeal.RingerId(2, false, ringerId));
            Assert::AreEqual(L"2222, Ringer", database->RingersDatabase().RingerFullName(ringerId, database->Settings()).c_str());
            Assert::IsTrue(firstPeal.RingerId(11, false, ringerId));
            Assert::AreEqual(L"Ringer, 11", database->RingersDatabase().RingerFullName(ringerId, database->Settings()).c_str());

            Peal thirdPeal = *database->PealsDatabase().FindPeal(3);
            Assert::IsTrue(thirdPeal.Handbell());
            Assert::AreEqual(L"22/12/1978", thirdPeal.Date().Str().c_str());

            database->PealsDatabase().GetRingersPealCount(filters, false, pealCounts);
            Assert::AreEqual((size_t)5, pealCounts.find(1)->second.TotalPeals());

            Assert::AreEqual(L"", database->PealsDatabase().FindPeal(3)->Footnotes().c_str());
            Assert::AreEqual(L"this is a foot note", database->PealsDatabase().FindPeal(4)->Footnotes().c_str());
            Assert::AreEqual(L"This is a foot note", database->PealsDatabase().FindPeal(5)->Footnotes().c_str());

            Assert::AreEqual((unsigned int)5010, database->PealsDatabase().FindPeal(1)->NoOfChanges());
            Assert::AreEqual((unsigned int)5011, database->PealsDatabase().FindPeal(2)->NoOfChanges());
            Assert::AreEqual((unsigned int)5011, database->PealsDatabase().FindPeal(3)->NoOfChanges());
            Assert::AreEqual((unsigned int)5012, database->PealsDatabase().FindPeal(4)->NoOfChanges());
            Assert::AreEqual((unsigned int)5011, database->PealsDatabase().FindPeal(5)->NoOfChanges());

            PerformanceDate pealDate(1975, 10, 20);
            const Duco::Peal* const foundPeal = database->PealsDatabase().FindPeal(database->PealsDatabase().FindPeal(pealDate), false);
            Assert::AreEqual(L"1111, Ringer", foundPeal->RingerName(1, false, *database).c_str());
            Assert::AreEqual(L"2222, Ringer", foundPeal->RingerName(2, false, *database).c_str());
            Assert::AreEqual(L"Ringer, 3333", foundPeal->RingerName(3, false, *database).c_str());
            Assert::AreEqual(L"4444, Ringer", foundPeal->RingerName(4, false, *database).c_str());
            Assert::AreEqual(L"5555, Ringer", foundPeal->RingerName(5, false, *database).c_str());
            Assert::AreEqual(L"6666, Ringer", foundPeal->RingerName(6, false, *database).c_str());
            Assert::AreEqual(L"7777, Ringer", foundPeal->RingerName(7, false, *database).c_str());
            Assert::AreEqual(L"8888, Ringer", foundPeal->RingerName(8, false, *database).c_str());
            Assert::AreEqual(L"9999, Ringer", foundPeal->RingerName(9, false, *database).c_str());
            Assert::AreEqual(L"1000, Ringer", foundPeal->RingerName(10, false, *database).c_str());
            Assert::AreEqual(L"Ringer, 11", foundPeal->RingerName(11, false, *database).c_str());
            Assert::AreEqual(L"12, Ringer", foundPeal->RingerName(12, false, *database).c_str());

        }

        TEST_METHOD(CsvImporter_Import_MikeChesterCSV_MissingFieldsFromDefault)
        {
            const std::string settingsFilename = "testsettings.config";
            Duco::SettingsFile defaultSettings("", settingsFilename);
            defaultSettings.ClearAllSettings();
            defaultSettings.Set(KFootnotesFieldNo, 32);
            defaultSettings.Set(KComposerFieldNo, 33);
            defaultSettings.Set(KSplicedMethodsFieldNo, 36);
            Assert::IsTrue(defaultSettings.Save());

            Duco::StatisticFilters filters(*database);
            Duco::CsvImporter importer(*this, *database, ".", settingsFilename);

            Assert::IsTrue(importer.Import("Mike chester sample with footnotes.csv"));
            Assert::AreEqual((size_t)5, database->PerformanceInfo(filters).TotalPeals());

            Assert::AreEqual(L"Owen Battye (No1)", database->PealsDatabase().FindPeal(1)->Composer().c_str());
            Assert::AreEqual(L"501 methods", database->PealsDatabase().FindPeal(1)->MultiMethods().c_str());
            Assert::AreEqual(L"this is a foot note", database->PealsDatabase().FindPeal(4)->Footnotes().c_str());
            Assert::AreEqual(L"This is also a foot note", database->PealsDatabase().FindPeal(5)->Footnotes().c_str());

            Duco::SettingsFile copySettings(defaultSettings);
            Assert::IsTrue(copySettings == defaultSettings);
            Assert::IsFalse(copySettings != defaultSettings);
        }

        TEST_METHOD(CsvImporter_Import_MultiLineFootNotesAndSymbols)
        {
            const std::string settingsFilename = "testsettings.config";
            Duco::SettingsFile defaultSettings("", settingsFilename);
            defaultSettings.ClearAllSettings();
            defaultSettings.Set(KFootnotesFieldNo, 32);
            defaultSettings.Set(KComposerFieldNo, 33);
            defaultSettings.Set(KSplicedMethodsFieldNo, 34);
            defaultSettings.Set(KEnforceQuotesFieldNo, true);
            Assert::IsTrue(defaultSettings.Save());

            Duco::StatisticFilters filters(*database);
            Duco::CsvImporter importer(*this, *database, ".", settingsFilename);

            Assert::IsTrue(importer.Import("Multiline footnotes and symbols.csv"));
            Assert::AreEqual((size_t)6, database->PerformanceInfo(filters).TotalPeals());

            Assert::AreEqual(L"Owen Battye (No1)", database->PealsDatabase().FindPeal(1)->Composer().c_str());
            Assert::AreEqual(L"501 methods", database->PealsDatabase().FindPeal(1)->MultiMethods().c_str());
            Assert::AreEqual(L"This is a footnote with a £ symbol", database->PealsDatabase().FindPeal(3)->Footnotes().c_str());
            Assert::AreEqual(L"This is a multi line foot note\r\nand this is line 2", database->PealsDatabase().FindPeal(4)->Footnotes().c_str());
            Assert::AreEqual(L"This is a foot note wiht a , and &", database->PealsDatabase().FindPeal(5)->Footnotes().c_str());

            Duco::SettingsFile copySettings(defaultSettings);
            Assert::IsTrue(copySettings == defaultSettings);
            Assert::IsFalse(copySettings != defaultSettings);
        }

        TEST_METHOD(CsvImporter_Import_Suffolk_Thornham)
        {
            const std::string settingsFilename = "suffolkcsvsettings.config";
            Duco::SettingsFile defaultSettings("", settingsFilename);

            Duco::StatisticFilters filters(*database);
            Duco::CsvImporter importer(*this, *database, ".", settingsFilename);

            Assert::IsTrue(importer.Import("suffolktest_thornham_only.csv"));
            size_t noOfLinkedTowers;
            Assert::AreEqual((size_t)1, database->NumberOfActiveTowers(noOfLinkedTowers));
            Assert::AreEqual((size_t)1, database->NumberOfAssociations());
            Assert::AreEqual((size_t)0, noOfLinkedTowers);
            Assert::AreEqual((size_t)51, database->PerformanceInfo(filters).TotalPeals());


            ObjectId perfIdDateOne = database->PealsDatabase().FindPeal(PerformanceDate(1984, 3, 31));
            Assert::AreEqual(L"Saturday 31 March 1984", database->PealsDatabase().FindPeal(perfIdDateOne)->Date().FullDate().c_str());

            ObjectId perfIdDateTwo = database->PealsDatabase().FindPeal(PerformanceDate(1982, 10, 8));
            Duco::Peal* perfTwo = database->PealsDatabase().FindPeal(perfIdDateTwo);
            Assert::AreEqual(L"1) Norwich Surprise. 2) Double Court Bob. 3) Cambridge Surprise.\r\n4) Double Oxford Bob, Thelwall Bob, Pinehurst Bob, St Clement's Bob, Childwall Bob, Buxton Bob. 5) Oxford Treble Bob. 6) Plain Bob.\r\n7) Kent Treble Bob.", perfTwo->MultiMethods().c_str());

            ObjectId perfIdDateThree = database->PealsDatabase().FindPeal(PerformanceDate(1989, 1, 7));
            Duco::Peal* perfThree = database->PealsDatabase().FindPeal(perfIdDateThree);
            Assert::AreEqual(L"First London Surprise Minor by all.\r\nFirst London : 2, 4. \r\nCompletes family : 5\r\nBirthday compliment to Helen T Waters of Cambridge granddaughter of the 3rd ringer.", perfThree->Footnotes().c_str());

            const Duco::Tower* theTower = database->TowersDatabase().FindTower(perfTwo->TowerId());
            Assert::AreEqual(L"St Mary", theTower->Name().c_str());
            Assert::AreEqual(L"Thornham Magna", theTower->City().c_str());
            Assert::AreEqual((size_t)2, theTower->NoOfRings());
        }

        TEST_METHOD(CsvImporter_Import_Suffolk)
        {
            const std::string settingsFilename = "suffolkcsvsettings.config";
            Duco::SettingsFile defaultSettings("", settingsFilename);

            Duco::StatisticFilters filters(*database);
            Duco::CsvImporter importer(*this, *database, ".", settingsFilename);

            Assert::IsTrue(importer.Import("suffolktest.csv"));
            Assert::AreEqual((size_t)9656, database->PerformanceInfo(filters).TotalPeals());

            Assert::AreEqual(L"Tuesday 3 April 1923", database->PealsDatabase().FindPeal(1)->Date().FullDate().c_str());
            Assert::AreEqual(L"2:36", database->PealsDatabase().FindPeal(1)->Time().PrintShortPealTime().c_str());
            Assert::AreEqual((unsigned int)5040, database->PealsDatabase().FindPeal(1)->NoOfChanges());
            Assert::AreEqual((unsigned int)5088, database->PealsDatabase().FindPeal(7)->NoOfChanges());
            Assert::AreEqual(L"", database->PealsDatabase().FindPeal(8)->Footnotes().c_str());
            Assert::AreEqual(L"Each called differently.", database->PealsDatabase().FindPeal(11)->MultiMethods().c_str());

            {
                PerformanceDate dateOfPeal(1973, 7, 28);
                Duco::ObjectId pealId = database->PealsDatabase().FindPeal(dateOfPeal);
                Peal* thePeal = database->PealsDatabase().FindPeal(pealId, true);
                Assert::AreEqual(L"Saturday 28 July 1973", thePeal->Date().FullDate().c_str());
                Assert::AreEqual(L"First of major - 2   \r\n25th for the Guild - 4\r\nSponsored peal for the Church Restoration Fund which raised £60", thePeal->Footnotes().c_str());
            }
            {
                PerformanceDate dateOfPeal(2018, 4, 16);
                Duco::ObjectId pealId = database->PealsDatabase().FindPeal(dateOfPeal);
                Peal* thePeal = database->PealsDatabase().FindPeal(pealId, true);
                Assert::AreEqual(L"Monday 16 April 2018", thePeal->Date().FullDate().c_str());
                Assert::AreEqual(DucoEngineUtils::Trim(L"Rung to celebrate the Diamond Wedding Anniversary of Dave & Pat Ward, ringers at this tower, married on this day at The Canongate Kirk on The Royal Mile in Edinburgh in 1958.\r\n"), DucoEngineUtils::Trim(thePeal->Footnotes()));
            }
            {
                PerformanceDate dateOfPeal(2004, 5, 2);
                Duco::ObjectId pealId = database->PealsDatabase().FindPeal(dateOfPeal);
                Peal* thePeal = database->PealsDatabase().FindPeal(pealId, true);
                Assert::AreEqual(L"Sunday 2 May 2004", thePeal->Date().FullDate().c_str());
                Assert::IsTrue(thePeal->Footnotes().starts_with(L"100th peal : 5\r\n70th birthday compliment to H JamesWightman, tower captain at this church.\r\nWhite"));
                Assert::IsTrue(thePeal->MultiMethods().starts_with(L"1) Beverley Surprise, IWD's Bus Pass Treble Place, Hunsbury Surprise,\r\nWhite Hart Treble Place, "));
                //Assert::AreEqual(L"1) Beverley Surprise, IWD's Bus Pass Treble Place, Hunsbury Surprise,\r\nWhite Hart Treble Place, ", thePeal->MultiMethods().c_str());
            }

            Assert::AreEqual(L"Suffolk Guild", database->AssociationsDatabase().FindAssociation(1)->Name().c_str());
            Assert::AreEqual((size_t)1, database->AssociationsDatabase().NumberOfObjects());
            Assert::AreEqual((size_t)9656, database->PealsDatabase().NumberOfObjects());
            Assert::AreEqual((size_t)459, database->TowersDatabase().NumberOfObjects());
            Assert::AreEqual((size_t)988, database->MethodsDatabase().NumberOfObjects());
        }
    };
}
