#include "CppUnitTest.h"
#include <Peal.h>
#include <PealDatabase.h>
#include <Ringer.h>
#include "ToString.h"
#include <ImportExportProgressCallback.h>
#include <RingingDatabase.h>
#include <StatisticFilters.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(PealFees_UnitTests), ImportExportProgressCallback
    {
    public:
        //From ImportExportProgressCallback
        void InitialisingImportExport(bool internalising, unsigned int versionNumber, size_t totalNumberOfObjects)
        {

        }
        void ObjectProcessed(bool internalising, Duco::TObjectType objectType, Duco::ObjectId objectId, int totalPercentage, size_t numberInThisSubDatabase)
        {

        }
        void ImportExportComplete(bool internalising)
        {

        }
        void ImportExportFailed(bool internalising, int errorCode)
        {
            Assert::IsFalse(true);
        }

        TEST_METHOD(PealFees_SetAndRead)
        {
            Peal thePeal;

            Assert::IsTrue(thePeal.SetPealFee(5, false, 150));
            Assert::AreEqual((unsigned int)150, thePeal.PealFee(5, false));
            Assert::IsTrue(thePeal.SetPealFee(5, false, 200));
            Assert::AreEqual((unsigned int)200, thePeal.PealFee(5, false));

            Assert::IsTrue(thePeal.SetPealFee(7, true, 151));
            Assert::AreEqual((unsigned int)151, thePeal.PealFee(7, true));
            Assert::IsTrue(thePeal.SetPealFee(7, true, 201));
            Assert::AreEqual((unsigned int)201, thePeal.PealFee(7, true));

            Assert::IsTrue(thePeal.SetPealFee(5, true, 152));
            Assert::AreEqual((unsigned int)152, thePeal.PealFee(5, true));
            Assert::IsTrue(thePeal.SetPealFee(5, true, 202));
            Assert::AreEqual((unsigned int)202, thePeal.PealFee(5, true));

            Assert::AreEqual((unsigned int)200, thePeal.PealFee(5, false));
        }
        TEST_METHOD(PealFees_AveragePealFee_NoRingers)
        {
            Peal thePeal;

            Assert::IsTrue(thePeal.SetPealFee(1, false, 150));
            Assert::IsTrue(thePeal.SetPealFee(2, false, 150));
            Assert::IsTrue(thePeal.SetPealFee(3, false, 150));
            Assert::IsTrue(thePeal.SetPealFee(4, false, 150));
            Assert::IsTrue(thePeal.SetPealFee(5, false, 150));
            Assert::IsTrue(thePeal.SetPealFee(5, false, 100));
            Assert::IsTrue(thePeal.SetPealFee(6, false, 150));
            Assert::AreEqual((unsigned int)850, thePeal.AvgPealFeePaidPerPerson());
        }

        TEST_METHOD(PealFees_AveragePealFee_WithoutStrappers)
        {
            Peal thePeal;

            thePeal.SetRingerId(1, 1, false);
            thePeal.SetRingerId(2, 2, false);
            thePeal.SetRingerId(3, 3, false);
            thePeal.SetRingerId(4, 4, false);
            Assert::IsTrue(thePeal.SetPealFee(1, false, 150));
            Assert::IsTrue(thePeal.SetPealFee(2, false, 150));
            Assert::IsTrue(thePeal.SetPealFee(3, false, 150));
            Assert::IsTrue(thePeal.SetPealFee(4, false, 150));
            Assert::IsTrue(thePeal.SetPealFee(5, false, 150));
            Assert::IsTrue(thePeal.SetPealFee(6, false, 150));
            thePeal.SetRingerId(5, 5, false);
            thePeal.SetRingerId(6, 6, false);

            Assert::AreEqual((unsigned int)150, thePeal.AvgPealFeePaidPerPerson());
        }

        TEST_METHOD(PealFees_AveragePealFee_WithoutStrappers_OneNotPaid)
        {

            Peal thePeal;

            thePeal.SetRingerId(1, 1, false);
            thePeal.SetRingerId(2, 2, false);
            thePeal.SetRingerId(3, 3, false);
            thePeal.SetRingerId(4, 4, false);
            thePeal.SetRingerId(5, 5, false);
            thePeal.SetRingerId(6, 6, false);
            Assert::IsTrue(thePeal.SetPealFee(1, false, 150));
            Assert::IsTrue(thePeal.SetPealFee(2, false, 150));
            Assert::IsTrue(thePeal.SetPealFee(3, false, 150));
            //Assert::IsTrue(thePeal.SetPealFee(4, false, 0));
            Assert::IsTrue(thePeal.SetPealFee(5, false, 150));
            Assert::IsTrue(thePeal.SetPealFee(6, false, 150));

            Assert::AreEqual((unsigned int)125, thePeal.AvgPealFeePaidPerPerson());
        }

        TEST_METHOD(PealFees_AveragePealFee_Strappers)
        {
            Peal thePeal;

            thePeal.SetRingerId(1, 1, false);
            thePeal.SetRingerId(2, 2, false);
            thePeal.SetRingerId(3, 3, false);
            thePeal.SetRingerId(4, 4, false);
            thePeal.SetRingerId(5, 5, false);
            thePeal.SetRingerId(6, 6, false);
            Assert::AreEqual((unsigned int)6, thePeal.NoOfRingers(true));
            thePeal.SetRingerId(7, 7, true);
            Assert::AreEqual((unsigned int)7, thePeal.NoOfRingers(true));
            Assert::IsTrue(thePeal.SetPealFee(1, false, 150));
            Assert::IsTrue(thePeal.SetPealFee(2, false, 150));
            Assert::IsTrue(thePeal.SetPealFee(3, false, 150));
            Assert::IsTrue(thePeal.SetPealFee(4, false, 150));
            Assert::IsTrue(thePeal.SetPealFee(5, false, 150));
            Assert::IsTrue(thePeal.SetPealFee(6, false, 150));
            Assert::AreEqual((unsigned int)7, thePeal.NoOfRingers(true));
            Assert::AreEqual((unsigned int)128, thePeal.AvgPealFeePaidPerPerson());

            Assert::IsTrue(thePeal.SetPealFee(7, true, 150));
            Assert::AreEqual((unsigned int)7, thePeal.NoOfRingers(true));
            Assert::AreEqual((unsigned int)150, thePeal.AvgPealFeePaidPerPerson());

            thePeal.RemoveRinger(7, true);
            Assert::AreEqual((unsigned int)6, thePeal.NoOfRingers(true));
            Assert::AreEqual((unsigned int)150, thePeal.AvgPealFeePaidPerPerson());
            Assert::IsFalse(thePeal.GetFeePayerId().ValidId());
        }

        TEST_METHOD(PealFees_FindFeePayer)
        {
            Duco::ObjectId KFeePayerId = 6;
            Peal thePeal;
            thePeal.SetRingerId(1, 1, false);
            thePeal.SetRingerId(2, 2, false);
            thePeal.SetRingerId(3, 3, false);
            thePeal.SetRingerId(4, 4, false);
            thePeal.SetRingerId(5, 5, false);
            thePeal.SetRingerId(KFeePayerId, 6, false);
            thePeal.SetRingerId(7, 6, true);
            Assert::IsFalse(thePeal.SetPealFee(1, false, 0));
            Assert::IsTrue(thePeal.SetPealFee(6, false, 600));

            Assert::AreEqual(KFeePayerId, thePeal.GetFeePayerId());
        }

        TEST_METHOD(PealFees_FindFeePayer_NoFees)
        {
            Duco::ObjectId KFeePayerId = 5;
            Peal thePeal;
            thePeal.SetRingerId(1, 1, false);
            thePeal.SetRingerId(2, 2, false);
            thePeal.SetRingerId(3, 3, false);
            thePeal.SetRingerId(4, 4, false);
            thePeal.SetRingerId(KFeePayerId, 5, false);
            thePeal.SetRingerId(6, 6, false);
            thePeal.SetConductorIdFromBell(5, true, false);

            Assert::AreEqual(KFeePayerId, thePeal.GetFeePayerId());
        }

        TEST_METHOD(PealFees_CheckMedianFees)
        {
            const std::string sampleDatabasename = "pealfees_test.duc";
            RingingDatabase database;

            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)50, databaseVersionNumber);

            Duco::StatisticFilters filters(database);
            Assert::AreEqual((size_t)4, database.PerformanceInfo(filters).TotalPeals());

            std::map<unsigned int, unsigned int> avgPealFeePerYear;
            database.PealsDatabase().MedianPealFeesPerPerson(avgPealFeePerYear, -1);

            Assert::AreEqual(100u, avgPealFeePerYear.find(2023)->second);
            Assert::AreEqual(100u, avgPealFeePerYear.find(2022)->second);
            Assert::IsTrue(avgPealFeePerYear.end() == avgPealFeePerYear.find(2000));
        }

        TEST_METHOD(PealFees_AllFeePayers)
        {
            const std::string sampleDatabasename = "pealfees_test.duc";
            RingingDatabase database;

            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)50, databaseVersionNumber);

            std::set<Duco::ObjectId> pealPayerIds;
            database.PealsDatabase().GetAllFeePayers(KNoId, pealPayerIds);

            Assert::IsTrue(pealPayerIds.end() != pealPayerIds.find(3));  // Oliver Austin
            Assert::IsTrue(pealPayerIds.end() != pealPayerIds.find(15)); // John (paid for 2), conducted 3

            Assert::IsTrue(pealPayerIds.end() == pealPayerIds.find(4));  // Owen - zero
        }

        TEST_METHOD(PealFees_CheckPealsWithUnPaidFees_ByJohnIn2022)
        {
            const std::string sampleDatabasename = "pealfees_test.duc";
            RingingDatabase database;

            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)50, databaseVersionNumber);

            Duco::StatisticFilters filters(database);
            Assert::AreEqual((size_t)4, database.PerformanceInfo(filters).TotalPeals());

            std::set<Duco::UnpaidPealFeeData> unpaidPeals;
            Assert::IsTrue(database.PealsDatabase().FindPealsWithUnpaidFees(15, KNoId, 2022, unpaidPeals));
            Assert::AreEqual((size_t)1, unpaidPeals.size());
            Assert::AreEqual(1000u, unpaidPeals.begin()->AmountUnPaidInPence());
        }

        TEST_METHOD(PealFees_CheckPealsWithUnPaidFees_ByJohnIn2023)
        {
            const std::string sampleDatabasename = "pealfees_test.duc";
            RingingDatabase database;

            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)50, databaseVersionNumber);

            Duco::StatisticFilters filters(database);
            Assert::AreEqual((size_t)4, database.PerformanceInfo(filters).TotalPeals());

            std::set<Duco::UnpaidPealFeeData> unpaidPeals;
            Assert::IsFalse(database.PealsDatabase().FindPealsWithUnpaidFees(15, KNoId, 2023, unpaidPeals));
            Assert::AreEqual((size_t)0, unpaidPeals.size());
        }

        TEST_METHOD(PealFees_CheckPealsWithUnPaidFees_ByAnyoneInAnyYear)
        {
            const std::string sampleDatabasename = "pealfees_test.duc";
            RingingDatabase database;

            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)50, databaseVersionNumber);

            Duco::StatisticFilters filters(database);
            Assert::AreEqual((size_t)4, database.PerformanceInfo(filters).TotalPeals());

            std::set<Duco::UnpaidPealFeeData> unpaidPeals;
            Assert::IsTrue(database.PealsDatabase().FindPealsWithUnpaidFees(-1, KNoId, -1, unpaidPeals));
            Assert::AreEqual((size_t)2, unpaidPeals.size());
        }
    };
}
