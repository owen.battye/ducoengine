#include "CppUnitTest.h"
#include <Change.h>
#include <Music.h>
#include <ProgressCallback.h>
#include "ToString.h"
#include <DucoEngineUtils.h>
#include <ChangeCollection.h>


using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(Music_UnitTests), ProgressCallback
    {
    public:
        // From ProgressCallback
        virtual void Initialised()
        {
        }
        void Step(int progressPercent)
        {
        }
        void Complete(bool error)
        {
            Assert::IsFalse(error);
        }
        TEST_METHOD(Music_ReadNonExistantFile)
        {
            Music music("", 8, "doesntexist.txt");
            Assert::AreEqual((size_t)0, music.NumberOfMusicGroups());
            Assert::AreEqual((size_t)0, music.NumberOfRollups());

            Change roundsOnEight(8);

            Assert::IsFalse(music.CheckChange(roundsOnEight));
            Assert::IsFalse(music.CheckChangeForRollup(roundsOnEight));
            Assert::AreEqual(L"", music.Detail().c_str());
        }

        TEST_METHOD(Music_SimpleFile)
        {
            Music music("", 8, "SimpleMusic.txt");
            Assert::AreEqual((size_t)9, music.NumberOfMusicGroups());
            Assert::AreEqual((size_t)3, music.NumberOfRollups());

            Change roundsOnEight(8);

            Assert::IsTrue(music.CheckChange(roundsOnEight));
            Assert::IsTrue(music.CheckChangeForRollup(roundsOnEight));

            std::wstring expected = L"1: xxxxx678\r\n1: 123xxxxx\r\n\r\nRollups:\r\n1: Four bell little bell rollups (1234)\r\n1: Three bell big bell rollups (678)";
            Assert::AreEqual(expected.c_str(), music.Detail().c_str());
        }

        TEST_METHOD(Music_DefaultFile_8BellMusic)
        {
            Music music("", 8, "Music.txt");
            Assert::AreEqual((size_t)15, music.NumberOfMusicGroups());
            Assert::AreEqual((size_t)8, music.NumberOfRollups());

            ChangeCollection changes (Change(8));
            changes.Add(Change(L"13572468"));
            changes.Add(Change(L"15263748"));
            changes.Add(Change(L"87654321"));
            changes.Add(Change(L"15263748")); // Duplicate
            changes.Add(Change(L"43215678"));
            changes.Add(Change(L"42315678"));
            changes.Add(Change(L"54321867"));
            changes.Add(Change(L"54321678"));
            changes.Add(Change(L"12348675"));
            changes.Add(Change(L"12345876"));
            Assert::IsFalse(changes.True());
            size_t falseChangeNo = 0;
            Assert::IsTrue(changes.FalseChangeNumber(falseChangeNo));
            Assert::AreEqual((size_t)4,falseChangeNo);
            music.GenerateMusic(changes, *this);

            std::wstring result = music.Detail();
            Assert::IsTrue(result.find(L"Tittums") != std::wstring::npos);
            Assert::IsTrue(result.find(L"Queens") != std::wstring::npos);
            Assert::IsTrue(result.find(L"Back-rounds") != std::wstring::npos);
            Assert::IsTrue(result.find(L"3: xxxxx678s") != std::wstring::npos);
            Assert::IsTrue(result.find(L"2: 123xxxxxs") != std::wstring::npos);

            Assert::IsTrue(result.find(L"2: Four bell little bell rollups") != std::wstring::npos);
            Assert::IsTrue(result.find(L"2: Three bell little bell rollups ") != std::wstring::npos);
            Assert::IsTrue(result.find(L"4: Four bell little bell rolldowns") != std::wstring::npos);
            Assert::IsTrue(result.find(L"4: Three bell little bell rolldowns ") != std::wstring::npos);
            Assert::IsTrue(result.find(L"2: Four bell big bell rollups ") != std::wstring::npos);
            Assert::IsTrue(result.find(L"3: Three bell big bell rollups ") != std::wstring::npos);
            Assert::IsTrue(result.find(L"1: Four bell big bell rolldowns") != std::wstring::npos);
            Assert::IsTrue(result.find(L"2: Three bell big bell rolldowns ") != std::wstring::npos);
        }
    };
}
