#pragma once
#include "CppUnitTest.h"

#include <Change.h>
#include <Composition.h>
#include <PerformanceDate.h>
#include <PerformanceTime.h>
#include <Gender.h>
#include <Picture.h>
#include <Ringer.h>
#include <ObjectId.h>
#include <ObjectType.h>

#include <list>

namespace Microsoft
{
    namespace VisualStudio
    {
        namespace CppUnitTestFramework
        {
            template<> inline std::wstring ToString<Duco::ObjectId>(const Duco::ObjectId& t) { return ToString(t.Id()); }
            template<> inline std::wstring ToString<Duco::TObjectType>(const Duco::TObjectType& t) { return ToString((int)t); }
            template<> inline std::wstring ToString<Duco::PerformanceDate>(const Duco::PerformanceDate& t) { return t.Str().c_str(); }
            template<> inline std::wstring ToString<Duco::PerformanceTime>(const Duco::PerformanceTime& t) { return t.PrintShortPealTime().c_str(); }
            template<> inline std::wstring ToString<Duco::Change>(const Duco::Change& t) { return t.Str(true,true).c_str(); }
            template<> inline std::wstring ToString<Duco::Composition>(const Duco::Composition& t) { return t.Name().c_str(); }
            template<> inline std::wstring ToString<Duco::Picture>(const Duco::Picture& t) { return t.Id().Str(); }
            template<> inline std::wstring ToString<Duco::Ringer>(const Duco::Ringer& t) { return t.FullName(false); }
            template<> inline std::wstring ToString<Duco::Gender>(const Duco::Gender& t) { return t.Str(); }
            template<> inline std::wstring ToString<std::list<Duco::ObjectId> >(const std::list<Duco::ObjectId>& t)
            { 
                std::wostringstream stream;
                std::list<Duco::ObjectId>::const_iterator it = t.begin();
                while (it != t.end())
                {
                    stream << *it;
                    stream << L", ";
                    ++it;
                }
                const std::wstring s = stream.str();
                const std::wstring ws(s.begin(), s.end());
                return ws;
            }

#ifndef WIN32
            template<> inline std::wstring ToString<std::list<size_t> >(const std::list<size_t>& t)
            {
                std::wstring retVal;
                std::list<size_t>::const_iterator it = t.begin();
                while (it != t.end())
                {
                    retVal.append(std::to_wstring(*it));
                    retVal.push_back(',');
                    ++it;
                }
                return retVal;
            }
#endif
            template<> inline std::wstring ToString<std::vector<unsigned int> >(const std::vector<unsigned int>& t)
            {
                std::wstring retVal;
                std::vector<unsigned int>::const_iterator it = t.begin();
                while (it != t.end())
                {
                    retVal.append(std::to_wstring(*it));
                    retVal.push_back(',');
                    ++it;
                }
                return retVal;
            }

            template<> inline std::wstring ToString<std::vector<std::wstring> >(const std::vector<std::wstring>& t)
            {
                std::wstring retVal;
                std::vector<std::wstring>::const_iterator it = t.begin();
                while (it != t.end())
                {
                    retVal.append(*it);
                    retVal.push_back(',');
                    ++it;
                }
                return retVal;
            }

            template<> inline std::wstring ToString<std::list<unsigned int> >(const std::list<unsigned int>& t)
            {
                std::wstring retVal;
                std::list<unsigned int>::const_iterator it = t.begin();
                while (it != t.end())
                {
                    retVal.append(std::to_wstring(*it));
                    retVal.push_back(',');
                    ++it;
                }
                return retVal;
            }
            template<> inline std::wstring ToString<std::set<std::wstring> >(const std::set<std::wstring>& t)
            {
                std::wstring retVal;
                std::set<std::wstring>::const_iterator it = t.begin();
                while (it != t.end())
                {
                    retVal.append(*it);
                    retVal.push_back(',');
                    ++it;
                }
                return retVal;
            }
            template<> inline std::wstring ToString<std::list<std::wstring> >(const std::list<std::wstring>& t)
            {
                std::wstring retVal;
                std::list<std::wstring>::const_iterator it = t.begin();
                while (it != t.end())
                {
                    retVal.append(*it);
                    retVal.push_back(',');
                    ++it;
                }
                return retVal;
            }
            template<> inline std::wstring ToString<std::array<unsigned int, 12> >(const std::array<unsigned int, 12>& t)
            {
                std::wstring retVal;
                std::array<unsigned int, 12>::const_iterator it = t.begin();
                while (it != t.end())
                {
                    retVal.append(std::to_wstring(*it));
                    retVal.push_back(',');
                    ++it;
                };
                return retVal;
            }
        }
    }
}
