#include "CppUnitTest.h"
#include <Course.h>
#include <Method.h>
#include "ToString.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(CompositionTable_UnitTests)
    {
    public:
        TEST_METHOD(CompositionTable_PlainBobMinor_TwoCourses)
        {
            Method plainBobMinor(1, 6, L"Plain", L"Bob", L"&x16x16x16-12", false);
            plainBobMinor.SetBobPlaceNotation(L"14");
            plainBobMinor.SetSinglePlaceNotation(L"1234");

            Composition compositionOne(plainBobMinor);
            Assert::IsTrue(compositionOne.AddCall('W', Duco::TLeadType::EBobLead));
            Assert::IsTrue(compositionOne.AddCall('H', Duco::TLeadType::EBobLead));
            Assert::IsTrue(compositionOne.AddCall('W', Duco::TLeadType::EBobLead));
            Assert::IsTrue(compositionOne.AddCall('H', Duco::TLeadType::EBobLead));

            CompositionTable table = compositionOne.CreateCompositionTable();
            Assert::AreEqual((size_t)2, table.NoOfCourses());

            Assert::AreEqual(L"45236", table.Course(0).CourseEnd().Str().c_str());
            Assert::AreEqual(L"-", table.Course(0).CallingStr(L'5').c_str());
            Assert::AreEqual(L"",  table.Course(0).CallingStr(L'3').c_str());
            Assert::AreEqual(L"",  table.Course(0).CallingStr(L'2').c_str());
            Assert::AreEqual(L"",  table.Course(0).CallingStr(L'4').c_str());
            Assert::AreEqual(L"-", table.Course(0).CallingStr(L'6').c_str());

            Assert::AreEqual(L"23456", table.Course(1).CourseEnd().Str().c_str());
            Assert::AreEqual(L"-", table.Course(1).CallingStr(L'5').c_str());
            Assert::AreEqual(L"", table.Course(1).CallingStr(L'3').c_str());
            Assert::AreEqual(L"", table.Course(1).CallingStr(L'2').c_str());
            Assert::AreEqual(L"", table.Course(1).CallingStr(L'4').c_str());
            Assert::AreEqual(L"-", table.Course(1).CallingStr(L'6').c_str());
        }

        TEST_METHOD(CompositionTable_PlainBobMajor_TwoCourses)
        {
            Method plainBobMajor(1, 8, L"Plain", L"Bob", L"&X18X18X18X18-12", false);
            plainBobMajor.SetBobPlaceNotation(L"14");
            plainBobMajor.SetSinglePlaceNotation(L"1234");

            Composition compositionOne(plainBobMajor);
            Assert::IsTrue(compositionOne.AddCall('W', Duco::TLeadType::EBobLead));
            Assert::IsTrue(compositionOne.AddCall('H', Duco::TLeadType::EBobLead));
            Assert::IsTrue(compositionOne.AddCall('W', Duco::TLeadType::EBobLead));
            Assert::IsTrue(compositionOne.AddCall('H', Duco::TLeadType::EBobLead));

            CompositionTable table = compositionOne.CreateCompositionTable();
            Assert::AreEqual((size_t)2, table.NoOfCourses());

            Assert::AreEqual(L"45236", table.Course(0).CourseEnd().Str().c_str());
            Assert::AreEqual(L"-", table.Course(0).CallingStr(L'7').c_str());
            Assert::AreEqual(L"",  table.Course(0).CallingStr(L'5').c_str());
            Assert::AreEqual(L"",  table.Course(0).CallingStr(L'3').c_str());
            Assert::AreEqual(L"",  table.Course(0).CallingStr(L'2').c_str());
            Assert::AreEqual(L"",  table.Course(0).CallingStr(L'4').c_str());
            Assert::AreEqual(L"",  table.Course(0).CallingStr(L'6').c_str());
            Assert::AreEqual(L"-", table.Course(0).CallingStr(L'8').c_str());

            Assert::AreEqual(L"23456", table.Course(1).CourseEnd().Str().c_str());
            Assert::AreEqual(L"-", table.Course(1).CallingStr(L'W').c_str());
            Assert::AreEqual(L"",  table.Course(1).CallingStr(L'V').c_str());
            Assert::AreEqual(L"",  table.Course(1).CallingStr(L'O').c_str());
            Assert::AreEqual(L"",  table.Course(1).CallingStr(L'I').c_str());
            Assert::AreEqual(L"",  table.Course(1).CallingStr(L'4').c_str());
            Assert::AreEqual(L"",  table.Course(1).CallingStr(L'M').c_str());
            Assert::AreEqual(L"-", table.Course(1).CallingStr(L'h').c_str());
        }

        TEST_METHOD(CompositionTable_PlainBobMajor_QuarterPeal)
        {
            Method plainBobMajor(1, 8, L"Plain", L"Bob", L"&X18X18X18X18-12", false);
            plainBobMajor.SetBobPlaceNotation(L"14");
            plainBobMajor.SetSinglePlaceNotation(L"1234");

            Composition compositionOne(plainBobMajor);
            Assert::IsTrue(compositionOne.AddCalls('W', L"2"));
            Assert::IsTrue(compositionOne.AddCalls('3', L"2"));
            Assert::IsTrue(compositionOne.AddCall ('4', Duco::TLeadType::EBobLead));
            Assert::IsTrue(compositionOne.AddCalls('H', L"3"));
            Assert::IsTrue(compositionOne.AddCall ('I', Duco::TLeadType::EBobLead));
            Assert::IsTrue(compositionOne.AddCall ('M', Duco::TLeadType::EBobLead));
            Assert::IsTrue(compositionOne.AddCalls('H', L"3"));
            Assert::IsTrue(compositionOne.AddCalls('B', L"2"));
            Assert::IsTrue(compositionOne.AddCall ('M', Duco::TLeadType::EBobLead));
            Assert::IsTrue(compositionOne.AddCall ('W', Duco::TLeadType::EBobLead));
            Assert::IsTrue(compositionOne.AddCalls('H', L"2"));

            CompositionTable table = compositionOne.CreateCompositionTable();
            Assert::AreEqual((size_t)4, table.NoOfCourses());

            Assert::AreEqual(L"357246", table.Course(0).CourseEnd().Str().c_str());
            Assert::AreEqual(L"2",  table.Course(0).CallingStr(L'7').c_str());
            Assert::AreEqual(L"",   table.Course(0).CallingStr(L'5').c_str());
            Assert::AreEqual(L"2",  table.Course(0).CallingStr(L'3').c_str());
            Assert::AreEqual(L"",   table.Course(0).CallingStr(L'2').c_str());
            Assert::AreEqual(L"-",  table.Course(0).CallingStr(L'4').c_str());
            Assert::AreEqual(L"",   table.Course(0).CallingStr(L'6').c_str());
            Assert::AreEqual(L"3",  table.Course(0).CallingStr(L'8').c_str());

            Assert::AreEqual(L"2",  table.Course(0).CallingStr(L'W').c_str());
            Assert::AreEqual(L"",   table.Course(0).CallingStr(L'V').c_str());
            Assert::AreEqual(L"2",  table.Course(0).CallingStr(L'O').c_str());
            Assert::AreEqual(L"",   table.Course(0).CallingStr(L'I').c_str());
            Assert::AreEqual(L"-",  table.Course(0).CallingStr(L'4').c_str());
            Assert::AreEqual(L"",   table.Course(0).CallingStr(L'M').c_str());
            Assert::AreEqual(L"3",  table.Course(0).CallingStr(L'H').c_str());

            Assert::AreEqual(L"3246578", table.Course(1).CourseEnd().Str(true).c_str());
            Assert::AreEqual(L"",   table.Course(1).CallingStr(L'W').c_str());
            Assert::AreEqual(L"",   table.Course(1).CallingStr(L'V').c_str());
            Assert::AreEqual(L"",   table.Course(1).CallingStr(L'O').c_str());
            Assert::AreEqual(L"-",  table.Course(1).CallingStr(L'I').c_str());
            Assert::AreEqual(L"",   table.Course(1).CallingStr(L'4').c_str());
            Assert::AreEqual(L"-",  table.Course(1).CallingStr(L'M').c_str());
            Assert::AreEqual(L"3",  table.Course(1).CallingStr(L'H').c_str());

            Course thirdCourse = table.Course(2);
            Assert::AreEqual(L"12534678", thirdCourse.CourseEnd().Str(true,true).c_str());
            Assert::AreEqual(L"",   table.Course(2).CallingStr(L'W').c_str());
            Assert::AreEqual(L"",   table.Course(2).CallingStr(L'V').c_str());
            Assert::AreEqual(L"2",  table.Course(2).CallingStr(L'O').c_str());
            Assert::AreEqual(L"",   table.Course(2).CallingStr(L'I').c_str());
            Assert::AreEqual(L"",   table.Course(2).CallingStr(L'4').c_str());
            Assert::AreEqual(L"-",  table.Course(2).CallingStr(L'M').c_str());
            Assert::AreEqual(L"",   table.Course(2).CallingStr(L'H').c_str());

            Assert::AreEqual(L"23456", table.Course(3).CourseEnd().Str().c_str());
            Assert::IsTrue(table.Course(3).CourseEnd().IsRounds());
            Assert::AreEqual(L"-",  table.Course(3).CallingStr(L'W').c_str());
            Assert::AreEqual(L"",   table.Course(3).CallingStr(L'V').c_str());
            Assert::AreEqual(L"",   table.Course(3).CallingStr(L'O').c_str());
            Assert::AreEqual(L"",   table.Course(3).CallingStr(L'I').c_str());
            Assert::AreEqual(L"",   table.Course(3).CallingStr(L'4').c_str());
            Assert::AreEqual(L"",   table.Course(3).CallingStr(L'M').c_str());
            Assert::AreEqual(L"2",  table.Course(3).CallingStr(L'H').c_str());

            Assert::IsTrue(compositionOne.CurrentEndChange().IsRounds());
            Assert::AreEqual((size_t)1280, compositionOne.NoOfChanges());
            Assert::AreEqual(L"12345678", compositionOne.EndChange(true, true).c_str());
            Assert::IsFalse(compositionOne.Proven());

            // This test has been seen to fail and generate about 4480 changes!
            // This test has been seen to fail at 357246
        }

        TEST_METHOD(CompositionTable_CambridgeMajor_NeverComesRound)
        {
            Method cambridgeMajor(1, 8, L"Cambridge", L"Surprise", L"&X38X14X1258X36X14X58X16X78-12", false);
            cambridgeMajor.SetBobPlaceNotation(L"14");

            Composition compositionOne(cambridgeMajor);
            Assert::IsTrue(compositionOne.AddCalls('M', L"1"));

            CompositionTable table = compositionOne.CreateCompositionTable();
            Assert::AreEqual((size_t)1, table.NoOfCourses());

            Assert::IsFalse(table.Course(0).CourseEnd().IsRounds());
        }
    };
}
