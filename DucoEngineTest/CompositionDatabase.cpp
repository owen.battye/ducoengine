#include "CppUnitTest.h"
#include <Composition.h>
#include <CompositionDatabase.h>
#include <DucoConfiguration.h>
#include <Method.h>
#include <MethodDatabase.h>
#include <Peal.h>
#include <PealDatabase.h>
#include <Ringer.h>
#include <RingerDatabase.h>
#include <RingingDatabase.h>
#include "ToString.h"
#include <RenumberProgressCallback.h>
#include <ProgressCallback.h>
#include <ImportExportProgressCallback.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(CompositionDatabase_UnitTests), Duco::ImportExportProgressCallback, Duco::RenumberProgressCallback, Duco::ProgressCallback
    {
    protected:
        RenumberProgressCallback::TRenumberStage lastStage;

    public:
        // RenumberProgressCallback
        void RenumberInitialised(RenumberProgressCallback::TRenumberStage newStage)
        {
            if (RenumberProgressCallback::TRenumberStage::ENotStarted != newStage)
            {
                Assert::IsTrue(newStage > lastStage);
            }
            lastStage = newStage;
        }

        void RenumberStep(size_t objectId, size_t total)
        {

        }
        void RenumberComplete()
        {
        }
        // From ImportExportProgressCallback
        void InitialisingImportExport(bool internalising, unsigned int versionNumber, size_t totalNumberOfObjects)
        {
        }
        void ObjectProcessed(bool internalising, Duco::TObjectType objectType, Duco::ObjectId objectId, int totalPercentage, size_t numberInThisSubDatabase)
        {
        }
        void ImportExportComplete(bool internalising)
        {
        }
        void ImportExportFailed(bool internalising, int errorCode)
        {
            Assert::AreEqual(0, errorCode);
        }
        // From ProgressCallback
        virtual void Initialised()
        {
        }
        void Step(int progressPercent)
        {
        }
        void Complete(bool error)
        {
            Assert::IsFalse(error);
        }
        TEST_METHOD(CompositionDatabase_InsertAndCheckCompositionsInserted)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Method plainBobMinor(KNoId, 6, L"Plain", L"Bob", L"&x16x16x16-12", false);
            plainBobMinor.SetBobPlaceNotation(L"14");
            Duco::ObjectId plainBobMinorId = database.MethodsDatabase().AddObject(plainBobMinor);
            Method plainBobMajor(KNoId, 8, L"Plain", L"Bob", L"&X18X18X18X18-12", false);
            Duco::ObjectId plainBobMajorId = database.MethodsDatabase().AddObject(plainBobMajor);
            Method cambridgeMajor(KNoId, 8, L"Cambridge", L"Surprise", L"&X38X14X1258X36X14X58X16X78-12", false);
            Duco::ObjectId cambridgeMajorId = database.MethodsDatabase().AddObject(cambridgeMajor);
            Assert::IsTrue(plainBobMinorId.ValidId());
            Assert::IsTrue(plainBobMajorId.ValidId());
            Assert::IsTrue(cambridgeMajorId.ValidId());

            Composition compOne(plainBobMinorId, database.MethodsDatabase());
            Composition compTwo(plainBobMajorId, database.MethodsDatabase());
            Composition compThree(cambridgeMajorId, database.MethodsDatabase());

            Duco::ObjectId compOneId = database.CompositionsDatabase().AddObject(compOne);
            Assert::IsTrue(compOneId.ValidId());
            Duco::ObjectId compTwoId = database.CompositionsDatabase().AddObject(compTwo);
            Assert::IsTrue(compTwoId.ValidId());
            Duco::ObjectId compThreeId = database.CompositionsDatabase().AddObject(compThree);
            Assert::IsTrue(compThreeId.ValidId());

            std::set<Duco::ObjectId> compositionIds;
            database.CompositionsDatabase().GetCompositionsByNoOfBells(compositionIds, database.MethodsDatabase(), 6);
            Assert::AreEqual((size_t)1, compositionIds.size());
            Assert::IsTrue(compositionIds.find(compOneId) != compositionIds.end());
            Assert::IsTrue(compositionIds.find(compTwoId) == compositionIds.end());
            Assert::IsTrue(compositionIds.find(compThreeId) == compositionIds.end());

            database.CompositionsDatabase().GetCompositionsByNoOfBells(compositionIds, database.MethodsDatabase(), 8);
            Assert::AreEqual((size_t)2, compositionIds.size());
            Assert::IsTrue(compositionIds.find(compOneId) == compositionIds.end());
            Assert::IsTrue(compositionIds.find(compTwoId) != compositionIds.end());
            Assert::IsTrue(compositionIds.find(compThreeId) != compositionIds.end());

            std::map<Duco::ObjectId, Duco::PealLengthInfo> compositionIdsAndCounts;
            Duco::PealLengthInfo newInfo;
            compositionIdsAndCounts[compOneId] = newInfo;
            database.CompositionsDatabase().AddMissingCompositions(compositionIdsAndCounts);
            Assert::AreEqual((size_t)3, compositionIdsAndCounts.size());
            Assert::AreEqual((size_t)0, compositionIdsAndCounts[compOneId].TotalPeals());
            Assert::AreEqual((size_t)0, compositionIdsAndCounts[compTwoId].TotalPeals());
            Assert::AreEqual((size_t)0, compositionIdsAndCounts[compThreeId].TotalPeals());
        }

        TEST_METHOD(CompositionDatabase_SortAndRenumber)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Method plainBobMinor(KNoId, 6, L"Plain", L"Bob", L"&x16x16x16-12", false);
            plainBobMinor.SetBobPlaceNotation(L"14");
            Duco::ObjectId plainBobMinorId = database.MethodsDatabase().AddObject(plainBobMinor);
            Assert::IsTrue(plainBobMinorId.ValidId());

            Composition compOne(plainBobMinorId, database.MethodsDatabase());
            compOne.SetName(L"BBBBBBB");
            Composition compTwo(plainBobMinorId, database.MethodsDatabase());
            compTwo.SetName(L"AAAAAAA");

            Duco::ObjectId compOneId = database.CompositionsDatabase().AddObject(compOne);
            Assert::IsTrue(compOneId.ValidId());
            Duco::ObjectId compTwoId = database.CompositionsDatabase().AddObject(compTwo);
            Assert::IsTrue(compTwoId.ValidId());

            Peal pealOne;
            pealOne.SetCompositionId(compTwoId);
            Duco::ObjectId pealOneId = database.PealsDatabase().AddObject(pealOne);

            database.RebuildDatabase(this);

            Assert::AreEqual(L"AAAAAAA", database.CompositionsDatabase().FindComposition(1)->Name().c_str());
            Assert::AreEqual(L"BBBBBBB", database.CompositionsDatabase().FindComposition(2)->Name().c_str());
            Assert::AreEqual(compOneId, database.PealsDatabase().FindPeal(pealOneId)->CompositionId());
        }

        TEST_METHOD(CompositionDatabase_DeleteUsedAndRenumber)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Method plainBobMinor(KNoId, 6, L"Plain", L"Bob", L"&x16x16x16-12", false);
            plainBobMinor.SetBobPlaceNotation(L"14");
            Duco::ObjectId plainBobMinorId = database.MethodsDatabase().AddObject(plainBobMinor);
            Assert::IsTrue(plainBobMinorId.ValidId());

            Composition compOne(plainBobMinorId, database.MethodsDatabase());
            compOne.SetName(L"BBBBBBB");
            std::wstring compTwoName = L"CCCCCC";
            Composition compTwo(plainBobMinorId, database.MethodsDatabase());
            compTwo.SetName(compTwoName);
            std::wstring compThreeName = L"AAAAAAA";
            Composition compThree(plainBobMinorId, database.MethodsDatabase());
            compThree.SetName(compThreeName);

            Duco::ObjectId compOneId = database.CompositionsDatabase().AddObject(compOne);
            Assert::IsTrue(compOneId.ValidId());
            Duco::ObjectId compTwoId = database.CompositionsDatabase().AddObject(compTwo);
            Assert::IsTrue(compTwoId.ValidId());
            Duco::ObjectId compThreeId = database.CompositionsDatabase().AddObject(compThree);
            Assert::IsTrue(compThreeId.ValidId());

            Peal pealOne;
            pealOne.SetCompositionId(compThreeId);
            Duco::ObjectId pealOneId = database.PealsDatabase().AddObject(pealOne);
            Peal pealTwo;
            pealTwo.SetCompositionId(compTwoId);
            Duco::ObjectId pealTwoId = database.PealsDatabase().AddObject(pealTwo);
            Assert::AreEqual((size_t)3, database.NumberOfCompositions());

            std::set<ObjectId> unusedMethods;
            std::set<ObjectId> unusedRingers;
            std::set<ObjectId> unusedTowers;
            std::multimap<ObjectId, ObjectId> unusedRings;
            std::set<ObjectId> unusedMethodSeries;
            std::set<ObjectId> unusedCompositions;
            std::set<ObjectId> unusedAssociations;
            database.FindUnused(unusedMethods, unusedRingers, unusedTowers, unusedRings, unusedMethodSeries, unusedCompositions, unusedAssociations);
            Assert::AreEqual((size_t)1, unusedCompositions.size());

            database.CompositionsDatabase().DeleteObjects(unusedCompositions);

            database.RebuildDatabase(this);

            Assert::AreEqual((size_t)2, database.NumberOfCompositions());
            Assert::AreEqual(compThreeName, database.CompositionsDatabase().FindComposition(1)->Name());
            Assert::AreEqual(compTwoName, database.CompositionsDatabase().FindComposition(2)->Name());

            Duco::ObjectId pealOneCompId = database.PealsDatabase().FindPeal(pealOneId)->CompositionId();
            Assert::AreEqual(Duco::ObjectId(1), pealOneCompId);
            Assert::AreEqual(compThreeName, database.CompositionsDatabase().FindComposition(pealOneCompId)->Name());

            Duco::ObjectId pealTwoCompId = database.PealsDatabase().FindPeal(pealTwoId)->CompositionId();
            Assert::AreEqual(Duco::ObjectId(2), pealTwoCompId);
            Assert::AreEqual(compTwoName, database.CompositionsDatabase().FindComposition(pealTwoCompId)->Name());

        }

        TEST_METHOD(CompositionDatabase_CheckDBVersion36)
        {
            //Before allowing ringiner id or name
            const std::string databaseName("composition_DBVer36.duc");
            const std::string databaseSaveName("composition_DBVer36_copy.duc");
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(databaseName.c_str(), this, databaseVersionNumber);

            Assert::AreEqual((unsigned int)36, database.DatabaseVersion());
            Assert::AreEqual((unsigned int)36, databaseVersionNumber);
            Assert::AreEqual((size_t)1, database.NumberOfRingers());
            Assert::AreEqual((size_t)1, database.NumberOfMethods());
            Assert::AreEqual((size_t)1, database.NumberOfCompositions());

            Assert::IsFalse(database.RebuildRecommended());
            Assert::IsFalse(database.RebuildDatabase(this));
            Assert::IsFalse(database.CompositionsDatabase().FindComposition(1) == NULL);
            Duco::ObjectId expectedRingerId(1);
            Assert::AreEqual(Duco::ObjectId(expectedRingerId), database.CompositionsDatabase().FindComposition(1)->ComposerId());
            Assert::AreEqual(L"Composed by Unknown Dunno (5040)", database.CompositionsDatabase().FindComposition(1)->Composer(database.RingersDatabase()).c_str());
            Assert::AreEqual(L"Unknown Dunno", database.RingersDatabase().FindRinger(expectedRingerId)->FullName(false).c_str());
            Assert::AreEqual((size_t)15120, database.CompositionsDatabase().FindComposition(1)->NoOfChanges());
            Assert::IsFalse(database.CompositionsDatabase().FindComposition(1)->Proven());
            Assert::IsFalse(database.CompositionsDatabase().FindComposition(1)->ProvenTrue());

            Duco::Composition compositionOne = *database.CompositionsDatabase().FindComposition(1);
            compositionOne.PrepareToProve(database.MethodsDatabase(), database.Settings());
            Assert::IsTrue(compositionOne.Prove(*this, ""));
            Assert::AreEqual((size_t)5040, compositionOne.NoOfChanges());
            Assert::IsTrue(compositionOne.Proven());
            Assert::IsTrue(compositionOne.ProvenTrue());
            // Still false in database
            Assert::IsFalse(database.CompositionsDatabase().FindComposition(1)->Proven());
            Assert::IsFalse(database.CompositionsDatabase().FindComposition(1)->ProvenTrue());
            Assert::AreEqual((size_t)15120, database.CompositionsDatabase().FindComposition(1)->NoOfChanges());

            database.CompositionsDatabase().UpdateObject(compositionOne);
            // Now true in database
            Assert::IsTrue(database.CompositionsDatabase().FindComposition(1)->Proven());
            Assert::AreEqual((size_t)5040, database.CompositionsDatabase().FindComposition(1)->NoOfChanges());
            Assert::IsTrue(database.Externalise(databaseSaveName.c_str(), this));

            RingingDatabase readDatabase;
            readDatabase.Internalise(databaseSaveName.c_str(), this, databaseVersionNumber);
            Duco::Composition readCompositionOne = *readDatabase.CompositionsDatabase().FindComposition(1);
            Assert::IsTrue(compositionOne == readCompositionOne);
            Assert::IsTrue(readCompositionOne.Proven());
            Assert::IsFalse(readCompositionOne.ProvenTrue()); // Doesnt produce changes until reproven.
            Assert::AreEqual((size_t)5040, readCompositionOne.NoOfChanges());
            Assert::AreEqual(Duco::ObjectId(expectedRingerId), readCompositionOne.ComposerId());
            Assert::AreEqual(L"Composed by Unknown Dunno (5040)", readCompositionOne.Composer(readDatabase.RingersDatabase()).c_str());
            Assert::AreEqual(L"Unknown Dunno", readDatabase.RingersDatabase().FindRinger(expectedRingerId)->FullName(false).c_str());
        }

        TEST_METHOD(CompositionDatabase_CheckDBVersion44)
        {
            //Before allowing ringiner id or name
            const std::string databaseName("two_compositions.duc");

            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(databaseName.c_str(), this, databaseVersionNumber);
            Assert::AreEqual(44u, databaseVersionNumber);
            Assert::AreEqual((size_t)1, database.NumberOfRingers());
            Assert::AreEqual((size_t)2, database.NumberOfCompositions());
            Assert::AreEqual((size_t)2, database.NumberOfMethods());

            const Composition* const compOne = database.CompositionsDatabase().FindComposition(1);
            Assert::AreEqual(L"Paul Needham", compOne->ComposerName().c_str());


            const Composition* const compTwo = database.CompositionsDatabase().FindComposition(2);
            Assert::AreEqual(L"Noone", compTwo->ComposerName().c_str());
        }
    };
}
