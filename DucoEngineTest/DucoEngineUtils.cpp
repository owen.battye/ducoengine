﻿#include "CppUnitTest.h"
#include <DucoEngineUtils.h>
#include <Ring.h>
#include "ToString.h"
#include <iostream>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(DucoEngineUtils_UnitTests)
    {
    public:

        TEST_METHOD(DucoEngineUtils_RemoveAllButValidChars)
        {
            std::wstring validChars = L"ABDCd";
            std::wstring validString = L"DDdCABDCAB";
            Assert::AreEqual(validString, DucoEngineUtils::RemoveAllButValidChars(L"aDDd.CrfgAaBhDjCkAlB;", validChars));
        }

        TEST_METHOD(DucoEngineUtils_StartsWith)
        {
            Assert::IsTrue(DucoEngineUtils::StartsWith(L"ABC", L"ABC"));
            Assert::IsTrue(DucoEngineUtils::StartsWith(L"ABC", L"ABCDEF"));
            Assert::IsFalse(DucoEngineUtils::StartsWith(L"ABC", L"AB"));
            Assert::IsFalse(DucoEngineUtils::StartsWith(L"", L"ABCDEF"));
            Assert::IsFalse(DucoEngineUtils::StartsWith(L"ABC", L"ACDEFE"));
        }

        TEST_METHOD(DucoEngineUtils_RemoveBrackets)
        {
            Assert::AreEqual(L"ABC", DucoEngineUtils::RemoveBrackets(L"ABC").c_str());
            Assert::AreEqual(L"ABC", DucoEngineUtils::RemoveBrackets(L"ABC (123)").c_str());
            Assert::AreEqual(L"ABC  DEF", DucoEngineUtils::RemoveBrackets(L"ABC (123) DEF (456)").c_str());
            Assert::AreEqual(L"ABC", DucoEngineUtils::RemoveBrackets(L" (123) ABC").c_str());
            Assert::AreEqual(L"ERF ABC", DucoEngineUtils::RemoveBrackets(L"ERF (123)ABC").c_str());
            Assert::AreEqual(L"", DucoEngineUtils::RemoveBrackets(L"(123)").c_str());
            Assert::AreEqual(L"", DucoEngineUtils::RemoveBrackets(L" (123)").c_str());
            Assert::AreEqual(L"(123", DucoEngineUtils::RemoveBrackets(L" (123").c_str());
        }

        TEST_METHOD(DucoEngineUtils_ConductorIndicator)
        {
            Assert::IsFalse(DucoEngineUtils::ConductorIndicator(L""));
            Assert::IsTrue(DucoEngineUtils::ConductorIndicator(L"C"));
            Assert::IsTrue(DucoEngineUtils::ConductorIndicator(L"c"));
            Assert::IsTrue(DucoEngineUtils::ConductorIndicator(L"conductor"));
            Assert::IsTrue(DucoEngineUtils::ConductorIndicator(L"Cond"));
            Assert::IsTrue(DucoEngineUtils::ConductorIndicator(L"Cond."));
            Assert::IsFalse(DucoEngineUtils::ConductorIndicator(L"Owen Battye"));
        }

        TEST_METHOD(DucoEngineUtils_CheckRingerNameForConductor)
        {
            std::wstring originalName = L"Owen Battye (C)";
            bool conductor = false;
            Assert::AreEqual(L"Owen Battye", DucoEngineUtils::CheckRingerNameForConductor(originalName, conductor).c_str());
            Assert::IsTrue(conductor);

            originalName = L"James Brown";
            Assert::AreEqual(originalName, DucoEngineUtils::CheckRingerNameForConductor(originalName, conductor));
            Assert::IsFalse(conductor);
        }

        TEST_METHOD(DucoEngineUtils_CheckRingerNameForConductor_OtherText)
        {
            std::wstring originalName = L"Owen Battye (Moulton)";
            bool conductor = false;
            Assert::AreEqual(originalName, DucoEngineUtils::CheckRingerNameForConductor(originalName, conductor));
            Assert::IsFalse(conductor);
        }

        TEST_METHOD(DucoEngineUtils_CheckRingerNameForConductor_OtherText_InvalidBrackets_AndConductor)
        {
            bool conductor = false;
            std::wstring originalName = L"Owen Battye (Cond))";
            Assert::AreEqual(L"Owen Battye", DucoEngineUtils::CheckRingerNameForConductor(originalName, conductor).c_str());
            Assert::IsTrue(conductor);
        }

        TEST_METHOD(DucoEngineUtils_CheckRingerNameForConductor_OtherText_InvalidBrackets)
        {
            bool conductor = false;
            std::wstring originalName = L"Owen Battye (Moulton))";
            Assert::AreEqual(originalName, DucoEngineUtils::CheckRingerNameForConductor(originalName, conductor));
            Assert::IsFalse(conductor);
        }

        TEST_METHOD(DucoEngineUtils_CheckRingerNameForConductorAndOtherBrackets)
        {
            std::wstring conductorAndPlace = L"James Adams (Ashton) (C)";
            bool isConductor = false;

            Assert::AreEqual(L"James Adams  (Ashton)", DucoEngineUtils::CheckRingerNameForConductor(conductorAndPlace, isConductor).c_str());
            Assert::IsTrue(isConductor);
        }

        TEST_METHOD(DucoEngineUtils_FormatTowerName)
        {
            Assert::AreEqual(L"Moulton, Northants. (St Peter)", DucoEngineUtils::FormatTowerName(L"Moulton", L"Northants", L"St Peter").c_str());
        }

        TEST_METHOD(DucoEngineUtils_FormatRingName)
        {
            Ring theRing;
            theRing.SetTenorWeight(L"12C");
            theRing.SetNoOfBells(8);

            Assert::AreEqual(L"8 bells. (12C) ", DucoEngineUtils::FormatRingName(theRing).c_str());

            theRing.SetName(L"Back eight");
            Assert::AreEqual(L"Back eight, 8 bells. (12C) ", DucoEngineUtils::FormatRingName(theRing).c_str());
        }

        TEST_METHOD(DucoEngineUtils_StringToDate)
        {
            PerformanceDate dateOne(1963, 8, 11);
            Assert::AreEqual(dateOne, PerformanceDate(L"11/08/1963"));

            PerformanceDate dateTwo(1965, 06, 15);
            Assert::AreEqual(dateTwo, PerformanceDate(L"15/06/1965"));
            Assert::AreEqual(dateTwo, PerformanceDate(L"15/JUN/1965"));

            Assert::AreEqual(dateTwo, PerformanceDate(L"15 JUN 1965", true));
        }

        TEST_METHOD(DucoEngineUtils_ToInteger_BellNumber)
        {
            Assert::AreEqual((unsigned int)1, DucoEngineUtils::ToInteger(L'1'));
            Assert::AreEqual((unsigned int)2, DucoEngineUtils::ToInteger(L'2'));
            Assert::AreEqual((unsigned int)3, DucoEngineUtils::ToInteger(L'3'));
            Assert::AreEqual((unsigned int)4, DucoEngineUtils::ToInteger(L'4'));
            Assert::AreEqual((unsigned int)5, DucoEngineUtils::ToInteger(L'5'));
            Assert::AreEqual((unsigned int)6, DucoEngineUtils::ToInteger(L'6'));
            Assert::AreEqual((unsigned int)7, DucoEngineUtils::ToInteger(L'7'));
            Assert::AreEqual((unsigned int)8, DucoEngineUtils::ToInteger(L'8'));
            Assert::AreEqual((unsigned int)9, DucoEngineUtils::ToInteger(L'9'));
            Assert::AreEqual((unsigned int)10, DucoEngineUtils::ToInteger(L'0'));
            Assert::AreEqual((unsigned int)11, DucoEngineUtils::ToInteger(L'E'));
            Assert::AreEqual((unsigned int)11, DucoEngineUtils::ToInteger(L'e'));
            Assert::AreEqual((unsigned int)12, DucoEngineUtils::ToInteger(L'T'));
            Assert::AreEqual((unsigned int)12, DucoEngineUtils::ToInteger(L't'));
            Assert::AreEqual((unsigned int)13, DucoEngineUtils::ToInteger(L'A'));
            Assert::AreEqual((unsigned int)13, DucoEngineUtils::ToInteger(L'a'));
            Assert::AreEqual((unsigned int)14, DucoEngineUtils::ToInteger(L'B'));
            Assert::AreEqual((unsigned int)14, DucoEngineUtils::ToInteger(L'b'));
            Assert::AreEqual((unsigned int)15, DucoEngineUtils::ToInteger(L'C'));
            Assert::AreEqual((unsigned int)15, DucoEngineUtils::ToInteger(L'c'));
            Assert::AreEqual((unsigned int)16, DucoEngineUtils::ToInteger(L'D'));
            Assert::AreEqual((unsigned int)16, DucoEngineUtils::ToInteger(L'd'));
            Assert::AreEqual((unsigned int)17, DucoEngineUtils::ToInteger(L'F'));
            Assert::AreEqual((unsigned int)17, DucoEngineUtils::ToInteger(L'f'));
            Assert::AreEqual((unsigned int)18, DucoEngineUtils::ToInteger(L'G'));
            Assert::AreEqual((unsigned int)18, DucoEngineUtils::ToInteger(L'g'));
            Assert::AreEqual((unsigned int)19, DucoEngineUtils::ToInteger(L'H'));
            Assert::AreEqual((unsigned int)19, DucoEngineUtils::ToInteger(L'h'));
            Assert::AreEqual((unsigned int)20, DucoEngineUtils::ToInteger(L'J'));
            Assert::AreEqual((unsigned int)20, DucoEngineUtils::ToInteger(L'j'));
            Assert::AreEqual((unsigned int)21, DucoEngineUtils::ToInteger(L'K'));
            Assert::AreEqual((unsigned int)21, DucoEngineUtils::ToInteger(L'k'));
            Assert::AreEqual((unsigned int)22, DucoEngineUtils::ToInteger(L'L'));
            Assert::AreEqual((unsigned int)22, DucoEngineUtils::ToInteger(L'l'));
            Assert::AreEqual((unsigned int)0, DucoEngineUtils::ToInteger(L' '));
        }

        TEST_METHOD(DucoEngineUtils_ToChar_BellNumber)
        {
            Assert::AreEqual(L'1', DucoEngineUtils::ToChar(1));
            Assert::AreEqual(L'2', DucoEngineUtils::ToChar(2));
            Assert::AreEqual(L'3', DucoEngineUtils::ToChar(3));
            Assert::AreEqual(L'4', DucoEngineUtils::ToChar(4));
            Assert::AreEqual(L'5', DucoEngineUtils::ToChar(5));
            Assert::AreEqual(L'6', DucoEngineUtils::ToChar(6));
            Assert::AreEqual(L'7', DucoEngineUtils::ToChar(7));
            Assert::AreEqual(L'8', DucoEngineUtils::ToChar(8));
            Assert::AreEqual(L'9', DucoEngineUtils::ToChar(9));
            Assert::AreEqual(L'0', DucoEngineUtils::ToChar(10));
            Assert::AreEqual(L'E', DucoEngineUtils::ToChar(11));
            Assert::AreEqual(L'T', DucoEngineUtils::ToChar(12));
            Assert::AreEqual(L'A', DucoEngineUtils::ToChar(13));
            Assert::AreEqual(L'B', DucoEngineUtils::ToChar(14));
            Assert::AreEqual(L'C', DucoEngineUtils::ToChar(15));
            Assert::AreEqual(L'D', DucoEngineUtils::ToChar(16));
            Assert::AreEqual(L'F', DucoEngineUtils::ToChar(17));
            Assert::AreEqual(L'G', DucoEngineUtils::ToChar(18));
            Assert::AreEqual(L'H', DucoEngineUtils::ToChar(19));
            Assert::AreEqual(L'J', DucoEngineUtils::ToChar(20));
            Assert::AreEqual(L'K', DucoEngineUtils::ToChar(21));
            Assert::AreEqual(L'L', DucoEngineUtils::ToChar(22));
            Assert::AreEqual(L'Z', DucoEngineUtils::ToChar(55));
        }

        TEST_METHOD(DucoEngineUtils_ToName)
        {
            Assert::AreEqual(L"3", DucoEngineUtils::ToName(3).c_str());
            Assert::AreEqual(L"Four", DucoEngineUtils::ToName(4).c_str());
            Assert::AreEqual(L"Five", DucoEngineUtils::ToName(5).c_str());
            Assert::AreEqual(L"Six", DucoEngineUtils::ToName(6).c_str());
            Assert::AreEqual(L"Seven", DucoEngineUtils::ToName(7).c_str());
            Assert::AreEqual(L"Eight", DucoEngineUtils::ToName(8).c_str());
            Assert::AreEqual(L"Nine", DucoEngineUtils::ToName(9).c_str());
            Assert::AreEqual(L"Ten", DucoEngineUtils::ToName(10).c_str());
            Assert::AreEqual(L"Eleven", DucoEngineUtils::ToName(11).c_str());
            Assert::AreEqual(L"Twelve", DucoEngineUtils::ToName(12).c_str());
            Assert::AreEqual(L"Thirteen", DucoEngineUtils::ToName(13).c_str());
            Assert::AreEqual(L"Fourteen", DucoEngineUtils::ToName(14).c_str());
            Assert::AreEqual(L"Fifteen", DucoEngineUtils::ToName(15).c_str());
            Assert::AreEqual(L"Sixteen", DucoEngineUtils::ToName(16).c_str());
            Assert::AreEqual(L"17", DucoEngineUtils::ToName(17).c_str());
        }

        TEST_METHOD(DucoEngineUtils_IsEven)
        {
            Assert::IsTrue(DucoEngineUtils::IsEven(2));
            Assert::IsFalse(DucoEngineUtils::IsEven(3));
            Assert::IsTrue(DucoEngineUtils::IsEven(6));
            Assert::IsFalse(DucoEngineUtils::IsEven(7));
            Assert::IsTrue(DucoEngineUtils::IsEven(8));
            Assert::IsFalse(DucoEngineUtils::IsEven(9));
            Assert::IsTrue(DucoEngineUtils::IsEven(10));
            Assert::IsFalse(DucoEngineUtils::IsEven(11));
            Assert::IsTrue(DucoEngineUtils::IsEven(12));
            Assert::IsFalse(DucoEngineUtils::IsEven(21));
            Assert::IsTrue(DucoEngineUtils::IsEven(22));
        }

        TEST_METHOD(DucoEngineUtils_NoOfBellsMatch)
        {
            Assert::IsTrue(DucoEngineUtils::NoOfBellsMatch(8, 8));
            Assert::IsTrue(DucoEngineUtils::NoOfBellsMatch(7, 8));
            Assert::IsTrue(DucoEngineUtils::NoOfBellsMatch(10, 10));
            Assert::IsTrue(DucoEngineUtils::NoOfBellsMatch(9, 10));

            Assert::IsFalse(DucoEngineUtils::NoOfBellsMatch(11, 10));
            Assert::IsFalse(DucoEngineUtils::NoOfBellsMatch(7, 10));
            Assert::IsFalse(DucoEngineUtils::NoOfBellsMatch(7, -1));
        }

        TEST_METHOD(DucoEngineUtils_IsAllUpperCase)
        {
            Assert::IsTrue(DucoEngineUtils::IsAllUpperCase(L"OWEN"));
            Assert::IsFalse(DucoEngineUtils::IsAllUpperCase(L"Owen"));
        }

        TEST_METHOD(DucoEngineUtils_PadStringWithZeros)
        {
            Assert::AreEqual(L"00010", DucoEngineUtils::PadStringWithZeros(L"10").c_str());
            Assert::AreEqual(L"10050", DucoEngineUtils::PadStringWithZeros(L"10050").c_str());
            Assert::AreEqual(L"00431", DucoEngineUtils::PadStringWithZeros(L"431").c_str());
            Assert::AreEqual(L"00009", DucoEngineUtils::PadStringWithZeros(L"9").c_str());
            Assert::AreEqual(L"01234", DucoEngineUtils::PadStringWithZeros(L"1234").c_str());
            Assert::AreEqual(L"", DucoEngineUtils::PadStringWithZeros(L"").c_str());
            Assert::AreEqual(L"00000", DucoEngineUtils::PadStringWithZeros(L"a").c_str());
            Assert::AreEqual(L"00000", DucoEngineUtils::PadStringWithZeros(L"ab").c_str());
            Assert::AreEqual(L"00012ab", DucoEngineUtils::PadStringWithZeros(L"12ab").c_str());
        }

        TEST_METHOD(DucoEngineUtils_Count)
        {
            Assert::AreEqual((unsigned int)1, DucoEngineUtils::Count(L"&x16x16x16-12", '-'));
            Assert::AreEqual((unsigned int)0, DucoEngineUtils::Count(L"&x16x16x16", '-'));
        }

        TEST_METHOD(DucoEngineUtils_IsNumber)
        {
            Assert::IsTrue(DucoEngineUtils::IsNumber(L"25"));
            Assert::IsFalse(DucoEngineUtils::IsNumber(L"25a"));
            Assert::IsFalse(DucoEngineUtils::IsNumber(L"abc"));
        }

        TEST_METHOD(DucoEngineUtils_SplitRingerName)
        {
            std::wstring owenBattyeAsConductor = L"R Owen Battye (C)";
            std::wstring forename;
            std::wstring surname;
            bool isConductor = false;

            DucoEngineUtils::SplitRingerName(owenBattyeAsConductor, forename, surname, isConductor);
            Assert::IsTrue(isConductor);
            Assert::AreEqual(L"R Owen", forename.c_str());
            Assert::AreEqual(L"Battye", surname.c_str());
        }

        TEST_METHOD(DucoEngineUtils_SplitRingerNameWithBrackets)
        {
            std::wstring stephaniePattenden = L"Stephanie J Pattenden (SRCY)";
            std::wstring forename;
            std::wstring surname;
            bool isConductor = false;

            DucoEngineUtils::SplitRingerName(stephaniePattenden, forename, surname, isConductor);
            Assert::IsFalse(isConductor);
            Assert::AreEqual(L"Stephanie J (SRCY)", forename.c_str());
            Assert::AreEqual(L"Pattenden", surname.c_str());
        }

        TEST_METHOD(DucoEngineUtils_SplitRingerNameWithBracketsAndConductor)
        {
            std::wstring conductorAndPlace = L"James Adams (Ashton) (C)";
            std::wstring forename;
            std::wstring surname;
            bool isConductor = false;

            DucoEngineUtils::SplitRingerName(conductorAndPlace, forename, surname, isConductor);
            Assert::IsTrue(isConductor);
            Assert::AreEqual(L"James (Ashton)", forename.c_str());
            Assert::AreEqual(L"Adams", surname.c_str());
        }

        TEST_METHOD(DucoEngineUtils_SplitRingerNameWithTooManyBrackets)
        {
            std::wstring conductorAndPlace = L"Robert Williams (Stalybridge))";
            std::wstring forename;
            std::wstring surname;
            bool isConductor = false;

            DucoEngineUtils::SplitRingerName(conductorAndPlace, forename, surname, isConductor);
            Assert::IsFalse(isConductor);
            Assert::AreEqual(L"Robert (Stalybridge))", forename.c_str());
            Assert::AreEqual(L"Williams", surname.c_str());
        }

        TEST_METHOD(DucoEngineUtils_SplitRingerNameWithBracketsAndSpaces)
        {
            std::wstring stephaniePattenden = L"Frank T Blagrove (Middlesex CA & London DG)";
            std::wstring forename;
            std::wstring surname;
            bool isConductor = false;

            DucoEngineUtils::SplitRingerName(stephaniePattenden, forename, surname, isConductor);
            Assert::IsFalse(isConductor);
            Assert::AreEqual(L"Frank T (Middlesex CA & London DG)", forename.c_str());
            Assert::AreEqual(L"Blagrove", surname.c_str());
        }

        TEST_METHOD(DucoEngineUtils_SplitRingerName_SurnameFirst)
        {
            std::wstring owenBattyeAsConductor = L"Battye, R Owen (C)";
            std::wstring forename;
            std::wstring surname;
            bool isConductor = false;

            DucoEngineUtils::SplitRingerName(owenBattyeAsConductor, forename, surname, isConductor);
            Assert::IsTrue(isConductor);
            Assert::AreEqual(L"R Owen", forename.c_str());
            Assert::AreEqual(L"Battye", surname.c_str());
        }

        TEST_METHOD(DucoEngineUtils_GetInitialsFromName)
        {
            Assert::AreEqual(L"RO", DucoEngineUtils::GetInitialsFromName(L"R O").c_str());
            Assert::AreEqual(L"RO", DucoEngineUtils::GetInitialsFromName(L"Robert Owen").c_str());
            Assert::AreEqual(L"RO", DucoEngineUtils::GetInitialsFromName(L"Robert Owen Battye", true).c_str());
            Assert::AreEqual(L"ROB", DucoEngineUtils::GetInitialsFromName(L"Robert Owen Battye").c_str());
            Assert::AreEqual(L"JL", DucoEngineUtils::GetInitialsFromName(L"Jennie Lynne").c_str());
            Assert::AreEqual(L"JL", DucoEngineUtils::GetInitialsFromName(L"Jennie Lynne Care", true).c_str());
            Assert::AreEqual(L"RO", DucoEngineUtils::GetInitialsFromName(L"R Owe").c_str());
        }

        TEST_METHOD(DucoEngineUtils_SplitTowerName_NorthamptonStGiles)
        {
            std::wstring fullTowerName = L"Northampton, Northamptonshire. (St Giles)";
            std::wstring name;
            std::wstring townOrCity;
            std::wstring county;

            DucoEngineUtils::SplitTowerName(fullTowerName, name, townOrCity, county);
            Assert::AreEqual(L"St Giles", name.c_str());
            Assert::AreEqual(L"Northampton", townOrCity.c_str());
            Assert::AreEqual(L"Northamptonshire", county.c_str());
        }

        TEST_METHOD(DucoEngineUtils_SplitTowerName_NorthamptonStGilesNoPunctuation)
        {
            std::wstring fullTowerName = L"Northampton Northamptonshire (St Giles)";
            std::wstring name;
            std::wstring townOrCity;
            std::wstring county;

            DucoEngineUtils::SplitTowerName(fullTowerName, name, townOrCity, county);
            Assert::AreEqual(L"St Giles", name.c_str());
            Assert::AreEqual(L"Northampton", townOrCity.c_str());
            Assert::AreEqual(L"Northamptonshire", county.c_str());
        }

        TEST_METHOD(DucoEngineUtils_SplitTowerName_KingsthorpeNorthamptonNoPunctuation)
        {
            std::wstring fullTowerName = L"Northampton Northamptonshire (St Peter, Kingsthorpe)";
            std::wstring name;
            std::wstring townOrCity;
            std::wstring county;

            DucoEngineUtils::SplitTowerName(fullTowerName, name, townOrCity, county);
            Assert::AreEqual(L"St Peter, Kingsthorpe", name.c_str());
            Assert::AreEqual(L"Northampton", townOrCity.c_str());
            Assert::AreEqual(L"Northamptonshire", county.c_str());
        }

        TEST_METHOD(DucoEngineUtils_SplitTowerName_MissingCounty)
        {
            std::wstring fullTowerName = L"Northampton (St Giles)";
            std::wstring name;
            std::wstring townOrCity;
            std::wstring county;

            DucoEngineUtils::SplitTowerName(fullTowerName, name, townOrCity, county);
            Assert::AreEqual(L"St Giles", name.c_str());
            Assert::AreEqual(L"Northampton", townOrCity.c_str());
            Assert::AreEqual(L"", county.c_str());
        }
        TEST_METHOD(DucoEngineUtils_SplitTowerName_TownOnly)
        {
            std::wstring fullTowerName = L"Northampton";
            std::wstring name = L"Should be cleared";
            std::wstring townOrCity = L"Should be cleared";
            std::wstring county = L"Should be cleared";

            DucoEngineUtils::SplitTowerName(fullTowerName, name, townOrCity, county);
            Assert::AreEqual(L"", name.c_str());
            Assert::AreEqual(L"Northampton", townOrCity.c_str());
            Assert::AreEqual(L"", county.c_str());
        }
        TEST_METHOD(DucoEngineUtils_SplitTowerName_Invalid)
        {
            std::wstring fullTowerName = L"";
            std::wstring name = L"Should be cleared";
            std::wstring townOrCity = L"Should be cleared";
            std::wstring county = L"Should be cleared";

            DucoEngineUtils::SplitTowerName(fullTowerName, name, townOrCity, county);
            Assert::AreEqual(L"", name.c_str());
            Assert::AreEqual(L"", townOrCity.c_str());
            Assert::AreEqual(L"", county.c_str());
        }

        TEST_METHOD(DucoEngineUtils_GetInitials)
        {
            std::wstring owenBattye = L"R Owen Battye";
            Assert::AreEqual(L"ROB", DucoEngineUtils::GetInitials(owenBattye).c_str());
        }

        TEST_METHOD(DucoEngineUtils_MethodNameContainsSpliced)
        {
            Assert::IsFalse(DucoEngineUtils::MethodNameContainsSpliced(L"Plain Bob Minor"));
            Assert::IsTrue(DucoEngineUtils::MethodNameContainsSpliced(L"Spliced Surprise Major"));
            Assert::IsTrue(DucoEngineUtils::MethodNameContainsSpliced(L"4 Spliced Surprise Major"));
            Assert::IsTrue(DucoEngineUtils::MethodNameContainsSpliced(L"4 Methods Surprise Major"));
            Assert::IsTrue(DucoEngineUtils::MethodNameContainsSpliced(L"Multimethods Surprise Major"));
        }

        TEST_METHOD(DucoEngineUtils_RemoveMethodCount_NoMethodCount)
        {
            std::wstring methodName = L"Plain";
            std::wstring noOfMethods = L"99";
            Assert::IsFalse(DucoEngineUtils::RemoveMethodCount(methodName, noOfMethods));
            Assert::AreEqual(L"", noOfMethods.c_str());
            Assert::AreEqual(L"Plain", methodName.c_str());
        }

        TEST_METHOD(DucoEngineUtils_RemoveMethodCount_MethodCountInBracketsAfterwards)
        {
            std::wstring methodName = L"Spliced (12m)";
            std::wstring noOfMethods = L"99";
            Assert::IsTrue(DucoEngineUtils::RemoveMethodCount(methodName, noOfMethods));
            Assert::AreEqual(L"12m", noOfMethods.c_str());
            Assert::AreEqual(L"Spliced", methodName.c_str());
        }

        TEST_METHOD(DucoEngineUtils_RemoveMethodCount_MethodCountInBracketsAfterwardsNoM)
        {
            std::wstring methodName = L"Spliced (7)";
            std::wstring noOfMethods = L"99";
            Assert::IsTrue(DucoEngineUtils::RemoveMethodCount(methodName, noOfMethods));
            Assert::AreEqual(L"7", noOfMethods.c_str());
            Assert::AreEqual(L"Spliced", methodName.c_str());
        }

        TEST_METHOD(DucoEngineUtils_RemoveMethodCount_MethodCountInBracketsBeforeNoM)
        {
            std::wstring methodName = L"(6) Spliced";
            std::wstring noOfMethods = L"99";
            Assert::IsTrue(DucoEngineUtils::RemoveMethodCount(methodName, noOfMethods));
            Assert::AreEqual(L"6", noOfMethods.c_str());
            Assert::AreEqual(L"Spliced", methodName.c_str());
        }

        TEST_METHOD(DucoEngineUtils_RemoveMethodCount_MethodCountInBracketsBefore)
        {
            std::wstring methodName = L"(5m) Spliced";
            std::wstring noOfMethods = L"99";
            Assert::IsTrue(DucoEngineUtils::RemoveMethodCount(methodName, noOfMethods));
            Assert::AreEqual(L"5m", noOfMethods.c_str());
            Assert::AreEqual(L"Spliced", methodName.c_str());
        }

        TEST_METHOD(DucoEngineUtils_RemoveMethodCount_MethodCountInBracketsInMiddle)
        {
            std::wstring methodName = L"Something (3m) Spliced";
            std::wstring noOfMethods = L"99";
            Assert::IsTrue(DucoEngineUtils::RemoveMethodCount(methodName, noOfMethods));
            Assert::AreEqual(L"3m", noOfMethods.c_str());
            Assert::AreEqual(L"Something Spliced", methodName.c_str());
        }

        TEST_METHOD(DucoEngineUtils_EncodeString_normalString)
        {
            std::wstring methodName = L"Something (3m) Spliced";
            Assert::AreEqual(methodName, DucoEngineUtils::EncodeString(methodName));
        }
        TEST_METHOD(DucoEngineUtils_EncodeString_StringWithQuotes)
        {
            std::wstring methodName = L"Bart's \\Surprise Royal\"";
            Assert::AreEqual(L"Bart\\\'s \\\\Surprise Royal\\\"", DucoEngineUtils::EncodeString(methodName).c_str());
        }
        TEST_METHOD(DucoEngineUtils_EncodeString_StringWithNewLine)
        {
            std::wostringstream temp;
            temp << "Owen Was ";
            temp << std::endl;
            temp << L" ere";
            std::cout << std::endl;
            Assert::AreEqual(L"Owen Was \\n ere", DucoEngineUtils::EncodeString(temp.str()).c_str());
        }

        TEST_METHOD(DucoEngineUtils_Tokenise_ConductorIds)
        {
            std::wstring originalConductors = L"1,5,3,2";
            std::list<Duco::ObjectId> conductors;
            std::list<Duco::ObjectId> expectedConductors = {1,5,3,2};
            DucoEngineUtils::Tokenise(originalConductors, conductors, L",");
            Assert::AreEqual(expectedConductors.size(), conductors.size());
            Assert::AreEqual(expectedConductors, conductors);
        }

        TEST_METHOD(DucoEngineUtils_Tokenise_DoveFieldIds)
        {
            std::wstring originalFields = L"TowerID,RingID,RingType,Place,Place2,PlaceCL,Dedicn,AltName";
            std::list<std::wstring> fields;
            std::list<std::wstring> expectedFields= { L"TowerID",L"RingID",L"RingType",L"Place",L"Place2",L"PlaceCL",L"Dedicn",L"AltName" };
            DucoEngineUtils::Tokenise(originalFields, fields, L",");
            Assert::AreEqual(expectedFields.size(), fields.size());
            Assert::AreEqual(expectedFields, fields);
        }

        TEST_METHOD(DucoEngineUtils_Tokenise_DoveStrings)
        {
            std::wstring originalFields = L"15023,5024,Full-circle ring,Abingdon,,\"Abingdon, S Helen\",S Helen,,";
            std::list<std::wstring> fields;
            std::list<std::wstring> expectedFields = { L"15023",L"5024",L"Full-circle ring",L"Abingdon",L"",L"Abingdon, S Helen",L"S Helen",L"" };
            DucoEngineUtils::Tokenise(originalFields, fields, L",");
            Assert::AreEqual(expectedFields.size(), fields.size());
            Assert::AreEqual(expectedFields, fields);
        }
        //    size_t start = s.find_first_not_of(WHITESPACE);

        TEST_METHOD(DucoEngineUtils_Trim_WhiteSpaceAtEachEnd)
        {
            std::wstring originalFields = L" \tTest ";
            Assert::AreEqual(std::wstring(L"Test"), DucoEngineUtils::Trim(originalFields));
        }

        TEST_METHOD(DucoEngineUtils_Trim_WhiteSpaceAtEachEndAndInMiddle)
        {
            std::wstring originalFields = L" \tTest test ";
            Assert::AreEqual(std::wstring(L"Test test"), DucoEngineUtils::Trim(originalFields));
        }

        TEST_METHOD(DucoEngineUtils_TrimTowerBaseId_LeadingZeros)
        {
            std::wstring originalFields = L" 0863";
            Assert::AreEqual(std::wstring(L"863"), DucoEngineUtils::TrimTowerBaseId(originalFields));
        }

        TEST_METHOD(DucoEngineUtils_TrimTowerBaseId_LeadingZerosAndKeepMiddle)
        {
            std::wstring originalFields = L" 08030";
            Assert::AreEqual(std::wstring(L"8030"), DucoEngineUtils::TrimTowerBaseId(originalFields));
        }

        TEST_METHOD(DucoEngineUtils_TenorWeight_KeepValidChars)
        {
            std::wstring originalWeight = L"18 cwt";
            Assert::AreEqual(std::wstring(L"18 cwt"), DucoEngineUtils::RemoveInvalidCharsFromTenorWeight(originalWeight));
        }

        TEST_METHOD(DucoEngineUtils_TenorWeight_RemoveInvalidChars)
        {
            std::wstring originalWeight (L"1-2–7 %cwt");
            Assert::AreEqual(std::wstring(L"1-2-7 cwt"), DucoEngineUtils::RemoveInvalidCharsFromTenorWeight(originalWeight));
        }

        TEST_METHOD(DucoEngineUtils_TenorKey_CorrectChars)
        {
            Assert::AreEqual(std::wstring(L"D"), DucoEngineUtils::ReplaceTenorKeySymbols(L"d"));
            Assert::AreEqual(std::wstring(L""), DucoEngineUtils::ReplaceTenorKeySymbols(L""));
            Assert::AreEqual(std::wstring(L"A"), DucoEngineUtils::ReplaceTenorKeySymbols(L"ABFS"));
            Assert::AreEqual(std::wstring(L""), DucoEngineUtils::ReplaceTenorKeySymbols(L"H"));
            Assert::AreEqual(std::wstring(L"F♯"), DucoEngineUtils::ReplaceTenorKeySymbols(L"F#"));
            Assert::AreEqual(std::wstring(L"F♯"), DucoEngineUtils::ReplaceTenorKeySymbols(L"F♯"));
            Assert::AreEqual(std::wstring(L"A♭"), DucoEngineUtils::ReplaceTenorKeySymbols(L"Ab"));
            Assert::AreEqual(std::wstring(L"A♭"), DucoEngineUtils::ReplaceTenorKeySymbols(L"A♭"));
        }

        TEST_METHOD(DucoEngineUtils_FormatIntegerToStringWithoutSeperators_1000)
        {
            Assert::AreEqual(std::wstring(L"1000"), DucoEngineUtils::ToString(1000));
        }
        TEST_METHOD(DucoEngineUtils_FormatIntegerToString_1000)
        {
            Assert::AreEqual(std::wstring(L"1,000"), DucoEngineUtils::ToString(1000, true));
        }
        TEST_METHOD(DucoEngineUtils_FormatFloatToStringWithoutSeperators_1000)
        {
            Assert::AreEqual(std::wstring(L"3.14159"), DucoEngineUtils::ToString(3.1415926f));
        }
        TEST_METHOD(DucoEngineUtils_FormatFloatToString_1000)
        {
            Assert::AreEqual(std::wstring(L"6,783.14160"), DucoEngineUtils::ToString(6783.1415966f, true));
        }

        TEST_METHOD(DucoEngineUtils_CompareString_IdenticalBellBoardId)
        {
            Assert::IsTrue(DucoEngineUtils::CompareString(L"1705567", L"1705567"));
        }

        TEST_METHOD(DucoEngineUtils_CompareString_NotMatch)
        {
            Assert::IsFalse(DucoEngineUtils::CompareString(L"James", L"Owen"));
        }
        TEST_METHOD(DucoEngineUtils_CompareString_IgnoreCase)
        {
            Assert::IsTrue(DucoEngineUtils::CompareString(L"OWEN", L"owen", true, false));
        }
        TEST_METHOD(DucoEngineUtils_CompareString_EmptyStrings_EmptyStringNotAllowed)
        {
            Assert::IsFalse(DucoEngineUtils::CompareString(L"", L"", false, false));
        }
        TEST_METHOD(DucoEngineUtils_CompareString_EmptyStrings_AllowEmpty)
        {
            Assert::IsTrue(DucoEngineUtils::CompareString(L"", L"", false, true));
        }

        TEST_METHOD(DucoEngineUtils_CompareStringWithIgnoredChars_MatchSaints)
        {
            Assert::IsTrue(DucoEngineUtils::FindSubstring(L"St Jamess", L"st james's", L" '", true));
        }

        TEST_METHOD(DucoEngineUtils_CompareTenor_Identical)
        {
            Assert::IsTrue(DucoEngineUtils::CompareTenorWeight(L"11-1-14", L"11-1-14"));
        }
        TEST_METHOD(DucoEngineUtils_CompareTenor_Rounded)
        {
            Assert::IsTrue(DucoEngineUtils::CompareTenorWeight(L"11", L"11-1-14"));
        }
        TEST_METHOD(DucoEngineUtils_CompareTenor_Blank)
        {
            Assert::IsFalse(DucoEngineUtils::CompareTenorWeight(L"", L"11-1-14"));
        }
        TEST_METHOD(DucoEngineUtils_CompareTenor_BothBlank)
        {
            Assert::IsFalse(DucoEngineUtils::CompareTenorWeight(L"", L""));
        }
        TEST_METHOD(DucoEngineUtils_CompareTenor_Close)
        {
            Assert::IsTrue(DucoEngineUtils::CompareTenorWeight(L"14-3-4", L"15"));
        }
        TEST_METHOD(DucoEngineUtils_CompareTenor_DifferentDashes)
        {
            Assert::IsTrue(DucoEngineUtils::CompareTenorWeight(L"15–1–16", L"15-1-16"));
        }
        TEST_METHOD(DucoEngineUtils_ToUpperCase)
        {
            std::wstring uppercase;
            DucoEngineUtils::ToUpperCase(L"Owen", uppercase);
            Assert::AreEqual(L"OWEN", uppercase.c_str());
        }
        TEST_METHOD(DucoEngineUtils_ToLowerCase)
        {
            std::wstring lowercase;
            DucoEngineUtils::ToLowerCase(L"Owen Battye", lowercase);
            Assert::AreEqual(L"owen battye", lowercase.c_str());
        }
        TEST_METHOD(DucoEngineUtils_ToLowerCase_string)
        {
            std::string lowercase;
            DucoEngineUtils::ToLowerCase("Owen", lowercase);
            Assert::AreEqual("owen", lowercase.c_str());
        }
        TEST_METHOD(DucoEngineUtils_ToCapitalised)
        {
            Assert::AreEqual(L"Owen battye", DucoEngineUtils::ToCapitalised(L"OWEN BATTYE", false).c_str());
            Assert::AreEqual(L"Owen Battye", DucoEngineUtils::ToCapitalised(L"OWEN BATTYE", true).c_str());
            Assert::AreEqual(L"R Owen Battye", DucoEngineUtils::ToCapitalised(L"R OWEN BATTYE", true).c_str());
            Assert::AreEqual(L"Surprise", DucoEngineUtils::ToCapitalised(L"SURPRISE", false).c_str());
            Assert::AreEqual(L"New cambridge", DucoEngineUtils::ToCapitalised(L"NEW CAMBRIDGE", false).c_str());
            Assert::AreEqual(L"Graham J N Colbourne", DucoEngineUtils::ToCapitalised(L"graham j n colbourne", true).c_str());
            Assert::AreEqual(L"Graham J N Colbourne", DucoEngineUtils::ToCapitalised(L"GRAHAM J N COLBOURNE", true).c_str());
            Assert::AreEqual(L"Graham J N colbourne", DucoEngineUtils::ToCapitalised(L"GRAHAM J N COLBOURNE", false).c_str());
        }

        TEST_METHOD(DucoEngineUtils_ConductorType_ToString)
        {
            Assert::AreEqual(L"Single conductor", DucoEngineUtils::ToString(TConductorType::ESingleConductor).c_str());
            Assert::AreEqual(L"Silent and non conducted", DucoEngineUtils::ToString(TConductorType::ESilentAndNonConducted).c_str());
            Assert::AreEqual(L"Jointly conducted", DucoEngineUtils::ToString(TConductorType::EJointlyConducted).c_str());
            Assert::AreEqual(L"", DucoEngineUtils::ToString((TConductorType)55).c_str());
        }

        TEST_METHOD(DucoEngineUtils_RingingWorldIssue)
        {
            Assert::AreEqual(L"", DucoEngineUtils::RingingWorldIssue(L"").c_str());
            Assert::AreEqual(L"1234", DucoEngineUtils::RingingWorldIssue(L"1234.5678").c_str());
            Assert::AreEqual(L"", DucoEngineUtils::RingingWorldIssue(L"000.000").c_str());
            Assert::AreEqual(L"123", DucoEngineUtils::RingingWorldIssue(L"0123.5678").c_str());
            Assert::AreEqual(L"", DucoEngineUtils::RingingWorldIssue(L".5678").c_str());
        }

        TEST_METHOD(DucoEngineUtils_RingingWorldPage)
        {
            Assert::AreEqual(L"", DucoEngineUtils::RingingWorldPage(L"").c_str());
            Assert::AreEqual(L"", DucoEngineUtils::RingingWorldPage(L"000.000").c_str());
            Assert::AreEqual(L"5678", DucoEngineUtils::RingingWorldPage(L"1234.5678").c_str());
            Assert::AreEqual(L"678", DucoEngineUtils::RingingWorldPage(L"0123.0678").c_str());
            Assert::AreEqual(L"5678", DucoEngineUtils::RingingWorldPage(L".5678").c_str());
        }

        TEST_METHOD(DucoEngineUtils_RingingWorldReferenceCompare)
        {
            Assert::IsFalse(DucoEngineUtils::MatchingRingingWorldReference(L"", L""));
            Assert::IsTrue(DucoEngineUtils::MatchingRingingWorldReference(L"1234", L"1234"));
            Assert::IsTrue(DucoEngineUtils::MatchingRingingWorldReference(L"0123", L"123"));
            Assert::IsTrue(DucoEngineUtils::MatchingRingingWorldReference(L"1234", L"1234.5678"));
            Assert::IsTrue(DucoEngineUtils::MatchingRingingWorldReference(L"123.0456", L"123.456"));

            Assert::IsFalse(DucoEngineUtils::MatchingRingingWorldReference(L"8765.4321", L"1234.5678"));
        }
    };
}
