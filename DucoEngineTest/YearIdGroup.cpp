#include "CppUnitTest.h"
#include <Peal.h>
#include <PealDatabase.h>
#include <RingingDatabase.h>
#include <StatisticFilters.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(YearIdGroup_UnitTests)
    {
    public:
        TEST_METHOD(YearIdGroup_UniqueYearlyCounts_SinglePealNoIds)
        {
            RingingDatabase database;
            Peal firstPeal;
            firstPeal.SetRingerId(1, 1, false);
            firstPeal.SetRingerId(2, 2, false);
            firstPeal.SetRingerId(3, 3, false);
            firstPeal.SetRingerId(4, 4, false);
            firstPeal.SetRingerId(5, 5, false);
            firstPeal.SetRingerId(6, 6, false);
            PerformanceDate pealDate (2000, 10, 1);
            firstPeal.SetDate(pealDate);
            database.PealsDatabase().AddObject(firstPeal);
            Duco::StatisticFilters filters(database);

            std::map<unsigned int, size_t> theTowerCounts;
            std::map<unsigned int, size_t> theRingerCounts;
            std::map<unsigned int, size_t> theMethodCounts;

            database.PealsDatabase().GetUniqueCounts(filters, theTowerCounts, theRingerCounts, theMethodCounts);

            Assert::AreEqual((size_t)1, theTowerCounts.size());
            Assert::AreEqual((size_t)1, theTowerCounts[2000]);
            Assert::AreEqual((size_t)1, theRingerCounts.size());
            Assert::AreEqual((size_t)6, theRingerCounts[2000]);
            Assert::AreEqual((size_t)1, theMethodCounts.size());
            Assert::AreEqual((size_t)1, theMethodCounts[2000]);
        }

        TEST_METHOD(YearIdGroup_UniqueYearlyCounts_TwoIdenticalPealsSameYear)
        {
            RingingDatabase database;
            Peal firstPeal;
            PerformanceDate pealDate(2000, 10, 1);
            firstPeal.SetDate(pealDate);
            firstPeal.SetRingerId(1, 1, false);
            firstPeal.SetRingerId(2, 2, false);
            firstPeal.SetRingerId(3, 3, false);
            firstPeal.SetRingerId(4, 4, false);
            firstPeal.SetRingerId(5, 5, false);
            firstPeal.SetRingerId(6, 6, false);
            database.PealsDatabase().AddObject(firstPeal);
            database.PealsDatabase().AddObject(firstPeal);
            Duco::StatisticFilters filters(database);

            std::map<unsigned int, size_t> theTowerCounts;
            std::map<unsigned int, size_t> theRingerCounts;
            std::map<unsigned int, size_t> theMethodCounts;

            database.PealsDatabase().GetUniqueCounts(filters, theTowerCounts, theRingerCounts, theMethodCounts);

            Assert::AreEqual((size_t)1, theTowerCounts.size());
            Assert::AreEqual((size_t)1, theTowerCounts[2000]);
            Assert::AreEqual((size_t)1, theRingerCounts.size());
            Assert::AreEqual((size_t)6, theRingerCounts[2000]);
            Assert::AreEqual((size_t)1, theMethodCounts.size());
            Assert::AreEqual((size_t)1, theMethodCounts[2000]);
        }
        TEST_METHOD(YearIdGroup_UniqueYearlyCounts_TwoIdenticalPealsTwoYears)
        {
            RingingDatabase database;
            Peal firstPeal;
            PerformanceDate pealDate(2000, 10, 1);
            firstPeal.SetDate(pealDate);
            firstPeal.SetRingerId(1, 1, false);
            firstPeal.SetRingerId(2, 2, false);
            firstPeal.SetRingerId(3, 3, false);
            firstPeal.SetRingerId(4, 4, false);
            firstPeal.SetRingerId(5, 5, false);
            firstPeal.SetRingerId(6, 6, false);
            database.PealsDatabase().AddObject(firstPeal);
            firstPeal.SetDate(PerformanceDate(2001, 10, 1));
            database.PealsDatabase().AddObject(firstPeal);
            Duco::StatisticFilters filters(database);

            std::map<unsigned int, size_t> theTowerCounts;
            std::map<unsigned int, size_t> theRingerCounts;
            std::map<unsigned int, size_t> theMethodCounts;

            database.PealsDatabase().GetUniqueCounts(filters, theTowerCounts, theRingerCounts, theMethodCounts);

            Assert::AreEqual((size_t)2, theTowerCounts.size());
            Assert::AreEqual((size_t)1, theTowerCounts[2000]);
            Assert::AreEqual((size_t)1, theTowerCounts[2001]);
            Assert::AreEqual((size_t)2, theRingerCounts.size());
            Assert::AreEqual((size_t)6, theRingerCounts[2000]);
            Assert::AreEqual((size_t)6, theRingerCounts[2001]);
            Assert::AreEqual((size_t)2, theMethodCounts.size());
            Assert::AreEqual((size_t)1, theMethodCounts[2000]);
            Assert::AreEqual((size_t)1, theMethodCounts[2001]);
        }

        TEST_METHOD(YearIdGroup_UniqueYearlyCounts_TwoPealsOneYear)
        {
            RingingDatabase database;
            Peal firstPeal;
            PerformanceDate pealDate(2000, 10, 1);
            firstPeal.SetDate(pealDate);
            firstPeal.SetTowerId(1);
            firstPeal.SetMethodId(4);
            firstPeal.SetRingerId(1, 1, false);
            firstPeal.SetRingerId(2, 2, false);
            firstPeal.SetRingerId(3, 3, false);
            firstPeal.SetRingerId(4, 4, false);
            firstPeal.SetRingerId(5, 5, false);
            firstPeal.SetRingerId(6, 6, false);
            database.PealsDatabase().AddObject(firstPeal);
            firstPeal.SetTowerId(2);
            firstPeal.SetMethodId(5);
            firstPeal.SetRingerId(7, 4, false);
            firstPeal.SetRingerId(8, 5, false);
            firstPeal.SetRingerId(9, 6, false);
            database.PealsDatabase().AddObject(firstPeal);
            Duco::StatisticFilters filters(database);

            std::map<unsigned int, size_t> theTowerCounts;
            std::map<unsigned int, size_t> theRingerCounts;
            std::map<unsigned int, size_t> theMethodCounts;

            database.PealsDatabase().GetUniqueCounts(filters, theTowerCounts, theRingerCounts, theMethodCounts);

            Assert::AreEqual((size_t)1, theTowerCounts.size());
            Assert::AreEqual((size_t)2, theTowerCounts[2000]);
            Assert::AreEqual((size_t)1, theRingerCounts.size());
            Assert::AreEqual((size_t)9, theRingerCounts[2000]);
            Assert::AreEqual((size_t)1, theMethodCounts.size());
            Assert::AreEqual((size_t)2, theMethodCounts[2000]);
        }
    };
}
