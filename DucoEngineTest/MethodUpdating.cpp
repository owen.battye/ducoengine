#include "CppUnitTest.h"
#include <Method.h>
#include <MethodDatabase.h>
#include <MethodNotationDatabaseUpdater.h>
#include "ToString.h"
#include <DucoEngineUtils.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(MethodUpdating_UnitTests)
    {
    private:
        Duco::MethodDatabase database;
        std::string methodDatbaseFilename = "allmeths.xml";

    public:
        MethodUpdating_UnitTests()
        {
            database.ClearObjects(true);
        }

        ~MethodUpdating_UnitTests()
        {
        }
        BEGIN_TEST_METHOD_ATTRIBUTE(FindPlaceNotation_BristolSurpriseRoyal)
            TEST_PRIORITY(2)
        END_TEST_METHOD_ATTRIBUTE()
        TEST_METHOD(FindPlaceNotation_BristolSurpriseRoyal)
        {
            std::wstring methodName = L"Bristol Surprise Royal";
            Duco::ObjectId bristolId = database.AddMethod(methodName);
            Assert::AreEqual(L"BristolSurpriseRoyal", DucoEngineUtils::MethodComparisionCharacters(methodName).c_str());
            std::wstring lowerCaseMethodName;
            DucoEngineUtils::ToLowerCase(DucoEngineUtils::MethodComparisionCharacters(methodName), lowerCaseMethodName);
            Assert::AreEqual(L"bristolsurpriseroyal", lowerCaseMethodName.c_str());

            Assert::AreEqual((size_t)1, database.NumberOfObjects());
            Assert::IsTrue(bristolId.ValidId());
            Assert::IsTrue(database.DataChanged());
            database.ResetDataChanged();
            Assert::IsFalse(database.DataChanged());

            std::map<std::wstring, Duco::ObjectId> newMethodsToUpdate;
            newMethodsToUpdate[lowerCaseMethodName] = bristolId;
            Duco::MethodNotationDatabaseUpdater methodReader (database, methodDatbaseFilename, NULL, newMethodsToUpdate);
            methodReader.ReadMethods();
            Assert::AreEqual((unsigned int)1, methodReader.NoOfMethodsUpdated());
            Assert::IsTrue(database.DataChanged());

            Assert::AreEqual((size_t)1, database.NumberOfObjects());
            const Method* bristol = database.FindMethod(bristolId, false);
            Assert::IsNotNull(bristol);
            Assert::AreEqual(L"&X50X14.50X50.36.14X70.58.16X16.70X16X10-10", bristol->PlaceNotation().c_str());
        }
        BEGIN_TEST_METHOD_ATTRIBUTE(FindPlaceNotation_Ao�tDelightMajor)
            TEST_PRIORITY(2)
        END_TEST_METHOD_ATTRIBUTE()
        TEST_METHOD(FindPlaceNotation_Ao�tDelightMajor)
        {
            std::wstring methodName = L"Ao�t Delight Major";
            Duco::ObjectId methodId = database.AddMethod(methodName);
            Assert::AreEqual(L"Ao�tDelightMajor", DucoEngineUtils::MethodComparisionCharacters(methodName).c_str());
            std::wstring lowerCaseMethodName;
            DucoEngineUtils::ToLowerCase(DucoEngineUtils::MethodComparisionCharacters(methodName), lowerCaseMethodName);
            Assert::AreEqual(L"ao�tdelightmajor", lowerCaseMethodName.c_str());

            std::map<std::wstring, Duco::ObjectId> newMethodsToUpdate;
            newMethodsToUpdate[lowerCaseMethodName] = methodId;
            Duco::MethodNotationDatabaseUpdater methodReader(database, methodDatbaseFilename, NULL, newMethodsToUpdate);
            methodReader.ReadMethods();
            Assert::AreEqual((unsigned int)1, methodReader.NoOfMethodsUpdated());

            Assert::AreEqual((size_t)1, database.NumberOfObjects());
            const Method* theMethod = database.FindMethod(methodId, false);
            Assert::IsNotNull(theMethod);
            Assert::AreEqual(L"&X58X14.58X56.18X14X38.16X34.58-12", theMethod->PlaceNotation().c_str());
        }

        BEGIN_TEST_METHOD_ATTRIBUTE(FindPlaceNotation_LondonNo3SurpriseRoyal)
            TEST_PRIORITY(2)
        END_TEST_METHOD_ATTRIBUTE()
        TEST_METHOD(FindPlaceNotation_LondonNo3SurpriseRoyal)
        {
            std::wstring methodName = L"London No 3 Surprise Royal";
            Duco::ObjectId methodId = database.AddMethod(methodName);
            std::wstring lowerCaseMethodName;
            DucoEngineUtils::ToLowerCase(DucoEngineUtils::MethodComparisionCharacters(methodName), lowerCaseMethodName);

            std::map<std::wstring, Duco::ObjectId> newMethodsToUpdate;
            newMethodsToUpdate[lowerCaseMethodName] = methodId;
            Duco::MethodNotationDatabaseUpdater methodReader(database, methodDatbaseFilename, NULL, newMethodsToUpdate);
            methodReader.ReadMethods();
            Assert::AreEqual((unsigned int)1, methodReader.NoOfMethodsUpdated());

            Assert::AreEqual((size_t)1, database.NumberOfObjects());
            const Method* theMethod = database.FindMethod(methodId, false);
            Assert::IsNotNull(theMethod);
            Assert::AreNotEqual(L"", theMethod->PlaceNotation().c_str());
            Assert::AreEqual(L"&30X30.14X12X30.14X14.50.16X16.70.14.58.14.90-12", theMethod->PlaceNotation().c_str());
        }

        BEGIN_TEST_METHOD_ATTRIBUTE(FindPlaceNotation_LondonNoDot3SurpriseRoyal)
            TEST_PRIORITY(2)
        END_TEST_METHOD_ATTRIBUTE()
        TEST_METHOD(FindPlaceNotation_LondonNoDot3SurpriseRoyal)
        {
            std::wstring methodName = L"London No.3 Surprise Royal";
            Duco::ObjectId methodId = database.AddMethod(methodName);
            std::wstring lowerCaseMethodName;
            DucoEngineUtils::ToLowerCase(DucoEngineUtils::MethodComparisionCharacters(methodName), lowerCaseMethodName);

            std::map<std::wstring, Duco::ObjectId> newMethodsToUpdate;
            newMethodsToUpdate[lowerCaseMethodName] = methodId;
            Duco::MethodNotationDatabaseUpdater methodReader(database, methodDatbaseFilename, NULL, newMethodsToUpdate);
            methodReader.ReadMethods();
            Assert::AreEqual((unsigned int)1, methodReader.NoOfMethodsUpdated());

            Assert::AreEqual((size_t)1, database.NumberOfObjects());
            const Method* theMethod = database.FindMethod(methodId, false);
            Assert::IsNotNull(theMethod);
            Assert::AreNotEqual(L"", theMethod->PlaceNotation().c_str());
            Assert::AreEqual(L"&30X30.14X12X30.14X14.50.16X16.70.14.58.14.90-12", theMethod->PlaceNotation().c_str());
        }

        BEGIN_TEST_METHOD_ATTRIBUTE(FindPlaceNotation_LondonNoDot3SurpriseRoyal_WithBrackets)
            TEST_PRIORITY(2)
        END_TEST_METHOD_ATTRIBUTE()
        TEST_METHOD(FindPlaceNotation_LondonNoDot3SurpriseRoyal_WithBrackets)
        {
            std::wstring methodName = L"London (No.3) Surprise Royal";
            Duco::ObjectId methodId = database.AddMethod(methodName);
            std::wstring lowerCaseMethodName;
            DucoEngineUtils::ToLowerCase(DucoEngineUtils::MethodComparisionCharacters(methodName), lowerCaseMethodName);

            std::map<std::wstring, Duco::ObjectId> newMethodsToUpdate;
            newMethodsToUpdate[lowerCaseMethodName] = methodId;
            Duco::MethodNotationDatabaseUpdater methodReader(database, methodDatbaseFilename, NULL, newMethodsToUpdate);
            methodReader.ReadMethods();
            Assert::AreEqual((unsigned int)1, methodReader.NoOfMethodsUpdated());

            Assert::AreEqual((size_t)1, database.NumberOfObjects());
            const Method* theMethod = database.FindMethod(methodId, false);
            Assert::IsNotNull(theMethod);
            Assert::AreNotEqual(L"", theMethod->PlaceNotation().c_str());
            Assert::AreEqual(L"&30X30.14X12X30.14X14.50.16X16.70.14.58.14.90-12", theMethod->PlaceNotation().c_str());
        }

        BEGIN_TEST_METHOD_ATTRIBUTE(FindPlaceNotation_StClementsCollegeBobMajor)
            TEST_PRIORITY(2)
        END_TEST_METHOD_ATTRIBUTE()
        TEST_METHOD(FindPlaceNotation_StClementsCollegeBobMajor)
        {
            std::wstring methodName = L"St Clements College Bob Major";
            Duco::ObjectId methodId = database.AddMethod(methodName);
            std::wstring lowerCaseMethodName;
            DucoEngineUtils::ToLowerCase(DucoEngineUtils::MethodComparisionCharacters(methodName), lowerCaseMethodName);

            std::map<std::wstring, Duco::ObjectId> newMethodsToUpdate;
            newMethodsToUpdate[lowerCaseMethodName] = methodId;
            Duco::MethodNotationDatabaseUpdater methodReader(database, methodDatbaseFilename, NULL, newMethodsToUpdate);
            methodReader.ReadMethods();
            Assert::AreEqual((unsigned int)1, methodReader.NoOfMethodsUpdated());

            Assert::AreEqual((size_t)1, database.NumberOfObjects());
            const Method* theMethod = database.FindMethod(methodId, false);
            Assert::IsNotNull(theMethod);
            Assert::AreNotEqual(L"", theMethod->PlaceNotation().c_str());
            Assert::AreEqual(L"&X18X38X38X38-12", theMethod->PlaceNotation().c_str());
        }
    };
}
