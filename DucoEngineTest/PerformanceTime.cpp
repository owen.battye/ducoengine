#include "CppUnitTest.h"
#include <PerformanceTime.h>
#include <DucoEngineUtils.h>
#include <ctime>
#include "ToString.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(PerformanceTime_UnitTests)
    {
    public:
        TEST_METHOD(PerformanceTime_DefaultConstructor)
        {
            PerformanceTime currentDate;
            Assert::IsFalse(currentDate.Valid());

            Assert::AreEqual(PerformanceTime(0), currentDate);
        }

        TEST_METHOD(PerformanceTime_3Hours)
        {
            PerformanceTime currentDate(180);

            Assert::IsTrue(currentDate.Valid());
            Assert::AreEqual(PerformanceTime(180), currentDate);

            Assert::AreEqual(L"3h 00m", currentDate.PrintPealTime(false).c_str());
            Assert::AreEqual(L"3hours", currentDate.PrintPealTime(true).c_str());
            Assert::AreEqual(L"3:00", currentDate.PrintShortPealTime().c_str());

            Assert::IsTrue(currentDate.Valid());
        }

        TEST_METHOD(PerformanceTime_EmptyPealTime)
        {
            PerformanceTime currentTime(0);

            Assert::IsFalse(currentTime.Valid());
            Assert::AreEqual(PerformanceTime(0), currentTime);

            Assert::AreEqual(L"0:00", currentTime.PrintPealTime(false).c_str());
            Assert::AreEqual(L"0:00", currentTime.PrintPealTime(true).c_str());
            Assert::AreEqual(L"0:00", currentTime.PrintShortPealTime().c_str());
            Assert::IsFalse(currentTime.Valid());
        }

        TEST_METHOD(PerformanceTime_LongestPealEverRung)
        {
            PerformanceTime currentDate(1078);
            Assert::AreEqual(PerformanceTime(1078), currentDate);

            Assert::AreEqual(L"17h 58m", currentDate.PrintPealTime(false).c_str());
            Assert::AreEqual(L"17hours 58minutes", currentDate.PrintPealTime(true).c_str());
            Assert::AreEqual(L"17:58", currentDate.PrintShortPealTime().c_str());

            Assert::IsTrue(currentDate.Valid());
        }

        TEST_METHOD(PerformanceTime_OverADay)
        {
            PerformanceTime currentTime(1442);
            Assert::AreEqual(PerformanceTime(1442), currentTime);

            Assert::AreEqual(L"1d 0h 02m", currentTime.PrintPealTime(false).c_str());
            Assert::AreEqual(L"1day 02minutes", currentTime.PrintPealTime(true).c_str());
            Assert::AreEqual(L"1:00:02", currentTime.PrintShortPealTime().c_str());

            Assert::IsTrue(currentTime.Valid());
        }

        TEST_METHOD(PerformanceTime_OverTwoDays)
        {
            PerformanceTime currentTime(2945);
            Assert::AreEqual(PerformanceTime(2945), currentTime);

            Assert::AreEqual(L"2d 1h 05m", currentTime.PrintPealTime(false).c_str());
            Assert::AreEqual(L"2days 1hour 05minutes", currentTime.PrintPealTime(true).c_str());
            Assert::AreEqual(L"2:01:05", currentTime.PrintShortPealTime().c_str());
            Assert::IsTrue(currentTime.Valid());
        }

        TEST_METHOD(PerformanceTime_PealTimesMatch)
        {
            PerformanceTime currentDate(153);
            PerformanceTime anotherDate(153);

            Assert::AreEqual(currentDate, anotherDate);

            PerformanceTime anotherDateShouldntMatch(154);
            Assert::AreNotEqual(currentDate, anotherDateShouldntMatch);

            Assert::AreEqual(L"2h 33m", currentDate.PrintPealTime(false).c_str());
            Assert::AreEqual(L"2hours 33minutes", currentDate.PrintPealTime(true).c_str());
            Assert::AreEqual(L"2:33", currentDate.PrintShortPealTime().c_str());

            Assert::IsTrue(currentDate.PealTimeClose(anotherDate));
            Assert::IsTrue(anotherDate.PealTimeClose(currentDate));
            Assert::IsTrue(currentDate.PealTimeClose(anotherDateShouldntMatch));
        }

        TEST_METHOD(PerformanceTime_PealTimeCloseMatch)
        {
            PerformanceTime dateOne(L"1h 53");
            PerformanceTime dateTwo(L"2h 53m");
            PerformanceTime dateThree(L"2:43");
            PerformanceTime dateFour(L"10h45m");
            
            Assert::IsTrue(dateOne.PealTimeClose(dateTwo));
            Assert::IsTrue(dateTwo.PealTimeClose(dateThree));
            Assert::IsFalse(dateOne.PealTimeClose(dateThree));
            Assert::IsFalse(dateOne.PealTimeClose(dateFour));
        }

        TEST_METHOD(PerformanceTime_StreamTests_Printing)
        {
            std::wostringstream streamTwo;
            PerformanceTime dateTwo(180);
            dateTwo.PrintTime(streamTwo);
            Assert::AreEqual(L"3h 00m", streamTwo.str().c_str());
        }

        TEST_METHOD(PerformanceTime_ChangesPerMinute)
        {
            PerformanceTime dateTwo(180);
            Assert::AreEqual(27.77f, dateTwo.ChangesPerMinute(5000), 0.01f);
        }

        TEST_METHOD(PerformanceTime_CompareOperators)
        {
            PerformanceTime timeOne(181);
            PerformanceTime timeTwo(180);
            Assert::IsTrue(timeOne != timeTwo);
            Assert::IsFalse(timeOne == timeTwo);
            Assert::IsTrue(timeOne >= timeTwo);
            Assert::IsFalse(timeOne <= timeTwo);
            Assert::IsTrue(timeOne > timeTwo);
            Assert::IsFalse(timeOne < timeTwo);
        }

        TEST_METHOD(PerformanceTime_CompareOperators_SameTime)
        {
            PerformanceTime timeOne(180);
            PerformanceTime timeTwo(180);
            Assert::IsFalse(timeOne != timeTwo);
            Assert::IsTrue(timeOne == timeTwo);
            Assert::IsTrue(timeOne >= timeTwo);
            Assert::IsTrue(timeOne <= timeTwo);
            Assert::IsFalse(timeOne > timeTwo);
            Assert::IsFalse(timeOne < timeTwo);
        }

        TEST_METHOD(PerformanceTime_OperatorMinus)
        {
            PerformanceTime timeOne (185);
            PerformanceTime timeTwo (180);
            Assert::AreEqual(5u, timeOne - timeTwo);
        }

        TEST_METHOD(PerformanceTime_1Year25Days3Hours0Mins)
        {
            PerformanceTime performanceTime(561780);

            Assert::IsTrue(performanceTime.Valid());

            Assert::AreEqual(L"1y 25d 3h 00m", performanceTime.PrintPealTime(false).c_str());
            Assert::AreEqual(L"1year 25days 3hours", performanceTime.PrintPealTime(true).c_str());
            Assert::AreEqual(L"1:25:3:00", performanceTime.PrintShortPealTime().c_str());
        }

        TEST_METHOD(PerformanceTime_400Years0Days11Hours14mins)
        {
            PerformanceTime performanceTime(210240674);

            Assert::IsTrue(performanceTime.Valid());
            Assert::AreEqual(L"400y 11h 14m", performanceTime.PrintPealTime(false).c_str());
            Assert::AreEqual(L"400years 11hours 14minutes", performanceTime.PrintPealTime(true).c_str());
            Assert::AreEqual(L"400:0:11:14", performanceTime.PrintShortPealTime().c_str());
        }

        TEST_METHOD(PerformanceTime_AssociationDateFormat)
        {
            PerformanceTime pealTime(164);
            Assert::AreEqual(L"2hrs 44mins", pealTime.TimeForAssociationReports().c_str());
        }
        TEST_METHOD(PerformanceTime_AssociationDateFormat2)
        {
            PerformanceTime pealTime(177);
            Assert::AreEqual(L"2hrs 57mins", pealTime.TimeForAssociationReports().c_str());
        }
        TEST_METHOD(PerformanceTime_AssociationDateFormat3)
        {
            PerformanceTime pealTime(125);
            Assert::AreEqual(L"2hrs 5mins", pealTime.TimeForAssociationReports().c_str());
        }
        TEST_METHOD(PerformanceTime_AssociationDateFormat4)
        {
            PerformanceTime pealTime(120);
            Assert::AreEqual(L"2hrs", pealTime.TimeForAssociationReports().c_str());
        }

    };
}
