﻿#include "CppUnitTest.h"
#include <AssociationDatabase.h>
#include <DatabaseSettings.h>
#include <Ring.h>
#include <Peal.h>
#include <PealDatabase.h>
#include <Picture.h>
#include <PictureDatabase.h>
#include <Method.h>
#include <MethodDatabase.h>
#include <Tower.h>
#include <TowerDatabase.h>
#include <RingingDatabase.h>
#include <RingerDatabase.h>
#include "ToString.h"
#include <ImportExportProgressCallback.h>
#include <iostream>
#include <fstream>
#include <filesystem>
#include <StatisticFilters.h>
#include <InvalidDatabaseVersionException.h>
#include <ProgressCallback.h>
#include <DatabaseSearch.h>
#include <SearchDuplicateObject.h>
#include <MethodSeriesDatabase.h>
#include <CompositionDatabase.h>
#include <Association.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(RingingDatabase_UnitTests), ImportExportProgressCallback, Duco::ProgressCallback
    {
    public:
        std::wstring mapBoxStaticKey = L"pk.eyJ1IjoiYmVsbHJpbmdlciIsImEiOiJjbDY0MXFreTMwdXM0M2txazR6emZ1NWFkIn0.zMVsJCK0GZGPcjb-kH0YvA";

        //From ImportExportProgressCallback
        void InitialisingImportExport(bool internalising, unsigned int versionNumber, size_t totalNumberOfObjects)
        {

        }
        void ObjectProcessed(bool internalising, Duco::TObjectType objectType, Duco::ObjectId objectId, int totalPercentage, size_t numberInThisSubDatabase)
        {

        }
        void ImportExportComplete(bool internalising)
        {

        }
        void ImportExportFailed(bool internalising, int errorCode)
        {
            Assert::IsFalse(true);
        }
        //ProgressCallback
        void Initialised()
        {

        }
        void Step(int progressPercent)
        {

        }
        void Complete(bool error)
        {

        }

        TEST_METHOD(RingingDatabase_ImportOldDatabase_Version12)
        {
            const std::string sampleDatabasename = "Ashley Wilson_DBVer_12.duc";
            RingingDatabase database;
            Duco::StatisticFilters filters(database);

            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)12, databaseVersionNumber);

            Assert::AreEqual((size_t)169, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)5, database.NumberOfAssociations());
            Assert::AreEqual((size_t)56, database.NumberOfTowers());
            Assert::AreEqual((size_t)176, database.NumberOfRingers());
            Assert::AreEqual((size_t)71, database.NumberOfMethods());
            Assert::AreEqual((size_t)0, database.NumberOfMethodSeries());
            Assert::AreEqual((size_t)0, database.NumberOfPictures());
            Assert::AreEqual((size_t)0, database.NumberOfCompositions());

            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EPeal));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EAssociationOrSociety));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EComposition));
            Assert::AreEqual((size_t)2, database.NumberOfObjectsWithErrors(false, TObjectType::EMethod));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EMethodSeries));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EPicture));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::ETower));
        }

        TEST_METHOD(RingingDatabase_ImportOldDatabase_Version13)
        {
            const std::string sampleDatabasename = "Lundy_DBVer_13.duc";
            RingingDatabase database;
            Duco::StatisticFilters filters(database);

            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)13, databaseVersionNumber);

            Assert::AreEqual((size_t)160, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)24, database.NumberOfAssociations());
            Assert::AreEqual((size_t)1, database.NumberOfTowers());
            Assert::AreEqual((size_t)449, database.NumberOfRingers());
            Assert::AreEqual((size_t)63, database.NumberOfMethods());
            Assert::AreEqual((size_t)2, database.NumberOfMethodSeries());
            Assert::AreEqual((size_t)0, database.NumberOfPictures());
            Assert::AreEqual((size_t)0, database.NumberOfCompositions());

            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EPeal));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EAssociationOrSociety));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EComposition));
            Assert::AreEqual((size_t)2, database.NumberOfObjectsWithErrors(false, TObjectType::EMethod));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EMethodSeries));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EPicture));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::ETower));
        }

        TEST_METHOD(RingingDatabase_ImportOldDatabase_Version21)
        {
            const std::string sampleDatabasename = "Lundy_DBVer_21.duc";
            RingingDatabase database;
            Duco::StatisticFilters filters(database);

            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)21, databaseVersionNumber);

            Assert::AreEqual((size_t)173, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)25, database.NumberOfAssociations());
            Assert::AreEqual((size_t)1, database.NumberOfTowers());
            Assert::AreEqual((size_t)475, database.NumberOfRingers());
            Assert::AreEqual((size_t)82, database.NumberOfMethods());
            Assert::AreEqual((size_t)12, database.NumberOfMethodSeries());
            Assert::AreEqual((size_t)0, database.NumberOfPictures());
            Assert::AreEqual((size_t)0, database.NumberOfCompositions());

            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EPeal));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EAssociationOrSociety));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EComposition));
            Assert::AreEqual((size_t)2, database.NumberOfObjectsWithErrors(false, TObjectType::EMethod));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EMethodSeries));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EPicture));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::ETower));
        }

        TEST_METHOD(RingingDatabase_ImportOldDatabase_Version26)
        {
            const std::string sampleDatabasename = "dordrecht_DBVer_26.duc";
            RingingDatabase database;
            Duco::StatisticFilters filters(database);

            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)26, databaseVersionNumber);

            Assert::AreEqual((size_t)71, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)10, database.NumberOfAssociations());
            Assert::AreEqual((size_t)1, database.NumberOfTowers());
            Assert::AreEqual((size_t)127, database.NumberOfRingers());
            Assert::AreEqual((size_t)59, database.NumberOfMethods());
            Assert::AreEqual((size_t)9, database.NumberOfMethodSeries());
            Assert::AreEqual((size_t)0, database.NumberOfPictures());
            Assert::AreEqual((size_t)0, database.NumberOfCompositions());

            Assert::AreEqual((size_t)1, database.NumberOfObjectsWithErrors(false, TObjectType::EPeal));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EAssociationOrSociety));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EComposition));
            Assert::AreEqual((size_t)4, database.NumberOfObjectsWithErrors(false, TObjectType::EMethod));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EMethodSeries));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EPicture));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::ETower));
        }

        TEST_METHOD(RingingDatabase_ImportOldDatabase_Version28)
        {
            const std::string sampleDatabasename = "shipping_forecast_DBVer_28.duc";
            RingingDatabase database;
            Duco::StatisticFilters filters(database);

            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)28, databaseVersionNumber);

            Assert::AreEqual((size_t)55, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)4, database.NumberOfAssociations());
            Assert::AreEqual((size_t)8, database.NumberOfTowers());
            Assert::AreEqual((size_t)64, database.NumberOfRingers());
            Assert::AreEqual((size_t)31, database.NumberOfMethods());
            Assert::AreEqual((size_t)1, database.NumberOfMethodSeries());
            Assert::AreEqual((size_t)0, database.NumberOfPictures());
            Assert::AreEqual((size_t)0, database.NumberOfCompositions());

            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EPeal));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EAssociationOrSociety));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EComposition));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EMethod));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EMethodSeries));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EPicture));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::ETower));
        }

        BEGIN_TEST_METHOD_ATTRIBUTE(RingingDatabase_ImportOldDatabase_Version32)
            TEST_PRIORITY(2)
        END_TEST_METHOD_ATTRIBUTE()
        TEST_METHOD(RingingDatabase_ImportOldDatabase_Version32)
        {
            const std::string sampleDatabasename = "john_m_thurman_DBVer_32.duc";
            RingingDatabase database;
            Duco::StatisticFilters filters(database);

            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)32, databaseVersionNumber);

            Assert::AreEqual((size_t)676, database.PerformanceInfo(filters).TotalPeals());
            filters.SetIncludeWithdrawn(true);
            Assert::AreEqual((size_t)678, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)32, database.NumberOfAssociations());
            Assert::AreEqual((size_t)297, database.NumberOfTowers());
            Assert::AreEqual((size_t)706, database.NumberOfRingers());
            Assert::AreEqual((size_t)230, database.NumberOfMethods());
            Assert::AreEqual((size_t)15, database.NumberOfMethodSeries());
            Assert::AreEqual((size_t)0, database.NumberOfPictures());
            Assert::AreEqual((size_t)0, database.NumberOfCompositions());

            Assert::AreEqual((size_t)11, database.NumberOfObjectsWithErrors(false, TObjectType::EPeal));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EAssociationOrSociety));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EComposition));
            Assert::AreEqual((size_t)2, database.NumberOfObjectsWithErrors(false, TObjectType::EMethod));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EMethodSeries));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EPicture));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::ETower));
        }

        BEGIN_TEST_METHOD_ATTRIBUTE(RingingDatabase_ImportOldDatabase_Version33)
            TEST_PRIORITY(2)
        END_TEST_METHOD_ATTRIBUTE()
        TEST_METHOD(RingingDatabase_ImportOldDatabase_Version33)
    {
            const std::string sampleDatabasename = "lundy_DBVer_33.duc";
            RingingDatabase database;
            Duco::StatisticFilters filters(database);

            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)33, databaseVersionNumber);

            Assert::AreEqual((size_t)250, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)27, database.NumberOfAssociations());
            Assert::AreEqual((size_t)1, database.NumberOfTowers());
            Assert::AreEqual((size_t)593, database.NumberOfRingers());
            Assert::AreEqual((size_t)103, database.NumberOfMethods());
            Assert::AreEqual((size_t)16, database.NumberOfMethodSeries());
            Assert::AreEqual((size_t)1, database.NumberOfPictures());
            Assert::AreEqual((size_t)0, database.NumberOfCompositions());

            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EPeal));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EAssociationOrSociety));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EComposition));
            Assert::AreEqual((size_t)2, database.NumberOfObjectsWithErrors(false, TObjectType::EMethod));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EMethodSeries));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EPicture));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::ETower));
        }

        TEST_METHOD(RingingDatabase_ImportOldDatabase_Version35)
        {
            const std::string sampleDatabasename = "Liverpool_cathedral_DBVer_35.duc";
            RingingDatabase database;
            Duco::StatisticFilters filters(database);

            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)35, databaseVersionNumber);

            Assert::AreEqual((size_t)48, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)13, database.NumberOfAssociations());
            Assert::AreEqual((size_t)1, database.NumberOfTowers());
            Assert::AreEqual((size_t)345, database.NumberOfRingers());
            Assert::AreEqual((size_t)22, database.NumberOfMethods());
            Assert::AreEqual((size_t)2, database.NumberOfMethodSeries());
            Assert::AreEqual((size_t)0, database.NumberOfPictures());
            Assert::AreEqual((size_t)0, database.NumberOfCompositions());

            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EPeal));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EAssociationOrSociety));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EComposition));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EMethod));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EMethodSeries));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EPicture));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::ETower));

            Assert::AreEqual(L"https://www.pealbase.co.uk/pealbase/guild.php?gid=", database.Settings().PealbaseAssociationURL().c_str());
            Assert::IsTrue(database.Settings().SettingsValid());
        }

        BEGIN_TEST_METHOD_ATTRIBUTE(RingingDatabase_ImportOldDatabase_Version36)
            TEST_PRIORITY(2)
        END_TEST_METHOD_ATTRIBUTE()
        TEST_METHOD(RingingDatabase_ImportOldDatabase_Version36)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            RingingDatabase database;
            Duco::StatisticFilters filters(database);

            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)36, databaseVersionNumber);

            Assert::AreEqual((size_t)600, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)16, database.NumberOfAssociations());
            Assert::AreEqual((size_t)319, database.NumberOfTowers());
            Assert::AreEqual((size_t)1, database.NumberOfRemovedTowers());
            Assert::AreEqual((size_t)351, database.NumberOfRingers());
            Assert::AreEqual((size_t)361, database.NumberOfMethods());
            Assert::AreEqual((size_t)62, database.NumberOfMethodSeries());
            Assert::AreEqual((size_t)0, database.NumberOfPictures());
            Assert::AreEqual((size_t)18, database.NumberOfCompositions());

            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EPeal));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EAssociationOrSociety));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EComposition));
            Assert::AreEqual((size_t)4, database.NumberOfObjectsWithErrors(false, TObjectType::EMethod));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EMethodSeries));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EPicture));
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::ETower));

            Assert::AreEqual(L"https://www.pealbase.co.uk/pealbase/guild.php?gid=", database.Settings().PealbaseAssociationURL().c_str());
            Assert::IsTrue(database.Settings().SettingsValid());
        }

        TEST_METHOD(RingingDatabase_FindPictureForTower)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Picture firstPicture;
            firstPicture.ReplacePicture("bellboard.jpg");
            Duco::ObjectId pictureId = database.PicturesDatabase().AddObject(firstPicture);
            Assert::IsTrue(pictureId.ValidId());

            Tower towerOne;
            towerOne.SetName(L"St Peter and Paul");
            towerOne.SetCity(L"Moulton");
            towerOne.SetCounty(L"Northamptonshire");
            Duco::ObjectId towerId = database.TowersDatabase().AddObject(towerOne);

            Peal onePeal;
            onePeal.SetPictureId(pictureId);
            onePeal.SetTowerId(towerId);
            database.PealsDatabase().AddObject(onePeal);

            Duco::ObjectId foundIdOne = database.FindPicture(L"moul");
            Assert::AreEqual(pictureId, foundIdOne);

            Duco::ObjectId foundIdTwo = database.FindPicture(L"peter");
            Assert::AreEqual(pictureId, foundIdTwo);
        }

        TEST_METHOD(RingingDatabase_BackupFilename)
        {
            RingingDatabase database;
            std::string filename = "owen_battye.duc";
            std::string expectedBackupFilename = "owen_battye.duc.bak";
            std::string expectedBackupFilename2 = "owen_battye.duc.ba2";
            Assert::AreEqual(expectedBackupFilename.c_str(), database.BackupFileName(filename.c_str()).c_str());
            Assert::AreEqual(expectedBackupFilename2.c_str(), database.BackupFileName(expectedBackupFilename.c_str()).c_str());
        }

        TEST_METHOD(RingingDatabase_MapBoxMap_NoData)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            size_t noOfTowersPlotted = 0;
            size_t noOfTowersNotPlotted = 0;
            Duco::StatisticFilters filters(database);
            filters.SetEuropeOnly(true);
            std::wstring pealMapUrl = database.MapBoxMap(noOfTowersPlotted, noOfTowersNotPlotted, mapBoxStaticKey, false, true, false, true, false, filters);

            Assert::AreEqual((size_t)0, noOfTowersPlotted);
        }

        TEST_METHOD(RingingDatabase_MapBoxMap_ThreePeals_TwoTowers)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Tower towerOne;
            towerOne.SetName(L"St Peter and Paul");
            towerOne.SetCity(L"Moulton");
            towerOne.SetCounty(L"Northamptonshire");
            towerOne.SetLongitude(L" -0.85278");
            towerOne.SetLatitude(L" 52.29034");
            Duco::ObjectId towerOneId = database.TowersDatabase().AddObject(towerOne);

            Peal onePeal;
            onePeal.SetTowerId(towerOneId);
            database.PealsDatabase().AddObject(onePeal);

            Tower towerTwo;
            towerTwo.SetName(L"RC Cathedral Church of St Francis Xavier");
            towerTwo.SetCity(L"Adelaide");
            towerTwo.SetCounty(L"South Australia");
            towerTwo.SetLongitude(L"138.60127258300781");
            towerTwo.SetLatitude(L"-34.928829193115234");
            Duco::ObjectId towerTwoId = database.TowersDatabase().AddObject(towerTwo);

            Peal pealTwo;
            pealTwo.SetTowerId(towerTwoId);
            database.PealsDatabase().AddObject(pealTwo);
            Peal pealThree;
            pealThree.SetTowerId(towerTwoId);
            database.PealsDatabase().AddObject(pealThree);

            size_t noOfTowersPlotted = 0;
            size_t noOfTowersNotPlotted = 0;

            Duco::StatisticFilters filters(database);
            std::wstring pealMapUrl = database.MapBoxMap(noOfTowersPlotted, noOfTowersNotPlotted, mapBoxStaticKey, false, true, true, false, false, filters);

            /*std::wofstream myfile;
            myfile.open("mapbox.html");
            myfile << pealMapUrl;
            myfile.close();*/

            Assert::AreEqual((size_t)2, noOfTowersPlotted);
            Assert::AreEqual((size_t)0, noOfTowersNotPlotted);
        }

        TEST_METHOD(RingingDatabase_MapBoxMap_TowerInUkAndHolland)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Tower towerOne;
            towerOne.SetCity(L"Manchester");
            towerOne.SetCounty(L"Manc");
            towerOne.SetLongitude(L" -2.24475");
            towerOne.SetLatitude(L" 53.48526");
            Duco::ObjectId towerOneId = database.TowersDatabase().AddObject(towerOne);

            Peal onePeal;
            onePeal.SetTowerId(towerOneId);
            database.PealsDatabase().AddObject(onePeal);

            Tower towerTwo;
            towerTwo.SetName(L"Clock haus");
            towerTwo.SetCity(L"Dordrecht");
            towerTwo.SetCounty(L"Holland");
            towerTwo.SetLongitude(L"4.65970");
            towerTwo.SetLatitude(L"51.81431");
            Duco::ObjectId towerTwoId = database.TowersDatabase().AddObject(towerTwo);

            Peal pealTwo;
            pealTwo.SetTowerId(towerTwoId);
            database.PealsDatabase().AddObject(pealTwo);
            Peal pealThree;
            pealThree.SetTowerId(towerTwoId);
            database.PealsDatabase().AddObject(pealThree);

            size_t noOfTowersPlotted = 0;
            size_t noOfTowersNotPlotted = 0;

            Duco::StatisticFilters filters(database);
            std::wstring pealMapUrl = database.MapBoxMap(noOfTowersPlotted, noOfTowersNotPlotted, mapBoxStaticKey, false, true, true, false, false, filters);

            /*std::wofstream myfile;
            myfile.open("mapbox.html");
            myfile << pealMapUrl;
            myfile.close();*/

            Assert::AreEqual((size_t)2, noOfTowersPlotted);
            Assert::AreEqual((size_t)0, noOfTowersNotPlotted);
        }

        TEST_METHOD(RingingDatabase_MapBoxMap_TowerInUkAndHollandAndChannelIslands)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Tower towerOne;
            towerOne.SetCity(L"Manchester");
            towerOne.SetCounty(L"Manc");
            towerOne.SetLongitude(L" -2.24475");
            towerOne.SetLatitude(L" 53.48526");
            Duco::ObjectId towerOneId = database.TowersDatabase().AddObject(towerOne);

            Peal onePeal;
            onePeal.SetTowerId(towerOneId);
            database.PealsDatabase().AddObject(onePeal);

            Tower towerTwo;
            towerTwo.SetName(L"Clock haus");
            towerTwo.SetCity(L"Dordrecht");
            towerTwo.SetCounty(L"Holland");
            towerTwo.SetLongitude(L"4.65970");
            towerTwo.SetLatitude(L"51.81431");
            Duco::ObjectId towerTwoId = database.TowersDatabase().AddObject(towerTwo);

            Tower towerThree;
            towerThree.SetName(L"St Peter Port");
            towerThree.SetCity(L"Guernsey");
            towerThree.SetCounty(L"Channel Islands");
            towerThree.SetLongitude(L"-2.53775");
            towerThree.SetLatitude(L"49.46400");
            Duco::ObjectId towerThreeId = database.TowersDatabase().AddObject(towerThree);

            Peal pealTwo;
            pealTwo.SetTowerId(towerTwoId);
            database.PealsDatabase().AddObject(pealTwo);
            Peal pealThree;
            pealThree.SetTowerId(towerTwoId);
            database.PealsDatabase().AddObject(pealThree);

            size_t noOfTowersPlotted = 0;
            size_t noOfTowersNotPlotted = 0;

            Duco::StatisticFilters filters(database);
            std::wstring pealMapUrl = database.MapBoxMap(noOfTowersPlotted, noOfTowersNotPlotted, mapBoxStaticKey, true, true, true, false, false, filters);

            /*std::wofstream myfile;
            myfile.open("mapbox.html");
            myfile << pealMapUrl;
            myfile.close();*/

            Assert::AreEqual((size_t)3, noOfTowersPlotted);
            Assert::AreEqual((size_t)0, noOfTowersNotPlotted);
        }


        TEST_METHOD(RingingDatabase_MapBoxMap_ThreeTowers)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Tower towerOne;
            towerOne.SetName(L"St Peter and Paul");
            towerOne.SetCity(L"Moulton");
            towerOne.SetCounty(L"Northamptonshire");
            towerOne.SetLongitude(L" -0.85278");
            towerOne.SetLatitude(L" 52.29034");
            Duco::ObjectId towerOneId = database.TowersDatabase().AddObject(towerOne);

            Tower towerTwo;
            towerTwo.SetName(L"RC Cathedral Church of St Francis Xavier");
            towerTwo.SetCity(L"Adelaide");
            towerTwo.SetCounty(L"South Australia");
            towerTwo.SetLongitude(L"138.60127258300781");
            towerTwo.SetLatitude(L"-34.928829193115234");
            Duco::ObjectId towerTwoId = database.TowersDatabase().AddObject(towerTwo);
            Peal pealTwo;
            pealTwo.SetTowerId(towerTwoId);
            database.PealsDatabase().AddObject(pealTwo);

            Tower towerThree;
            towerThree.SetName(L"St John in the Oaks");
            towerThree.SetCity(L"NEWPORT");
            towerThree.SetCounty(L"Isle Of wight");
            Duco::ObjectId towerThreeId = database.TowersDatabase().AddObject(towerThree);
            Peal pealThree;
            pealThree.SetTowerId(towerThreeId);
            database.PealsDatabase().AddObject(pealThree);

            size_t noOfTowersPlotted = 0;
            size_t noOfTowersNotPlotted = 0;
            Duco::StatisticFilters filters(database);
            filters.SetEuropeOnly(true);
            std::wstring pealMapUrl = database.MapBoxMap(noOfTowersPlotted, noOfTowersNotPlotted, mapBoxStaticKey, true, true, true, true, false, filters);

            Assert::AreEqual((size_t)1, noOfTowersPlotted);
            Assert::AreEqual((size_t)0, noOfTowersNotPlotted);

            /*std::wofstream myfile;
            myfile.open("mapbox.html");
            myfile << pealMapUrl;
            myfile.close();*/
        }

        TEST_METHOD(RingingDatabase_MapBoxMap_OwenBattye_Version36)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            RingingDatabase database;

            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)36, databaseVersionNumber);
            Assert::AreEqual(databaseVersionNumber, database.DatabaseVersion());

            size_t noOfTowersPlotted = 0;
            size_t noOfTowersNotPlotted = 0;
            Duco::StatisticFilters filters(database);
            filters.SetEuropeOnly(true);
            filters.SetStartDate(true, Duco::PerformanceDate(2019, 1, 01));
            filters.SetEndDate(true, Duco::PerformanceDate(2020, 12, 31));
            std::wstring pealMapUrl = database.MapBoxMap(noOfTowersPlotted, noOfTowersNotPlotted, mapBoxStaticKey, false, true, true, false, false, filters);

            /*std::wofstream myfile;
            myfile.open("mapbox.html");
            myfile << pealMapUrl;
            myfile.close();*/

            Assert::AreEqual((size_t)6, noOfTowersPlotted);
        }

        TEST_METHOD(RingingDatabase_OwenBattye_Version43_CheckHalifaxRings)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_43.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)43, databaseVersionNumber);
            Assert::AreEqual(databaseVersionNumber, database.DatabaseVersion());

            const Tower* const halifax = database.TowersDatabase().FindTower(78);
            Assert::AreEqual(L"HALIFAX, West Yorkshire. (Minster Church of S John Bapt)", halifax->FullName().c_str());
            Assert::AreEqual((size_t)4, halifax->NoOfRings());

            Assert::AreEqual(L"10-2-12 in G", halifax->FindRingByIndex(0)->TenorDescription().c_str());
            Assert::AreEqual(L"28-0-14 in D", halifax->FindRingByIndex(1)->TenorDescription().c_str());
            Assert::AreEqual(L"10-2-12 in F♯", halifax->FindRingByIndex(2)->TenorDescription().c_str());
            Assert::AreEqual(L"28-0-14 in D", halifax->FindRingByIndex(3)->TenorDescription().c_str());

            Assert::IsFalse(halifax->Valid(database, true, true));
            Assert::IsTrue(halifax->ErrorCode().test(ERingWarning_PossibleInvalidTenorKey));

            Assert::AreEqual(L"https://www.pealbase.co.uk/pealbase/guild.php?gid=", database.Settings().PealbaseAssociationURL().c_str());
            Assert::IsTrue(database.Settings().SettingsValid());
        }

        TEST_METHOD(RingingDatabase_MapBoxMap_OwenBattye_LiverpoolCathedralOnly)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            RingingDatabase database;

            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)36, databaseVersionNumber);
            Assert::AreEqual(databaseVersionNumber, database.DatabaseVersion());

            size_t noOfTowersPlotted = 0;
            size_t noOfTowersNotPlotted = 0;
            Duco::StatisticFilters filters(database);
            filters.SetStartDate(true, Duco::PerformanceDate(2018, 11, 16));
            filters.SetEndDate(true, Duco::PerformanceDate(2018, 11, 16));
            std::wstring pealMapUrl = database.MapBoxMap(noOfTowersPlotted, noOfTowersNotPlotted, mapBoxStaticKey, false, true, true, false, false, filters);

            /*std::wofstream myfile;
            myfile.open("mapbox.html");
            myfile << pealMapUrl;
            myfile.close();*/

            Assert::AreEqual((size_t)1, noOfTowersPlotted);
        }
        TEST_METHOD(RingingDatabase_GetTowerCircling_AshleyWilson)
        {
            const std::string sampleDatabasename = "Ashley Wilson_DBVer_12.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)12, databaseVersionNumber);

            Duco::StatisticFilters filters(database);
            filters.SetTower(true, 1);
            std::map<unsigned int, Duco::CirclingData> circlingdata;
            size_t numberOfTimesCircling;
            Duco::PealLengthInfo noOfPeals;
            Duco::PerformanceDate firstCircledDate;
            size_t daysToFirstCircle;

            database.GetTowerCircling(filters, circlingdata, noOfPeals, numberOfTimesCircling, firstCircledDate, daysToFirstCircle);

            Assert::AreEqual((size_t)0, numberOfTimesCircling);
            Assert::AreEqual((size_t)1, noOfPeals.TotalPeals());
            Assert::AreEqual((size_t)10, circlingdata.size());
            Duco::PerformanceDate expectedCircleDate;
            expectedCircleDate.ResetToEarliest();
            Assert::AreEqual(expectedCircleDate, firstCircledDate);

            Assert::AreEqual((size_t)1, circlingdata[9].PealCount());
            Assert::AreEqual(PerformanceDate(2010,3, 3), circlingdata[9].FirstPealDate());

            Assert::AreEqual((size_t)0, circlingdata[1].PealCount());
            Assert::AreEqual(expectedCircleDate, circlingdata[1].FirstPealDate());
            Assert::AreEqual((size_t)0, circlingdata[2].PealCount());
            Assert::AreEqual(expectedCircleDate, circlingdata[2].FirstPealDate());
            Assert::AreEqual((size_t)0, circlingdata[3].PealCount());
            Assert::AreEqual(expectedCircleDate, circlingdata[3].FirstPealDate());
            Assert::AreEqual((size_t)0, circlingdata[4].PealCount());
            Assert::AreEqual(expectedCircleDate, circlingdata[4].FirstPealDate());
            Assert::AreEqual((size_t)0, circlingdata[5].PealCount());
            Assert::AreEqual(expectedCircleDate, circlingdata[5].FirstPealDate());
            Assert::AreEqual((size_t)0, circlingdata[6].PealCount());
            Assert::AreEqual(expectedCircleDate, circlingdata[6].FirstPealDate());
            Assert::AreEqual((size_t)0, circlingdata[7].PealCount());
            Assert::AreEqual(expectedCircleDate, circlingdata[7].FirstPealDate());
            Assert::AreEqual((size_t)0, circlingdata[8].PealCount());
            Assert::AreEqual(expectedCircleDate, circlingdata[8].FirstPealDate());
            Assert::AreEqual((size_t)0, circlingdata[10].PealCount());
            Assert::AreEqual(expectedCircleDate, circlingdata[10].FirstPealDate());

        }

        TEST_METHOD(RingingDatabase_GetTowerCircling_OwenBattyeOnLundy_AllBells)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)36, databaseVersionNumber);

            Duco::StatisticFilters filters(database);
            Duco::ObjectId towerId(164); // Lundy
            filters.SetTower(true, towerId);
            std::map<unsigned int, Duco::CirclingData> circlingdata;
            size_t numberOfTimesCircling;
            Duco::PealLengthInfo noOfPeals;
            Duco::PerformanceDate firstCircledDate;
            size_t daysToFirstCircle;

            database.GetTowerCircling(filters, circlingdata, noOfPeals, numberOfTimesCircling, firstCircledDate, daysToFirstCircle);

            Assert::AreEqual((size_t)4, numberOfTimesCircling);
            Assert::AreEqual((size_t)53, noOfPeals.TotalPeals());
            Assert::AreEqual((size_t)10, circlingdata.size());
            Assert::AreEqual(Duco::PerformanceDate(2008,8,31), firstCircledDate);

            Assert::AreEqual((size_t)8, circlingdata[1].PealCount());
            Assert::AreEqual(PerformanceDate(2006, 5, 15), circlingdata[1].FirstPealDate());
            Assert::AreEqual((size_t)4, circlingdata[2].PealCount());
            Assert::AreEqual(PerformanceDate(2004, 10, 8), circlingdata[2].FirstPealDate());
            Assert::AreEqual((size_t)4, circlingdata[3].PealCount());
            Assert::AreEqual(PerformanceDate(2007, 9, 2), circlingdata[3].FirstPealDate());
            Assert::AreEqual((size_t)6, circlingdata[4].PealCount());
            Assert::AreEqual(PerformanceDate(2004, 10, 5), circlingdata[4].FirstPealDate());
            Assert::AreEqual((size_t)4, circlingdata[5].PealCount());
            Assert::AreEqual(PerformanceDate(2005, 8, 3), circlingdata[5].FirstPealDate());
            Assert::AreEqual((size_t)5, circlingdata[6].PealCount());
            Assert::AreEqual(PerformanceDate(2006, 5, 14), circlingdata[6].FirstPealDate());
            Assert::AreEqual((size_t)5, circlingdata[7].PealCount());
            Assert::AreEqual(PerformanceDate(2007, 9, 1), circlingdata[7].FirstPealDate());
            Assert::AreEqual((size_t)6, circlingdata[8].PealCount());
            Assert::AreEqual(PerformanceDate(2004, 10, 5), circlingdata[8].FirstPealDate());
            Assert::AreEqual((size_t)6, circlingdata[9].PealCount());
            Assert::AreEqual(PerformanceDate(2004, 10, 4), circlingdata[9].FirstPealDate());
            Assert::AreEqual((size_t)5, circlingdata[10].PealCount());
            Assert::AreEqual(PerformanceDate(2008, 8, 31), circlingdata[10].FirstPealDate());
        }

        TEST_METHOD(RingingDatabase_GetTowerCircling_OwenBattyeOnLundy_Back6)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)36, databaseVersionNumber);

            Duco::StatisticFilters filters(database);
            Duco::ObjectId towerId(164); // Lundy
            filters.SetTower(true, towerId); // Back 6
            filters.SetRing(true, 1);
            std::map<unsigned int, Duco::CirclingData> circlingdata;
            size_t numberOfTimesCircling;
            Duco::PealLengthInfo noOfPeals;
            Duco::PerformanceDate firstCircledDate;
            size_t daysToFirstCircle;

            database.GetTowerCircling(filters, circlingdata, noOfPeals, numberOfTimesCircling, firstCircledDate, daysToFirstCircle);
            Duco::PerformanceDate expectedCircleDate;
            expectedCircleDate.ResetToEarliest();

            Assert::AreEqual((size_t)0, numberOfTimesCircling);
            Assert::AreEqual((size_t)2, noOfPeals.TotalPeals());
            Assert::AreEqual((size_t)6, circlingdata.size());
            Assert::AreEqual(expectedCircleDate, firstCircledDate);

            Assert::AreEqual((size_t)0, circlingdata[1].PealCount());
            Assert::AreEqual(expectedCircleDate, circlingdata[1].FirstPealDate());
            Assert::AreEqual((size_t)0, circlingdata[2].PealCount());
            Assert::AreEqual(expectedCircleDate, circlingdata[2].FirstPealDate());
            Assert::AreEqual((size_t)1, circlingdata[3].PealCount());
            Assert::AreEqual(PerformanceDate(2017, 4, 24), circlingdata[3].FirstPealDate());
            Assert::AreEqual((size_t)0, circlingdata[4].PealCount());
            Assert::AreEqual(expectedCircleDate, circlingdata[4].FirstPealDate());
            Assert::AreEqual((size_t)0, circlingdata[5].PealCount());
            Assert::AreEqual(expectedCircleDate, circlingdata[5].FirstPealDate());
            Assert::AreEqual((size_t)1, circlingdata[6].PealCount());
            Assert::AreEqual(PerformanceDate(2012, 5, 14), circlingdata[6].FirstPealDate());
        }

        TEST_METHOD(RingingDatabase_GetTowerCircling_NoTower)
        {
            const std::string sampleDatabasename = "paul_mason_kettering.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)50, databaseVersionNumber);

            Duco::StatisticFilters filters(database);
            //filters.SetTower(true, 1);
            std::map<unsigned int, Duco::CirclingData> circlingdata;
            size_t numberOfTimesCircling;
            Duco::PealLengthInfo noOfPeals;
            Duco::PerformanceDate firstCircledDate;
            size_t daysToFirstCircle;

            database.GetTowerCircling(filters, circlingdata, noOfPeals, numberOfTimesCircling, firstCircledDate, daysToFirstCircle);

            Assert::AreEqual((size_t)18446744073709551615, numberOfTimesCircling);
            Assert::AreEqual((size_t)0, noOfPeals.TotalPeals());
            Assert::AreEqual((size_t)0, circlingdata.size());
        }

        TEST_METHOD(RingingDatabase_GetTowerCircling_PaulMason_Kettering)
        {
            const std::string sampleDatabasename = "paul_mason_kettering.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)50, databaseVersionNumber);

            Duco::StatisticFilters filters(database);
            filters.SetTower(true, 228);
            filters.SetIncludeLinkedTowers(false);
            filters.SetRinger(true, 1);
            std::map<unsigned int, Duco::CirclingData> circlingdata;
            size_t numberOfTimesCircling;
            Duco::PealLengthInfo noOfPeals;
            Duco::PerformanceDate firstCircledDate;
            size_t daysToFirstCircle;

            database.GetTowerCircling(filters, circlingdata, noOfPeals, numberOfTimesCircling, firstCircledDate, daysToFirstCircle);

            Assert::AreEqual((size_t)0, numberOfTimesCircling);
            Assert::AreEqual((size_t)2, noOfPeals.TotalPeals());
            Assert::AreEqual((size_t)5039+5040, noOfPeals.TotalChanges());
            Assert::AreEqual((size_t)12, circlingdata.size());

            Assert::AreEqual((size_t)0, circlingdata[0].PealCount()); // Bells dont start at 0, but 1, so this must retrun 0.
            Assert::AreEqual((size_t)0, circlingdata[1].PealCount());
            Assert::AreEqual((size_t)0, circlingdata[2].PealCount());
            Assert::AreEqual((size_t)0, circlingdata[3].PealCount());
            Assert::AreEqual((size_t)0, circlingdata[4].PealCount());
            Assert::AreEqual((size_t)0, circlingdata[5].PealCount());
            Assert::AreEqual((size_t)0, circlingdata[6].PealCount());
            Assert::AreEqual((size_t)0, circlingdata[7].PealCount());
            Assert::AreEqual((size_t)0, circlingdata[8].PealCount());
            Assert::AreEqual((size_t)0, circlingdata[9].PealCount());
            Assert::AreEqual((size_t)1, circlingdata[10].PealCount());
            Assert::AreEqual((size_t)1, circlingdata[11].PealCount());
            Assert::AreEqual((size_t)0, circlingdata[12].PealCount());

            Assert::AreEqual(PerformanceDate(1753, 1, 1), circlingdata[8].FirstPealDate());
            Assert::AreEqual(PerformanceDate(1996, 2, 3), circlingdata[10].FirstPealDate());
            Assert::AreEqual(PerformanceDate(1994, 5, 21), circlingdata[11].FirstPealDate());

            filters.SetTower(true, 80);
            database.GetTowerCircling(filters, circlingdata, noOfPeals, numberOfTimesCircling, firstCircledDate, daysToFirstCircle);

            Assert::AreEqual((size_t)0, numberOfTimesCircling);
            Assert::AreEqual((size_t)4, noOfPeals.TotalPeals());
            Assert::AreEqual((size_t)5040+5040+5100+5042, noOfPeals.TotalChanges());
            Assert::AreEqual((size_t)13, circlingdata.size());

            filters.SetIncludeLinkedTowers(true);
            database.GetTowerCircling(filters, circlingdata, noOfPeals, numberOfTimesCircling, firstCircledDate, daysToFirstCircle);
            Assert::AreEqual((size_t)0, numberOfTimesCircling);
            Assert::AreEqual((size_t)6, noOfPeals.TotalPeals());
            Assert::AreEqual((size_t)30301, noOfPeals.TotalChanges());
            Assert::AreEqual((size_t)13, circlingdata.size());


            filters.SetTower(true, 228);
            filters.SetIncludeLinkedTowers(true);
            database.GetTowerCircling(filters, circlingdata, noOfPeals, numberOfTimesCircling, firstCircledDate, daysToFirstCircle);
            Assert::AreEqual((size_t)0, numberOfTimesCircling);
            Assert::AreEqual((size_t)6, noOfPeals.TotalPeals());
            Assert::AreEqual((size_t)30301, noOfPeals.TotalChanges());
            Assert::AreEqual((size_t)12, circlingdata.size());
        }

        TEST_METHOD(RingingDatabase_GetMethodCircling_CambridgeSRoyal)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)36, databaseVersionNumber);

            Duco::StatisticFilters filters(database);
            std::map<unsigned int, Duco::CirclingData> circlingdata;
            size_t numberOfTimesCircling = 0;
            Duco::PerformanceDate firstCircledDate;

            Duco::ObjectId methodId = database.MethodsDatabase().FindMethod(L"Cambridge", L"Surprise", 10);
            filters.SetMethod(true, methodId);

            database.GetMethodCircling(filters, circlingdata, numberOfTimesCircling, firstCircledDate);
            Duco::PerformanceDate expectedCircleDate;
            expectedCircleDate.ResetToEarliest();

            Assert::AreEqual((size_t)0, numberOfTimesCircling);
            Assert::AreEqual((size_t)10, circlingdata.size());
            Assert::AreEqual(expectedCircleDate, firstCircledDate);

            Assert::AreEqual((size_t)1, circlingdata[1].PealCount());
            Assert::AreEqual(PerformanceDate(2018, 7, 26), circlingdata[1].FirstPealDate());
            Assert::AreEqual((size_t)1, circlingdata[2].PealCount());
            Assert::AreEqual(PerformanceDate(2009, 6, 10), circlingdata[2].FirstPealDate());
            Assert::AreEqual((size_t)0, circlingdata[3].PealCount());
            Assert::AreEqual(expectedCircleDate, circlingdata[3].FirstPealDate());
            Assert::AreEqual((size_t)0, circlingdata[4].PealCount());
            Assert::AreEqual(expectedCircleDate, circlingdata[4].FirstPealDate());
            Assert::AreEqual((size_t)1, circlingdata[5].PealCount());
            Assert::AreEqual(PerformanceDate(2004, 6, 5), circlingdata[5].FirstPealDate());
            Assert::AreEqual((size_t)5, circlingdata[6].PealCount());
            Assert::AreEqual(PerformanceDate(1997, 4, 2), circlingdata[6].FirstPealDate());
            Assert::AreEqual((size_t)4, circlingdata[7].PealCount());
            Assert::AreEqual(PerformanceDate(1993, 10, 2), circlingdata[7].FirstPealDate());
            Assert::AreEqual((size_t)4, circlingdata[8].PealCount());
            Assert::AreEqual(PerformanceDate(1995, 6, 24), circlingdata[8].FirstPealDate());
            Assert::AreEqual((size_t)3, circlingdata[9].PealCount());
            Assert::AreEqual(PerformanceDate(1995, 3, 11), circlingdata[9].FirstPealDate());
            Assert::AreEqual((size_t)0, circlingdata[10].PealCount());
            Assert::AreEqual(expectedCircleDate, circlingdata[10].FirstPealDate());
        }

        TEST_METHOD(RingingDatabase_GetMethodCircling_CambridgeSMinor)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)36, databaseVersionNumber);

            Duco::StatisticFilters filters(database);
            std::map<unsigned int, Duco::CirclingData> circlingdata;
            size_t numberOfTimesCircling = 0;
            Duco::PerformanceDate firstCircledDate;

            Duco::ObjectId methodId = database.MethodsDatabase().FindMethod(L"Cambridge", L"Surprise", 6);
            filters.SetMethod(true, methodId);

            database.GetMethodCircling(filters, circlingdata, numberOfTimesCircling, firstCircledDate);
            Duco::PerformanceDate expectedCircleDate;
            expectedCircleDate.ResetToEarliest();

            Assert::AreEqual((size_t)0, numberOfTimesCircling);
            Assert::AreEqual((size_t)6, circlingdata.size());
            Assert::AreEqual(expectedCircleDate, firstCircledDate);

            Assert::AreEqual((size_t)0, circlingdata[1].PealCount());
            Assert::AreEqual(expectedCircleDate, circlingdata[1].FirstPealDate());
            Assert::AreEqual((size_t)0, circlingdata[2].PealCount());
            Assert::AreEqual(expectedCircleDate, circlingdata[2].FirstPealDate());
            Assert::AreEqual((size_t)0, circlingdata[3].PealCount());
            Assert::AreEqual(expectedCircleDate, circlingdata[3].FirstPealDate());
            Assert::AreEqual((size_t)0, circlingdata[4].PealCount());
            Assert::AreEqual(expectedCircleDate, circlingdata[4].FirstPealDate());
            Assert::AreEqual((size_t)1, circlingdata[5].PealCount());
            Assert::AreEqual(PerformanceDate(1994, 1, 22), circlingdata[5].FirstPealDate());
            Assert::AreEqual((size_t)0, circlingdata[6].PealCount());
            Assert::AreEqual(expectedCircleDate, circlingdata[6].FirstPealDate());
        }

        TEST_METHOD(RingingDatabase_GetMethodCircling_NoMethodSet)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_36.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)36, databaseVersionNumber);

            Duco::StatisticFilters filters(database);
            std::map<unsigned int, Duco::CirclingData> circlingdata;
            size_t numberOfTimesCircling = 0;
            Duco::PerformanceDate firstCircledDate;

            database.GetMethodCircling(filters, circlingdata, numberOfTimesCircling, firstCircledDate);
            Duco::PerformanceDate expectedCircleDate;
            expectedCircleDate.ResetToEarliest();

            Assert::AreEqual((size_t)0, numberOfTimesCircling);
            Assert::AreEqual((size_t)0, circlingdata.size());
            Assert::AreEqual(expectedCircleDate, firstCircledDate);
        }

        TEST_METHOD(RingingDatabase_EmptyDatabase_UnsupportedVersion)
        {
            const char* sampleDatabasename = "emptydatabase_DBVer999.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            auto func = [&] {
                database.Internalise(sampleDatabasename, NULL, databaseVersionNumber);
            };
            Assert::ExpectException<Duco::InvalidDatabaseVersionException>(func, L"Database version not supported");

        }

        TEST_METHOD(RingingDatabase_ImportOldDatabaseVersion38_CheckTowerKeys)
        {
            const std::string sampleDatabasename = "owen_battye_DBVer_38.duc";
            RingingDatabase database;

            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)38, databaseVersionNumber);

            Assert::AreEqual((size_t)356, database.MethodsDatabase().NumberOfObjects());
            Assert::AreEqual((size_t)611, database.PealsDatabase().NumberOfObjects());
            Assert::AreEqual((size_t)223, database.TowersDatabase().NumberOfObjects());
            Assert::AreEqual((size_t)359, database.RingersDatabase().NumberOfObjects());

            const Tower* const moulton = database.TowersDatabase().FindTower(129);
            Assert::IsNotNull(moulton);
            Assert::AreEqual(L"MOULTON", moulton->City().c_str());
            Assert::AreEqual(12u, moulton->Bells());
            const Ring* const backEight = moulton->FindRing(0);
            Assert::AreEqual(L"Eight Bell Peal", backEight->Name().c_str());
            Assert::AreEqual(L"11-1-14", backEight->TenorWeight().c_str());
            Assert::AreEqual(L"A", backEight->TenorKey().c_str());
            const Ring* const backTen = moulton->FindRing(1);
            Assert::AreEqual(L"Ten Bell Peal", backTen->Name().c_str());
            Assert::AreEqual(L"11-1-14", backTen->TenorWeight().c_str());
            Assert::AreEqual(L"A", backTen->TenorKey().c_str());
            const Ring* const allTwelve = moulton->FindRing(2);
            Assert::AreEqual(L"Twelve Bell Peal", allTwelve->Name().c_str());
            Assert::AreEqual(L"11-1-14", allTwelve->TenorWeight().c_str());
            Assert::AreEqual(L"A", allTwelve->TenorKey().c_str());

            Assert::AreEqual(L"https://www.pealbase.co.uk/pealbase/guild.php?gid=", database.Settings().PealbaseAssociationURL().c_str());
            Assert::IsTrue(database.Settings().SettingsValid());
        }

        TEST_METHOD(RingingDatabase_ImportOldDatabaseVersion41_JohnThurman)
        {
            const std::string sampleDatabasename = "john_m_thurman_DBVer_41.duc";
            RingingDatabase database;
            Duco::StatisticFilters filters(database);

            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)41, databaseVersionNumber);

            Assert::AreEqual((size_t)324, database.MethodsDatabase().NumberOfObjects());
            Assert::AreEqual((size_t)942, database.PealsDatabase().NumberOfObjects());
            Assert::AreEqual((size_t)940, database.PealsDatabase().PerformanceInfo(filters).TotalPeals());
            filters.SetExcludeValid(true);
            filters.SetIncludeWithdrawn(false);
            Assert::AreEqual((size_t)0, database.PealsDatabase().PerformanceInfo(filters).TotalPeals());
            filters.SetIncludeWithdrawn(true);
            Assert::AreEqual((size_t)2, database.PealsDatabase().PerformanceInfo(filters).TotalPeals());
            size_t linkedTowers = 0;
            Assert::AreEqual((size_t)360, database.TowersDatabase().NumberOfActiveTowers(linkedTowers));
            Assert::AreEqual((size_t)2, linkedTowers);
            Assert::AreEqual((size_t)362, database.NumberOfTowers());
            Assert::AreEqual((size_t)810, database.RingersDatabase().NumberOfObjects());
            Assert::AreEqual((size_t)38, database.AssociationsDatabase().NumberOfObjects());

            // This wouldmt have internallised before, due to special characters in tenor weights
        }

        TEST_METHOD(RingingDatabase_ImportOldDatabaseVersion37_TodmordenUnitarian_LotsOfPictures)
        {
            const std::string sampleDatabasename = "todmorden_unitarian_DBVer_37.duc";
            RingingDatabase database;
            Duco::StatisticFilters filters(database);

            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)37, databaseVersionNumber);

            Assert::AreEqual((size_t)51, database.PealsDatabase().NumberOfObjects());
            Assert::AreEqual((size_t)230, database.NumberOfRingers());
            size_t linkedTowers = 0;
            Assert::AreEqual((size_t)1, database.TowersDatabase().NumberOfActiveTowers(linkedTowers));
            Assert::AreEqual((size_t)0, linkedTowers);
            Assert::AreEqual((size_t)24, database.MethodsDatabase().NumberOfObjects());
            PealLengthInfo withDrawn;
            Assert::AreEqual((size_t)51, database.PealsDatabase().PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)0, withDrawn.TotalPeals());
            Assert::AreEqual((size_t)7, database.AssociationsDatabase().NumberOfObjects());
            Assert::AreEqual((size_t)11, database.PicturesDatabase().NumberOfObjects());

            Assert::AreEqual(L"https://www.pealbase.co.uk/pealbase/guild.php?gid=", database.Settings().PealbaseAssociationURL().c_str());
            Assert::IsTrue(database.Settings().SettingsValid());
        }
        TEST_METHOD(RingingDatabase_MethodDBVer38_CheckCallings)
        {
            const std::string sampleDatabasename = "bristol_royal_DBVer_38.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)38, databaseVersionNumber);

            const Method* const bristol = database.MethodsDatabase().FindMethod(1);

            Assert::IsNotNull(bristol);
            Assert::AreEqual(L"", bristol->PealbaseLink().c_str());
            Assert::AreEqual(L"&x50x14.50x50.36.14x70.58.16x16.70x16x10-10", bristol->PlaceNotation().c_str());
            std::wstring notation;
            Assert::IsTrue(bristol->BobPlaceNotation(notation));
            Assert::AreEqual(L"18", notation.c_str());
            Assert::IsTrue(bristol->SinglePlaceNotation(notation));
            Assert::AreEqual(L"1890", notation.c_str());
            Assert::IsTrue(bristol->BobPlaceNotation(notation, 1));
            Assert::AreEqual(L"14", notation.c_str());
            Assert::IsTrue(bristol->SinglePlaceNotation(notation, 1));
            Assert::AreEqual(L"1234", notation.c_str());
            Assert::IsFalse(bristol->BobPlaceNotation(notation, 2));
            Assert::AreEqual(L"", notation.c_str());
            Assert::IsFalse(bristol->SinglePlaceNotation(notation, 2));
            Assert::AreEqual(L"", notation.c_str());

            Assert::AreEqual(L"https://www.pealbase.co.uk/pealbase/guild.php?gid=", database.Settings().PealbaseAssociationURL().c_str());
            Assert::IsTrue(database.Settings().SettingsValid());
        }

        TEST_METHOD(RingingDatabase_MethodDBVer42_CheckCallings)
        {
            const std::string sampleDatabasename = "bristol_royal_DBVer_42.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)42, databaseVersionNumber);

            const Method* const bristol = database.MethodsDatabase().FindMethod(1);

            Assert::IsNotNull(bristol);
            Assert::AreEqual(L"", bristol->PealbaseLink().c_str());
            Assert::AreEqual(L"&x50x14.50x50.36.14x70.58.16x16.70x16x10-10", bristol->PlaceNotation().c_str());
            std::wstring notation;
            Assert::IsTrue(bristol->BobPlaceNotation(notation));
            Assert::AreEqual(L"18", notation.c_str());
            Assert::IsTrue(bristol->SinglePlaceNotation(notation));
            Assert::AreEqual(L"1890", notation.c_str());
            Assert::IsTrue(bristol->BobPlaceNotation(notation, 1));
            Assert::AreEqual(L"14", notation.c_str());
            Assert::IsTrue(bristol->SinglePlaceNotation(notation, 1));
            Assert::AreEqual(L"1234", notation.c_str());
            Assert::IsFalse(bristol->BobPlaceNotation(notation, 2));
            Assert::AreEqual(L"", notation.c_str());
            Assert::IsFalse(bristol->SinglePlaceNotation(notation, 2));
            Assert::AreEqual(L"", notation.c_str());

            Assert::AreEqual(L"https://www.pealbase.co.uk/pealbase/guild.php?gid=", database.Settings().PealbaseAssociationURL().c_str());
            Assert::IsTrue(database.Settings().SettingsValid());
            Assert::IsTrue(database.Settings().SettingsAllDefault());
        }

        TEST_METHOD(RingingDatabase_MapBoxMarkerString_TowerWithOnePeal)
        {
            RingingDatabase database;

            Duco::Tower theTower;
            theTower.SetNoOfBells(10);
            theTower.SetName(L"St Paul");
            theTower.SetCity(L"Burnley");
            theTower.SetLongitude(L"-2.24016");
            theTower.SetLatitude(L"53.79257");
            std::wstring towerMarkString = database.MapBoxMarkerString(1, 1, 1, theTower);

            Assert::AreEqual(L"{\"type\": \"Feature\", \"properties\" : {\"id\": \"4294967295\", \"count\": 1, \"tower\": 'Burnley. (St Paul)', \"bells\": 10, \"colour\": 'green'}, \"geometry\": { \"type\": \"Point\", \"coordinates\": [-2.24016, 53.79257] } },", towerMarkString.c_str());
        }

        TEST_METHOD(RingingDatabase_MapBoxMarkerString_Manchester)
        {
            RingingDatabase database;
            unsigned int databaseVer;
            database.Internalise("manchester_other_towers.duc", this, databaseVer);

            size_t noOfTowersPlotted;
            size_t noOfTowersNotPlotted;

            Duco::StatisticFilters filters(database);
            std::wstring pealMapUrl = database.MapBoxMap(noOfTowersPlotted, noOfTowersNotPlotted, mapBoxStaticKey, false, false, true, true, false, filters);

            /*std::wofstream myfile;
            myfile.open("mapbox.html");
            myfile << pealMapUrl;
            myfile.close();*/

            Assert::AreEqual((size_t)17, noOfTowersPlotted);
            Assert::AreEqual((size_t)0, noOfTowersNotPlotted);
        }

        TEST_METHOD(RingingDatabase_DBVersion45)
        {
            const std::string sampleDatabasename = "barnoldswick_DBVer_45.duc";
            RingingDatabase database;
            Duco::StatisticFilters filters(database);
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual(45u, databaseVersionNumber);
            Assert::AreEqual((size_t)39, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)70, database.NumberOfRingers());
            Assert::AreEqual((size_t)16, database.NumberOfMethods());
            Assert::AreEqual((size_t)0, database.NumberOfPictures());
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EPeal));
            Assert::AreEqual(L"https://complib.org/composition/search", database.Settings().CompLibURL().c_str());
        }

        TEST_METHOD(RingingDatabase_DBVersion46)
        {
            const std::string sampleDatabasename = "andy_ingham_DBVer_46.duc";
            RingingDatabase database;
            Duco::StatisticFilters filters(database);
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual(46u, databaseVersionNumber);

            Assert::AreEqual((size_t)29, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)98, database.NumberOfRingers());
            Assert::AreEqual((size_t)15, database.NumberOfMethods());
            Assert::AreEqual((size_t)0, database.NumberOfPictures());
            Assert::AreEqual((size_t)0, database.NumberOfObjectsWithErrors(false, TObjectType::EPeal));
            Assert::AreEqual((size_t)10, database.NumberOfObjectsWithErrors(true, TObjectType::EPeal));
            Assert::AreEqual(L"https://complib.org/composition/search", database.Settings().CompLibURL().c_str());
        }

        TEST_METHOD(RingingDatabase_AllComposers)
        {
            RingingDatabase database;
            unsigned int databaseVer;
            database.Internalise("manchester_other_towers.duc", this, databaseVer);

            std::map<std::wstring, Duco::PealLengthInfo> sortedComposerCounts;

            Duco::StatisticFilters filters(database);
            database.PealsDatabase().GetAllComposersByPealCount(filters, false, sortedComposerCounts);

            Assert::AreEqual((size_t)362, sortedComposerCounts.size());
            Assert::AreEqual((size_t)1, sortedComposerCounts.find(L"A B Mills")->second.TotalPeals());
            Assert::AreEqual((size_t)4, sortedComposerCounts.find(L"A G Reading")->second.TotalPeals());
            Assert::AreEqual((size_t)1, sortedComposerCounts.find(L"A G Reading (No.2)")->second.TotalPeals());
            Assert::AreEqual((size_t)1, sortedComposerCounts.find(L"A G Reading (No.7)")->second.TotalPeals());
        }

        TEST_METHOD(RingingDatabase_AllComposers_IgnoreBrackets)
        {
            RingingDatabase database;
            unsigned int databaseVer;
            database.Internalise("manchester_other_towers.duc", this, databaseVer);

            std::map<std::wstring, Duco::PealLengthInfo> sortedComposerCounts;

            Duco::StatisticFilters filters(database);
            database.PealsDatabase().GetAllComposersByPealCount(filters, true, sortedComposerCounts);

            Assert::AreEqual((size_t)306, sortedComposerCounts.size());
            Assert::AreEqual((size_t)1, sortedComposerCounts.find(L"A B Mills")->second.TotalPeals());
            Assert::AreEqual((size_t)6, sortedComposerCounts.find(L"A G Reading")->second.TotalPeals());
            Assert::IsTrue(sortedComposerCounts.find(L"A G Reading (No.2)") == sortedComposerCounts.end());
            Assert::IsTrue(sortedComposerCounts.find(L"A G Reading (No.7)") == sortedComposerCounts.end());
        }

        TEST_METHOD(RingingDatabase_AllConductors)
        {
            RingingDatabase database;
            unsigned int databaseVer;
            database.Internalise("manchester_other_towers.duc", this, databaseVer);

            std::multimap<Duco::PealLengthInfo, ObjectId> conductorCounts;

            Duco::StatisticFilters filters(database);
            database.PealsDatabase().GetAllConductorsByPealCount(filters, false, conductorCounts);

            Assert::AreEqual((size_t)232, conductorCounts.size());
            //Assert::AreEqual((size_t)1, conductorCounts.find(L"A G Reading (No.7)")->second.TotalPeals());
            std::pair<Duco::PealLengthInfo, ObjectId> firstConductor = *conductorCounts.begin();
            Assert::AreEqual(L"Kelly, John", database.RingersDatabase().RingerFullName(firstConductor.second, database.Settings()).c_str());
            Assert::AreEqual((size_t)1, firstConductor.first.TotalPeals());

            std::pair<Duco::PealLengthInfo, ObjectId> lastConductor = *conductorCounts.rbegin();
            Assert::AreEqual(L"Randall, Peter C", database.RingersDatabase().RingerFullName(lastConductor.second, database.Settings()).c_str());
            Assert::AreEqual((size_t)143, lastConductor.first.TotalPeals());
        }

        TEST_METHOD(RingingDatabase_SetTowersWithHandbellPealsToBeHandbellTowers)
        {
            RingingDatabase database;

            Tower handBellTowerOne;
            //handBellTowerOne.SetHandbell(true);
            Duco::ObjectId handbellTowerOneId = database.TowersDatabase().AddObject(handBellTowerOne);

            Tower towerBellTowerOne;
            Duco::ObjectId towerOneId = database.TowersDatabase().AddObject(towerBellTowerOne);

            Tower handBellTowerTwo;
            //handBellTowerTwo.SetHandbell(true);
            Duco::ObjectId handbellTowerTwoId = database.TowersDatabase().AddObject(handBellTowerTwo);

            Peal towerBellPeal;
            towerBellPeal.SetTowerId(towerOneId);
            Duco::ObjectId towerbellPealId = database.PealsDatabase().AddObject(towerBellPeal);

            Peal handBellPealOne;
            handBellPealOne.SetHandbell(true);
            handBellPealOne.SetTowerId(handbellTowerTwoId);
            Duco::ObjectId handbellPealOneId = database.PealsDatabase().AddObject(handBellPealOne);

            Peal handBellPealTwo;
            handBellPealTwo.SetTowerId(handbellTowerOneId);
            handBellPealTwo.SetHandbell(true);
            Duco::ObjectId handbellPealTwoId = database.PealsDatabase().AddObject(handBellPealTwo);

            Assert::IsTrue(database.SetTowersWithHandbellPealsAsHandbells());

            Assert::AreEqual(towerOneId, database.PealsDatabase().FindPeal(towerbellPealId)->TowerId());
            Assert::IsFalse(database.TowersDatabase().FindTower(towerOneId)->Handbell());
            Assert::AreEqual(handbellTowerOneId, database.PealsDatabase().FindPeal(handbellPealTwoId)->TowerId());
            Assert::IsTrue(database.TowersDatabase().FindTower(handbellTowerOneId)->Handbell());
            Assert::AreEqual(handbellTowerTwoId, database.PealsDatabase().FindPeal(handbellPealOneId)->TowerId());
            Assert::IsTrue(database.TowersDatabase().FindTower(handbellTowerTwoId)->Handbell());
        }

        TEST_METHOD(RingingDatabase_ReadDBVer50AndCheckCSVExport)
        {
            const std::string sampleDatabasename = "judith_caton_DBVer_50.duc";
            const std::string externaliseName = "judith_caton_DBVer_50.csv";
            RingingDatabase database;
            Duco::StatisticFilters filters(database);

            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)50, databaseVersionNumber);

            Assert::AreEqual((size_t)23, database.PerformanceInfo(filters).TotalPeals());

            database.ExternaliseToCsv(externaliseName.c_str(), this, sampleDatabasename);

            Assert::AreEqual((size_t)database.PerformanceInfo(filters).TotalPeals() + 1, NumberOfLinesInFile(externaliseName));
            Assert::AreEqual((std::uintmax_t)11338, FileSize(externaliseName));
            NumberOfColumnsInFile(externaliseName);
        }

        TEST_METHOD(RingingDatabase_ReadDBVer50AndCheckPealsValidity)
        {
            const std::string sampleDatabasename = "halifax_DBVer_50.duc";
            RingingDatabase database;

            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)50, databaseVersionNumber);

            Duco::StatisticFilters filters(database);
            Assert::AreEqual((size_t)218, database.PerformanceInfo(filters).TotalPeals());

            const Duco::Peal* const thePeal = database.PealsDatabase().FindPeal(45);
            Assert::IsFalse(thePeal->Valid(database, true, true));

            Assert::AreEqual(L"Ringers incomplete", thePeal->ErrorString(database.Settings(), true).c_str());
        }

        TEST_METHOD(RingingDatabase_ExternaliseToCSVWithCharsInFootnote)
        {
            const std::string externaliseName = "Footnotes_With_SpecialChars.csv";
            const std::string internaliseName = "quarter_footnote_special_chars.duc";
            RingingDatabase database;

            unsigned int databaseVersionNumber = 0;
            database.Internalise(internaliseName.c_str(), this, databaseVersionNumber);
            Assert::AreEqual((unsigned int)50, databaseVersionNumber);

            Duco::StatisticFilters filters(database);
            Assert::AreEqual((size_t)2, database.PerformanceInfo(filters).TotalPeals());

            database.ExternaliseToCsv(externaliseName.c_str(), this, externaliseName);

            Assert::AreEqual((size_t)database.PerformanceInfo(filters).TotalPeals()+1, NumberOfLinesInFile(externaliseName));
            Assert::AreEqual((std::uintmax_t)1382, FileSize(externaliseName));
            NumberOfColumnsInFile(externaliseName);
        }

        TEST_METHOD(RingingDatabase_SuggestRinger)
        {
            const std::string internaliseName = "owen_battye_DBVer_36.duc";
            unsigned int databaseVersionNumber = 0;

            RingingDatabase database;
            database.Internalise(internaliseName.c_str(), this, databaseVersionNumber);

            Assert::AreEqual((unsigned int)36, databaseVersionNumber);

            std::set<Duco::ObjectId> nearMatches;
            ObjectId owen = database.RingersDatabase().SuggestRinger(L"R Owen Battye", true, nearMatches);
            Assert::IsTrue(owen.ValidId());

            Assert::IsFalse(database.RingersDatabase().SuggestRinger(L"R Owe Battye", true, nearMatches).ValidId());
            Assert::AreEqual(owen, *nearMatches.begin());

            Assert::IsFalse(database.RingersDatabase().SuggestRinger(L"R O Battye", true, nearMatches).ValidId());
            Assert::AreEqual(owen, *nearMatches.begin());
        }

        TEST_METHOD(RingingDatabase_Capitalise_NoData)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            //database.CapitaliseFields(*this, bool methodName, bool MethodType, bool county, bool dedication, bool ringerNames);
            database.CapitaliseFields(*this, true, true, true, true, true);

            Assert::IsFalse(database.DataChanged());
        }

        TEST_METHOD(RingingDatabase_Capitalise)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Method method(KNoId, 6, L"CAMBRIDGE", L"SURPRISE", L"", false);
            Duco::ObjectId methodId = database.MethodsDatabase().AddObject(method);

            Tower tower(KNoId, L"ST PAUL", L"WALKDEN", L"LANCASHIRE", 8);
            Duco::ObjectId towerId = database.TowersDatabase().AddObject(tower);

            Ringer ringer(KNoId, L"R OWEN", L"BATTYE");
            Duco::ObjectId ringerId = database.RingersDatabase().AddObject(ringer);

            database.ResetDataChanged();
            //database.CapitaliseFields(*this, bool methodName, bool MethodType, bool county, bool dedication, bool ringerNames);
            database.CapitaliseFields(*this, true, true, true, true, true);

            Assert::IsTrue(database.DataChanged());

            Assert::AreEqual(L"Cambridge", database.MethodsDatabase().FindMethod(methodId)->Name().c_str());
            Assert::AreEqual(L"Surprise", database.MethodsDatabase().FindMethod(methodId)->Type().c_str());
            Assert::AreEqual(L"Lancashire", database.TowersDatabase().FindTower(towerId)->County().c_str());
            Assert::AreEqual(L"St Paul", database.TowersDatabase().FindTower(towerId)->Name().c_str());
            Assert::AreEqual(L"R Owen Battye", database.RingersDatabase().FindRinger(ringerId)->FullName(false).c_str());
        }


        TEST_METHOD(RingingDatabase_DuplicatePeals)
        {
            const std::string sampleDatabasename = "3_duplicate_peals.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);

            StatisticFilters filters(database);
            DatabaseSearch search(database);
            search.AddValidationArgument(new TSearchDuplicateObject(true));

            std::set<Duco::ObjectId> pealIds;
            search.Search(*this, pealIds, TObjectType::EPeal);

            Assert::AreEqual((size_t)3, pealIds.size());
        }

        TEST_METHOD(RingingDatabase_NonDuplicatePeals)
        {
            const std::string sampleDatabasename = "non_duplicate_peals.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);

            StatisticFilters filters(database);
            DatabaseSearch search(database);
            search.AddValidationArgument(new TSearchDuplicateObject(true));

            std::set<Duco::ObjectId> pealIds;
            search.Search(*this, pealIds, TObjectType::EPeal);

            Assert::AreEqual((size_t)0, pealIds.size());
        }

        TEST_METHOD(RingingDatabase_ExportToCSV)
        {
            const std::string sampleDatabasename = "exporttocsvtest_withsinglequotes.duc";
            RingingDatabase database;
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);

            Assert::IsTrue(database.ExternaliseToCsv("exporttocsvtest_withsinglequotes.csv", this, sampleDatabasename));

            Assert::AreEqual((size_t)3, NumberOfLinesInFile("exporttocsvtest_withsinglequotes.csv"));
            NumberOfColumnsInFile("exporttocsvtest_withsinglequotes.csv");
        }

        TEST_METHOD(RingingDatabase_RemoveUsedLinkedRingerIds)
        {
            RingingDatabase database;
            database.RingersDatabase().AddObject(Ringer(1, L"A", L"A"));
            database.RingersDatabase().AddObject(Ringer(2, L"B", L"B"));
            database.RingersDatabase().AddObject(Ringer(3, L"C", L"C"));

            database.RingersDatabase().LinkRingers(1, 2);

            std::set<ObjectId> unusedMethods;
            std::set<ObjectId> unusedRingers;
            std::set<ObjectId> unusedTowers;
            std::multimap<ObjectId, ObjectId> unusedRings;
            std::set<ObjectId> unusedMethodSeries;
            std::set<ObjectId> unusedCompositions;
            std::set<ObjectId> unusedAssociations;
            database.FindUnused(unusedMethods, unusedRingers, unusedTowers, unusedRings, unusedMethodSeries, unusedCompositions, unusedAssociations);

            Assert::IsTrue(unusedRingers.contains(3));
        }

        TEST_METHOD(RingingDatabase_FindUnusedAssociationIds)
        {
            RingingDatabase database;
            Duco::ObjectId assOneId = database.AssociationsDatabase().AddObject(Association(1, L"A", L"A"));
            Association assTwo(2, L"B", L"B");
            assTwo.AddAssociationLink(assOneId);
            Duco::ObjectId assTwoId = database.AssociationsDatabase().AddObject(assTwo);

            Peal newPeal;
            newPeal.SetAssociationId(assOneId);
            database.PealsDatabase().AddObject(newPeal);

            std::set<ObjectId> unusedMethods;
            std::set<ObjectId> unusedRingers;
            std::set<ObjectId> unusedTowers;
            std::multimap<ObjectId, ObjectId> unusedRings;
            std::set<ObjectId> unusedMethodSeries;
            std::set<ObjectId> unusedCompositions;
            std::set<ObjectId> unusedAssociations;
            database.FindUnused(unusedMethods, unusedRingers, unusedTowers, unusedRings, unusedMethodSeries, unusedCompositions, unusedAssociations);

            Assert::AreEqual((size_t)0, unusedAssociations.size());
        }

        TEST_METHOD(RingingDatabase_GetRingersPealCount)
        {
            RingingDatabase database;
            unsigned int databaseVer;
            database.Internalise("manchester_other_towers.duc", this, databaseVer);

            std::set<Duco::ObjectId> nearMatches;
            Duco::ObjectId ringer1Id = database.RingersDatabase().SuggestRinger(L"Charles H Blakey", false, nearMatches);
            Duco::ObjectId ringer2Id = database.RingersDatabase().SuggestRinger(L"Charles W Blakey", false, nearMatches);
            Duco::ObjectId ringer3Id = database.RingersDatabase().SuggestRinger(L"Derek J Thomas", false, nearMatches);
            Duco::ObjectId ringer4Id = database.RingersDatabase().SuggestRinger(L"Derek Thomas", false, nearMatches);

            database.RingersDatabase().LinkRingers(ringer1Id, ringer2Id);

            std::map<ObjectId, Duco::PealLengthInfo> sortedRingerIds;

            StatisticFilters filters(database);
            database.PealsDatabase().GetRingersPealCount(filters, false, sortedRingerIds);

            Assert::IsTrue(sortedRingerIds.contains(ringer1Id));
            Assert::IsTrue(sortedRingerIds.contains(ringer2Id));
            Assert::IsTrue(sortedRingerIds.contains(ringer3Id));
            Assert::IsTrue(sortedRingerIds.contains(ringer4Id));
            Assert::AreEqual((size_t)1, sortedRingerIds[ringer1Id].TotalPeals());
            Assert::AreEqual((size_t)2, sortedRingerIds[ringer2Id].TotalPeals());
            Assert::AreEqual((size_t)243, sortedRingerIds[ringer3Id].TotalPeals());
            Assert::AreEqual((size_t)14, sortedRingerIds[ringer4Id].TotalPeals());
        }

        TEST_METHOD(RingingDatabase_GetRingersPealCount_LinkedRingers_CombinedCorrectly)
        {
            RingingDatabase database;
            unsigned int databaseVer;
            database.Internalise("manchester_other_towers.duc", this, databaseVer);

            std::set<Duco::ObjectId> nearMatches;
            Duco::ObjectId ringer1Id = database.RingersDatabase().SuggestRinger(L"Charles H Blakey", false, nearMatches);
            Duco::ObjectId ringer2Id = database.RingersDatabase().SuggestRinger(L"Charles W Blakey", false, nearMatches);
            Duco::ObjectId ringer3Id = database.RingersDatabase().SuggestRinger(L"Derek J Thomas", false, nearMatches);
            Duco::ObjectId ringer4Id = database.RingersDatabase().SuggestRinger(L"Derek Thomas", false, nearMatches);


            database.RingersDatabase().LinkRingers(ringer1Id, ringer2Id);

            std::map<ObjectId, Duco::PealLengthInfo> sortedRingerIds;

            StatisticFilters filters (database);
            database.PealsDatabase().GetRingersPealCount(filters, true, sortedRingerIds);

            Assert::IsFalse(sortedRingerIds.contains(ringer1Id));
            Assert::IsTrue(sortedRingerIds.contains(ringer2Id));
            Assert::IsTrue(sortedRingerIds.contains(ringer3Id));
            Assert::IsTrue(sortedRingerIds.contains(ringer4Id));
            Assert::AreEqual((size_t)243, sortedRingerIds[ringer3Id].TotalPeals());
            Assert::AreEqual((size_t)14, sortedRingerIds[ringer4Id].TotalPeals());
            Assert::AreEqual((size_t)3, sortedRingerIds[ringer2Id].TotalPeals());
        }

        TEST_METHOD(RingingDatabase_OpenDatabaseCheckRingerLinksReinitialised)
        {
            RingingDatabase database;
            database.RingersDatabase().AddObject(Ringer(1, L"A", L"A"));
            database.RingersDatabase().AddObject(Ringer(2, L"B", L"B"));
            database.RingersDatabase().LinkRingers(1, 2);

            Assert::IsTrue(database.RingersDatabase().RingersLinked(1, 2));

            const std::string sampleDatabasename = "non_duplicate_peals.duc";
            unsigned int databaseVersionNumber = 0;
            database.Internalise(sampleDatabasename.c_str(), this, databaseVersionNumber);

            Assert::IsFalse(database.RingersDatabase().RingersLinked(1, 2));
        }

        TEST_METHOD(RingingDatabase_AddWrongObjectsToWrongDatabases)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Method newMethod;
            Ringer newRinger;

            Assert::IsFalse(database.RingersDatabase().AddObject(newMethod).ValidId());
            Assert::IsFalse(database.MethodsDatabase().AddObject(newRinger).ValidId());
            Assert::IsFalse(database.TowersDatabase().AddObject(newMethod).ValidId());
            Assert::IsFalse(database.PealsDatabase().AddObject(newMethod).ValidId());
            Assert::IsFalse(database.PicturesDatabase().AddObject(newMethod).ValidId());
            Assert::IsFalse(database.MethodSeriesDatabase().AddObject(newMethod).ValidId());
            Assert::IsFalse(database.CompositionsDatabase().AddObject(newMethod).ValidId());
            Assert::IsFalse(database.AssociationsDatabase().AddObject(newMethod).ValidId());
        }

        size_t NumberOfLinesInFile(const std::string& filename)
        {
            size_t number_of_lines = 0;
            std::string line;
            std::ifstream myfile(filename.c_str());

            while (std::getline(myfile, line))
                ++number_of_lines;

            return number_of_lines;
        }

        size_t NumberOfColumnsInFile(const std::string& filename)
        {
            size_t number_of_columns = 0;
            std::string line;
            std::ifstream myfile(filename.c_str());

            while (std::getline(myfile, line))
            {
                size_t number_of_columns_on_line = 0;
                bool insideQuotes = false;
                std::string::const_iterator it = line.begin();
                while (it != line.end())
                {
                    if (*it == ',' && !insideQuotes)
                    {
                        ++number_of_columns_on_line;
                    }
                    else if (*it == '"')
                    {
                        insideQuotes = !insideQuotes;
                    }
                    ++it;
                }
                Assert::AreEqual((size_t)41, number_of_columns_on_line);
                number_of_columns = std::max(number_of_columns, number_of_columns_on_line);
            }
            Assert::AreEqual((size_t)41, number_of_columns);
            return number_of_columns;
        }

        std::uintmax_t FileSize(const std::string& filename)
        {
            std::error_code ec;
            const std::filesystem::path path(filename.c_str());
            return std::filesystem::file_size(path, ec);
        }
    };
}
