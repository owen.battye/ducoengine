#include "CppUnitTest.h"
#include <Method.h>
#include <MethodDatabase.h>
#include <Peal.h>
#include <PealDatabase.h>
#include <Ringer.h>
#include <RingerDatabase.h>
#include <RingingDatabase.h>
#include <Tower.h>
#include <TowerDatabase.h>
#include <PealLengthInfo.h>
#include <ImportExportProgressCallback.h>
#include "ToString.h"
#include <StatisticFilters.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(PealLengthInfo_UnitTests), ImportExportProgressCallback
    {
    public:
        void InitialisingImportExport(bool internalising, unsigned int versionNumber, size_t totalNumberOfObjects)
        {
            Assert::IsTrue(internalising);
            Assert::AreEqual((unsigned int)37, versionNumber);
            Assert::AreEqual((size_t)26, totalNumberOfObjects);
        }
        void ObjectProcessed(bool internalising, Duco::TObjectType objectType, Duco::ObjectId objectId, int totalPercentage, size_t numberInThisSubDatabase)
        {
            Assert::IsTrue(internalising);
        }
        void ImportExportComplete(bool internalising)
        {
            Assert::IsTrue(internalising);
        }
        void ImportExportFailed(bool internalising, int errorCode)
        {
            Assert::IsTrue(internalising);
            Assert::AreEqual(0, errorCode);
        }

        TEST_METHOD(PealLengthInfo_Lengths)
        {
            RingingDatabase database;
            Peal firstPeal;
            firstPeal.SetChanges(5000);
            PerformanceTime pealTimeOne(180);
            firstPeal.SetTime(pealTimeOne);
            database.PealsDatabase().AddObject(firstPeal);

            Peal secondPeal;
            secondPeal.SetChanges(5100);
            PerformanceTime pealTimeTwo(120);
            secondPeal.SetTime(pealTimeTwo);
            database.PealsDatabase().AddObject(secondPeal);

            StatisticFilters filters (database);
            std::set<Duco::ObjectId> pealIds;
            pealIds.insert(Duco::ObjectId(1));
            pealIds.insert(Duco::ObjectId(2));

            PealLengthInfo pealInfo = database.PealsDatabase().AllMatches(filters, pealIds);

            Assert::AreEqual((size_t)2, pealInfo.TotalPeals());
            Assert::AreEqual((size_t)10100, pealInfo.TotalChanges());
            Assert::AreEqual((size_t)300, pealInfo.TotalMinutes());
            Assert::AreEqual((unsigned int)5050, pealInfo.AverageChanges());
            Assert::AreEqual((unsigned int)150, pealInfo.AverageMinutes());
            Assert::AreEqual((float)33.6666667, pealInfo.AveragePealSpeed());
        }

        TEST_METHOD(PealLengthInfo_NoChangesInOnePeal)
        {
            RingingDatabase database;
            Peal firstPeal;
            firstPeal.SetChanges(0);
            PerformanceTime pealTimeOne(180);
            firstPeal.SetTime(pealTimeOne);
            database.PealsDatabase().AddObject(firstPeal);

            Peal secondPeal;
            secondPeal.SetChanges(5100);
            PerformanceTime pealTimeTwo(120);
            secondPeal.SetTime(pealTimeTwo);
            database.PealsDatabase().AddObject(secondPeal);

            StatisticFilters filters(database);
            std::set<Duco::ObjectId> pealIds;
            pealIds.insert(Duco::ObjectId(1));
            pealIds.insert(Duco::ObjectId(2));

            PealLengthInfo pealInfo = database.PealsDatabase().AllMatches(filters, pealIds);

            Assert::AreEqual((size_t)2, pealInfo.TotalPeals());
            Assert::AreEqual((size_t)5100, pealInfo.TotalChanges());
            Assert::AreEqual((size_t)300, pealInfo.TotalMinutes());
            Assert::AreEqual((unsigned int)5100, pealInfo.AverageChanges());
            Assert::AreEqual((unsigned int)150, pealInfo.AverageMinutes());
            Assert::AreEqual((float)42.5, pealInfo.AveragePealSpeed());
        }

        TEST_METHOD(PealLengthInfo_NoTimeInOnePeal)
        {
            RingingDatabase database;
            Peal firstPeal;
            firstPeal.SetChanges(5000);
            PerformanceTime pealTimeOne(180);
            firstPeal.SetTime(pealTimeOne);
            database.PealsDatabase().AddObject(firstPeal);

            Peal secondPeal;
            secondPeal.SetChanges(5100);
            PerformanceTime pealTimeTwo(0);
            secondPeal.SetTime(pealTimeTwo);
            database.PealsDatabase().AddObject(secondPeal);

            StatisticFilters filters(database);
            std::set<Duco::ObjectId> pealIds;
            pealIds.insert(Duco::ObjectId(1));
            pealIds.insert(Duco::ObjectId(2));

            PealLengthInfo pealInfo = database.PealsDatabase().AllMatches(filters, pealIds);

            Assert::AreEqual((size_t)2, pealInfo.TotalPeals());
            Assert::AreEqual((size_t)10100, pealInfo.TotalChanges());
            Assert::AreEqual((size_t)180, pealInfo.TotalMinutes());
            Assert::AreEqual((unsigned int)5050, pealInfo.AverageChanges());
            Assert::AreEqual((unsigned int)180, pealInfo.AverageMinutes());
            Assert::AreEqual((float)27.7777778, pealInfo.AveragePealSpeed());
        }

        TEST_METHOD(PealLengthInfo_AlmostEmptyDatabase)
        {
            RingingDatabase database;
            unsigned int databaseVersion = 0;
            database.Internalise("Bridget_paul_almost_empty.duc", this, databaseVersion);

            StatisticFilters filters(database);
            std::set<Duco::ObjectId> pealIds;
            PealLengthInfo pealInfo = database.PealsDatabase().AllMatches(filters, pealIds);

            Assert::AreEqual((size_t)0, pealInfo.TotalPeals());
            Assert::AreEqual((size_t)0, pealInfo.TotalChanges());
            Assert::AreEqual((size_t)0, pealInfo.TotalMinutes());
            Assert::AreEqual((unsigned int)0, pealInfo.AverageChanges());
            Assert::AreEqual((unsigned int)0, pealInfo.AverageMinutes());
            Assert::AreEqual((float)0, pealInfo.AveragePealSpeed());
        }

        TEST_METHOD(PealLengthInfo_AddTwoTogether)
        {
            Peal pealOne;
            pealOne.SetDate(PerformanceDate(2010, 12, 1));
            pealOne.SetChanges(5001);
            pealOne.SetTime(PerformanceTime(181));

            PealLengthInfo pealInfoOne(pealOne);

            Peal pealTwo;
            pealTwo.SetDate(PerformanceDate(2015, 10, 31));
            pealTwo.SetChanges(5060);
            pealTwo.SetTime(PerformanceTime(245));

            PealLengthInfo pealInfoTwo(pealTwo);
            pealInfoTwo += pealInfoOne;

            Assert::AreEqual((size_t)2, pealInfoTwo.TotalPeals());
            Assert::AreEqual((size_t)10061, pealInfoTwo.TotalChanges());
            Assert::AreEqual((size_t)426, pealInfoTwo.TotalMinutes());
            Assert::AreEqual((unsigned int)5031, pealInfoTwo.AverageChanges());
            Assert::AreEqual((unsigned int)213, pealInfoTwo.AverageMinutes());
            Assert::AreEqual((float)23.617, pealInfoTwo.AveragePealSpeed(), (float)0.001);
        }

        TEST_METHOD(PealLengthInfo_AddTwoTogetherViaDatabase)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);
            Tower towerOne;
            Tower towerTwo;
            Duco::ObjectId towerOneId = database.TowersDatabase().AddObject(towerOne);
            Duco::ObjectId towerTwoId = database.TowersDatabase().AddObject(towerTwo);


            Peal pealOne;
            Duco::PerformanceDate firstPealDate(2010, 12, 1);
            pealOne.SetTowerId(towerTwoId);
            pealOne.SetDate(firstPealDate);
            pealOne.SetChanges(5001);
            pealOne.SetTime(PerformanceTime(181));
            Duco::ObjectId pealOneId = database.PealsDatabase().AddObject(pealOne);

            std::set<Duco::ObjectId> foundPealIds;
            StatisticFilters filters(database);
            filters.SetTower(true, towerTwoId);
            PealLengthInfo pealInfoOne = database.PealsDatabase().AllMatches(filters, foundPealIds);
            Assert::AreEqual((size_t)1, foundPealIds.size());
            Assert::AreEqual(pealOneId, *foundPealIds.begin());
            
            Peal pealTwo;
            Duco::PerformanceDate secondPealDate(2015, 10, 31);
            pealTwo.SetTowerId(towerOneId);
            pealTwo.SetDate(secondPealDate);
            pealTwo.SetChanges(5060);
            pealTwo.SetTime(PerformanceTime(245));
            Duco::ObjectId pealTwoId = database.PealsDatabase().AddObject(pealTwo);

            filters.SetTower(true, towerOneId);
            PealLengthInfo pealInfoTwo = database.PealsDatabase().AllMatches(filters, foundPealIds);
            Assert::AreEqual((size_t)1, foundPealIds.size());
            Assert::AreEqual(pealTwoId, *foundPealIds.begin());

            pealInfoTwo += pealInfoOne;
            Assert::AreEqual((size_t)2, pealInfoTwo.TotalPeals());
            Assert::AreEqual((size_t)10061, pealInfoTwo.TotalChanges());
            Assert::AreEqual((size_t)426, pealInfoTwo.TotalMinutes());
            Assert::AreEqual((unsigned int)5031, pealInfoTwo.AverageChanges());
            Assert::AreEqual((unsigned int)213, pealInfoTwo.AverageMinutes());
            Assert::AreEqual((float)23.617, pealInfoTwo.AveragePealSpeed(), (float)0.001);

            Assert::AreEqual(L"1/12/2010", pealInfoTwo.DateOfFirstPealString().c_str());
            Assert::AreEqual(L"31/10/2015", pealInfoTwo.DateOfLastPealString().c_str());
            Assert::AreEqual(pealTwoId, pealInfoTwo.LongestChangesId());
            Assert::AreEqual(pealOneId, pealInfoTwo.ShortestChangesId());
            Assert::AreEqual(pealTwoId, pealInfoTwo.LongestMinutesId());
            Assert::AreEqual(pealOneId, pealInfoTwo.ShortestMinutesId());
            Assert::AreEqual(pealOneId, pealInfoTwo.FastestCpmId());
            Assert::AreEqual(pealTwoId, pealInfoTwo.SlowestCpmId());
            Assert::AreEqual(pealOneId, pealInfoTwo.FirstPealId());
            Assert::AreEqual(pealTwoId, pealInfoTwo.LastPealId());
        }

        TEST_METHOD(PealLengthInfo_AddTwoTogether_BothEmpty)
        {
            PealLengthInfo pealInfoOne;
            PealLengthInfo pealInfoTwo;

            pealInfoTwo += pealInfoOne;
            Assert::AreEqual((size_t)0, pealInfoTwo.TotalPeals());
            Assert::AreEqual((size_t)0, pealInfoTwo.TotalChanges());
            Assert::AreEqual((size_t)0, pealInfoTwo.TotalMinutes());
            Assert::AreEqual((unsigned int)0, pealInfoTwo.AverageChanges());
            Assert::AreEqual((unsigned int)0, pealInfoTwo.AverageMinutes());
            Assert::AreEqual((float)0, pealInfoTwo.AveragePealSpeed(), (float)0.001);
        }

        TEST_METHOD(PealDatabase_LinkedTowers)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);
            Tower towerOne(KNoId, L"One", L"One", L"One", 8);
            Duco::ObjectId towerOneId = database.TowersDatabase().AddObject(towerOne);
            Tower towerTwo(KNoId, L"Two", L"Two", L"Two", 10);
            Duco::ObjectId towerTwoId = database.TowersDatabase().AddObject(towerTwo);
            Tower towerThree(KNoId, L"Three", L"Three", L"Three", 12);
            towerThree.SetLinkedTowerId(towerOneId);
            Duco::ObjectId towerThreeId = database.TowersDatabase().AddObject(towerThree);

            Peal pealOne;
            pealOne.SetTowerId(towerOneId);
            pealOne.SetChanges(5001);
            pealOne.SetTime(PerformanceTime(120));
            database.PealsDatabase().AddObject(pealOne);
            Peal pealTwo;
            pealTwo.SetTowerId(towerTwoId);
            pealTwo.SetChanges(5002);
            pealTwo.SetTime(PerformanceTime(150));
            database.PealsDatabase().AddObject(pealTwo);
            Peal pealThree;
            pealThree.SetTowerId(towerThreeId);
            pealThree.SetChanges(5003);
            pealThree.SetTime(PerformanceTime(180));
            database.PealsDatabase().AddObject(pealThree);

            std::set<Duco::ObjectId> foundPealIds;
            StatisticFilters filters(database);
            filters.SetTower(true, towerOneId);
            PealLengthInfo pealInfo1 = database.PealsDatabase().AllMatches(filters, foundPealIds);
            Assert::AreEqual((size_t)2, foundPealIds.size());
            Assert::AreEqual((size_t)2, pealInfo1.TotalPeals());

            filters.SetIncludeLinkedTowers(false);
            PealLengthInfo pealInfo2 = database.PealsDatabase().AllMatches(filters, foundPealIds);
            Assert::AreEqual((size_t)1, foundPealIds.size());
            Assert::AreEqual((size_t)1, pealInfo2.TotalPeals());

            filters.SetTower(true, towerTwoId);
            PealLengthInfo pealInfo3 = database.PealsDatabase().AllMatches(filters, foundPealIds);
            Assert::AreEqual((size_t)1, foundPealIds.size());
            Assert::AreEqual((size_t)1, pealInfo3.TotalPeals());

            filters.SetIncludeLinkedTowers(true);
            filters.SetTower(true, towerTwoId);
            PealLengthInfo pealInfo4 = database.PealsDatabase().AllMatches(filters, foundPealIds);
            Assert::AreEqual((size_t)1, foundPealIds.size());
            Assert::AreEqual((size_t)1, pealInfo4.TotalPeals());
        }

        TEST_METHOD(PealLengthInfo_TimeBetweenPeals_4YearsApart)
        {
            std::map<unsigned int, ObjectId> ringerIds;
            std::set<ObjectId> conductorIds;

            Peal pealOne(Duco::ObjectId(1), KNoId, KNoId, KNoId, KNoId, L"Composers", L"Footnotes", PerformanceDate(2010, 12, 1), PerformanceTime(181), 5001, false, ringerIds, Duco::TConductorType::ESilentAndNonConducted, conductorIds, 0);
            PealLengthInfo pealInfoOne(pealOne);

            Peal pealTwo (Duco::ObjectId(1), KNoId, KNoId, KNoId, KNoId, L"Composers", L"Footnotes", PerformanceDate(2015, 10, 31), PerformanceTime(245), 5002, false, ringerIds, Duco::TConductorType::ESilentAndNonConducted, conductorIds, 0);
            Assert::AreEqual(1795u, pealTwo.Date().NumberOfDaysUntil(pealOne.Date()));

            pealInfoOne += pealTwo;

            Assert::AreEqual(L"4 years and 335 days", pealInfoOne.DurationBetweenFirstAndLastPealString().c_str());
        }

        TEST_METHOD(PealLengthInfo_TimeBetweenPeals_4YearsApart_WrongOrder)
        {
            std::map<unsigned int, ObjectId> ringerIds;
            std::set<ObjectId> conductorIds;

            Peal pealOne(Duco::ObjectId(1), KNoId, KNoId, KNoId, KNoId, L"Composers", L"Footnotes", PerformanceDate(2010, 12, 1), PerformanceTime(181), 5001, false, ringerIds, Duco::TConductorType::ESilentAndNonConducted, conductorIds, 0);
            PealLengthInfo pealInfoOne(pealOne);

            Peal pealTwo(Duco::ObjectId(1), KNoId, KNoId, KNoId, KNoId, L"Composers", L"Footnotes", PerformanceDate(2015, 10, 31), PerformanceTime(245), 5002, false, ringerIds, Duco::TConductorType::ESilentAndNonConducted, conductorIds, 0);
            Assert::AreEqual(1795u, pealOne.Date().NumberOfDaysUntil(pealTwo.Date()));

            pealInfoOne += pealTwo;

            Assert::AreEqual(L"4 years and 335 days", pealInfoOne.DurationBetweenFirstAndLastPealString().c_str());
        }

        TEST_METHOD(PealLengthInfo_TimeBetweenPeals_1YearApart)
        {
            std::map<unsigned int, ObjectId> ringerIds;
            std::set<ObjectId> conductorIds;

            Peal pealOne(Duco::ObjectId(1), KNoId, KNoId, KNoId, KNoId, L"Composers", L"Footnotes", PerformanceDate(2010, 10, 1), PerformanceTime(181), 5001, false, ringerIds, Duco::TConductorType::ESilentAndNonConducted, conductorIds, 0);
            PealLengthInfo pealInfoOne(pealOne);

            Peal pealTwo(Duco::ObjectId(1), KNoId, KNoId, KNoId, KNoId, L"Composers", L"Footnotes", PerformanceDate(2011, 10, 2), PerformanceTime(245), 5002, false, ringerIds, Duco::TConductorType::ESilentAndNonConducted, conductorIds, 0);
            Assert::AreEqual(366u, pealTwo.Date().NumberOfDaysUntil(pealOne.Date()));

            pealInfoOne += pealTwo;

            Assert::AreEqual(L"1 year and 1 day", pealInfoOne.DurationBetweenFirstAndLastPealString().c_str());
        }

        TEST_METHOD(PealLengthInfo_TimeBetweenPeals_SameDay)
        {
            std::map<unsigned int, ObjectId> ringerIds;
            std::set<ObjectId> conductorIds;

            Peal pealOne(Duco::ObjectId(1), KNoId, KNoId, KNoId, KNoId, L"Composers", L"Footnotes", PerformanceDate(2010, 12, 1), PerformanceTime(181), 5001, false, ringerIds, Duco::TConductorType::ESilentAndNonConducted, conductorIds, 0);
            PealLengthInfo pealInfoOne(pealOne);

            Peal pealTwo(Duco::ObjectId(1), KNoId, KNoId, KNoId, KNoId, L"Composers", L"Footnotes", PerformanceDate(2010, 12, 1), PerformanceTime(245), 5002, false, ringerIds, Duco::TConductorType::ESilentAndNonConducted, conductorIds, 0);
            Assert::AreEqual(0u, pealTwo.Date().NumberOfDaysUntil(pealOne.Date()));

            pealInfoOne += pealTwo;

            Assert::AreEqual(L"Zero days", pealInfoOne.DurationBetweenFirstAndLastPealString().c_str());
        }

        TEST_METHOD(PealLengthInfo_TimeBetweenPeals_ExactlyAYear)
        {
            std::map<unsigned int, ObjectId> ringerIds;
            std::set<ObjectId> conductorIds;

            Peal pealOne(Duco::ObjectId(1), KNoId, KNoId, KNoId, KNoId, L"Composers", L"Footnotes", PerformanceDate(2010, 12, 1), PerformanceTime(181), 5001, false, ringerIds, Duco::TConductorType::ESilentAndNonConducted, conductorIds, 0);
            PealLengthInfo pealInfoOne(pealOne);

            Peal pealTwo(Duco::ObjectId(1), KNoId, KNoId, KNoId, KNoId, L"Composers", L"Footnotes", PerformanceDate(2011, 12, 1), PerformanceTime(245), 5002, false, ringerIds, Duco::TConductorType::ESilentAndNonConducted, conductorIds, 0);
            Assert::AreEqual(365u, pealTwo.Date().NumberOfDaysUntil(pealOne.Date()));

            pealInfoOne += pealTwo;

            Assert::AreEqual(L"1 year", pealInfoOne.DurationBetweenFirstAndLastPealString().c_str());
        }

        TEST_METHOD(PealLengthInfo_TimeBetweenPeals_PeterRandall_LongTime)
        {
            std::map<unsigned int, ObjectId> ringerIds;
            std::set<ObjectId> conductorIds;

            Peal pealOne(Duco::ObjectId(1), KNoId, KNoId, KNoId, KNoId, L"Composers", L"Footnotes", PerformanceDate(1969, 8, 2), PerformanceTime(181), 5001, false, ringerIds, Duco::TConductorType::ESilentAndNonConducted, conductorIds, 0);
            PealLengthInfo pealInfoOne(pealOne);

            Peal pealTwo(Duco::ObjectId(1), KNoId, KNoId, KNoId, KNoId, L"Composers", L"Footnotes", PerformanceDate(2022, 9, 23), PerformanceTime(245), 5002, false, ringerIds, Duco::TConductorType::ESilentAndNonConducted, conductorIds, 0);
            Assert::AreEqual(19410u, pealTwo.Date().NumberOfDaysUntil(pealOne.Date()));

            pealInfoOne += pealTwo;

            Assert::AreEqual(L"53 years and 65 days", pealInfoOne.DurationBetweenFirstAndLastPealString().c_str());
        }
    };
}
