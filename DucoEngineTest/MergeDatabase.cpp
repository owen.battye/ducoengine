﻿#include "CppUnitTest.h"
#include <RingingDatabase.h>
#include <Peal.h>
#include <PealDatabase.h>
#include <Tower.h>
#include <TowerDatabase.h>
#include <Picture.h>
#include <PictureDatabase.h>
#include <Method.h>
#include <MethodDatabase.h>
#include <MethodSeries.h>
#include <MethodSeriesDatabase.h>
#include <Composition.h>
#include <CompositionDatabase.h>
#include "ToString.h"
#include <ImportExportProgressCallback.h>
#include <RenumberProgressCallback.h>
#include <DucoEngineUtils.h>
#include <MergeDatabase.h>
#include <ProgressCallback.h>
#include <fstream>
#include <StatisticFilters.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(MergeDatabase_UnitTests), ImportExportProgressCallback, ProgressCallback, RenumberProgressCallback
    {
    public:
        //From ImportExportProgressCallback
        void InitialisingImportExport(bool internalising, unsigned int versionNumber, size_t totalNumberOfObjects)
        {

        }
        void ObjectProcessed(bool internalising, Duco::TObjectType objectType, Duco::ObjectId objectId, int totalPercentage, size_t numberInThisSubDatabase)
        {

        }
        void ImportExportComplete(bool internalising)
        {

        }
        void ImportExportFailed(bool internalising, int errorCode)
        {
            Assert::IsFalse(true);
        }

        // from ProgressCallback
        void Initialised()
        {
        }
        void Step(int progressPercent)
        {
        }
        void Complete(bool error)
        {
            Assert::AreEqual(expectedError, error);
        }
        // From RenumberProgressCallback
        void RenumberInitialised(RenumberProgressCallback::TRenumberStage newStage)
        {
        }
        void RenumberStep(size_t objectId, size_t total)
        {
        }
        void RenumberComplete()
        {
        }

        const size_t NumberOfRingersInMergedDatabase = 12;
        const char* databaseOne = "mergedatabase_1.duc";
        const char* databaseTwo = "mergedatabase_2.duc";
        const char* databaseThree = "mergedatabase_3.duc";
        const char* databaseFour = "mergedatabase_4.duc";
        const char* databaseFive = "mergedatabase_5.duc";
        const char* databaseSix = "mergedatabase_6.duc";
        const char* databaseSeven = "mergedatabase_7_2methods.duc";
        const char* databaseEight = "mergedatabase_8_2methods_and_series.duc";
        bool expectedError;
        TEST_METHOD(MergeDatabase_OpenAlreadyMergedDatabase)
        {
            expectedError = false;
            RingingDatabase database;
            database.ClearDatabase(false, true);
            Assert::IsFalse(database.DataChanged());

            unsigned int databaseVersion = 0;
            database.Internalise("mergedatabase_merged.duc", this, databaseVersion);
            Duco::StatisticFilters filters(database);
            Assert::IsFalse(database.DataChanged());
            Assert::IsTrue(databaseVersion > 0);
            Assert::AreEqual((size_t)2, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual(NumberOfRingersInMergedDatabase, database.NumberOfRingers());
            Assert::AreEqual((size_t)2, database.NumberOfTowers());
            Assert::AreEqual((size_t)2, database.NumberOfAssociations());
            database.Settings().SetLastNameFirst(false);

            CheckPealOnApril09th(database);
            CheckPealOnApril20th(database);
        }

        TEST_METHOD(MergeDatabase_OpenUnmergeDatabase1)
        {
            expectedError = false;
            RingingDatabase database;
            database.ClearDatabase(false, true);
            Assert::IsFalse(database.DataChanged());

            unsigned int databaseVersion = 0;
            database.Internalise(databaseOne, this, databaseVersion);
            Duco::StatisticFilters filters(database);
            Assert::IsFalse(database.DataChanged());
            Assert::IsTrue(databaseVersion > 0);
            database.Settings().SetLastNameFirst(false);
            Assert::AreEqual((size_t)1, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)6, database.NumberOfRingers());
            Assert::AreEqual((size_t)1, database.NumberOfTowers());
            Assert::AreEqual((size_t)1, database.NumberOfAssociations());

            CheckPealOnApril09th(database);
            }

        void CheckPealOnApril09th(Duco::RingingDatabase& database)
        {
            expectedError = false;
            Duco::ObjectId firstPealId = database.PealsDatabase().FindPeal(PerformanceDate(2010, 4, 9));
            const Duco::Peal* const firstPeal = database.PealsDatabase().FindPeal(firstPealId, false);

            Assert::AreEqual(L"South Northamptonshire Society", firstPeal->AssociationName(database).c_str());
            Assert::AreEqual(L"Harpole, Northamptonshire. (All Saints)", firstPeal->TowerName(database).c_str());
            Assert::AreEqual((unsigned int)5040, firstPeal->NoOfChanges());
            Assert::AreEqual(L" 9-APR-2010", firstPeal->Date().DateWithLongerMonth().c_str());
            Assert::AreEqual(L"Treble Dodging Minor", firstPeal->MethodName(database).c_str());
            Assert::AreEqual(L"Bridget Paul", firstPeal->RingerName(1, false, database).c_str());
            Assert::AreEqual(L"Alan A Paul", firstPeal->RingerName(2, false, database).c_str());
            Assert::AreEqual(L"Nicola E Firminger", firstPeal->RingerName(3, false, database).c_str());
            Assert::AreEqual(L"David E Cloake", firstPeal->RingerName(4, false, database).c_str());
            Assert::AreEqual(L"Graham C Paul", firstPeal->RingerName(5, false, database).c_str());
            Assert::AreEqual(L"Stephen Borman", firstPeal->RingerName(6, false, database).c_str());

            Assert::AreEqual(L"Wishing every happiness to Nicola and Stephen who marry in this church on 10th April.", firstPeal->Footnotes().c_str());
            Assert::IsFalse(firstPeal->Withdrawn());
            Assert::IsFalse(firstPeal->DoubleHanded());
            Assert::AreEqual(L"1083105", firstPeal->BellBoardId().c_str());
            Assert::IsFalse(firstPeal->Handbell());
            Assert::AreEqual((unsigned int)6, firstPeal->NoOfBellsRung(database));
            bool dualOrder = true;
            Assert::AreEqual((unsigned int)6, firstPeal->NoOfBellsInMethod(database.MethodsDatabase(), dualOrder));
            Assert::IsFalse(dualOrder);
            Assert::AreEqual((unsigned int)6, firstPeal->NoOfRingers());

        }
        TEST_METHOD(MergeDatabase_OpenUnmergeDatabase2)
        {
            expectedError = false;
            RingingDatabase database;
            database.ClearDatabase(false, true);
            Assert::IsFalse(database.DataChanged());

            unsigned int databaseVersion = 0;
            database.Internalise(databaseTwo, this, databaseVersion);
            Assert::IsFalse(database.DataChanged());
            Assert::IsTrue(databaseVersion > 0);
            database.Settings().SetLastNameFirst(false);
            Duco::StatisticFilters filters(database);
            Assert::AreEqual((size_t)1, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)8, database.NumberOfRingers());
            Assert::AreEqual((size_t)1, database.NumberOfTowers());
            Assert::AreEqual((size_t)1, database.NumberOfAssociations());

            CheckPealOnApril20th(database);
            }

        void CheckPealOnApril20th(Duco::RingingDatabase& database)
        {
            Duco::ObjectId firstPealId = database.PealsDatabase().FindPeal(PerformanceDate(2008,4,20));
            const Duco::Peal* const firstPeal = database.PealsDatabase().FindPeal(firstPealId, false);

            Assert::AreEqual(L"Saint Lawrence Society, Towcester", firstPeal->AssociationName(database).c_str());
            Assert::AreEqual(L"Easton Neston, Northamptonshire. (St Mary)", firstPeal->TowerName(database).c_str());
            Assert::AreEqual((unsigned int)5056, firstPeal->NoOfChanges());
            Assert::AreEqual(L"20-APR-2008", firstPeal->Date().DateWithLongerMonth().c_str());
            Assert::AreEqual(L"Yorkshire Surprise Major", firstPeal->MethodName(database).c_str());
            Assert::AreEqual(L"Mark T Weaver", firstPeal->RingerName(1, false, database).c_str());
            Assert::AreEqual(L"Bridget Paul", firstPeal->RingerName(2, false, database).c_str());
            Assert::AreEqual(L"Barbara A Foster", firstPeal->RingerName(3, false, database).c_str());
            Assert::AreEqual(L"Brett Masters", firstPeal->RingerName(4, false, database).c_str());
            Assert::AreEqual(L"Jonathan G W King", firstPeal->RingerName(5, false, database).c_str());
            Assert::AreEqual(L"Graham C Paul", firstPeal->RingerName(6, false, database).c_str());
            Assert::AreEqual(L"John Lindsay", firstPeal->RingerName(7, false, database).c_str());
            Assert::AreEqual(L"Barry E Saunders", firstPeal->RingerName(8, false, database).c_str());

            Assert::AreEqual(L"A pre-Wedding compliment to Barbara (3) and Jonathan (5) who are to be married at St. Lawrence Towcester on Saturday 26 April 2008.", firstPeal->Footnotes().c_str());
            Assert::IsFalse(firstPeal->Withdrawn());
            Assert::IsFalse(firstPeal->DoubleHanded());
            Assert::AreEqual(L"1077689", firstPeal->BellBoardId().c_str());
            Assert::IsFalse(firstPeal->Handbell());
            Assert::AreEqual((unsigned int)8, firstPeal->NoOfBellsRung(database));
            bool dualOrder = true;
            Assert::AreEqual((unsigned int)8, firstPeal->NoOfBellsInMethod(database.MethodsDatabase(), dualOrder));
            Assert::IsFalse(dualOrder);
            Assert::AreEqual((unsigned int)8, firstPeal->NoOfRingers());
        }

        TEST_METHOD(MergeDatabase_MergeDatabases2Into1_MergeAllPeals)
        {
            expectedError = true;
            RingingDatabase database;
            database.ClearDatabase(false, true);
            Assert::IsFalse(database.DataChanged());

            unsigned int databaseVersion = 0;
            database.Internalise(databaseOne, this, databaseVersion);
            Assert::IsFalse(database.DataChanged());
            database.Settings().SetLastNameFirst(false);
            CheckPealOnApril09th(database);
            Duco::StatisticFilters filters(database);
            Assert::AreEqual((size_t)1, database.PerformanceInfo(filters).TotalPeals());

            //Duco::ObjectId bridgetInDBOne = 1;
            MergeDatabase merger(*this, database);
            merger.ReadDatabase(databaseTwo);
            Assert::AreEqual(L"Saint Lawrence Society, Towcester", merger.AssociationName(1).c_str());
            Assert::AreEqual(L"Yorkshire Surprise Major", merger.MethodName(1).c_str());
            Assert::AreEqual(L"Easton Neston, Northamptonshire. (St Mary)", merger.TowerName(1).c_str());
            Assert::IsNull(merger.FindPeal(2));

            std::set<Duco::ObjectId> pealIdsToMerge;
            merger.Merge(pealIdsToMerge);

            Assert::IsTrue(databaseVersion > 0);
            Assert::AreEqual((size_t)2, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual(NumberOfRingersInMergedDatabase, database.NumberOfRingers());
            Assert::AreEqual((size_t)2, database.NumberOfTowers());
            Assert::AreEqual((size_t)2, database.NumberOfAssociations());

            CheckPealOnApril09th(database);
            CheckPealOnApril20th(database);

            database.RebuildDatabase(this);
            CheckPealOnApril09th(database);
            CheckPealOnApril20th(database);

            database.ExternaliseToXml("merged_test_afterunittest.xml", this, "frommergingunittests.duc", false, true);

            Assert::IsTrue(compareFiles("merged_test_afterunittest.xml", "merged_test.xml"));
        }

        bool compareFiles(const std::string& p1, const std::string& p2) {
            std::wifstream f1(p1, std::wifstream::binary | std::wifstream::ate);
            std::wifstream f2(p2, std::wifstream::binary | std::wifstream::ate);

            if (f1.fail() || f2.fail()) {
                return false; //file problem
            }

            if (f1.tellg() != f2.tellg()) {
                return false; //size mismatch
            }

            //seek back to beginning and use std::equal to compare contents
            f1.seekg(0, std::wifstream::beg);
            f2.seekg(0, std::wifstream::beg);
            return std::equal(std::istreambuf_iterator<wchar_t>(f1.rdbuf()),
                std::istreambuf_iterator<wchar_t>(),
                std::istreambuf_iterator<wchar_t>(f2.rdbuf()));
        }

        TEST_METHOD(MergeDatabase_MergeDatabases1Into2_MergePealsSpecifiedFromList)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);
            Assert::IsFalse(database.DataChanged());

            unsigned int databaseVersion = 0;
            database.Internalise(databaseTwo, this, databaseVersion);
            Assert::IsFalse(database.DataChanged());
            database.Settings().SetLastNameFirst(false);
            Duco::StatisticFilters filters(database);

            CheckPealOnApril20th(database);

            MergeDatabase merger(*this, database);
            merger.ReadDatabase(databaseOne);

            merger.Merge(merger.PealIds());

            Assert::IsTrue(databaseVersion > 0);
            Assert::AreEqual((size_t)2, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual(NumberOfRingersInMergedDatabase, database.NumberOfRingers());
            Assert::AreEqual((size_t)2, database.NumberOfTowers());
            Assert::AreEqual((size_t)2, database.NumberOfAssociations());

            CheckPealOnApril09th(database);
            CheckPealOnApril20th(database);
        }

        TEST_METHOD(MergeDatabase_MergeDatabases2Into1_MergePealsWithRingerId)
        {
            expectedError = true;
            RingingDatabase database;
            Duco::StatisticFilters filters(database);
            database.ClearDatabase(false, true);
            Assert::IsFalse(database.DataChanged());

            unsigned int databaseVersion = 0;
            database.Internalise(databaseOne, this, databaseVersion);
            Assert::IsFalse(database.DataChanged());
            database.Settings().SetLastNameFirst(false);
            CheckPealOnApril09th(database);

            Duco::ObjectId bridgetInDBOne = 1;
            MergeDatabase merger(*this, database);
            merger.ReadDatabase(databaseTwo, bridgetInDBOne);
            Assert::AreEqual(L"Bridget Paul", merger.DefaultRingerExternalName().c_str());

            Assert::AreEqual((size_t)1, merger.PealIds().size());
            merger.Merge(merger.PealIds());

            Assert::IsTrue(databaseVersion > 0);
            Assert::AreEqual((size_t)2, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual(NumberOfRingersInMergedDatabase, database.NumberOfRingers());
            Assert::AreEqual((size_t)2, database.NumberOfTowers());
            Assert::AreEqual((size_t)2, database.NumberOfAssociations());

            CheckPealOnApril09th(database);
            CheckPealOnApril20th(database);
        }

        TEST_METHOD(MergeDatabase_MergeDatabases4Into3_MergeAllPeals)
        {
            RingingDatabase database;
            Duco::StatisticFilters filters(database);
            database.ClearDatabase(false, true);
            Assert::IsFalse(database.DataChanged());

            unsigned int databaseVersion = 0;
            database.Internalise(databaseThree, this, databaseVersion);
            Assert::IsFalse(database.DataChanged());
            //CheckPealOnApril09th(database);
            //CheckPealOnApril20th(database);
            //Assert::AreEqual((size_t)2, database.PerformanceInfo());
            //Assert::AreEqual((size_t)3, database.NumberOfRingers());
            //Assert::AreEqual((size_t)2, database.NumberOfTowers());
            //Assert::AreEqual((size_t)2, database.NumberOfAssociations());

            MergeDatabase merger(*this, database);
            merger.ReadDatabase(databaseFour);

            std::set<Duco::ObjectId> pealIdsToMerge;
            merger.Merge(pealIdsToMerge);

            Assert::IsTrue(databaseVersion > 0);

            //CheckPealOnApril09th(database);
            //CheckPealOnApril20th(database);
            //Assert::AreEqual((size_t)2, database.PerformanceInfo());
            //Assert::AreEqual((size_t)3, database.NumberOfRingers());
            //Assert::AreEqual((size_t)2, database.NumberOfTowers());
            //Assert::AreEqual((size_t)2, database.NumberOfAssociations());

            database.RebuildDatabase(this);

            database.ExternaliseToXml("merged_test3_afterunittest.xml", this, "frommergingunittests3.duc", false, true);

            Assert::IsTrue(compareFiles("merged_test3_afterunittest.xml", "merged_test3.xml"));
        }

        TEST_METHOD(MergeDatabase_MergeDatabasesContainingPictureAndMethodSeries)
        {
            RingingDatabase database;
            Duco::StatisticFilters filters(database);
            unsigned int databaseVersion = 0;
            database.Internalise(databaseFive, this, databaseVersion);
            Assert::IsFalse(database.DataChanged());

            Assert::AreEqual((size_t)1, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)1, database.NumberOfPictures());
            Assert::AreEqual((size_t)1, database.NumberOfMethodSeries());
            Assert::AreEqual((size_t)8, database.NumberOfMethods());
            Assert::AreEqual((size_t)6, database.NumberOfRingers());
            Assert::AreEqual((size_t)1, database.NumberOfTowers());
            Assert::AreEqual((size_t)0, database.NumberOfCompositions());
            CheckPealOnFebuary17th(database);

            MergeDatabase merger(*this, database);
            merger.ReadDatabase(databaseSix);

            std::set<Duco::ObjectId> pealIdsToMerge;
            merger.Merge(pealIdsToMerge);

            Assert::AreEqual((size_t)2, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)2, database.NumberOfPictures());
            CheckPealOnFebuary17th(database);
            CheckPealOnJune21st(database);

            database.RebuildDatabase(this);
            CheckPealOnFebuary17th(database);
            CheckPealOnJune21st(database);
        }

        void CheckPealOnFebuary17th(Duco::RingingDatabase& database)
        {
            Duco::ObjectId firstPealId = database.PealsDatabase().FindPeal(PerformanceDate(2007, 2, 17));
            const Duco::Peal* const firstPeal = database.PealsDatabase().FindPeal(firstPealId, false);
            Assert::AreEqual(Duco::ObjectId(), firstPeal->PictureId());
            Assert::AreEqual(L"392259", firstPeal->BellBoardId().c_str());
            Assert::AreEqual(L"Saunders, Barry E", firstPeal->RingerName(1, false, database).c_str());
            Assert::AreEqual(L"Sampson, Colin E", firstPeal->RingerName(4, false, database).c_str());
            Assert::AreEqual(L"Thurman, John M", firstPeal->ConductorName(database).c_str());

            Assert::AreEqual(L"Treble Dodging Minor", firstPeal->MethodName(database).c_str());

            const Duco::Tower* const firstTower = database.TowersDatabase().FindTower(firstPeal->TowerId(), false);
            Assert::AreEqual(L"12-1-12 in F♯", firstTower->TenorDescription(firstPeal->RingId()).c_str());
            Assert::AreNotEqual(Duco::ObjectId(), firstTower->PictureId());

            const Duco::Picture* const firstPicture = database.PicturesDatabase().FindPicture(firstTower->PictureId(), false);
            Assert::IsTrue(firstPicture->ContainsValidPicture());
            Assert::AreEqual((size_t)67595, firstPicture->PictureSize());
            Assert::AreEqual(".png", firstPicture->GetFileExtension().c_str());
            Assert::IsTrue(firstPicture->Valid(database, true, true));

            const Duco::MethodSeries* const firstSeries = database.MethodSeriesDatabase().FindMethodSeries(firstPeal->SeriesId(), false);
            Assert::IsTrue(firstSeries->MethodNames(database.MethodsDatabase()).find(L"Elston") != std::wstring::npos);
            Assert::IsTrue(firstSeries->MethodNames(database.MethodsDatabase()).find(L"Oswald") != std::wstring::npos);
            Assert::IsTrue(firstSeries->MethodNames(database.MethodsDatabase()).find(L"Pembroke") != std::wstring::npos);
            Assert::IsTrue(firstSeries->MethodNames(database.MethodsDatabase()).find(L"Allendale") != std::wstring::npos);
            Assert::IsTrue(firstSeries->MethodNames(database.MethodsDatabase()).find(L"Hull") != std::wstring::npos);
            Assert::IsTrue(firstSeries->MethodNames(database.MethodsDatabase()).find(L"Lincoln") != std::wstring::npos);
            Assert::IsTrue(firstSeries->MethodNames(database.MethodsDatabase()).find(L"Rossendale") != std::wstring::npos);
            Assert::AreEqual((size_t)7, firstSeries->NoOfMethods());
        }

        TEST_METHOD(MergeDatabase_MergeDatabasesContainingComposition)
        {
            RingingDatabase database;
            Duco::StatisticFilters filters(database);
            unsigned int databaseVersion = 0;
            database.Internalise(databaseSix, this, databaseVersion);
            Assert::IsFalse(database.DataChanged());

            Assert::AreEqual((size_t)1, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)2, database.NumberOfPictures());
            Assert::AreEqual((size_t)0, database.NumberOfMethodSeries());
            Assert::AreEqual((size_t)8, database.NumberOfMethods());
            Assert::AreEqual((size_t)9, database.NumberOfRingers());
            Assert::AreEqual((size_t)1, database.NumberOfTowers());
            Assert::AreEqual((size_t)2, database.NumberOfCompositions());

            CheckPealOnJune21st(database);

            MergeDatabase merger(*this, database);
            merger.ReadDatabase(databaseFive);

            std::set<Duco::ObjectId> pealIdsToMerge;
            merger.Merge(pealIdsToMerge);

            Assert::AreEqual((size_t)2, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)3, database.NumberOfPictures());
            CheckPealOnJune21st(database);
            CheckPealOnFebuary17th(database);

            database.RebuildDatabase(this);
            CheckPealOnJune21st(database);
            CheckPealOnFebuary17th(database);

        }

        void CheckPealOnJune21st(Duco::RingingDatabase& database)
        {
            Duco::ObjectId firstPealId = database.PealsDatabase().FindPeal(PerformanceDate(2014, 6, 21));
            const Duco::Peal* const firstPeal = database.PealsDatabase().FindPeal(firstPealId, false);
            Assert::AreNotEqual(Duco::ObjectId(), firstPeal->PictureId());
            Assert::AreEqual(L"1148253", firstPeal->BellBoardId().c_str());
            Assert::AreEqual(L"Paul, Bridget", firstPeal->RingerName(1, false, database).c_str());
            Assert::AreEqual(L"Higson, Jennie", firstPeal->RingerName(2, false, database).c_str());
            Assert::AreEqual(L"Jordan, Gregory M", firstPeal->RingerName(3, false, database).c_str());
            Assert::AreEqual(L"Parsons, Nicholas J", firstPeal->RingerName(4, false, database).c_str());
            Assert::AreEqual(L"Paul, Graham C", firstPeal->RingerName(5, false, database).c_str());
            Assert::AreEqual(L"Willson, Giles", firstPeal->RingerName(6, false, database).c_str());
            Assert::AreEqual(L"Hickmott, Robin J", firstPeal->RingerName(7, false, database).c_str());
            Assert::AreEqual(L"Sampson, Colin E", firstPeal->RingerName(8, false, database).c_str());
            Assert::AreEqual(L"Higson, Jennie", firstPeal->ConductorName(database).c_str());

            Assert::AreEqual(L"Yorkshire Surprise Major", firstPeal->MethodName(database).c_str());

            const Duco::Tower* const firstTower = database.TowersDatabase().FindTower(firstPeal->TowerId(), false);
            Assert::AreEqual(L"17-0-4 in E", firstTower->TenorDescription(firstPeal->RingId()).c_str());
            Assert::AreEqual(Duco::ObjectId(), firstTower->PictureId());

            const Duco::Picture* const firstPicture = database.PicturesDatabase().FindPicture(firstPeal->PictureId(), false);
            Assert::IsTrue(firstPicture->ContainsValidPicture());
            Assert::AreNotEqual((size_t)67595, firstPicture->PictureSize());
            Assert::AreEqual(".jpeg", firstPicture->GetFileExtension().c_str());
            Assert::IsTrue(firstPicture->Valid(database, true, true));

            const Duco::Composition* const firstComposition = database.CompositionsDatabase().FindComposition(firstPeal->CompositionId(), false);
            Assert::AreEqual((size_t)1220, firstComposition->NoOfChanges());
            Assert::AreEqual(firstComposition->MethodId(), firstPeal->MethodId());
        }

        void CheckMethodSeries(Duco::ObjectId seriesId, const RingingDatabase& database)
        {
            const MethodSeries* series = database.MethodSeriesDatabase().FindMethodSeries(seriesId);
            Assert::IsNotNull(series);
            Assert::AreEqual(L"Oxford and Kent Treble Bob", series->MethodNames(database.MethodsDatabase()).c_str());
        }

        TEST_METHOD(MergeDatabase_MergeDatabasesContainingMethodsAndSeries)
        {
            RingingDatabase database;
            Duco::StatisticFilters filters(database);
            unsigned int databaseVersion = 0;
            database.Internalise(databaseEight, this, databaseVersion);
            Assert::IsFalse(database.DataChanged());

            Assert::AreEqual((size_t)1, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)0, database.NumberOfPictures());
            Assert::AreEqual((size_t)1, database.NumberOfMethodSeries());
            Assert::AreEqual((size_t)3, database.NumberOfMethods());
            Assert::AreEqual((size_t)1, database.NumberOfRingers());
            Assert::AreEqual((size_t)1, database.NumberOfTowers());

            CheckMethodSeries(1, database);

            database.ClearDatabase(false, false);
            database.Internalise(databaseSeven, this, databaseVersion);
            Assert::IsFalse(database.DataChanged());

            Assert::AreEqual((size_t)0, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)0, database.NumberOfPictures());
            Assert::AreEqual((size_t)0, database.NumberOfMethodSeries());
            Assert::AreEqual((size_t)2, database.NumberOfMethods());
            Assert::AreEqual((size_t)0, database.NumberOfRingers());
            Assert::AreEqual((size_t)0, database.NumberOfTowers());
            Assert::AreEqual((size_t)0, database.NumberOfCompositions());

            MergeDatabase merger(*this, database);
            merger.ReadDatabase(databaseEight);

            std::set<Duco::ObjectId> pealIdsToMerge;
            merger.Merge(pealIdsToMerge);

            Assert::AreEqual((size_t)1, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)0, database.NumberOfPictures());
            Assert::AreEqual((size_t)1, database.NumberOfMethodSeries());
            Assert::AreEqual((size_t)5, database.NumberOfMethods());
            Assert::AreEqual((size_t)1, database.NumberOfRingers());
            Assert::AreEqual((size_t)1, database.NumberOfTowers());
            Assert::AreEqual((size_t)0, database.NumberOfCompositions());

            CheckMethodSeries(1, database);
        }

        TEST_METHOD(MergeDatabase_MergeDatabases2Into1_MergePealsWithTowerbaseId)
        {
            expectedError = false;
            RingingDatabase database;
            database.ClearDatabase(false, true);
            Assert::IsFalse(database.DataChanged());
            unsigned int databaseVersion = 0;
            database.Internalise("mergedb_helmtowers.duc", this, databaseVersion);

            Duco::StatisticFilters filters(database);
            Assert::AreEqual((size_t)9, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)4, database.NumberOfTowers());

            ObjectId helmshoreId = database.TowersDatabase().FindTowerByTowerbaseId(L"2390");
            filters.SetTower(true, helmshoreId);
            Assert::AreEqual((size_t)1, database.PerformanceInfo(filters).TotalPeals());

            MergeDatabase merger(*this, database);
            merger.ReadDatabase("mergedb_helmshore.duc", KNoId, L"2390");

            std::set<Duco::ObjectId> pealIdsToMerge;
            pealIdsToMerge.insert(794); // this one already exists in the database
            pealIdsToMerge.insert(535);
            pealIdsToMerge.insert(177);
            pealIdsToMerge.insert(1976);
            merger.Merge(pealIdsToMerge);

            filters.SetTower(false, helmshoreId);
            Assert::AreEqual((size_t)12, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)4, database.NumberOfTowers());

            filters.SetTower(true, helmshoreId);
            Assert::AreEqual((size_t)4, database.PerformanceInfo(filters).TotalPeals());

            database.RebuildDatabase(this);
            ObjectId newhelmshoreId = database.TowersDatabase().FindTowerByTowerbaseId(L"2390");

            filters.SetTower(true, newhelmshoreId);
            Assert::AreEqual((size_t)4, database.PerformanceInfo(filters).TotalPeals());
        }
    };
}
