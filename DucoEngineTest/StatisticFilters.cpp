#include "CppUnitTest.h"
#include <StatisticFilters.h>
#include <DatabaseSettings.h>
#include <RingingDatabase.h>
#include <RingerDatabase.h>
#include <Peal.h>
#include <PealDatabase.h>
#include <Tower.h>
#include <TowerDatabase.h>

#include <ctime>
#include "ToString.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(StatisticFilters_UnitTests)
    {
        const std::wstring defaultFilterString = L"Filtered by:\n\tExclude withdrawn peals.";
        const std::wstring filterDisabledString = L"Filters disabled";
    public:
        TEST_METHOD(StatisticFilters_DefaultConstructor)
        {
            RingingDatabase database;
            StatisticFilters filters(database);

            Assert::AreEqual(defaultFilterString, filters.Description());
            Assert::IsTrue(filters.Enabled());
        }

        TEST_METHOD(StatisticFilters_Disabled)
        {
            RingingDatabase database;
            StatisticFilters filters(database);
            filters.SetIncludeWithdrawn(true);
            filters.SetIncludeLinkedTowers(false);

            Assert::AreEqual(filterDisabledString, filters.Description());
            Assert::IsFalse(filters.Enabled());
        }

        TEST_METHOD(StatisticFilters_IncludeWithdrawn)
        {
            RingingDatabase database;
            StatisticFilters filters(database);
            filters.SetIncludeWithdrawn(true);

            Assert::IsTrue(filters.Enabled());
            Assert::AreEqual(L"Filtered by:\n\tInclude withdrawn peals.", filters.Description().c_str());
        }


        TEST_METHOD(StatisticFilters_SetRinger)
        {
            RingingDatabase database;
            StatisticFilters filters(database);

            database.RingersDatabase().AddRinger(L"James", L"Bond");
            ObjectId ringerId = database.RingersDatabase().AddRinger(L"Owen", L"Battye");

            filters.SetRinger(true, ringerId);
            Assert::AreEqual(L"Filtered by:\n\tExclude withdrawn peals.\tRinger: Battye, Owen and include linked.", filters.Description().c_str());

            filters.SetRinger(false, ringerId);
            Assert::AreEqual(defaultFilterString, filters.Description());

            Assert::IsTrue(filters.Enabled());

            ObjectId newRingerId;
            Assert::IsFalse(filters.Ringer(newRingerId));
            Assert::AreEqual(ringerId, newRingerId);
        }

        TEST_METHOD(StatisticFilters_DefaultRinger)
        {
            RingingDatabase database;
            StatisticFilters filters(database);

            database.RingersDatabase().AddRinger(L"James", L"Bond");
            ObjectId ringerId = database.RingersDatabase().AddRinger(L"Owen", L"Battye");
            database.Settings().SetDefaultRinger(ringerId);
            filters.SetDefaultRinger();

            Assert::AreEqual(L"Filtered by:\n\tExclude withdrawn peals.\tRinger: Battye, Owen and include linked.", filters.Description().c_str());

            filters.SetRinger(false, ringerId);
            Assert::AreEqual(defaultFilterString, filters.Description());

            Assert::IsTrue(filters.Enabled());

            ObjectId newRingerId;
            Assert::IsFalse(filters.Ringer(newRingerId));
            Assert::AreEqual(ringerId, newRingerId);
        }

        TEST_METHOD(StatisticFilters_SetConductor)
        {
            RingingDatabase database;
            StatisticFilters filters(database);

            database.RingersDatabase().AddRinger(L"James", L"Bond");
            ObjectId ringerId = database.RingersDatabase().AddRinger(L"Owen", L"Battye");

            filters.SetConductor(true, ringerId);
            Assert::AreEqual(L"Filtered by:\n\tExclude withdrawn peals.\tConductor: Battye, Owen", filters.Description().c_str());

            Assert::IsTrue(filters.Enabled());

            StatisticFilters copiedFilters(database);
            copiedFilters = filters;

            Assert::IsTrue(copiedFilters.Enabled());

            filters.SetConductor(false, ringerId);
            Assert::AreEqual(defaultFilterString, filters.Description());

            Assert::IsTrue(copiedFilters.Enabled());

            ObjectId newRingerId;
            Assert::IsFalse(filters.Conductor(newRingerId));
            Assert::AreEqual(ringerId, newRingerId);

            Assert::AreEqual(L"Filtered by:\n\tExclude withdrawn peals.\tConductor: Battye, Owen", copiedFilters.Description().c_str());
        }


        TEST_METHOD(StatisticFilters_SetType)
        {
            RingingDatabase database;
            StatisticFilters filters(database);

            std::wstring typeString = L"Surprise";
            filters.SetType(true, typeString);
            Assert::AreEqual(L"Filtered by:\n\tExclude withdrawn peals.\tType: Surprise.", filters.Description().c_str());

            filters.SetType(false, typeString);
            Assert::AreEqual(defaultFilterString, filters.Description());

            StatisticFilters copiedFilters(filters);

            std::wstring newType;
            Assert::IsFalse(filters.Type(newType));
            Assert::AreEqual(newType, typeString);

            Assert::IsTrue(filters.Enabled());

            filters.Clear();
            Assert::IsTrue(filters.Enabled());
            Assert::AreEqual(defaultFilterString, filters.Description());

            Assert::AreEqual(defaultFilterString, copiedFilters.Description());
        }

        TEST_METHOD(StatisticFilters_SetCounty)
        {
            RingingDatabase database;
            StatisticFilters filters(database);

            std::wstring countyString = L"Northamptonshire";
            filters.SetCounty(true, countyString);
            Assert::AreEqual(L"Filtered by:\n\tExclude withdrawn peals.\tCounty: Northamptonshire.", filters.Description().c_str());

            filters.SetCounty(false, countyString);
            Assert::AreEqual(defaultFilterString, filters.Description());

            StatisticFilters copiedFilters(filters);

            std::wstring newCounty;
            Assert::IsFalse(filters.County(newCounty));
            Assert::AreEqual(newCounty, countyString);

            filters.Clear();
            Assert::AreEqual(defaultFilterString, filters.Description());

            Assert::AreEqual(L"Filtered by:\n\tExclude withdrawn peals.", copiedFilters.Description().c_str());
        }

        TEST_METHOD(StatisticFilters_EuropeOnly)
        {
            RingingDatabase database;
            StatisticFilters filters(database);

            Assert::IsFalse(filters.EuropeOnly());
            filters.SetEuropeOnly(true);

            Assert::AreEqual(L"Filtered by:\n\tExclude withdrawn peals.\tOnly towers in europe.", filters.Description().c_str());

            filters.Clear();
            Assert::AreEqual(defaultFilterString, filters.Description());
        }

        TEST_METHOD(StatisticFilters_CopyConstructor_FromDefaultConstructor)
        {
            RingingDatabase database;
            StatisticFilters filters(database);
            StatisticFilters filters2(filters);

            Assert::AreEqual(filters.Description(), filters2.Description());

            filters.Clear();
            Assert::AreEqual(defaultFilterString, filters.Description());
        }

        TEST_METHOD(StatisticFilters_CopyConstructor_EnableTower)
        {
            RingingDatabase database;
            StatisticFilters filters(database);
            filters.SetTower(true, Duco::ObjectId(1));
            filters.SetIncludeLinkedTowers(true);

            StatisticFilters filters2(filters);

            Assert::AreEqual(filters.Description(), filters2.Description());

            filters.Clear();
            filters2.SetTower(false, Duco::ObjectId(1));
            // Linked towers are enabled by default
            Assert::AreEqual(filters.Description(), filters2.Description());
        }

        TEST_METHOD(StatisticFilters_CopyConstructor_EnableMethod)
        {
            RingingDatabase database;
            StatisticFilters filters(database);
            filters.SetMethod(true, Duco::ObjectId(1));

            StatisticFilters filters2(filters);

            Assert::AreEqual(filters.Description(), filters2.Description());

            filters.Clear();
            Assert::AreNotEqual(filters.Description(), filters2.Description());
        }


        TEST_METHOD(StatisticFilters_SetDates)
        {
            RingingDatabase database;
            Duco::PerformanceDate startDate(2021, 7, 4);
            Duco::PerformanceDate endDate(2022, 12, 31);
            StatisticFilters filters(database);
            filters.SetStartDate(false, startDate);
            filters.SetEndDate(true, endDate);
            Duco::PerformanceDate checkDate;

            Assert::IsFalse(filters.StartDateEnabled(checkDate));
            Assert::AreEqual(startDate, checkDate);
            Assert::IsTrue(filters.EndDateEnabled(checkDate));
            Assert::AreEqual(endDate, checkDate);
            Assert::AreEqual(startDate, filters.StartDate());
            Assert::AreEqual(endDate, filters.EndDate());
        }

        TEST_METHOD(StatisticFilters_LinkedTowers)
        {
            RingingDatabase database;

            Tower towerOne;
            Tower towerTwo;
            Duco::ObjectId towerOneId = database.TowersDatabase().AddObject(towerOne);
            towerTwo.SetLinkedTowerId(towerOneId);
            Duco::ObjectId towerTwoId = database.TowersDatabase().AddObject(towerTwo);

            Peal pealOne;
            pealOne.SetTowerId(towerOneId);
            Duco::ObjectId pealOneId = database.PealsDatabase().AddObject(pealOne);
            Peal pealTwo;
            pealTwo.SetTowerId(towerTwoId);
            Duco::ObjectId pealTwoId = database.PealsDatabase().AddObject(pealTwo);

            StatisticFilters filters(database);
            filters.SetIncludeLinkedTowers(true);
            filters.SetTower(true, towerOneId);

            PealLengthInfo info = database.PerformanceInfo(filters);

            Assert::AreEqual((size_t)2, info.TotalPeals());
            {
                // This creation and deletion, proven that statistic filters shallow copier crashed,
                // when linked towers were processed.
                // Don't remove this or the second search.
                StatisticFilters localFilters(filters);
            }
            PealLengthInfo info2 = database.PerformanceInfo(filters);
            Assert::AreEqual((size_t)2, info.TotalPeals());
        }
    };
}
