#include "CppUnitTest.h"
#include <DucoEngineUtils.h>
#include <MissingRinger.h>
#include "ToString.h"
#include <iostream>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(MissingRinger_UnitTests)
    {
    public:

        TEST_METHOD(MissingRinger_Comparitor)
        {
            std::set<Duco::MissingRinger> missingRingers;

            MissingRinger firstRinger;
            firstRinger.bellNo = 1;
            firstRinger.forename = L"Sarah";
            firstRinger.surname = L"Battye";
            missingRingers.insert(firstRinger);

            MissingRinger secondRinger;
            secondRinger.bellNo = 12;
            secondRinger.forename = L"Owen";
            secondRinger.surname = L"Battye";
            missingRingers.insert(secondRinger);

            MissingRinger thirdRinger;
            thirdRinger.bellNo = 12;
            thirdRinger.strapper = true;
            thirdRinger.forename = L"James";
            thirdRinger.surname = L"Bond";
            missingRingers.insert(thirdRinger);
        }
    };
}