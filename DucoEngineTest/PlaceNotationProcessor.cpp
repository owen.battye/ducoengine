#include "CppUnitTest.h"

#include <Method.h>
#include <MethodDatabase.h>
#include <Change.h>
#include <Lead.h>
#include <PlaceNotation.h>
#include <PlaceNotationProcessor.h>

#include "ToString.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(PlaceNotationProcessor_UnitTests)
    {
    public:
        TEST_METHOD(PlaceNotationProcessor_PlainBobMinor_WholeCourse)
        {
            Method plainBobMinor(1, 6, L"Plain", L"Bob", L"&x6x1x6-2", false);
            plainBobMinor.SetBobPlaceNotation(L"14");
            //plainBobMinor.SetSinglePlaceNotation(L"1234");

            PlaceNotationProcessor processor(plainBobMinor);

            PlaceNotation notations (plainBobMinor, 0);
            processor.ProcessNotation(notations);

            Assert::AreEqual((size_t)12, notations.ChangesPerLead());
            Assert::AreEqual(L"&X16X16X16-12", notations.FullNotation(true).c_str());
            Assert::AreEqual(L"X16X16X16", notations.FullNotation(false, false).c_str());
            Assert::AreEqual(L"&X16X16X16", notations.FullNotation(true, false).c_str());

            Assert::AreEqual(L"12", notations.PlainLeadEndNotation().c_str());
            Assert::IsTrue(notations.HasNotationForCallingType(TLeadType::EPlainLead));
            Assert::IsTrue(notations.HasNotationForCallingType(TLeadType::EBobLead));
            Assert::IsFalse(notations.HasNotationForCallingType(TLeadType::ESingleLead));

            Assert::AreEqual(Duco::ObjectId(1), notations.MethodId());
            Assert::AreEqual(6u, notations.Order());
            Assert::IsFalse(notations.HasNotationForCallingType(TLeadType::ESingleLead));

            Change roundsOnSix(plainBobMinor.Order());
            Change expectedFirstLeadEnd(L"135264");
            Change expectedSecondLeadEnd(L"156342");
            Change expectedThirdLeadEnd(L"164523");
            Change expectedFourthLeadEnd(L"142635");
            Change expectedFifthLeadEnd(roundsOnSix);

            Duco::Lead* firstLead = notations.CreateLead(roundsOnSix, 0);
            Change leadEnds(plainBobMinor.Order());
            Assert::IsTrue(firstLead->LeadEnd(leadEnds));
            Assert::AreEqual(expectedFirstLeadEnd, leadEnds);

            Assert::IsTrue(notations.CreateNextLeadEndChange(Duco::EPlainLead, leadEnds));
            Assert::AreEqual(expectedSecondLeadEnd, leadEnds);

            Assert::IsTrue(notations.CreateNextLeadEndChange(Duco::EPlainLead, leadEnds));
            Assert::AreEqual(expectedThirdLeadEnd, leadEnds);

            Assert::IsTrue(notations.CreateNextLeadEndChange(Duco::EPlainLead, leadEnds));
            Assert::AreEqual(expectedFourthLeadEnd, leadEnds);

            Assert::IsTrue(notations.CreateNextLeadEndChange(Duco::EPlainLead, leadEnds));
            Assert::AreEqual(expectedFifthLeadEnd, leadEnds);
        }

        TEST_METHOD(PlaceNotationProcessor_PlainBobMinor_BobCourse)
        {
            Method plainBobMinor(1, 6, L"Plain", L"Bob", L"&x6x1x6-2", false);
            plainBobMinor.SetBobPlaceNotation(L"14");
            //plainBobMinor.SetSinglePlaceNotation(L"1234");

            PlaceNotationProcessor processor(plainBobMinor);
            PlaceNotation notations(plainBobMinor, 0);
            processor.ProcessNotation(notations);

            Change roundsOnSix(plainBobMinor.Order());
            Change expectedFirstLeadEnd(L"123564");
            Change expectedSecondLeadEnd(L"123645");
            Change expectedThirdLeadEnd(roundsOnSix);

            Change leadEnds(plainBobMinor.Order());
            Assert::IsTrue(notations.CreateNextLeadEndChange(Duco::EBobLead, leadEnds));
            Assert::AreEqual(expectedFirstLeadEnd, leadEnds);

            Assert::IsTrue(notations.CreateNextLeadEndChange(Duco::EBobLead, leadEnds));
            Assert::AreEqual(expectedSecondLeadEnd, leadEnds);

            Assert::IsTrue(notations.CreateNextLeadEndChange(Duco::EBobLead, leadEnds));
            Assert::AreEqual(expectedThirdLeadEnd, leadEnds);
        }

        TEST_METHOD(PlaceNotationProcessor_PlainBobMinor_BestStartingLead)
        {
            MethodDatabase database;
            Method plainBobMinor(1, 6, L"Plain", L"Bob", L"&x6x1x6-2", false);
            plainBobMinor.SetBobPlaceNotation(L"14");
            ObjectId methodId = database.AddObject(plainBobMinor);
            Assert::AreEqual(-1u, plainBobMinor.PrintFromBell());

            PlaceNotationProcessor processor(plainBobMinor);
            PlaceNotation notations(plainBobMinor, 0);
            processor.ProcessNotation(notations);

            Assert::AreEqual((unsigned int)2, notations.BestStartingLead(database));

            const Method* const updatedMethod = database.FindMethod(methodId);
            Assert::AreEqual((unsigned int)2, notations.BestStartingLead(database));
        }

        TEST_METHOD(PlaceNotationProcessor_BristolSurpriseRoyal_BestStartingLead)
        {
            MethodDatabase database;
            Method bristolSurpriseRoyal(1, 10, L"Bristol", L"Surprise", L"&x5x4.50x50.36.4x7.58.6x6.7x6x1-0", false);
            bristolSurpriseRoyal.SetBobPlaceNotation(L"18");
            ObjectId methodId = database.AddObject(bristolSurpriseRoyal);
            Assert::AreEqual(-1u, bristolSurpriseRoyal.PrintFromBell());

            PlaceNotationProcessor processor(bristolSurpriseRoyal);
            PlaceNotation notations(bristolSurpriseRoyal, 0);
            processor.ProcessNotation(notations);

            Assert::AreEqual((unsigned int)10, notations.BestStartingLead(database));

            const Method* const updatedMethod = database.FindMethod(methodId);
            Assert::AreEqual((unsigned int)10, notations.BestStartingLead(database));

            Assert::AreEqual((size_t)40, notations.ChangesPerLead());
            Assert::AreEqual(L"&X50X14.50X50.36.14X70.58.16X16.70X16X10-10", notations.FullNotation(true).c_str());
        }

        TEST_METHOD(PlaceNotationProcessor_BristolSurpriseRoyal_PlainCourseLeadEnds)
        {
            MethodDatabase database;
            Method bristolSurpriseRoyal(1, 10, L"Bristol", L"Surprise", L"&x5x4.50x50.36.4x7.58.6x6.7x6x1-0", false);
            bristolSurpriseRoyal.SetBobPlaceNotation(L"18");

            PlaceNotationProcessor processor(bristolSurpriseRoyal);
            PlaceNotation notations(bristolSurpriseRoyal, 0);
            processor.ProcessNotation(notations);

            Change roundsOnTen(bristolSurpriseRoyal.Order());
            Change expectedFirstLeadEnd  (L"1352749608");
            Change expectedSecondLeadEnd (L"1573920486");
            Change expectedThirdLeadEnd  (L"1795038264");
            Change expectedFourthLeadEnd (L"1907856342");
            Change expectedFifthLeadEnd  (L"1089674523");
            Change expectedSixthLeadEnd  (L"1860492735");
            Change expectedSeventhLeadEnd(L"1648203957");
            Change expectedEighthLeadEnd (L"1426385079");
            Change expectedNinthLeadEnd  (roundsOnTen);

            Duco::Lead* firstLead = notations.CreateLead(roundsOnTen, 0);
            Change leadEnds(bristolSurpriseRoyal.Order());
            Assert::IsTrue(firstLead->LeadEnd(leadEnds));
            Assert::AreEqual(expectedFirstLeadEnd, leadEnds);

            Assert::IsTrue(notations.CreateNextLeadEndChange(Duco::EPlainLead, leadEnds));
            Assert::AreEqual(expectedSecondLeadEnd, leadEnds);

            Assert::IsTrue(notations.CreateNextLeadEndChange(Duco::EPlainLead, leadEnds));
            Assert::AreEqual(expectedThirdLeadEnd, leadEnds);

            Assert::IsTrue(notations.CreateNextLeadEndChange(Duco::EPlainLead, leadEnds));
            Assert::AreEqual(expectedFourthLeadEnd, leadEnds);

            Assert::IsTrue(notations.CreateNextLeadEndChange(Duco::EPlainLead, leadEnds));
            Assert::AreEqual(expectedFifthLeadEnd, leadEnds);

            Assert::IsTrue(notations.CreateNextLeadEndChange(Duco::EPlainLead, leadEnds));
            Assert::AreEqual(expectedSixthLeadEnd, leadEnds);

            Assert::IsTrue(notations.CreateNextLeadEndChange(Duco::EPlainLead, leadEnds));
            Assert::AreEqual(expectedSeventhLeadEnd, leadEnds);

            Assert::IsTrue(notations.CreateNextLeadEndChange(Duco::EPlainLead, leadEnds));
            Assert::AreEqual(expectedEighthLeadEnd, leadEnds);

            Assert::IsTrue(notations.CreateNextLeadEndChange(Duco::EPlainLead, leadEnds));
            Assert::AreEqual(expectedNinthLeadEnd, leadEnds);
        }

        TEST_METHOD(PlaceNotationProcessor_BristolSurpriseRoyal_BobCourseLeadEnds)
        {
            MethodDatabase database;
            Method bristolSurpriseRoyal(1, 10, L"Bristol", L"Surprise", L"&x5x4.50x50.36.4x7.58.6x6.7x6x1-0", false);
            bristolSurpriseRoyal.SetBobPlaceNotation(L"18");

            PlaceNotationProcessor processor(bristolSurpriseRoyal);
            PlaceNotation notations(bristolSurpriseRoyal, 0);
            processor.ProcessNotation(notations);

            Change roundsOnTen(bristolSurpriseRoyal.Order());
            Change expectedFirstLeadEnd  (L"1352749086");
            Change expectedSecondLeadEnd (L"1573928604");
            Change expectedThirdLeadEnd  (L"1795830462");
            Change expectedFourthLeadEnd (L"1987056243");
            Change expectedFifthLeadEnd  (L"1809674325");
            Change expectedSixthLeadEnd  (L"1068492537");
            Change expectedSeventhLeadEnd(L"1640283759");
            Change expectedEighthLeadEnd (L"1426305978");
            Change expectedNinthLeadEnd  (roundsOnTen);

            Duco::Lead* firstLead = notations.CreateLead(roundsOnTen, 0);
            Change leadEnds(bristolSurpriseRoyal.Order());
            Assert::IsTrue(firstLead->LeadEnd(Duco::EBobLead, leadEnds));
            Assert::AreEqual(expectedFirstLeadEnd, leadEnds);

            Assert::IsTrue(notations.CreateNextLeadEndChange(Duco::EBobLead, leadEnds));
            Assert::AreEqual(expectedSecondLeadEnd, leadEnds);

            Assert::IsTrue(notations.CreateNextLeadEndChange(Duco::EBobLead, leadEnds));
            Assert::AreEqual(expectedThirdLeadEnd, leadEnds);

            Assert::IsTrue(notations.CreateNextLeadEndChange(Duco::EBobLead, leadEnds));
            Assert::AreEqual(expectedFourthLeadEnd, leadEnds);

            Assert::IsTrue(notations.CreateNextLeadEndChange(Duco::EBobLead, leadEnds));
            Assert::AreEqual(expectedFifthLeadEnd, leadEnds);

            Assert::IsTrue(notations.CreateNextLeadEndChange(Duco::EBobLead, leadEnds));
            Assert::AreEqual(expectedSixthLeadEnd, leadEnds);

            Assert::IsTrue(notations.CreateNextLeadEndChange(Duco::EBobLead, leadEnds));
            Assert::AreEqual(expectedSeventhLeadEnd, leadEnds);

            Assert::IsTrue(notations.CreateNextLeadEndChange(Duco::EBobLead, leadEnds));
            Assert::AreEqual(expectedEighthLeadEnd, leadEnds);

            Assert::IsTrue(notations.CreateNextLeadEndChange(Duco::EBobLead, leadEnds));
            Assert::AreEqual(expectedNinthLeadEnd, leadEnds);
        }

        TEST_METHOD(PlaceNotationProcessor_BristolSurpriseMajor_SingleCourseLeadEnds)
        {
            MethodDatabase database;
            Method bristolSurpriseMajor(1, 8, L"Bristol", L"Surprise", L"&X58X14.58X58.36.14X14.58X14X18-18", false);
            bristolSurpriseMajor.SetBobPlaceNotation(L"14");

            PlaceNotationProcessor processor(bristolSurpriseMajor);
            PlaceNotation notations(bristolSurpriseMajor, 0);
            processor.ProcessNotation(notations);

            Change roundsOnEight(bristolSurpriseMajor.Order());
            Change expectedFirstLeadEnd(L"14235678");
            Change expectedSecondLeadEnd(L"13425678");
            Change expectedThirdLeadEnd(roundsOnEight);

            Duco::Lead* firstLead = notations.CreateLead(roundsOnEight, 0);
            //firstLead->SetLeadEndType();
            Change leadEnds(bristolSurpriseMajor.Order());
            Assert::IsTrue(firstLead->LeadEnd(Duco::EBobLead, leadEnds));
            Assert::AreEqual(expectedFirstLeadEnd, leadEnds);

            Assert::IsTrue(notations.CreateNextLeadEndChange(Duco::EBobLead, leadEnds));
            Assert::AreEqual(expectedSecondLeadEnd, leadEnds);

            Assert::IsTrue(notations.CreateNextLeadEndChange(Duco::EBobLead, leadEnds));
            Assert::AreEqual(expectedThirdLeadEnd, leadEnds);
        }


    };
}
