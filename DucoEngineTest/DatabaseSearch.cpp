#include <algorithm>
#include <ctime>
#include "CppUnitTest.h"

#include <Association.h>
#include <AssociationDatabase.h>
#include <UppercaseField.h>
#include <DatabaseSearch.h>
#include <DatabaseSettings.h>
#include <ImportExportProgressCallback.h>
#include <Method.h>
#include <MethodDatabase.h>
#include <Peal.h>
#include <PealDatabase.h>
#include <ProgressCallback.h>
#include <ReplaceStringField.h>
#include <RingingDatabase.h>
#include <Ringer.h>
#include <RingerDatabase.h>
#include <SearchFieldDateArgument.h>
#include <SearchFieldLinkedTowerIdArgument.h>
#include <SearchDuplicateObject.h>
#include <SearchFieldStringArgument.h>
#include <SearchFieldBoolArgument.h>
#include <SearchFieldPealFeesArgument.h>
#include <SearchFieldRingerArgument.h>
#include <SearchFieldNumberArgument.h>
#include <SearchFieldNegativeArgument.h>
#include <SearchFieldIdArgument.h>
#include <SearchValidObject.h>
#include <StatisticFilters.h>
#include <Tower.h>
#include <TowerDatabase.h>
#include <SearchGenderArgument.h>

#include "ToString.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(DatabaseSearch_UnitTests), ProgressCallback, ImportExportProgressCallback
    {
    public:
        void Initialised()
        {
        }

        void Step(int progressPercent)
        {
        }

        void Complete(bool error)
        {
        }
        //From ImportExportProgressCallback
        void InitialisingImportExport(bool internalising, unsigned int versionNumber, size_t totalNumberOfObjects)
        {
        }
        void ObjectProcessed(bool internalising, Duco::TObjectType objectType, Duco::ObjectId objectId, int totalPercentage, size_t numberInThisSubDatabase)
        {
        }
        void ImportExportComplete(bool internalising)
        {
        }
        void ImportExportFailed(bool internalising, int errorCode)
        {
            Assert::IsFalse(true);
        }

        TEST_METHOD(DatabaseSearch_DefaultConstructor)
        {
            Duco::RingingDatabase database;
            DatabaseSearch search (database);

            std::set<Duco::ObjectId> foundIds;

            search.Search(*this, foundIds, TObjectType::EMethod);
            Assert::AreEqual((size_t)0, foundIds.size());
        }

        TEST_METHOD(DatabaseSearch_OneMethod_FindName_NotMatch)
        {
            Duco::RingingDatabase database;
            DatabaseSearch search(database);

            Method newMethod(KNoId, 6, L"Plain", L"Surprise", L"");
            database.MethodsDatabase().AddObject(newMethod);

            std::set<Duco::ObjectId> foundIds;

            Duco::TSearchFieldStringArgument* newStr = new Duco::TSearchFieldStringArgument(TSearchFieldId::EMethodName, L"Bristol", true);
            search.AddArgument(newStr);
            //EMethodType
            search.Search(*this, foundIds, TObjectType::EMethod);
            Assert::AreEqual((size_t)0, foundIds.size());
        }

        TEST_METHOD(DatabaseSearch_OneMethod_FindName_Match)
        {
            Duco::RingingDatabase database;
            DatabaseSearch search(database);

            Method newMethod(KNoId, 6, L"Plain", L"Surprise", L"");
            database.MethodsDatabase().AddObject(newMethod);

            std::set<Duco::ObjectId> foundIds;

            Duco::TSearchFieldStringArgument* newStr = new Duco::TSearchFieldStringArgument(TSearchFieldId::EMethodName, L"Plain", false);
            search.AddArgument(newStr);

            search.Search(*this, foundIds, TObjectType::EMethod);
            Assert::AreEqual((size_t)1, foundIds.size());
        }

        TEST_METHOD(DatabaseSearch_OneMethod_FindFullName_Match)
        {
            Duco::RingingDatabase database;
            DatabaseSearch search(database);

            Method newMethod(KNoId, 6, L"Plain", L"Surprise", L"");
            database.MethodsDatabase().AddObject(newMethod);

            std::set<Duco::ObjectId> foundIds;

            Duco::TSearchFieldStringArgument* newStr = new Duco::TSearchFieldStringArgument(TSearchFieldId::EFullMethodName, L"n S", true);
            search.AddArgument(newStr);

            search.Search(*this, foundIds, TObjectType::EMethod);
            Assert::AreEqual((size_t)1, foundIds.size());
        }

        TEST_METHOD(DatabaseSearch_OneMethod_FindType_Match)
        {
            Duco::RingingDatabase database;
            DatabaseSearch search(database);

            Method newMethod(KNoId, 6, L"Plain", L"Surprise", L"");
            database.MethodsDatabase().AddObject(newMethod);

            std::set<Duco::ObjectId> foundIds;

            Duco::TSearchFieldStringArgument* newStr = new Duco::TSearchFieldStringArgument(TSearchFieldId::EMethodType, L"Surprise", false);
            search.AddArgument(newStr);

            search.Search(*this, foundIds, TObjectType::EMethod);
            Assert::AreEqual((size_t)1, foundIds.size());
        }

        TEST_METHOD(DatabaseSearch_TwoDuplicatePeals)
        {
            RingingDatabase database;
            Duco::StatisticFilters filters(database);
            std::string filename = "ashley_wilson - duplicates.duc";
            unsigned int versionNumber = 0;
            database.Internalise(filename.c_str(), this, versionNumber);
            Assert::AreEqual((size_t)360, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)12, database.NumberOfAssociations());
            Assert::AreEqual((size_t)114, database.NumberOfTowers());

            DatabaseSearch search(database);
            Duco::TSearchDuplicateObject* newStr = new Duco::TSearchDuplicateObject(false);
            search.AddValidationArgument(newStr);

            std::set<Duco::ObjectId> foundIds;
            search.Search(*this, foundIds, TObjectType::EPeal);
            Assert::AreEqual((size_t)4, foundIds.size());
        }

        TEST_METHOD(DatabaseSearch_ErrorsAndWarningsInPeal)
        {
            RingingDatabase database;
            std::string filename = "ashley_wilson - duplicates.duc";
            unsigned int versionNumber = 0;
            database.Internalise(filename.c_str(), this, versionNumber);
            Duco::StatisticFilters filters(database);

            Assert::AreEqual((size_t)360, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)12, database.NumberOfAssociations());
            Assert::AreEqual((size_t)114, database.NumberOfTowers());

            /*database.Settings().EnableValidationCheck(EPealWarning_AssociationMissing, TObjectType::EPeal);
            database.Settings().EnableValidationCheck(EPealWarning_AssociationBlank, TObjectType::EPeal);
            database.Settings().EnableValidationCheck(EPealWarning_SeriesMissing, TObjectType::EPeal);
            database.Settings().EnableValidationCheck(EMethodWarning_MissingBobPlaceNotation, TObjectType::EMethod);
            database.Settings().EnableValidationCheck(EMethodWarning_MissingSinglePlaceNotation, TObjectType::EMethod);*/

            DatabaseSearch search(database);
            Duco::TSearchValidObject* newStr = new Duco::TSearchValidObject(true);
            search.AddValidationArgument(newStr);

            std::set<Duco::ObjectId> foundIds;
            search.Search(*this, foundIds, TObjectType::EPeal);

            Assert::AreEqual((size_t)23, foundIds.size());
            Assert::IsTrue(foundIds.contains(ObjectId(8)));
            Assert::IsTrue(foundIds.contains(ObjectId(10)));
            Assert::IsTrue(foundIds.contains(ObjectId(25)));
            Assert::IsTrue(foundIds.contains(ObjectId(28)));
            Assert::IsTrue(foundIds.contains(ObjectId(32)));
            Assert::IsTrue(foundIds.contains(ObjectId(35)));
            Assert::IsTrue(foundIds.contains(ObjectId(56)));
            Assert::IsTrue(foundIds.contains(ObjectId(74)));
            Assert::IsTrue(foundIds.contains(ObjectId(92)));
            Assert::IsTrue(foundIds.contains(ObjectId(94)));
            Assert::IsTrue(foundIds.contains(ObjectId(97)));
            Assert::IsTrue(foundIds.contains(ObjectId(98)));
            Assert::IsTrue(foundIds.contains(ObjectId(115)));
            Assert::IsTrue(foundIds.contains(ObjectId(128)));
            Assert::IsTrue(foundIds.contains(ObjectId(180)));
            Assert::IsTrue(foundIds.contains(ObjectId(182)));
            Assert::IsTrue(foundIds.contains(ObjectId(252)));
            Assert::IsTrue(foundIds.contains(ObjectId(270)));
            Assert::IsTrue(foundIds.contains(ObjectId(274)));
            Assert::IsTrue(foundIds.contains(ObjectId(304)));
            Assert::IsTrue(foundIds.contains(ObjectId(325)));
            Assert::IsTrue(foundIds.contains(ObjectId(341)));
            Assert::IsTrue(foundIds.contains(ObjectId(355)));

            Assert::IsTrue(std::find(foundIds.begin(), foundIds.end(), Duco::ObjectId(98)) != foundIds.end());
        }

        BEGIN_TEST_METHOD_ATTRIBUTE(DatabaseSearch_ErrorsInPeal)
            TEST_PRIORITY(2)
        END_TEST_METHOD_ATTRIBUTE()
        TEST_METHOD(DatabaseSearch_ErrorsInPeal)
        {
            RingingDatabase database;
            std::string filename = "ashley_wilson - duplicates.duc";
            unsigned int versionNumber = 0;
            database.Internalise(filename.c_str(), this, versionNumber);
            Duco::StatisticFilters filters(database);

            Assert::AreEqual((size_t)360, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)12, database.NumberOfAssociations());
            Assert::AreEqual((size_t)114, database.NumberOfTowers());

            DatabaseSearch search(database);
            Duco::TSearchValidObject* newStr = new Duco::TSearchValidObject(false);
            search.AddValidationArgument(newStr);

            std::set<Duco::ObjectId> foundIds;
            search.Search(*this, foundIds, TObjectType::EPeal);
            Assert::AreEqual((size_t)4, foundIds.size());
            //97, 98, 8, 10

            Assert::IsTrue(std::find(foundIds.begin(), foundIds.end(), Duco::ObjectId(8)) != foundIds.end());
            Assert::IsTrue(std::find(foundIds.begin(), foundIds.end(), Duco::ObjectId(10)) != foundIds.end());
            Assert::IsTrue(std::find(foundIds.begin(), foundIds.end(), Duco::ObjectId(97)) != foundIds.end());
            Assert::IsTrue(std::find(foundIds.begin(), foundIds.end(), Duco::ObjectId(98)) != foundIds.end());
        }

        TEST_METHOD(DatabaseSearch_FindDuplicateAssociations)
        {
            RingingDatabase database;
            Duco::ObjectId societyId1 = database.AssociationsDatabase().CreateAssociation(L"The Dordrecht Society of Change ringers");
            Duco::ObjectId societyId2 = database.AssociationsDatabase().CreateAssociation(L"Dordrecht Society of Change ringers");
            Duco::ObjectId societyId3 = database.AssociationsDatabase().CreateAssociation(L"Dordrecht Society");
            Duco::ObjectId societyId4 = database.AssociationsDatabase().CreateAssociation(L"Dordrecht Society");

            Assert::AreEqual((size_t)4, database.AssociationsDatabase().NumberOfObjects());

            DatabaseSearch search(database);
            Duco::TSearchValidObject* newStr = new Duco::TSearchValidObject(false);
            search.AddValidationArgument(newStr);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::EAssociationOrSociety);

            Assert::AreEqual((size_t)2, objectIds.size());
        }

        TEST_METHOD(DatabaseSearch_FindDuplicateMethods)
        {
            RingingDatabase database;
            database.MethodsDatabase().CreateDefaultBellNames();
            Duco::ObjectId methodIdOne = database.MethodsDatabase().AddMethod(L"Grandsire Triples");
            Duco::ObjectId methodIdTwo = database.MethodsDatabase().AddMethod(L"Grandsire Triples");
            Duco::ObjectId methodIdThree = database.MethodsDatabase().AddMethod(L"Grandsire Cinques");
            Duco::ObjectId methodIdFour = database.MethodsDatabase().AddMethod(L"Cambridge Surprise Royal");
            Method theMethod(5, 8, L"Cambridge", L"Surprise", L"&X38X14X1258X36X14X58X16X78-12", true);
            Method theMethod2(6, 8, L"Cambridge", L"Surpise", L"&X38X14X1258X36X14X58X16X78-12", true);
            Duco::ObjectId methodIdFive = database.MethodsDatabase().AddObject(theMethod);
            Duco::ObjectId methodIdSix = database.MethodsDatabase().AddObject(theMethod2);

            Assert::AreEqual((size_t)6, database.MethodsDatabase().NumberOfObjects());

            DatabaseSearch search(database);
            Duco::TSearchDuplicateObject* newStr = new Duco::TSearchDuplicateObject(false);
            search.AddValidationArgument(newStr);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::EMethod);

            Assert::AreEqual((size_t)4, objectIds.size());
            Assert::IsTrue(std::find(objectIds.begin(), objectIds.end(), methodIdOne) != objectIds.end());
            Assert::IsTrue(std::find(objectIds.begin(), objectIds.end(), methodIdTwo) != objectIds.end());
            Assert::IsFalse(std::find(objectIds.begin(), objectIds.end(), methodIdThree) != objectIds.end());
            Assert::IsFalse(std::find(objectIds.begin(), objectIds.end(), methodIdFour) != objectIds.end());
            Assert::IsTrue(std::find(objectIds.begin(), objectIds.end(), methodIdFive) != objectIds.end());
            Assert::IsTrue(std::find(objectIds.begin(), objectIds.end(), methodIdSix) != objectIds.end());
        }

        TEST_METHOD(DatabaseSearch_FindDuplicateRingers)
        {
            RingingDatabase database;
            Duco::ObjectId idOne = database.RingersDatabase().AddRinger(L"Owen Battye");

            Ringer jenniePaul(KNoId, L"Jennie", L"Paul");
            jenniePaul.AddNameChange(L"Jennie", L"Higson", PerformanceDate(2010, 06, 03));
            Duco::ObjectId idTwo = database.RingersDatabase().AddObject(jenniePaul);
            Duco::ObjectId idThree = database.RingersDatabase().AddRinger(L"Jennie Higson");
            Duco::ObjectId idFour = database.RingersDatabase().AddRinger(L"Mark Reynolds");
            Ringer owenBattye(KNoId, L"Robert", L"Battye");
            owenBattye.SetAlsoKnownAs(L"Owen Battye");
            Duco::ObjectId idFive = database.RingersDatabase().AddObject(owenBattye);

            Assert::AreEqual((size_t)5, database.RingersDatabase().NumberOfObjects());

            DatabaseSearch search(database);
            Duco::TSearchDuplicateObject* newStr = new Duco::TSearchDuplicateObject(true);
            search.AddValidationArgument(newStr);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::ERinger);

            Assert::AreEqual((size_t)4, objectIds.size());
            Assert::IsTrue(std::find(objectIds.begin(), objectIds.end(), idOne) != objectIds.end());
            Assert::IsTrue(std::find(objectIds.begin(), objectIds.end(), idTwo) != objectIds.end());
            Assert::IsTrue(std::find(objectIds.begin(), objectIds.end(), idThree) != objectIds.end());
            Assert::IsFalse(std::find(objectIds.begin(), objectIds.end(), idFour) != objectIds.end());
            Assert::IsTrue(std::find(objectIds.begin(), objectIds.end(), idFive) != objectIds.end());
        }

        TEST_METHOD(DatabaseSearch_IgnoreDuplicateRingersWhenLinked)
        {
            RingingDatabase database;
            Duco::ObjectId idOne = database.RingersDatabase().AddRinger(L"R Owen Battye");
            Duco::ObjectId idTwo = database.RingersDatabase().AddRinger(L"Jennie Paul");
            Duco::ObjectId idThree = database.RingersDatabase().AddRinger(L"R O Battye");

            DatabaseSearch search(database);
            Duco::TSearchDuplicateObject* newStr = new Duco::TSearchDuplicateObject(true);
            search.AddValidationArgument(newStr);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::ERinger);

            Assert::AreEqual((size_t)2, objectIds.size());

            database.RingersDatabase().LinkRingers(idOne, idThree);
            search.Search(*this, objectIds, TObjectType::ERinger);

            Assert::AreEqual((size_t)0, objectIds.size());
        }

        TEST_METHOD(DatabaseSearch_FindDuplicateRingers_WarningsOff)
        {
            RingingDatabase database;
            Duco::ObjectId idOne = database.RingersDatabase().AddRinger(L"Owen Battye");

            Ringer jenniePaul(KNoId, L"Jennie", L"Paul");
            jenniePaul.AddNameChange(L"Jennie", L"Higson", PerformanceDate(2010, 06, 03));
            Duco::ObjectId idTwo = database.RingersDatabase().AddObject(jenniePaul);
            Duco::ObjectId idThree = database.RingersDatabase().AddRinger(L"Jennie Higson");
            Duco::ObjectId idFour = database.RingersDatabase().AddRinger(L"Mark Reynolds");
            Ringer owenBattye(KNoId, L"Robert", L"Battye");
            owenBattye.SetAlsoKnownAs(L"Owen Battye");
            Duco::ObjectId idFive = database.RingersDatabase().AddObject(owenBattye);

            Assert::AreEqual((size_t)5, database.RingersDatabase().NumberOfObjects());

            DatabaseSearch search(database);
            Duco::TSearchDuplicateObject* newStr = new Duco::TSearchDuplicateObject(false);
            search.AddValidationArgument(newStr);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::ERinger);

            Assert::AreEqual((size_t)0, objectIds.size());
        }


        TEST_METHOD(DatabaseSearch_FindDuplicateRingers2)
        {
            RingingDatabase database;
            Duco::ObjectId idOne = database.RingersDatabase().AddRinger(L"Peter J Eales");

            Ringer ringerTwo(KNoId, L"Jill", L"Eales");
            ringerTwo.AddNameChange(L"Jill", L"Haseldine", PerformanceDate(1981, 05, 30));
            Duco::ObjectId idTwo = database.RingersDatabase().AddObject(ringerTwo);

            Assert::AreEqual((size_t)2, database.RingersDatabase().NumberOfObjects());

            DatabaseSearch search(database);
            Duco::TSearchDuplicateObject* newStr = new Duco::TSearchDuplicateObject(true);
            search.AddValidationArgument(newStr);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::ERinger);

            Assert::AreEqual((size_t)0, objectIds.size());
        }

        TEST_METHOD(DatabaseSearch_FindDuplicateRingers3)
        {
            RingingDatabase database;
            Duco::ObjectId idOne = database.RingersDatabase().AddRinger(L"Ann White");
            Duco::ObjectId idTwo = database.RingersDatabase().AddRinger(L"Joanna L White");

            Assert::AreEqual((size_t)2, database.RingersDatabase().NumberOfObjects());

            DatabaseSearch search(database);
            Duco::TSearchDuplicateObject* newStr = new Duco::TSearchDuplicateObject(true);
            search.AddValidationArgument(newStr);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::ERinger);

            Assert::AreEqual((size_t)0, objectIds.size());
        }

        TEST_METHOD(DatabaseSearch_FindDuplicateRingers_Without_GenderSet)
        {
            RingingDatabase database;
            Ringer ringerOne(KNoId, L"Alan G", L"Kirk");
            Ringer ringerTwo(KNoId, L"Alan", L"Kirk");
            Duco::ObjectId idOne = database.RingersDatabase().AddObject(ringerOne);
            Duco::ObjectId idTwo = database.RingersDatabase().AddObject(ringerTwo);

            Assert::AreEqual((size_t)2, database.RingersDatabase().NumberOfObjects());

            DatabaseSearch search(database);
            Duco::TSearchDuplicateObject* newStr = new Duco::TSearchDuplicateObject(true);
            search.AddValidationArgument(newStr);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::ERinger);

            Assert::AreEqual((size_t)2, objectIds.size());
        }

        TEST_METHOD(DatabaseSearch_FindDuplicateRingers_With_GenderSet)
        {
            RingingDatabase database;
            Ringer ringerOne(KNoId, L"Alan G", L"Kirk");
            ringerOne.SetMale();
            Ringer ringerTwo(KNoId, L"Alan", L"Kirk");
            ringerTwo.SetMale();
            Duco::ObjectId idOne = database.RingersDatabase().AddObject(ringerOne);
            Duco::ObjectId idTwo = database.RingersDatabase().AddObject(ringerTwo);

            Assert::AreEqual((size_t)2, database.RingersDatabase().NumberOfObjects());

            DatabaseSearch search(database);
            Duco::TSearchDuplicateObject* newStr = new Duco::TSearchDuplicateObject(true);
            search.AddValidationArgument(newStr);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::ERinger);

            Assert::AreEqual((size_t)2, objectIds.size());
        }

        TEST_METHOD(DatabaseSearch_FindDeceasedRingers)
        {
            RingingDatabase database;
            Ringer ringerOne(KNoId, L"Barry E", L"Saunders");
            ringerOne.SetDeceased(true);
            Ringer ringerTwo(KNoId, L"James", L"Linell");
            ringerTwo.SetDateOfDeath(PerformanceDate(2010, 06, 03));
            Ringer ringerThree(KNoId, L"Owen", L"Battye");
            Duco::ObjectId idOne = database.RingersDatabase().AddObject(ringerOne);
            Duco::ObjectId idTwo = database.RingersDatabase().AddObject(ringerTwo);
            Duco::ObjectId idThree = database.RingersDatabase().AddObject(ringerThree);

            DatabaseSearch search(database);
            Duco::TSearchFieldBoolArgument* newStr = new Duco::TSearchFieldBoolArgument(EDeceasedRingers);
            search.AddArgument(newStr);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::ERinger);

            Assert::AreEqual((size_t)2, objectIds.size());
            Assert::IsTrue(objectIds.find(idThree) == objectIds.end());
        }

        TEST_METHOD(DatabaseSearch_FindDeceasedRingersInPeal)
        {
            RingingDatabase database;
            Ringer ringerOne(KNoId, L"Barry E", L"Saunders");
            ringerOne.SetDeceased(true);
            Ringer ringerThree(KNoId, L"Owen", L"Battye");
            Duco::ObjectId idOne = database.RingersDatabase().AddObject(ringerOne);
            Duco::ObjectId idThree = database.RingersDatabase().AddObject(ringerThree);

            Assert::AreEqual((size_t)2, database.RingersDatabase().NumberOfObjects());

            Peal pealOne;
            pealOne.SetRingerId(idOne, 1, false);
            Duco::ObjectId pealOneId = database.PealsDatabase().AddObject(pealOne);
            Peal pealTwo;
            pealTwo.SetRingerId(idThree, 1, false);
            Duco::ObjectId pealTwoId = database.PealsDatabase().AddObject(pealTwo);
            Assert::AreEqual((size_t)2, database.PealsDatabase().NumberOfObjects());

            DatabaseSearch search(database);
            Duco::TSearchFieldBoolArgument* newStr = new Duco::TSearchFieldBoolArgument(EDeceasedRingers);
            search.AddArgument(newStr);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::EPeal);

            Assert::AreEqual((size_t)1, objectIds.size());
            Assert::IsTrue(objectIds.find(pealOneId) != objectIds.end());
        }

        TEST_METHOD(DatabaseSearch_FindLinkedRingers)
        {
            RingingDatabase database;
            Ringer ringerOne(KNoId, L"Barry E", L"Saunders");
            ringerOne.SetDeceased(true);
            Ringer ringerTwo(KNoId, L"James", L"Linell");
            Ringer ringerThree(KNoId, L"Owen", L"Battye");
            Duco::ObjectId idOne = database.RingersDatabase().AddObject(ringerOne);
            Duco::ObjectId idTwo = database.RingersDatabase().AddObject(ringerTwo);
            Duco::ObjectId idThree = database.RingersDatabase().AddObject(ringerThree);
            database.RingersDatabase().LinkRingers(idTwo, idThree);

            Assert::IsFalse(database.RingersDatabase().LinkedRinger(idOne));
            Assert::IsTrue(database.RingersDatabase().LinkedRinger(idTwo));
            Assert::IsTrue(database.RingersDatabase().LinkedRinger(idThree));

            DatabaseSearch search(database);
            Duco::TSearchFieldBoolArgument* newStr = new Duco::TSearchFieldBoolArgument(ELinkedRinger);
            search.AddArgument(newStr);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::ERinger);

            Assert::AreEqual((size_t)2, objectIds.size());
            Assert::IsTrue(objectIds.find(idOne) == objectIds.end());
        }

        TEST_METHOD(DatabaseSearch_FindMaleRingers)
        {
            RingingDatabase database;
            Ringer ringerOne(KNoId, L"Jennie", L"Paul");
            ringerOne.SetFemale();
            Ringer ringerTwo(KNoId, L"Owen", L"Battye");
            ringerTwo.SetMale();
            Ringer ringerThree(KNoId, L"No", L"One");
            Duco::ObjectId idOne = database.RingersDatabase().AddObject(ringerOne);
            Duco::ObjectId idTwo = database.RingersDatabase().AddObject(ringerTwo);
            Duco::ObjectId idThree = database.RingersDatabase().AddObject(ringerThree);

            Assert::AreEqual((size_t)3, database.RingersDatabase().NumberOfObjects());

            DatabaseSearch search(database);
            Duco::TSearchGenderArgument* newStr = new Duco::TSearchGenderArgument(true, false);
            search.AddArgument(newStr);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::ERinger);

            Assert::AreEqual((size_t)1, objectIds.size());
            Assert::IsTrue(objectIds.find(idTwo) != objectIds.end());
        }

        TEST_METHOD(DatabaseSearch_FindFemaleRingers)
        {
            RingingDatabase database;
            Ringer ringerOne(KNoId, L"Jennie", L"Paul");
            ringerOne.SetFemale();
            Ringer ringerTwo(KNoId, L"Owen", L"Battye");
            ringerTwo.SetMale();
            Ringer ringerThree(KNoId, L"No", L"One");
            Duco::ObjectId idOne = database.RingersDatabase().AddObject(ringerOne);
            Duco::ObjectId idTwo = database.RingersDatabase().AddObject(ringerTwo);
            Duco::ObjectId idThree = database.RingersDatabase().AddObject(ringerThree);

            Assert::AreEqual((size_t)3, database.RingersDatabase().NumberOfObjects());

            DatabaseSearch search(database);
            Duco::TSearchGenderArgument* newStr = new Duco::TSearchGenderArgument(false, true);
            search.AddArgument(newStr);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::ERinger);

            Assert::AreEqual((size_t)1, objectIds.size());
            Assert::IsTrue(objectIds.find(idOne) != objectIds.end());
        }

        TEST_METHOD(DatabaseSearch_FindRingersWithoutGenderSet)
        {
            RingingDatabase database;
            Ringer ringerOne(KNoId, L"Jennie", L"Paul");
            ringerOne.SetFemale();
            Ringer ringerTwo(KNoId, L"Owen", L"Battye");
            ringerTwo.SetMale();
            Ringer ringerThree(KNoId, L"No", L"One");
            Duco::ObjectId idOne = database.RingersDatabase().AddObject(ringerOne);
            Duco::ObjectId idTwo = database.RingersDatabase().AddObject(ringerTwo);
            Duco::ObjectId idThree = database.RingersDatabase().AddObject(ringerThree);

            Assert::AreEqual((size_t)3, database.RingersDatabase().NumberOfObjects());

            DatabaseSearch search(database);
            Duco::TSearchGenderArgument* newStr = new Duco::TSearchGenderArgument(false, false);
            search.AddArgument(newStr);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::ERinger);

            Assert::AreEqual((size_t)1, objectIds.size());
            Assert::IsTrue(objectIds.find(idThree) != objectIds.end());
        }

        Duco::ObjectId
        CreatePeal(RingingDatabase& database, unsigned int numberOfBells, unsigned int associationId, unsigned int feePerRinger)
        {
            Peal thePeal;
            thePeal.SetAssociationId(associationId);
            for (unsigned int count = 1; count <= numberOfBells; ++count)
            {
                thePeal.SetRingerId(count, count, false);
                thePeal.SetPealFee(count, false, feePerRinger);
            }
            Duco::ObjectId pealId = database.PealsDatabase().AddObject(thePeal);
            Assert::AreEqual(feePerRinger * numberOfBells, thePeal.TotalPealFeePaid());
            return pealId;
        }

        TEST_METHOD(DatabaseSearch_InconsistantPealFees)
        {
            RingingDatabase database;
            database.MethodsDatabase().CreateDefaultBellNames();
            unsigned int associationId = 2;

            CreatePeal(database, 8, associationId, 200);
            CreatePeal(database, 10, associationId, 200);
            CreatePeal(database, 6, associationId, 200);
            CreatePeal(database, 12, associationId+1, 200);

            Peal pealOne;
            pealOne.SetAssociationId(associationId);
            pealOne.SetRingerId(1, 1, false);
            pealOne.SetPealFee(1, false, 200);
            pealOne.SetRingerId(2, 2, false);
            pealOne.SetPealFee(2, false, 200);
            pealOne.SetRingerId(3, 3, false);
            pealOne.SetPealFee(3, false, 000);
            pealOne.SetRingerId(4, 4, false);
            pealOne.SetPealFee(4, false, 100);
            pealOne.SetRingerId(5, 5, false);
            pealOne.SetPealFee(5, false, 200);
            pealOne.SetRingerId(6, 6, false);
            pealOne.SetPealFee(6, false, 200);

            Duco::ObjectId idOne = database.PealsDatabase().AddObject(pealOne);
            Assert::AreEqual(900U, pealOne.TotalPealFeePaid());

            Assert::AreEqual((size_t)5, database.PealsDatabase().NumberOfObjects());

            DatabaseSearch search(database);
            Duco::TSearchFieldPealFeesArgument* newStr = new Duco::TSearchFieldPealFeesArgument(associationId);
            search.AddArgument(newStr);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::EPeal);

            Assert::AreEqual((size_t)1, objectIds.size());
            Assert::IsTrue(std::find(objectIds.begin(), objectIds.end(), idOne) != objectIds.end());

            Peal pealTwo;
            pealTwo.SetAssociationId(associationId);
            pealTwo.SetRingerId(1, 1, false);
            pealTwo.SetRingerId(2, 2, false);
            pealTwo.SetRingerId(3, 3, false);
            pealTwo.SetRingerId(4, 4, false);
            pealTwo.SetRingerId(5, 5, false);
            pealTwo.SetRingerId(6, 6, false);
            Duco::ObjectId idTwo = database.PealsDatabase().AddObject(pealTwo);
            Assert::AreEqual(0U, pealTwo.TotalPealFeePaid());

            Assert::AreEqual((size_t)6, database.PealsDatabase().NumberOfObjects());
            search.Search(*this, objectIds, TObjectType::EPeal);
            Assert::AreEqual((size_t)2, objectIds.size());
            Assert::IsTrue(std::find(objectIds.begin(), objectIds.end(), idOne) != objectIds.end());
            Assert::IsTrue(std::find(objectIds.begin(), objectIds.end(), idTwo) != objectIds.end());

            Peal pealThree;
            pealThree.SetAssociationId(associationId);
            pealThree.SetRingerId(1, 1, false);
            pealThree.SetRingerId(2, 2, false);
            pealThree.SetRingerId(3, 3, false);
            pealThree.SetRingerId(4, 4, false);
            pealThree.SetPealFee(4, false, 1200);
            pealThree.SetRingerId(5, 5, false);
            pealThree.SetRingerId(6, 6, false);
            Duco::ObjectId idThree = database.PealsDatabase().AddObject(pealThree);
            Assert::AreEqual(1200U, pealThree.TotalPealFeePaid());
            Assert::AreEqual(Duco::ObjectId(4), pealThree.GetFeePayerId());

            Assert::AreEqual((size_t)7, database.PealsDatabase().NumberOfObjects());
            search.Search(*this, objectIds, TObjectType::EPeal);
            Assert::AreEqual((size_t)2, objectIds.size());
            Assert::IsTrue(std::find(objectIds.begin(), objectIds.end(), idOne) != objectIds.end());
            Assert::IsTrue(std::find(objectIds.begin(), objectIds.end(), idTwo) != objectIds.end());
        }

        TEST_METHOD(DatabaseSearch_PealContainsRingerById)
        {
            RingingDatabase database;
            database.MethodsDatabase().CreateDefaultBellNames();
            Duco::ObjectId ringerId = database.RingersDatabase().AddRinger(L"Owen Battye");
            unsigned int associationId = 2;

            CreatePeal(database, 8, associationId, 200);
            CreatePeal(database, 8, associationId, 200);

            DatabaseSearch search(database);
            TSearchFieldRingerPosition position (L"");
            Duco::TSearchFieldRingerArgument* newStr = new Duco::TSearchFieldRingerArgument(ringerId, position);
            search.AddArgument(newStr);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::EPeal);

            Assert::AreEqual((size_t)2, objectIds.size());
            Assert::IsTrue(std::find(objectIds.begin(), objectIds.end(), ringerId) != objectIds.end());

            Assert::AreEqual(L"Any bell", newStr->Position().c_str());
        }

        TEST_METHOD(DatabaseSearch_PealContainsRingerBySurname)
        {
            RingingDatabase database;
            database.MethodsDatabase().CreateDefaultBellNames();
            Duco::ObjectId ringerId = database.RingersDatabase().AddRinger(L"Owen Battye");
            unsigned int associationId = 2;

            CreatePeal(database, 8, associationId, 200);
            CreatePeal(database, 8, associationId, 200);

            DatabaseSearch search(database);
            TSearchFieldRingerPosition position(L"Any bell");
            Duco::TSearchFieldRingerArgument* newStr = new Duco::TSearchFieldRingerArgument(L"Battye", position);
            search.AddArgument(newStr);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::EPeal);

            Assert::AreEqual((size_t)2, objectIds.size());
            Assert::IsTrue(std::find(objectIds.begin(), objectIds.end(), ringerId) != objectIds.end());

            Assert::AreEqual(L"Any bell", newStr->Position().c_str());
        }

        TEST_METHOD(DatabaseSearch_PealContainsRingerByFullname)
        {
            RingingDatabase database;
            database.MethodsDatabase().CreateDefaultBellNames();
            Duco::ObjectId ringerId = database.RingersDatabase().AddRinger(L"Owen Battye");
            unsigned int associationId = 2;

            CreatePeal(database, 8, associationId, 200);
            CreatePeal(database, 8, associationId, 200);

            DatabaseSearch search(database);
            TSearchFieldRingerPosition position(L"1");
            Duco::TSearchFieldRingerArgument* newStr = new Duco::TSearchFieldRingerArgument(L"Battye, Owen", position);
            search.AddArgument(newStr);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::EPeal);

            Assert::AreEqual((size_t)2, objectIds.size());
            Assert::IsTrue(std::find(objectIds.begin(), objectIds.end(), ringerId) != objectIds.end());

            Assert::AreEqual(L"Treble", newStr->Position().c_str());
        }

        TEST_METHOD(DatabaseSearch_PealDoesntContainRingerByIdAndPosition)
        {
            RingingDatabase database;
            database.MethodsDatabase().CreateDefaultBellNames();
            Duco::ObjectId ringerId = database.RingersDatabase().AddRinger(L"Owen Battye");
            unsigned int associationId = 2;

            CreatePeal(database, 8, associationId, 200);
            CreatePeal(database, 8, associationId, 200);

            DatabaseSearch search(database);
            TSearchFieldRingerPosition position(L"2");
            Duco::TSearchFieldRingerArgument* newStr = new Duco::TSearchFieldRingerArgument(ringerId, position);
            search.AddArgument(newStr);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::EPeal);

            Assert::AreEqual((size_t)0, objectIds.size());
            Assert::IsFalse(std::find(objectIds.begin(), objectIds.end(), ringerId) != objectIds.end());

            Assert::AreEqual(L"2", newStr->Position().c_str());
        }

        TEST_METHOD(DatabaseSearch_MethodsNotBristolRoyal)
        {
            RingingDatabase database;
            unsigned int databaseVersion;
            database.Internalise("owen_battye_DBVer_38.duc", this, databaseVersion);

            Duco::StatisticFilters filters(database);
            Assert::AreEqual((size_t)611, database.PerformanceInfo(filters).TotalPeals());

            DatabaseSearch search(database);
            Duco::TSearchFieldIdArgument* newStr = new Duco::TSearchFieldIdArgument(TSearchFieldId::EMethodId, 54);
            Duco::TSearchFieldNegativeArgument* newArg = new Duco::TSearchFieldNegativeArgument(newStr);
            search.AddArgument(newArg);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::EPeal);

            Assert::AreEqual((size_t)516, objectIds.size());
        }

        TEST_METHOD(DatabaseSearch_PealsOnFourteenBells)
        {
            RingingDatabase database;
            unsigned int databaseVersion;
            database.Internalise("owen_battye_DBVer_38.duc", this, databaseVersion);

            Duco::StatisticFilters filters(database);
            Assert::AreEqual((size_t)611, database.PerformanceInfo(filters).TotalPeals());

            DatabaseSearch search(database);
            Duco::TSearchFieldNumberArgument* newArg = new Duco::TSearchFieldNumberArgument(TSearchFieldId::ENoOfBells, 14);
            search.AddArgument(newArg);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::EPeal);

            Assert::AreEqual((size_t)1, objectIds.size());
        }

        TEST_METHOD(DatabaseSearch_TowersLinkedToNorthamptonOldBells)
        {
            RingingDatabase database;
            unsigned int databaseVersion;
            database.Internalise("owen_battye_DBVer_38.duc", this, databaseVersion);
            Duco::StatisticFilters filters(database);
            Assert::AreEqual((size_t)611, database.PerformanceInfo(filters).TotalPeals());

            DatabaseSearch search(database);
            Duco::TSearchFieldIdArgument* newArg = new Duco::TSearchFieldIdArgument(TSearchFieldId::ELinkedTowerId, 138);
            search.AddArgument(newArg);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::ETower);

            Assert::AreEqual((size_t)1, objectIds.size());
        }

        TEST_METHOD(DatabaseSearch_PealsWith5000Changes)
        {
            RingingDatabase database;
            unsigned int databaseVersion;
            database.Internalise("owen_battye_DBVer_38.duc", this, databaseVersion);

            Duco::StatisticFilters filters(database);
            Assert::AreEqual((size_t)611, database.PerformanceInfo(filters).TotalPeals());

            DatabaseSearch search(database);
            Duco::TSearchFieldNumberArgument* newStr = new Duco::TSearchFieldNumberArgument(TSearchFieldId::EChanges, 5000);
            search.AddArgument(newStr);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::EPeal);

            Assert::AreEqual((size_t)53, objectIds.size());
        }

        TEST_METHOD(DatabaseSearch_PealsWithMoreThan5201Changes)
        {
            RingingDatabase database;
            unsigned int databaseVersion;
            database.Internalise("owen_battye_DBVer_38.duc", this, databaseVersion);

            Duco::StatisticFilters filters(database);
            Assert::AreEqual((size_t)611, database.PerformanceInfo(filters).TotalPeals());

            DatabaseSearch search(database);
            Duco::TSearchFieldNumberArgument* newStr = new Duco::TSearchFieldNumberArgument(TSearchFieldId::EChanges, 5202, TSearchType::EMoreThanOrEqualTo);
            search.AddArgument(newStr);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::EPeal);

            Assert::AreEqual((size_t)4, objectIds.size());
        }

        TEST_METHOD(DatabaseSearch_UpdateMethodName)
        {
            RingingDatabase database;
            database.MethodsDatabase().CreateDefaultBellNames();
            Duco::ObjectId methodId = database.MethodsDatabase().AddMethod(L"lower case Surprise Minor");
            Assert::IsTrue(methodId.ValidId());

            TUppercaseField* update = new TUppercaseField(TUpdateFieldId::EMethod_Name);
            TSearchFieldStringArgument* surpriseMethods = new TSearchFieldStringArgument(Duco::TSearchFieldId::EMethodType, L"surprise");

            DatabaseSearch search(database);
            search.AddArgument(surpriseMethods);
            search.AddUpdateArgument(update);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::EMethod);

            const Duco::Method* const method = database.MethodsDatabase().FindMethod(methodId);
            Assert::AreEqual(L"LOWER CASE", method->Name().c_str());
            Assert::AreEqual(L"Surprise", method->Type().c_str());
        }

        TEST_METHOD(DatabaseSearch_UpdateMethodType)
        {
            RingingDatabase database;
            database.MethodsDatabase().CreateDefaultBellNames();
            Duco::ObjectId methodId = database.MethodsDatabase().AddMethod(L"lower case Surprise Minor");
            Assert::IsTrue(methodId.ValidId());

            TUppercaseField* update = new TUppercaseField(TUpdateFieldId::EMethod_Type);

            DatabaseSearch search(database);
            search.AddUpdateArgument(update);

            std::set<Duco::ObjectId> objectIds;
            objectIds.insert(methodId);
            Assert::IsTrue(search.Update(*this, objectIds, TObjectType::EMethod, TObjectType::EMethod));

            const Duco::Method* const method = database.MethodsDatabase().FindMethod(methodId);
            Assert::AreEqual(L"lower case", method->Name().c_str());
            Assert::AreEqual(L"SURPRISE", method->Type().c_str());
        }

        TEST_METHOD(DatabaseSearch_UpdatePealComposer)
        {
            RingingDatabase database;
            database.MethodsDatabase().CreateDefaultBellNames();
            Peal thePeal;
            thePeal.SetComposer(L"lower case");
            thePeal.SetFootnotes(L"lower case");
            Duco::ObjectId pealId = database.PealsDatabase().AddObject(thePeal);
            Assert::IsTrue(pealId.ValidId());

            TUppercaseField* update = new TUppercaseField(TUpdateFieldId::EPeal_Composer);

            DatabaseSearch search(database);
            search.AddUpdateArgument(update);

            std::set<Duco::ObjectId> objectIds;
            objectIds.insert(pealId);
            Assert::IsTrue(search.Update(*this, objectIds, TObjectType::EPeal, TObjectType::EPeal));

            const Duco::Peal* const updatedPeal = database.PealsDatabase().FindPeal(pealId);
            Assert::AreEqual(L"LOWER CASE", updatedPeal->Composer().c_str());
            Assert::AreEqual(L"lower case", updatedPeal->Footnotes().c_str());
        }

        TEST_METHOD(DatabaseSearch_UpdatePealFootnotes)
        {
            RingingDatabase database;
            database.MethodsDatabase().CreateDefaultBellNames();
            Peal thePeal;
            thePeal.SetComposer(L"lower case");
            thePeal.SetFootnotes(L"lower case");
            Duco::ObjectId pealId = database.PealsDatabase().AddObject(thePeal);
            Assert::IsTrue(pealId.ValidId());

            TUppercaseField* update = new TUppercaseField(TUpdateFieldId::EPeal_Footnotes);

            DatabaseSearch search(database);
            search.AddUpdateArgument(update);

            std::set<Duco::ObjectId> objectIds;
            objectIds.insert(pealId);
            Assert::IsTrue(search.Update(*this, objectIds, TObjectType::EPeal, TObjectType::EPeal));

            const Duco::Peal* const updatedPeal = database.PealsDatabase().FindPeal(pealId);
            Assert::AreEqual(L"lower case", updatedPeal->Composer().c_str());
            Assert::AreEqual(L"LOWER CASE", updatedPeal->Footnotes().c_str());
        }

        TEST_METHOD(DatabaseSearch_UpdateTowerName)
        {
            RingingDatabase database;
            database.MethodsDatabase().CreateDefaultBellNames();
            Tower newTower;
            newTower.SetName(L"lower case");
            newTower.SetCity(L"lower case");
            Duco::ObjectId towerId = database.TowersDatabase().AddObject(newTower);
            Assert::IsTrue(towerId.ValidId());

            TUppercaseField* update = new TUppercaseField(TUpdateFieldId::ETower_Name);

            DatabaseSearch search(database);
            search.AddUpdateArgument(update);

            std::set<Duco::ObjectId> objectIds;
            objectIds.insert(towerId);
            Assert::IsTrue(search.Update(*this, objectIds, TObjectType::ETower, TObjectType::ETower));

            const Duco::Tower* const tower = database.TowersDatabase().FindTower(towerId);
            Assert::AreEqual(L"LOWER CASE", tower->Name().c_str());
            Assert::AreEqual(L"lower case", tower->City().c_str());
        }

        TEST_METHOD(DatabaseSearch_UpdateTowerCity)
        {
            RingingDatabase database;
            database.MethodsDatabase().CreateDefaultBellNames();
            Tower newTower;
            newTower.SetName(L"lower case");
            newTower.SetCity(L"lower case");
            Duco::ObjectId towerId = database.TowersDatabase().AddObject(newTower);
            Assert::IsTrue(towerId.ValidId());

            TUppercaseField* update = new TUppercaseField(TUpdateFieldId::ETower_City);

            DatabaseSearch search(database);
            search.AddUpdateArgument(update);

            std::set<Duco::ObjectId> objectIds;
            objectIds.insert(towerId);
            Assert::IsTrue(search.Update(*this, objectIds, TObjectType::ETower, TObjectType::ETower));

            const Duco::Tower* const tower = database.TowersDatabase().FindTower(towerId);
            Assert::AreEqual(L"lower case", tower->Name().c_str());
            Assert::AreEqual(L"LOWER CASE", tower->City().c_str());
        }

        TEST_METHOD(DatabaseSearch_UpdatePealMethodName)
        {
            RingingDatabase database;
            database.MethodsDatabase().CreateDefaultBellNames();
            Duco::ObjectId methodId = database.MethodsDatabase().AddMethod(L"lower case Surprise Minor");
            Assert::IsTrue(methodId.ValidId());
            Peal thePeal;
            thePeal.SetMethodId(methodId);
            Duco::ObjectId pealId = database.PealsDatabase().AddObject(thePeal);
            Assert::IsTrue(pealId.ValidId());

            TUppercaseField* update = new TUppercaseField(TUpdateFieldId::EMethod_Name);

            DatabaseSearch search(database);
            search.AddUpdateArgument(update);

            std::set<Duco::ObjectId> objectIds;
            objectIds.insert(pealId);
            Assert::IsTrue(search.Update(*this, objectIds, TObjectType::EMethod, TObjectType::EPeal));

            const Duco::Method* const updatedMethod = database.MethodsDatabase().FindMethod(methodId);
            Assert::AreEqual(L"LOWER CASE", updatedMethod->Name().c_str());
            Assert::AreEqual(L"Surprise", updatedMethod->Type().c_str());
        }

        TEST_METHOD(DatabaseSearch_ReplaceMethodName)
        {
            RingingDatabase database;
            database.MethodsDatabase().CreateDefaultBellNames();
            Duco::ObjectId methodId = database.MethodsDatabase().AddMethod(L"lower case Surprise Minor");
            Assert::IsTrue(methodId.ValidId());

            TReplaceStringField* update = new TReplaceStringField(TUpdateFieldId::EMethod_Name, false, L"lOwEr", L"UPPER");

            DatabaseSearch search(database);
            search.AddUpdateArgument(update);

            std::set<Duco::ObjectId> objectIds;
            objectIds.insert(methodId);
            Assert::IsTrue(search.Update(*this, objectIds, TObjectType::EMethod, TObjectType::EMethod));

            const Duco::Method* const updatedMethod = database.MethodsDatabase().FindMethod(methodId);
            Assert::AreEqual(L"UPPER case", updatedMethod->Name().c_str());
            Assert::AreEqual(L"Surprise", updatedMethod->Type().c_str());
        }

        TEST_METHOD(DatabaseSearch_ReplaceMethodType)
        {
            RingingDatabase database;
            database.MethodsDatabase().CreateDefaultBellNames();
            Duco::ObjectId methodId = database.MethodsDatabase().AddMethod(L"lower case Surprise Minor");
            Assert::IsTrue(methodId.ValidId());
            Duco::ObjectId methodIdNotChanged = database.MethodsDatabase().AddMethod(L"upper case surprise Minor");
            Assert::IsTrue(methodIdNotChanged.ValidId());

            TReplaceStringField* update = new TReplaceStringField(TUpdateFieldId::EMethod_Type, true, L"Surprise", L"Delight");

            DatabaseSearch search(database);
            search.AddUpdateArgument(update);

            std::set<Duco::ObjectId> objectIds;
            objectIds.insert(methodId);
            objectIds.insert(methodIdNotChanged);
            Assert::IsTrue(search.Update(*this, objectIds, TObjectType::EMethod, TObjectType::EMethod));

            const Duco::Method* const updatedMethod = database.MethodsDatabase().FindMethod(methodId);
            Assert::AreEqual(L"lower case", updatedMethod->Name().c_str());
            Assert::AreEqual(L"Delight", updatedMethod->Type().c_str());

            const Duco::Method* const notUpdatedMethod = database.MethodsDatabase().FindMethod(methodIdNotChanged);
            Assert::AreEqual(L"upper case", notUpdatedMethod->Name().c_str());
            Assert::AreEqual(L"surprise", notUpdatedMethod->Type().c_str());
        }

        TEST_METHOD(DatabaseSearch_UpdateTowerDetails)
        {
            RingingDatabase database;
            database.MethodsDatabase().CreateDefaultBellNames();
            Tower newTower;
            newTower.SetName(L"SS Peter and Paul");
            newTower.SetCity(L"Moulton");
            Duco::ObjectId towerId = database.TowersDatabase().AddObject(newTower);
            Assert::IsTrue(towerId.ValidId());

            TReplaceStringField* update = new TReplaceStringField(TUpdateFieldId::ETower_Name, true, L"SS Peter and Paul", L"St Peter and Paul");

            DatabaseSearch search(database);
            search.AddUpdateArgument(update);

            std::set<Duco::ObjectId> objectIds;
            objectIds.insert(towerId);
            Assert::IsTrue(search.Update(*this, objectIds, TObjectType::ETower, TObjectType::ETower));

            const Duco::Tower* const tower = database.TowersDatabase().FindTower(towerId);
            Assert::AreEqual(L"St Peter and Paul", tower->Name().c_str());
            Assert::AreEqual(L"Moulton", tower->City().c_str());
        }

        TEST_METHOD(DatabaseSearch_SearchForPealsWithNonHumanRingers)
        {
            RingingDatabase database;
            database.MethodsDatabase().CreateDefaultBellNames();

            Ringer newRinger (KNoId, L"Bell", L"Board");
            newRinger.SetNonHuman(true);
            ObjectId robotId = database.RingersDatabase().AddObject(newRinger);
            Assert::IsTrue(robotId.ValidId());

            Peal pealOne;
            ObjectId pealOneId = database.PealsDatabase().AddObject(pealOne);
            Peal pealTwo;
            pealTwo.SetRingerId(robotId, 10, true);
            ObjectId pealTwoId = database.PealsDatabase().AddObject(pealTwo);
            Peal pealThree;
            ObjectId pealThreeId = database.PealsDatabase().AddObject(pealThree);

            TSearchFieldBoolArgument* arg = new TSearchFieldBoolArgument(TSearchFieldId::ENonHumanRinger);

            DatabaseSearch search(database);
            search.AddArgument(arg);

            std::set<Duco::ObjectId> pealIds;
            search.Search(*this, pealIds, Duco::TObjectType::EPeal);

            Assert::AreEqual((size_t)1, pealIds.size());
            Assert::AreEqual(pealTwoId, *pealIds.begin());

        }

        TEST_METHOD(DatabaseSearch_SearchForNonHumanRingers)
        {
            RingingDatabase database;
            database.MethodsDatabase().CreateDefaultBellNames();

            Ringer newRobot(KNoId, L"Bell", L"Board");
            newRobot.SetNonHuman(true);
            ObjectId robotId = database.RingersDatabase().AddObject(newRobot);
            Assert::IsTrue(robotId.ValidId());
            Ringer newRinger(KNoId, L"Owen", L"Battye");
            ObjectId humanId = database.RingersDatabase().AddObject(newRinger);
            Assert::IsTrue(humanId.ValidId());
            TSearchFieldBoolArgument* arg = new TSearchFieldBoolArgument(TSearchFieldId::ENonHumanRinger);

            DatabaseSearch search(database);
            search.AddArgument(arg);

            std::set<Duco::ObjectId> ringerIds;
            search.Search(*this, ringerIds, Duco::TObjectType::ERinger);

            Assert::AreEqual((size_t)1, ringerIds.size());
            Assert::AreEqual(robotId, *ringerIds.begin());
        }

        TEST_METHOD(DatabaseSearch_FindPealByAssociationId)
        {
            RingingDatabase database;
            database.MethodsDatabase().CreateDefaultBellNames();
            Association newAssociation;
            Duco::ObjectId associationId = database.AssociationsDatabase().AddObject(newAssociation);
            Assert::IsTrue(associationId.ValidId());

            TSearchFieldIdArgument* arg = new TSearchFieldIdArgument(TSearchFieldId::EAssociationId, associationId);

            DatabaseSearch search(database);
            search.AddArgument(arg);

            std::set<Duco::ObjectId> associationIds;
            search.Search(*this, associationIds, Duco::TObjectType::EAssociationOrSociety);

            Assert::AreEqual((size_t)1, associationIds.size());
        }

        TEST_METHOD(DatabaseSearch_FindPealByAssociationName)
        {
            RingingDatabase database;
            database.MethodsDatabase().CreateDefaultBellNames();
            Association firstAssociation;
            firstAssociation.SetName(L"Lancashire association");
            Duco::ObjectId associationOneId = database.AssociationsDatabase().AddObject(firstAssociation);
            Assert::IsTrue(associationOneId.ValidId());
            Association secondAssociation;
            secondAssociation.SetName(L"ANCIENT SOCIETY OF COLLEGE YOUTHS");
            Duco::ObjectId associationTwoId = database.AssociationsDatabase().AddObject(secondAssociation);
            Assert::IsTrue(associationTwoId.ValidId());

            TSearchFieldStringArgument* arg = new TSearchFieldStringArgument(TSearchFieldId::EAssociationName, L"Lancashire");

            DatabaseSearch search(database);
            search.AddArgument(arg);

            std::set<Duco::ObjectId> associationIds;
            search.Search(*this, associationIds, Duco::TObjectType::EAssociationOrSociety);

            Assert::AreEqual((size_t)1, associationIds.size());
            Assert::IsTrue(*associationIds.begin() == associationOneId);
        }

        TEST_METHOD(DatabaseSearch_HandbellTowers)
        {
            RingingDatabase database;
            Tower towerBell(KNoId, L"Tower", L"Tower", L"Tower", 8);
            Tower handbellBell(KNoId, L"Hand", L"Hand", L"Hand", 6);
            handbellBell.SetHandbell(true);
            Duco::ObjectId towerId = database.TowersDatabase().AddObject(towerBell);
            Duco::ObjectId handId = database.TowersDatabase().AddObject(handbellBell);

            DatabaseSearch search(database);
            Duco::TSearchFieldBoolArgument* newArg = new Duco::TSearchFieldBoolArgument(TSearchFieldId::EHandbellTower);
            search.AddArgument(newArg);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::ETower);

            Assert::AreEqual((size_t)1, objectIds.size());
        }

        TEST_METHOD(DatabaseSearch_LinkedTowers)
        {
            RingingDatabase database;
            Tower towerOne(KNoId, L"One", L"Tower", L"Tower", 8);
            Duco::ObjectId towerOneId = database.TowersDatabase().AddObject(towerOne);
            Tower towerTwo(KNoId, L"Two", L"Tower", L"Tower", 8);
            Duco::ObjectId towerTwoId = database.TowersDatabase().AddObject(towerTwo);
            Tower towerThree(KNoId, L"Three", L"Tower", L"Tower", 8);
            towerThree.SetLinkedTowerId(towerOneId);
            Duco::ObjectId towerThreeId = database.TowersDatabase().AddObject(towerThree);

            DatabaseSearch search(database);
            Duco::TSearchFieldLinkedTowerIdArgument* newArg = new Duco::TSearchFieldLinkedTowerIdArgument(towerOneId);
            search.AddArgument(newArg);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::ETower);

            Assert::AreEqual((size_t)2, objectIds.size());
            Assert::IsFalse(objectIds.find(towerOneId) == objectIds.end());
            Assert::IsTrue(objectIds.find(towerTwoId) == objectIds.end());
            Assert::IsFalse(objectIds.find(towerThreeId) == objectIds.end());
        }

        TEST_METHOD(DatabaseSearch_PealsInLinkedTowers)
        {
            RingingDatabase database;
            Tower towerOne(KNoId, L"One", L"Tower", L"Tower", 8);
            Duco::ObjectId towerOneId = database.TowersDatabase().AddObject(towerOne);
            Tower towerTwo(KNoId, L"Two", L"Tower", L"Tower", 8);
            Duco::ObjectId towerTwoId = database.TowersDatabase().AddObject(towerTwo);
            Tower towerThree(KNoId, L"Three", L"Tower", L"Tower", 8);
            towerThree.SetLinkedTowerId(towerOneId);
            Duco::ObjectId towerThreeId = database.TowersDatabase().AddObject(towerThree);

            Peal pealOne;
            pealOne.SetTowerId(towerTwoId);
            Duco::ObjectId pealOneId = database.PealsDatabase().AddObject(pealOne);
            Peal pealTwo;
            pealTwo.SetTowerId(towerThreeId);
            Duco::ObjectId pealTwoId = database.PealsDatabase().AddObject(pealTwo);
            Peal pealThree;
            pealThree.SetTowerId(towerOneId);
            Duco::ObjectId pealThreeId = database.PealsDatabase().AddObject(pealThree);

            DatabaseSearch search(database);
            Duco::TSearchFieldLinkedTowerIdArgument* newArg = new Duco::TSearchFieldLinkedTowerIdArgument(towerOneId);
            search.AddArgument(newArg);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::EPeal);

            Assert::AreEqual((size_t)2, objectIds.size());
            Assert::IsTrue(objectIds.find(pealOneId) == objectIds.end());
            Assert::IsFalse(objectIds.find(pealTwoId) == objectIds.end());
            Assert::IsFalse(objectIds.find(pealThreeId) == objectIds.end());
        }

        TEST_METHOD(DatabaseSearch_PerformanceDates_OnSameDay)
        {
            RingingDatabase database;

            PerformanceDate dateOne(2010, 6, 1);
            PerformanceDate dateTwo(2010, 6, 2);
            PerformanceDate dateThree(2010, 6, 3);
            
            Peal pealOne;
            pealOne.SetDate(dateOne);
            Duco::ObjectId pealOneId = database.PealsDatabase().AddObject(pealOne);
            Peal pealTwo;
            pealTwo.SetDate(dateTwo);
            Duco::ObjectId pealTwoId = database.PealsDatabase().AddObject(pealTwo);
            Peal pealThree;
            pealThree.SetDate(dateThree);
            Duco::ObjectId pealThreeId = database.PealsDatabase().AddObject(pealThree);

            DatabaseSearch search(database);
            Duco::TSearchFieldDateArgument* newArg = new Duco::TSearchFieldDateArgument(Duco::EPealDate, dateTwo, TSearchType::EEqualTo);
            search.AddArgument(newArg);

            std::set<Duco::ObjectId> objectIds;

            search.Search(*this, objectIds, TObjectType::EPeal);
            Assert::AreEqual((size_t)1, objectIds.size());
            Assert::IsTrue(objectIds.find(pealTwoId) != objectIds.end());
        }

        TEST_METHOD(DatabaseSearch_PerformanceDates_AfterDay)
        {
            RingingDatabase database;

            PerformanceDate dateOne(2010, 6, 1);
            PerformanceDate dateTwo(2010, 6, 2);
            PerformanceDate dateThree(2010, 6, 3);

            Peal pealOne;
            pealOne.SetDate(dateOne);
            Duco::ObjectId pealOneId = database.PealsDatabase().AddObject(pealOne);
            Peal pealTwo;
            pealTwo.SetDate(dateTwo);
            Duco::ObjectId pealTwoId = database.PealsDatabase().AddObject(pealTwo);
            Peal pealThree;
            pealThree.SetDate(dateThree);
            Duco::ObjectId pealThreeId = database.PealsDatabase().AddObject(pealThree);

            DatabaseSearch search(database);
            Duco::TSearchFieldDateArgument* newArg = new Duco::TSearchFieldDateArgument(Duco::EPealDate, dateTwo, TSearchType::EMoreThan);
            search.AddArgument(newArg);

            std::set<Duco::ObjectId> objectIds;

            search.Search(*this, objectIds, TObjectType::EPeal);
            Assert::AreEqual((size_t)1, objectIds.size());
            Assert::IsTrue(objectIds.find(pealThreeId) != objectIds.end());
        }

        TEST_METHOD(DatabaseSearch_PerformanceDates_BeforeAndIncludingDay)
        {
            RingingDatabase database;

            PerformanceDate dateOne(2010, 6, 1);
            PerformanceDate dateTwo(2010, 6, 2);
            PerformanceDate dateThree(2010, 6, 3);

            Peal pealOne;
            pealOne.SetDate(dateOne);
            Duco::ObjectId pealOneId = database.PealsDatabase().AddObject(pealOne);
            Peal pealTwo;
            pealTwo.SetDate(dateTwo);
            Duco::ObjectId pealTwoId = database.PealsDatabase().AddObject(pealTwo);
            Peal pealThree;
            pealThree.SetDate(dateThree);
            Duco::ObjectId pealThreeId = database.PealsDatabase().AddObject(pealThree);

            DatabaseSearch search(database);
            Duco::TSearchFieldDateArgument* newArg = new Duco::TSearchFieldDateArgument(Duco::EPealDate, dateTwo, TSearchType::ELessThanOrEqualTo);
            search.AddArgument(newArg);

            std::set<Duco::ObjectId> objectIds;

            search.Search(*this, objectIds, TObjectType::EPeal);
            Assert::AreEqual((size_t)2, objectIds.size());
            Assert::IsTrue(objectIds.find(pealOneId) != objectIds.end());
            Assert::IsTrue(objectIds.find(pealTwoId) != objectIds.end());
        }

        TEST_METHOD(DatabaseSearch_WithStrapper)
        {
            RingingDatabase database;
            unsigned int databaseVersion;
            database.Internalise("owen_battye_DBVer_38.duc", this, databaseVersion);
            Duco::StatisticFilters filters(database);
            Assert::AreEqual((size_t)611, database.PerformanceInfo(filters).TotalPeals());

            DatabaseSearch search(database);
            Duco::TSearchFieldBoolArgument* newArg = new Duco::TSearchFieldBoolArgument(TSearchFieldId::EStrapper);
            search.AddArgument(newArg);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::EPeal);

            Assert::AreEqual((size_t)1, objectIds.size());
            Assert::IsTrue(objectIds.contains(589));
        }

        TEST_METHOD(DatabaseSearch_WithoutStrapper)
        {
            RingingDatabase database;
            unsigned int databaseVersion;
            database.Internalise("owen_battye_DBVer_38.duc", this, databaseVersion);
            Duco::StatisticFilters filters(database);
            Assert::AreEqual((size_t)611, database.PerformanceInfo(filters).TotalPeals());

            DatabaseSearch search(database);
            Duco::TSearchFieldBoolArgument* newArg = new Duco::TSearchFieldBoolArgument(TSearchFieldId::EStrapper, false);
            search.AddArgument(newArg);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::EPeal);

            Assert::AreEqual((size_t)610, objectIds.size());
            Assert::IsFalse(objectIds.contains(589));
        }

        TEST_METHOD(DatabaseSearch_WithStrapper_LukeMarshall)
        {
            RingingDatabase database;
            unsigned int databaseVersion;
            database.Internalise("owen_battye_DBVer_38.duc", this, databaseVersion);
            Duco::StatisticFilters filters(database);
            Assert::AreEqual((size_t)611, database.PerformanceInfo(filters).TotalPeals());
            Duco::ObjectId luke(204); // Luke D MArshall
            DatabaseSearch search(database);
            Duco::TSearchFieldIdArgument* newArg = new Duco::TSearchFieldIdArgument(TSearchFieldId::EStrapper, luke);
            search.AddArgument(newArg);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::EPeal);

            Assert::AreEqual((size_t)1, objectIds.size());
            Assert::IsTrue(objectIds.contains(589));
        }

        TEST_METHOD(DatabaseSearch_WithStrapperAshley)
        {
            RingingDatabase database;
            unsigned int databaseVersion;
            database.Internalise("Ashley Wilson_DBVer_12.duc", this, databaseVersion);
            Duco::StatisticFilters filters(database);
            Assert::AreEqual((size_t)169, database.PerformanceInfo(filters).TotalPeals());

            DatabaseSearch search(database);
            Duco::TSearchFieldBoolArgument* newArg = new Duco::TSearchFieldBoolArgument(TSearchFieldId::EStrapper);
            search.AddArgument(newArg);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::EPeal);

            Assert::AreEqual((size_t)0, objectIds.size());
        }

        TEST_METHOD(DatabaseSearch_MethodSeriesContainingMethod)
        {
            RingingDatabase database;
            unsigned int databaseVersion;
            database.Internalise("owen_battye_DBVer_38.duc", this, databaseVersion);
            Duco::StatisticFilters filters(database);
            Assert::AreEqual((size_t)611, database.PerformanceInfo(filters).TotalPeals());

            Duco::ObjectId cambridge = database.MethodsDatabase().SuggestMethod(L"Cambridge", L"Surprise", 6);
            DatabaseSearch search(database);
            Duco::TSearchFieldIdArgument* newArg = new Duco::TSearchFieldIdArgument(TSearchFieldId::EMethodId, cambridge);
            search.AddArgument(newArg);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::EMethodSeries);

            Assert::AreEqual((size_t)30, objectIds.size());
            Assert::IsTrue(objectIds.contains(10));
            Assert::IsTrue(objectIds.contains(6));
            Assert::IsFalse(objectIds.contains(5));
        }

        TEST_METHOD(DatabaseSearch_SplicedMethod)
        {
            RingingDatabase database;
            unsigned int databaseVersion;
            database.Internalise("owen_battye_DBVer_38.duc", this, databaseVersion);
            Duco::StatisticFilters filters(database);
            Assert::AreEqual((size_t)611, database.PerformanceInfo(filters).TotalPeals());

            DatabaseSearch search(database);
            Duco::TSearchFieldBoolArgument* newArg = new Duco::TSearchFieldBoolArgument(TSearchFieldId::EMethodIsSpliced);
            search.AddArgument(newArg);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::EMethod);

            Assert::AreEqual((size_t)14, objectIds.size());
            Assert::IsTrue(objectIds.contains(3));
            Assert::IsTrue(objectIds.contains(13));
            Assert::IsFalse(objectIds.contains(15));
        }

        TEST_METHOD(DatabaseSearch_ContainsRinger)
        {
            RingingDatabase database;
            unsigned int databaseVersion;
            database.Internalise("owen_battye_DBVer_38.duc", this, databaseVersion);
            Duco::StatisticFilters filters(database);
            Assert::AreEqual((size_t)611, database.PerformanceInfo(filters).TotalPeals());

            DatabaseSearch search(database);
            Duco::TSearchFieldIdArgument* newArg = new Duco::TSearchFieldIdArgument(TSearchFieldId::ERingerId, 155);
            search.AddArgument(newArg);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::EPeal);

            Assert::AreEqual((size_t)141, objectIds.size());
        }

        TEST_METHOD(DatabaseSearch_DoesNotContainsRinger)
        {
            RingingDatabase database;
            unsigned int databaseVersion;
            database.Internalise("owen_battye_DBVer_38.duc", this, databaseVersion);
            Duco::StatisticFilters filters(database);
            Assert::AreEqual((size_t)611, database.PerformanceInfo(filters).TotalPeals());

            DatabaseSearch search(database);
            Duco::TSearchFieldIdArgument* newArg = new Duco::TSearchFieldIdArgument(TSearchFieldId::ERingerId, 155, ENotEqualTo);
            search.AddArgument(newArg);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::EPeal);

            Assert::AreEqual((size_t)470, objectIds.size());
        }

        TEST_METHOD(DatabaseSearch_ContainsStrapper)
        {
            RingingDatabase database;
            unsigned int databaseVersion;
            database.Internalise("owen_battye_DBVer_38.duc", this, databaseVersion);
            Duco::StatisticFilters filters(database);
            Assert::AreEqual((size_t)611, database.PerformanceInfo(filters).TotalPeals());

            DatabaseSearch search(database);
            Duco::TSearchFieldIdArgument* newArg = new Duco::TSearchFieldIdArgument(TSearchFieldId::EStrapper, 204);
            search.AddArgument(newArg);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::EPeal);

            Assert::AreEqual((size_t)1, objectIds.size());
        }

        TEST_METHOD(DatabaseSearch_ContainsStrapper_NoResults)
        {
            RingingDatabase database;
            unsigned int databaseVersion;
            database.Internalise("owen_battye_DBVer_38.duc", this, databaseVersion);
            Duco::StatisticFilters filters(database);
            Assert::AreEqual((size_t)611, database.PerformanceInfo(filters).TotalPeals());

            DatabaseSearch search(database);
            Duco::TSearchFieldIdArgument* newArg = new Duco::TSearchFieldIdArgument(TSearchFieldId::EStrapper, 2);
            search.AddArgument(newArg);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::EPeal);

            Assert::AreEqual((size_t)0, objectIds.size());
        }

        TEST_METHOD(DatabaseSearch_RWReferenceMissing)
        {
            RingingDatabase database;
            unsigned int databaseVersion;
            database.Internalise("owen_battye_DBVer_38.duc", this, databaseVersion);

            DatabaseSearch search(database);
            Duco::TSearchFieldStringArgument* newArg = new Duco::TSearchFieldStringArgument(TSearchFieldId::EBlankRWReference, L"", false, false);
            search.AddArgument(newArg);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::EPeal);

            Assert::AreEqual((size_t)144, objectIds.size());
        }

        TEST_METHOD(DatabaseSearch_RWReferencePresent)
        {
            RingingDatabase database;
            unsigned int databaseVersion;
            database.Internalise("owen_battye_DBVer_38.duc", this, databaseVersion);

            DatabaseSearch search(database);
            Duco::TSearchFieldStringArgument* newArg = new Duco::TSearchFieldStringArgument(TSearchFieldId::EBlankRWReference, L"", false, true);
            search.AddArgument(newArg);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::EPeal);

            Assert::AreEqual((size_t)467, objectIds.size());
        }

        TEST_METHOD(DatabaseSearch_BellboardReferenceMissing)
        {
            RingingDatabase database;
            unsigned int databaseVersion;
            database.Internalise("owen_battye_DBVer_38.duc", this, databaseVersion);

            DatabaseSearch search(database);
            Duco::TSearchFieldStringArgument* newArg = new Duco::TSearchFieldStringArgument(TSearchFieldId::EBellBoardReference, L"", false, false);
            search.AddArgument(newArg);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::EPeal);

            Assert::AreEqual((size_t)0, objectIds.size());
        }

        TEST_METHOD(DatabaseSearch_BellboardReferencePresent)
        {
            RingingDatabase database;
            unsigned int databaseVersion;
            database.Internalise("owen_battye_DBVer_38.duc", this, databaseVersion);

            DatabaseSearch search(database);
            Duco::TSearchFieldStringArgument* newArg = new Duco::TSearchFieldStringArgument(TSearchFieldId::EBellBoardReference, L"", false, true);
            search.AddArgument(newArg);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::EPeal);

            Assert::AreEqual((size_t)611, objectIds.size());
        }

        TEST_METHOD(DatabaseSearch_Tower_DoveReference)
        {
            RingingDatabase database;
            Tower towerWithDoveReference(KNoId, L"Test", L"Test", L"Test", 8);
            towerWithDoveReference.SetDoveRef(L"Test");
            Duco::ObjectId towerIdWithDoveRef = database.TowersDatabase().AddObject(towerWithDoveReference);
            Tower towerWithoutDoveReference(KNoId, L"Test2", L"Test2", L"Test2", 8);
            Duco::ObjectId towerIdWithoutDoveRef = database.TowersDatabase().AddObject(towerWithoutDoveReference);

            Assert::IsTrue(towerWithDoveReference.DoveRefSet());
            Assert::IsFalse(towerWithoutDoveReference.DoveRefSet());

            DatabaseSearch search(database);
            TSearchFieldBoolArgument* blankReferenceArgument = new TSearchFieldBoolArgument(EDoveReference);
            search.AddArgument(blankReferenceArgument);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::ETower);

            Assert::AreEqual((size_t)1, objectIds.size());
            Assert::AreEqual(towerIdWithoutDoveRef, *objectIds.begin());
        }


        TEST_METHOD(DatabaseSearch_Tower_TowerbaseReference)
        {
            RingingDatabase database;
            Tower towerWithTowerbaseReference(KNoId, L"Test", L"Test", L"Test", 8);
            towerWithTowerbaseReference.SetTowerbaseId(L"Test");
            Duco::ObjectId towerIdWithtowerbaseRef = database.TowersDatabase().AddObject(towerWithTowerbaseReference);
            Tower towerWithoutTowerbaseReference(KNoId, L"Test2", L"Test2", L"Test2", 8);
            Duco::ObjectId towerIdWithouttowerBaseRef = database.TowersDatabase().AddObject(towerWithoutTowerbaseReference);

            Assert::IsTrue(towerWithTowerbaseReference.ValidTowerbaseId());
            Assert::IsFalse(towerWithoutTowerbaseReference.ValidTowerbaseId());

            DatabaseSearch search(database);
            TSearchFieldBoolArgument* blankReferenceArgument = new TSearchFieldBoolArgument(ETowerbaseId, false);
            search.AddArgument(blankReferenceArgument);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::ETower);

            Assert::AreEqual((size_t)1, objectIds.size());
            Assert::AreEqual(towerIdWithtowerbaseRef, *objectIds.begin());
        }

        TEST_METHOD(DatabaseSearch_SortTowers)
        {
            RingingDatabase database;
            //Duco::StatisticFilters filters(database);
            std::string filename = "sortdistance.duc";
            unsigned int versionNumber = 0;
            database.Internalise(filename.c_str(), this, versionNumber);
            Assert::AreEqual((size_t)5, database.NumberOfTowers());

            DatabaseSearch search(database);
            TSearchFieldBoolArgument* sortArgument = new TSearchFieldBoolArgument(ETowerContainsValidLocation);
            search.AddArgument(sortArgument);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::ETower);

            Duco::ObjectId burnleyId = database.TowersDatabase().SuggestTower(L"Burnley", 10, false);

            Assert::AreEqual((size_t)4, objectIds.size());

            std::list<Duco::ObjectId> sortedIds = search.Sort(objectIds, TObjectType::ETower, burnleyId);
            Assert::AreEqual(sortedIds.size(), objectIds.size());
            std::list<Duco::ObjectId>::const_iterator it = sortedIds.begin();
            Assert::AreEqual(*it, Duco::ObjectId(3));
            ++it;
            Assert::AreEqual(*it, Duco::ObjectId(4));
            ++it;
            Assert::AreEqual(*it, Duco::ObjectId(2));
            ++it;
            Assert::AreEqual(*it, Duco::ObjectId(1));
        }

        TEST_METHOD(DatabaseSearch_SimulatedSound)
        {
            RingingDatabase database;
            Peal newPealWithSimulatedSound;
            newPealWithSimulatedSound.SetSimulatedSound(true);
            Duco::ObjectId pealId = database.PealsDatabase().AddObject(newPealWithSimulatedSound);

            Peal newPeal;
            database.PealsDatabase().AddObject(newPeal);
            

            DatabaseSearch search(database);
            TSearchFieldBoolArgument* sortArgument = new TSearchFieldBoolArgument(ESimulatedSound);
            search.AddArgument(sortArgument);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::EPeal);

            Assert::AreEqual(objectIds.size(), (size_t)1);
            Assert::AreEqual(*objectIds.begin(), pealId);
        }

        TEST_METHOD(DatabaseSearch_RingerPealBaseReferenceSearch)
        {
            RingingDatabase database;
            Ringer withReference12345 (KNoId, L"Test1", L"Test1");
            withReference12345.SetPealbaseReference(L"12345");
            Ringer withReference55555(KNoId, L"Test2", L"Test2");
            withReference55555.SetPealbaseReference(L"55555");
            Ringer withoutReference(KNoId, L"Test4", L"Test3");

            Duco::ObjectId idRef12345 = database.RingersDatabase().AddObject(withReference12345);
            Duco::ObjectId idRef55555 = database.RingersDatabase().AddObject(withReference55555);
            Duco::ObjectId idWithoutRef = database.RingersDatabase().AddObject(withoutReference);

            DatabaseSearch search(database);
            TSearchFieldStringArgument* sortArgument = new TSearchFieldStringArgument(EPealBaseReference, L"5");
            search.AddArgument(sortArgument);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::ERinger);

            Assert::AreEqual(objectIds.size(), (size_t)2);
            Assert::IsTrue(objectIds.contains(idRef12345));
            Assert::IsTrue(objectIds.contains(idRef55555));
        }

        TEST_METHOD(DatabaseSearch_MethodName)
        {
            RingingDatabase database;
            Method cambridge(KNoId, 6, L"Cambridge", L"Surprise", L"");
            Method surfleet(KNoId, 6, L"Surfleet", L"Surprise", L"" );

            Duco::ObjectId cambridgeId = database.MethodsDatabase().AddObject(cambridge);
            Duco::ObjectId surfleetId = database.MethodsDatabase().AddObject(surfleet);

            Peal pealOne;
            pealOne.SetMethodId(surfleetId);

            Peal pealTwo;
            pealTwo.SetMethodId(cambridgeId);

            Duco::ObjectId pealOneId = database.PealsDatabase().AddObject(pealOne);
            Duco::ObjectId pealTwoId = database.PealsDatabase().AddObject(pealTwo);

            DatabaseSearch search(database);
            TSearchFieldStringArgument* sortArgument = new TSearchFieldStringArgument(EFullMethodName, L"Cambridge");
            search.AddArgument(sortArgument);

            std::set<Duco::ObjectId> objectIds;
            search.Search(*this, objectIds, TObjectType::EPeal);

            Assert::AreEqual(objectIds.size(), (size_t)1);
            Assert::IsTrue(objectIds.contains(pealTwoId));
            Assert::IsFalse(objectIds.contains(pealOneId));
        }
    };
}
