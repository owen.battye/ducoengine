#include "CppUnitTest.h"
#include <Tower.h>
#include <DoveSearch.h>
#include <DoveDatabase.h>
#include <DoveObject.h>
#include "ToString.h"
#include <RingingDatabase.h>
#include <TowerDatabase.h>
#include <Tower.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(DoveSearch_UnitTests)
    {
        Duco::RingingDatabase database;
        Duco::DoveSearch* doveSearch = new DoveSearch(database, "dove.csv", NULL, "");;

        TEST_METHOD(Dove_Moulton)
        {
            Assert::IsTrue(doveSearch->Search(L"Moulton", 12, false, false, false, true, false, false));
            Assert::AreEqual((size_t)1, doveSearch->DoveDatabase().NoOfTowers());
        }

        TEST_METHOD(Dove_12BellAntiClockwise)
        {
            Assert::IsTrue(doveSearch->Search(L"", 12, false, false, false, true, false, false, true));
            Assert::AreEqual((size_t)1, doveSearch->DoveDatabase().NoOfTowers());
        }

        TEST_METHOD(Dove_11BellAntiClockwise)
        {
            Assert::IsFalse(doveSearch->Search(L"", 11, false, false, false, true, false, false, true));
            Assert::AreEqual((size_t)0, doveSearch->DoveDatabase().NoOfTowers());
        }

        TEST_METHOD(Dove_10BellAntiClockwise)
        {
            Assert::IsTrue(doveSearch->Search(L"", 10, false, false, false, true, false, false, true));
            Assert::AreEqual((size_t)1, doveSearch->DoveDatabase().NoOfTowers());
        }

        TEST_METHOD(Dove_10OrMoreBellAntiClockwise)
        {
            Assert::IsTrue(doveSearch->Search(L"", 10, true, false, false, true, false, false, true));
            Assert::AreEqual((size_t)2, doveSearch->DoveDatabase().NoOfTowers());
        }

        TEST_METHOD(Dove_AllBellAntiClockwise)
        {
            Assert::IsTrue(doveSearch->Search(L"", 0, false, false, false, true, false, false, true));
            Assert::AreEqual((size_t)286, doveSearch->DoveDatabase().NoOfTowers());
        }

        TEST_METHOD(Dove_AllRingableBellsAntiClockwise)
        {
            Assert::IsTrue(doveSearch->Search(L"", 0, false, false, false, true, false, true, true));
            Assert::AreEqual((size_t)168, doveSearch->DoveDatabase().NoOfTowers());
        }
    };
}
