#include "CppUnitTest.h"
#include <LeadOrder.h>
#include <PlaceNotation.h>
#include <Method.h>
#include "ToString.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(LeadOrder_UnitTests)
    {
    public:
        TEST_METHOD(LeadOrder_PlainBobMinor)
        {
            Method plainBobMinor(1, 6, L"Plain", L"Bob", L"&x16x16x16-12", false);

            PlaceNotation notation(plainBobMinor, 6);
            Assert::AreEqual((unsigned int)6, notation.LeadOrder().HomePosition());
            Assert::AreEqual((unsigned int)5, notation.LeadOrder().WrongPosition());
            Assert::AreEqual((unsigned int)4, notation.LeadOrder().MiddlePosition());
            Assert::AreEqual((size_t)12, notation.ChangesPerLead());
            Assert::IsFalse(notation.LeadOrder().IncludeTreble());
            Assert::IsFalse(notation.LeadOrder().IsPrinciple());

            Assert::AreEqual((size_t)5, notation.LeadOrder().Size());
            std::vector<unsigned int> leadOrder = { 5, 3, 2, 4, 6 };
            Assert::AreEqual(leadOrder, notation.LeadOrder().Leads());
        }

        TEST_METHOD(LeadOrder_CambridgeMinor)
        {
            Method cambridgeMinor(1, 6, L"Cambridge", L"Surprise", L"&X36X14X12X36X14X56-12", false);

            PlaceNotation notation(cambridgeMinor, 6);
            Assert::AreEqual((size_t)5, notation.LeadOrder().Size());
            Assert::AreEqual((unsigned int)6, notation.LeadOrder().HomePosition());
            Assert::AreEqual((unsigned int)5, notation.LeadOrder().WrongPosition());
            Assert::AreEqual((unsigned int)4, notation.LeadOrder().MiddlePosition());
            Assert::AreEqual((size_t)24, notation.ChangesPerLead());
            Assert::IsFalse(notation.LeadOrder().IncludeTreble());
            Assert::IsFalse(notation.LeadOrder().IsPrinciple());
            Assert::IsTrue(notation.LeadOrder().ValidLeadOrder(5));
            Assert::IsFalse(notation.LeadOrder().ValidLeadOrder(6));

            std::vector<unsigned int> leadOrder = { 3,4,5,2,6 };
            Assert::AreEqual(leadOrder, notation.LeadOrder().Leads());
        }

        TEST_METHOD(LeadOrder_PlainBobMajor)
        {
            Method plainBobMajor(1, 8, L"Plain", L"Bob", L"&X18X18X18X18-12", false);

            PlaceNotation notation(plainBobMajor, 8);
            Assert::AreEqual((unsigned int)8, notation.LeadOrder().HomePosition());
            Assert::AreEqual((unsigned int)7, notation.LeadOrder().WrongPosition());
            Assert::AreEqual((unsigned int)6, notation.LeadOrder().MiddlePosition());
            Assert::AreEqual((size_t)16, notation.ChangesPerLead());
            Assert::IsFalse(notation.LeadOrder().IncludeTreble());
            Assert::IsFalse(notation.LeadOrder().IsPrinciple());
            Assert::IsTrue(notation.LeadOrder().ValidLeadOrder(7));
            Assert::IsFalse(notation.LeadOrder().ValidLeadOrder(8));

            Assert::AreEqual((size_t)7, notation.LeadOrder().Size());
            std::vector<unsigned int> leadOrder = { 7,5,3,2,4,6,8 };
            Assert::AreEqual(leadOrder, notation.LeadOrder().Leads());
        }

        TEST_METHOD(LeadOrder_BristolMaximus)
        {
            Method bristolMax(1, 12, L"Bristol", L"Surprise", L"&X5TX14.5TX5T.36.14X7T.58.16X9T.70.18X18.9TX18X1T-1T", false);

            PlaceNotation notation(bristolMax, 12);
            Assert::AreEqual((size_t)11, notation.LeadOrder().Size());
            Assert::AreEqual((unsigned int)12, notation.LeadOrder().HomePosition());
            Assert::AreEqual((unsigned int)11, notation.LeadOrder().WrongPosition());
            Assert::AreEqual((unsigned int)10, notation.LeadOrder().MiddlePosition());
            Assert::AreEqual((size_t)48, notation.ChangesPerLead());
            Assert::IsFalse(notation.LeadOrder().IncludeTreble());
            Assert::IsFalse(notation.LeadOrder().IsPrinciple());
            Assert::IsTrue(notation.LeadOrder().ValidLeadOrder(11));
            Assert::IsFalse(notation.LeadOrder().ValidLeadOrder(12));

            std::vector<unsigned int> leadOrder = { 7,2,8,11,5,4,10,9,3,6,12 };
            Assert::AreEqual(leadOrder, notation.LeadOrder().Leads());
        }

        TEST_METHOD(LeadOrder_StedmanCaters)
        {
            Method stedmanCaters(1, 9, L"Stedman", L"", L"3.1.9.3.1.3.1.3.9.1.3.1", false);

            PlaceNotation notation(stedmanCaters, 9);
            Assert::AreEqual((size_t)9, notation.LeadOrder().Size());
            //Assert::AreEqual((unsigned int)12, notation.LeadOrder().HomePosition());
            //Assert::AreEqual((unsigned int)11, notation.LeadOrder().WrongPosition());
            //Assert::AreEqual((unsigned int)10, notation.LeadOrder().MiddlePosition());
            Assert::IsTrue(notation.LeadOrder().IncludeTreble());
            Assert::IsTrue(notation.LeadOrder().IsPrinciple());
            Assert::IsTrue(notation.LeadOrder().ValidLeadOrder(9));
            //Assert::IsFalse(notation.LeadOrder().ValidLeadOrder(8));

            std::vector<unsigned int> leadOrder = { 6,1,7,8,4,3,2,5,9 };
            Assert::AreEqual(leadOrder, notation.LeadOrder().Leads());
        }
    };
}
