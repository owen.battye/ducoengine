@echo off

if ["%DevEnvDir%"] == [""] (
    IF EXIST "C:\\Program Files\\Microsoft Visual Studio\\2022\\Enterprise\\Common7\\Tools\\VsDevCmd.bat" (
        call "C:\\Program Files\\Microsoft Visual Studio\\2022\\Enterprise\\Common7\\Tools\\VsDevCmd.bat"
    ) ELSE (
        IF EXIST "C:\\Program Files (x86)\\Microsoft Visual Studio\\2019\\Professional\\Common7\\Tools\\VsDevCmd.bat" (
            call "C:\\Program Files (x86)\\Microsoft Visual Studio\\2019\\Professional\\Common7\\Tools\\VsDevCmd.bat"
        ) ELSE (
            echo Cannot find Visual studio setup script.
            exit /b 1
        )
    )
)

call nuget.exe restore || exit /b 2

call msbuild DucoEngine.sln /property:Configuration=Release /property:Platform=x64 || exit /b 3
call msbuild DucoEngine.sln /property:Configuration=Debug /property:Platform=x64 || exit /b 4
