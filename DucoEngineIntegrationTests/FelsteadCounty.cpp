#include "CppUnitTest.h"

#include <FelsteadCounty.h>
#include <RingingDatabase.h>
#include <DoveDatabase.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

#define DOVEFILENAME "dove.csv"
#define LANCASHIRECOUNTYID L"59"
#define NORTHAMPTONSHIRECOUNTYID L"83"

namespace DucoEngineTest
{
    TEST_CLASS(FelsteadCounty_UnitTests)
    {
    public:
        TEST_METHOD(FelsteadCounty_Construction)
        {
            RingingDatabase database;
            FelsteadCounty parser(database, LANCASHIRECOUNTYID, L"Lancashire");
            Assert::IsTrue(parser.StartDownload());
            Assert::AreEqual((size_t)0, parser.NumberOfTowers());
        }


        TEST_METHOD(FelsteadCounty_Lancashire_FromFile)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            FelsteadCounty parser(database, LANCASHIRECOUNTYID, L"Lancashire");
            parser.Process(L"felstead_county_lancashire.html");

            Assert::AreEqual((size_t)198, parser.NumberOfTowers());

            DoveDatabase doveDatabase(DOVEFILENAME, NULL, "", true);
            doveDatabase.Import();
            Assert::IsTrue(parser.ResolveTowers(doveDatabase));

            Assert::AreEqual(parser.NumberOfTowers(), database.NumberOfTowers());
        }

        TEST_METHOD(FelsteadCounty_Lancashire_Download)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            FelsteadCounty parser(database, LANCASHIRECOUNTYID, L"Lancashire");
            Assert::IsTrue(parser.StartDownload());

            parser.Wait();
            Assert::AreEqual((size_t)198, parser.NumberOfTowers());

            DoveDatabase doveDatabase(DOVEFILENAME, NULL, "", true);
            doveDatabase.Import();
            Assert::IsTrue(parser.ResolveTowers(doveDatabase));

            Assert::AreEqual(parser.NumberOfTowers(), database.NumberOfTowers());
            Assert::IsTrue(database.Externalise("Lancashire_towers_inc_removed.duc", NULL));
        }

        TEST_METHOD(FelsteadCounty_Northamptonshire_Download)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            FelsteadCounty parser(database, NORTHAMPTONSHIRECOUNTYID, L"Northamptonshire");
            Assert::IsTrue(parser.StartDownload());

            parser.Wait();
            Assert::AreEqual((size_t)214, parser.NumberOfTowers()); // new tower Draughton

            DoveDatabase doveDatabase(DOVEFILENAME, NULL, "", true);
            doveDatabase.Import();
            Assert::IsTrue(parser.ResolveTowers(doveDatabase));

            Assert::AreEqual(parser.NumberOfTowers(), database.NumberOfTowers());

            Assert::IsTrue(database.Externalise("Northamptonshire_towers_inc_removed.duc", NULL));
        }
    };

}
