#include "CppUnitTest.h"

#include <FelsteadPealCount.h>
#include <RingingDatabase.h>
#include <DatabaseSettings.h>
#include <StatisticFilters.h>
#include <TowerDatabase.h>
#include <Tower.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(FelsteadPealCount_UnitTests)
    {
    public:
        TEST_METHOD(FelsteadPealCount_BasicTest_Hulme)
        {
            Tower theTower;
            DatabaseSettings settings;
            FelsteadPealCount parser (settings, theTower);
            parser.ImportFile("felstead_tower_7108.html");
            Assert::AreEqual((size_t)3, parser.NumberOfPeals());
        }

        TEST_METHOD(FelsteadPealCount_DownloadFile_TowerThatDoesntExist)
        {
            Tower theTower;
            theTower.SetTowerbaseId(L"1234567890");

            RingingDatabase database;
            FelsteadPealCount parser(database.Settings(), theTower);
            parser.Download();

            Assert::AreEqual((size_t)-1, parser.NumberOfPeals());
        }

        TEST_METHOD(FelsteadPealCount_CheckDatabaseWithTwoTowers)
        {
            RingingDatabase database;

            Tower harpole;
            harpole.SetTowerbaseId(L"2296");
            harpole.SetLatitude(L"52.24254");
            harpole.SetLongitude(L"-0.99011");
            Duco::ObjectId harpoleId = database.TowersDatabase().AddObject(harpole);
            Tower moulton;
            moulton.SetTowerbaseId(L"3475");
            moulton.SetLatitude(L"52.29034");
            moulton.SetLongitude(L"-0.85278");
            Duco::ObjectId moultonId = database.TowersDatabase().AddObject(moulton);

            std::map<Duco::ObjectId, Duco::PealLengthInfo> sortedTowerIds;
            Duco::StatisticFilters filters (database);
            database.TowersDatabase().GetFelsteadPealCount(sortedTowerIds, database.Settings(), filters);

            Assert::AreEqual((size_t)2, sortedTowerIds.size());
            Assert::AreEqual((size_t)41, sortedTowerIds.find(harpoleId)->second.TotalPeals());
            Assert::IsTrue(sortedTowerIds.find(moultonId)->second.TotalPeals() > 491);
        }

        TEST_METHOD(FelsteadPealCount_CheckDatabaseWithTwoTowers_PositionNotSet)
        {
            RingingDatabase database;

            Tower harpole;
            harpole.SetTowerbaseId(L"2296");
            Duco::ObjectId harpoleId = database.TowersDatabase().AddObject(harpole);
            Tower moulton;
            moulton.SetTowerbaseId(L"3475");
            Duco::ObjectId moultonId = database.TowersDatabase().AddObject(moulton);

            std::map<Duco::ObjectId, Duco::PealLengthInfo> sortedTowerIds;
            Duco::StatisticFilters filters(database);
            database.TowersDatabase().GetFelsteadPealCount(sortedTowerIds, database.Settings(), filters);

            Assert::AreEqual((size_t)0, sortedTowerIds.size());
        }

        TEST_METHOD(FelsteadPealCount_DuplicateFelsteadId)
        {
            RingingDatabase database;

            Tower townHall;
            townHall.SetTowerbaseId(L"3247");
            townHall.SetLongitude(L" -2.24458");
            townHall.SetLatitude(L" 53.47929");
            Duco::ObjectId townHallId = database.TowersDatabase().AddObject(townHall);
            Tower oldTownHall;
            oldTownHall.SetTowerbaseId(L"3247");
            oldTownHall.SetLongitude(L" -2.24458");
            oldTownHall.SetLatitude(L" 53.47929");
            oldTownHall.SetLinkedTowerId(townHallId);
            Duco::ObjectId oldTownHallId = database.TowersDatabase().AddObject(oldTownHall);

            std::map<Duco::ObjectId, Duco::PealLengthInfo> sortedTowerIds;
            Duco::StatisticFilters filters(database);
            database.TowersDatabase().GetFelsteadPealCount(sortedTowerIds, database.Settings(), filters);

            Assert::AreEqual((size_t)2, sortedTowerIds.size());
            Assert::AreEqual((size_t)234, sortedTowerIds[townHallId].TotalPeals());
            Assert::AreEqual((size_t)234, sortedTowerIds[oldTownHallId].TotalPeals());

            database.TowersDatabase().MergeTowersWithSameLocation(sortedTowerIds);

            Assert::AreEqual((size_t)1, sortedTowerIds.size());
            Assert::AreEqual((size_t)468, sortedTowerIds[townHallId].TotalPeals());

        }

        TEST_METHOD(FelsteadPealCount_NoFelsteadId)
        {
            RingingDatabase database;

            Tower noWhere;
            Duco::ObjectId noWhereId = database.TowersDatabase().AddObject(noWhere);

            std::map<Duco::ObjectId, Duco::PealLengthInfo> sortedTowerIds;
            Duco::StatisticFilters filters(database);
            database.TowersDatabase().GetFelsteadPealCount(sortedTowerIds, database.Settings(), filters);
            Assert::AreEqual((size_t)0, sortedTowerIds.size());
            Assert::AreEqual((size_t)0, sortedTowerIds[noWhereId].TotalPeals());
        }
   };
}