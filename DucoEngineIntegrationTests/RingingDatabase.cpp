﻿#include "CppUnitTest.h"
#include <Association.h>
#include <AssociationDatabase.h>
#include <PealDatabase.h>
#include <MethodDatabase.h>
#include <Tower.h>
#include <TowerDatabase.h>
#include <RingingDatabase.h>
#include <RingerDatabase.h>
#include "..\DucoEngineTest\ToString.h"
#include <ImportExportProgressCallback.h>
#include <fstream>
#include <ProgressCallback.h>
#include <BellboardPerformanceParser.h>
#include <BellboardPerformanceParserCallback.h>


using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{
    TEST_CLASS(RingingDatabase_UnitTests), Duco::ImportExportProgressCallback, Duco::ProgressCallback, Duco::BellboardPerformanceParserCallback
    {
    public:
        std::wstring mapBoxStaticKey = L"pk.eyJ1IjoiYmVsbHJpbmdlciIsImEiOiJjbDY0MXFreTMwdXM0M2txazR6emZ1NWFkIn0.zMVsJCK0GZGPcjb-kH0YvA";

        //From ImportExportProgressCallback
        void InitialisingImportExport(bool internalising, unsigned int versionNumber, size_t totalNumberOfObjects)
        {

        }
        void ObjectProcessed(bool internalising, Duco::TObjectType objectType, Duco::ObjectId objectId, int totalPercentage, size_t numberInThisSubDatabase)
        {

        }
        void ImportExportComplete(bool internalising)
        {

        }
        void ImportExportFailed(bool internalising, int errorCode)
        {
            Assert::IsFalse(true);
        }
        //ProgressCallback
        void Initialised()
        {

        }
        void Step(int progressPercent)
        {

        }
        void Complete(bool error)
        {

        }
        // From BellboardPerformanceParserCallback
        void Completed(const std::wstring& bellboardId, bool errors)
        {
            Assert::IsFalse(errors);
        }
        void Cancelled(const std::wstring& bellboardId, const char* message)
        {
            Assert::IsFalse(true);
        }

        TEST_METHOD(RingingDatabase_BellboardReferenceUpdate_Update)
        {
            RingingDatabase database;

            Tower salford(KNoId, L"Sacred Trinity", L"Salford", L"Greater Manchester", 8);
            salford.SetTowerbaseId(L"4200");
            salford.AddDefaultRing(L"Eight bell peal", L"11-2-17");
            Duco::ObjectId salfordId = database.TowersDatabase().AddObject(salford);

            Peal thePeal;
            thePeal.SetTowerId(salfordId);
            thePeal.SetBellBoardId(L"1628002");

            Duco::ObjectId pealId = database.PealsDatabase().AddObject(thePeal);

            Assert::AreEqual(L"1628002", thePeal.BellBoardId().c_str());
            Assert::AreEqual(L"", thePeal.RingingWorldReference().c_str());

            bool cancellationFlag;
            Assert::AreEqual((size_t)1, database.CheckBellBoardForUpdatedReferences(*this, false, cancellationFlag));

            thePeal = *database.PealsDatabase().FindPeal(pealId);

            Assert::AreEqual(L"1628003", thePeal.BellBoardId().c_str());
            Assert::AreEqual(L"4356.1044", thePeal.RingingWorldReference().c_str());
        }

        TEST_METHOD(RingingDatabase_BellboardReferenceUpdate_NoUpdate)
        {
            RingingDatabase database;

            Tower salford(KNoId, L"Sacred Trinity", L"Salford", L"Greater Manchester", 8);
            salford.SetTowerbaseId(L"4200");
            salford.AddDefaultRing(L"Eight bell peal", L"11-2-17");
            Duco::ObjectId salfordId = database.TowersDatabase().AddObject(salford);

            Peal thePeal;
            thePeal.SetTowerId(salfordId);
            thePeal.SetBellBoardId(L"1627914");
            thePeal.SetRingingWorldReference(L"4610.0847");

            Duco::ObjectId pealId = database.PealsDatabase().AddObject(thePeal);

            Assert::AreEqual(L"1627914", thePeal.BellBoardId().c_str());
            Assert::AreEqual(L"4610.0847", thePeal.RingingWorldReference().c_str());

            bool cancellationFlag;
            Assert::AreEqual((size_t)0, database.CheckBellBoardForUpdatedReferences(*this, false, cancellationFlag));

            thePeal = *database.PealsDatabase().FindPeal(pealId);

            Assert::AreEqual(L"1627914", thePeal.BellBoardId().c_str());
            Assert::AreEqual(L"4610.0847", thePeal.RingingWorldReference().c_str());
        }

        TEST_METHOD(RingingDatabase_BellboardReferenceUpdate_OnlyThoseWithoutRWPages)
        {
            RingingDatabase database;

            Tower salford(KNoId, L"Sacred Trinity", L"Salford", L"Greater Manchester", 8);
            salford.SetTowerbaseId(L"4200");
            salford.AddDefaultRing(L"Eight bell peal", L"11-2-17");
            Duco::ObjectId salfordId = database.TowersDatabase().AddObject(salford);

            Peal pealOne;
            pealOne.SetTowerId(salfordId);
            pealOne.SetBellBoardId(L"1627914");
            pealOne.SetRingingWorldReference(L"4610.0847");
            Duco::ObjectId pealOneId = database.PealsDatabase().AddObject(pealOne);

            Peal pealTwo;
            pealTwo.SetTowerId(salfordId);
            pealTwo.SetBellBoardId(L"1229491");
            Duco::ObjectId pealTwoId = database.PealsDatabase().AddObject(pealTwo);

            bool cancellationFlag;
            Assert::AreEqual((size_t)1, database.CheckBellBoardForUpdatedReferences(*this, true, cancellationFlag));

            pealOne = *database.PealsDatabase().FindPeal(pealOneId);
            Assert::AreEqual(L"1627914", pealOne.BellBoardId().c_str());
            Assert::AreEqual(L"4610.0847", pealOne.RingingWorldReference().c_str());

            pealTwo = *database.PealsDatabase().FindPeal(pealTwoId);
            Assert::AreEqual(L"1229491", pealTwo.BellBoardId().c_str());
            Assert::AreEqual(L"5586.476", pealTwo.RingingWorldReference().c_str());
        }

        TEST_METHOD(RingingDatabase_BellboardReferenceUpdate_NonExistenceReference)
        {
            RingingDatabase database;

            Tower salford(KNoId, L"Sacred Trinity", L"Salford", L"Greater Manchester", 8);
            salford.SetTowerbaseId(L"4200");
            salford.AddDefaultRing(L"Eight bell peal", L"11-2-17");
            Duco::ObjectId salfordId = database.TowersDatabase().AddObject(salford);

            Peal thePeal;
            thePeal.SetTowerId(salfordId);
            thePeal.SetBellBoardId(L"999999");

            Duco::ObjectId pealId = database.PealsDatabase().AddObject(thePeal);

            Assert::AreEqual(L"999999", thePeal.BellBoardId().c_str());
            Assert::AreEqual(L"", thePeal.RingingWorldReference().c_str());

            bool cancellationFlag;
            Assert::AreEqual((size_t)0, database.CheckBellBoardForUpdatedReferences(*this, false, cancellationFlag));

            thePeal = *database.PealsDatabase().FindPeal(pealId);

            Assert::AreEqual(L"999999", thePeal.BellBoardId().c_str());
            Assert::AreEqual(L"", thePeal.RingingWorldReference().c_str());
        }

        TEST_METHOD(RingingDatabase_BellBoardUpdates_NoPeals)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            bool cancellationFlag;
            Assert::AreEqual((size_t)0, database.CheckBellBoardForUpdatedReferences(*this, false, cancellationFlag));
        }

        TEST_METHOD(RingingDatabase_BellBoardMarkedAsDuplicate)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            Tower burnley(KNoId, L"Burnley", L"Lancashire", L"St Peter", 10);
            burnley.SetTowerbaseId(L"863");
            burnley.AddDefaultRing(L"Ten bell peal", L"18-0-8");
            Duco::ObjectId burnleyId = database.TowersDatabase().AddObject(burnley);

            Peal thePeal;
            thePeal.SetTowerId(burnleyId);
            thePeal.SetBellBoardId(L"1655485");
            Duco::ObjectId pealId = database.PealsDatabase().AddObject(thePeal);

            bool cancellationFlag;
            Assert::AreEqual((size_t)1, database.CheckBellBoardForUpdatedReferences(*this, false, cancellationFlag));

            Peal updatedPeal = *database.PealsDatabase().FindPeal(pealId);

            Assert::AreEqual(L"", updatedPeal.BellBoardId().c_str());

        }

        /*TEST_METHOD(RingingDatabase_BellboardReferenceUpdate_BigDatabase) // comment this one out!
        {
            RingingDatabase database;
            unsigned int databaseVer;
            database.Internalise("manchester_other_towers.duc", this, databaseVer);

            bool cancellationFlag;
            Assert::AreEqual((size_t)37, database.CheckBellBoardForUpdatedReferences(*this, false, cancellationFlag));
        }*/

        TEST_METHOD(RingingDatabase_Download1357149_CheckXML)
        {
            std::wstring bellboardId = L"1357149";
            std::wstring updatedBellboardId = L"1801827";
            RingingDatabase database;
            database.ClearDatabase(false, true);
            database.RingersDatabase().AddRinger(L"R Owen", L"Battye");
            database.RingersDatabase().AddRinger(L"Raymond A", L"Vickers");
            database.RingersDatabase().AddRinger(L"Jennie L", L"Care");
            database.RingersDatabase().AddRinger(L"Barry R", L"Care");
            database.RingersDatabase().AddRinger(L"Richard", L"Haseldine");
            database.RingersDatabase().AddRinger(L"W Eric", L"Critchley");
            database.RingersDatabase().AddRinger(L"James", L"Linnell");
            database.RingersDatabase().AddRinger(L"Charles E", L"Truman");
            database.RingersDatabase().AddRinger(L"Graham C", L"Paul");
            database.RingersDatabase().AddRinger(L"Robert H", L"Eady");

            database.MethodsDatabase().AddMethod(L"Grandsire Caters");
            Tower moulton(KNoId, L"SS Peter and Paul", L"Moulton", L"Northamptonshire", 10);
            moulton.AddDefaultRing(L"10 bells", L"11-1-14", L"A♭");
            moulton.SetTowerbaseId(L"3475");
            Assert::IsTrue(database.TowersDatabase().AddObject(moulton).ValidId());
            Association southNorthants(KNoId, L"South Northamptonshire Society", L"");
            Duco::ObjectId perfId = database.AssociationsDatabase().AddObject(southNorthants);
            Assert::IsTrue(perfId.ValidId());

            Duco::BellboardPerformanceParser importer(database, *this, NULL);
            Assert::IsTrue(importer.DownloadPerformance(bellboardId));

            Assert::AreEqual(updatedBellboardId, importer.CurrentPerformance().BellBoardId());
            Assert::IsTrue(database.PealsDatabase().AddObject(importer.CurrentPerformance()).ValidId());

            std::set<Duco::ObjectId> performanceIds;
            performanceIds.insert(perfId);
            database.ExternaliseToBellboardXml("test.xml", performanceIds, this);

            std::ifstream in("test.xml", std::ifstream::ate | std::ifstream::binary);
            Assert::AreEqual(1408ul, (unsigned long)in.tellg());
        }
    };
}
