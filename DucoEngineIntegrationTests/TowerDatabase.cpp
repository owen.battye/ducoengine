﻿#include "CppUnitTest.h"
#include <Tower.h>
#include <TowerDatabase.h>
#include <PealDatabase.h>
#include <RingingDatabase.h>
#include "..\DucoEngineTest\ToString.h"
#include <RenumberProgressCallback.h>
#include <ImportExportProgressCallback.h>
#include <StatisticFilters.h>
#include <DucoEngineUtils.h>
#include <Peal.h>
#include "ProgressCallback.h"
#include <chrono>
#include "DucoEngineLog.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;
using namespace std::chrono;

namespace DucoEngineTest
{
    TEST_CLASS(TowerDatabase_UnitTests), Duco::ImportExportProgressCallback
    {
    protected:
        RenumberProgressCallback::TRenumberStage lastStage;

    public:
        // From ImportExportProgressCallback
        void InitialisingImportExport(bool internalising, unsigned int versionNumber, size_t totalNumberOfObjects)
        {
        }
        void ObjectProcessed(bool internalising, Duco::TObjectType objectType, Duco::ObjectId objectId, int totalPercentage, size_t numberInThisSubDatabase)
        {
        }
        void ImportExportComplete(bool internalising)
        {
        }
        void ImportExportFailed(bool internalising, int errorCode)
        {
            Assert::AreEqual(0, errorCode);
        }

        TEST_METHOD(TowerDatabase_ThreadedFelsteadDownload)
        {
            RingingDatabase database;
            database.ClearDatabase(false, true);

            const std::string tempFilename = "all_isle_of_wight.duc";
            unsigned int dbVer;
            database.Internalise(tempFilename.c_str(), this, dbVer);

            Assert::AreEqual(44u, dbVer);
            size_t noOfLinkedTowers;
            Assert::AreEqual((size_t)14, database.NumberOfActiveTowers(noOfLinkedTowers));
            Assert::AreEqual((size_t)0, noOfLinkedTowers);

            std::map<Duco::ObjectId, Duco::PealLengthInfo> sortedTowerIds;
            Duco::StatisticFilters filters (database);
            database.TowersDatabase().GetFelsteadPealCount(sortedTowerIds, database.Settings(), filters, false);

            Assert::AreEqual((size_t)14, sortedTowerIds.size());

            Assert::AreEqual((size_t)57, sortedTowerIds.find(1)->second.TotalPeals());
            Assert::AreEqual((size_t)48, sortedTowerIds.find(7)->second.TotalPeals());
            Assert::AreEqual((size_t)54, sortedTowerIds.find(11)->second.TotalPeals());
        }

    };
}
