﻿#include "CppUnitTest.h"

#include <Association.h>
#include <AssociationDatabase.h>
#include <BellboardPerformanceParser.h>
#include <BellboardPerformanceParserCallback.h>
#include <DownloadedPeal.h>
#include <DucoEngineUtils.h>
#include <DucoConfiguration.h>
#include <ImportExportProgressCallback.h>
#include <MethodDatabase.h>
#include <Method.h>
#include <PealDatabase.h>
#include <RenumberProgressCallback.h>
#include <RingingDatabase.h>
#include <RingerDatabase.h>
#include <Ring.h>
#include <Ringer.h>
#include <StatisticFilters.h>
#include <TowerDatabase.h>
#include <Tower.h>
#include "..\DucoEngineTest\ToString.h"
#include <fstream>
#include <cpr/api.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Duco;

namespace DucoEngineTest
{

    TEST_CLASS(BellboardPerformanceParser_UnitTests), Duco::BellboardPerformanceParserCallback, Duco::RenumberProgressCallback, Duco::ImportExportProgressCallback
    {
        Duco::RingingDatabase database;
        Duco::BellboardPerformanceParser* importer;
        bool expectedErrors;
        bool expectedFatalError;

    public:
        BellboardPerformanceParser_UnitTests()
            : expectedErrors(false), expectedFatalError(false)
        {
            database.ClearDatabase(false, true);
            importer = new BellboardPerformanceParser(database, *this, NULL);
        }
        ~BellboardPerformanceParser_UnitTests()
        {
            delete importer;
        }

        // From ImportExportProgressCallback
        void InitialisingImportExport(bool internalising, unsigned int versionNumber, size_t totalNumberOfObjects)
        {
        }
        void ObjectProcessed(bool internalising, Duco::TObjectType objectType, Duco::ObjectId objectId, int totalPercentage, size_t numberInThisSubDatabase)
        {
        }
        void ImportExportComplete(bool internalising)
        {
        }
        void ImportExportFailed(bool internalising, int errorCode)
        {
            Assert::IsTrue(false);
        }

        // From RenumberProgressCallback
        void RenumberInitialised(RenumberProgressCallback::TRenumberStage newStage)
        {

        }
        void RenumberStep(size_t objectId, size_t total)
        {

        }
        void RenumberComplete()
        {

        }

        // From BellboardPerformanceParserCallback
        void Completed(const std::wstring& bellboardId, bool errors)
        {
            Assert::AreEqual(expectedErrors, errors);
        }
        void Cancelled(const std::wstring& bellboardId, const char* message)
        {
            Assert::IsTrue(expectedFatalError);
        }

        void FindCheckRinger(RingingDatabase& database, const Duco::Peal& thePeal, unsigned int bellNo, bool strapper, const std::wstring& expectedName) const
        {
            Duco::ObjectId ringerId;
            Assert::IsTrue(thePeal.RingerId(bellNo, strapper, ringerId));
            const Ringer* const pealRinger = database.RingersDatabase().FindRinger(ringerId);
            Assert::AreEqual(expectedName, pealRinger->FullName(database.Settings().LastNameFirst()));
        }

        void CheckMissingRinger(RingingDatabase& database, const Duco::DownloadedPeal& thePeal, unsigned int bellNo, bool strapper, const std::wstring& expectedName) const
        {
            Duco::ObjectId ringerId;
            Assert::IsTrue(thePeal.IsMissingRinger(bellNo, strapper));
            Assert::IsFalse(thePeal.RingerId(bellNo, strapper, ringerId));
            Assert::IsFalse(ringerId.ValidId());
            Assert::AreEqual(expectedName, thePeal.MissingRinger(bellNo, strapper, database.Settings()));
        }

        void CheckNoRinger(RingingDatabase& database, const Duco::DownloadedPeal& thePeal, unsigned int bellNo, bool strapper, const std::wstring& expectedName) const
        {
            Duco::ObjectId ringerId;
            Assert::IsFalse(thePeal.IsMissingRinger(bellNo, strapper));
            Assert::IsFalse(thePeal.RingerId(bellNo, strapper, ringerId));
            Assert::IsFalse(ringerId.ValidId());
            Assert::AreEqual(expectedName, thePeal.MissingRinger(bellNo, strapper, database.Settings()));
        }
        void CheckNotMissingRinger(RingingDatabase& database, const Duco::DownloadedPeal& thePeal, unsigned int bellNo, bool strapper, const std::wstring& expectedName) const
        {
            Duco::ObjectId ringerId;
            Assert::IsFalse(thePeal.IsMissingRinger(bellNo, strapper));
            Assert::IsTrue(thePeal.RingerId(bellNo, strapper, ringerId));
            Assert::IsTrue(ringerId.ValidId());
            Assert::AreEqual(expectedName, thePeal.MissingRinger(bellNo, strapper, database.Settings()));
        }

        TEST_METHOD(BellboardPerformanceParser_Import_NonExistingFile)
        {
            database.ClearDatabase(false, true);
            
            expectedFatalError = true;
            importer->ParseFile("hghghg.xml");
            //Should assert on completion - see callback
        }

        TEST_METHOD(BellboardPerformanceParser_ID_1031498_InvalidFile)
        {
            expectedFatalError = true;
            database.ClearDatabase(false, true);
            Assert::IsTrue(importer->ParseFile("bellboard_1031498_Invalid.xml"));
            //Check callback for error handling
        }

        TEST_METHOD(BellboardPerformanceParser_ParseTwoAtTheSameTime)
        {
            expectedErrors = true;
            //expectedFatalError = true;
            database.ClearDatabase(false, true);
            Assert::IsTrue(importer->DownloadPerformance(L"441092"));
            Assert::IsTrue(importer->DownloadPerformance(L"17274"));
            //Check callback for error handling
        }

        TEST_METHOD(BellboardPerformanceParser_ID_1031498_EverythingInDatabase)
        {
            std::string tempDatabase("bellboard_1031498.duc");
            {
                database.ClearDatabase(false, true);
                database.Settings().SetLastNameFirst(false);
                database.RingersDatabase().AddRinger(L"Trevor W Marchbank");
                database.RingersDatabase().AddRinger(L"Peter L Furniss");
                database.RingersDatabase().AddRinger(L"John M Thurman");
                database.RingersDatabase().AddRinger(L"George H Campling");
                database.RingersDatabase().AddRinger(L"Neil J Murray");
                database.RingersDatabase().AddRinger(L"Lee Pinnington");
                database.RingersDatabase().AddRinger(L"Carol A Marchbank");
                database.RingersDatabase().AddRinger(L"Andrew D Sibson");
                database.RingersDatabase().AddRinger(L"Pauline Campling");
                database.RingersDatabase().AddRinger(L"Geoffrey R Gardner");
                database.AssociationsDatabase().CreateAssociation(L"Lancashire Association");

                importer->EnableMethodsDatabase(database.MethodsDatabase(), "allmeths.xml");
                importer->EnableDoveDatabase("dove.csv", "");

                importer->ParseFile("bellboard_1031498.xml");
                Assert::IsTrue(importer->ImportedTower());
                Assert::IsTrue(importer->ImportedMethod());
                ValidatePealForID1031498(importer->CurrentPerformance());
                database.PealsDatabase().AddObject(importer->CurrentPerformance());
                ValidatePealForID1031498(*database.PealsDatabase().FindPeal(1, false));

                Assert::IsTrue(database.RebuildDatabase(this));
                ValidatePealForID1031498(*database.PealsDatabase().FindPeal(1, false));

                database.Externalise(tempDatabase.c_str(), this);
            }

            RingingDatabase readDatabase;
            unsigned int databaseVersion = 0;
            readDatabase.Internalise(tempDatabase.c_str(), this, databaseVersion);
            Assert::AreEqual(DucoConfiguration::LatestDatabaseVersion(), databaseVersion);

            Duco::StatisticFilters filters(database);
            Assert::AreEqual((size_t)10, readDatabase.NumberOfRingers());
            Assert::AreEqual((size_t)1, readDatabase.PerformanceInfo(filters).TotalPeals());
            ValidatePealForID1031498(*readDatabase.PealsDatabase().FindPeal(1, false));
            Assert::IsFalse(importer->CurrentPerformance().HasMissingRingers());
            Assert::AreEqual(L"", importer->CurrentPerformance().MissingRingers(database.Settings()).c_str());
        }

        void ValidatePealForID1031498(const Duco::Peal& thePeal)
        {
            Assert::AreEqual(L"1031498", thePeal.BellBoardId().c_str());
            Assert::AreEqual(L"5507.1146", thePeal.RingingWorldReference().c_str());
            Assert::AreEqual(L"For evensong", thePeal.Footnotes().c_str());
            Assert::AreEqual(L"Lancashire Association", thePeal.AssociationName(database).c_str());
            Assert::AreEqual(L"30/10/2016", thePeal.Date().Str().c_str());
            Assert::AreEqual(PerformanceTime(180), thePeal.Time());
            Assert::AreEqual((unsigned int)5040, thePeal.NoOfChanges());
            Assert::AreEqual(L"John M Thurman (No.1)", thePeal.Composer().c_str());

            const Tower* const theTower = database.TowersDatabase().FindTower(thePeal.TowerId(), false);
            Assert::AreEqual(L"Burnley", theTower->City().c_str());
            Assert::AreEqual(L"St Peter", theTower->Name().c_str());
            Assert::AreEqual(L"Lancashire", theTower->County().c_str());
            Assert::IsTrue(thePeal.RingId().ValidId());
            const Duco::Ring* const theRing = theTower->FindRing(thePeal.RingId());
            Assert::AreEqual((unsigned int)10, theRing->NoOfBells());
            Assert::AreEqual(L"18-0-8", theRing->TenorWeight().c_str());

            FindCheckRinger(database, thePeal, 1, false, L"Neil J Murray");
            FindCheckRinger(database, thePeal, 2, false, L"Lee Pinnington");
            FindCheckRinger(database, thePeal, 3, false, L"Carol A Marchbank");
            FindCheckRinger(database, thePeal, 4, false, L"Andrew D Sibson");
            FindCheckRinger(database, thePeal, 5, false, L"Pauline Campling");
            FindCheckRinger(database, thePeal, 6, false, L"Geoffrey R Gardner");
            FindCheckRinger(database, thePeal, 7, false, L"Trevor W Marchbank");
            FindCheckRinger(database, thePeal, 8, false, L"Peter L Furniss");
            FindCheckRinger(database, thePeal, 9, false, L"John M Thurman");
            FindCheckRinger(database, thePeal, 10, false, L"George H Campling");

            Assert::AreEqual(L"John M Thurman", thePeal.ConductorName(database).c_str());
            Assert::IsTrue(thePeal.ConductorOnBell(9, false));
            Assert::IsFalse(thePeal.ConductorOnBell(9, true));
            Assert::IsFalse(thePeal.ConductorOnBell(8, true));
            Assert::IsFalse(thePeal.ConductorOnBell(7, false));

            Assert::AreEqual((unsigned int)10, thePeal.NoOfBellsRung(database));
            Assert::AreEqual((unsigned int)10, thePeal.NoOfRingers());
            bool dualOrderMethod = false;
            Assert::AreEqual((unsigned int)10, thePeal.NoOfBellsInMethod(database.MethodsDatabase(), dualOrderMethod));
            Assert::IsFalse(dualOrderMethod);
            Assert::AreEqual((unsigned int)10, thePeal.NoOfBellsInTower(database.TowersDatabase()));
        }
        TEST_METHOD(BellboardPerformanceParser_ID1310725_SomeObjectsMissing_NoImport)
        {
            expectedErrors = true;
            database.ClearDatabase(false, true);
            database.RingersDatabase().AddRinger(L"Lee Pinnington");
            database.RingersDatabase().AddRinger(L"James E Andrews");
            database.RingersDatabase().AddRinger(L"Rachel J Mitchell");
            database.RingersDatabase().AddRinger(L"David T G Jones");
            database.RingersDatabase().AddRinger(L"Andrew B Mills");
            database.RingersDatabase().AddRinger(L"David L Thomas");
            database.RingersDatabase().AddRinger(L"Raymond D Helliwell");
            database.RingersDatabase().AddRinger(L"Matthew D Warburton");
            database.AssociationsDatabase().CreateAssociation(L"Lancashire Association");

            importer->ParseFile("bellboard_1310725.xml");
            Assert::IsFalse(importer->ImportedTower());
            Assert::IsFalse(importer->ImportedMethod());

            database.PealsDatabase().AddObject(importer->CurrentPerformance());

            Assert::AreEqual(L"1310725", importer->CurrentPerformance().BellBoardId().c_str());
            Assert::AreEqual(L"", importer->CurrentPerformance().RingingWorldReference().c_str());
            Assert::AreEqual(L"Rung for BBC Children In Need", importer->CurrentPerformance().Footnotes().c_str());
            Assert::AreEqual(L"Lancashire Association", importer->CurrentPerformance().AssociationName(database).c_str());
            Assert::AreEqual(L"15/11/2019", importer->CurrentPerformance().Date().Str().c_str());
            Assert::AreEqual(PerformanceTime(269), importer->CurrentPerformance().Time());
            Assert::AreEqual((unsigned int)5016, importer->CurrentPerformance().NoOfChanges());
            Assert::AreEqual(L"John Hyden", importer->CurrentPerformance().Composer().c_str());

            Assert::IsFalse(importer->CurrentPerformance().TowerId().ValidId());
            Assert::IsFalse(importer->CurrentPerformance().RingId().ValidId());
            Assert::AreEqual(L"Liverpool, Merseyside. (Cathedral Church of Christ)", importer->CurrentPerformance().MissingTower().c_str());
            Assert::IsFalse(importer->CurrentPerformance().MissingRingOnly());
            Assert::AreEqual(L"82-0-11", importer->CurrentPerformance().MissingTenorWeight().c_str());
            Assert::AreEqual(L"Ab", importer->CurrentPerformance().MissingTenorKey().c_str());
            Assert::AreEqual(L"12 bell peal", importer->CurrentPerformance().MissingRing(database).c_str());

            CheckMissingRinger(database, importer->CurrentPerformance(), 1, false, L"Hunter, Paul B");
            FindCheckRinger(database, importer->CurrentPerformance(), 2, false, L"Pinnington, Lee");
            FindCheckRinger(database, importer->CurrentPerformance(), 3, false, L"Mitchell, Rachel J");
            FindCheckRinger(database, importer->CurrentPerformance(), 4, false, L"Andrews, James E");
            FindCheckRinger(database, importer->CurrentPerformance(), 5, false, L"Jones, David T G");
            FindCheckRinger(database, importer->CurrentPerformance(), 6, false, L"Mills, Andrew B");
            FindCheckRinger(database, importer->CurrentPerformance(), 7, false, L"Thomas, David L");
            FindCheckRinger(database, importer->CurrentPerformance(), 8, false, L"Helliwell, Raymond D");
            CheckMissingRinger(database, importer->CurrentPerformance(), 9, false, L"Marshall, Luke D");
            CheckNoRinger(database, importer->CurrentPerformance(), 9, true, L"");
            CheckMissingRinger(database, importer->CurrentPerformance(), 10, false, L"Kellett, Ben");
            CheckMissingRinger(database, importer->CurrentPerformance(), 11, false, L"Furniss, Peter L");
            CheckMissingRinger(database, importer->CurrentPerformance(), 12, false, L"Mitchell, Lenard J");
            FindCheckRinger(database, importer->CurrentPerformance(), 12, true, L"Warburton, Matthew D");
            Assert::AreEqual((size_t)8, database.NumberOfRingers());
            Assert::IsFalse(importer->CurrentPerformance().MethodId().ValidId());
            Assert::AreEqual(L"Stedman Cinques", importer->CurrentPerformance().MissingMethod().c_str());

            Assert::AreEqual(L"Hunter, Paul B", importer->CurrentPerformance().ConductorName(database).c_str());
            Assert::IsTrue(importer->CurrentPerformance().ConductorOnBell(1, false));
            Assert::IsFalse(importer->CurrentPerformance().ConductorOnBell(1, true));
            Assert::IsFalse(importer->CurrentPerformance().ConductorOnBell(8, true));
            Assert::IsFalse(importer->CurrentPerformance().ConductorOnBell(7, false));
            Assert::IsTrue(importer->CurrentPerformance().IsMissingRingerConductor(1, false));
            Assert::IsFalse(importer->CurrentPerformance().IsMissingRingerConductor(8, false));
            Assert::IsFalse(importer->CurrentPerformance().IsMissingRingerConductor(9, false));
            Assert::AreEqual((unsigned int)13, importer->CurrentPerformance().NoOfRingers(true));
            Assert::AreEqual((unsigned int)12, importer->CurrentPerformance().NoOfRingers(false));
            bool dualOrderMethod = false;
            Assert::AreEqual((unsigned int)0, importer->CurrentPerformance().NoOfBellsInMethod(database.MethodsDatabase(), dualOrderMethod));
            Assert::AreEqual((unsigned int)0, importer->CurrentPerformance().NoOfBellsInTower(database.TowersDatabase()));
            Assert::AreEqual((unsigned int)12, importer->CurrentPerformance().NoOfBellsRung(database));

            Assert::IsTrue(importer->CurrentPerformance().HasMissingRingers());
            database.Settings().SetLastNameFirst(false);
            Assert::AreEqual(L"Paul B Hunter, Luke D Marshall, Ben Kellett, Peter L Furniss and Lenard J Mitchell", importer->CurrentPerformance().MissingRingers(database.Settings()).c_str());
        }

        TEST_METHOD(BellboardPerformanceParser_ID1303466_SomeObjectsMissing_TowerAndMethodImported_PealOnLessBellsThanInTower)
        {
            expectedErrors = true;
            database.ClearDatabase(false, true);
            database.RingersDatabase().AddRinger(L"Barbara A King");
            database.RingersDatabase().AddRinger(L"John I White");
            database.RingersDatabase().AddRinger(L"Joanne C Wilby");
            database.RingersDatabase().AddRinger(L"Ann White");
            database.RingersDatabase().AddRinger(L"Stephanie J Warboys");
            database.RingersDatabase().AddRinger(L"Simon J Dixon");

            Duco::StatisticFilters filters(database);
            Assert::AreEqual((size_t)0, database.NumberOfTowers());
            Assert::AreEqual((size_t)6, database.NumberOfRingers());
            Assert::AreEqual((size_t)0, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)0, database.NumberOfMethods());

            importer->EnableMethodsDatabase(database.MethodsDatabase(), "allmeths.xml");
            importer->EnableDoveDatabase("dove.csv", "");

            importer->ParseFile("bellboard_1303466.xml");
            Assert::IsTrue(importer->ImportedTower());
            Assert::IsTrue(importer->ImportedMethod());

            database.PealsDatabase().AddObject(importer->CurrentPerformance());

            Assert::AreEqual((size_t)1, database.NumberOfTowers());
            Assert::AreEqual((size_t)6, database.NumberOfRingers());
            Assert::AreEqual((size_t)1, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)1, database.NumberOfMethods());
            Assert::AreEqual(L"1303466", importer->CurrentPerformance().BellBoardId().c_str());
            Assert::AreEqual(L"", importer->CurrentPerformance().RingingWorldReference().c_str());
            Assert::AreEqual(L"Rung prior to the Installation and Induction of Paula Challen as Rector of the Tove benefice.", importer->CurrentPerformance().Footnotes().c_str());
            Assert::AreEqual(L"", importer->CurrentPerformance().AssociationName(database).c_str());
            Assert::AreEqual(L"21/9/2019", importer->CurrentPerformance().Date().Str().c_str());
            Assert::AreEqual(PerformanceTime(192), importer->CurrentPerformance().Time());
            Assert::AreEqual((unsigned int)5040, importer->CurrentPerformance().NoOfChanges());
            Assert::AreEqual(L"Robert W Lee", importer->CurrentPerformance().Composer().c_str());

            Assert::IsTrue(importer->CurrentPerformance().TowerId().ValidId());
            Assert::AreEqual(L"", importer->CurrentPerformance().MissingTower().c_str());
            Assert::IsFalse(importer->CurrentPerformance().MissingRingOnly());
            Assert::AreEqual(L"", importer->CurrentPerformance().MissingTenorWeight().c_str());
            Assert::AreEqual(L"", importer->CurrentPerformance().MissingTenorKey().c_str());
            Assert::AreEqual(L"", importer->CurrentPerformance().MissingRing(database).c_str());
            Assert::AreEqual(Duco::ObjectId(1), importer->CurrentPerformance().TowerId());
            Assert::AreEqual(Duco::ObjectId(0), importer->CurrentPerformance().RingId());
            const Tower* const theTower = database.TowersDatabase().FindTower(importer->CurrentPerformance().TowerId(), false);
            const Duco::Ring* const theRing = theTower->FindRing(importer->CurrentPerformance().RingId());
            //Assert::AreEqual((unsigned int)10, theRing->NoOfBells()); - didnt create a new ring.
            Assert::AreEqual(L"23-1-16", theRing->TenorWeight().c_str());

            FindCheckRinger(database, importer->CurrentPerformance(), 1, false, L"Warboys, Stephanie J");
            FindCheckRinger(database, importer->CurrentPerformance(), 2, false, L"White, Ann");
            FindCheckRinger(database, importer->CurrentPerformance(), 3, false, L"Wilby, Joanne C");
            FindCheckRinger(database, importer->CurrentPerformance(), 4, false, L"King, Barbara A");
            FindCheckRinger(database, importer->CurrentPerformance(), 5, false, L"Dixon, Simon J");
            CheckMissingRinger(database, importer->CurrentPerformance(), 6, false, L"Bulleid, Christopher E");
            CheckNoRinger(database, importer->CurrentPerformance(), 6, true, L"");
            CheckMissingRinger(database, importer->CurrentPerformance(), 7, false, L"Masters, Brett C");
            CheckMissingRinger(database, importer->CurrentPerformance(), 8, false, L"Stanworth, A John");
            CheckMissingRinger(database, importer->CurrentPerformance(), 9, false, L"Allton, Richard I");
            FindCheckRinger(database, importer->CurrentPerformance(), 10, false, L"White, John I");
            Assert::AreEqual((size_t)6, database.NumberOfRingers());

            Assert::IsTrue(importer->CurrentPerformance().MethodId().ValidId());
            Assert::AreEqual(L"Yorkshire Surprise Royal", database.MethodsDatabase().FindMethod(importer->CurrentPerformance().MethodId(), true)->FullName(database.MethodsDatabase()).c_str());
            Assert::AreEqual(L"St Lawrence Society, Towcester", importer->CurrentPerformance().MissingAssociation().c_str());

            Assert::AreEqual(L"Allton, Richard I", importer->CurrentPerformance().ConductorName(database).c_str());
            Assert::IsTrue(importer->CurrentPerformance().ConductorOnBell(9, false));
            Assert::IsTrue(importer->CurrentPerformance().IsMissingRingerConductor(9, false));
            Assert::IsFalse(importer->CurrentPerformance().IsMissingRingerConductor(8, false));
            Assert::IsFalse(importer->CurrentPerformance().IsMissingRingerConductor(8, true));
            Assert::IsFalse(importer->CurrentPerformance().IsMissingRingerConductor(10, false));
            Assert::AreEqual((unsigned int)10, importer->CurrentPerformance().NoOfRingers(true));
            Assert::AreEqual((unsigned int)10, importer->CurrentPerformance().NoOfRingers(false));
            Assert::AreEqual((unsigned int)12, importer->CurrentPerformance().NoOfBellsInTower(database.TowersDatabase()));
            bool dualOrderMethod = false;
            Assert::AreEqual((unsigned int)10, importer->CurrentPerformance().NoOfBellsInMethod(database.MethodsDatabase(), dualOrderMethod));
            //Assert::AreEqual((unsigned int)10, importer->CurrentPerformance().NoOfBellsRung(database)); // Wrong ring selected

            Assert::IsTrue(importer->CurrentPerformance().HasMissingRingers());
            Assert::AreEqual(L"Bulleid, Christopher E; Masters, Brett C; Stanworth, A John and Allton, Richard I", importer->CurrentPerformance().MissingRingers(database.Settings()).c_str());
        }

        TEST_METHOD(BellboardPerformanceParser_ID1303466_AllObjectsMissing_NoImport)
        {
            expectedErrors = true;
            database.ClearDatabase(false, true);
            database.Settings().SetLastNameFirst(false);

            importer->ParseFile("bellboard_1303466.xml");
            Assert::IsFalse(importer->ImportedTower());
            Assert::IsFalse(importer->ImportedMethod());

            database.PealsDatabase().AddObject(importer->CurrentPerformance());
            Duco::StatisticFilters filters(database);

            Assert::AreEqual((size_t)0, database.NumberOfTowers());
            Assert::AreEqual((size_t)0, database.NumberOfRingers());
            Assert::AreEqual((size_t)1, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual(L"1303466", importer->CurrentPerformance().BellBoardId().c_str());
            Assert::AreEqual(L"", importer->CurrentPerformance().RingingWorldReference().c_str());
            Assert::AreEqual(L"Rung prior to the Installation and Induction of Paula Challen as Rector of the Tove benefice.", importer->CurrentPerformance().Footnotes().c_str());
            Assert::AreEqual(L"", importer->CurrentPerformance().AssociationName(database).c_str());
            Assert::AreEqual(L"21/9/2019", importer->CurrentPerformance().Date().Str().c_str());
            Assert::AreEqual(PerformanceTime(192), importer->CurrentPerformance().Time());
            Assert::AreEqual((unsigned int)5040, importer->CurrentPerformance().NoOfChanges());
            Assert::AreEqual(L"Robert W Lee", importer->CurrentPerformance().Composer().c_str());

            Assert::IsFalse(importer->CurrentPerformance().TowerId().ValidId());
            Assert::IsFalse(importer->CurrentPerformance().RingId().ValidId());
            Assert::AreEqual(L"Towcester, Northamptonshire. (St Lawrence)", importer->CurrentPerformance().MissingTower().c_str());
            Assert::IsFalse(importer->CurrentPerformance().MissingRingOnly());
            Assert::AreEqual(L"23-1-16", importer->CurrentPerformance().MissingTenorWeight().c_str());
            Assert::AreEqual(L"E", importer->CurrentPerformance().MissingTenorKey().c_str());
            Assert::AreEqual(L"10 bell peal", importer->CurrentPerformance().MissingRing(database).c_str());

            Assert::IsTrue(importer->CurrentPerformance().HasMissingRingers());
            CheckMissingRinger(database, importer->CurrentPerformance(), 1, false, L"Stephanie J Warboys");
            CheckMissingRinger(database, importer->CurrentPerformance(), 2, false, L"Ann White");
            CheckMissingRinger(database, importer->CurrentPerformance(), 3, false, L"Joanne C Wilby");
            CheckMissingRinger(database, importer->CurrentPerformance(), 4, false, L"Barbara A King");
            CheckMissingRinger(database, importer->CurrentPerformance(), 5, false, L"Simon J Dixon");
            CheckMissingRinger(database, importer->CurrentPerformance(), 6, false, L"Christopher E Bulleid");
            CheckNoRinger(database, importer->CurrentPerformance(), 6, true, L"");
            CheckMissingRinger(database, importer->CurrentPerformance(), 7, false, L"Brett C Masters");
            CheckMissingRinger(database, importer->CurrentPerformance(), 8, false, L"A John Stanworth");
            CheckMissingRinger(database, importer->CurrentPerformance(), 9, false, L"Richard I Allton");
            CheckMissingRinger(database, importer->CurrentPerformance(), 10, false, L"John I White");
            CheckNoRinger(database, importer->CurrentPerformance(), 11, false, L"");

            Assert::IsFalse(importer->CurrentPerformance().MethodId().ValidId());
            Assert::AreEqual(L"Yorkshire Surprise Royal", importer->CurrentPerformance().MissingMethod().c_str());
            Assert::AreEqual(L"St Lawrence Society, Towcester", importer->CurrentPerformance().MissingAssociation().c_str());

            Assert::AreEqual(L"Richard I Allton", importer->CurrentPerformance().ConductorName(database).c_str());
            Assert::IsTrue(importer->CurrentPerformance().ConductorOnBell(9, false));
            Assert::IsTrue(importer->CurrentPerformance().IsMissingRingerConductor(9, false));
            Assert::IsFalse(importer->CurrentPerformance().IsMissingRingerConductor(8, false));
            Assert::IsFalse(importer->CurrentPerformance().IsMissingRingerConductor(8, true));
            Assert::IsFalse(importer->CurrentPerformance().IsMissingRingerConductor(10, false));
            Assert::AreEqual((unsigned int)10, importer->CurrentPerformance().NoOfRingers(true));
            Assert::AreEqual((unsigned int)10, importer->CurrentPerformance().NoOfRingers(false));
            bool dualOrder = false;
            Assert::AreEqual((unsigned int)0, importer->CurrentPerformance().NoOfBellsInMethod(database.MethodsDatabase(), dualOrder));
            Assert::AreEqual((unsigned int)0, importer->CurrentPerformance().NoOfBellsInTower(database.TowersDatabase()));
            Assert::AreEqual((unsigned int)10, importer->CurrentPerformance().NoOfBellsRung(database));

            Assert::AreEqual(L"Stephanie J Warboys, Ann White, Joanne C Wilby, Barbara A King, Simon J Dixon, Christopher E Bulleid, Brett C Masters, A John Stanworth, Richard I Allton and John I White", importer->CurrentPerformance().MissingRingers(database.Settings()).c_str());
        }


        TEST_METHOD(BellboardPerformanceParser_ID1107622_Then_ID1051452)
        {
            expectedErrors = true;
            database.ClearDatabase(false, true);
            Duco::StatisticFilters filters(database);
            Duco::ObjectId alanAPaul = database.RingersDatabase().AddRinger(L"Alan A Paul");
            database.RingersDatabase().AddRinger(L"Bridget Paul");
            //database.RingersDatabase().AddRinger(L"Joanne C Lovell"); // Created after peal created
            database.RingersDatabase().AddRinger(L"Foster, Barbara A");
            database.RingersDatabase().AddRinger(L"Barry E Saunders");
            database.RingersDatabase().AddRinger(L"Graham C Paul");
            database.RingersDatabase().AddRinger(L"James Clatworthy");
            database.RingersDatabase().AddRinger(L"Andrew W R Wilby");
            std::wstring firstPealAssociation = L"Saint Lawrence Society, Towcester";
            database.AssociationsDatabase().CreateAssociation(firstPealAssociation);
            Assert::AreEqual((size_t)7, database.NumberOfRingers());
            Assert::AreEqual((size_t)1, database.NumberOfAssociations());

            importer->EnableMethodsDatabase(database.MethodsDatabase(), "allmeths.xml");
            importer->EnableDoveDatabase("dove.csv", "");

            importer->ParseFile("bellboard_1107622.xml");
            Assert::IsTrue(importer->ImportedTower());
            Assert::IsTrue(importer->ImportedMethod());
            CheckMissingRinger(database, importer->CurrentPerformance(), 3, false, L"Lovell, Joanne C");
            ObjectId joanne = database.RingersDatabase().AddRinger(L"Joanne C Lovell");

            {
                DownloadedPeal updatedPeal = importer->CurrentPerformance();
                updatedPeal.SetRingerId(joanne, 3, false);

                Assert::AreEqual(firstPealAssociation, importer->CurrentPerformance().AssociationName(database));
                Assert::AreEqual(L"David W Beard", importer->CurrentPerformance().Composer().c_str());
                FindCheckRinger(database, importer->CurrentPerformance(), 1, false, L"Paul, Alan A");
                FindCheckRinger(database, importer->CurrentPerformance(), 2, false, L"Paul, Bridget");
                FindCheckRinger(database, updatedPeal, 3, false, L"Lovell, Joanne C");
                FindCheckRinger(database, updatedPeal, 4, false, L"Foster, Barbara A");
                FindCheckRinger(database, updatedPeal, 5, false, L"Saunders, Barry E");
                FindCheckRinger(database, updatedPeal, 6, false, L"Paul, Graham C");
                FindCheckRinger(database, updatedPeal, 7, false, L"Clatworthy, James");
                FindCheckRinger(database, updatedPeal, 8, false, L"Wilby, Andrew W R");
                Assert::AreEqual(L"30/11/2002", updatedPeal.Date().Str().c_str());
                Assert::AreEqual(PerformanceTime(179), updatedPeal.Time());
                Assert::AreEqual((unsigned int)5120, updatedPeal.NoOfChanges());
                Assert::AreEqual(L"Bristol Surprise Major", database.MethodsDatabase().FindMethod(updatedPeal.MethodId())->FullName(database).c_str());
                Assert::IsTrue(updatedPeal.TowerId().ValidId());
                Assert::IsTrue(updatedPeal.RingId().ValidId());
                const Tower* const theTower = database.TowersDatabase().FindTower(importer->CurrentPerformance().TowerId(), false);
                const Duco::Ring* const theRing = theTower->FindRing(importer->CurrentPerformance().RingId());
                Assert::AreEqual((unsigned int)8, theRing->NoOfBells());
                Assert::AreEqual(L"15-1-20", theRing->TenorWeight().c_str());

                Assert::AreEqual(L"", updatedPeal.MissingAssociation().c_str());
                Assert::AreEqual(L"", updatedPeal.MissingRingers(database.Settings()).c_str());
                Assert::AreEqual(L"", updatedPeal.MissingMethod().c_str());
                Assert::IsFalse(updatedPeal.MissingRingOnly());
                Assert::AreEqual(L"", updatedPeal.MissingTenorKey().c_str());
                Assert::AreEqual(L"", updatedPeal.MissingTenorWeight().c_str());
                Assert::AreEqual(L"", updatedPeal.MissingTower().c_str());
                Assert::AreEqual((unsigned int)8, updatedPeal.NoOfBellsRung(database));
                database.PealsDatabase().AddObject(updatedPeal);
                Assert::AreEqual((size_t)1, database.PerformanceInfo(filters).TotalPeals());
                Assert::AreEqual((size_t)1, database.NumberOfTowers());
                Assert::AreEqual((size_t)1, database.NumberOfMethods());
                Assert::AreEqual((size_t)8, database.NumberOfRingers());
            } // First peal out of scope

            // Start second peal import
            expectedErrors = true;
            importer->EnableMethodsDatabase(database.MethodsDatabase(), "");
            importer->EnableDoveDatabase("", "");
            importer->ParseFile("bellboard_1051452.xml");

            Assert::IsFalse(importer->ImportedTower());
            Assert::IsFalse(importer->ImportedMethod());
            Assert::IsFalse(importer->CurrentPerformance().TowerId().ValidId());
            Assert::IsFalse(importer->CurrentPerformance().RingId().ValidId());
            Assert::AreEqual(L"21/12/2002", importer->CurrentPerformance().Date().Str().c_str());
            Assert::AreEqual(PerformanceTime(169), importer->CurrentPerformance().Time());
            Assert::AreEqual((unsigned int)5088, importer->CurrentPerformance().NoOfChanges());

            Assert::AreEqual(L"South Northamptonshire Society", importer->CurrentPerformance().MissingAssociation().c_str());
            Assert::AreEqual(L"Paul, Jennie; Mason, Paul M; Firminger, Nicola E; Dawson, George A and Higson, Andrew D", importer->CurrentPerformance().MissingRingers(database.Settings()).c_str());
            Assert::AreEqual(L"London Surprise Major", importer->CurrentPerformance().MissingMethod().c_str());
            //Assert::AreEqual(L"London Surprise Major", importer->CurrentPerformance().MethodName(database).c_str());
            Assert::IsFalse(importer->CurrentPerformance().MissingRingOnly());
            Assert::AreEqual(L"", importer->CurrentPerformance().MissingTenorKey().c_str());
            Assert::AreEqual(L"14-3-7", importer->CurrentPerformance().MissingTenorWeight().c_str());
            Assert::AreEqual(L"Northampton, Northamptonshire. (Holy Sepulchre)", importer->CurrentPerformance().MissingTower().c_str());
            Assert::IsFalse(importer->CurrentPerformance().MethodId().ValidId());
            Assert::IsFalse(importer->CurrentPerformance().TowerId().ValidId());
            Assert::IsFalse(importer->CurrentPerformance().RingId().ValidId());
            CheckMissingRinger(database, importer->CurrentPerformance(), 1, false, L"Paul, Jennie");
            FindCheckRinger(database, importer->CurrentPerformance(), 2, false, L"Paul, Bridget");
            CheckMissingRinger(database, importer->CurrentPerformance(), 3, false, L"Mason, Paul M");
            CheckMissingRinger(database, importer->CurrentPerformance(), 4, false, L"Firminger, Nicola E");
            CheckMissingRinger(database, importer->CurrentPerformance(), 5, false, L"Dawson, George A");
            FindCheckRinger(database, importer->CurrentPerformance(), 6, false, L"Saunders, Barry E");
            FindCheckRinger(database, importer->CurrentPerformance(), 7, false, L"Paul, Graham C");
            CheckMissingRinger(database, importer->CurrentPerformance(), 8, false, L"Higson, Andrew D");
            bool strapper = false;
            Assert::IsFalse(importer->CurrentPerformance().ContainsRinger(alanAPaul, strapper));
            CheckNotMissingRinger(database, importer->CurrentPerformance(), 2, false, L"");
            CheckNotMissingRinger(database, importer->CurrentPerformance(), 6, false, L"");
            CheckNotMissingRinger(database, importer->CurrentPerformance(), 7, false, L"");
            Assert::AreEqual((unsigned int)8, importer->CurrentPerformance().NoOfBellsRung(database));

            database.PealsDatabase().AddObject(importer->CurrentPerformance());
            Assert::AreEqual((size_t)2, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)1, database.NumberOfTowers());
            Assert::AreEqual((size_t)1, database.NumberOfMethods());
            Assert::AreEqual((size_t)8, database.NumberOfRingers());
        }

        TEST_METHOD(BellboardPerformanceParser_ID1356821_MissingTenorAndMethod_CheckRingerNumbers)
        {
            expectedErrors = true;
            database.ClearDatabase(false, true);
            database.Settings().SetLastNameFirst(false);
            Duco::ObjectId alanAPaul = database.RingersDatabase().AddRinger(L"Alan A Paul");
            Duco::ObjectId methodId = database.MethodsDatabase().AddMethod(L"(7m)", L"Surprise", 6, false);

            importer->ParseFile("bellboard_1356821.xml");

            DownloadedPeal updatedPeal = importer->CurrentPerformance();

            Assert::AreEqual((unsigned int)6, updatedPeal.NoOfRingers());
            Assert::AreEqual((unsigned int)6, updatedPeal.NoOfBellsRung(database));

            updatedPeal.SetRingerId(KNoId, 6, false);

            Assert::AreEqual((unsigned int)6, updatedPeal.NoOfBellsRung(database));
            Assert::AreEqual((unsigned int)6, updatedPeal.NoOfRingers());

            updatedPeal.SetRingerId(KNoId, 8, false);

            Assert::AreEqual((unsigned int)6, updatedPeal.NoOfRingers());
            Assert::AreEqual((unsigned int)6, updatedPeal.NoOfBellsRung(database));

            CheckMissingRinger(database, updatedPeal, 1, false, L"Bridget Paul");
            CheckMissingRinger(database, updatedPeal, 2, false, L"Nicola Firminger");
            FindCheckRinger(database, updatedPeal, 3, false, L"Alan A Paul");
            CheckMissingRinger(database, updatedPeal, 4, false, L"Colin E Sampson");
            CheckMissingRinger(database, updatedPeal, 5, false, L"Graham C Paul");
            CheckMissingRinger(database, updatedPeal, 6, false, L"Barry E Saunders");
            Assert::AreEqual(L"Graham C Paul", updatedPeal.ConductorName(database.RingersDatabase(), database.Settings()).c_str());
            Assert::IsFalse(updatedPeal.ConductorOnBell(2, false));
            Assert::IsTrue(updatedPeal.ConductorOnBell(5, false));
            Assert::IsFalse(updatedPeal.ConductorOnBell(6, false));

            Duco::ObjectId nicolaF = database.RingersDatabase().AddRinger(L"Nicola Firminger");
            updatedPeal.SetRingerId(nicolaF, 2, false);
            FindCheckRinger(database, updatedPeal, 2, false, L"Nicola Firminger");

            updatedPeal.SetRingerId(KNoId, 5, false);
            CheckMissingRinger(database, updatedPeal, 5, false, L"Graham C Paul");
            Assert::AreEqual(L"Graham C Paul", updatedPeal.ConductorName(database.RingersDatabase(), database.Settings()).c_str());
            Assert::IsFalse(updatedPeal.ConductorOnBell(2, false));
            Assert::IsTrue(updatedPeal.ConductorOnBell(5, false));
            Assert::IsFalse(updatedPeal.ConductorOnBell(6, false));

            Duco::ObjectId graham = database.RingersDatabase().AddRinger(L"Graham C Paul");
            updatedPeal.SetRingerId(graham, 5, false);
            FindCheckRinger(database, updatedPeal, 5, false, L"Graham C Paul");
            Assert::AreEqual(L"Graham C Paul", updatedPeal.ConductorName(database.RingersDatabase(), database.Settings()).c_str());
            Assert::IsFalse(updatedPeal.ConductorOnBell(2, false));
            Assert::IsTrue(updatedPeal.ConductorOnBell(5, false));
            Assert::IsFalse(updatedPeal.ConductorOnBell(6, false));

            Assert::AreEqual(methodId, updatedPeal.MethodId());

            Duco::StatisticFilters filters(database);
            Assert::AreEqual((size_t)0, database.PerformanceInfo(filters).TotalPeals());
            Assert::AreEqual((size_t)0, database.NumberOfTowers());
            Assert::AreEqual((size_t)1, database.NumberOfMethods());
            Assert::AreEqual((size_t)3, database.NumberOfRingers());
        }

        TEST_METHOD(BellboardPerformanceParser_YACR)
        {
            expectedErrors = true;
            database.ClearDatabase(false, true);

            Tower barnoldswick(KNoId, L"St Mary Le Ghyll", L"BARNOLDSWICK", L"Lancashire", 6);
            barnoldswick.AddRing(6, L"12", L"A♭");
            Duco::ObjectId barnoldswickId = database.TowersDatabase().AddObject(barnoldswick);
            Assert::IsTrue(barnoldswickId.ValidId());

            importer->ParseFile("bellboard_1336396.xml");

            CheckPealFor1336396(barnoldswickId);
        }

        TEST_METHOD(BellboardPerformanceParser_QuarterWithSimulatedSound)
        {
            expectedErrors = true;
            database.ClearDatabase(false, true);

            importer->ParseFile("bellboard_1114276.xml");

            Assert::AreEqual(1264u, importer->CurrentPerformance().NoOfChanges());
            Assert::IsTrue(importer->CurrentPerformance().SimulatedSound());
        }

        TEST_METHOD(BellboardPerformanceParser_ID1336396_Download)
        {
            expectedErrors = true;
            database.ClearDatabase(false, true);

            Tower barnoldswick(KNoId, L"St Mary Le Ghyll", L"BARNOLDSWICK", L"Lancashire", 6);
            barnoldswick.AddRing(6, L"12", L"A♭");
            Duco::ObjectId barnoldswickId = database.TowersDatabase().AddObject(barnoldswick);
            Assert::IsTrue(barnoldswickId.ValidId());

            importer->DownloadPerformance(L"1336396");

            CheckPealFor1336396(barnoldswickId);
        }

        void CheckPealFor1336396(const Duco::ObjectId& barnoldswickId)
        {
            CheckMissingRinger(database, importer->CurrentPerformance(), 1, false, L"Holland, Andrew J");
            CheckMissingRinger(database, importer->CurrentPerformance(), 2, false, L"Holland, Jane");
            CheckMissingRinger(database, importer->CurrentPerformance(), 3, false, L"Hutchings, Raymond A");
            CheckMissingRinger(database, importer->CurrentPerformance(), 4, false, L"Dove, C Barrie");
            CheckMissingRinger(database, importer->CurrentPerformance(), 5, false, L"Price, Kevin M");
            CheckMissingRinger(database, importer->CurrentPerformance(), 6, false, L"Gordon, Andrew W");

            Assert::AreEqual(barnoldswickId, importer->CurrentPerformance().TowerId());
            Assert::IsTrue(importer->CurrentPerformance().RingId().ValidId());
            Assert::AreEqual(L"", importer->CurrentPerformance().MissingTenorWeight().c_str());
            Assert::AreEqual(L"", importer->CurrentPerformance().MissingTenorKey().c_str());
            Assert::AreEqual(L"", importer->CurrentPerformance().MissingTower().c_str());

            Assert::AreEqual(L"One extent each of London, Wells, York, Primrose and Norwich and two of Cambridge.", importer->CurrentPerformance().MultiMethods().c_str());
            Assert::AreEqual(L"Surprise Minor (6m)", importer->CurrentPerformance().MissingMethod().c_str());
            Assert::AreEqual(L"Price, Kevin M", importer->CurrentPerformance().ConductorName(database).c_str());

            Assert::IsFalse(importer->CurrentPerformance().SimulatedSound());
        }

        TEST_METHOD(BellboardPerformanceParser_ID1336396_YACR_SimilarMethodAlreadyExists)
        {
            expectedErrors = true;
            database.ClearDatabase(false, true);

            Tower barnoldswick(KNoId, L"St Mary Le Ghyll", L"BARNOLDSWICK", L"Lancashire", 6);
            barnoldswick.AddRing(6, L"0", L"A♭");
            Duco::ObjectId barnoldswickId = database.TowersDatabase().AddObject(barnoldswick);
            Assert::IsTrue(barnoldswickId.ValidId());

            Method expectedMethod(KNoId, 6, L"(6)", L"Surprise", L"");
            Duco::ObjectId expectedMethodId = database.MethodsDatabase().AddObject(expectedMethod);
            Assert::IsTrue(expectedMethodId.ValidId());

            importer->ParseFile("bellboard_1336396.xml");

            CheckMissingRinger(database, importer->CurrentPerformance(), 1, false, L"Holland, Andrew J");
            CheckMissingRinger(database, importer->CurrentPerformance(), 2, false, L"Holland, Jane");
            CheckMissingRinger(database, importer->CurrentPerformance(), 3, false, L"Hutchings, Raymond A");
            CheckMissingRinger(database, importer->CurrentPerformance(), 4, false, L"Dove, C Barrie");
            CheckMissingRinger(database, importer->CurrentPerformance(), 5, false, L"Price, Kevin M");
            CheckMissingRinger(database, importer->CurrentPerformance(), 6, false, L"Gordon, Andrew W");

            Assert::AreEqual(L"One extent each of London, Wells, York, Primrose and Norwich and two of Cambridge.", importer->CurrentPerformance().MultiMethods().c_str());
            Assert::AreEqual(L"", importer->CurrentPerformance().MissingMethod().c_str());
            Assert::AreEqual(expectedMethodId, importer->CurrentPerformance().MethodId());
            Assert::AreEqual(L"Price, Kevin M", importer->CurrentPerformance().ConductorName(database).c_str());
        }

        TEST_METHOD(BellboardPerformanceParser_1337157_YACR_SimilarMethodAlreadyExists)
        {
            expectedErrors = true;
            database.ClearDatabase(false, true);

            Method expectedMethod(KNoId, 6, L"(7)", L"Surprise", L"");
            Duco::ObjectId expectedMethodId = database.MethodsDatabase().AddObject(expectedMethod);
            Assert::IsTrue(expectedMethodId.ValidId());

            importer->ParseFile("bellboard_1337157.xml");

            Assert::AreEqual(L"", importer->CurrentPerformance().MissingMethod().c_str());
            Assert::AreEqual(expectedMethodId, importer->CurrentPerformance().MethodId());
        }

        TEST_METHOD(BellboardPerformanceParser_1556769_WithFlatSymbol)
        {
            expectedErrors = true;
            database.ClearDatabase(false, true);

            importer->ParseFile("bellboard_1556769.xml");

            Assert::IsFalse(importer->ImportedTower());
            Assert::IsFalse(importer->CurrentPerformance().MissingRingOnly());
            Assert::AreEqual(L"Ab", importer->CurrentPerformance().MissingTenorKey().c_str());
            Assert::AreEqual(L"10-0-12", importer->CurrentPerformance().MissingTenorWeight().c_str());
        }

        TEST_METHOD(BellboardPerformanceParser_ID_39800_TowerWithDifferentNumberOfBellsToPeal)
        {
            expectedErrors = true;
            database.ClearDatabase(false, true);
            database.AssociationsDatabase().CreateAssociation(L"Southampton University Guild");
            unsigned int numberOfBells = 0;
            Duco::ObjectId methodId = database.MethodsDatabase().FindOrCreateMethod(L"Grandsire Caters", numberOfBells);
            Assert::AreEqual((unsigned int)9, numberOfBells);

            importer->EnableDoveDatabase("dove.csv", "");

            importer->ParseFile("bellboard_39800.xml");

            Assert::IsTrue(importer->ImportedTower());
            Assert::IsFalse(importer->ImportedMethod());

            Assert::AreEqual(L"", importer->CurrentPerformance().MissingMethod().c_str());
            Assert::AreEqual(methodId, importer->CurrentPerformance().MethodId());

            Assert::AreEqual((unsigned int)10, importer->CurrentPerformance().NoOfBellsRung(database));
            Assert::AreEqual((unsigned int)10, importer->CurrentPerformance().NoOfRingers(true));
            Assert::AreEqual((unsigned int)10, importer->CurrentPerformance().NoOfBellsInTower(database.TowersDatabase()));
            Assert::IsTrue(importer->CurrentPerformance().TowerId().ValidId());
            Assert::IsTrue(importer->CurrentPerformance().RingId().ValidId());
            Assert::AreEqual(L"", importer->CurrentPerformance().MissingTower().c_str());
            Assert::IsFalse(importer->CurrentPerformance().MissingRingOnly());
        }

        TEST_METHOD(BellboardPerformanceParser_ID_1272897_TowerWithDifferentNumberOfBellsToPeal_LightFrontSix)
        {
            expectedErrors = true;
            database.ClearDatabase(false, true);
            database.AssociationsDatabase().CreateAssociation(L"Southampton University Guild");
            unsigned int numberOfBells = 0;
            Duco::ObjectId methodId = database.MethodsDatabase().FindOrCreateMethod(L"Spliced Surprise Minor", numberOfBells);
            Assert::AreEqual((unsigned int)6, numberOfBells);

            importer->EnableDoveDatabase("dove.csv", "");

            importer->ParseFile("bellboard_1272897.xml");

            Assert::AreEqual(L"5813", importer->TowerBaseId().c_str());
            Assert::IsTrue(importer->ImportedTower());
            Assert::IsFalse(importer->ImportedMethod());

            Assert::AreEqual(L"", importer->CurrentPerformance().MissingMethod().c_str());
            Assert::AreEqual(methodId, importer->CurrentPerformance().MethodId());

            Assert::AreEqual((unsigned int)6, importer->CurrentPerformance().NoOfBellsRung(database));
            Assert::AreEqual((unsigned int)6, importer->CurrentPerformance().NoOfRingers(true));
            Assert::AreEqual((unsigned int)6, importer->CurrentPerformance().NoOfBellsInTower(database.TowersDatabase()));
            Assert::IsTrue(importer->CurrentPerformance().TowerId().ValidId());
            Assert::IsTrue(importer->CurrentPerformance().RingId().ValidId());
            Assert::IsFalse(importer->CurrentPerformance().MissingRingOnly());
            Assert::AreEqual(L"", importer->CurrentPerformance().MissingTower().c_str());
            Assert::AreEqual(L"", importer->CurrentPerformance().MissingTenorWeight().c_str());
            Assert::AreEqual(L"", importer->CurrentPerformance().MissingTenorKey().c_str());

            const Tower* const theTower = database.TowersDatabase().FindTower(importer->CurrentPerformance().TowerId(), false);
            Assert::AreEqual(L"Lundy Island", theTower->City().c_str());
            Assert::AreEqual(L"St Helen", theTower->Name().c_str());
            Assert::AreEqual(L"Devon", theTower->County().c_str());
            const Duco::Ring* const theRing = theTower->FindRing(importer->CurrentPerformance().RingId());
            Assert::AreEqual((unsigned int)6, theRing->NoOfBells());
            std::wstring expectedTenorWeight(L"5-1-19");
            Assert::AreEqual(expectedTenorWeight, theRing->TenorWeight());
            Assert::AreEqual(std::wstring(L"C♯"), theRing->TenorKey());
        }

        TEST_METHOD(BellboardPerformanceParser_ID_1272897_TowerWithDifferentNumberOfBellsToPeal_LightFrontSix_TowerExists)
        {
            expectedErrors = true;
            database.ClearDatabase(false, true);
            database.AssociationsDatabase().CreateAssociation(L"Southampton University Guild");
            unsigned int numberOfBells = 0;
            Duco::ObjectId methodId = database.MethodsDatabase().FindOrCreateMethod(L"Spliced Surprise Minor", numberOfBells);
            Assert::AreEqual((unsigned int)6, numberOfBells);
            Tower lundy(KNoId, L"St Helen", L"Lundy Island", L"Devon", 10);
            lundy.AddRing(10, L"13-1-18", L"F♯");
            ObjectId ringId = lundy.AddRing(6, L"5", L"C♯");
            Duco::ObjectId lundyId = database.TowersDatabase().AddObject(lundy);
            Assert::IsTrue(lundyId.ValidId());

            importer->ParseFile("bellboard_1272897.xml");

            Assert::IsFalse(importer->ImportedTower());
            Assert::IsFalse(importer->ImportedMethod());

            Assert::AreEqual(L"", importer->CurrentPerformance().MissingMethod().c_str());
            Assert::AreEqual(methodId, importer->CurrentPerformance().MethodId());

            Assert::AreEqual((unsigned int)6, importer->CurrentPerformance().NoOfBellsRung(database));
            Assert::AreEqual((unsigned int)6, importer->CurrentPerformance().NoOfRingers(true));
            Assert::AreEqual((unsigned int)6, importer->CurrentPerformance().NoOfBellsInTower(database.TowersDatabase()));
            Assert::AreEqual(lundyId, importer->CurrentPerformance().TowerId());
            Assert::AreEqual(ringId, importer->CurrentPerformance().RingId());
            Assert::IsFalse(importer->CurrentPerformance().MissingRingOnly());
            Assert::AreEqual(L"", importer->CurrentPerformance().MissingTower().c_str());
            Assert::AreEqual(L"", importer->CurrentPerformance().MissingTenorWeight().c_str());
            Assert::AreEqual(L"", importer->CurrentPerformance().MissingTenorKey().c_str());
        }

        TEST_METHOD(BellboardPerformanceParser_ID_1272897_TowerWithDifferentNumberOfBellsToPeal_LightFrontSix_RingAlreadyExists)
        {
            expectedErrors = true;
            database.ClearDatabase(false, true);
            database.AssociationsDatabase().CreateAssociation(L"Southampton University Guild");
            unsigned int numberOfBells = 0;
            Duco::ObjectId methodId = database.MethodsDatabase().FindOrCreateMethod(L"Spliced Surprise Minor", numberOfBells);
            Assert::AreEqual((unsigned int)6, numberOfBells);
            Tower lundy(KNoId, L"St Helen", L"Lundy Island", L"Devon", 10);
            Duco::ObjectId ringId = lundy.AddRing(6, L"5-1-19", L"C♯");
            lundy.AddRing(10, L"13-1-18", L"F♯");
            Duco::ObjectId lundyId = database.TowersDatabase().AddObject(lundy);
            Assert::IsTrue(lundyId.ValidId());

            importer->EnableDoveDatabase("dove.csv", "");

            importer->ParseFile("bellboard_1272897.xml");

            Assert::IsFalse(importer->ImportedTower());
            Assert::IsFalse(importer->ImportedMethod());

            Assert::AreEqual(L"", importer->CurrentPerformance().MissingMethod().c_str());
            Assert::AreEqual(methodId, importer->CurrentPerformance().MethodId());

            Assert::AreEqual((unsigned int)6, importer->CurrentPerformance().NoOfBellsRung(database));
            Assert::AreEqual((unsigned int)6, importer->CurrentPerformance().NoOfRingers(true));
            Assert::AreEqual((unsigned int)6, importer->CurrentPerformance().NoOfBellsInTower(database.TowersDatabase()));
            Assert::AreEqual(L"", importer->CurrentPerformance().MissingTower().c_str());
            Assert::AreEqual(L"", importer->CurrentPerformance().MissingTenorWeight().c_str());
            Assert::AreEqual(L"", importer->CurrentPerformance().MissingTenorKey().c_str());
            Assert::IsFalse(importer->CurrentPerformance().MissingRingOnly());
            Assert::AreEqual(lundyId, importer->CurrentPerformance().TowerId());
            Assert::AreEqual(ringId, importer->CurrentPerformance().RingId());
        }

        TEST_METHOD(BellboardPerformanceParser_ID_1561766_RingersWithDoubleBarrelledSurname)
        {
            expectedErrors = true;
            database.ClearDatabase(false, true);
            database.AssociationsDatabase().CreateAssociation(L"Central European Association");
            unsigned int numberOfBells = 0;
            Duco::ObjectId methodId = database.MethodsDatabase().FindOrCreateMethod(L"Bristol Surprise Major", numberOfBells);
            Assert::AreEqual((unsigned int)8, numberOfBells);
            Duco::ObjectId thirzaId = database.RingersDatabase().AddRinger(L"Thirza R", L"de Kok");
            Duco::ObjectId harmJanId = database.RingersDatabase().AddRinger(L"Harm Jan A", L"de Kok");
            Duco::ObjectId cliveId = database.RingersDatabase().AddRinger(L"Clive G", L"Smith");

            importer->ParseFile("bellboard_1561766.xml");

            Assert::IsFalse(importer->ImportedTower());
            Assert::IsFalse(importer->ImportedMethod());

            Assert::AreEqual(L"", importer->CurrentPerformance().MissingMethod().c_str());
            Assert::AreEqual(methodId, importer->CurrentPerformance().MethodId());
            Assert::AreEqual((unsigned int)8, importer->CurrentPerformance().NoOfBellsRung(database));

            Duco::ObjectId foundId;
            Assert::IsTrue(importer->CurrentPerformance().RingerId(1, false, foundId));
            Assert::AreEqual(cliveId, foundId);
            Assert::IsTrue(importer->CurrentPerformance().RingerId(5, false, foundId));
            Assert::AreEqual(thirzaId, foundId);
            Assert::IsTrue(importer->CurrentPerformance().RingerId(7, false, foundId));
            Assert::AreEqual(harmJanId, foundId);
            Assert::AreEqual(L"Bright, Nicola D", importer->CurrentPerformance().MissingRinger(2, false, database.Settings()).c_str());
            Assert::AreEqual(L"Trumpler, Eric R", importer->CurrentPerformance().MissingRinger(3, false, database.Settings()).c_str());
            Assert::AreEqual(L"Bright, Martin J", importer->CurrentPerformance().MissingRinger(4, false, database.Settings()).c_str());
            Assert::AreEqual(L"Diserens, Brian P", importer->CurrentPerformance().MissingRinger(6, false, database.Settings()).c_str());
            Assert::AreEqual(L"Hampson, Catherine E", importer->CurrentPerformance().MissingRinger(8, false, database.Settings()).c_str());

            Assert::AreEqual(harmJanId, *importer->CurrentPerformance().Conductors().begin());
        }

        TEST_METHOD(BellboardPerformanceParser_ID_1582007)
        {
            expectedErrors = true;
            database.ClearDatabase(false, true);

            importer->EnableDoveDatabase("dove.csv", "");
            importer->ParseFile("bellboard_1582007.xml");

            Assert::AreEqual(L"863", importer->TowerBaseId().c_str());
            Assert::IsTrue(importer->ImportedTower());
            Assert::IsFalse(importer->CurrentPerformance().Handbell());
            Assert::IsFalse(importer->CurrentPerformance().Withdrawn());
        }

        TEST_METHOD(BellboardPerformanceParser_ID_1561432_False)
        {
            expectedErrors = true;
            database.ClearDatabase(false, true);

            importer->EnableDoveDatabase("dove.csv", "");
            importer->ParseFile("bellboard_1561432.xml");

            Assert::AreEqual(L"4306", importer->TowerBaseId().c_str());
            Assert::IsTrue(importer->ImportedTower());
            Assert::IsTrue(importer->CurrentPerformance().Withdrawn());
        }

        TEST_METHOD(BellboardPerformanceParser_ID_1614537_Handbell)
        {
            expectedErrors = true;
            database.ClearDatabase(false, true);

            importer->EnableDoveDatabase("dove.csv", "");
            importer->ParseFile("bellboard_1614537.xml");
            CheckPeal_1614537();
        }

        TEST_METHOD(BellboardPerformanceParser_ID_1614537_Handbell_Download)
        {
            expectedErrors = true;
            database.ClearDatabase(false, true);

            importer->EnableDoveDatabase("dove.csv", "");
            importer->DownloadPerformance(L"1614537");
            CheckPeal_1614537();
        }

        void CheckPeal_1614537()
        {
            Assert::AreEqual(L"", importer->TowerBaseId().c_str());
            Assert::IsFalse(importer->ImportedTower());
            Assert::AreEqual(L"Yorkshire Association", importer->CurrentPerformance().MissingAssociation().c_str());
            Assert::AreEqual(L"Romanby, North Yorkshire. (19 The Green)", importer->CurrentPerformance().MissingTower().c_str());
            Assert::IsTrue(importer->CurrentPerformance().Handbell());

            Assert::AreEqual(PerformanceTime(150), importer->CurrentPerformance().Time());
            Assert::AreEqual(PerformanceDate(2023, 4, 28), importer->CurrentPerformance().Date());
            Assert::AreEqual(L"Spliced Surprise Major (4m)", importer->CurrentPerformance().MissingMethod().c_str());
            Assert::AreEqual(L"1280 each Cambridge, Lincolnshire, Rutland; 1216 Yorkshire; 60 com, atw", importer->CurrentPerformance().MultiMethods().c_str());
            Assert::AreEqual(L"1614537", importer->CurrentPerformance().BellBoardId().c_str());
            Assert::AreEqual(L"R D S Brown", importer->CurrentPerformance().Composer().c_str());

            Assert::AreEqual(L"Randall, Peter C", importer->CurrentPerformance().MissingRinger(1, false, database.Settings()).c_str());
            Assert::AreEqual(L"Town, Jennifer A", importer->CurrentPerformance().MissingRinger(2, false, database.Settings()).c_str());
            Assert::AreEqual(L"Enzor, Christopher H", importer->CurrentPerformance().MissingRinger(3, false, database.Settings()).c_str());
            Assert::AreEqual(L"Tithecott, Nicholas C", importer->CurrentPerformance().MissingRinger(4, false, database.Settings()).c_str());
        }

        TEST_METHOD(BellboardPerformanceParser_ID_6080)
        {
            expectedErrors = true;
            database.ClearDatabase(false, true);
            unsigned int dbVer;
            database.Internalise("Nicola_firminger.duc", this, dbVer);

            //importer->EnableDoveDatabase("dove.csv", "");
            importer->DownloadPerformance(L"6080");

            //Assert::AreEqual(L"863", importer->TowerBaseId().c_str());
            //Assert::IsTrue(importer->ImportedTower());
            FindCheckRinger(database, importer->CurrentPerformance(), 1, false, L"Castle, Steve");
            FindCheckRinger(database, importer->CurrentPerformance(), 2, false, L"Esbester, R Mark");
            FindCheckRinger(database, importer->CurrentPerformance(), 3, false, L"LeMarechal, Roy");
            CheckMissingRinger(database, importer->CurrentPerformance(), 4, false, L"Morris, Janet L");
            CheckMissingRinger(database, importer->CurrentPerformance(), 5, false, L"Griffiths, Clare J");
            CheckMissingRinger(database, importer->CurrentPerformance(), 6, false, L"Smith, Anthony P");
        }

        TEST_METHOD(BellboardPerformanceParser_DownloadModifiedPerformance_CheckBBIdIsLatestid)
        {
            expectedErrors = true;
            database.ClearDatabase(false, true);

            importer->DownloadPerformance(L"1628002");

            Assert::AreEqual(L"1628003", importer->CurrentPerformance().BellBoardId().c_str());
            Assert::AreEqual(L"4356", importer->CurrentPerformance().RingingWorldIssue().c_str());
            Assert::AreEqual(L"1044", importer->CurrentPerformance().RingingWorldPage().c_str());
        }

        TEST_METHOD(BellboardPerformanceParser_DownloadModifiedPerformance_CausedCrash)
        {
            expectedErrors = true;
            database.ClearDatabase(false, true);

            importer->DownloadPerformance(L"1354946");

            Assert::AreEqual(L"1354946", importer->CurrentPerformance().BellBoardId().c_str());
        }

        TEST_METHOD(BellboardPerformanceParser_UnusualRWLinkFormt)
        {
            expectedErrors = true;
            database.ClearDatabase(false, true);

            Assert::IsTrue(importer->DownloadPerformance(L"1619290"));

            Assert::AreEqual(L"1619290", importer->CurrentPerformance().BellBoardId().c_str());
            Assert::AreEqual(L"5846a", importer->CurrentPerformance().RingingWorldIssue().c_str());
            Assert::AreEqual(L"487", importer->CurrentPerformance().RingingWorldPage().c_str());

            Assert::IsTrue(importer->CurrentPerformance().ValidRingingWorldLink());
            Assert::IsTrue(importer->CurrentPerformance().ValidRingingWorldReferenceForUpload());
            Assert::AreEqual(L"https://bb.ringingworld.co.uk/issues/2023/487", importer->CurrentPerformance().RingingWorldLink(database.Settings()).c_str());
        }

        TEST_METHOD(BellboardPerformanceParser_InvalidBellboardAddress)
        {
            expectedErrors = true;
            expectedFatalError = true;
            database.ClearDatabase(false, true);
            database.Settings().SetBellBoardURL(L"https://nowhere.com");

            Assert::AreEqual(L"https://nowhere.com/view.php?id=1628002", database.Settings().BellBoardDownloadUrl(L"1628002").c_str());

            Assert::IsFalse(importer->DownloadPerformance(L"1628002"));
            Assert::AreEqual(L"", importer->CurrentPerformance().BellBoardId().c_str());
        }

        TEST_METHOD(BellboardPerformanceParser_Download1510472_withStrapper)
        {
            expectedErrors = true;
            database.ClearDatabase(false, true);

            importer->DownloadPerformance(L"1510472");

            Assert::AreEqual(L"1813212", importer->CurrentPerformance().BellBoardId().c_str());

            Assert::AreEqual(9u, importer->CurrentPerformance().NoOfRingers());
            Assert::AreEqual(8u, importer->CurrentPerformance().NoOfRingers(false));
            CheckMissingRinger(database, importer->CurrentPerformance(), 1, false, L"Sharples, J");
            CheckMissingRinger(database, importer->CurrentPerformance(), 8, false, L"Hawksworth, W");
            CheckMissingRinger(database, importer->CurrentPerformance(), 8, true, L"Downs, Joseph");
        }

        TEST_METHOD(BellboardPerformanceParser_Download1764405)
        {
            expectedErrors = true;
            database.ClearDatabase(false, true);

            importer->DownloadPerformance(L"1764405");

            Assert::AreEqual(L"1764405", importer->CurrentPerformance().BellBoardId().c_str());

            Assert::AreEqual(8u, importer->CurrentPerformance().NoOfRingers());
            Assert::AreEqual(8u, importer->CurrentPerformance().NoOfRingers(false));
            CheckMissingRinger(database, importer->CurrentPerformance(), 1, false, L"Blanchard, Alyson E");
            CheckMissingRinger(database, importer->CurrentPerformance(), 2, false, L"Riley, Alex S");
            CheckMissingRinger(database, importer->CurrentPerformance(), 8, false, L"Riley, Roger S");

            Assert::AreEqual(L"Riley, Alex S", importer->CurrentPerformance().ConductorName(database).c_str());
            Assert::IsTrue(importer->CurrentPerformance().ConductorOnBell(2, false));
            Assert::AreEqual(L"Leeds, West Yorkshire. (Cathedral Church of St Anne)", importer->CurrentPerformance().MissingTower().c_str());

            Assert::AreEqual(L"2927", importer->CurrentPerformance().MissingTowerbaseId().c_str());
            Assert::AreEqual(L"11425", importer->CurrentPerformance().MissingDoveId().c_str());

        }
   };
}
