@echo off

if ["%DevEnvDir%"] == [""] (
    call "C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\Enterprise\\Common7\\Tools\\VsDevCmd.bat"
)

call msbuild DucoUI.sln /property:Configuration=Debug /property:Platform=x64 || exit /b 1

OpenCppCoverage.exe --cover_children --sources %CD%  --modules DucoEngine --modules DucoUI --continue_after_cpp_exception --export_type html:AppCoverageReport -- Debug\x64\DucoUI.exe

start %CD%\\AppCoverageReport\\index.html
