DucoEngine is a database for bell ringing records. It stores them in a flat binary file, it started as an excuse for me to have something to model in C++. It contains separate databases for:
* Associations
* Compositions
* Methods
* Method Series
* Peals / Performances / Quarters
* Pictures
* Ringers
* Towers

The DucoEngine is pretty much under constant development, as and when I find extra things I want it to do... so if you think of anything just email owen@rocketpole.co.uk. I'll probably be more than happy to add it, especially if it's another silly stat.

External libraries
==================
Xerces
------
    This library is used to process and read the XML files (e.g. Export of the database or reading on Central council Method collections, or bell board data). The webpage of this project is here: http://xerces.apache.org/xerces-c/releases_archive.html#Release311

RudeConfig
----------
    This library is used to generate a configuration file for CSV import. The original is here: http://rudeserver.com/config/index.html and its just rebuilt and released exactly as per the original.

libcpr
------
    This Library is used for downloads via http, for example from bell board or felstead data.  The webpage of this project is here: https://github.com/libcpr/cpr

Importing from Dove
===================
You need to download the dove data CSV file from their website, you can do this from here: https://dove.cccbr.org.uk/downloads. I dont redistribute their data with this library.

Dove saints are stored in an abbreviated form, if you want any expanding on import, use the DoveSaints.txt file. This specifies what strings to expand: e.g. "Cathedral church " replaces "Cath Ch "


Generating music from compositions
==================================
There's one more text file in the installation directory, Music.txt, that is used to generate music data in compositions.

* Lines starting with a "#" are ignored.
* Lines starting with "!" are assumed to be roll ups to find anywhere in a change and the string after a "." is the name of this roll up type.

Otherwise lines are assumed to be changes to search for.

Where an "x" is any bell, the first string after the first "." is the name of the change, and subsequent changes after "."'s are sub categories of the change to find.

License
=======
As far as Im concerned anyone can use this library, and modify it in any way they like. I provide no guarantee of any support, and/or acceptance of any liability. As such you can consider it released under MIT license.
All the code for this and the DucoUI component are on github or gitlab, depending on which I currently like the most. Im happy to give any active bell ringer with some programming knowledge access, to view or contribute to it on request.

Publishing
==========
The nuget package for DucoEngine was originally published here: https://www.nuget.org/packages/DucoEngine