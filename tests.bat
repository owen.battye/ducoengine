@echo on
SETLOCAL

call build.bat || exit /b 1

OpenCppCoverage.exe --cover_children --sources %CD%  --modules DucoEngine.dll --continue_after_cpp_exception --excluded_sources *libcpr* --excluded_modules DucoEngineTest --excluded_modules xerces --excluded_modules vstest.console.exe --excluded_modules rudeconfig --export_type html:CoverageReport --excluded_line_regex "\s*\}.*" --excluded_line_regex "\s*else.*" -- vstest.console.exe %CD%\\Debug\\x64\\DucoEngineTest.dll
set TESTSPASSED=%ERRORLEVEL%

start %CD%\\CoverageReport\\index.html

exit /b %TESTSPASSED%
